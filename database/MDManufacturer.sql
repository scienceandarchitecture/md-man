/*
SQLyog Community v11.52 (32 bit)
MySQL - 5.6.16-1~exp1 : Database - mdmanufacturer
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mdmanufacturer` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `mdmanufacturer`;

/*Table structure for table `addressIterator` */

DROP TABLE IF EXISTS `addressIterator`;

CREATE TABLE `addressIterator` (
  `macId` int(11) NOT NULL AUTO_INCREMENT,
  `macAddress` varchar(50) NOT NULL,
  PRIMARY KEY (`macId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `addressIterator` */

insert  into `addressIterator`(`macId`,`macAddress`) values (1,'0A:0B:0C:00:00:55');

/*Table structure for table `customer` */

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `custId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `address` varchar(512) DEFAULT NULL,
  `shipAddress` varchar(512) DEFAULT NULL,
  `instructions` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`custId`)
) ENGINE=InnoDB AUTO_INCREMENT=560072 DEFAULT CHARSET=latin1;

/*Data for the table `customer` */

/*insert  into `customer`(`custId`,`name`,`address`,`shipAddress`,`instructions`) values (560068,'Benison','new address pune','Benison Technology pvt ltd','Instructions'),(560071,'MESHDYNAMICS','2953 Bunker Hill Ln Ste 400,Santa Clara CA 95054','2953 Bunker Hill Ln Ste 400,Santa Clara CA 95054','Customer account UPS of FEDEX Prirority');*/
insert  into `customer`(`custId`,`name`,`address`,`shipAddress`,`instructions`) values (560068,'Benison','new address pune','Benison Technology pvt ltd','Instructions');

/*Table structure for table `deviceTest` */

DROP TABLE IF EXISTS `deviceTest`;

CREATE TABLE `deviceTest` (
  `macAddress` varchar(50) NOT NULL,
  `wlan0Reading` varchar(50) NOT NULL,
  `wlan1Reading` varchar(50) NOT NULL,
  `wlan2Reading` varchar(50) NOT NULL,
  `wlan3Reading` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `deviceTest` */

/*Table structure for table `flashDeployModels` */

DROP TABLE IF EXISTS `flashDeployModels`;

CREATE TABLE `flashDeployModels` (
  `flashdeploymodelsId` int(11) NOT NULL AUTO_INCREMENT,
  `psku` varchar(255) NOT NULL,
  `addressesPerUnit` int(11) NOT NULL,
  `configFileName` varchar(255) NOT NULL,
  `phyModes` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`flashdeploymodelsId`),
  KEY `modelId_idx` (`psku`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `flashDeployModels` */

INSERT  INTO `flashDeployModels`(`flashdeploymodelsId`,`psku`,`addressesPerUnit`,`configFileName`,`phyModes`,`description`) VALUES (1,'MD4444-4444',6,'MD4444-4444.conf','0,0,0,0','-'),(2,'MD4440-4444',6,'MD4440-4444.conf','0,0,0,0','-'),(3,'MD4124-Ixxx',6,'MD4124-Ixxx.conf','0,0,0,0','-'),(4,'MD4255-xAxA',6,'MD4255-xAxA.conf','0,0,0,0','-'),(5,'MD4345-I4x4',6,'MD4345-I4x4.conf','0,0,0,0','-'),(6,'MD4144-4xxx',6,'MD4144-4xxx.conf','0,0,0,0','-'),(7,'MD4254-IAxx',6,'MD4254-IAxx.conf','0,0,0,0','-'),(8,'MD4220-IIxx-S',6,'MD4220-IIxx-S','0,0,0,0','-'),(9,'MD4450-AAAA',6,'MD4450-AAAA.conf','0,0,0,0','-'),(10,'MD4000-FIPS-MD4350-AAIx',6,'MD4000-FIPS-MD4350-AAIx.conf','0,0,0,0','-'),(11,'MD4000-FIPS-MD4250-IAxx',6,'MD4000-FIPS-MD4250-AIxx.conf','0,0,0,0','-'),(12,'MD4150-xAxx',6,'MD4150-xAxx.conf','0,0,0,0','-'),(13,'MS4150-xAxx',6,'MS4150-xAxx.conf','0,0,0,0','-'),(14,'MD4000-FIPS',6,'MD4000-FIPS.conf','0,0,0,0','-'),(15,'MD4000-FIPS-MD4120-Ixxx',6,'MD4000-FIPS-MD4120-Ixxx.conf','0,0,0,0','-'),(16,'MD4000-FIPS-MD4320-IIIx',6,'MD4000-FIPS-MD4320-IIIx.conf','0,0,0,0','-'),(17,'MD4000-FIPS-MD4452-AAIA',6,'MD4000-FIPS-MD4452-AAIA.conf','0,0,0,0','-'),(18,'MD4000-FIPS-MD4458-AAII',6,'MD4000-FIPS-MD4458-AAII.conf','0,0,0,0','-'),(19,'MD4120-Ixxx',6,'MD4120-Ixxx.conf','0,0,0,0','-'),(20,'MD4150-Axxx',6,'MD4150-Axxx.conf','0,0,0,0','-'),(21,'MD4150-Axxx-M',6,'MD4150-Axxx-M.conf','0,0,0,0','-'),(22,'MD4220-BBxx',6,'MD4220-BBxx.conf','2,2,2','-'),(23,'MD4220-GGxx',6,'MD4220-GGxx.conf','2,6,6','-'),(24,'MD4220-IIxx',6,'MD4220-IIxx.conf','2,3,3','-'),(25,'MD4220-IIxx-M',6,'MD4220-IIxx-M.conf','0,0,0,0','-'),(26,'MD4220-xxVV',6,'MD4220-xxVV.conf','0,0,0,0','-'),(27,'MD4224-IIxx',6,'MD4224-IIxx.conf','0,0,0,0','-'),(28,'MD4240-44xx',6,'MD4240-44xx.conf','0,0,0,0','--'),(29,'MD4240-PPxx',6,'MD4240-PPxx.conf','0,0,0,0','-'),(30,'MD4250-AAxx',6,'MD4250-AAxx.conf','2,1,1','-'),(31,'MD4250-AAxx-M',6,'MD4250-AAxx-M.conf','0,0,0,0','-'),(32,'MD4250-AAxx-T',6,'MD4250-AAxx-T.conf','0,0,0,0','-'),(33,'MD4250-IAxx',6,'MD4250-IAxx.conf','0,0,0,0','-'),(34,'MD4254-AAxx',6,'MD4254-AAxx.conf','0,0,0,0','-'),(35,'MD4290-MMxx',6,'MD4290-MMxx.conf','2,2,2','test model for 900MHz'),(36,'MD4290-xxNN',6,'MD4290-xxNN.conf','0,0,0,0','-'),(37,'MD4320-BBBx',6,'MD4320-BBBx.conf','3,2,2,2','-'),(38,'MD4320-GGBx',6,'MD4320-GGBx.conf','3,6,6,2','-'),(39,'MD4320-IIBx',6,'MD4320-IIBx.conf','3,3,3,2','-'),(40,'MD4320-IIIx',6,'MD4320-IIIx.conf','0,0,0,0','-'),(41,'MD4324-IIIx',6,'MD4324-IIIx.conf','0,0,0,0','-'),(42,'MD4325-BBxB',6,'MD4325-BBxB.conf','3,2,2,2','-'),(43,'MD4325-GGxG',6,'MD4325-GGxG.conf','3,6,6,6','-'),(44,'MD4325-IIxI',6,'MD4325-IIxI.conf','3,3,3,3','-'),(45,'MD4340-449x',6,'MD4340-449x.conf','0,0,0,0','interesting config'),(46,'MD4340-44Ax',6,'MD4340-44Ax.conf','0,0,0,0','-'),(47,'MD4340-44Ix',6,'MD4340-44Ix.conf','0,0,0,0','-'),(48,'MD4344-444x',6,'MD4344-444x.conf','0,0,0,0','-'),(49,'MD4345-44x4',6,'MD4345-44x4.conf','0,0,0,0','-'),(50,'MD4350-AA4x',6,'MD4350-AA4x.conf','0,0,0,0','-'),(51,'MD4350-AAAx',6,'MD4350-AAAx.conf','0,0,0,0','-'),(52,'MD4350-AABx',6,'MD4350-AABx.conf','3,1,1,2','-'),(53,'MD4350-AAGx',6,'MD4350-AAGx.conf','3,1,1,6','-'),(54,'MD4350-AAIx',6,'MD4350-AAIx.conf','3,1,1,3','-'),(55,'MD4352-AAAx',6,'MD4352-AAAx.conf','3,1,1,1','PGA'),(56,'MD4352-AAxA',6,'MD4352-AAxA.conf','0,0,0,0','-'),(57,'MD4354-AAIx',6,'MD4354-AAIx.conf','0,0,0,0','-'),(58,'MD4354-AAxA',6,'MD4354-AAxA.conf','0,0,0,0','-'),(59,'MD4355-AAxA',6,'MD4355-AAxA.conf','3,1,1,3','-'),(60,'MD4355-AIxA-33x3',6,'MD4355-AIxA.conf','0,0,0,0','-'),(61,'MD4390-Ix99',6,'MD4390-Ix99.conf','0,0,0,0','-'),(62,'MD4390-IxEE',6,'MD4390-IxEE.conf','0,0,0,0','-'),(63,'MD4390-IxNN',6,'MD4390-IxNN.conf','0,0,0,0','-'),(64,'MD4390-NNIx',6,'MD4390-NNIx.conf','0,0,0,0','-'),(65,'MD4394-999x',6,'MD4394-999x.conf','0,0,0,0','three downlinks'),(66,'MD4422-IIII',6,'MD4422-IIII.conf','0,0,0,0','-'),(67,'MD4424-IIII',6,'MD4424-IIII.conf','4,3,3,3,3','4 BG downlink ROOT'),(68,'MD4425-IIII',6,'MD4425-IIII.conf','0,0,0,0','-'),(69,'MD4442-44A4',6,'MD4442-44A4.conf','0,0,0,0','-'),(70,'MD4448-44II',6,'MD4448-44II.conf','0,0,0,0','-'),(71,'MD4452-AAAA',6,'MD4452-AAAA.conf','0,0,0,0','-'),(72,'MD4452-AABA',6,'MD4452-AABA.conf','4,1,1,2,1','-'),(73,'MD4452-AAGA',6,'MD4452-AAGA.conf','4,1,1,6,1','-'),(74,'MD4452-AAIA',6,'MD4452-AAIA.conf','4,1,1,3,1','-'),(75,'MD4454-AAAA',6,'MD4454-AAAA.conf','4,1,1,1,1','4 A downlink ROOT'),(76,'MD4454-AAI4',6,'MD4454-AAI4.conf','0,0,0,0','-'),(77,'MD4454-AAIA',6,'MD4454-AAIA.conf','4,1,1,3,1','All downlinks'),(78,'MD4454-AAII',6,'MD4454-AAII.conf','0,0,0,0','-'),(79,'MD4454-AIII',6,'MD4454-AIII.conf','4,1,3,3,3','-'),(80,'MD4455-AA4A',6,'MD4455-AA4A.conf','0,0,0,0','-'),(81,'MD4455-AABA',6,'MD4455-AABA.conf','4,1,1,2,1','-'),(82,'MD4455-AAGA',6,'MD4455-AAGA.conf','4,1,1,6,1','-'),(83,'MD4455-AAIA',6,'MD4455-AAIA.conf','4,1,1,3,1','-'),(84,'MD4458-AAII',6,'MD4458-AAII.conf','4,1,1,3,3','-'),(85,'MD4458-IAII',6,'MD4458-IAII.conf','4,3,1,3,3','-'),(86,'MD4495-99I9',6,'MD4495-99I9.conf','0,0,0,0','trainwreck-of-a-node'),(87,'MD4498-99II',6,'MD4498-99II.conf','0,0,0,0','node for Henry'),(88,'MS4458-AAII',6,'MS4458-AAII.conf','0,0,0,0','-'),(89,'MD4245-x4x4',6,'MD4245-x4x4.conf','0.0.0.0','newly add for testing');

/*Table structure for table `forecastItem` */

DROP TABLE IF EXISTS `forecastItem`;

CREATE TABLE `forecastItem` (
  `forecastItemId` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) DEFAULT NULL,
  `fcastId` int(11) DEFAULT NULL,
  `psku` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`forecastItemId`),
  KEY `fcastId` (`fcastId`),
  KEY `psku` (`psku`),
  CONSTRAINT `ForecastMaster_FK_1` FOREIGN KEY (`fcastId`) REFERENCES `forecastMaster` (`fcastId`),
  CONSTRAINT `Product_FK_2` FOREIGN KEY (`psku`) REFERENCES `product` (`psku`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `forecastItem` */

/*insert  into `forecastItem`(`forecastItemId`,`quantity`,`fcastId`,`psku`) values (3,12,3,'psku1'),(9,21,1,'pk1'),(10,34,1,'psku2'),(15,21,2,'pk2'),(16,21,2,'pk1'),(17,200,6,'psku2'),(18,100,6,'psku1'),(19,11,7,'p1'),(20,1,8,'p1'),(21,12,9,'p1');*/

/*Table structure for table `forecastMaster` */

DROP TABLE IF EXISTS `forecastMaster`;

CREATE TABLE `forecastMaster` (
  `fcastId` int(11) NOT NULL AUTO_INCREMENT,
  `custName` varchar(255) DEFAULT NULL,
  `estDate` datetime DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `salesMan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`fcastId`),
  KEY `salesMan` (`salesMan`),
  CONSTRAINT `forecastMaster_ibfk_1` FOREIGN KEY (`salesMan`) REFERENCES `userRole` (`userName`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `forecastMaster` */

/*insert  into `forecastMaster`(`fcastId`,`custName`,`estDate`,`notes`,`salesMan`) values (1,'dynamic','2016-11-20 00:00:00','notes','Benison'),(2,'Benison','2016-11-23 00:00:00','my notes desc','Benison'),(3,'Benison','2016-11-21 00:00:00','my new notes','Benison'),(4,'Benison','2016-11-24 00:00:00','notes','Dynamic'),(5,'abc','2016-11-29 17:09:53','notes ','Dynamic'),(6,'ABC','2016-10-22 00:00:00','My Notes','Benison'),(7,'Benison','2016-12-06 00:00:00','my notes','Benison'),(8,'Benison','2016-12-06 00:00:00','my notes','Dynamic'),(9,'dynamic','2016-12-18 00:00:00','notes','Benison');*/

/*Table structure for table `inventory` */

DROP TABLE IF EXISTS `inventory`;

CREATE TABLE `inventory` (
  `macAddress` varchar(50) NOT NULL,
  `modelId` varchar(50) NOT NULL,
  `serial` varchar(50) NOT NULL,
  `firmwareVersion` varchar(20) DEFAULT NULL,
  `batchNumber` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inventory` */

/*Table structure for table `invoiceDeviceList` */

DROP TABLE IF EXISTS `invoiceDeviceList`;

CREATE TABLE `invoiceDeviceList` (
  `invoiceNumber` varchar(50) NOT NULL,
  `macAddress` varchar(50) NOT NULL,
  `shipId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invoiceDeviceList` */

/*Table structure for table `invoiceItem` */

DROP TABLE IF EXISTS `invoiceItem`;

CREATE TABLE `invoiceItem` (
  `invoiceItemId` int(11) NOT NULL AUTO_INCREMENT,
  `extraDesc` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `psku` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `invid` int(11) DEFAULT NULL,
  PRIMARY KEY (`invoiceItemId`),
  KEY `invid` (`invid`),
  KEY `psku` (`psku`),
  CONSTRAINT `invoiceItem_ibfk_1` FOREIGN KEY (`psku`) REFERENCES `product` (`psku`),
  CONSTRAINT `InvoiceMaster_FK_1` FOREIGN KEY (`invid`) REFERENCES `invoiceMaster` (`invId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `invoiceItem` */

/*insert  into `invoiceItem`(`invoiceItemId`,`extraDesc`,`price`,`psku`,`quantity`,`invid`) values (253,'Item 2',1200,'MD4245-x4x4 ',8,97),(254,'test',1000,'MD4144-4xxx',4,97),(255,'1 vlan',600,'MD4440-4444',4,98),(256,'1-App',700,'MD4440-4444',2,98),(257,'0 app vlan',500,'MD4440-4444',3,98),(258,'desc',10,'MD4144-4xxx',10,99);*/

/*Table structure for table `invoiceItemSubProduct` */

DROP TABLE IF EXISTS `invoiceItemSubProduct`;

CREATE TABLE `invoiceItemSubProduct` (
  `invoiceitemsubproductId` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceItemId` int(11) NOT NULL,
  `subproductName` varchar(10) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`invoiceitemsubproductId`),
  KEY `invoiceItemId_idx` (`invoiceItemId`),
  KEY `subproductName_idx` (`subproductName`),
  CONSTRAINT `invoiceItemId` FOREIGN KEY (`invoiceItemId`) REFERENCES `invoiceItem` (`invoiceItemId`),
  CONSTRAINT `subproductName` FOREIGN KEY (`subproductName`) REFERENCES `subProduct` (`subproductName`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `invoiceItemSubProduct` */

/*insert  into `invoiceItemSubProduct`(`invoiceitemsubproductId`,`invoiceItemId`,`subproductName`,`quantity`) values (112,253,'APP',3),(113,253,'VLAN',2),(114,254,'APP',1),(115,254,'VLAN',1),(116,255,'VLAN',1),(117,256,'APP',1),(118,258,'APP',1);*/

/*Table structure for table `invoiceMaster` */

DROP TABLE IF EXISTS `invoiceMaster`;

CREATE TABLE `invoiceMaster` (
  `status` tinyint(4) NOT NULL,
  `amount` double DEFAULT NULL,
  `custId` int(11) DEFAULT NULL,
  `custName` varchar(50) DEFAULT NULL,
  `invDate` datetime DEFAULT NULL,
  `dueDate` datetime DEFAULT NULL,
  `billTo` varchar(512) DEFAULT NULL,
  `shipTo` varchar(512) DEFAULT NULL,
  `salesRep` varchar(50) DEFAULT NULL,
  `terms` varchar(50) DEFAULT NULL,
  `createdBy` varchar(50) DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `shipping` double DEFAULT NULL,
  `invId` int(11) NOT NULL AUTO_INCREMENT,
  `poNumber` varchar(50) DEFAULT NULL,
  `comments` varchar(512) DEFAULT NULL,
  `closedBy` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`invId`),
  KEY `InvoiceMaster_ibfk_1` (`custId`),
  KEY `closedBy` (`closedBy`),
  KEY `createdBy` (`createdBy`),
  CONSTRAINT `invoiceMaster_ibfk_1` FOREIGN KEY (`custId`) REFERENCES `customer` (`custId`),
  CONSTRAINT `invoiceMaster_ibfk_2` FOREIGN KEY (`closedBy`) REFERENCES `userRole` (`userName`),
  CONSTRAINT `invoiceMaster_ibfk_3` FOREIGN KEY (`createdBy`) REFERENCES `userRole` (`userName`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `invoiceMaster` */

/*insert  into `invoiceMaster`(`status`,`amount`,`custId`,`custName`,`invDate`,`dueDate`,`billTo`,`shipTo`,`salesRep`,`terms`,`createdBy`,`discount`,`shipping`,`invId`,`poNumber`,`comments`,`closedBy`) values (0,13600,560071,'MESHDYNAMICS','2017-02-17 00:00:00','2017-02-28 00:00:00','2953 Bunker Hill Ln Ste 400,Santa Clara CA 95054','2953 Bunker Hill Ln Ste 400,Santa Clara CA 95054','Benison','terms applied',NULL,100,150,97,'12','Customer account UPS of FEDEX Prirority',NULL),(0,5300,560071,'MESHDYNAMICS','2017-02-17 00:00:00','2017-03-02 00:00:00','2953 Bunker Hill Ln Ste 400,Santa Clara CA 95054','2953 Bunker Hill Ln Ste 400,Santa Clara CA 95054','Benison','terms applied',NULL,0,0,98,'2','Customer account UPS of FEDEX Prirority',NULL);*/

/*Table structure for table `invoicePayment` */

DROP TABLE IF EXISTS `invoicePayment`;

CREATE TABLE `invoicePayment` (
  `payId` int(11) NOT NULL AUTO_INCREMENT,
  `amount` double DEFAULT NULL,
  `invId` int(11) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `payDate` datetime DEFAULT NULL,
  `rpId` int(11) DEFAULT NULL,
  PRIMARY KEY (`payId`),
  KEY `invId` (`invId`),
  KEY `rpId` (`rpId`),
  CONSTRAINT `invoicePayment_ibfk_1` FOREIGN KEY (`invId`) REFERENCES `invoiceMaster` (`invId`),
  CONSTRAINT `invoicePayment_ibfk_2` FOREIGN KEY (`rpId`) REFERENCES `rawPayment` (`rpId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invoicePayment` */

/*Table structure for table `invoiceTrackingList` */

DROP TABLE IF EXISTS `invoiceTrackingList`;

CREATE TABLE `invoiceTrackingList` (
  `invId` int(11) DEFAULT NULL,
  `carrier` varchar(50) NOT NULL,
  `trackingNumber` varchar(80) NOT NULL,
  `trackId` int(11) NOT NULL AUTO_INCREMENT,
  `shipId` int(11) DEFAULT NULL,
  PRIMARY KEY (`trackId`),
  KEY `SHIPID` (`shipId`),
  KEY `invId` (`invId`),
  CONSTRAINT `invoiceTrackingList_ibfk_1` FOREIGN KEY (`shipId`) REFERENCES `shipMaster` (`shipId`),
  CONSTRAINT `invoiceTrackingList_ibfk_2` FOREIGN KEY (`invId`) REFERENCES `invoiceMaster` (`invId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invoiceTrackingList` */

/*Table structure for table `jobItem` */

DROP TABLE IF EXISTS `jobItem`;

CREATE TABLE `jobItem` (
  `jobItemId` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceItemId` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `jobId` int(11) DEFAULT NULL,
  PRIMARY KEY (`jobItemId`),
  KEY `jobId` (`jobId`),
  KEY `modelId` (`invoiceItemId`),
  CONSTRAINT `JobItem_FK_1` FOREIGN KEY (`jobId`) REFERENCES `jobMaster` (`jobId`),
  CONSTRAINT `jobItem_ibfk_1` FOREIGN KEY (`invoiceItemId`) REFERENCES `invoiceItem` (`invoiceItemId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `jobItem` */

/*insert  into `jobItem`(`jobItemId`,`invoiceItemId`,`quantity`,`jobId`) values (233,253,8,117),(234,254,4,118),(235,255,2,119),(237,257,3,120),(239,256,2,121),(240,255,2,122);*/

/*Table structure for table `jobItemSubProduct` */

DROP TABLE IF EXISTS `jobItemSubProduct`;

CREATE TABLE `jobItemSubProduct` (
  `jobitemsubproductId` int(11) NOT NULL AUTO_INCREMENT,
  `jobItemId` int(11) NOT NULL,
  `invoiceitemsubproductId` int(11) NOT NULL,
  PRIMARY KEY (`jobitemsubproductId`),
  KEY `jobItemId_idx` (`jobItemId`),
  KEY `invoiceItemSubProductId_idx` (`invoiceitemsubproductId`),
  CONSTRAINT `invoiceitemsubproductId` FOREIGN KEY (`invoiceitemsubproductId`) REFERENCES `invoiceItemSubProduct` (`invoiceitemsubproductId`),
  CONSTRAINT `jobItemId` FOREIGN KEY (`jobItemId`) REFERENCES `jobItem` (`jobItemId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `jobItemSubProduct` */

/* insert  into `jobItemSubProduct`(`jobitemsubproductId`,`jobItemId`,`invoiceitemsubproductId`) values (276,233,113),(277,233,112),(278,234,115),(279,234,114),(280,235,116),(283,239,117),(285,240,116);*/

/*Table structure for table `jobMaster` */

DROP TABLE IF EXISTS `jobMaster`;

CREATE TABLE `jobMaster` (
  `invId` int(11) NOT NULL,
  `jobId` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(32) NOT NULL,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `acknowledged` timestamp NULL DEFAULT NULL,
  `finished` timestamp NULL DEFAULT NULL,
  `needBy` timestamp NULL DEFAULT NULL,
  `ackFinishBy` timestamp NULL DEFAULT NULL,
  `createdBy` varchar(50) DEFAULT NULL,
  `ackBy` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`jobId`),
  KEY `CREATEDBY` (`createdBy`),
  KEY `ackBy` (`ackBy`),
  KEY `invId` (`invId`),
  CONSTRAINT `jobMaster_ibfk_1` FOREIGN KEY (`createdBy`) REFERENCES `userRole` (`userName`),
  CONSTRAINT `jobMaster_ibfk_3` FOREIGN KEY (`createdBy`) REFERENCES `userRole` (`userName`),
  CONSTRAINT `jobMaster_ibfk_4` FOREIGN KEY (`ackBy`) REFERENCES `userRole` (`userName`),
  CONSTRAINT `jobMaster_ibfk_6` FOREIGN KEY (`invId`) REFERENCES `invoiceMaster` (`invId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `jobMaster` */

/*insert  into `jobMaster`(`invId`,`jobId`,`state`,`created`,`acknowledged`,`finished`,`needBy`,`ackFinishBy`,`createdBy`,`ackBy`) values (97,117,'created','2017-02-16 00:00:00',NULL,NULL,'2017-02-22 00:00:00',NULL,'Benison','Benison'),(97,118,'created','2017-02-17 00:00:00',NULL,NULL,'2017-02-17 00:00:00',NULL,'Benison','Benison'),(98,119,'created','2017-02-17 00:00:00',NULL,NULL,'2017-02-23 00:00:00',NULL,'Benison','Benison'),(98,120,'created','2017-02-15 00:00:00',NULL,NULL,'2017-02-15 00:00:00',NULL,'Benison','Benison'),(98,121,'created','2017-02-17 00:00:00',NULL,NULL,'2017-02-17 00:00:00',NULL,'Benison','Benison'),(98,122,'created','2017-02-17 00:00:00',NULL,NULL,'2017-02-17 00:00:00',NULL,'Benison','Benison');*/

/*Table structure for table `jobModelFlashDetails` */

DROP TABLE IF EXISTS `jobModelFlashDetails`;

CREATE TABLE `jobModelFlashDetails` (
  `jobmodelflashdetailsId` int(11) NOT NULL AUTO_INCREMENT,
  `jobId` int(11) DEFAULT NULL,
  `psku` varchar(255) DEFAULT NULL,
  `jobItemId` int(11) DEFAULT NULL,
  `macAddress` varchar(35) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `version` varchar(10) DEFAULT NULL,
  `serialNo` varchar(30) DEFAULT NULL,
  `imageType` tinyint(1) DEFAULT NULL,
  `boardMacAddress` varchar(35) DEFAULT NULL,
  `downloadLink` varchar(1024) DEFAULT NULL,
  `imageName` varchar(1024) DEFAULT NULL,
  `lastUpdatedTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`jobmodelflashdetailsId`),
  KEY `jobId` (`jobId`),
  KEY `modelId` (`psku`),
  CONSTRAINT `jobmodelflashdetails_fk_1` FOREIGN KEY (`jobId`) REFERENCES `jobMaster` (`jobId`),
  CONSTRAINT `jobModelFlashDetails_ibfk_2` FOREIGN KEY (`psku`) REFERENCES `product` (`psku`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `jobModelFlashDetails` */

/*insert  into `jobModelFlashDetails`(`jobmodelflashdetailsId`,`jobId`,`psku`,`macAddress`,`status`,`version`,`serialNo`,`boardMacAddress`,`downloadLink`) values (57,117,'MD4245-x4x4 ','0A:0B:0C:00:00:43','closed','1.1','1','11:11:11:11:11:00',''),(58,117,'MD4245-x4x4 ','0A:0B:0C:00:00:49','open','1','2','11:11:11:11:11:11',''),(59,117,'MD4440-4444','0A:0B:0C:00:00:4F','1','2.0.8','1','94:65:9C:90:3C:2E','http://localhost:8080');*/

/*Table structure for table `login` */

DROP TABLE IF EXISTS `login`;

CREATE TABLE `login` (
  `userId` varchar(16) NOT NULL,
  `password` varchar(100) NOT NULL,
  `employeeId` char(5) NOT NULL,
  `accessLevel` tinyint(3) unsigned NOT NULL,
  `extNo` smallint(6) DEFAULT NULL,
  `userName` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `login` */

/*Table structure for table `model` */

DROP TABLE IF EXISTS `model`;

CREATE TABLE `model` (
  `modelId` varchar(64) NOT NULL,
  `description` varchar(50) NOT NULL,
  `domesticPrice` decimal(10,0) NOT NULL,
  `intlPrice` decimal(10,0) NOT NULL,
  `manfDescription` varchar(50) NOT NULL,
  PRIMARY KEY (`modelId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `model` */

/*Table structure for table `modelMacDetails` */

DROP TABLE IF EXISTS `modelMacDetails`;

CREATE TABLE `modelMacDetails` (
  `modelmacdetailsId` int(11) NOT NULL AUTO_INCREMENT,
  `jobmodelflashdetailsId` int(11) DEFAULT NULL,
  `macAddress` varchar(35) DEFAULT NULL,
  `macType` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`modelmacdetailsId`),
  KEY `jobmodelflashdetailsId_idx` (`jobmodelflashdetailsId`),
  CONSTRAINT `jobmodelflashdetailsId` FOREIGN KEY (`jobmodelflashdetailsId`) REFERENCES `jobModelFlashDetails` (`jobmodelflashdetailsId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `modelMacDetails` */

/*insert  into `modelMacDetails`(`modelmacdetailsId`,`jobmodelflashdetailsId`,`macAddress`) values (68,57,'0A:0B:0C:00:00:43'),(69,57,'0A:0B:0C:00:00:44'),(70,57,'0A:0B:0C:00:00:45'),(71,57,'0A:0B:0C:00:00:46'),(72,57,'0A:0B:0C:00:00:47'),(73,57,'0A:0B:0C:00:00:48'),(74,58,'0A:0B:0C:00:00:49'),(75,58,'0A:0B:0C:00:00:4A'),(76,58,'0A:0B:0C:00:00:4B'),(77,58,'0A:0B:0C:00:00:4C'),(78,58,'0A:0B:0C:00:00:4D'),(79,58,'0A:0B:0C:00:00:4E'),(80,59,'0A:0B:0C:00:00:4F'),(81,59,'0A:0B:0C:00:00:50'),(82,59,'0A:0B:0C:00:00:51'),(83,59,'0A:0B:0C:00:00:52'),(84,59,'0A:0B:0C:00:00:53'),(85,59,'0A:0B:0C:00:00:54');*/

/*Table structure for table `pendingFlashJobs` */

DROP TABLE IF EXISTS `pendingFlashJobs`;

CREATE TABLE `pendingFlashJobs` (
  `macAddress` varchar(50) NOT NULL,
  `modelId` varchar(50) NOT NULL,
  `serialNumber` varchar(50) NOT NULL,
  `qualityCheck` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pendingFlashJobs` */

/*Table structure for table `product` */

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `psku` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `maxAppCount` int(11) DEFAULT NULL,
  `maxVlanCount` int(11) DEFAULT NULL,
  PRIMARY KEY (`psku`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `product` */

insert  into `product`(`psku`,`description`,`maxAppCount`,`maxVlanCount`) values ('MD1250-KJxx','MD1250-KJxx.conf',0,5),('MD1270-KJxx','MD1270-KJxx.conf',0,5),('MD1350-KKJxxx','MD1350-KKJxxx.conf',0,5),('MD1550-KJxx','MD1550-KJxx.conf',0,5),('MD4144-4xxx','4-slot motherboard, Mesh software, 4.9GHz uplink, 4.9GHz scanner,EMA 67 enclosure, pole mounting brackets',5,3),('MD4245-x4x4','4-slot motherboard, Mesh software, 4.9GHz uplink, 4.9GHz scanner,EMA 67 enclosure, pole mounting brackets',5,3),('MD4440-4444','4-slot motherboard, Mesh software, 4.9GHz uplink, 4.9GHz scanner,EMA 67 enclosure, pole mounting brackets',5,3),('MD4454-AAIA','MD4454-AAIA.conf',5,3),('MD4454-AAII','MD4454-AAII.conf',5,3),('MD4454-AII','MD4454-AII.conf',5,3),('MD4455-AA-B','MD4455-AA-B.conf',5,3),('MD4455-AA-G','MD4455-AA-G.conf',5,3),('MD4455-AAGA','MD4455-AAGA.conf',5,3),('MD4455-AAIA','MD4455-AAIA.conf',5,3),('MD4458-AA-BB','MD4458-AA-BB.conf',5,3),('MD4458-AA-GG','MD4458-AA-GG.conf',5,3),('MD6350-KKJxxx','MD6350-KKJxxx.conf',5,5),('MD6351-KKJxxx','MD6351-KKJxxx.conf',5,5),('MD6425-JJJIxx','MD6425-JJJIxx.conf',5,5),('MD6455-CCJAxx','MD6455-CCJAxx.conf',5,5);

/*Table structure for table `productItem` */

DROP TABLE IF EXISTS `productItem`;

CREATE TABLE `productItem` (
  `productItemId` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) DEFAULT NULL,
  `psku` varchar(255) DEFAULT NULL,
  `rsku` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`productItemId`),
  KEY `psku` (`psku`),
  KEY `rsku` (`rsku`),
  CONSTRAINT `Product_FK_1` FOREIGN KEY (`psku`) REFERENCES `product` (`psku`),
  CONSTRAINT `RawInventory_FK_1` FOREIGN KEY (`rsku`) REFERENCES `rawInventory` (`rsku`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `productItem` */

insert  into `productItem`(`productItemId`,`quantity`,`psku`,`rsku`) values (1,48,'MD4245-x4x4','MBENC'),(2,38,'MD4144-4xxx','SR2'),(3,60,'MD4440-4444','MB4'),(4,10,'MD6350-KKJxxx','CM10'),(5,2,'MD6350-KKJxxx','CM10-SC'),(6,3,'MD6425-JJJIxx','MB4'),(7,5,'MD6455-CCJAxx','PoE'),(8,10,'MD4454-AAIA','SR2'),(9,4,'MD4454-AAII','MBENC'),(10,23,'MD4454-AII','PoE'),(11,4,'MD4455-AA-B','MB4'),(12,30,'MD4455-AA-B','PoE'),(13,4,'MD4455-AA-G','MBENC'),(14,2,'MD4455-AAGA','MB4'),(15,5,'MD4455-AAGA','MBENC'),(16,10,'MD4455-AAIA','SR5'),(17,10,'MD4458-AA-BB','SR2'),(18,15,'MD4458-AA-GG','MBENC'),(19,10,'MD6351-KKJxxx','CM10'),(20,10,'MD1350-KKJxxx','CM10'),(21,2,'MD1350-KKJxxx','CM10-SC'),(22,10,'MD1250-KJxx','SR2'),(23,12,'MD1550-KJxx','CM10-SC'),(24,10,'MD1270-KJxx','PoE'),(25,10,'MD1270-KJxx','SR5');

/*Table structure for table `rawInventory` */

DROP TABLE IF EXISTS `rawInventory`;

CREATE TABLE `rawInventory` (
  `rsku` varchar(255) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rsku`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rawInventory` */

insert  into `rawInventory`(`rsku`,`comment`,`description`) values ('CM10','Wistron 400 mW Mini-PCI','03/16/2010'),('CM10-SC','Wistrons @ MeshDynamics (SC)','Updated by MD only [as of 5/28/08]'),('MB4','4-slot motherboard (SP102)','03/16/2010'),('MBENC','Mini-Box enclosure Kit','03/16/2010'),('PoE','24V 2A Power over Ethernet','03/16/2010'),('SR2','Ubiquiti 2.4 GHz Mini-PCI','Non-inventory'),('SR5','Ubiquiti 5 GHz Mini-PCI','Non-inventory');

/*Table structure for table `rawPayment` */

DROP TABLE IF EXISTS `rawPayment`;

CREATE TABLE `rawPayment` (
  `rpId` int(11) NOT NULL AUTO_INCREMENT,
  `amount` double DEFAULT NULL,
  `closed` datetime DEFAULT NULL,
  `closedBy` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `createdBy` varchar(255) DEFAULT NULL,
  `invId` int(11) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `custId` int(11) DEFAULT NULL,
  PRIMARY KEY (`rpId`),
  KEY `closedBy` (`closedBy`),
  KEY `createdBy` (`createdBy`),
   KEY `custId` (`custId`),
  CONSTRAINT `rawPayment_ibfk_1` FOREIGN KEY (`closedBy`) REFERENCES `userRole` (`userName`),
  CONSTRAINT `rawPayment_ibfk_2` FOREIGN KEY (`createdBy`) REFERENCES `userRole` (`userName`),
CONSTRAINT `rawPayment_ibfk_3` FOREIGN KEY (`custId`) REFERENCES `customer` (`custId`)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `rawPayment` */

/*insert  into `rawPayment`(`rpId`,`amount`,`closed`,`closedBy`,`created`,`createdBy`,`invId`,`notes`,`state`) values (1,500,'2011-08-19 00:00:00','benison','2011-08-19 00:00:00','benison',0,'Payment for PO ','open'),(8,1500,'2016-11-25 14:37:44','Dynamic','2016-11-21 00:00:00','Dynamic',0,'Payment for MD','open');*/

/*Table structure for table `rfViewSignUp` */

DROP TABLE IF EXISTS `rfViewSignUp`;

CREATE TABLE `rfViewSignUp` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `organization` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `url` varchar(250) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `city` varchar(200) NOT NULL,
  `state` varchar(200) NOT NULL,
  `country` varchar(50) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `timing` varchar(50) NOT NULL,
  `budgeted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rfViewSignUp` */

/*Table structure for table `shipItem` */

DROP TABLE IF EXISTS `shipItem`;

CREATE TABLE `shipItem` (
  `shipItemId` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) DEFAULT NULL,
  `psku` varchar(255) DEFAULT NULL,
  `shipId` int(11) DEFAULT NULL,
  PRIMARY KEY (`shipItemId`),
  KEY `psku` (`psku`),
  KEY `shipId` (`shipId`),
  CONSTRAINT `shipItem_ibfk_1` FOREIGN KEY (`psku`) REFERENCES `product` (`psku`),
  CONSTRAINT `shipItem_ibfk_2` FOREIGN KEY (`shipId`) REFERENCES `shipMaster` (`shipId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `shipItem` */

/*Table structure for table `shipItemSubProduct` */

DROP TABLE IF EXISTS `shipItemSubProduct`;

CREATE TABLE `shipItemSubProduct` (
  `shipitemsubproductId` int(11) NOT NULL AUTO_INCREMENT,
  `shipItemId` int(11) NOT NULL,
  `invoiceitemsubproductId` int(11) NOT NULL,
  PRIMARY KEY (`shipitemsubproductId`),
  KEY `shipItemId_idx` (`shipItemId`),
  KEY `invoiceitemsubproductId_idx` (`invoiceitemsubproductId`),
  KEY `shipItemId_idx1` (`shipItemId`),
  KEY `invoiceitemsubproductId_idx1` (`invoiceitemsubproductId`),
  KEY `shipItemId` (`shipItemId`),
  CONSTRAINT `` FOREIGN KEY (`shipItemId`) REFERENCES `shipItem` (`shipItemId`),
  CONSTRAINT `invoiceitemsubproductId2` FOREIGN KEY (`invoiceitemsubproductId`) REFERENCES `invoiceItemSubProduct` (`invoiceitemsubproductId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `shipItemSubProduct` */

/*insert  into `shipItemSubProduct`(`shipitemsubproductId`,`shipItemId`,`invoiceitemsubproductId`) values (77,23,108);*/

/*Table structure for table `shipMaster` */

DROP TABLE IF EXISTS `shipMaster`;

CREATE TABLE `shipMaster` (
  `custId` int(11) NOT NULL,
  `shipId` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `acknowledged` datetime DEFAULT NULL,
  `finished` datetime DEFAULT NULL,
  `shipBy` datetime DEFAULT NULL,
  `custName` varchar(50) NOT NULL,
  `address` varchar(512) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `instructions` varchar(512) NOT NULL,
  `commitBy` datetime DEFAULT NULL,
  `invId` int(11) DEFAULT NULL,
  `createdBy` varchar(50) DEFAULT NULL,
  `ackBy` varchar(50) DEFAULT NULL,
  `jobId` int(11) DEFAULT NULL,
  PRIMARY KEY (`shipId`),
  KEY `CREATEDBY` (`createdBy`),
  KEY `JOBID` (`jobId`),
  KEY `INVID` (`invId`),
  KEY `ackBy` (`ackBy`),
  KEY `shipMaster_ibfk_6` (`custId`),
  CONSTRAINT `shipMaster_ibfk_1` FOREIGN KEY (`createdBy`) REFERENCES `userRole` (`userName`),
  CONSTRAINT `shipMaster_ibfk_2` FOREIGN KEY (`jobId`) REFERENCES `jobMaster` (`jobId`),
  CONSTRAINT `shipMaster_ibfk_3` FOREIGN KEY (`invId`) REFERENCES `invoiceMaster` (`invId`),
  CONSTRAINT `shipMaster_ibfk_4` FOREIGN KEY (`createdBy`) REFERENCES `userRole` (`userName`),
  CONSTRAINT `shipMaster_ibfk_5` FOREIGN KEY (`ackBy`) REFERENCES `userRole` (`userName`),
  CONSTRAINT `shipMaster_ibfk_6` FOREIGN KEY (`custId`) REFERENCES `customer` (`custId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `shipMaster` */

/*Table structure for table `stockItems` */

DROP TABLE IF EXISTS `stockItems`;

CREATE TABLE `stockItems` (
  `macAddress` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stockItems` */

/*Table structure for table `subProduct` */

DROP TABLE IF EXISTS `subProduct`;

CREATE TABLE `subProduct` (
  `subproductName` varchar(10) NOT NULL,
  `subproductDescription` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`subproductName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='The sub product table will hold information related to VLAN and APP which can be installed on model with new MacAddress';

/*Data for the table `subProduct` */

insert  into `subProduct`(`subproductName`,`subproductDescription`) values ('APP','App desc'),('VLAN','Vlan desc');

/*Table structure for table `terms` */

DROP TABLE IF EXISTS `terms`;

CREATE TABLE `terms` (
  `termId` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`termId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `terms` */

INSERT  INTO `terms`(`termId`,`text`) VALUES (1,'Due on receipt'),(2,'Advance Payment'),(3,'Payment on Order'),(4,'Pre-pay with order'),(5,'Cash on Delivery'),(6,'Warranty Replacement');

/*Table structure for table `userRole` */

DROP TABLE IF EXISTS `userRole`;

CREATE TABLE `userRole` (
  `userName` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `fullName` varchar(50) NOT NULL,
  `roles` varchar(2048) DEFAULT NULL,
  `views` varchar(2048) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `userRole` */

insert  into `userRole`(`userName`,`password`,`fullName`,`roles`,`views`,`email`) values ('Benison','NMX76sxon821E4veD41bsQ==','Benison Tech','Admin','[\"Purchase Order\",\"Payments\",\"Inventory\",\"Revenue\",\"Products\",\"Job Orders\",\"Ship Orders\",\"PO Summary\",\"Image Generation\",\"Manage User\"]','manik.chandra@benisontech.com'),('pinki','txf1HCPNifpCjLD50B9llw==','pinki','Admin','[\"Purchase Order\",\"Payments\",\"Inventory\",\"Revenue\",\"Products\",\"Job Orders\",\"Ship Orders\",\"PO Summary\",\"Image Generation\",\"Manage User\"]','pinki.d@benisontech.com');

/*Table structure for table `webEmail` */

DROP TABLE IF EXISTS `webEmail`;

CREATE TABLE `webEmail` (
  `toAddress` varchar(50) DEFAULT NULL,
  `company` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `subject` varchar(128) DEFAULT NULL,
  `body` varchar(512) DEFAULT NULL,
  `timeStamp` datetime DEFAULT NULL,
  `id` int(11) NOT NULL,
  `referer` varchar(50) DEFAULT NULL,
  `ipAddress` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `webEmail` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;