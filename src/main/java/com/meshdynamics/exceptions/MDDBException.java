/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MDConstraintViolationException.java
 * Comments : TODO 
 * Created  : 21-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |21-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.exceptions;

public class MDDBException extends Exception {

	private static final long serialVersionUID = 1L;

	private int code;

	public MDDBException() {
		super();
	}

	public MDDBException(String message) {
		super(message);
	}

	public MDDBException(int code, String message, Throwable cause) {
		super(message, cause);
		this.code = code;
	}

	public MDDBException(int code, String message) {
		super(message);
		this.code = code;
	}
	public MDDBException(Throwable cause) {
		super(cause);
	}

	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(int code) {
		this.code = code;
	}
	
}
