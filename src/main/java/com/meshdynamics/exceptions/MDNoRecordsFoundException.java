/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MDNoRecordsFoundException.java
 * Comments : TODO 
 * Created  : 27-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |27-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.exceptions;

public class MDNoRecordsFoundException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MDNoRecordsFoundException() {
		super();
	}

	public MDNoRecordsFoundException(String message) {
		super(message);
	}

	public MDNoRecordsFoundException(Throwable cause) {
		super(cause);
	}
}
