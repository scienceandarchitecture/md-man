/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MDBusinessException.java
 * Comments : TODO 
 * Created  : 21-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |21-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.exceptions;

public class MDBusinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MDBusinessException() {
		super();
	}

	public MDBusinessException(String message) {
		super(message);
	}

	public MDBusinessException(Throwable cause) {
		super(cause);
	}
}
