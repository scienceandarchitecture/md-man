/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ProductItemVo.java
 * Comments : TODO 
 * Created  : Sep 27, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |    Comment                       |     Author    |
 * ---------------------------------------------------------------------------
 * |  0  |Sep 27, 2016   |    Created                    	  |      Pinki	  |  
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.vo;

public class ProductItemVo {
	
	private int quantity;
	private String psku;
	private String description;
	private String rsku;
	private int productItemId;
	
	public int getProductItemId() {
		return productItemId;
	}

	public void setProductItemId(int productItemId) {
		this.productItemId = productItemId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getPsku() {
		return psku;
	}

	public void setPsku(String psku) {
		this.psku = psku;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRsku() {
		return rsku;
	}

	public void setRsku(String rsku) {
		this.rsku = rsku;
	}
}
