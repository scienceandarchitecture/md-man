package com.meshdynamics.vo;

import java.util.List;

public class MacAddressInvoiceItemIdVo {

	int invoiceItemId; 
	String psku;
	int itemQuantity;
	
	List<MacSubProductCountVo>  macSubProducts;
	int addressCount ;
	public int getInvoiceItemId() {
		return invoiceItemId;
	}
	public void setInvoiceItemId(int invoiceItemId) {
		this.invoiceItemId = invoiceItemId;
	}
	public String getPsku() {
		return psku;
	}
	public void setPsku(String psku) {
		this.psku = psku;
	}
	public int getItemQuantity() {
		return itemQuantity;
	}
	public void setItemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}
	public List<MacSubProductCountVo> getMacSubProducts() {
		return macSubProducts;
	}
	public void setMacSubProducts(List<MacSubProductCountVo> macSubProductCount) {
		this.macSubProducts = macSubProductCount;
	}
	public int getAddressCount() {
		return addressCount;
	}
	public void setAddressCount(int addressCount) {
		this.addressCount = addressCount;
	}
}
