package com.meshdynamics.vo;

public class RawInventoryVo {

	private String rsku;
	private String description;
	private String comment;

	public String getRsku() {
		return rsku;
	}

	public void setRsku(String rsku) {
		this.rsku = rsku;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}
