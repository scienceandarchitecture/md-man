/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MacDetailsMetaResultVo.java
 * Comments : TODO 
 * Created  : Mar 24, 2017
 *
 * 
 * Author   :	Pinki 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Mar 24, 2017  | Created                             | Pinki     |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.vo;

import java.util.List;

public class MacDetailsMetaResultVo {

	private String customerName;
	private int invId;
	private int totalMac;
	private int availableMac;
	private int flashedMacCount;
	private List<MacDetailsInvoiceItemVo> macDetailsInvoiceItemList;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public int getInvId() {
		return invId;
	}

	public void setInvId(int invId) {
		this.invId = invId;
	}

	public int getTotalMac() {
		return totalMac;
	}

	public void setTotalMac(int totalMac) {
		this.totalMac = totalMac;
	}

	public int getAvailableMac() {
		return availableMac;
	}

	public void setAvailableMac(int availableMac) {
		this.availableMac = availableMac;
	}

	public int getFlashedMacCount() {
		return flashedMacCount;
	}

	public void setFlashedMacCount(int flashedMacCount) {
		this.flashedMacCount = flashedMacCount;
	}

	public List<MacDetailsInvoiceItemVo> getMacDetailsInvoiceItemList() {
		return macDetailsInvoiceItemList;
	}

	public void setMacDetailsInvoiceItemList(
			List<MacDetailsInvoiceItemVo> macDetailsInvoiceItemList) {
		this.macDetailsInvoiceItemList = macDetailsInvoiceItemList;
	}
}
