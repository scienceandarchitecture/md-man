/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ForecastItemVo.java
 * Comments : TODO 
 * Created  : 13-Sep-2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  1  |13-Sep-2016  | Created                             | Pinki  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/

package com.meshdynamics.vo;

public class ForecastItemVo {
	
	private int fcastId;
	private int quantity;
	private String psku;
	private int forecastItemId;
	//private String itemDescription;

	/**
	 * @return the psku
	 */
	public String getPsku() {
		return psku;
	}

	/**
	 * @param psku
	 *            the psku to set
	 */
	public void setPsku(String psku) {
		this.psku = psku;
	}

	/**
	 * @return the forecastItemId
	 */
	public int getForecastItemId() {
		return forecastItemId;
	}

	/**
	 * @param forecastItemId
	 *            the forecastItemId to set
	 */
	public void setForecastItemId(int forecastItemId) {
		this.forecastItemId = forecastItemId;
	}

	/**
	 * @return the itemDescription
	 *//*
	public String getItemDescription() {
		return itemDescription;
	}

	*//**
	 * @param itemDescription
	 *            the itemDescription to set
	 *//*
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}*/

	/**
	 * 
	 * @return fcastId
	 */
	public int getFcastId() {
		return fcastId;
	}

	/**
	 * 
	 * @param fcastId
	 *            the facstId is set
	 */
	public void setFcastId(int fcastId) {
		this.fcastId = fcastId;
	}

	/**
	 * 
	 * @return quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * 
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
