package com.meshdynamics.vo;


public class JobOrderVoCast {
	
	private int jobId;

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

}
