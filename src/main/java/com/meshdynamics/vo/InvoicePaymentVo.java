/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : InvoicePaymentVo.java
 * Comments : TODO 
 * Created  : 16-Sep-2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |16-Sep-2016  | Created                             | Pinki  		  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/

package com.meshdynamics.vo;

public class InvoicePaymentVo {
	private double amount;
	private String notes;
	private String payDate;
	private int payId;
	private int invId;
	private int rpId;

	/**
	 * 
	 * @return
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * 
	 * @param amount
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/**
	 * 
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * 
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getPayDate() {
		return payDate;
	}

	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}

	/**
	 * 
	 * @return
	 */
	public int getPayId() {
		return payId;
	}

	/**
	 * 
	 * @param payId
	 */
	public void setPayId(int payId) {
		this.payId = payId;
	}

	/**
	 * 
	 * @return
	 */
	public int getInvId() {
		return invId;
	}

	/**
	 * 
	 * @param invId
	 */
	public void setInvId(int invId) {
		this.invId = invId;
	}

	/**
	 * 
	 * @return
	 */
	public int getRpId() {
		return rpId;
	}

	/**
	 * 
	 * @param rpId
	 */
	public void setRpId(int rpId) {
		this.rpId = rpId;
	}

}
