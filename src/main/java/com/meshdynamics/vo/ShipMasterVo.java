package com.meshdynamics.vo;

import java.util.Set;

public class ShipMasterVo {

	private int shipId;
	private String state;
	private String created;
	private String acknowledged;
	private String finished;
	private String shipBy;
	private String custName;
	private String address;
	private String phone;
	private String instructions;
	private String commitBy;
	private int invId;
	private String createdBy;
	private String ackBy;
	private int jobId;
	private int custId;

	public Set<ShipItemVo> shipItems;

	public Set<ShipItemVo> getShipItems() {
		return shipItems;
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public void setShipItems(Set<ShipItemVo> shipItems) {
		this.shipItems = shipItems;
	}

	public int getShipId() {
		return shipId;
	}

	public void setShipId(int shipId) {
		this.shipId = shipId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getAcknowledged() {
		return acknowledged;
	}

	public void setAcknowledged(String acknowledged) {
		this.acknowledged = acknowledged;
	}

	public String getFinished() {
		return finished;
	}

	public void setFinished(String finished) {
		this.finished = finished;
	}

	public String getShipBy() {
		return shipBy;
	}

	public void setShipBy(String shipBy) {
		this.shipBy = shipBy;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public String getCommitBy() {
		return commitBy;
	}

	public void setCommitBy(String commitBy) {
		this.commitBy = commitBy;
	}

	public int getInvId() {
		return invId;
	}

	public void setInvId(int invId) {
		this.invId = invId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getAckBy() {
		return ackBy;
	}

	public void setAckBy(String ackBy) {
		this.ackBy = ackBy;
	}

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}
}
