/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : SubProductVo.java
 * Comments : TODO 
 * Created  : Dec 16, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Dec 16, 2016  | Created                             |Pinki 			|
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.vo;


public class SubProductVo {

	private String subProductName;

	private String subProductDescription;

	/**
	 * @return the subProductName
	 */
	public String getSubProductName() {
		return subProductName;
	}

	/**
	 * @param subProductName
	 *            the subProductName to set
	 */
	public void setSubProductName(String subProductName) {
		this.subProductName = subProductName;
	}

	/**
	 * @return the subProductDescription
	 */
	public String getSubProductDescription() {
		return subProductDescription;
	}

	/**
	 * @param subProductDescription
	 *            the subProductDescription to set
	 */
	public void setSubProductDescription(String subProductDescription) {
		this.subProductDescription = subProductDescription;
	}
}
