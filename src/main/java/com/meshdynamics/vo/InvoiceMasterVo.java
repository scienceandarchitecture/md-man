/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : InvoiceVo.java
 * Comments : TODO 
 * Created  : 15-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |15-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.vo;

import java.util.Set;

public class InvoiceMasterVo {

	private String status;
	private double amount;
	private int custId;
	private String custName;
	private String invDate;
	private String dueDate;
	private String billTo;
	private String shipTo;
	private String salesRep;
	private String terms;
	private String createdBy;
	private double discount;
	private double shipping;
	private String poNumber;
	private String comments;
	private String closedBy;
	private int invId;
	private int termId;
	private double balance;
	private int invoicePaymentCount;
	private int invoiceTrackingCount;
	public Set<InvoiceItemVo> invoiceitems;

	/**
	 * @return the invoicePaymentCount
	 */
	public int getInvoicePaymentCount() {
		return invoicePaymentCount;
	}

	/**
	 * @param invoicePaymentCount
	 *            the invoicePaymentCount to set
	 */
	public void setInvoicePaymentCount(int invoicePaymentCount) {
		this.invoicePaymentCount = invoicePaymentCount;
	}

	/**
	 * @return the invoiceTrackingCount
	 */
	public int getInvoiceTrackingCount() {
		return invoiceTrackingCount;
	}

	/**
	 * @param invoiceTrackingCount
	 *            the invoiceTrackingCount to set
	 */
	public void setInvoiceTrackingCount(int invoiceTrackingCount) {
		this.invoiceTrackingCount = invoiceTrackingCount;
	}

	/**
	 * @return the termId
	 */
	public int getTermId() {
		return termId;
	}

	/**
	 * @param termId
	 *            the termId to set
	 */
	public void setTermId(int termId) {
		this.termId = termId;
	}


	/**
	 * @return the invoiceitems
	 */
	public Set<InvoiceItemVo> getInvoiceitems() {
		return invoiceitems;
	}

	/**
	 * @param invoiceitems
	 *            the invoiceitems to set
	 */
	public void setInvoiceitems(Set<InvoiceItemVo> invoiceitems) {
		this.invoiceitems = invoiceitems;
	}

	/**
	 * @return the invId
	 */
	public int getInvId() {
		return invId;
	}

	/**
	 * @param invId
	 *            the invId to set
	 */
	public void setInvId(int invId) {
		this.invId = invId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/**
	 * @return the custId
	 */
	public int getCustId() {
		return custId;
	}

	/**
	 * @param custId
	 *            the custId to set
	 */
	public void setCustId(int custId) {
		this.custId = custId;
	}

	/**
	 * @return the custName
	 */
	public String getCustName() {
		return custName;
	}

	/**
	 * @param custName
	 *            the custName to set
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}

	/**
	 * @return the billTo
	 */
	public String getBillTo() {
		return billTo;
	}

	/**
	 * @param billTo
	 *            the billTo to set
	 */
	public void setBillTo(String billTo) {
		this.billTo = billTo;
	}

	/**
	 * @return the shipTo
	 */
	public String getShipTo() {
		return shipTo;
	}

	/**
	 * @param shipTo
	 *            the shipTo to set
	 */
	public void setShipTo(String shipTo) {
		this.shipTo = shipTo;
	}

	/**
	 * @return the salesRep
	 */
	public String getSalesRep() {
		return salesRep;
	}

	/**
	 * @param salesRep
	 *            the salesRep to set
	 */
	public void setSalesRep(String salesRep) {
		this.salesRep = salesRep;
	}

	/**
	 * @return the terms
	 */
	public String getTerms() {
		return terms;
	}

	/**
	 * @param terms
	 *            the terms to set
	 */
	public void setTerms(String terms) {
		this.terms = terms;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the discount
	 */
	public double getDiscount() {
		return discount;
	}

	/**
	 * @param discount
	 *            the discount to set
	 */
	public void setDiscount(double discount) {
		this.discount = discount;
	}

	/**
	 * @return the shipping
	 */
	public double getShipping() {
		return shipping;
	}

	/**
	 * @param shipping
	 *            the shipping to set
	 */
	public void setShipping(double shipping) {
		this.shipping = shipping;
	}

	/**
	 * @return the poNumber
	 */
	public String getPoNumber() {
		return poNumber;
	}

	/**
	 * @param poNumber
	 *            the poNumber to set
	 */
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *            the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the closedBy
	 */
	public String getClosedBy() {
		return closedBy;
	}

	/**
	 * @param closedBy
	 *            the closedBy to set
	 */
	public void setClosedBy(String closedBy) {
		this.closedBy = closedBy;
	}

	/**
	 * @return the invDate
	 */
	public String getInvDate() {
		return invDate;
	}

	/**
	 * @param invDate
	 *            the invDate to set
	 */
	public void setInvDate(String invDate) {
		this.invDate = invDate;
	}

	/**
	 * @return the balance
	 */
	public double getBalance() {
		return balance;
	}

	/**
	 * @param balance
	 *            the balance to set
	 */
	public void setBalance(double balance) {
		this.balance = balance;
	}

	/**
	 * @return the dueDate
	 */
	public String getDueDate() {
		return dueDate;
	}

	/**
	 * @param dueDate
	 *            the dueDate to set
	 */
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
}
