package com.meshdynamics.vo;

public class RawInventoryVoCast {
	
	private String rsku;

	public String getRsku() {
		return rsku;
	}

	public void setRsku(String rsku) {
		this.rsku = rsku;
	}

}
