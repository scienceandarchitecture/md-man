package com.meshdynamics.vo;

import java.util.List;

public class RevenueListVo {

	double amount;
	List<RevenueVo> revenueVoList = null;

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public List<RevenueVo> getRevenueVoList() {
		return revenueVoList;
	}

	public void setRevenueVoList(List<RevenueVo> revenueVoList) {
		this.revenueVoList = revenueVoList;
	}
}
