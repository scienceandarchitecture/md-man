package com.meshdynamics.vo;

public class LoginVo {

	private String userId;
	private String password;
	private String employeeId;
	private byte accessLevel;
	private short extNo;
	private String userName;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public byte getAccessLevel() {
		return accessLevel;
	}
	public void setAccessLevel(byte accessLevel) {
		this.accessLevel = accessLevel;
	}
	public short getExtNo() {
		return extNo;
	}
	public void setExtNo(short extNo) {
		this.extNo = extNo;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
}
