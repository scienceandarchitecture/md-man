/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ForecastMasterVo.java
 * Comments : TODO 
 * Created  : 13-Sep-2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  1  |13-Sep-2016  | Created                             | Pinki  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/

package com.meshdynamics.vo;

import java.util.Set;

public class ForecastMasterVo {
	private int fcastId;
	private String salesMan;
	private String custName;
	private String estDate;
	private String notes;

	Set<ForecastItemVo> forecastItemVo;

	/**
	 * @return the fcastId
	 */
	public int getFcastId() {
		return fcastId;
	}

	/**
	 * @param fcastId
	 *            the fcastId to set
	 */
	public void setFcastId(int fcastId) {
		this.fcastId = fcastId;
	}

	/**
	 * 
	 * @return salesMan
	 */
	public String getSalesMan() {
		return salesMan;
	}

	/**
	 * 
	 * @param salesman
	 *            the salesMan to set
	 */
	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}

	/**
	 * 
	 * @return custName
	 */
	public String getCustName() {
		return custName;
	}

	/**
	 * 
	 * @param custName
	 *            the custName to set
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}

	/**
	 * @return the estDate
	 */
	public String getEstDate() {
		return estDate;
	}

	/**
	 * @param estDate
	 *            the estDate to set
	 */
	public void setEstDate(String estDate) {
		this.estDate = estDate;
	}

	public Set<ForecastItemVo> getForecastItemVo() {
		return forecastItemVo;
	}

	public void setForecastItemVo(Set<ForecastItemVo> forecastItemVo) {
		this.forecastItemVo = forecastItemVo;
	}

	/**
	 * 
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * 
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}
}
