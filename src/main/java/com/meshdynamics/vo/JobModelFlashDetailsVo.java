package com.meshdynamics.vo;

public class JobModelFlashDetailsVo {

	private int jobmodelflashdetailsId;
	private int jobId;
	private String psku;
	private String macAddress;
	private String status;
	private String version;
	private String serialNo;
	private String imageType;
	private String boardMacAddress;
	private String downloadLink;
	private String imageName;
	private int jobItemId;
	private int invoiceitemsubproductId;
	private int jobitemsubproductId;

	public int getJobItemId() {
		return jobItemId;
	}

	public void setJobItemId(int jobItemId) {
		this.jobItemId = jobItemId;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public int getJobmodelflashdetailsId() {
		return jobmodelflashdetailsId;
	}

	public void setJobmodelflashdetailsId(int jobmodelflashdetailsId) {
		this.jobmodelflashdetailsId = jobmodelflashdetailsId;
	}

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	public String getPsku() {
		return psku;
	}

	public void setPsku(String psku) {
		this.psku = psku;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	
	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public String getBoardMacAddress() {
		return boardMacAddress;
	}

	public void setBoardMacAddress(String boardMacAddress) {
		this.boardMacAddress = boardMacAddress;
	}

	public String getDownloadLink() {
		return downloadLink;
	}

	public void setDownloadLink(String downloadLink) {
		this.downloadLink = downloadLink;
	}

	@Override
	public String toString() {
		return "JobModelFlashDetailsVo [jobmodelflashdetailsId=" + jobmodelflashdetailsId + ", jobId=" + jobId
				+ ", psku=" + psku + ", macAddress=" + macAddress + ", status=" + status + ", version=" + version
				+ ", serialNo=" + serialNo + ", boardMacAddress=" + boardMacAddress + ", downloadLink=" + downloadLink
				+ "]";
	}

	public int getInvoiceitemsubproductId() {
		return invoiceitemsubproductId;
	}

	public void setInvoiceitemsubproductId(int invoiceitemsubproductId) {
		this.invoiceitemsubproductId = invoiceitemsubproductId;
	}

	public int getJobitemsubproductId() {
		return jobitemsubproductId;
	}

	public void setJobitemsubproductId(int jobitemsubproductId) {
		this.jobitemsubproductId = jobitemsubproductId;
	}

}
