/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ProductVo.java
 * Comments : TODO 
 * Created  : Sep 27, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |    Comment                       |     Author    |
 * ---------------------------------------------------------------------------
 * |  0  |Sep 27, 2016 |    Created                    	  |      Pinki	  |  
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.vo;

import java.util.Set;

public class ProductVo {

	private int productId;
	private String psku;
	private String description;
	private int maxAppCount;
	private int maxVlanCount;
	
	public Set<ProductItemVo> productItems;
		
	public String getPsku() {
		return psku;
	}
	public void setPsku(String psku) {
		this.psku = psku;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getMaxAppCount() {
		return maxAppCount;
	}
	public void setMaxAppCount(int maxAppCount) {
		this.maxAppCount = maxAppCount;
	}
	public int getMaxVlanCount() {
		return maxVlanCount;
	}
	public void setMaxVlanCount(int maxVlanCount) {
		this.maxVlanCount = maxVlanCount;
	}
	public Set<ProductItemVo> getProductItems() {
		return productItems;
	}
	public void setProductItems(Set<ProductItemVo> productItems) {
		this.productItems = productItems;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
}

