package com.meshdynamics.vo;

import java.util.Set;

public class JobItemVo {

	private int jobId;
	private int invoiceItemId;
	private int quantity;
	private int jobItemId;
	private String psku;
	public Set<JobItemSubProductVo> jobItemSubProducts;

	/**
	 * @return the psku
	 */
	public String getPsku() {
		return psku;
	}

	/**
	 * @param psku the psku to set
	 */
	public void setPsku(String psku) {
		this.psku = psku;
	}

	/**
	 * @return the jobItemId
	 */
	public int getJobItemId() {
		return jobItemId;
	}

	/**
	 * @param jobItemId
	 *            the jobItemId to set
	 */
	public void setJobItemId(int jobItemId) {
		this.jobItemId = jobItemId;
	}

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	/**
	 * @return the invoiceItemId
	 */
	public int getInvoiceItemId() {
		return invoiceItemId;
	}

	/**
	 * @param invoiceItemId the invoiceItemId to set
	 */
	public void setInvoiceItemId(int invoiceItemId) {
		this.invoiceItemId = invoiceItemId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the jobItemSubProducts
	 */
	public Set<JobItemSubProductVo> getJobItemSubProducts() {
		return jobItemSubProducts;
	}

	/**
	 * @param jobItemSubProducts the jobItemSubProducts to set
	 */
	public void setJobItemSubProducts(Set<JobItemSubProductVo> jobItemSubProducts) {
		this.jobItemSubProducts = jobItemSubProducts;
	}
}
