package com.meshdynamics.vo;

public class MacSubProductCountVo {
	String subProductName;
	int invoiceItemSubProductQuantity;
	public String getSubProductName() {
		return subProductName;
	}
	public void setSubProductName(String subProductName) {
		this.subProductName = subProductName;
	}
	public int getInvoiceItemSubProductQuantity() {
		return invoiceItemSubProductQuantity;
	}
	public void setInvoiceItemSubProductQuantity(int invoiceItemSubProductQuantity) {
		this.invoiceItemSubProductQuantity = invoiceItemSubProductQuantity;
	}
}
