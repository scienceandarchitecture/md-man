/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MacRangeVo.java
 * Comments : TODO 
 * Created  : Mar 23, 2017
 *
 * 
 * Author   :	Pinki 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Mar 23, 2017  | Created                             | Pinki     |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.vo;

public class MacRangeVo {

	private String custName;
	private int invId;
	private int totalMac;
	private String searchFlag;
	private String boardMacAddress;
	private String macAddress;

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public int getInvId() {
		return invId;
	}

	public void setInvId(int invId) {
		this.invId = invId;
	}

	public int getTotalMac() {
		return totalMac;
	}

	public void setTotalMac(int totalMac) {
		this.totalMac = totalMac;
	}

	public String getSearchFlag() {
		return searchFlag;
	}

	public void setSearchFlag(String searchFlag) {
		this.searchFlag = searchFlag;
	}

	public String getBoardMacAddress() {
		return boardMacAddress;
	}

	public void setBoardMacAddress(String boardMacAddress) {
		this.boardMacAddress = boardMacAddress;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
}
