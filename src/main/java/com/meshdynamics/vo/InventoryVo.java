/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : InventoryVo.java
 * Comments : TODO 
 * Created  : 13-Sep-2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  1  |13-Sep-2016  | Created                             | Pinki  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/

package com.meshdynamics.vo;

public class InventoryVo {
	private String macAddress;
	private String modelId;
	private String serial;
	private String firmwareVersion;
	private int batchNumber;

	/**
	 * 
	 * @return the macAddress
	 */
	public String getMacAddress() {
		return macAddress;
	}

	/**
	 * 
	 * @param macAddress
	 *            the macAddress to set
	 */
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	/**
	 * 
	 * @return the modelId
	 */
	public String getModelId() {
		return modelId;
	}

	/**
	 * 
	 * @param modelId
	 *            the modelId to set
	 */
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	/**
	 * 
	 * @return the serial
	 */
	public String getSerial() {
		return serial;
	}

	/**
	 * 
	 * @param serial
	 *            serial to set
	 */
	public void setSerial(String serial) {
		this.serial = serial;
	}

	/**
	 * 
	 * @return the firmwareVersion
	 */
	public String getFirmwareVersion() {
		return firmwareVersion;
	}

	/**
	 * 
	 * @param firmwareVersion
	 */
	public void setFirmwareVersion(String firmwareVersion) {
		this.firmwareVersion = firmwareVersion;
	}

	/**
	 * 
	 * @return the batchNumber
	 */
	public int getBatchNumber() {
		return batchNumber;
	}

	/**
	 * 
	 * @param batchNumber
	 */
	public void setBatchNumber(int batchNumber) {
		this.batchNumber = batchNumber;
	}
}
