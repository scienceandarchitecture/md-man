package com.meshdynamics.vo;

import java.util.List;

public class MacAddressSummaryVo {

	int custId;
	String customerName;
	int totalMacCount;
	List<MacAddressInvIdVo>  macAddressInvIds ;
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public List<MacAddressInvIdVo> getMacAddressInvIds() {
		return macAddressInvIds;
	}
	public void setMacAddressInvIds(List<MacAddressInvIdVo> macAddressInvIds) {
		this.macAddressInvIds = macAddressInvIds;
	}
	public int getTotalMacCount() {
		return totalMacCount;
	}
	public void setTotalMacCount(int totalMacCount) {
		this.totalMacCount = totalMacCount;
	}
	
	
		
}
