/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : InvoiceDeviceListVo.java
 * Comments : TODO 
 * Created  : 16-Sep-2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |16-Sep-2016  | Created                             | Pinki  		  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.vo;

public class InvoiceDeviceListVo {

	private String invoiceNumber;
	private String macAddress;
	private int shipId;
	
	/**
	 * 
	 * @return the invoiceNumber
	 */
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	
	/**
	 * 
	 * @param invoiceNumber
	 * 		the invoiceNumber to set
	 */
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	
	/**
	 * 
	 * @return the macAddress
	 */
	public String getMacAddress() {
		return macAddress;
	}
	
	/**
	 * 
	 * @param macAddress
	 * 		the macAdddress to set
	 */
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	/**
	 * 
	 * @return the shipId
	 */
	public int getShipId() {
		return shipId;
	}
	/**
	 * 
	 * @param shipId
	 * 		the shipId to set
	 */
	public void setShipId(int shipId) {
		this.shipId = shipId;
	}
}
