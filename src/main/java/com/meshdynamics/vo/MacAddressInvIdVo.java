package com.meshdynamics.vo;

import java.util.List;

public class MacAddressInvIdVo {

	int invId;
	List <MacAddressInvoiceItemIdVo> MacAddressInvoiceItemIs;
	int invMacAddressCount; 
	
	public int getInvId() {
		return invId;
	}
	public void setInvId(int invId) {
		this.invId = invId;
	}
	public List<MacAddressInvoiceItemIdVo> getMacAddressInvoiceItemIs() {
		return MacAddressInvoiceItemIs;
	}
	public void setMacAddressInvoiceItemIs(
			List<MacAddressInvoiceItemIdVo> macAddressInvoiceItemIs) {
		MacAddressInvoiceItemIs = macAddressInvoiceItemIs;
	}
	public int getInvMacAddressCount() {
		return invMacAddressCount;
	}
	public void setInvMacAddressCount(int invMacAddressCount) {
		this.invMacAddressCount = invMacAddressCount;
	}
	
}
