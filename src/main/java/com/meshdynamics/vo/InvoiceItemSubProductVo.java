/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : InvoiceItemSubProductVo.java
 * Comments : TODO 
 * Created  : Dec 16, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Dec 16, 2016  | Created                             | Pinki          |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.vo;

public class InvoiceItemSubProductVo {

	private int invoiceitemsubproductId;
	private int invoiceItemId;
	private String subproductName;
	private int quantity;

	/**
	 * @return the invoiceitemsubproductId
	 */
	public int getInvoiceitemsubproductId() {
		return invoiceitemsubproductId;
	}

	/**
	 * @param invoiceitemsubproductId
	 *            the invoiceitemsubproductId to set
	 */
	public void setInvoiceitemsubproductId(int invoiceitemsubproductId) {
		this.invoiceitemsubproductId = invoiceitemsubproductId;
	}

	/**
	 * @return the invoiceItemId
	 */
	public int getInvoiceItemId() {
		return invoiceItemId;
	}

	/**
	 * @param invoiceItemId
	 *            the invoiceItemId to set
	 */
	public void setInvoiceItemId(int invoiceItemId) {
		this.invoiceItemId = invoiceItemId;
	}

	/**
	 * @return the subproductName
	 */
	public String getSubproductName() {
		return subproductName;
	}

	/**
	 * @param subproductName
	 *            the subproductName to set
	 */
	public void setSubproductName(String subproductName) {
		this.subproductName = subproductName;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + invoiceItemId;
		result = prime * result + invoiceitemsubproductId;
		result = prime * result + quantity;
		result = prime * result
				+ ((subproductName == null) ? 0 : subproductName.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvoiceItemSubProductVo other = (InvoiceItemSubProductVo) obj;
		if (invoiceItemId != other.invoiceItemId)
			return false;
		if (invoiceitemsubproductId != other.invoiceitemsubproductId)
			return false;
		if (quantity != other.quantity)
			return false;
		if (subproductName == null) {
			if (other.subproductName != null)
				return false;
		} else if (!subproductName.equals(other.subproductName))
			return false;
		return true;
	}
}
