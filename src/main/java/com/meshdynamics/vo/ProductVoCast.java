package com.meshdynamics.vo;

public class ProductVoCast {

	private String psku;

	public String getPsku() {
		return psku;
	}

	public void setPsku(String psku) {
		this.psku = psku;
	}

}
