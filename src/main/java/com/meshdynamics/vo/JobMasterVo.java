package com.meshdynamics.vo;

import java.util.Set;

import javax.persistence.Transient;

public class JobMasterVo {

	private int jobId;
	private String state;
	private String created;
	private String acknowledged;
	private String finished;
	private String needBy;
	private String ackFinishBy;
	private String createdBy;
	private String ackBy;
	private int invId;
	private String custName;
	private int generatedImage;
	private int imageToBeGenerated;
	
	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public int getInvId() {
		return invId;
	}

	public void setInvId(int invId) {
		this.invId = invId;
	}

	private Set<JobItemVo> jobItems;

	/**
	 * @return the jobItems
	 */
	public Set<JobItemVo> getJobItems() {
		return jobItems;
	}

	/**
	 * @param jobItems
	 *            the jobItems to set
	 */
	public void setJobItems(Set<JobItemVo> jobItems) {
		this.jobItems = jobItems;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getAcknowledged() {
		return acknowledged;
	}

	public void setAcknowledged(String acknowledged) {
		this.acknowledged = acknowledged;
	}

	public String getFinished() {
		return finished;
	}

	public void setFinished(String finished) {
		this.finished = finished;
	}

	public String getNeedBy() {
		return needBy;
	}

	public void setNeedBy(String needBy) {
		this.needBy = needBy;
	}

	public String getAckFinishBy() {
		return ackFinishBy;
	}

	public void setAckFinishBy(String ackFinishBy) {
		this.ackFinishBy = ackFinishBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getAckBy() {
		return ackBy;
	}

	public void setAckBy(String ackBy) {
		this.ackBy = ackBy;
	}

	public int getGeneratedImage() {
		return generatedImage;
	}

	public void setGeneratedImage(int generatedImage) {
		this.generatedImage = generatedImage;
	}

	public int getImageToBeGenerated() {
		return imageToBeGenerated;
	}

	public void setImageToBeGenerated(int imageToBeGenerated) {
		this.imageToBeGenerated = imageToBeGenerated;
	}
}
