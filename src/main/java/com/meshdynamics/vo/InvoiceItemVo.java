/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : InvoiceItemVo.java
 * Comments : TODO 
 * Created  : 16-Sep-2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |16-Sep-2016  | Created                             | Pinki  		  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.vo;

import java.util.Set;

public class InvoiceItemVo {
	private String psku;
	private int quantity;
	private double price;
	private int invid;
	private String extraDesc;
	private int invoiceItemId;
	public Set<InvoiceItemSubProductVo> invoiceItemSubProducts;

	/**
	 * @return the invoiceItemId
	 */
	public int getInvoiceItemId() {
		return invoiceItemId;
	}

	/**
	 * @param invoiceItemId the invoiceItemId to set
	 */
	public void setInvoiceItemId(int invoiceItemId) {
		this.invoiceItemId = invoiceItemId;
	}

	/**
	 * 
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * 
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * 
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * 
	 * @param price
	 *            the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * 
	 * @return the invid
	 */
	public int getInvid() {
		return invid;
	}

	/**
	 * 
	 * @param invid
	 *            the invid to set
	 */
	public void setInvid(int invid) {
		this.invid = invid;
	}

	/**
	 * 
	 * @return the extraDesc
	 */
	public String getExtraDesc() {
		return extraDesc;
	}

	/**
	 * 
	 * @param extraDesc
	 *            the extraDesc to set
	 */
	public void setExtraDesc(String extraDesc) {
		this.extraDesc = extraDesc;
	}

	/**
	 * @return the psku
	 */
	public String getPsku() {
		return psku;
	}

	/**
	 * @param psku
	 *            the psku to set
	 */
	public void setPsku(String psku) {
		this.psku = psku;
	}

	/**
	 * @return the invoiceItemSubProducts
	 */
	public Set<InvoiceItemSubProductVo> getInvoiceItemSubProducts() {
		return invoiceItemSubProducts;
	}

	/**
	 * @param invoiceItemSubProducts the invoiceItemSubProducts to set
	 */
	public void setInvoiceItemSubProducts(
			Set<InvoiceItemSubProductVo> invoiceItemSubProducts) {
		this.invoiceItemSubProducts = invoiceItemSubProducts;
	}

}
