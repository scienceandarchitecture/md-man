/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MacDetailsInvoiceItemVo.java
 * Comments : TODO 
 * Created  : Mar 24, 2017
 *
 * 
 * Author   :	Pinki 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Mar 24, 2017  | Created                             | Pinki     |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.vo;

import java.util.List;

public class MacDetailsInvoiceItemVo {

	private int invoiceItemId;
	private String psku;
	private String app;
	private String vlan;
	private List<String> macList;
	private int macCount;
	private String boardMac; 
	private String version;

	public int getInvoiceItemId() {
		return invoiceItemId;
	}

	public void setInvoiceItemId(int invoiceItemId) {
		this.invoiceItemId = invoiceItemId;
	}

	public String getPsku() {
		return psku;
	}

	public void setPsku(String psku) {
		this.psku = psku;
	}

	public String getApp() {
		return app;
	}

	public void setApp(String app) {
		this.app = app;
	}

	public String getVlan() {
		return vlan;
	}

	public void setVlan(String vlan) {
		this.vlan = vlan;
	}

	public List<String> getMacList() {
		return macList;
	}

	public void setMacList(List<String> macList) {
		this.macList = macList;
	}

	public int getMacCount() {
		return macCount;
	}

	public void setMacCount(int macCount) {
		this.macCount = macCount;
	}

	public String getBoardMac() {
		return boardMac;
	}

	public void setBoardMac(String boardMac) {
		this.boardMac = boardMac;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}