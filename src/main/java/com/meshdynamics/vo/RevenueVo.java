package com.meshdynamics.vo;

import java.util.List;

public class RevenueVo {

	private int purchaseOrder;
	private String customerName;
	private String salesRep;
	private double amount;
	private String notes;
	private double totalAmount;
	private String recognizedOn;
	private List<InvoiceItemVo> invoiceItems;

	public int getPurchaseOrder() {
		return purchaseOrder;
	}

	public void setPurchaseOrder(int purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getSalesRep() {
		return salesRep;
	}

	public void setSalesRep(String salesRep) {
		this.salesRep = salesRep;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getRecognizedOn() {
		return recognizedOn;
	}

	public void setRecognizedOn(String recognizedOn) {
		this.recognizedOn = recognizedOn;
	}

	public List<InvoiceItemVo> getInvoiceItems() {
		return invoiceItems;
	}

	public void setInvoiceItems(List<InvoiceItemVo> invoiceItems) {
		this.invoiceItems = invoiceItems;
	}
}
