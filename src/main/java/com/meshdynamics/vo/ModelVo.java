package com.meshdynamics.vo;

public class ModelVo {
	
	private String modelId;
	private String description;
	private double domesticPrice;
	private double intlPrice;
	private String manfDescription;
	
	public String getModelId() {
		return modelId;
	}
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getDomesticPrice() {
		return domesticPrice;
	}
	public void setDomesticPrice(double domesticPrice) {
		this.domesticPrice = domesticPrice;
	}
	public double getIntlPrice() {
		return intlPrice;
	}
	public void setIntlPrice(double intlPrice) {
		this.intlPrice = intlPrice;
	}
	public String getManfDescription() {
		return manfDescription;
	}
	public void setManfDescription(String manfDescription) {
		this.manfDescription = manfDescription;
	}
	
	

}
