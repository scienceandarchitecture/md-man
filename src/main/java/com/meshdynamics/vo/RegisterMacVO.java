package com.meshdynamics.vo;

import java.util.List;

public class RegisterMacVO {

	private String boardMacAddress;
	private List<String> rMacAddress;
	private List<String> vlanMacAddress;
	private List<String> appMacAddress;

	public String getBoardMacAddress() {
		return boardMacAddress;
	}

	public void setBoardMacAddress(String boardMacAddress) {
		this.boardMacAddress = boardMacAddress;
	}

	public List<String> getrMacAddress() {
		return rMacAddress;
	}

	public void setrMacAddress(List<String> rMacAddress) {
		this.rMacAddress = rMacAddress;
	}

	public List<String> getVlanMacAddress() {
		return vlanMacAddress;
	}

	public void setVlanMacAddress(List<String> vlanMacAddress) {
		this.vlanMacAddress = vlanMacAddress;
	}

	public List<String> getAppMacAddress() {
		return appMacAddress;
	}

	public void setAppMacAddress(List<String> appMacAddress) {
		this.appMacAddress = appMacAddress;
	}

}
