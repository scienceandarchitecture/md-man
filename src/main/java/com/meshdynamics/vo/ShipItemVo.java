package com.meshdynamics.vo;

import java.util.Set;

public class ShipItemVo {

	private int shipItemId;
	private String psku;
	private String description;
	private int quantity;
	private int shipId;
	public Set<ShipItemSubProductVo> shipItemSubProducts;

	public int getShipItemId() {
		return shipItemId;
	}

	public void setShipItemId(int shipItemId) {
		this.shipItemId = shipItemId;
	}

	public String getPsku() {
		return psku;
	}

	public void setPsku(String psku) {
		this.psku = psku;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getShipId() {
		return shipId;
	}

	public void setShipId(int shipId) {
		this.shipId = shipId;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the shipItemSubProducts
	 */
	public Set<ShipItemSubProductVo> getShipItemSubProducts() {
		return shipItemSubProducts;
	}

	/**
	 * @param shipItemSubProducts
	 *            the shipItemSubProducts to set
	 */
	public void setShipItemSubProducts(
			Set<ShipItemSubProductVo> shipItemSubProducts) {
		this.shipItemSubProducts = shipItemSubProducts;
	}

}
