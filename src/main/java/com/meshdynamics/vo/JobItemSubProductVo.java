/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : JobItemSubProductVo.java
 * Comments : TODO 
 * Created  : Dec 21, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Dec 21, 2016  | Created                             | Pinki		   |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.vo;

import java.util.List;

public class JobItemSubProductVo {

	private int jobitemsubproductId;
	private int jobItemId;
	private int invoiceitemsubproductId;
	private List<InvoiceItemSubProductVo> invoiceItemSubProductList;

	/**
	 * @return the jobitemsubproductId
	 */
	public int getJobitemsubproductId() {
		return jobitemsubproductId;
	}

	/**
	 * @param jobitemsubproductId
	 *            the jobitemsubproductId to set
	 */
	public void setJobitemsubproductId(int jobitemsubproductId) {
		this.jobitemsubproductId = jobitemsubproductId;
	}

	/**
	 * @return the jobItemId
	 */
	public int getJobItemId() {
		return jobItemId;
	}

	/**
	 * @param jobItemId
	 *            the jobItemId to set
	 */
	public void setJobItemId(int jobItemId) {
		this.jobItemId = jobItemId;
	}

	/**
	 * @return the invoiceitemsubproductId
	 */
	public int getInvoiceitemsubproductId() {
		return invoiceitemsubproductId;
	}

	/**
	 * @param invoiceitemsubproductId
	 *            the invoiceitemsubproductId to set
	 */
	public void setInvoiceitemsubproductId(int invoiceitemsubproductId) {
		this.invoiceitemsubproductId = invoiceitemsubproductId;
	}

	/**
	 * @return the invoiceItemSubProductList
	 */
	public List<InvoiceItemSubProductVo> getInvoiceItemSubProductList() {
		return invoiceItemSubProductList;
	}

	/**
	 * @param invoiceItemSubProductList
	 *            the invoiceItemSubProductList to set
	 */
	public void setInvoiceItemSubProductList(
			List<InvoiceItemSubProductVo> invoiceItemSubProductList) {
		this.invoiceItemSubProductList = invoiceItemSubProductList;
	}

}
