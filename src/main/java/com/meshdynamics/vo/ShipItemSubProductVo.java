/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ShipItemSubProductVo.java
 * Comments : TODO 
 * Created  : Dec 21, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Dec 21, 2016  | Created                             | Pinki          |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.vo;

public class ShipItemSubProductVo {

	private int shipitemsubproductId;
	private int shipItemId;
	private int invoiceitemsubproductId;

	/**
	 * @return the shipitemsubproductId
	 */
	public int getShipitemsubproductId() {
		return shipitemsubproductId;
	}

	/**
	 * @param shipitemsubproductId
	 *            the shipitemsubproductId to set
	 */
	public void setShipitemsubproductId(int shipitemsubproductId) {
		this.shipitemsubproductId = shipitemsubproductId;
	}

	/**
	 * @return the shipItemId
	 */
	public int getShipItemId() {
		return shipItemId;
	}

	/**
	 * @param shipItemId
	 *            the shipItemId to set
	 */
	public void setShipItemId(int shipItemId) {
		this.shipItemId = shipItemId;
	}

	/**
	 * @return the invoiceitemsubproductId
	 */
	public int getInvoiceitemsubproductId() {
		return invoiceitemsubproductId;
	}

	/**
	 * @param invoiceitemsubproductId
	 *            the invoiceitemsubproductId to set
	 */
	public void setInvoiceitemsubproductId(int invoiceitemsubproductId) {
		this.invoiceitemsubproductId = invoiceitemsubproductId;
	}

}
