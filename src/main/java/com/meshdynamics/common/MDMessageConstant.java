/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MDMessageConstant.java
 * Comments : MDMessageConstant.java 
 * Created  : 23-May-2017
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |23-May-2017  | Created                             | Pinki 		 |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.common;

public class MDMessageConstant {

		// Properties messages constants for PO Controller UI 
		public static final String PUCHASE_ORDER_ADD_SUCCESS            = "purchaseOrderAddSuccess";
		public static final String GET_PURCHASE_ORDERS_SUCCESS          = "getPurchaseOrdersSuccess";
		public static final String GET_PURCHASE_ORDER_SUCCESS	        = "getPurchaseOrderSuccess";
		public static final String PURCHASE_ORDER						= "purchaseOrder";
		public static final String DELETE_PURCHASE_ORDER_SUCCESS        = "deletePurchaseOrder";
		public static final String UPDATE_PURCHSE_ORDER_SUCCESS         = "updatePurchaseOrderSuccess";
		public static final String GET_PURCHASE_ORDERLIST_CRITERIA_BASED= "getPurchaseOrderListBasedOnCritera";
		
		// message constants for RawPayment UI
		public static final String RAW_PAYMENT_NAME						= "rawPaymentName";
		public static final String RAW_PAYMENT_ADD_SUCCESS 				= "rawPaymentAddSuccess";
		public static final String RAW_PAYMENTS_GET_SUCCESS 			= "getRawPayments_SUCCESS";
		public static final String RAW_PAYMENT_GET_SUCCESS 				= "getRawPaymentSuccess";
		public static final String RAW_PAYMENT_UPDATE_SUCCESS 			= "updateRawPaymentSuccess";
		public static final String RAW_PAYMENT_DELETE_SUCCESS 			= "deleteRawPaymentSuccess";
		public static final String GET_OPEN_RAWPAYMENT_LIST_SUCCESS 	= "getOpenRawPaymentListSuccess";
		
		// message constants for invoicePayment UI 
		public static final String PO_FETCHING_SATAUS					= "poFetchingFailed";
		public static final String PO_PAYMENT_SUCCESS					= "successfulMessage";
		public static final String PO_PAYMENT_NAME						= "poPaymentName";
		public static final String PO_PAYMENT_ADD_SUCCESS 				= "poPaymentAddSuccess";
		public static final String PO_PAYMNETS_GET_SUCCESS 				= "getPOPayments_SUCCESS";
		public static final String PO_PAYMENT_GET_SUCCESS 				= "getPOPaymentSuccess";
		public static final String PO_PAYMENT_UPDATE_SUCCESS 			= "updatePOPaymentSuccess";
		public static final String PO_PAYMENT_DELETE_SUCCESS 			= "deletePOPaymentSuccess";
		public static final String PO_PAYMENT_COUNT_BY_INVOICE_ID_SUCCESS = "getPOPaymentCountByIDSuccess";
		public static final String PAYMNETS_PAGINATION_GET_SUCCESS 		= "getPaymentUsingPaginationSuccess";
		public static final String REVENUE_LIST_SUCCESS 				= "getRevenueListSuccess";
		
		// messages constants for JobOrder Controller UI
		public static final String JOB_ADD_SUCCESS      				= "jobAddSuccess";
		public static final String JOB_ORDER_NAME						= "jobOrderName";
		public static final String JOB_ORDERS_GET_SUCCESS 				= "getJobOrders_Success";
		public static final String JOB_ORDER_GET_SUCCESS 				= "getJobOrderSuccess";
		public static final String JOB_ORDER_UPDATE_SUCCESS 			= "updateJobOrderSuccess";
		public static final String JOB_ORDER_DELETE_SUCCESS 			= "deleteJobOrderSuccess";
		public static final String JOB_ORDER_LIST_BASED_CRITERIA_SUCCESS= "getJobOrderBasedOnCriteria";
		public static final String GET_JOB_ORDERLIST_CRITERIA_BASED 	= "getJobOrderListBasedPaginationCriteria";
		public static final String JOB_STATE_FLASH_STATUS				= "jobStateFlashStatus";
		
		// messages constants for Customer Controller UI
		public static final String CUSTOMER_NAME						= "customerName";
		public static final String CUSTOMER_ADD_SUCCESS					= "customerAddSuccess";
		public static final String CUSTOMER_UPDATE_SUCCESS				= "updateCustomerSuccess";
		public static final String CUSTOMER_UPDATE_FAILURE				= "updateCustomerFailure";
		public static final String CUSTOMER_DELETE_SUCCESS 				= "deleteCustomerSuccess";
		public static final String CUSTOMERS_GET_SUCCESS				= "customerListGetSuccess"; 
		public static final String CUSTOMER_GET_SUCCESS					= "getCustomerSuccess";
		
		// messages constants for Inventory Controller UI				
		public static final String INVENTORY_ADD_SUCCESS				= "inventoryAddSuccess";
		public static final String INVENTORY_UPDATE_SUCCESS				= "updateInventorySuccess";
		public static final String INVENTORY_DELETE_SUCCESS				= "deleteInventorySuccess";
		public static final String GET_RAW_INVENTORIES_SUCCESS			= "getRawInventoriesSuccess";
		public static final String INVENTORY_UPDATE_FAILURE				= "updateInventoryFailure";
		public static final String GET_INVENTORY_LIST_PAGINATION 		= "getInventoryListUsingPagination";
		// exception message
		public static final String BUSINESS_EXCEPTION					= "businessException";
		
		// message constants for jobModelFlashDetails Controller UI
		public static final String JOB_MODEL_FLASH_DETAILS_ADD_SUCCESS  = "addJobModelFlashDetailsSuccess";
		public static final String JOB_MODEL_FLASH_DETAILS_UPDATE_SUCCESS="updateJobModelFlashDetailSuccess";
		public static final String GET_JOB_MODEL_FLASH_DETAILS_SUCCESS  = "getJobModelFlashDetailsSuccess";
		public static final String GET_JOB_MODEL_FLASH_DETAIL_SUCCESS   = "getJobModelFlashDetailSuccess";
		public static final String GET_JOB_FLASHDETAILS_BY_INVOICEID_MODELID="getJobModelFlashDetailsByInvoiceIdAndModelId";
		public static final String REGENERATE_BUILD						= "regenerateBuild";
		public static final String ADD_MULTIPLE_JOB_FLASH_SUCCESS		= "addJobModelFlashDetailsList";
		public static final String VERIFY_MAC_ADDRESS					= "verifyMacAddress";
		public static final String IMAGE_DOWNLOAD_PATH_SUCCESS			= "imageDownloadPathSuccess";
		// exception message
		
		
		// message constants for login Controller not an UI
		public static final String LOGIN_SUCCESS						= "Login successfully completed";
		public static final String LOGIN_FAILURE						= " Login Failure ";
		public static final String LOGOUT_FROM_MDMAN 					= " is Logged out from MDManufacturer";
		public static final String LOGOUT_SUCCESS						= " logout successfully ";
		public static final String LOGOUT_FAILURE						= " Unable to logout ";
		public static final String USERNAME_FAILURE						= " No userName exist";
		public static final String PASSWORD_FAILURE						= " Incorrect Password";
		public static final String UNABLE_LOGIN							= " unable to login";
		public static final String AUTHORIZATION_FAILURE				= " UNAUTHORISED..";
		// Login message values
		public static final String AUTHKEY								= "authkey";
		public static final String USER									= "user";
		public static final String CURRENT_PASSWORD						= "currentPassword";
		public static final String NEW_PASSWORD							= "newPassword";
		
		
		// UI based login controller msg
		public static final String CHANGE_PASSWORD						= "changePassword";
		public static final String FORGOT_PASSWORD						= "forgotPassword";
		
		// message constants for macAddress Controller UI
		
		// message constants for macVerification Controller UI
		
		// message constants for modelController
		
		// message constants for Product Controller UI
		public static final String PRODUCT_NAME							= "productName";
		public static final String PRODUCT_ADD_SUCCESS					= "productAddSuccess";
		public static final String GET_PRODUCTS_SUCCESS					= "getProducts_SUCCESS";
		public static final String GET_PRODUCT_SUCCESS					= "getProductSuccess";
		public static final String DELETE_PRODUCT_SUCCESS				= "deleteProductSuccess";
		public static final String UPDATE_PRODUCT_SUCCESS				= "updateProductSuccess";
		public static final String PRODUCT_GET_SUCCESS 					= "getProductSuccess";
		
		// message constants for Ship Controller UI
		public static final String SHIP_NAME						    = "shipName";
		public static final String SHIP_ADD_SUCCESS						= "shipAddSuccess";
		public static final String GET_SHIP_ITEM_LIST_SUCCESS			= "getShipListSuccess";
		public static final String GET_SHIP_ITEM_SUCCESS				= "getShipSuccess";
		public static final String DELETE_SHIP_ITEM_SUCCESS				= "deleteShipItemSuccess";
		public static final String UPDATE_SHIP_ITEM_SUCCESS				= "updateShipItemSuccess";
		public static final String GET_SHIP_ITEM_BY_INVOICE_ID			= "getShipItemByInvoiceIdSuccess";
		public static final String SHIP_LIST_CRITERIA_BASED 			= "getShipListBasedOnPagination";
		
		// message constants for SubProduct Controller ,Not an UI
		public static final String SUBPRODUCT_ADD_SUCCESS				= "SubProduct Added Successfully ";
		public static final String GET_SUBPRODUCTS_SUCCESS				= "SubProduct list fetched successfully";
		
		// Message constants for Terms Controller,Not ON UI
		public static final String TERMS_ADD_SUCCESS					= "Terms Added Successfully";
		public static final String GET_TERMS_SUCCESS					= "Terms List fetched successfully";
		public static final String DELETE_TERM_SUCCESS					= "Term deleted successfully";
		public static final String UPDATE_TERM_SUCCESS					= "Terms updated successfully";
		
		// message constants for tracking Controller
		public static final String TRACKING								= "tracking";
		public static final String TRACKING_ADD_FAILURE					= "trackingAddFailure";
		public static final String TRACKING_ADD_SUCCESS					= "trackingAddSuccess";
		public static final String GET_TRACKING_LIST_SUCCESS			= "getTrackingListSuccess";
		public static final String GET_TRACKING_SUCCESS					= "getTrackingSuccess";
		public static final String GET_TRACKING_COUNT_BY_INVOICE_ID_SUCCESS="getTrackingCountByInvoiceIdSuccess";
		public static final String UPDATE_TRACKING_SUCCESS				= "updateTrackingSuccess";
		public static final String DELETE_TRACKING_LIST_SUCCESS			= "deleteTrackingSuccess";
		public static final String GET_TRACKING_BY_INVOICE_ID_SUCCESS	= "getTrackingByInvoiceIdSuccess";
		
		// message constants for userRole Controller
		public static final String USER_PREFIX							= "userPrefix";
		public static final String USER_ADD_SUCCESS						= "userAddSuccess";
		public static final String GET_USERS_SUCCESS					= "getUsers_SUCCESS";
		public static final String UPDATE_USER_SUCCESS					= "updateUserSuccess";
		public static final String DELETE_USER_SUCCESS					= "deleteUserSuccess";
		public static final String GET_USER_SUCCESS						= "getUserSuccess";
		public static final String GET_USERS_PAGINATION_LIST_SUCCESS 	= "getUsersBasedOnPagination";
		public static final String CHANGE_USER_PASSWORD					= "changeUserPassword";
		
		// message constants for macAddress controller
		public static final String GET_MAC_ADDRESS						= "MacAddressSummaryList Fetched successfully";
		public static final String VIEW_MAC_ADDRESS						= "macRange Details fetched Successfully";
		public static final String AND_CLAUSE					    	= " AND ";
		public static final String WHERE_CLAUSE							= " where ";
		public static final String BETWEEN_CLAUSE						= " BETWEEN ";
		
		public static final String TRUE									= "true";
		public static final String FALSE								= "false";
		public static final String LOGIN_MESSAGE						= "Hi,";
		public static final String PASSWORD_GENERATED_MESSAGE			= "Password has been generated,Please find below !";
		public static final String SUCCESSFUL_LOGIN_MESSAGE				= "HAPPY LOGIN!";
		public static final String USER_NAME							= "userName";
		public static final String ALREADY_REGISTERED					= "alreadyRegistered";
		
		public static final String USER_NAME_DELETE_EXCEPTION			= "userNameDeleteException";
		public static final String NO_USER_NAME							= "noUserName";
		public static final String NOT_EXIST							= "noExist";
		
		public static final String KEY_CREATED							= "created";
		
		/*********************************** Exception Message Constants *******************************/
		public static final String TERMS_BUSINESS_EXCEPTION			= "termsBusinessException";
		public static final String CUSTOMER_BUSINESS_EXCEPTION		= "customerBusinessException";
		public static final String NO_INVOICE_BUSINESS_EXCEPTION 	= "noInvoiceBusinessException";
		public static final String NO_RECORDS_EXCEPTION				= "noRecordsException";
		public static final String PO_DELETE_BUSINESS_EXCEPTION		= "poDeleteBusinessException";
		public static final String PO_DELETE_JOB_BUSINESS_EXCEPTION	= "poDeleteJobBusinessException";
		public static final String JOB_CREATED_BUSINESS_EXCEPTION	= "jobCreatedBusinessException";
		public static final String COUNT_JOB_SHIP_BUSINESS_EXCEPTION= "countJobShipBusinessException";
		public static final String PO_SHIPPING_BUSINESS_EXCEPTION	= "poShippingBusinessException";
		public static final String PO_JOB_BUSINESS_EXCEPTION		= "poJobBusinessException";
		public static final String PO_PAYMENT_BUSINESS_EXCEPTION 	= "poPaymentBusinessException";
		public static final String CRITERIA_BUSINESS_EXCEPTION		= "criteriaBusinessException";
		
		public static final String CUSTOMER_EXIST_EXCEPTION			= "customerAlreadyExistException";
		public static final String NO_CUSTOMER_EXCEPTION			= "noCustomerException";
		public static final String CUSTOMER_DELETE_EXCEPTION		= "customerDeleteException";
		public static final String NO_CUSTOMER_RECORD_EXCEPTION	    = "NoCustomerRecordException";
		
		public static final String DUPLICATE_ENTRY_EXCEPTION		= "duplicateInventoryException";
		public static final String INVENTORY_DELETE_EXCEPTION		= "inventoryDeleteException";
		public static final String JOB_DELETE_EXCEPTION				= "jobDeleteException";
		public static final String NO_JOB_CREATED_EXCEPTION 		= "jobOrderNotCreatedException";
		public static final String JOB_CLOSE_EXCEPTION				= "jobCloseException";
		public static final String JOB_UPDATE_IF_BUILD_DONE_EXCEPTION="jobUpdateBuildDoneException";
		public static final String JOB_FLASHING_INITIATED_EXCEPTION	= "flashingInitiatedException";
		
		public static final String ALREADY_CREATED_EXCEPTION 		= "alreadyCreatedException";
		public static final String JOB_USED_FOR_FLASHING_EXCEPTION	= "jobUsedForFlashing";
		
		public static final String NO_RECORD_EXCEPTION				= "noRecordException";
		public static final String CONNECTION_TIMED_OUT_EXCEPTION	= "connectionTimedOut";
		
		public static final String GIVEN_MAC_EXCEPTION				= "givenMac";
		public static final String ALREADY_USE_EXCEPTION			= "alreadyUseMac";
		public static final String LIMIT_MAC_ADDRESS_EXCEPTION		= "limitMacAddress";
		
		public static final String NO_JOB_MODEL_LIST				= "noJobModelList";
		public static final String NO_JOB_FOR_IMAGE_EXCEPTION		= "noJobImage";
		
		public static final String PASSWORD_NOT_MATCH				= "passwordNotMatch";
		public static final String USER_NOT_EXIST					= "userNotExist";
		public static final String INCORRECT_PASSWORD				= "incorrectPassword";
		
		public static final String NO_USER_CONFIGURATION			= "noUserConfiguration";
		public static final String NO_EMAIL_CONFIGURATION			= "noEmailConfiguration";
		
		public static final String MAC_ADDRESS_NOT_EXIST			= "macAddressNotExist";
		
		public static final String COMPLETE_PAYMENT_EXCEPTION		= "completePaymentException";
		public static final String RAW_PAYMENT_DELETE_EXCEPTION		= "rawPaymentDeleteException";
		
		public static final String PSKU_EXIST_EXCEPTION				= "pskuExistException";
		public static final String CONFIG_FILE_EXIST_EXCEPTION		= "configFileExist";
		
		public static final String NO_PRODUCT_EXCEPTION				= "noProductException";
		public static final String DELETE_PRODUCT_EXCEPTION			= "deleteProductException";
		public static final String UPDATE_PRODUCT_EXCEPTION			= "updateProductException";
		
		public static final String SHIPPING_ALREADY_DONE_EXCEPTION  = "shippingAlreadyDoneException";
		public static final String UNKNOWN_USER						= "unknownUser";
		public static final String SHIPPING_DONE_EXCEPTION			= "shippingDoneException";
		
		public static final String NO_SHIPPING						= "noShipping";
		public static final String DELETE_SHIP_EXCEPTION			= "deleteShipException";
		
		public static final String NO_RECORD_SHIP					= "noRecordShip";
		public static final String NO_SHIP_ID						= "noShipId";
		
		public static final String NO_SUB_PRODUCTS					= "noSubProducts";
		public static final String NO_TERMS_EXIST					= "noTermsExist";
		
		public static final String NO_TRACKING_LIST					= "noTrackingList";
		public static final String NO_INVOICE_TRACKING				= "noInvoiceTracking";
		
		public static final String NO_TRACKING_LIST_AVAILABLE		= "noInvoiceTrackingList";
		public static final String TRACKING_EXIST					= "trackingExist";
		
		//////// warning Message //////////
		
		public static final String STATUS_INVALID					= "Status is invalid-";
		public static final String WEBSOCKET_CONNECTION_NOT_UP 		= "webSocketConnectionFailed";
		
		public static final String MD1_SERIES						= "md1";
		public static final String MD4_SERIES						= "md4";
		public static final String MD6_SERIES						= "md6";
		
}
