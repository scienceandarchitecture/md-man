package com.meshdynamics.common.helper;

public class MacAddress {

	/**
	 * 
	 * All the MeshDynamics MacAddresses starts with 00:12:ce:...
	 * 
	 */

	private static String MeshDynamicsMacAddress = "00:12:CE";

	static {
		mResetAddress = new MacAddress((short) 0);
		mUndefinedAddress = new MacAddress((short) 255);
		mTempAddress = new MacAddress((short) 0);
	}

	public short[] macAddress;
	public static final MacAddress mResetAddress;
	public static final MacAddress mUndefinedAddress;
	public static final MacAddress mTempAddress;

	public MacAddress() {
		macAddress = new short[6];
	}

	public MacAddress(short initialval) {
		macAddress = new short[6];
		for (int i = 0; i < 6; i++) {
			macAddress[i] = initialval;
		}
	}

	public MacAddress(byte[] bytes) {
		macAddress = new short[6];
		setBytes(bytes);
	}

	public int hashCode() {
		return macAddress[0] * 1 + macAddress[1] * 2 + macAddress[2] * 3 + macAddress[3] * 4 + macAddress[4] * 5
				+ macAddress[5] * 6;
	}

	public byte[] getBytes() {
		byte[] bt = new byte[6];
		for (int i = 0; i < 6; i++) {
			bt[i] = (byte) macAddress[i];
		}
		return bt;
	}

	public int getBytes(byte[] buffer, int position) {
		if (buffer.length < position + MacAddress.GetLength())
			return 0;
		for (int i = 0; i < macAddress.length; i++) {
			buffer[position + i] = (byte) macAddress[i];
		}
		return macAddress.length;

	}

	public int setBytes(String newAddress) {
		int i = 0, j = 0;
		if (newAddress.length() != 17) {
			return 1;
		}
		try {
			for (i = 0; i < newAddress.length(); i++) {
				String s = newAddress.substring(i, i + 2);
				Integer it = Integer.valueOf(s, 16);
				macAddress[j++] = it.shortValue();
				i += 2;
			}
		} catch (Exception e) {
			return 1;
		}
		return 0;
	}

	public int setBytes(byte[] newBytes) {
		try {
			for (int i = 0; i < 6; i++) {
				if (newBytes[i] < 0) {
					macAddress[i] = (short) (128 + (128 + newBytes[i]));
				} else {
					macAddress[i] = newBytes[i];
				}
			}
		} catch (Exception e) {
			return 1;
		}
		return 0;
	}

	public void resetAddress() {
		for (int i = 0; i < 6; i++) {
			macAddress[i] = 0;
		}
	}

	private String hexString(short value) {
		String ret;
		int quotient;
		char r;
		ret = "";
		if (value < 0) {
			value = (short) (128 + (128 + value));
		}
		while (value > 0) {
			quotient = value / 16;
			if (quotient > 9 && quotient < 16) {
				r = 'A';
				r += (quotient - 10);
				ret += r;
				// quotient = 0;
			} else {
				ret += quotient;
			}
			value = (byte) (value % 16);
			if (value > 9 && value < 16) {
				r = 'A';
				r += (value - 10);
				ret += r;
			} else {
				ret += value;
			}
			value = 0;
		}
		return ret;
	}

	public String toString() {
		String ret = "";
		for (int i = 0; i < 6; i++) {
			if (macAddress[i] == 0) {
				ret += "00"; 
			} else {
				ret += hexString(macAddress[i]);
			}
			ret += ":"; 
		}
		return ret = ret.substring(0, ret.length() - 1);
	}

	public boolean equals(Object obj) {
		boolean equal = true;
		if (obj != null) {
			MacAddress newAddress = (MacAddress) obj;
			for (int i = 0; i < 6; i++) {
				if (macAddress[i] != newAddress.macAddress[i]) {
					equal = false;
					break;
				}
			}
		}
		return equal;
	}

	public static int GetLength() {
		return 6;
	}

	public static boolean isMeshDynamicsMacaddress(String macAddress) {
		if (macAddress == null) {
			return false;
		}
		return macAddress.startsWith(MeshDynamicsMacAddress);

	}

	public String toRMSString() {
		String ret = "";
		for (int i = 0; i < 6; i++) {
			if (macAddress[i] == 0) {
				ret += "00";
			} else {
				ret += hexString(macAddress[i]);
			}
		}
		return ret;
	}

	public static String getMacAddressbyLongValue(long macAddressValue) {
		String macAddress = "";
		byte[] buffer = new byte[6];
		buffer[0] = (byte) (macAddressValue >> 40);
		buffer[1] = (byte) (macAddressValue >> 32);
		buffer[2] = (byte) (macAddressValue >> 24);
		buffer[3] = (byte) (macAddressValue >> 16);
		buffer[4] = (byte) (macAddressValue >> 8);
		buffer[5] = (byte) macAddressValue;
		macAddress = new MacAddress(buffer).toString();
		return macAddress;
	}
}
