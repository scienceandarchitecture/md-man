/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : Helper.java
 * Comments : TODO 
 * Created  : 15-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |15-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.common.helper;

import java.security.Key;
import java.security.KeyManagementException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.hibernate.HibernateException;
import org.hibernate.JDBCException;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.NonUniqueResultException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.PersistentObjectException;
import org.hibernate.QueryTimeoutException;
import org.hibernate.TransactionException;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.DataException;
import org.hibernate.exception.JDBCConnectionException;
import org.hibernate.exception.LockAcquisitionException;
import org.hibernate.id.IdentifierGenerationException;

import com.meshdynamics.common.Constant;

public class Helper {
//	private static final SimpleDateFormat dateFormat = new SimpleDateFormat(
//			"yyyy-MM-dd");
	private static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat(
			Constant.DATE_TIME_FORMAT);
	private static final String CHAR_LIST =
	        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	static Cipher cipher;
	public static synchronized Date getDateFromString(String sdate) {
		Date date = null;
		if (sdate != null && !("".equalsIgnoreCase(sdate))) {
			try {
				date = dateFormatTl.get().parse(sdate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return date;
	}

	public static synchronized String getStringDateTimeFromDate(Date date) {
		String sdate = "";
		if (date != null) {
			sdate = dateTimeFormat.format(date);
		}
		return sdate;
	}
	
	public static int getExceptionCode(HibernateException he) {
		int code = 600;
		if (he instanceof JDBCException) {
			if(he instanceof ConstraintViolationException){
				code  = Constant.CONSTRAINT_VIOLATION_EXCEPTION;
			}else if(he instanceof DataException){
				code = Constant.DATA_EXCEPTION;
			}else if(he instanceof JDBCConnectionException){
				code = Constant.JDBC_CONNECTION_EXCEPTION;
			}else if(he instanceof LockAcquisitionException){
				code = Constant.LOCK_ACQUISITION_EXCEPTION;
			}else if(he instanceof QueryTimeoutException){
				code = Constant.QUERY_TIMEOUT_EXCEPTION;
			}else{
				code = Constant.JDBC_EXCEPTION;
			}
		} else if (he instanceof PersistentObjectException) {
			code = Constant.PERSISTENT_OBJECT_EXCEPTION;
		} else if (he instanceof NonUniqueObjectException) {
			code = Constant.NON_UNIQUE_OBJECT_EXCEPTION;
		} else if (he instanceof NonUniqueResultException) {
			code = Constant.NON_UNIQUE_RESULT_EXCEPTION;
		} else if (he instanceof TransactionException) {
			code = Constant.TRANSACTION_EXCEPTION;
		} else if(he instanceof IdentifierGenerationException){
			code = Constant.IDENTIFIER_GENERATION_EXCEPTION;
		}else if(he instanceof ObjectNotFoundException){
			code = Constant.IDENTIFIER_GENERATION_EXCEPTION;
		}
		return code;
	}
	
	
	
	public static synchronized String encrypt(String plainText)
			throws Exception {
		String encryptedText = "";
		if (plainText != null && !("".equalsIgnoreCase(plainText))) {
			byte[] plainTextByte = plainText.getBytes();
			Key key = Helper.getSecretKey();
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] encryptedByte = cipher.doFinal(plainTextByte);
			Base64.Encoder encoder = Base64.getEncoder();
			encryptedText = encoder.encodeToString(encryptedByte);
		}
		return encryptedText;

	}

	public static synchronized String decrypt(String encryptedText)
			throws Exception {
		String decryptedText = "";
		if (encryptedText != null && !("".equalsIgnoreCase(encryptedText))) {
			Base64.Decoder decoder = Base64.getDecoder();
			byte[] encryptedTextByte = decoder.decode(encryptedText);
			Key key = Helper.getSecretKey();
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
			decryptedText = new String(decryptedByte);
		}
		return decryptedText;

	}

	public static Key getSecretKey() throws Exception {
		Key key = new SecretKeySpec(
				get_SHA_1_SecureUsernamforSalt(Constant.KEY), "AES");
		cipher = Cipher.getInstance("AES");
		return key;
	}

	private static byte[] get_SHA_1_SecureUsernamforSalt(String key) {

		byte[] bytes = null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			bytes = md.digest(key.getBytes());
			bytes = Arrays.copyOf(bytes, 16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return bytes;
	}
	
	public static String generateRandomString(){
    	int RANDOM_STRING_LENGTH = 30;     
        StringBuffer randStr = new StringBuffer();
        for(int i=0; i<RANDOM_STRING_LENGTH; i++){
            int number = getRandomNumber();
            char ch = CHAR_LIST.charAt(number);
            randStr.append(ch);
        }
       return randStr.toString();
    }
	private static int getRandomNumber() {
        int randomInt = 0;
        Random randomGenerator = new Random();
        randomInt = randomGenerator.nextInt(CHAR_LIST.length());
        if (randomInt - 1 == -1) {
            return randomInt;
        } else {
            return randomInt - 1;
        }
    }

	/**
	 * @param sdate
	 * @return
	 */
	public static  Date getDateTimeFromString(String sdate) {
		Date date = null;
		if (sdate != null && !("".equalsIgnoreCase(sdate))) {
			try {
				date = dateTimeFormatTl.get().parse(sdate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return date;
	}

	public static boolean getTimeDiffFlagInMinutes(Date lastUpdatedTime, Date currentDateTime, long interval) {
		long lstUpdtdTime = lastUpdatedTime.getTime();
		long currTime = currentDateTime.getTime();
		
		return (currTime - lstUpdtdTime) > (interval*60*1000) ? true : false;
		
	}
	 
	private static final ThreadLocal<DateFormat> dateTimeFormatTl
	               = new ThreadLocal<DateFormat>(){
		@Override
		protected DateFormat initialValue() {
			return new SimpleDateFormat(Constant.DATE_TIME_FORMAT);
	    }
	};
	
	private static final ThreadLocal<DateFormat> dateFormatTl = new ThreadLocal<DateFormat>() {
		@Override
		protected DateFormat initialValue() {
			return new SimpleDateFormat(Constant.DATE_FORMAT);
		}
	};
	
	
	public static final SSLContext getSSLContext() throws NoSuchAlgorithmException, KeyManagementException   {
		  TrustManager []trustManager= new TrustManager[] {
				  new X509TrustManager() {
				        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				            return null;
				        }
				        public void checkClientTrusted(
				            java.security.cert.X509Certificate[] certs, String authType) {
				        }
				        public void checkServerTrusted(
				            java.security.cert.X509Certificate[] certs, String authType) {
				        }
				    }   
		  };
	      SSLContext sslContext = SSLContext.getInstance("SSL");    
	      sslContext.init(null, trustManager, null);
	      return sslContext;
	
	}
	
	

}
