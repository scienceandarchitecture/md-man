/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MDProperties.java
 * Comments : MDProperties.java 
 * Created  : 27-Jul-2016
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |27-Jul-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.common.helper;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 */
public class MDProperties {
	private static InputStream in1;
	private static InputStream in2;

	

/*
 *  MDProperties.read
 *  @param attribute
 *  @return String
 *  comments : TODO
 */
public static String read(String attribute) {
		String result = "";
		in1 = null;
		in2 = null;
		try {
			Properties properties = new Properties();
			in1 = MDProperties.class.getResourceAsStream("/md.properties");
			properties.load(in1);
			in2 = MDProperties.class.getResourceAsStream("/mdMessage.properties");
			properties.load(in2);
			result = properties.getProperty(attribute);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			
				try {
					if (in1 != null) {
						in1.close();
					}if(null != in2){
						in2.close();
					}
					
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return result;
	}
}
