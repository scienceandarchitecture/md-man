package com.meshdynamics.common.helper;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.meshdynamics.common.WSClient;

public class BSWebSocketConnection extends TimerTask {

	
	static final Logger LOGGER = Logger.getLogger(BSWebSocketConnection.class);
	private static final long UPDATE_PERIOD = 90000L;
	private static Timer timer = null;
	
	public BSWebSocketConnection() {
		// TODO Auto-generated constructor stub
		if (null != timer) {
			timer.cancel();
			timer = null;
		}
		timer = new Timer();
		timer.schedule(this, 0, UPDATE_PERIOD);
		
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		LOGGER.trace(" Start timer run ");
		new WSClient().Connect();
		LOGGER.trace(" End timer run ");
		
	}

}
