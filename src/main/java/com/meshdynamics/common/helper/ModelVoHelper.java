package com.meshdynamics.common.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.meshdynamics.common.Constant;
import com.meshdynamics.model.Customer;
import com.meshdynamics.model.ForecastItem;
import com.meshdynamics.model.ForecastMaster;
import com.meshdynamics.model.Inventory;
import com.meshdynamics.model.InvoiceDeviceList;
import com.meshdynamics.model.InvoiceItem;
import com.meshdynamics.model.InvoiceItemSubProduct;
import com.meshdynamics.model.InvoiceMaster;
import com.meshdynamics.model.InvoicePayment;
import com.meshdynamics.model.InvoiceTracking;
import com.meshdynamics.model.JobItem;
import com.meshdynamics.model.JobItemSubProduct;
import com.meshdynamics.model.JobMaster;
import com.meshdynamics.model.JobModelFlashDetails;
import com.meshdynamics.model.Login;
import com.meshdynamics.model.Model;
import com.meshdynamics.model.Product;
import com.meshdynamics.model.ProductItem;
import com.meshdynamics.model.RawInventory;
import com.meshdynamics.model.RawPayment;
import com.meshdynamics.model.ShipItem;
import com.meshdynamics.model.ShipItemSubProduct;
import com.meshdynamics.model.ShipMaster;
import com.meshdynamics.model.StockItems;
import com.meshdynamics.model.SubProduct;
import com.meshdynamics.model.Terms;
import com.meshdynamics.model.UserRole;
import com.meshdynamics.vo.CustomerVo;
import com.meshdynamics.vo.ForecastItemVo;
import com.meshdynamics.vo.ForecastMasterVo;
import com.meshdynamics.vo.InventoryVo;
import com.meshdynamics.vo.InvoiceDeviceListVo;
import com.meshdynamics.vo.InvoiceItemSubProductVo;
import com.meshdynamics.vo.InvoiceItemVo;
import com.meshdynamics.vo.InvoiceMasterVo;
import com.meshdynamics.vo.InvoicePaymentVo;
import com.meshdynamics.vo.InvoiceTrackingVo;
import com.meshdynamics.vo.JobItemSubProductVo;
import com.meshdynamics.vo.JobItemVo;
import com.meshdynamics.vo.JobMasterVo;
import com.meshdynamics.vo.JobModelFlashDetailsVo;
import com.meshdynamics.vo.LoginVo;
import com.meshdynamics.vo.ModelVo;
import com.meshdynamics.vo.ProductItemVo;
import com.meshdynamics.vo.ProductVo;
import com.meshdynamics.vo.RawInventoryVo;
import com.meshdynamics.vo.RawPaymentVo;
import com.meshdynamics.vo.ShipItemSubProductVo;
import com.meshdynamics.vo.ShipItemVo;
import com.meshdynamics.vo.ShipMasterVo;
import com.meshdynamics.vo.StockItemsVo;
import com.meshdynamics.vo.SubProductVo;
import com.meshdynamics.vo.TermsVo;
import com.meshdynamics.vo.UserRoleVo;

public class ModelVoHelper {

	public static Customer customerVoToModel(CustomerVo customerVo) {
		Customer customer = new Customer();
		customer.setAddress(customerVo.getAddress());
		customer.setCustId(customerVo.getCustId());
		customer.setInstructions(customerVo.getInstructions());
		customer.setName(customerVo.getName());
		customer.setShipAddress(customerVo.getShipAddress());

		return customer;
	}

	public static CustomerVo customerModelToVo(Customer customer) {
		CustomerVo customerVo = new CustomerVo();
		customerVo.setAddress(customer.getAddress());
		customerVo.setCustId(customer.getCustId());
		customerVo.setInstructions(customer.getInstructions());
		customerVo.setName(customer.getName());
		customerVo.setShipAddress(customer.getShipAddress());

		return customerVo;
	}

	public static InvoiceMaster invoiceMasterVoToModel(
			InvoiceMasterVo invoiceMasterVo) {

		InvoiceMaster invoiceMaster = new InvoiceMaster();
		InvoiceItem invoiceItem = new InvoiceItem();
		Customer customer = new Customer();
		Set<InvoiceItem> invoiceItems = new LinkedHashSet<>();

		invoiceMaster.setCustName(invoiceMasterVo.getCustName());
		invoiceMaster.setBalance(invoiceMasterVo.getBalance());
		invoiceMaster.setInvId(invoiceMasterVo.getInvId());
		invoiceMaster.setAmount(invoiceMasterVo.getAmount());
		invoiceMaster.setBillTo(invoiceMasterVo.getBillTo());
		invoiceMaster.setClosedBy(invoiceMasterVo.getClosedBy());
		invoiceMaster.setComments(invoiceMasterVo.getComments());
		invoiceMaster.setCreatedBy(invoiceMasterVo.getCreatedBy());
		invoiceMaster.setDiscount(invoiceMasterVo.getDiscount());
		invoiceMaster.setDueDate(Helper.getDateFromString(invoiceMasterVo
				.getDueDate()));
		invoiceMaster.setInvDate(Helper.getDateFromString(invoiceMasterVo
				.getInvDate()));
		invoiceMaster.setPoNumber(invoiceMasterVo.getPoNumber());
		invoiceMaster.setSalesRep(invoiceMasterVo.getSalesRep());
		invoiceMaster.setShipping(invoiceMasterVo.getShipping());
		invoiceMaster.setShipTo(invoiceMasterVo.getShipTo());

		if (Constant.UI_INVOICE_OPEN.equalsIgnoreCase(invoiceMasterVo
				.getStatus()))
			invoiceMaster.setStatus(Constant.DB_INVOICE_OPEN);
		else if (Constant.UI_INVOICE_CLOSE.equalsIgnoreCase(invoiceMasterVo
				.getStatus()))
			invoiceMaster.setStatus(Constant.DB_INVOICE_CLOSED);

		invoiceMaster.setTerms(invoiceMasterVo.getTerms());
		invoiceMaster.setTermId(invoiceMasterVo.getTermId());

		for (InvoiceItemVo invoiceItemVo : invoiceMasterVo.getInvoiceitems()) {
			invoiceItem = invoiceItemVoToModel(invoiceItemVo);
			invoiceItem.setInvoiceMaster(invoiceMaster);
			invoiceItems.add(invoiceItem);
		}
		invoiceMaster.setInvoiceItems(invoiceItems);
		customer.setCustId(invoiceMasterVo.getCustId());
		invoiceMaster.setCustomer(customer);
		return invoiceMaster;
	}

	public static InvoiceMasterVo invoiceMasterModelToVo(
			InvoiceMaster invoiceMaster) {

		InvoiceMasterVo invoiceMasterVo = new InvoiceMasterVo();
		Set<InvoiceItemVo> invoiceItemsVo = new LinkedHashSet<>();

		invoiceMasterVo.setInvoicePaymentCount(invoiceMaster
				.getInvoicePaymentCount());
		invoiceMasterVo.setInvoiceTrackingCount(invoiceMaster
				.getInvoiceTrackingCount());

		invoiceMasterVo.setAmount(invoiceMaster.getAmount());
		invoiceMasterVo.setBalance(invoiceMaster.getBalance());
		invoiceMasterVo.setBillTo(invoiceMaster.getBillTo());
		invoiceMasterVo.setClosedBy(invoiceMaster.getClosedBy());
		invoiceMasterVo.setComments(invoiceMaster.getComments());
		invoiceMasterVo.setCreatedBy(invoiceMaster.getCreatedBy());
		invoiceMasterVo.setCustId(invoiceMaster.getCustomer().getCustId());
		invoiceMasterVo.setCustName(invoiceMaster.getCustName());
		invoiceMasterVo.setDiscount(invoiceMaster.getDiscount());
		invoiceMasterVo.setDueDate(Helper
				.getStringDateTimeFromDate(invoiceMaster.getDueDate()));
		invoiceMasterVo.setInvDate(Helper
				.getStringDateTimeFromDate(invoiceMaster.getInvDate()));
		invoiceMasterVo.setInvId(invoiceMaster.getInvId());
		invoiceMasterVo.setPoNumber(invoiceMaster.getPoNumber());
		invoiceMasterVo.setSalesRep(invoiceMaster.getSalesRep());
		invoiceMasterVo.setShipping(invoiceMaster.getShipping());
		invoiceMasterVo.setShipTo(invoiceMaster.getShipTo());

		if (Constant.DB_INVOICE_OPEN == invoiceMaster.getStatus())
			invoiceMasterVo.setStatus(Constant.UI_INVOICE_OPEN);
		else if (Constant.DB_INVOICE_CLOSED == invoiceMaster.getStatus())
			invoiceMasterVo.setStatus(Constant.UI_INVOICE_CLOSE);

		invoiceMasterVo.setTerms(invoiceMaster.getTerms());
		Set<InvoiceItem> invoiceItems = invoiceMaster.getInvoiceItems();

		List<InvoiceItem> invList=new ArrayList<InvoiceItem>(invoiceItems.size());
		invList.addAll(invoiceItems);
		Collections.sort(invList,new InvoiceItem());
		
		for (InvoiceItem invoiceItem : invList) {
			invoiceItemsVo.add(ModelVoHelper.invoiceItemModelToVo(invoiceItem));

		}
		
		invoiceMasterVo.setInvoiceitems(invoiceItemsVo);
		return invoiceMasterVo;
	}

	/***
	 * 
	 * @param invoiceDeviceListVo
	 * @return
	 */

	public static InvoiceDeviceList invoiceDeviceListVoToModel(
			InvoiceDeviceListVo invoiceDeviceListVo) {
		InvoiceDeviceList invoiceDeviceList = new InvoiceDeviceList();
		invoiceDeviceList.setInvoiceNumber(invoiceDeviceListVo
				.getInvoiceNumber());
		invoiceDeviceList.setMacAddress(invoiceDeviceListVo.getMacAddress());
		invoiceDeviceList.setShipId(invoiceDeviceListVo.getShipId());
		return invoiceDeviceList;
	}

	public static InvoiceDeviceListVo invoiceDeviceListModelToVo(
			InvoiceDeviceList invoiceDeviceList) {
		InvoiceDeviceListVo invoiceDeviceListVo = new InvoiceDeviceListVo();
		invoiceDeviceList
				.setInvoiceNumber(invoiceDeviceList.getInvoiceNumber());
		invoiceDeviceList.setMacAddress(invoiceDeviceList.getMacAddress());
		invoiceDeviceList.setShipId(invoiceDeviceList.getShipId());
		return invoiceDeviceListVo;
	}

	/*****
	 * 
	 * @param invoiceItemVo
	 * @return
	 */
	public static InvoiceItem invoiceItemVoToModel(InvoiceItemVo invoiceItemVo) {
		InvoiceItem invoiceItem = new InvoiceItem();

		Set<InvoiceItemSubProduct> invoiceItemSubProducts = new HashSet<>();

		InvoiceItemSubProduct invoiceItemSubProduct = new InvoiceItemSubProduct();
		invoiceItem.setPsku(invoiceItemVo.getPsku());
		invoiceItem.setQuantity(invoiceItemVo.getQuantity());
		invoiceItem.setPrice(invoiceItemVo.getPrice());
		invoiceItem.setExtraDesc(invoiceItemVo.getExtraDesc());
		invoiceItem.setInvoiceItemId(invoiceItemVo.getInvoiceItemId());

		for (InvoiceItemSubProductVo invoiceItemSubProductVo : invoiceItemVo
				.getInvoiceItemSubProducts()) {
			invoiceItemSubProduct = invoiceItemSubProductVoToModel(invoiceItemSubProductVo);
			invoiceItemSubProduct.setInvoiceItem(invoiceItem);
			invoiceItemSubProducts.add(invoiceItemSubProduct);
		}

		invoiceItem.setInvoiceItemSubProducts(invoiceItemSubProducts);
		return invoiceItem;
	}

	public static InvoiceItemVo invoiceItemModelToVo(InvoiceItem invoiceItem) {
		InvoiceItemVo invoiceItemVo = new InvoiceItemVo();
		Set<InvoiceItemSubProductVo> invoiceItemSubProductVo = new LinkedHashSet<>();

		invoiceItemVo.setPsku(invoiceItem.getPsku());
		invoiceItemVo.setInvid(invoiceItem.getInvoiceMaster().getInvId());
		invoiceItemVo.setPrice(invoiceItem.getPrice());
		invoiceItemVo.setQuantity(invoiceItem.getQuantity());
		invoiceItemVo.setExtraDesc(invoiceItem.getExtraDesc());
		invoiceItemVo.setInvoiceItemId(invoiceItem.getInvoiceItemId());

		Set<InvoiceItemSubProduct> invoiceItemSubProducts = invoiceItem
				.getInvoiceItemSubProducts();
		
		List<InvoiceItemSubProduct> invoiceItemList=new ArrayList<InvoiceItemSubProduct>(invoiceItemSubProducts.size());
		invoiceItemList.addAll(invoiceItemSubProducts);
		Collections.sort(invoiceItemList,new InvoiceItemSubProduct());
		
		for (InvoiceItemSubProduct invoiceItemSubProduct : invoiceItemList) {
			invoiceItemSubProductVo.add(ModelVoHelper
					.invoiceItemSubProductToVo(invoiceItemSubProduct));
		}
		invoiceItemVo.setInvoiceItemSubProducts(invoiceItemSubProductVo);
		return invoiceItemVo;
	}

	public static Set<InvoiceItem> invoiceMasterItemsVotoInvoiceItems(
			InvoiceMasterVo invoiceMasterVo) {
		Set<InvoiceItem> invoiceItemsList = new HashSet<>();
		Set<InvoiceItemVo> itemsVo = invoiceMasterVo.getInvoiceitems();
		InvoiceItem invoiceItem = null;
		for (InvoiceItemVo invoiceItemVo : itemsVo) {
			invoiceItem = new InvoiceItem();
			invoiceItem.setPsku(invoiceItemVo.getPsku());
			invoiceItem.setPrice(invoiceItemVo.getPrice());
			invoiceItem.setQuantity(invoiceItemVo.getQuantity());
			invoiceItem.setExtraDesc(invoiceItemVo.getExtraDesc());
			invoiceItemsList.add(invoiceItem);
		}
		return invoiceItemsList;
	}

	public static ForecastItem forecastItemVoToModel(
			ForecastItemVo forecastItemVo) {

		ForecastItem forecastItem = new ForecastItem();
		Product product = new Product();

		forecastItem.setQuantity(forecastItemVo.getQuantity());

		product.setPsku(forecastItemVo.getPsku());
		forecastItem.setProduct(product);

		return forecastItem;
	}

	public static ForecastItemVo forecastItemModelToVo(ForecastItem forecastItem) {
		ForecastItemVo forecastItemVo = new ForecastItemVo();

		forecastItemVo
				.setFcastId(forecastItem.getForecastMaster().getFcastId());
		forecastItemVo.setQuantity(forecastItem.getQuantity());
		forecastItemVo.setForecastItemId(forecastItem.getForecastItemId());
		forecastItemVo.setPsku(forecastItem.getProduct().getPsku());
		return forecastItemVo;
	}

	public static ForecastMaster forecastMasterVoToModel(
			ForecastMasterVo forecastMasterVo) {

		ForecastMaster forecastMaster = new ForecastMaster();
		ForecastItem forecastItem = new ForecastItem();
		Set<ForecastItem> forecastItems = new HashSet<>();
		forecastMaster.setFcastId(forecastMasterVo.getFcastId());
		forecastMaster.setCustName(forecastMasterVo.getCustName());
		forecastMaster.setEstDate(Helper.getDateFromString(forecastMasterVo
				.getEstDate()));
		forecastMaster.setNotes(forecastMasterVo.getNotes());
		forecastMaster.setSalesMan(forecastMasterVo.getSalesMan());
		for (ForecastItemVo forecastItemVo : forecastMasterVo
				.getForecastItemVo()) {
			forecastItem = forecastItemVoToModel(forecastItemVo);
			forecastItem.setForecastMaster(forecastMaster);
			forecastItems.add(forecastItem);
		}
		forecastMaster.setForecastItems(forecastItems);
		return forecastMaster;
	}

	public static ForecastMasterVo forecastMasterModeltovo(
			ForecastMaster forecastMaster) {
		ForecastMasterVo forecastMasterVo = new ForecastMasterVo();
		Set<ForecastItemVo> forecastItemVos = new HashSet<>();

		forecastMasterVo.setFcastId(forecastMaster.getFcastId());
		forecastMasterVo.setCustName(forecastMaster.getCustName());
		forecastMasterVo.setEstDate(Helper
				.getStringDateTimeFromDate(forecastMaster.getEstDate()));
		forecastMasterVo.setNotes(forecastMaster.getNotes());
		forecastMasterVo.setSalesMan(forecastMaster.getSalesMan());
		for (ForecastItem forecastItem : forecastMaster.getForecastItems()) {
			forecastItemVos.add(forecastItemModelToVo(forecastItem));
		}
		forecastMasterVo.setForecastItemVo(forecastItemVos);
		return forecastMasterVo;
	}

	public static InventoryVo inventoryModeltoVo(Inventory inventory) {
		InventoryVo inventoryVo = new InventoryVo();
		inventoryVo.setModelId(inventory.getModelId());
		inventoryVo.setBatchNumber(inventory.getBatchNumber());
		inventoryVo.setFirmwareVersion(inventory.getFirmwareVersion());
		inventoryVo.setMacAddress(inventory.getMacAddress());
		inventoryVo.setSerial(inventory.getSerial());
		return inventoryVo;
	}

	public static InvoicePayment invoicePaymentVoToModel(
			InvoicePaymentVo invoicePaymentVo) {
		InvoicePayment invoicePayment = new InvoicePayment();
		RawPayment rawPayment = new RawPayment();
		InvoiceMaster invoiceMaster = new InvoiceMaster();

		invoicePayment.setPayId(invoicePaymentVo.getPayId());
		invoicePayment.setAmount(invoicePaymentVo.getAmount());

		invoiceMaster.setInvId(invoicePaymentVo.getInvId());
		invoicePayment.setInvoiceMaster(invoiceMaster);

		invoicePayment.setNotes(invoicePaymentVo.getNotes());
		invoicePayment.setPayDate(Helper.getDateFromString(invoicePaymentVo
				.getPayDate()));
		rawPayment.setRpId(invoicePaymentVo.getRpId());
		invoicePayment.setRawPayment(rawPayment);

		return invoicePayment;
	}

	public static InvoicePaymentVo invoicePaymentModelToVo(
			InvoicePayment invoicePayment) {

		InvoicePaymentVo invoicePaymentVo = new InvoicePaymentVo();
		invoicePaymentVo.setPayId(invoicePayment.getPayId());
		invoicePaymentVo.setAmount(invoicePayment.getAmount());
		invoicePaymentVo.setInvId(invoicePayment.getInvoiceMaster().getInvId());
		invoicePaymentVo.setNotes(invoicePayment.getNotes());
		invoicePaymentVo.setPayDate(Helper
				.getStringDateTimeFromDate(invoicePayment.getPayDate()));
		invoicePaymentVo.setRpId(invoicePayment.getRawPayment().getRpId());
		return invoicePaymentVo;
	}

	
	public static JobItem jobItemVoToModel(JobItemVo jobItemVo) {
		JobItem jobItem = new JobItem();
		InvoiceItem invoiceItem = new InvoiceItem();
		Set<JobItemSubProduct> jobItemSubProducts = new HashSet<>();

		JobItemSubProduct jobItemSubProduct = new JobItemSubProduct();

		invoiceItem.setInvoiceItemId(jobItemVo.getInvoiceItemId()); // changed
		jobItem.setInvoiceItem(invoiceItem);
		jobItem.setQuantity(jobItemVo.getQuantity());
		jobItem.setJobItemId(jobItemVo.getJobItemId());
		jobItem.setPsku(jobItemVo.getPsku());

		for (JobItemSubProductVo jobItemSubProductVo : jobItemVo
				.getJobItemSubProducts()) {
			jobItemSubProduct = jobItemSubProductVoToModel(jobItemSubProductVo);
			jobItemSubProduct.setJobItem(jobItem);
			jobItemSubProducts.add(jobItemSubProduct);
		}
		jobItem.setJobItemSubProducts(jobItemSubProducts);
		return jobItem;
	}
	
	

	public static JobItemVo jobItemModelToVo(JobItem jobItem) {
		JobItemVo jobItemVo = new JobItemVo();
		Set<JobItemSubProductVo> jobItemSubProductVo = new LinkedHashSet<>();

		jobItemVo.setJobId(jobItem.getJobMaster().getJobId());
		jobItemVo.setInvoiceItemId(jobItem.getInvoiceItem().getInvoiceItemId()); 
		jobItemVo.setQuantity(jobItem.getQuantity());
		jobItemVo.setJobItemId(jobItem.getJobItemId());
		jobItemVo.setPsku(jobItem.getPsku());

		Set<JobItemSubProduct> jobItemSubProducts = jobItem
				.getJobItemSubProducts();

		List<JobItemSubProduct> jobItemList = new ArrayList<JobItemSubProduct>(
				jobItemSubProducts.size());
		jobItemList.addAll(jobItemSubProducts);
		Collections.sort(jobItemList, new JobItemSubProduct());
		
		for (JobItemSubProduct jobItemSubProduct : jobItemList){
			jobItemSubProductVo.add(ModelVoHelper
					.jobItemSubProductModelToVo(jobItemSubProduct));
		}
		jobItemVo.setJobItemSubProducts(jobItemSubProductVo);

		return jobItemVo;
	}

	public static JobItemSubProduct jobItemSubProductVoToModel(
			JobItemSubProductVo jobItemSubProductVo) {
		JobItemSubProduct jobItemSubProduct = new JobItemSubProduct();

		InvoiceItemSubProduct invoiceItemSubProduct = new InvoiceItemSubProduct();
		invoiceItemSubProduct.setInvoiceItemSubProductId(jobItemSubProductVo
				.getInvoiceitemsubproductId());
		jobItemSubProduct.setInvoiceitemsubproductId(jobItemSubProductVo
				.getInvoiceitemsubproductId());
		jobItemSubProduct.setJobItemSubProductId(jobItemSubProductVo
				.getJobitemsubproductId());

		return jobItemSubProduct;
	}

	public static JobItemSubProductVo jobItemSubProductModelToVo(
			JobItemSubProduct jobItemSubProduct) {
		JobItemSubProductVo jobItemSubProductVo = new JobItemSubProductVo();
		List<InvoiceItemSubProductVo> invoiceItemSubProducts = new ArrayList<>();

		jobItemSubProductVo.setInvoiceitemsubproductId(jobItemSubProduct
				.getInvoiceitemsubproductId());
		jobItemSubProductVo.setJobItemId(jobItemSubProduct.getJobItem()
				.getJobItemId());
		jobItemSubProductVo.setJobitemsubproductId(jobItemSubProduct
				.getJobItemSubProductId());
		
		for (InvoiceItemSubProduct invoiceItemSubProduct : jobItemSubProduct
				.getInvoiceItemSubProductList()) {
			invoiceItemSubProducts
					.add(invoiceItemSubProductToVo(invoiceItemSubProduct));
		}
		jobItemSubProductVo
				.setInvoiceItemSubProductList(invoiceItemSubProducts);
		return jobItemSubProductVo;
	}

	public static JobMaster jobMasterVoToModel(JobMasterVo jobMasterVo) {

		JobMaster jobMaster = new JobMaster();
		Set<JobItem> jobItems = new HashSet<>();
		JobItem jobItem = new JobItem();

		jobMaster.setJobId(jobMasterVo.getJobId());
		jobMaster.setState(jobMasterVo.getState());
		jobMaster
				.setCreated(Helper.getDateFromString(jobMasterVo.getCreated()));
		jobMaster.setAcknowledged(Helper.getDateFromString(jobMasterVo
				.getAcknowledged()));
		jobMaster.setFinished(Helper.getDateFromString(jobMasterVo
				.getFinished()));
		jobMaster.setNeedBy(Helper.getDateFromString(jobMasterVo.getNeedBy()));
		jobMaster.setAckFinishBy(Helper.getDateFromString(jobMasterVo
				.getAckFinishBy()));
		jobMaster.setCreatedBy(jobMasterVo.getCreatedBy());
		jobMaster.setAckBy(jobMasterVo.getAckBy());
		jobMaster.setInvId(jobMasterVo.getInvId());
		jobMaster.setGeneratedImage(jobMasterVo.getGeneratedImage());
		jobMaster.setImageToBeGenerated(jobMasterVo.getImageToBeGenerated());
		for (JobItemVo jobItemVo : jobMasterVo.getJobItems()) {
			jobItem = jobItemVoToModel(jobItemVo); // new added
			jobItem.setJobMaster(jobMaster);
			jobItems.add(jobItem);
		}
		jobMaster.setJobItems(jobItems);
		return jobMaster;
	}

	public static JobMasterVo jobMasterModelToVo(JobMaster jobMaster) {

		JobMasterVo jobMasterVo = new JobMasterVo();
		Set<JobItemVo> jobItemVos = new LinkedHashSet<>();

		jobMasterVo.setCustName(jobMaster.getCustName());
		jobMasterVo.setJobId(jobMaster.getJobId());
		jobMasterVo.setState(jobMaster.getState());
		jobMasterVo.setCreated(Helper.getStringDateTimeFromDate(jobMaster
				.getCreated()));
		jobMasterVo.setAcknowledged(Helper.getStringDateTimeFromDate(jobMaster
				.getAcknowledged()));
		jobMasterVo.setFinished(Helper.getStringDateTimeFromDate(jobMaster
				.getFinished()));
		jobMasterVo.setNeedBy(Helper.getStringDateTimeFromDate(jobMaster
				.getNeedBy()));
		jobMasterVo.setAckFinishBy(Helper.getStringDateTimeFromDate(jobMaster
				.getAckFinishBy()));
		jobMasterVo.setCreatedBy(jobMaster.getCreatedBy());
		jobMasterVo.setAckBy(jobMaster.getAckBy());
		jobMasterVo.setInvId(jobMaster.getInvId());
		jobMasterVo.setGeneratedImage(jobMaster.getGeneratedImage());
		jobMasterVo.setImageToBeGenerated(jobMaster.getImageToBeGenerated());
		
		Set<JobItem> jobItems = jobMaster.getJobItems();

		List<JobItem> jobList=new ArrayList<JobItem>(jobItems.size());
		jobList.addAll(jobItems);
		Collections.sort(jobList,new JobItem());
		for(JobItem jobItem : jobList){
			jobItemVos.add(ModelVoHelper.jobItemModelToVo(jobItem));
		}
		jobMasterVo.setJobItems(jobItemVos);
		return jobMasterVo;
	}

	
	public static Login loginVoToModel(LoginVo loginVo) {
		Login login = new Login();
		login.setUserId(loginVo.getUserId());
		login.setUserName(loginVo.getUserName());
		login.setPassword(loginVo.getPassword());
		login.setEmployeeId(loginVo.getEmployeeId());
		login.setExtNo(loginVo.getExtNo());
		login.setAccessLevel(loginVo.getAccessLevel());
		return login;
	}

	public static LoginVo loginModelToVo(Login login) {
		LoginVo loginVo = new LoginVo();
		loginVo.setUserId(login.getUserId());
		loginVo.setUserName(login.getUserName());
		loginVo.setEmployeeId(login.getEmployeeId());
		loginVo.setExtNo(login.getExtNo());
		loginVo.setPassword(login.getPassword());
		loginVo.setAccessLevel(login.getAccessLevel());
		return loginVo;
	}

	public static Model modelVoToModel(ModelVo modelVo) {
		Model model = new Model();
		model.setModelId(modelVo.getModelId());
		model.setDescription(modelVo.getDescription());
		model.setIntlPrice(modelVo.getIntlPrice());
		model.setDomesticPrice(modelVo.getDomesticPrice());
		model.setManfDescription(modelVo.getManfDescription());
		return model;
	}

	public static ModelVo modelModelToVo(Model model) {
		ModelVo modelVo = new ModelVo();
		modelVo.setModelId(model.getModelId());
		modelVo.setDescription(model.getDescription());
		modelVo.setIntlPrice(model.getIntlPrice());
		modelVo.setDomesticPrice(model.getDomesticPrice());
		modelVo.setManfDescription(model.getManfDescription());
		return modelVo;
	}

	public static RawPayment rawPaymentVoToModel(RawPaymentVo rawPaymentVo) {

		RawPayment rawPayment = new RawPayment();

		rawPayment.setRpId(rawPaymentVo.getRpId());
		rawPayment.setState(rawPaymentVo.getState());
		rawPayment.setAmount(rawPaymentVo.getAmount());
		rawPayment
				.setClosed(Helper.getDateFromString(rawPaymentVo.getClosed()));

		rawPayment.setClosedBy(rawPaymentVo.getClosedBy());
		rawPayment.setCreated(Helper.getDateFromString(rawPaymentVo
				.getCreated()));

		rawPayment.setCreatedBy(rawPaymentVo.getCreatedBy());

		rawPayment.setInvId(rawPaymentVo.getInvId());

		rawPayment.setNotes(rawPaymentVo.getNotes());
		Customer customer = new Customer();
		customer.setCustId(rawPaymentVo.getCustId());
		
		rawPayment.setCustomer(customer);
		
		return rawPayment;
	}

	public static RawPaymentVo rawPaymentModelToVo(RawPayment rawPayment) {
		RawPaymentVo rawPaymentVo = new RawPaymentVo();
		rawPaymentVo.setRpId(rawPayment.getRpId());
		rawPaymentVo.setAmount(rawPayment.getAmount());
		rawPaymentVo.setInvId(rawPayment.getInvId());
		rawPaymentVo.setClosed(Helper.getStringDateTimeFromDate(rawPayment
				.getClosed()));

		rawPaymentVo.setClosedBy(rawPayment.getClosedBy());
		rawPaymentVo.setCreated(Helper.getStringDateTimeFromDate(rawPayment
				.getCreated()));

		rawPaymentVo.setCreatedBy(rawPayment.getCreatedBy());

		rawPaymentVo.setNotes(rawPayment.getNotes());
		rawPaymentVo.setState(rawPayment.getState());
		
		Customer customer = rawPayment.getCustomer();
		if(null != customer){
	         rawPaymentVo.setCustName(customer.getName());
		     rawPaymentVo.setCustId(customer.getCustId());
		}
		
	
		return rawPaymentVo;
	}

	public static RawInventory rawInventoryVoToModel(
			RawInventoryVo rawInventoryVo) {
		RawInventory rawInventory = new RawInventory();
		rawInventory.setRsku(rawInventoryVo.getRsku());
		rawInventory.setComment(rawInventoryVo.getComment());
		rawInventory.setDescription(rawInventoryVo.getDescription());
		return rawInventory;
	}

	public static RawInventoryVo rawInventoryModelToVo(RawInventory rawInventory) {
		RawInventoryVo rawInventoryVo = new RawInventoryVo();
		rawInventoryVo.setRsku(rawInventory.getRsku());
		rawInventoryVo.setComment(rawInventory.getComment());
		rawInventoryVo.setDescription(rawInventory.getDescription());
		return rawInventoryVo;
	}

	public static ShipItem shipItemVoToModel(ShipItemVo shipItemVo) {

		ShipItem shipItem = new ShipItem();
		Set<ShipItemSubProduct> shipItemSubProducts = new HashSet<>();
		ShipItemSubProduct shipItemSubProduct = new ShipItemSubProduct();

		Product product = new Product();
		product.setPsku(shipItemVo.getPsku());
		shipItem.setProduct(product);

		shipItem.setQuantity(shipItemVo.getQuantity());
		shipItem.setShipItemId(shipItemVo.getShipItemId());

		for (ShipItemSubProductVo shipItemSubProductVo : shipItemVo
				.getShipItemSubProducts()) {
			shipItemSubProduct = shipItemSubProductVoToModel(shipItemSubProductVo);
			shipItemSubProduct.setShipItem(shipItem);
			shipItemSubProducts.add(shipItemSubProduct);
		}
		shipItem.setShipItemSubProducts(shipItemSubProducts);
		return shipItem;
	}

	public static ShipItemVo shipItemModelToVo(ShipItem shipItem) {

		ShipItemVo shipItemVo = new ShipItemVo();
		Set<ShipItemSubProductVo> shipItemSubProductVo = new LinkedHashSet<>();
		shipItemVo.setPsku(shipItem.getProduct().getPsku());
		shipItemVo.setDescription(shipItem.getProduct().getDescription());
		shipItemVo.setQuantity(shipItem.getQuantity());
		shipItemVo.setShipId(shipItem.getShipMaster().getShipId());
		shipItemVo.setShipItemId(shipItem.getShipItemId());

		Set<ShipItemSubProduct> shipItemSubProducts = shipItem
				.getShipItemSubProducts();

		List<ShipItemSubProduct> shipItemList = new ArrayList<ShipItemSubProduct>(
				shipItemSubProducts.size());
		shipItemList.addAll(shipItemSubProducts);
		Collections.sort(shipItemList, new ShipItemSubProduct());

		for (ShipItemSubProduct shipItemSubProduct : shipItemList) {
			shipItemSubProductVo.add(ModelVoHelper
					.shipItemSubProductModelToVo(shipItemSubProduct));
		}
		shipItemVo.setShipItemSubProducts(shipItemSubProductVo);

		return shipItemVo;
	}

	public static ShipItemSubProduct shipItemSubProductVoToModel(
			ShipItemSubProductVo shipItemSubProductVo) {
		ShipItemSubProduct shipItemSUbProduct = new ShipItemSubProduct();
		InvoiceItemSubProduct invoiceItemSubProduct = new InvoiceItemSubProduct();
		ShipItem shipItem = new ShipItem();

		invoiceItemSubProduct.setInvoiceItemSubProductId(shipItemSubProductVo
				.getInvoiceitemsubproductId());
		shipItemSUbProduct.setInvoiceItemSubProduct(invoiceItemSubProduct);

		shipItemSUbProduct.setShipItemSubProductId(shipItemSubProductVo
				.getShipitemsubproductId());

		shipItem.setShipItemId(shipItemSubProductVo.getShipItemId());
		shipItemSUbProduct.setShipItem(shipItem);
		return shipItemSUbProduct;
	}

	public static ShipItemSubProductVo shipItemSubProductModelToVo(
			ShipItemSubProduct shipItemSubProduct) {
		ShipItemSubProductVo shipItemSubProductVo = new ShipItemSubProductVo();
		shipItemSubProductVo.setInvoiceitemsubproductId(shipItemSubProduct
				.getInvoiceItemSubProduct().getInvoiceItemSubProductId());
		shipItemSubProductVo.setShipItemId(shipItemSubProduct.getShipItem()
				.getShipItemId());
		shipItemSubProductVo.setShipitemsubproductId(shipItemSubProduct
				.getShipItemSubProductId());
		return shipItemSubProductVo;

	}

	public static ShipMaster shipMasterVoToModel(ShipMasterVo shipMasterVo) {

		Set<ShipItem> shipItems = new LinkedHashSet<>();
		ShipItem shipItem = new ShipItem();

		ShipMaster shipMaster = new ShipMaster();
		shipMaster.setCustId(shipMasterVo.getCustId());
		shipMaster.setShipId(shipMasterVo.getShipId());
		shipMaster.setState(shipMasterVo.getState());
		shipMaster.setCreated(Helper.getDateFromString(shipMasterVo
				.getCreated()));
		shipMaster.setAcknowledged(Helper.getDateFromString(shipMasterVo
				.getAcknowledged()));
		shipMaster.setFinished(Helper.getDateFromString(shipMasterVo
				.getFinished()));
		shipMaster
				.setShipBy(Helper.getDateFromString(shipMasterVo.getShipBy()));
		shipMaster.setCustName(shipMasterVo.getCustName());
		shipMaster.setAddress(shipMasterVo.getAddress());
		shipMaster.setPhone(shipMasterVo.getPhone());
		shipMaster.setInstructions(shipMasterVo.getInstructions());
		shipMaster.setCommitBy(Helper.getDateFromString(shipMasterVo
				.getCommitBy()));
		shipMaster.setAckBy(shipMasterVo.getAckBy());
		shipMaster.setCreatedBy(shipMasterVo.getCreatedBy());

		InvoiceMaster invoiceMaster = new InvoiceMaster();
		invoiceMaster.setInvId(shipMasterVo.getInvId());
		shipMaster.setInvoiceMaster(invoiceMaster);

		JobMaster jobMaster = new JobMaster();
		jobMaster.setJobId(shipMasterVo.getJobId());
		shipMaster.setJobMaster(jobMaster);
		
		for (ShipItemVo shipItemVo : shipMasterVo.getShipItems()) {
			// shipItem = shipItemVoToModel(shipItemVo);
			shipItem.setShipMaster(shipMaster);
			shipItems.add(shipItemVoToModel(shipItemVo));

		}
		shipMaster.setShipItems(shipItems);
		return shipMaster;
	}

	public static ShipMasterVo shipMasterModelToVo(ShipMaster shipMaster) {

		ShipMasterVo shipMasterVo = new ShipMasterVo();
		Set<ShipItemVo> shipItemVoList = new LinkedHashSet<>();

		shipMasterVo.setShipId(shipMaster.getShipId());
		shipMasterVo.setCustId(shipMaster.getCustId());
		shipMasterVo.setState(shipMaster.getState());
		shipMasterVo.setCreated(Helper.getStringDateTimeFromDate(shipMaster
				.getCreated()));
		shipMasterVo.setAcknowledged(Helper
				.getStringDateTimeFromDate(shipMaster.getAcknowledged()));
		shipMasterVo.setFinished(Helper.getStringDateTimeFromDate(shipMaster
				.getFinished()));
		shipMasterVo.setShipBy(Helper.getStringDateTimeFromDate(shipMaster
				.getShipBy()));
		shipMasterVo.setCustName(shipMaster.getCustName());
		shipMasterVo.setAddress(shipMaster.getAddress());
		shipMasterVo.setPhone(shipMaster.getPhone());
		shipMasterVo.setInstructions(shipMaster.getInstructions());
		shipMasterVo.setCommitBy(Helper.getStringDateTimeFromDate(shipMaster
				.getCommitBy()));
		shipMasterVo.setInvId(shipMaster.getInvoiceMaster().getInvId());

		// shipMasterVo.setCreatedBy(shipMaster.getUserRole().getUserName());
		shipMasterVo.setCreatedBy(shipMaster.getCreatedBy());

		shipMasterVo.setAckBy(shipMaster.getAckBy());
		shipMasterVo.setJobId(shipMaster.getJobMaster().getJobId());
		
		Set<ShipItem> shipItems = shipMaster.getShipItems();

		List<ShipItem> shipList = new ArrayList<ShipItem>(shipItems.size());
		shipList.addAll(shipItems);
		Collections.sort(shipList, new ShipItem());

		for (ShipItem shipItem : shipList) {
			shipItemVoList.add(shipItemModelToVo(shipItem));
		}
		shipMasterVo.setShipItems(shipItemVoList);

		return shipMasterVo;
	}

	public static StockItems stockItemsVoToModel(StockItemsVo stockItemsVo) {

		StockItems stockItems = new StockItems();
		stockItems.setMacAddress(stockItemsVo.getMacAddress());
		return stockItems;
	}

	public static StockItemsVo stockItemModelToVo(StockItems stockItems) {

		StockItemsVo stockItemsVo = new StockItemsVo();
		stockItemsVo.setMacAddress(stockItems.getMacAddress());
		return stockItemsVo;
	}

	public static Terms termsVoToModel(TermsVo termsVo) {

		Terms terms = new Terms();
		terms.setTermId(termsVo.getTermId());
		terms.setText(termsVo.getText());

		return terms;
	}

	public static TermsVo termsModelToVo(Terms terms) {

		TermsVo termsVo = new TermsVo();
		termsVo.setTermId(terms.getTermId());
		termsVo.setText(terms.getText());

		return termsVo;

	}

	public static UserRole userRoleVoToUserRole(UserRoleVo userRoleVo)
			throws Exception {
		UserRole userRole = new UserRole();
		userRole.setEmail(userRoleVo.getEmail());
		userRole.setFullName(userRoleVo.getFullName());
		userRole.setPassword(Helper.encrypt(userRoleVo.getPassword()));
		userRole.setRoles(userRoleVo.getRoles());
		userRole.setUserName(userRoleVo.getUserName());
		userRole.setViews(userRoleVo.getViews());
		return userRole;
	}

	public static UserRoleVo userRoleToUserRoleVo(UserRole userRole) {
		UserRoleVo userRoleVo = new UserRoleVo();
		userRoleVo.setEmail(userRole.getEmail());
		userRoleVo.setFullName(userRole.getFullName());
		userRoleVo.setPassword(userRole.getPassword());
		userRoleVo.setRoles(userRole.getRoles());
		userRoleVo.setUserName(userRole.getUserName());
		userRoleVo.setViews(userRole.getViews());
		return userRoleVo;
	}

	public static Product productVoToModel(ProductVo productVo) {

		Product product = new Product();

		Set<ProductItem> productItems = new HashSet<>();

		product.setProductId(productVo.getProductId());
		product.setPsku(productVo.getPsku());
		product.setDescription(productVo.getDescription());
		product.setMaxAppCount(productVo.getMaxAppCount());
		product.setMaxVlanCount(productVo.getMaxVlanCount());

		Set<ProductItemVo> productItemVoList = productVo.getProductItems();

		for (ProductItemVo productItemVo : productItemVoList) {
			productItems.add(productItemVoToModel(productVo, productItemVo));
		}
		product.setProductItems(productItems);
		return product;
	}

	public static ProductVo productModelToVo(Product product) {

		ProductVo productVo = new ProductVo();
		Set<ProductItemVo> productItemsVo = new LinkedHashSet<>();

		productVo.setProductId(product.getProductId());
		productVo.setPsku(product.getPsku());
		productVo.setDescription(product.getDescription());
		productVo.setMaxAppCount(product.getMaxAppCount());
		productVo.setMaxVlanCount(product.getMaxVlanCount());
		
		Set<ProductItem> productItems = product.getProductItems();
		List<ProductItem> productList = new ArrayList<ProductItem>(productItems.size());
		productList.addAll(productItems);
		Collections.sort(productList,new ProductItem());
		
		for (ProductItem productItem : productList) {
			productItemsVo.add(ModelVoHelper.productItemModelToVo(productItem));
		}
		productVo.setProductItems(productItemsVo);
		return productVo;
	}

	public static ProductItem productItemVoToModel(ProductVo productVo,
			ProductItemVo productItemVo) {

		ProductItem productItem = new ProductItem();
		Product product = new Product();
		RawInventory rawInventory = new RawInventory();

		productItem.setQuantity(productItemVo.getQuantity());

		product.setPsku(productVo.getPsku());
		productItem.setProduct(product);

		rawInventory.setRsku(productItemVo.getRsku());
		rawInventory.setDescription(productItemVo.getDescription());
		productItem.setRawInventory(rawInventory);
		return productItem;
	}

	public static ProductItemVo productItemModelToVo(ProductItem productItem) {
		ProductItemVo productItemVo = new ProductItemVo();
		productItemVo.setPsku(productItem.getProduct().getPsku());
		productItemVo.setQuantity(productItem.getQuantity());
		productItemVo.setDescription(productItem.getRawInventory()
				.getDescription());
		productItemVo.setRsku(productItem.getRawInventory().getRsku());
		productItemVo.setProductItemId(productItem.getProductItemId());

		return productItemVo;
	}

	public static Set<ProductItem> productItemsVotoProductItems(
			ProductVo productVo) {
		Set<ProductItem> productItemsList = new HashSet<>();
		Set<ProductItemVo> itemsVo = productVo.getProductItems();
		ProductItem productItem = null;
		for (ProductItemVo productItemVo : itemsVo) {
			productItem = new ProductItem();

			productItem.setQuantity(productItemVo.getQuantity());
			productItemsList.add(productItem);
		}
		return productItemsList;
	}

	public static InvoiceTracking invoiceTrackingVoToModel(
			InvoiceTrackingVo invoiceTrackingVo) {
		InvoiceTracking invoiceTracking = new InvoiceTracking();
		InvoiceMaster invoiceMaster = new InvoiceMaster();
		ShipMaster shipMaster = new ShipMaster();

		invoiceTracking.setTrackId(invoiceTrackingVo.getTrackId());
		invoiceMaster.setInvId(invoiceTrackingVo.getInvId());
		invoiceTracking.setInvoiceMaster(invoiceMaster);
		invoiceTracking.setCarrier(invoiceTrackingVo.getCarrier());
		invoiceTracking
				.setTrackingNumber(invoiceTrackingVo.getTrackingNumber());
		shipMaster.setShipId(invoiceTrackingVo.getShipId());
		invoiceTracking.setShipMaster(shipMaster);
		return invoiceTracking;
	}

	public static InvoiceTrackingVo invoiceTrackingModelToVo(
			InvoiceTracking invoiceTracking) {
		InvoiceTrackingVo invoiceTrackingVo = new InvoiceTrackingVo();
		invoiceTrackingVo.setInvId(invoiceTracking.getInvoiceMaster()
				.getInvId());
		invoiceTrackingVo.setTrackId(invoiceTracking.getTrackId());
		invoiceTrackingVo.setCarrier(invoiceTracking.getCarrier());
		invoiceTrackingVo
				.setShipId(invoiceTracking.getShipMaster().getShipId());
		invoiceTrackingVo
				.setTrackingNumber(invoiceTracking.getTrackingNumber());

		return invoiceTrackingVo;
	}

	public static JobModelFlashDetails jobModelFlashDetailsVotoModel(
			JobModelFlashDetailsVo detailsVo) {
		JobModelFlashDetails details = new JobModelFlashDetails();
		JobMaster jobMaster = new JobMaster();
		JobItem jobItem = new JobItem();
		Product product = new Product();

		details.setJobmodelflashdetailsId(detailsVo.getJobmodelflashdetailsId());
		details.setMacAddress(detailsVo.getMacAddress());
		details.setSerialNo(detailsVo.getSerialNo());
		details.setImageType(Integer.valueOf(detailsVo.getImageType()!=null?detailsVo.getImageType().toString() : Constant.IMAGE_TYPE_VALUE));
		details.setStatus(detailsVo.getStatus());
		details.setVersion(detailsVo.getVersion());
		details.setBoardMacAddress(detailsVo.getBoardMacAddress());
		details.setDownloadLink(detailsVo.getDownloadLink());
		product.setPsku(detailsVo.getPsku());
		details.setImageName(detailsVo.getImageName());
		details.setProduct(product);
		
		jobItem.setJobItemId(detailsVo.getJobItemId());
		details.setJobItem(jobItem);
		
		jobMaster.setJobId(detailsVo.getJobId());
		details.setJobMaster(jobMaster);

		return details;
	}

	public static JobModelFlashDetailsVo jobModelFlashDetailsToVO(
			JobModelFlashDetails details) {
		JobModelFlashDetailsVo detailsVo = new JobModelFlashDetailsVo();
		detailsVo
				.setJobmodelflashdetailsId(details.getJobmodelflashdetailsId());
		detailsVo.setJobId(details.getJobMaster().getJobId());
		detailsVo.setPsku(details.getProduct().getPsku());
		detailsVo.setMacAddress(details.getMacAddress());
		detailsVo.setBoardMacAddress(details.getBoardMacAddress());
		detailsVo.setSerialNo(details.getSerialNo());
		detailsVo.setImageType(details.getImageType()!= null ? details.getImageType().toString() : Constant.IMAGE_TYPE_VALUE);
		detailsVo.setStatus(details.getStatus());
		detailsVo.setVersion(details.getVersion());
		detailsVo.setDownloadLink(details.getDownloadLink());
		//detailsVo.setImageName(details.getImageName());
		if(null != details.getJobItem()){
			detailsVo.setJobItemId(details.getJobItem().getJobItemId());
		}
		
		return detailsVo;
	}

	public static SubProduct subProductVoToModel(SubProductVo subProductVo) {
		SubProduct subProduct = new SubProduct();
		subProduct.setSubProductName(subProductVo.getSubProductName());
		subProduct.setSubProductDescription(subProductVo
				.getSubProductDescription());
		return subProduct;
	}

	public static SubProductVo subProductModelToVo(SubProduct subProduct) {
		SubProductVo subProductVo = new SubProductVo();
		subProductVo.setSubProductName(subProduct.getSubProductName());
		subProductVo.setSubProductDescription(subProduct
				.getSubProductDescription());
		return subProductVo;
	}

	public static InvoiceItemSubProduct invoiceItemSubProductVoToModel(
			InvoiceItemSubProductVo invoiceItemSubProductVo) {
		InvoiceItem invoiceItem = new InvoiceItem();
		SubProduct subProduct = new SubProduct();
		InvoiceItemSubProduct invoiceItemSubProduct = new InvoiceItemSubProduct();

		invoiceItem
				.setInvoiceItemId(invoiceItemSubProductVo.getInvoiceItemId());
		invoiceItemSubProduct.setInvoiceItem(invoiceItem);

		invoiceItemSubProduct
				.setInvoiceItemSubProductId(invoiceItemSubProductVo
						.getInvoiceitemsubproductId());
		invoiceItemSubProduct
				.setQuantity(invoiceItemSubProductVo.getQuantity());

		subProduct.setSubProductName(invoiceItemSubProductVo
				.getSubproductName());
		invoiceItemSubProduct.setSubProduct(subProduct);

		return invoiceItemSubProduct;
	}

	public static InvoiceItemSubProductVo invoiceItemSubProductToVo(
			InvoiceItemSubProduct invoiceItemSubProduct) {
		InvoiceItemSubProductVo invoiceItemSubProductVo = new InvoiceItemSubProductVo();
		invoiceItemSubProductVo.setInvoiceItemId(invoiceItemSubProduct
				.getInvoiceItem().getInvoiceItemId());
		invoiceItemSubProductVo
				.setInvoiceitemsubproductId(invoiceItemSubProduct
						.getInvoiceItemSubProductId());
		invoiceItemSubProductVo
				.setQuantity(invoiceItemSubProduct.getQuantity());
		invoiceItemSubProductVo.setSubproductName(invoiceItemSubProduct
				.getSubProduct().getSubProductName());
		return invoiceItemSubProductVo;
	}
}
