package com.meshdynamics.common.helper;

import com.meshdynamics.common.Constant;

public class MDSqlConstant {
	
	// Query constants for Add invoiceDao
	public static final String CUSTOMER_QUERY						= "from Customer where custId=:custId";
	public static final String TERM_QUERY							= "FROM Terms WHERE text=:text";
	
	// Query constants for get Invoices
	public static final String INVOICE_LIST_QUERY					= "from InvoiceMaster ";
	public static final String INVOICE_PAYMENT_COUNT_QUERY			= "select count(payId) from InvoicePayment where invId=:invId";
	public static final String INVOICE_TRACKING_QUERY				= "select count(payId) from InvoiceTracking where invId=:invId";
	public static final String INVOICE_TRACKING_COUNT_QUERY			= "select count(trackId) from "+"InvoiceTracking where invId=:invId";
	public static final String INVOICE_ITEMS_QUERY					= "from InvoiceItem where invId=:invId  order by psku asc";
	public static final String INVOICE_ITEM_SUB_PRODUCT_QUERY		= "from InvoiceItemSubProduct where invoiceItemId=:invoiceItemId";
	public static final String PURCHASE_ORDER_ID_QUERY				= "SELECT invId,poNumber,customer.custId FROM InvoiceMaster WHERE invId like '";
	public static final String PURCHASE_ORDER_CUST_NAME_QUERY		= "SELECT custName FROM InvoiceMaster where invId = :invId";
	
	// Query constants for getBalance
	public static final String INVOICE_PAYMENT_AMOUNT_QUERY			= "SELECT amount FROM invoicePayment WHERE invId=:invId";
	public static final String INVOICE_PAYMENT_LIST					= "from InvoicePayment where invId=:invId";
	public static final String INVOICE_PAYMENT_RAW_LIST				= "from InvoicePayment where rpId = :rpId";
	
	// Query constants for getInvoiceById
	public static final String INVOICE_CUSTOMER_ID_QUERY 			= "FROM InvoiceMaster where custId=:custId";
	public static final String INVOICE_MASTER_QUERY 				= "from InvoiceMaster where invId=:invId";
	public static final String INVOICE_ITEM_QUERY 					= "from InvoiceItem where invId=:invId";
	public static final String INVOICE_ITEM_SUUB_PRODUCT_QUERY		= "from InvoiceItemSubProduct where invoiceItemId=:invoiceItemId";
	public static final String INVOICE_SUB_PRODUCT_QUERY 			= "FROM InvoiceItemSubProduct where invoiceitemsubproductId=:invoiceitemsubproductId";
	
	// Query constants for deleteInvoice
	public static final String INNER_QUERY							= "SELECT *  FROM "
																		+ "((SELECT invId FROM invoicePayment)"
																		+ "UNION ALL(SELECT invId FROM rawPayment)"
																		+ "UNION ALL(SELECT invId FROM shipMaster)"
																		+ "UNION ALL(SELECT invId FROM jobMaster))"
																		+ "t WHERE t.invId=";
	
	public static final String INVOICE_AMOUNT_QUERY					= "SELECT amount FROM invoiceMaster WHERE invId=:invId";
	
	public static final String DELETE_INVOICE_QUERY					= "delete from InvoiceItemSubProduct where invoiceItemId=:invoiceItemId";
	public static final String DB_VLAN_SUB_PRODUCT_QUERY			= "SELECT quantity FROM InvoiceItemSubProduct WHERE "
																		+ "invoiceItemId=:invoiceItemId and subproductName=:subproductName";
	public static final String DB_APP_SUB_PRODUCT_QUERY				= "SELECT quantity FROM InvoiceItemSubProduct WHERE "
																		+ "invoiceItemId=:invoiceItemId and subproductName=:subproductName";
	public static final String PAYMENT_AMOUNT_QUERY					= "SELECT amount FROM InvoicePayment where invId = :invId";
	public static final String INVOICE_GENERIC_QUERY				= "From InvoiceMaster ";
	public static final String INVOICE_GENERIC_COUNT_QUERY			= "SELECT COUNT(*) AS COUNT FROM InvoiceMaster where ";
	
	
	// Query JobMaster
	public static final String JOB_MASTER_QUERY						= "from JobMaster where invId=:invId";
	public static final String JOB_ITEMS_QUANTITY_QUERY				= "SELECT quantity FROM JobItem WHERE jobItemId = :jobItemId";
	public static final String JOB_ID_QUERY							= "SELECT state FROM JobMaster where invId=:invId";
	public static final String JOB_STATE_QUERY						= "SELECT state FROM JobMaster where invId=:invId and state=:state";
	public static final String SHIP_ID_QUERY						= "SELECT state FROM ShipMaster where invId=:invId";
	public static final String SHIP_STATE_QUERY						= "SELECT state FROM ShipMaster where invId=:invId and state=:state";
	
	/****************************************** Customer Query constants********************************************/
	
	public static final String CUSTOMER_NAME_LIKE_QUERY				= "from Customer where name like'";
	public static final String CUSTOMER_NAME_QUERY					= "from Customer where name=:name";
	public static final String CUSTOMER_LIST_QUERY					= "from Customer ";
	public static final String CUSTOMER_INNER_QUERY					= "SELECT custId FROM InvoiceMaster where custId=:custId";
	
	/*********************** Inventory Query Constants ********************************************/
	
	public static final String INVENTORY_QUERY						= "from RawInventory where rsku=:rsku";
	public static final String INVENTORY_LIST_QUERY					= "from RawInventory ";
	public static final String INNER_INVENTORY_QUERY				= "SELECT rsku FROM productItem where rsku=:rsku";
	public static final String INVENTORY_BY_SKU_QUERY				= "SELECT rsku FROM RawInventory WHERE rsku like '";
	
	/******************************* Job Query Constants ******************************************/
	
	public static final String JOB_ITEM_QUANTITY_QUERY				= "SELECT quantity FROM JobItem where invoiceItemId = :invoiceItemId";
	public static final String JOB_LIST_QUERY						= "from JobMaster";
	public static final String JOB_SUB_QUERY						= "FROM InvoiceItemSubProduct"
																		+ " WHERE invoiceitemsubproductId "
																		+ "IN (SELECT jobItemSubProduct.invoiceitemsubproductId "
																		+ "FROM JobItemSubProduct  jobItemSubProduct"
																		+ " WHERE jobItemsubproductId=:jobItemsubproductId)";
	public static final String JOB_ITEM_QUERY						= "from JobItem where jobId=:jobId";
	public static final String JOB_ITEM_ASC_QUERY					= "from JobItem where jobId=:jobId order by psku asc";
	public static final String ITEM_PSKU_QUERY						= "from InvoiceItem where psku=:psku";
	public static final String JOB_ITEM_SUB_PRODUCT_QUERY			= "from JobItemSubProduct where jobItemId=:jobItemId";
	public static final String JOB_MASTER_ID_QUERY					= "from JobMaster where jobId = :jobId";
	public static final String DELETE_JOB_QUERY						= "delete from JobItemSubProduct where jobItemId=:jobItemId";
	public static final String INNER_DELETE_QUERY					= "SELECT jobId FROM shipMaster where jobId=:jobId";
	
	public static final String JOB_MODEL_FLASH_QUERY			    = "FROM JobModelFlashDetails where jobId=:jobId";
	public static final String JOB_GENERIC_QUERY					= "FROM JobMaster ";
	public static final String FETCH_JOB_ID							= "SELECT jobId FROM JobMaster where invId=:invId";
	public static final String JOB_ORDER_ID_QUERY			    	= "SELECT jobId FROM JobMaster WHERE jobId like '";
	public static final String JOB_STATE_FLASH_QUERY				= "select state from JobMaster where jobId = :jobId";
	public static final String JOB_ITEM_BASED_JOB_ID_QUERY			= "FROM JobItem where jobId=:jobId";
	public static final String JOB_ITEM_FLASH_QUERY					= "SELECT jobItemId FROM JobModelFlashDetails where jobId=:jobId";
	public static final String JOB_MASTER_STATE_QUERY				= "SELECT state FROM JobMaster WHERE jobId=:jobId";
	
	/***************************************** JobModelFlashDetails**********************************************/
	public static final String JOB_MODEL_FLASH_PSKU_QUERY 			= "from FlashDeployModels where psku = :psku";
	public static final String JOB_MODEL_FLASH_MAC_ADDRESS_QUERY    = "FROM JobModelFlashDetails where boardMacAddress=:boardMacAddress";
	public static final String INVOICEITEM_FROM_JOBITEM_QUERY		= "SELECT invoiceItemId FROM jobItem where jobItemId=:jobItemId";
	public static final String JOBID_JOBITEM_ID_FLASH_QUERY			= "from JobModelFlashDetails where jobId = :jobId and jobItemId = :jobItemId";
	public static final String JOB_MODEL_FLASH_LIST_QUERY			= "from JobModelFlashDetails ";
	public static final String JOB_MODEL_FLASH_COUNT_QUERY 			= "Select count(*) from JobModelFlashDetails where jobId=:jobId and status >= :status";
	public static final String PSKU_MAC_FLASH_QUERY					= "from JobModelFlashDetails where psku = :psku AND boardMacAddress = :boardMacAddress AND jobmodelflashdetailsId=:jobmodelflashdetailsId";
	
	public static final String MAC_TYPE_QUERY						= "from ModelMacDetails where macType=:macType AND jobmodelflashdetailsId=:jobmodelflashdetailsId";
	public static final String IMAGE_NAME_FLASH_QUERY				= "from JobModelFlashDetails where imageName = :imageName";
	public static final String ADDRESS_ITERATOR_LIST				= "from AddressIterator ";
	
	/*********************************************** UserRole *****************************************************/
	
	public static final String FETCH_USER_QUERY						= "from UserRole where userName = :userName";
	public static final String FETCH_EMAIL_QUERY					= "from UserRole where email=:email";
	public static final String FETCH_EMAIL							= "SELECT email from UserRole where userName=:userName";
	
	
	/************************************************ MacAddress **************************************************/
	
	public static final String MAC_ADDRESS_QUERY					= "SELECT customer.custId custId, "
																	+ // index 0
																	"customer.Name customerName , "
																	+ // index 1
																	"invoiceMaster.invId invId, "
																	+ // index 2
																	"invoiceItem.invoiceItemId  invoiceItemId, "
																	+ // index 3
																	"invoiceItem.psku psku, "
																	+ // index 4
																	"invoiceItem.quantity invoiceItemQuantity, "
																	+ // index 5
																	"flashDeployModels.addressesPerUnit  addressesPerUnit "
																	+ // index6
																	"FROM customer,  " + "invoiceMaster , "
																	+ "invoiceItem, "
																	+ "flashDeployModels "
																	+ "where  customer.custId=invoiceMaster.custId "
																	+ "and invoiceMaster.invId=invoiceItem.invId " 
																	+ "and invoiceItem.psku=flashDeployModels.psku ";
	
	public static final String MAC_ADDRESS_QUERY_ORDER_BY			= " order by customer.custId,invoiceMaster.invId,invoiceItem.invoiceItemId";
	
	public static final String BOARD_MAC_BASED_MAC_QUERY			= "SELECT jobModelFlashDetails.boardMacAddress "
																	+ "FROM jobModelFlashDetails "
																	+ "INNER JOIN modelMacDetails "
																	+ "ON jobModelFlashDetails.jobmodelflashdetailsId = modelMacDetails.jobmodelflashdetailsId "
																	+ "WHERE modelMacDetails.macAddress = :macAddress ";
	
	public static final String FETCH_MAC_QUERY						= "SELECT  DISTINCT "
																	+ "ii.invid, "
																	+ "ii.psku, "
																	+ "(SELECT concat_ws(' ', subproductName, isu.quantity ) FROM invoiceItemSubProduct isu WHERE isu.invoiceItemId=ii.invoiceItemId AND isu.subproductName='APP') App, "
																	+ "CONCAT_WS(' ',(SELECT CONCAT_WS(' ', subproductName, isu.quantity ) FROM invoiceItemSubProduct isu WHERE isu.invoiceItemId=ii.invoiceItemId && isu.subproductName='VLAN' ),  ii.quantity ) Vlan, "
																	+ "ji.invoiceItemId, "
																	+ "jf.boardMacAddress, "
																	+ "ji.jobid, "
																	+ "jf.jobmodelflashdetailsId, "
																	+ "jf.version, "
																	+ "mm.macAddress, "
																	+ "mm.macType "
																	+ "FROM mdmanufacturer.invoiceItem ii,  jobItem ji, jobModelFlashDetails jf, modelMacDetails mm "
																	+ "WHERE ii.invoiceItemId=ji.invoiceItemId  "
																	+ "AND ji.jobId=jf.jobId "
																	+ "AND ji.jobItemId = jf.jobItemId "
																	+ "AND jf.jobmodelflashdetailsId=mm.jobmodelflashdetailsId "
																	+ "AND jf.boardMacAddress=:boardMac "
																	+ "AND ii.invid=:invId  ";
	
	public static final String FETCH_MAC_ON_CRITERIA_QUERY			= "SELECT  DISTINCT "
																	+ "ii.invid, "
																	+ "concat_ws(' ', ii.psku, ii.quantity ) psku, "
																	+ "(SELECT concat_ws(' ', subproductName, isu.quantity ) FROM invoiceItemSubProduct isu WHERE isu.invoiceItemId=ii.invoiceItemId AND isu.subproductName='APP') App, "
																	+ "(SELECT concat_ws(' ', subproductName, isu.quantity ) FROM invoiceItemSubProduct isu WHERE isu.invoiceItemId=ii.invoiceItemId && isu.subproductName='VLAN') Vlan, "
																	+ "ji.invoiceItemId, "
																	+ "jf.boardMacAddress, "
																	+ "ji.jobid, "
																	+ "jf.jobmodelflashdetailsId, "
																	+ "jf.version, "
																	+ "mm.macAddress, "
																	+ "mm.macType "
																	+ "FROM mdmanufacturer.invoiceItem ii,  jobItem ji, jobModelFlashDetails jf, modelMacDetails mm "
																	+ "WHERE ii.invoiceItemId=ji.invoiceItemId  "
																	+ "AND ji.jobId=jf.jobId "
																	+ "AND ji.jobItemId = jf.jobItemId "
																	+ "AND jf.jobmodelflashdetailsId=mm.jobmodelflashdetailsId "
																	+ "AND ii.invid=:invId  ";
	
	public static final String INVOICE_ID_BASED_FLASHID_QUERY		= "SELECT invId FROM jobMaster INNER JOIN jobModelFlashDetails ON jobMaster.jobId = jobModelFlashDetails.jobId AND jobmodelflashdetailsId=:jobmodelflashdetailsId";
	
	public static final String FLASH_ID_BASED_MAC_ADDRESS_QUERY		= "SELECT jobModelFlashDetailsId FROM modelMacDetails WHERE macAddress=:macAddress";
	
	public static final String COUNT_MAC_USING_FLASH_ID_QUERY		= "SELECT COUNT(*) FROM ModelMacDetails WHERE jobmodelflashdetailsid=:jobmodelflashdetailsid";
	
	public static final String FETCH_JOBID_FROM_FLASH_QUERY			= "SELECT jobId FROM jobModelFlashDetails WHERE jobmodelflashdetailsid=:jobmodelflashdetailsid";
	
	public static final String FLASH_ID_BASED_BOARD_MAC_ADDRESS_QUERY= "SELECT jobModelFlashDetailsId FROM jobModelFlashDetails WHERE boardMacAddress=:boardMacAddress";
	
	public static final String FLASH_ID_BASED_JOBID_QUERY			= "SELECT jobmodelflashdetailsid FROM jobModelFlashDetails WHERE jobId=:jobId";
	
	public static final String INVOICE_ID_BASED_BOARDMACADDRESS_QUERY= "SELECT invId FROM jobMaster INNER JOIN jobModelFlashDetails ON jobMaster.jobId = jobModelFlashDetails.jobId WHERE boardMacAddress=:boardMacAddress";
	
	public static final String COUNT_MAC_QUERY						= "SELECT COUNT(*) FROM customer, "
																	+ "invoiceMaster, "
																	+ "invoiceItem, "
																	+ "flashDeployModels "
																	+ "WHERE  customer.custId=invoiceMaster.custId "
																	+ "AND invoiceMaster.invId=invoiceItem.invId "
																	+ "AND invoiceItem.psku=flashDeployModels.psku ";
	
	public static final String FETCH_BOARD_MAC_ADDRESS				= "SELECT boardMacAddress FROM JobModelFlashDetails where jobId=:jobId";
	
	public static final String BOARD_MAC_COUNT						= "SELECT  " 
																	+ "jf.boardMacAddress "
																	+ "FROM mdmanufacturer.invoiceItem ii,  jobItem ji, jobModelFlashDetails jf, modelMacDetails mm "
																	+ "WHERE ii.invoiceItemId=ji.invoiceItemId  "
																	+ "AND ji.jobId=jf.jobId "
																	+ "AND ji.jobItemId = jf.jobItemId "
																	+ "AND jf.jobmodelflashdetailsId=mm.jobmodelflashdetailsId "
																	+ "AND ii.invid = :invId "
																	+ "AND jf.boardMacAddress = :boardMacAddress";
	
	public static final String RAW_PAYMENT_AMOUNT_QUERY				= "SELECT amount FROM RawPayment WHERE rpId=:rpId";
	public static final String RAW_PAYMENT_LIST						= "from RawPayment where rpId=:rpId";
	public static final String RAW_PAYMENTS_LIST					= "from RawPayment ";
	
	public static final String RAW_PAYMENT_STATE_QUERY				= "from RawPayment where state=:state and custId=:custId";
	public static final String INVOICE_PAYMENTS_LIST				= "from InvoicePayment ";
	
	public static final String REVENUE_LIST_QUERY					= "select distinct "
																		+ "invoiceMaster.invId purchaseOrder, "  // 0
																		+ "customer.Name customerName, "  // 1
																		+ "userRole.userName salesRep, "  // 2
																		+ "invoicePayment.amount amount, " //3
																		+ "invoicePayment.notes notes, "  // 4
																		+ "shipMaster.acknowledged acknowledged, "  // 5
																		+ "invoicePayment.payId payId "
																		+ "from "
																		+ "mdmanufacturer.invoiceMaster, "
																		+ "mdmanufacturer.customer,  "
																		+ "mdmanufacturer.userRole, "
																		+ "mdmanufacturer.invoicePayment, "
																		+ "mdmanufacturer.shipMaster , "
																		+ "mdmanufacturer.rawPayment "
																		+ "where  "
																		+ "invoiceMaster.custId=customer.custId "
																		+ "and invoiceMaster.salesRep=userRole.userName "
																		+ "and invoiceMaster.invId=shipMaster.invId "
																		+ "and invoiceMaster.invId=invoicePayment.invId "
																		+ "and invoiceMaster.invId=rawPayment.invId "
																		+ "and shipMaster.acknowledged is not null "
																		+ "AND invoiceMaster.status='"
																		+ Constant.DB_INVOICE_CLOSED
																		+ "' "
																		+ "and rawPayment.state='"
																		+ Constant.UI_RAW_CLOSED
																		+ "' ";
	
	public static final String REVENUE_TOTAL_AMOUNT_QUERY			= "select "
																		+ "invoicePayment.amount AS TotalAmount "
																		+ "from "
																		+ "mdmanufacturer.invoiceMaster, "
																		+ "mdmanufacturer.customer,  "
																		+ "mdmanufacturer.userRole, "
																		+ "mdmanufacturer.invoicePayment, "
																		+ "mdmanufacturer.shipMaster , "
																		+ "mdmanufacturer.rawPayment "
																		+ "where  "
																		+ "invoiceMaster.custId=customer.custId "
																		+ "and invoiceMaster.salesRep=userRole.userName "
																		+ "and invoiceMaster.invId=shipMaster.invId "
																		+ "and invoiceMaster.invId=invoicePayment.invId "
																		+ "and invoiceMaster.invId=rawPayment.invId "
																		+ "and shipMaster.acknowledged is not null "
																		+ "AND invoiceMaster.status='"
																		+ Constant.DB_INVOICE_CLOSED
																		+ "' "
																		+ "and rawPayment.state='"
																		+ Constant.UI_RAW_CLOSED
																		+ "' ";
	
	public static final String DELETE_INVOICE_PAYMENT_QUERY			= "delete from InvoicePayment where invId = :invId";

	public static final String PRODUCT_PSKU_QUERY					= "from Product where psku=:psku";
	public static final String INVOICE_ITEM_PSKU_QUERY 				= "From InvoiceItem where psku=:psku";
	public static final String PRODUCT_BY_PRODUCT_ID_QUERY			= "SELECT psku FROM Product WHERE psku like '";

	
	public static final String FLASH_DEPLOY_CONFIG_QUERY			= "from FlashDeployModels where psku=:psku";
	
	public static final String PRODUCTS_LIST						= "from Product ";
	
	public static final String DELETE_PRODUCT_INNER_QUERY 			= "SELECT * FROM ((SELECT psku FROM forecastItem) UNION ALL (SELECT psku FROM shipItem) union all (select psku FROM invoiceItem)) t WHERE t.psku='";
	public static final String DELETE_PRODUCT_QUERY					= "delete from ProductItem where psku=:psku";
	
	public static final String SHIP_JOB_QUERY						= "FROM ShipMaster WHERE jobId=:jobId";
	public static final String USER_ROLE_USERNAME_QUERY				= "from UserRole where userName=:userName";
	public static final String USER_ROLE_LIST						= "from UserRole ";
	
	public static final String INVOICE_CUST_ID_QUERY				= "SELECT custId FROM invoiceMaster where invId=:invId";
	public static final String SHIP_CUSTOMER_QUERY					= "SELECT shipAddress FROM Customer WHERE custId=:custId";
	
	public static final String SHIP_LIST_QUERY						= "from ShipMaster ";
	public static final String SHIP_QUERY							= "from ShipMaster where shipId = :shipId";
	public static final String SHIP_INVOICE_ID_QUERY				= "FROM ShipMaster where invId=:invId";
	public static final String SHIP_GENERIC_QUERY 					= "FROM ShipMaster ";
	
	public static final String INVOICE_TRACKING_SHIP_QUERY			= "from InvoiceTracking where shipId=:shipId";
	public static final String TRACKING_QUERY						= "FROM InvoiceTracking where trackingNumber=:trackingNumber";
	
	public static final String SUB_PRODUCT_LIST						= "from SubProduct ";
	public static final String TERMS_LIST							= "from Terms ";
	
	public static final String TERM_ID_QUERY						= "from Terms where termId=:termId";
	public static final String INVOICE_TRACKING_LIST				= "from InvoiceTracking ";
	public static final String TRACKING_ID_QUERY					= "from InvoiceTracking where trackId = :trackId";
	
	public static final String TRACKING_INVOICE_ID_QUERY			= "from InvoiceTracking where invId=:invId";
	public static final String USER_ROLE_PASSWORD_QUERY				= "SELECT password FROM UserRole where userName=:userName";
	
	public static final String DELETE_USER_ROLE_QUERY				= "SELECT *  FROM "
																		+ "((SELECT createdBy FROM shipMaster)"
																		+ "UNION ALL(SELECT createdBy FROM invoiceMaster)"
																		+ "UNION ALL(SELECT createdBy FROM jobMaster))"
																		+ "t WHERE t.createdBy=:createdBy";
	
	public static final String FORECAST_LIST_QUERY					= "from ForecastMaster ";
	public static final String FORECAST_ID_QUERY					= "from ForecastMaster where fcastId=:fcastId";
	public static final String DELETE_FORECAST_QUERY				= "delete from ForecastItem where fcastId=:fcastId";
	
}
