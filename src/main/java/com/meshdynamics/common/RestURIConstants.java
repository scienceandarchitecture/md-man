/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : RestConstant.java
 * Comments : RestConstant.java 
 * Created  : 28-Jul-2016
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |28-Jul-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.common;

public class RestURIConstants {

	public static final String ADD_CUSTOMERS = "/addCustomers";
	public static final String EDIT_CUSTOMER_RECORD = "/editCustomerRecord";
	public static final String GET_CUSTOMER_LIST = "/getCustomerList";
	public static final String DELETE_CUSTOMER_BY_ID = "/deleteCustomerById";
	public static final String GET_CUSTOMER_BY_ID = "/getCustomerById";
	public static final String GET_CUSTOMER_BY_NAME = "/getCustomerByName";

	public static final String ADD_PRODUCT = "/addProducts";
	public static final String GET_PRODUCT = "/getProductList";
	public static final String DELETE_PRODUCT_BY_ID = "/deleteProductById";
	public static final String GET_PRODUCT_BY_ID = "/getProductById";
	public static final String UPDATE_PRODUCT = "/updateProducts";
    public static final String GET_PURCHASE_ORDER_LIST = "/getPurchaseOrderList";
	public static final String GET_PRODUCT_LIST_USING_PAGINATION = "/getProductListUsingPagination";
	public static final String GET_PRODUCT_LIST_BY_PRODUCT_ID = "/getProductListByProductId";

	public static final String ADD_INVOICE = "/addInvoices";
	public static final String GET_INVOICE = "/getInvoices";
	public static final String GET_INVOICE_BY_ID = "/getInvoiceById";
	public static final String DELETE_INVOICE_BY_ID = "/deleteInvoiceById";
	public static final String UPDATE_INVOICE_BY_ID = "/updateInvoiceById";
	public static final String GET_PURCHASE_ORDER_BY_ID = "/getPurchaseOrderById";

	public static final String ADD_TERMS = "/addTerms";
	public static final String UPDATE_TERMS = "/updateTerms";
	public static final String GET_TERMS = "/getTerms";
	public static final String DELETE_TERM_BY_ID = "/deleteTermById";

	public static final String ADD_RAW_INVENTORY = "/addRawInventories";
	public static final String UPDATE_RAW_INVENTORY = "/updateRawInventories";
	public static final String GET_RAW_INVENTORY_LIST = "/getRawInventories";
	public static final String DELETE_RAW_INVENTORY_BY_ID = "/deleteRawInventoryById";
	public static final String GET_INVENTORY_LIST_USING_PAGINATION = "/getInventoryListUsingPagination";
	public static final String GET_RAW_INVENTORY_BY_SKU = "/getRawInventoryBySKU";

	public static final String ADD_USERS = "/addUsers";
	public static final String GET_USERS = "/getUsers";
	public static final String UPDATE_USER = "/updateUser";
	public static final String DELETE_USER_BY_ID = "/deleteUserById";
	public static final String GET_USER_BY_ID = "getUserByID";
	public static final String GET_USERS_BASED_ON_PAGINATION = "/getUserBasedOnPagination";

	public static final String ADD_FORECAST = "/addForecast";
	public static final String GET_FORECASTS = "/getForecastList";
	public static final String GET_FORECAST_BY_ID = "/getForecastById";
	public static final String DELETE_FORECAST_BY_FORECAST_ID = "/deleteForecastByForecastId";
	public static final String UPDATE_FORECAST = "/updateForecast";

	public static final String ADD_SHIPPING_ITEM = "/addShippingItems";
	public static final String GET_SHIPPING_ITEM = "/getShippingItemList";
	public static final String GET_SHIP_ITEM_BY_ID = "/getShipItemByID";
	public static final String DELETE_SHIPMASTER_BY_ID = "/deleteShipMasterByID";
	public static final String UPDATE_SHIP_ITEMS = "/updateShipItems";
	public static final String GET_SHIPPING_ITEM_BY_INVOICE_ID = "/getShippingItemByInvoiceId";
	public static final String GET_SHIP_LIST_PAGINATION = "/getShipListBasedOnPagination";

	public static final String ADD_JOB_ORDERS = "/addJobOrders";
	public static final String GET_JOB_ORDERS = "/getJobOrders";
	public static final String GET_JOB_ORDER_BY_ID = "/getJobOrderById";
	public static final String UPDATE_JOB_ORDER = "/updateJobOrder";
	public static final String DELETE_JOB_ORDER_BY_JOB_ID = "/deleteJobOrderByJobId";
    public static final String GET_JOB_ORDER_LIST = "/getJobOrderList";
    public static final String GET_JOB_ORDER_LIST_BY_CRITERIA = "/getJobOrderListByCriteria";
    public static final String GET_JOB_ORDER_LIST_PAGINATION = "/getJobOrderListBasedOnPagination";
    public static final String GET_JOB_ORDER_LIST_BY_ID = "/getJobOrderListById";

	public static final String ADD_RAWPAYMENT = "/addRawPayment";
	public static final String GET_RAW_PAYMENT_LIST = "/getRawPaymentList";
	public static final String GET_RAW_PAYMENT_BY_ID = "/getRawPaymentById";
	public static final String UPDATE_RAW_PAYMENT = "/updateRawPayment";
	public static final String DELETE_RAW_PAYMENT_BY_ID = "/deleteRawPaymentById";
	public static final String GET_OPEN_RAW_PAYMENT_LIST = "/getOpenRawPaymentList";
	public static final String GET_PAYMENT_LIST_USING_PAGINATION = "/getPaymentListUsingPagination";

	public static final String ADD_INVOICE_PAYMENT = "/addInvoicePayment";
	public static final String GET_INVOICE_PAYMENT_LIST = "/getInvoicePaymentList";
	public static final String UPDATE_INVOICE_PAYMENT = "/updateInvoicePayment";
	public static final String GET_INVOICE_PAYMENT_BY_INVOICE_ID = "/getInvoicePaymentByInvoiceId";
	public static final String DELETE_INVOICE_PAYMENT_BY_INVOICE_ID = "/deleteInvoicePaymentByInvoiceId";
	public static final String GET_PAYMENT_COUNT_BY_INVOICE_ID = "/getPaymentCountByInvoiceId";

	public static final String LOGOUT = "/logout";
	public static final String LOGIN = "/login";
	public static final String CHANGE_PASSWORD = "/changePassword";
	public static final String FORGOT_PASSWORD = "/forgotPassword";

	public static final String ADD_TRACKING = "/addTracking";
	public static final String GET_TRACKING_LIST = "/getTrackingList";
	public static final String GET_TRACK_LIST_BY_TRACKING_ID = "getTrackListById";
	public static final String GET_TRACKING_COUNT_BY_INVOICE_ID = "/getTrackingCountByInvoiceId";
	public static final String UPDATE_INVOICE_TRACKING = "/updateInvoiceTracking";// modified
	public static final String DELETE_INVOICE_TRACKING_BY_ID = "/deleteInvoiceTrackingById";// modified
	public static final String GET_INVOICE_TRACK_LIST_BY_INVID = "/getInvoiceTrackListByInvId";// modified
	
	public static final String GET_MODEL_LIST = "/getModelList";

	public static final String ADD_JOBMODELDETAILS = "/addJobModelFlashDetails";
	public static final String UPDATE_JOBMODELDETAILS = "/updateJobModelFlashDetails";
	public static final String DELETE_JOBMODELDETAILS_BYID = "/deleteJobModelFlashDetailsById";
	public static final String GET_JOBMODELDETAILS_BYID = "/getJobModelFlashDetailsById";
	public static final String GET_JOBMODELDETAILS_LIST = "/getJobModelFlashDetailsList";
	public static final String GET_JOBMODELDETAILS_BYJOBID_AND_MODELID = "/getJobModelFlashDetailsByJobIdAndModelId";
	public static final String GET_MACADDRESSBY_JOBMODELFLASHDETAILS_ID = "/getMacAddressByJobModelFlashDetailsId";
	public static final String ADD_JOBMODELDETAILS_LIST = "/addJobModelFlashDetailsList";
	public static final String VERIFY_MAC_ADDRESS = "/verifyMacAddress";
	public static final String FETCH_BUILD_SERVER_IMAGE_DOWNLOAD_PATH = "/fetchBuildServerImageDownloadPath";
	
	public static final String GET_SUB_PRODUCT = "/getSubProduct";
	public static final String ADD_SUB_PRODUCT = "/addSubProduct";
	
	public static final String GET_MAC_ADDRESS_SUMMARY = "/getMacAddressSummary";
	public static final String GET_MAC_ADDRESS_Details = "/getMacAddressDetails";
	public static final String GET_MAC_ADDRESS_LIST_USING_PAGINATION = "/getMacAddressListUsingPagination";
	public static final String GET_MAC_ADDRESS_LIST_BASED_SEARCH_CRITERIA = "/getMacAdressListBasedSearchCriteria";
	public static final String VIEW_MAC_DETAILS_BASED_ON_CRITERIA = "/viewMacDetailsBasedOnCritera";
	
	public static final String GET_REVENUE_LIST = "/getRevenueList";
	public static final String GET_REVENUE_LIST_USING_PAGINATION = "/getRevenueListUsingPagination";
	public static final String GENERATE_BUILD_API_URL = "http://localhost:8081/MDBuild/build/generateBuild";
	
	public static final String REGENERATE_BUILD= "/regenerateBuild";
	public static final String VIEW_MAC_RANGE= "/viewMacRange";
	public static final String GET_PURCHASE_ORDER_LIST_PAGINATION = "/getPurchaseOrderListWithPagination";

}
