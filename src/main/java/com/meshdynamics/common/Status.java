/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : Status.java
 * Comments : Status.java 
 * Created  : 27-Jul-2016
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |27-Jul-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.common;

public enum Status {
	Success(0), Failure(1);

	private final int value;

	Status(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}
}
