/**
 * 
 */
package com.meshdynamics.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author SANJAY
 *
 */
public class MDManufacturerProperties {

	public static String getValue(String attribute) throws IOException {
		String result = "";
		InputStream in = null;
		try {
			in = MDManufacturerProperties.class.getResourceAsStream("/message.properties");
			Properties properties = new Properties();
			properties.load(in);
			result = properties.getProperty(attribute);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}
}
