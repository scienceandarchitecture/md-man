package com.meshdynamics.common;

/**
 * @author Sanjay
 *
 */
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class LoginCache {

	private static Map<String, String> map = null;

	private LoginCache() {
	}

	public synchronized static Map<String, String> getInstance() {
		if (map == null) {
			map = new ConcurrentHashMap<String, String>();
		}

		return map;
	}
}
