/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : RestConstant.java
 * Comments : RestConstant.java 
 * Created  : 28-Jul-2016
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |28-Jul-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.common;

import org.springframework.http.MediaType;

public interface RestConstants {
	
	public static final String MEDIA_TYPE		=	MediaType.APPLICATION_JSON_VALUE;
	
	public static final int OK 					= 	200;
	public static final int CREATED 			= 	201;
	public static final int NO_RECORDS			=	202;
	public static final int PARTIAL_CONTENT		=	206;
	
	public static final int UNAUTHORISED		=	401;
	public static final int UNSUPPORTED_MEDIA	=	415;
	public static final int LOGIN_TIMEOUT		=  	440;
	public static final int BAD_REQUEST			=	400;
	public static final int INVALID_TOKEN		=	498;
	
	public static final int INTERNAL_SERVER_ERROR=	500;
	public static final int NOT_IMPLEMENTED		=	501;
	public static final int NOT_MODIFIED		=	304;
	
	
	public static final int GET					=	0;
	public static final int POST				=	1;
	public static final int PUT					=	3;
	public static final int DELETE				=	4;
	
	
	
	
}
