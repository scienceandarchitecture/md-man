/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : DBConstant.java
 * Comments : DBConstant.java 
 * Created  : 23-May-2017
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |25-May-2017  | Created                             | Pinki 		 |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.common;

public class DBConstant {
	
	public static final String CUSTOMER_ID							= "custId";
	public static final String TEXT									= "text";
	public static final String INVOICE_ID							= "invId";
	public static final String INVOICE_ITEM_ID						= "invoiceItemId";
	public static final String INVOICE_ITEM_SUB_PRODUCT_ID			= "invoiceitemsubproductId";
	public static final String SUB_PRODUCT							= "subproductName";
	public static final String VLAN									= "VLAN";
	public static final String APP 									= "APP";
	public static final String STATE								= "state";
	public static final String STATUS								= "status";
	public static final String FINISHED								= "finished";
	public static final String ACKNNOWLEDGED						= "acknowledged";
	
	public static final String CUSTOMER_NAME						= "name";
	public static final String RSKU									= "rsku";
	public static final String PSKU									= "psku";
	public static final String PO_PREFIX							= "invoiceMaster.";
	public static final String CUSTOMER_PREFIX						= "customer.";
	
	public static final String JOB_ID								= "jobId";
	public static final String JOB_ITEM_ID							= "jobItemId";
	public static final String FLASH_ID 							= "jobmodelflashdetailsid";
	public static final String JOB_ITEM_SUB_PRODUCT_ID				= "jobItemsubproductId";
	
	public static final String BOARD_MAC_ADDRESS					= "boardMacAddress";	
	public static final String MAC_ADDRESS							= "macAddress";
	public static final String BOARD_MAC							= "boardMac";
	
	public static final String CONFIG_FILE_NAME						= "MD4";
	public static final String TARGET_CNS							= "CNS";
	public static final String TARGET_IMX							= "IMX";
	public static final String JOBFLASH_PREFIX						= "jf.";
	public static final String MODELMAC_PREFIX						= "mm.";
	
	public static final String AUTHENTICATION						= "Authentication";
	public static final String RESPONSE_BODY						= "responseBody";
	public static final String MAC_ADDRESS_DIGIT					= "macAddressDigest";
	public static final String UNIQUE_ID							= "uniqueId";
	public static final String SIGNED_KEY							= "signedKey";
	public static final String RESULT 								= "result";
	
	public static final String JOB_MODEL_FLASH_DETAIL_ID			= "jobmodelflashdetailsId";
	public static final String MAC_TYPE								= "macType";
	
	public static final String IMAGE_NAME							= "imageName";
	
	public static final String USER_NAME							= "userName";
	public static final String PASSWORD								= "password";
	public static final String EMAIL 								= "email";
	
	public static final String RAW_PAYMENT_ID						= "rpId";
	public static final String OPEN 								= "open";
	
	public static final String CONFIG_NAME					     	= "configFileName";
	public static final String CONFIG_SUFFIX						= ".conf";
	public static final String PHYSICAL_MODE						= "0,0,0,0";
	
	public static final String SHIP_ID								= "shipId";
	public static final String TERM_ID								= "termId";
	
	public static final String TRACK_ID								= "trackId";
	public static final String TRACK_NUMBER							= "trackingNumber";
	
	public static final String CREATED_BY							= "createdBy";
	
	public static final String FORECAST_ID							= "fcastId";
	

}
