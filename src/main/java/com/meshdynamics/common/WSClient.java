package com.meshdynamics.common;


import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import javax.websocket.ClientEndpoint;
import javax.websocket.ClientEndpointConfig;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import org.apache.log4j.Logger;

import com.meshdynamics.common.helper.Helper;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.common.helper.MacAddress;
import com.meshdynamics.dao.JobModelflashdetailsDaoImpl;
import com.meshdynamics.service.JobModelFlashDetailsServiceImpl;

@ClientEndpoint
public class WSClient extends Endpoint {

    
	private static final Logger logger = Logger
			.getLogger(WSClient.class);
	protected  static WebSocketContainer container;
    protected static  Session userSession = null;
//  CountDownLatch  countDownLatch=new  CountDownLatch(1);
    

    public static final String SSL_CONTEXT_PROPERTY ="org.apache.tomcat.websocket.SSL_CONTEXT";
    
    public static Map<String,String>keyMap=new HashMap<String,String>();
    static{
    	
    	 createClientMacAddress();
    }
    public WSClient() {
        container = ContainerProvider.getWebSocketContainer();
    }

    public void Connect() {
    	logger.trace("WSClient.Connect() started");
    	boolean flag=true;
          try {
        	  
        	  if(null!=userSession){
        		  
        		  if(userSession.isOpen()){
        			// 	  flag=false;  
        		  }
        	  }
        	  
        	  if(flag){
         	       
        			logger.info("Before Connection : "+MDProperties.read(Constant.WEB_SOCKET_SERVER_URL));
        		  container.setAsyncSendTimeout(-1);
       		  userSession = container.connectToServer(this, getClientEndpointConfig(),new URI(MDProperties.read(Constant.WEB_SOCKET_SERVER_URL)));
	          // userSession = container.connectToServer(this, new URI(MDProperties.read(Constant.WEB_SOCKET_SERVER_URL)));
	             // countDownLatch.countDown();
       		  Thread.sleep(50);
                 System.out.println("After Connection: "+MDProperties.read(Constant.WEB_SOCKET_SERVER_URL));
	              SendMessage(WSClient.keyMap.get(Constant.CLIENT_MAC).toString());
        	  }
	        	  
             
            } catch (Exception e) {//(NoSuchAlgorithmException |DeploymentException | URISyntaxException | IOException | KeyManagementException e) {// | KeyStoreException | CertificateException e) {
                e.printStackTrace();
            }
          
      	logger.trace("WSClient.Connect() end");
    }
  //  public static class ConnectionClientEndpoint extends Endpoint {
    	 
    public void SendMessage(String sMsg) throws IOException, InterruptedException {
    	logger.trace("WSClient.SendMessage() started");
    	 System.out.println("Before: "+sMsg);
    	userSession.getBasicRemote().sendText(sMsg);
    //	Thread.sleep(1000);
        System.out.println("After:"+sMsg);
        logger.trace("WSClient.SendMessage() end");
    }

    @OnOpen
    public void onOpen(Session session) {
    	logger.trace("WSClient.onOpen() started");
    	logger.info("Connected");
        logger.trace("WSClient.onOpen() end");
    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
    	logger.trace("WSClient.onClose() started");
    	logger.info("Session Closed with reason"+closeReason.getReasonPhrase());
    	logger.trace("WSClient.onClose() end");
    }
//    @OnError
//    public void onError(Session session, Throwable throwable) {
//      	System.out.println("Session Closed with reason 1"+throwable.getMessage());
//      	
//    	System.out.println("Session Closed with reason333"+throwable.getCause());
//        
//    }
    
    
    @OnMessage
    public void onMessage(Session session, String msg, EndpointConfig config) {
    	logger.trace("WSClient.onMessage() started");
    	
    	logger.info("Received message: "+msg);
        messageProcessor(msg);
   
        logger.trace("WSClient.onMessage() end");
       /* if(msg.endsWith(".jtag") || msg.endsWith(".bin")){
        	 System.out.println("File competion  message: "+msg);
        	 JobModelFlashDetailsServiceImpl jobModelFlashDetailsServiceImpl = ApplicationContextProvider.getApplicationContext().getBean("jobModelFlashDetailsServiceImpl", JobModelFlashDetailsServiceImpl.class);
        	
        	 try {
				jobModelFlashDetailsServiceImpl.updateJobModelFlashDetailsByImageName(msg);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }else  if(msg.startsWith("key:")){
        	
        	 System.out.println("Response to connection message: "+msg);
        	String [] keyvalue=  msg.split(":");
        	WSClient.keyMap.put(keyvalue[0], keyvalue[1]);
        	 
        }else{
        	 System.out.println("Invalid message: "+msg);
        }*/
    }

    public void Disconnect() throws IOException {
    	logger.trace("WSClient.Disconnect() started");
        userSession.close();
        logger.trace("WSClient.Disconnect() end");        
    }
    
    private void messageProcessor(String msg) {
    	   if(msg.endsWith(".jtag") || msg.endsWith(".bin")){
          	 System.out.println("File competion  message: "+msg);
          	 JobModelFlashDetailsServiceImpl jobModelFlashDetailsServiceImpl = ApplicationContextProvider.getApplicationContext().getBean("jobModelFlashDetailsServiceImpl", JobModelFlashDetailsServiceImpl.class);
          	
          	 try {
  				jobModelFlashDetailsServiceImpl.updateJobModelFlashDetailsByImageName(msg);
  			} catch (Exception e) {
  				// TODO Auto-generated catch block
  				e.printStackTrace();
  			}
          }else  if(msg.startsWith("key:")){
          	
          	 System.out.println("Response to connection message: "+msg);
          	String [] keyvalue=  msg.split(":");
          	WSClient.keyMap.put(keyvalue[0], keyvalue[1]);
          	 
          }else{
          	 System.out.println("Invalid message: "+msg);
          }

    }
    
    private static void createClientMacAddress(){
    	
		try {
			NetworkInterface networkInterface=null;
			InetAddress inetAddress= null;
			try{
				 inetAddress= InetAddress.getLocalHost();
			}catch(UnknownHostException e) {
				e.printStackTrace();
			}
			
			if(null!=inetAddress){
				networkInterface=NetworkInterface.getByInetAddress(inetAddress);
			}
			if(null==networkInterface){
				networkInterface=NetworkInterface.getNetworkInterfaces().nextElement();
			}
			byte [] bmac=networkInterface.getHardwareAddress();
			MacAddress macAddress=new MacAddress(bmac);
			String strMacAddress=macAddress.toString();
			WSClient.keyMap.put("mac",strMacAddress);	
			
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
    }
    
    @OnError
    public void onError(Throwable  throwable ) {
    	logger.trace("WSClient.onError() started");
        
    	if(null !=throwable) {
    		throwable.printStackTrace();
    		if(null!=userSession) {
    			  logger.info("aaaa Before Connection : "+Constant.WEB_SOCKET_SERVER_URL);
  	            
    			Connect();
    			 logger.info("aaaa After Connection : "+Constant.WEB_SOCKET_SERVER_URL);
  	            
    		}
    	}
    	logger.trace("WSClient.onError() end");
        
    	
    }

	@Override
	public void onOpen(Session session, EndpointConfig config) {
		logger.trace("WSClient.onOpen(Session session, EndpointConfig config) started");
	     
		userSession = session;
		// TODO Auto-generated method stub
		System.out.println("Connected new");
		final String sessionId = session.getId();
		logger.trace("established session with id: " + sessionId);

		// add text based message handler
		session.addMessageHandler(new MessageHandler.Whole<String>() {

			@Override
			public void onMessage(String msg) {
				System.out.println(sessionId + ": text message: " + msg);
			     messageProcessor(msg);
			}
		});

		// add binary based message handler
		session.addMessageHandler(new MessageHandler.Whole<ByteBuffer>() {

			@Override
			public void onMessage(ByteBuffer buffer) {
				System.out.println(sessionId + ": binary message: "
						+ new String(buffer.array()));
			     messageProcessor(new String(buffer.array()));
			}
		});
		logger.trace("WSClient.onOpen(Session session, EndpointConfig config) end");
		 
	}
	

	
	
	private ClientEndpointConfig getClientEndpointConfig() throws KeyManagementException, NoSuchAlgorithmException {
		  ClientEndpointConfig.Configurator configurator = new ClientEndpointConfig.Configurator() ;
		  ClientEndpointConfig clientEndpointConfig = ClientEndpointConfig.Builder.create().configurator(configurator).build();
	      clientEndpointConfig.getUserProperties().put(SSL_CONTEXT_PROPERTY, Helper.getSSLContext()); // replace of context to sslContext	
	      return clientEndpointConfig;
	}
	

}