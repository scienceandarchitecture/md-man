/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : Constant.java
 * Comments : Constant.java 
 * Created  : 27-Jul-2016
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |27-Jul-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.common;

/**
 * Used for constants
 */
public class Constant {
	public static final String DEFAULT_USERNAME = "admin"; //$NON-NLS-1$
	public static final String DEFAULT_PASSWORD = "adminpassword";
	public static final String KEY				= "benisonmdsecurekey";
	public static final String GENERATE_BUILD_URL 	= "generateBuildURL";
	public static final String DOWNLOAD_BUILD_URL 	= "downloadBuildURL";
	public static final String WEB_SOCKET_SERVER_URL	= "webSocketServerURL";
	
	public static final String ADMIN_EMAIL			= "adminEmail";
	public static final String ADMIN_PASSWORD		= "adminPassword";
	public static final String SMTP_AUTHENTICATION	= "mail.smtp.auth";
	public static final String SMTP_ENABLE			= "mail.smtp.starttls.enable";
	public static final String SMTP_HOST			= "mail.smtp.host";
	public static final String SMTP_PORT			= "mail.smtp.port";
	public static final String SMTP_PORT_NO			= "25";
	public static final String DOMAIN_NAME			= "domain";
	
	public static final String 	UI_INVOICE_OPEN				= "open";
	public static final String 	UI_INVOICE_CLOSE			= "closed";
	public static final byte    DB_INVOICE_OPEN				=	0;
	public static final byte    DB_INVOICE_CLOSED			=	1;
	public static final String 	UI_RAW_OPEN 				= 	"OPEN";
	public static final String 	UI_RAW_CLOSED 				= 	"CLOSED";
	public static final String RADIO_FLASH					=	"Radio";
	public static final String VLAN_FLASH					= 	"Vlan";
	public static final String APP_FLASH					= 	"App";
	public static final String STATUS_INPROGRESS			=	"Inprogress";
	public static final String STATUS_INPROGRESS_JOBDETAILS_VALUE		= 	"2";
	
	public static final String STATUS_COMPLETE				=	"Complete";
	public static final String STATUS_COMPLETE_VALUE		= 	"3";
	
	public static final String STATUS_FAILURE				=	"FAILURE";
	public static final String STATUS_FAILURE_VALUE			= 	"0";
	public static final String STATUS_SUCCESS				=	"Success";
	
	public static final String CLIENT_KEY					= 	"key";
	public static final String CLIENT_MAC					= 	"mac";
	
	public static final String STATUS_REGENERATE_BUILD_VALUE= 	"1";
	public static final String IMAGE_TYPE_VALUE = "1";
	public static final String ADDRESS_PER_UNIT				=	"6";
	public static final String MD1_SERIES_ADDRESS_PER_UNIT	= 	"md1";
	public static final String MD4_SERIES_ADDRESS_PER_UNIT	=	"md4";
	public static final String MD6_SERIES_ADDRESS_PER_UNIT	= 	"md6";
	
	public static final String MD1_SERIES					= 	"MD1";
	public static final String MD4_SERIES					= 	"MD4";
	public static final String MD6_SERIES					= 	"MD6";
	
	
	
	//Database related exception responseCode  
	public static int HIBERNATE_EXCEPTION			=	600;
	public static int JDBC_EXCEPTION				=	601;
	public static int NON_UNIQUE_OBJECT_EXCEPTION	=	602;
	public static int NON_UNIQUE_RESULT_EXCEPTION	=	603;
	public static int PERSISTENT_OBJECT_EXCEPTION	=	604;
	public static int TRANSACTION_EXCEPTION			=	605;
	public static int IDENTIFIER_GENERATION_EXCEPTION=	606;
	public static int CONSTRAINT_VIOLATION_EXCEPTION =	607;
	public static int DATA_EXCEPTION				 =	608;
	public static int JDBC_CONNECTION_EXCEPTION		 =	609;
	public static int LOCK_ACQUISITION_EXCEPTION	 = 	610;
	public static int QUERY_TIMEOUT_EXCEPTION		 =	611;
	public static int SQL_GRAMMER_EXCEPTION			 =	612;
	public static int OBJECT_NOT_FOUND_EXCEPTION	 =	613;

	public static int macRegister                    =  614;
	public static int not_macRegister                =  615;
	//Business related exception responseCode
	public static int BUSSINESS_EXCEPTION			=	99;
	
	////System related exception responseCode
	public static int SYSTEM_EXCEPTION				=	100;
	
	// connection TimeOut Exception
	public static final int REQUEST_TIMEOUT_EXCEPTION = 408;
	
	//constants for flashing status time intervals
	public static final String FLASHING_STATUS_INPROGRESS = "flashing.status.inprogress";
	public static final String FLASHING_STATUS_COMPLETE = "flashing.status.complete";

	public static final String DATE_TIME_FORMAT="yyyy-MM-dd HH:mm:ss";
	public static final String DATE_FORMAT="yyyy-MM-dd";

}
