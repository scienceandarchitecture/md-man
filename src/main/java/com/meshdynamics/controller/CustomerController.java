/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : CustomerController.java
 * Comments : CustomerController.java is used to handle all the requests and responses which is related to customers. 
 * Created  : 23-Sep-2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |23-Sep-2016  | Created                             | Pinki		  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.ResponseEntity;
import com.meshdynamics.common.RestConstants;
import com.meshdynamics.common.RestURIConstants;
import com.meshdynamics.common.Status;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.service.CustomerServiceImpl;
import com.meshdynamics.vo.CustomerVo;

/********************* Add Customers **********************/
@Controller
@RequestMapping("/customers")
public class CustomerController {

	private static final Logger logger = Logger
			.getLogger(CustomerController.class);

	@Autowired
	CustomerServiceImpl customerServiceImpl;

	@RequestMapping(value = RestURIConstants.ADD_CUSTOMERS, method = RequestMethod.POST, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity addCustomers(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody CustomerVo customerVo) {

		logger.info("CustomerController.addCustomer() started");
		ResponseEntity response = new ResponseEntity();
		boolean flg = false;
		try {
			flg = customerServiceImpl.addCustomers(customerVo);
			if (flg) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.CUSTOMER_NAME) 
						+" '"+customerVo.getName()+"' "+MDProperties.read(MDMessageConstant.CUSTOMER_ADD_SUCCESS));
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("CustomerController.addCustomer() end");
		return response;
	}

	/********************* Fetch Customer List **************************/
	@RequestMapping(value = RestURIConstants.GET_CUSTOMER_LIST, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getCustomerList(
			@RequestHeader(value = "Authentication") String authentication) {

		logger.info("CustomerController.getCustomerList() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<CustomerVo> customerList = customerServiceImpl
					.getCustomerList();
			if (null != customerList && customerList.size() > 0) {
				response.setData(customerList);
				response.setMessage(MDProperties.read(MDMessageConstant.CUSTOMERS_GET_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("CustomController.getCustomerList() end");
		return response;
	}

	/********************** Edit Customer Record ***********************/

	@RequestMapping(value = RestURIConstants.EDIT_CUSTOMER_RECORD, method = RequestMethod.PUT, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity updateCustomer(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody CustomerVo customerVo) {

		logger.info("CustomerController.updateCustomer() started");
		ResponseEntity response = new ResponseEntity();
		boolean flg = false;
		try {
			flg = customerServiceImpl.updateCustomerDetails(customerVo);
			if (flg) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.CUSTOMER_UPDATE_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			} else {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.CUSTOMER_UPDATE_FAILURE));
				response.setResponseCode(RestConstants.NOT_MODIFIED);
				response.setStatus(Status.Failure.getValue());
			}
		} catch (Exception e) {
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(RestConstants.NOT_MODIFIED);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("CustomerController.updateCustomer() end");
		return response;
	}

	/************************* Delete Customer Record By customerId ***********************/

	@RequestMapping(value = RestURIConstants.DELETE_CUSTOMER_BY_ID, method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity DeleteCustomerById(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("custId") int custId) {

		logger.info("CustomerController.deleteCustomerById() started");
		ResponseEntity response = new ResponseEntity();
		try {
			boolean flg = customerServiceImpl.deleteCustomer(custId);
			if (flg) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.CUSTOMER_DELETE_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mde) {
			logger.error(mde.getMessage());
			response.setData("");
			response.setMessage(mde.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mdbe) {
			logger.error(mdbe.getMessage());
			response.setData("");
			response.setMessage("" + mdbe.getMessage());
			response.setResponseCode(mdbe.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("CustomerController.deleteCustomerById() end");
		return response;
	}

	/************************* Get Customer Record By customerId ***********************/

	@RequestMapping(value = RestURIConstants.GET_CUSTOMER_BY_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getCustomerById(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("custId") int custId) {

		logger.info("CustomerController.getCustomerById() started");
		ResponseEntity response = new ResponseEntity();
		try {
			CustomerVo customerVo = customerServiceImpl.getCustomerById(custId);
			response.setData(customerVo);
			response.setMessage(MDProperties.read(MDMessageConstant.CUSTOMER_GET_SUCCESS));
			response.setResponseCode(RestConstants.OK);
			response.setStatus(Status.Success.getValue());
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mde) {
			logger.error(mde.getMessage());
			response.setData("");
			response.setMessage(mde.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mdbe) {
			logger.error(mdbe.getMessage());
			response.setData("");
			response.setMessage("" + mdbe.getMessage());
			response.setResponseCode(mdbe.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("CustomerController.getCustomerById() end");
		return response;
	}
	
	@RequestMapping(value = RestURIConstants.GET_CUSTOMER_BY_NAME, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getCustomerByName(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("name") String name) {

		logger.info("CustomerController.getCustomerByName() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<CustomerVo> customerVoList = customerServiceImpl.getCustomerByName(name);
			response.setData(customerVoList);
			response.setMessage(MDProperties.read(MDMessageConstant.CUSTOMER_GET_SUCCESS));
			response.setResponseCode(RestConstants.OK);
			response.setStatus(Status.Success.getValue());
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mde) {
			logger.error(mde.getMessage());
			response.setData("");
			response.setMessage(mde.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mdbe) {
			logger.error(mdbe.getMessage());
			response.setData("");
			response.setMessage("" + mdbe.getMessage());
			response.setResponseCode(mdbe.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("CustomerController.getCustomerByName() end");
		return response;
	}
}
