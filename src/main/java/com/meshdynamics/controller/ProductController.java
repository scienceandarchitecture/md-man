/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ProductController.java
 * Comments : TODO 
 * Created  : Sep 28, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |    Comment                       |     Author    |
 * ---------------------------------------------------------------------------
 * |  0  |Sep 28, 2016 |    Created                    	  |      Pinki	  |  
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.ResponseEntity;
import com.meshdynamics.common.RestConstants;
import com.meshdynamics.common.RestURIConstants;
import com.meshdynamics.common.Status;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.service.ProductServiceImpl;
import com.meshdynamics.vo.ProductVo;

@Controller
@RequestMapping("/products")
public class ProductController {

	private static final Logger logger = Logger
			.getLogger(ProductController.class);

	@Autowired(required = true)
	ProductServiceImpl productServiceImpl;

	@RequestMapping(value = RestURIConstants.ADD_PRODUCT, method = RequestMethod.POST, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity addProduct(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody final ProductVo productVo) {

		logger.info("ProductController.addProduct() started");
		ResponseEntity response = new ResponseEntity();
		boolean flg = false;
		try {
			flg = productServiceImpl.addProduct(productVo);
			if (flg) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.PRODUCT_ADD_SUCCESS));
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getMessage());
			response.setData("");
			response.setMessage("" + mde.getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("ProductController.addProduct() end");
		return response;
	}

	@RequestMapping(value = RestURIConstants.GET_PRODUCT, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getProducts(
			@RequestHeader(value = "Authentication") String authentication) {

		logger.info("ProductController.getProducts() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<ProductVo> productList = productServiceImpl.getProducts();
			if (productList != null && productList.size() > 0) {
				response.setData(productList);
				response.setMessage(MDProperties.read(MDMessageConstant.GET_PRODUCTS_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("ProductController.getProducts() end");
		return response;
	}

	/************************* Delete Product Record ***********************/

	@RequestMapping(value = RestURIConstants.DELETE_PRODUCT_BY_ID, method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity DeleteProduct(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("psku") String psku) {

		logger.info("ProductController.deleteProductById() started");
		ResponseEntity response = new ResponseEntity();
		boolean flag = false;
		try {
			flag = productServiceImpl.deleteProduct(psku);
			if (flag) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.PRODUCT_NAME)+" '"+
						psku+"' "+MDProperties.read(MDMessageConstant.DELETE_PRODUCT_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("ProductController.deleteProductById() end");
		return response;
	}

	/************************************* Get Product ById ***************************/

	@RequestMapping(value = RestURIConstants.GET_PRODUCT_BY_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getProductById(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("psku") String psku) {

		logger.info("ProductController.getProductById() started");
		ResponseEntity response = new ResponseEntity();
		try {
			ProductVo productVo = productServiceImpl.getProductById(psku);
			if (null != productVo) {
				response.setData(productVo);
				response.setMessage(MDProperties.read(MDMessageConstant.GET_PRODUCT_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("ProductController.getProductById() end");
		return response;
	}

	/******************************* UpdateProduct ****************************/

	@RequestMapping(value = RestURIConstants.UPDATE_PRODUCT, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity updateProduct(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody ProductVo productVo) {

		logger.info("ProductController.updateProduct() started");
		ResponseEntity response = new ResponseEntity();
		boolean flg = false;
		try {
			flg = productServiceImpl.updateProduct(productVo);
			if (flg) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.PRODUCT_NAME)+" '"+
						productVo.getPsku()+"' "+ MDProperties.read(MDMessageConstant.UPDATE_PRODUCT_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());

		} catch (Exception e) {
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("ProductController.updateProduct() end");
		return response;
	}
	
	@RequestMapping(value = RestURIConstants.GET_PRODUCT_LIST_USING_PAGINATION, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity getProductListUsingPagination(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody String criteria,
			@RequestParam int start, int noOfRecords) {

		logger.info("ProductController.getProductListUsingPagination() started");
		ResponseEntity responseEntity = new ResponseEntity();
		try {
			List<ProductVo> productList = productServiceImpl
					.getProductListUsingPagination(criteria,start, noOfRecords);
			int count = productServiceImpl.getCount(criteria);
			if (null != productList && count > 0) {
				responseEntity.setCount(count);
				responseEntity.setData(productList);
				responseEntity.setMessage(MDProperties
						.read(MDMessageConstant.PRODUCT_GET_SUCCESS));
				responseEntity.setResponseCode(RestConstants.OK);
				responseEntity.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(nre.getMessage());
			responseEntity.setResponseCode(RestConstants.NO_RECORDS);
			responseEntity.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(mbe.getMessage());
			responseEntity.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			responseEntity.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(e.getMessage());
			responseEntity.setResponseCode(Constant.SYSTEM_EXCEPTION);
			responseEntity.setStatus(Status.Failure.getValue());
		}
		logger.info("PayMentController.getProductListUsingPagination() end");
		return responseEntity;
	}
	
	@RequestMapping(value = RestURIConstants.GET_PRODUCT_LIST_BY_PRODUCT_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getProductListByProductId(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("psku") String psku) {
		logger.info("PayMentController.getProductListByProductId() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<?> productListVoList = productServiceImpl
					.getProductListByProductId(psku);
			int count = productListVoList.size();
			response.setData(productListVoList);
			response.setCount(count);
			response.setMessage(MDProperties
					.read(MDMessageConstant.GET_PRODUCTS_SUCCESS));
			response.setResponseCode(RestConstants.OK);
			response.setStatus(Status.Success.getValue());
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mdbe) {
			logger.error(mdbe.getMessage());
			response.setData("");
			response.setMessage("" + mdbe.getMessage());
			response.setResponseCode(mdbe.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("PayMentController.getProductListByProductId() end");
		return response;
	}
}