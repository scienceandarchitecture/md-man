package com.meshdynamics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.ResponseEntity;
import com.meshdynamics.common.RestConstants;
import com.meshdynamics.common.RestURIConstants;
import com.meshdynamics.common.Status;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.service.MacAddressServiceImpl;
import com.meshdynamics.vo.MacAddressSummaryVo;
import com.meshdynamics.vo.MacDetailsMetaResultVo;
import com.meshdynamics.vo.MacRangeVo;

@Controller
@RequestMapping("/macaddress")
public class MacAddressController {

		private static final Logger logger = Logger
				.getLogger(MacAddressController.class);
		
		@Autowired
		MacAddressServiceImpl macAddressServiceImpl;
		
		/********************* Fetch MacAddress List **************************/
		@RequestMapping(value = RestURIConstants.GET_MAC_ADDRESS_SUMMARY, method = RequestMethod.GET)
		public @ResponseBody ResponseEntity getMacAddressSummaryList(
				@RequestHeader(value = "Authentication") String authentication) {

			logger.info("MacAddressController.getMacAddressSummaryList() started");
			ResponseEntity response = new ResponseEntity();
			try {
				List<MacAddressSummaryVo> macAddressSummaryList = macAddressServiceImpl.
						getMacAddressSummaryList();
				if (macAddressSummaryList != null && macAddressSummaryList.size() > 0) {
					response.setData(macAddressSummaryList);
					response.setMessage(MDMessageConstant.GET_MAC_ADDRESS);
					response.setResponseCode(RestConstants.OK);
					response.setStatus(Status.Success.getValue());
				}
			} catch (MDBusinessException mbe) {
				logger.error(mbe.getMessage());
				response.setData("");
				response.setMessage(mbe.getMessage());
				response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
				response.setStatus(Status.Failure.getValue());
			} catch (Exception e) {
				logger.error(e.getMessage());
				response.setData("");
				response.setMessage(e.getMessage());
				response.setResponseCode(Constant.SYSTEM_EXCEPTION);
				response.setStatus(Status.Failure.getValue());
			}
			logger.info("MacAddressController.getMacAddressSummaryList() end");
			return response;
		}


		@RequestMapping(value = RestURIConstants.VIEW_MAC_RANGE, method = RequestMethod.POST, consumes = RestConstants.MEDIA_TYPE)
		public @ResponseBody ResponseEntity macRangeDetails(
				@RequestHeader(value = "Authentication") String authentication,
				@RequestBody MacRangeVo macRangeVo,
				@RequestParam int start, int noOfRecords) {
			logger.info("MacRangeController.macRangeDetails() started");
			ResponseEntity response = new ResponseEntity();
			try {
				MacDetailsMetaResultVo macAddressSummaryList = macAddressServiceImpl.fetchMacRangeDetails(macRangeVo,start,noOfRecords);
				int count = macAddressServiceImpl.getCount(macRangeVo);
				response.setData(macAddressSummaryList);
				response.setCount(count);
				response.setMessage(MDMessageConstant.VIEW_MAC_ADDRESS);
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			} catch (MDBusinessException mbe) {
				logger.error(mbe.getMessage());
				response.setData("");
				response.setMessage(mbe.getMessage());
				response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
				response.setStatus(Status.Failure.getValue());
			} catch (Exception e) {
				logger.error(e.getMessage());
				response.setData("");
				response.setMessage(e.getMessage());
				response.setResponseCode(Constant.SYSTEM_EXCEPTION);
				response.setStatus(Status.Failure.getValue());
			}
			logger.info("MacRangeController.macRangeDetails() end");
			return response;
		}
		
		@RequestMapping(value = RestURIConstants.GET_MAC_ADDRESS_LIST_USING_PAGINATION, method = RequestMethod.POST)
		public @ResponseBody ResponseEntity getMacAddressSummaryListUsingPagination(
				@RequestHeader(value = "Authentication") String authentication,
				@RequestBody String criteria,
				@RequestParam int start, int noOfRecords) {

			logger.info("MacAddressController.getMacAddressSummaryListUsingPagination() started");
			ResponseEntity response = new ResponseEntity();
			try {
				List<MacAddressSummaryVo> macAddressSummaryList = macAddressServiceImpl.
						getMacAddressListUsingPagination(criteria,start,noOfRecords);
				int count = macAddressServiceImpl.getCount(criteria);
				if (macAddressSummaryList != null && count > 0) {
					response.setCount(count);
					response.setData(macAddressSummaryList);
					response.setMessage(MDMessageConstant.GET_MAC_ADDRESS);
					response.setResponseCode(RestConstants.OK);
					response.setStatus(Status.Success.getValue());
				}
			} catch (MDBusinessException mbe) {
				logger.error(mbe.getMessage());
				response.setData("");
				response.setMessage(mbe.getMessage());
				response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
				response.setStatus(Status.Failure.getValue());
			} catch (Exception e) {
				logger.error(e.getMessage());
				response.setData("");
				response.setMessage(e.getMessage());
				response.setResponseCode(Constant.SYSTEM_EXCEPTION);
				response.setStatus(Status.Failure.getValue());
			}
			logger.info("MacAddressController.getMacAddressSummaryListUsingPagination() end");
			return response;
		}
		
		@RequestMapping(value = RestURIConstants.GET_MAC_ADDRESS_LIST_BASED_SEARCH_CRITERIA, method = RequestMethod.POST)
		public @ResponseBody ResponseEntity getMacAddressListBasedSearchCritera(
				@RequestHeader(value = "Authentication") String authentication,
				@RequestBody String criteria,
				@RequestParam int start, int noOfRecords) {

			logger.info("MacAddressController.getMacAddressListBasedSearchCritera() started");
			ResponseEntity response = new ResponseEntity();
			try {
				List<MacAddressSummaryVo> macAddressSummaryList = macAddressServiceImpl
						.getMacAddressListBasedSearchCritera(criteria,start,noOfRecords);
				if (macAddressSummaryList != null) {
					response.setData(macAddressSummaryList);
					response.setMessage(MDMessageConstant.GET_MAC_ADDRESS);
					response.setResponseCode(RestConstants.OK);
					response.setStatus(Status.Success.getValue());
				}
			} catch (MDDBException mdbe) {
				logger.error(mdbe.getMessage());
				response.setData("");
				response.setMessage("" + mdbe.getMessage());
				response.setResponseCode(mdbe.getCode());
				response.setStatus(Status.Failure.getValue());
			} catch (MDBusinessException mbe) {
				logger.error(mbe.getMessage());
				response.setData("");
				response.setMessage(mbe.getMessage());
				response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
				response.setStatus(Status.Failure.getValue());
			} catch (Exception e) {
				logger.error(e.getMessage());
				response.setData("");
				response.setMessage(e.getMessage());
				response.setResponseCode(Constant.SYSTEM_EXCEPTION);
				response.setStatus(Status.Failure.getValue());
			}
			logger.info("MacAddressController.getMacAddressListBasedSearchCritera() end");
			return response;
		}
}
