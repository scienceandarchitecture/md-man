/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : PayMentController.java
 * Comments : TODO 
 * Created  : 03-Oct-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |03-Oct-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.controller;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.ResponseEntity;
import com.meshdynamics.common.RestConstants;
import com.meshdynamics.common.RestURIConstants;
import com.meshdynamics.common.Status;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.service.PaymentServiceImpl;
import com.meshdynamics.vo.InvoicePaymentVo;
import com.meshdynamics.vo.RawPaymentVo;
import com.meshdynamics.vo.RevenueListVo;
import com.meshdynamics.vo.RevenueVo;

@Controller
@RequestMapping("/payment")
public class PayMentController {
	private static final Logger logger = Logger
			.getLogger(PayMentController.class);

	@Autowired
	PaymentServiceImpl paymentServiceImpl;

	@RequestMapping(value = RestURIConstants.ADD_RAWPAYMENT, method = RequestMethod.POST, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity addRawPayment(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody RawPaymentVo rawPaymentVo) {

		logger.info("PayMentController.addRawPayment() started");
		ResponseEntity response = new ResponseEntity();
		boolean flg = false;
		try {
			flg = paymentServiceImpl.addRawPayment(rawPaymentVo);
			if (flg) {
				response.setData("");
				response.setMessage(MDProperties
						.read(MDMessageConstant.RAW_PAYMENT_ADD_SUCCESS));
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("PayMentController.addRawPayment() end");
		return response;
	}

	/*************************** getrawPayment **********************************/

	@RequestMapping(value = RestURIConstants.GET_RAW_PAYMENT_LIST, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getRawPaymentList(
			@RequestHeader(value = "Authentication") String authentication) {

		logger.info("PayMentController.getRawPaymentList started");
		ResponseEntity responseEntity = new ResponseEntity();
		try {
			List<RawPaymentVo> rawPaymentList = paymentServiceImpl
					.getRawPayment();
			if (null != rawPaymentList && rawPaymentList.size() > 0) {
				responseEntity.setData(rawPaymentList);
				responseEntity.setMessage(MDProperties
						.read(MDMessageConstant.RAW_PAYMENTS_GET_SUCCESS));
				responseEntity.setResponseCode(RestConstants.OK);
				responseEntity.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(mde.getCause().getMessage());
			responseEntity.setResponseCode(mde.getCode());
			responseEntity.setStatus(Status.Failure.getValue());

		} catch (MDNoRecordsFoundException nrfe) {
			logger.error(nrfe.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(nrfe.getMessage());
			responseEntity.setResponseCode(RestConstants.NO_RECORDS);
			responseEntity.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(e.getMessage());
			responseEntity.setResponseCode(Constant.SYSTEM_EXCEPTION);
			responseEntity.setStatus(Status.Failure.getValue());
		}
		logger.info("PayMentController.getRawPaymentList end");
		return responseEntity;
	}

	/************************** getRawPaymentById *******************/

	@RequestMapping(value = RestURIConstants.GET_RAW_PAYMENT_BY_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getRawPaymentById(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("rpId") int rpId) {

		logger.info("PayMentController.getRawPaymentById() started");
		ResponseEntity responseEntity = new ResponseEntity();
		try {
			RawPaymentVo rawPaymentVo = paymentServiceImpl
					.getRawPaymentById(rpId);
			if (null != rawPaymentVo) {
				responseEntity.setData(rawPaymentVo);
				responseEntity.setMessage(MDProperties
						.read(MDMessageConstant.RAW_PAYMENT_GET_SUCCESS));
				responseEntity.setResponseCode(RestConstants.OK);
				responseEntity.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(nre.getMessage());
			responseEntity.setResponseCode(RestConstants.NO_RECORDS);
			responseEntity.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(mde.getCause().getMessage());
			responseEntity.setResponseCode(mde.getCode());
			responseEntity.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(e.getMessage());
			responseEntity.setResponseCode(Constant.SYSTEM_EXCEPTION);
			responseEntity.setStatus(Status.Failure.getValue());
		}
		logger.info("PayMentController.getRawPaymentById() end");
		return responseEntity;
	}

	/************************************** UpdateRawPayment ************************************/

	@RequestMapping(value = RestURIConstants.UPDATE_RAW_PAYMENT, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity updateRawPayment(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody RawPaymentVo rawPaymentVo) {

		logger.info("PayMentController.updateRawPayment() started");
		ResponseEntity responseEntity = new ResponseEntity();
		try {
			boolean flag = paymentServiceImpl.updateRawPayment(rawPaymentVo);
			if (flag) {
				responseEntity.setData(rawPaymentVo);
				responseEntity
						.setMessage(MDProperties
								.read(MDMessageConstant.RAW_PAYMENT_NAME)
								+ " '"
								+ rawPaymentVo.getRpId()
								+ "' "
								+ MDProperties
										.read(MDMessageConstant.RAW_PAYMENT_UPDATE_SUCCESS));
				responseEntity.setResponseCode(RestConstants.OK);
				responseEntity.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(mde.getCause().getMessage());
			responseEntity.setResponseCode(mde.getCode());
			responseEntity.setStatus(Status.Failure.getValue());
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(nre.getMessage());
			responseEntity.setResponseCode(RestConstants.NO_RECORDS);
			responseEntity.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(e.getMessage());
			responseEntity.setResponseCode(Constant.SYSTEM_EXCEPTION);
			responseEntity.setStatus(Status.Failure.getValue());
		}
		logger.info("PayMentController.updateRawPayment() end");
		return responseEntity;
	}

	/************************************* DeleteRawPaymentById ******************/

	@RequestMapping(value = RestURIConstants.DELETE_RAW_PAYMENT_BY_ID, method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity deleteRawPaymentById(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("rpId") int rpId) {

		logger.info("PayMentController.deleteRawPaymentById() started");
		ResponseEntity response = new ResponseEntity();
		try {
			paymentServiceImpl.deleteRawPaymentById(rpId);
			response.setData("");
			response.setMessage(MDProperties
					.read(MDMessageConstant.RAW_PAYMENT_NAME)
					+ " '"
					+ rpId
					+ "' "
					+ MDProperties
							.read(MDMessageConstant.RAW_PAYMENT_DELETE_SUCCESS));
			response.setResponseCode(RestConstants.OK);
			response.setStatus(Status.Success.getValue());
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("PayMentController.deleteRawPaymentById() end");
		return response;
	}

	/*********************************** GetOpenRawPaymentList *********************************/

	@RequestMapping(value = RestURIConstants.GET_OPEN_RAW_PAYMENT_LIST, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getOpenRawPaymentList(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("custId") int custId) {

		logger.info("PayMentController.getOpenRawPaymentList started");
		ResponseEntity responseEntity = new ResponseEntity();
		try {
			List<RawPaymentVo> rawPaymentList = paymentServiceImpl
					.getOpenRawPayment(custId);
			if (rawPaymentList != null && rawPaymentList.size() > 0) {
				responseEntity.setData(rawPaymentList);
				responseEntity
						.setMessage(MDProperties
								.read(MDMessageConstant.GET_OPEN_RAWPAYMENT_LIST_SUCCESS));
				responseEntity.setResponseCode(RestConstants.OK);
				responseEntity.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(mde.getCause().getMessage());
			responseEntity.setResponseCode(mde.getCode());
			responseEntity.setStatus(Status.Failure.getValue());

		} catch (MDNoRecordsFoundException nrfe) {
			logger.error(nrfe.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(nrfe.getMessage());
			responseEntity.setResponseCode(RestConstants.NO_RECORDS);
			responseEntity.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(e.getMessage());
			responseEntity.setResponseCode(Constant.SYSTEM_EXCEPTION);
			responseEntity.setStatus(Status.Failure.getValue());
		}
		logger.info("PayMentController.getOpenRawPaymentList end");
		return responseEntity;
	}

	/************************************ Add Invoice **********************************/

	@RequestMapping(value = RestURIConstants.ADD_INVOICE_PAYMENT, method = RequestMethod.POST, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity addInvoicePayment(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody InvoicePaymentVo invoicePaymentVo) {

		logger.info("PayMentController.addInvoicePayment() started");
		ResponseEntity response = new ResponseEntity();
		boolean flg = false;
		try {
			flg = paymentServiceImpl.addInvoicePayment(invoicePaymentVo);
			if (flg) {
				response.setData("");
				response.setMessage(MDProperties
						.read(MDMessageConstant.PO_PAYMENT_ADD_SUCCESS)
						+ " '"
						+ invoicePaymentVo.getInvId()
						+ "' "
						+ MDProperties
								.read(MDMessageConstant.PO_PAYMENT_SUCCESS));
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());

		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("PayMentController.addInvoicePayment() end");
		return response;
	}

	/******************************************* getInvoicePaymentList *********************************/

	@RequestMapping(value = RestURIConstants.GET_INVOICE_PAYMENT_LIST, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getInvoicePaymentList(
			@RequestHeader(value = "Authentication") String authentication) {

		logger.info("PayMentController.getInvoicePaymentList() started");
		ResponseEntity responseEntity = new ResponseEntity();
		try {
			List<InvoicePaymentVo> invoicePaymentList = paymentServiceImpl
					.getInvoicePaymentList();
			if (invoicePaymentList != null && invoicePaymentList.size() > 0) {
				responseEntity.setData(invoicePaymentList);
				responseEntity.setMessage(MDProperties
						.read(MDMessageConstant.PO_PAYMNETS_GET_SUCCESS));
				responseEntity.setResponseCode(RestConstants.OK);
				responseEntity.setStatus(Status.Success.getValue());
			} else {
				responseEntity.setData("");
				responseEntity.setMessage(MDProperties
						.read(MDMessageConstant.PO_FETCHING_SATAUS));
				responseEntity.setResponseCode(Constant.BUSSINESS_EXCEPTION);
				responseEntity.setStatus(Status.Failure.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(mde.getCause().getMessage());
			responseEntity.setResponseCode(mde.getCode());
			responseEntity.setStatus(Status.Failure.getValue());

		} catch (MDNoRecordsFoundException nrfe) {
			logger.error(nrfe.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(nrfe.getMessage());
			responseEntity.setResponseCode(RestConstants.NO_RECORDS);
			responseEntity.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(e.getMessage());
			responseEntity.setResponseCode(Constant.SYSTEM_EXCEPTION);
			responseEntity.setStatus(Status.Failure.getValue());
		}
		logger.info("PayMentController.getInvoicePaymentList() end");
		return responseEntity;
	}

	/*********************************** getInvoicePaymentByID *************************/
	@RequestMapping(value = RestURIConstants.GET_INVOICE_PAYMENT_BY_INVOICE_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getInvoicePaymentByInvoiceId(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("invId") int invId) {

		logger.info("PayMentController.getInvoicePaymentByInvoiceId() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<InvoicePaymentVo> invoicePaymentVo = paymentServiceImpl
					.getInvoicePaymentByInvoiceId(invId);
			if (invoicePaymentVo != null) {
				response.setData(invoicePaymentVo);
				response.setMessage(MDProperties
						.read(MDMessageConstant.PO_PAYMENT_GET_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("PayMentController.getInvoicePaymentByInvoiceId() end");
		return response;
	}

	/****************************************** UpdateInvoicePayment ****************************/

	@RequestMapping(value = RestURIConstants.UPDATE_INVOICE_PAYMENT, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity updateInvoicePayment(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody InvoicePaymentVo invoicePaymentVo) {

		logger.info("PayMentController.updateInvoicePayment() started");
		ResponseEntity responseEntity = new ResponseEntity();
		try {
			boolean flag = paymentServiceImpl
					.updateInvoicePayment(invoicePaymentVo);
			if (flag) {
				responseEntity.setData(" ");
				responseEntity
						.setMessage(MDProperties
								.read(MDMessageConstant.PO_PAYMENT_NAME)
								+ " '"
								+ invoicePaymentVo.getInvId()
								+ "' "
								+ MDProperties
										.read(MDMessageConstant.PO_PAYMENT_UPDATE_SUCCESS));
				responseEntity.setResponseCode(RestConstants.OK);
				responseEntity.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(mde.getCause().getMessage());
			responseEntity.setResponseCode(mde.getCode());
			responseEntity.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(e.getMessage());
			responseEntity.setResponseCode(Constant.SYSTEM_EXCEPTION);
			responseEntity.setStatus(Status.Failure.getValue());
		}
		logger.info("PayMentController.updateInvoicePayment() end");
		return responseEntity;
	}

	/********************************** DeleteInvoicePaymentById ****************************/

	@RequestMapping(value = RestURIConstants.DELETE_INVOICE_PAYMENT_BY_INVOICE_ID, method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity deleteInvoicePaymentByInvoiceId(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("invId") int invId) {

		logger.info("PayMentController.deleteInvoicePaymentByInvoiceId() started");
		ResponseEntity response = new ResponseEntity();
		try {
			boolean flg = paymentServiceImpl
					.deleteInvoicePaymentByInvoiceId(invId);
			if (flg) {
				response.setData("");
				response.setMessage(MDProperties
						.read(MDMessageConstant.PO_PAYMENT_NAME)
						+ " '"
						+ invId
						+ "' "
						+ MDProperties
								.read(MDMessageConstant.PO_PAYMENT_DELETE_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage("" + mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("PayMentController.deleteInvoicePaymentByInvoiceId() end");
		return response;
	}

	/************************************* GetPaymentCountByInvoiceId **********************************/

	@RequestMapping(value = RestURIConstants.GET_PAYMENT_COUNT_BY_INVOICE_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getPaymentCountByInvoiceId(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("invId") int invId) {

		logger.info("PayMentController.getPaymentCountByInvoiceId() started");
		ResponseEntity response = new ResponseEntity();
		try {
			Map<String, Integer> paymentCount = paymentServiceImpl
					.getPaymentCountByInvoiceId(invId);
			response.setData(paymentCount);
			response.setMessage(MDProperties
					.read(MDMessageConstant.PO_PAYMENT_COUNT_BY_INVOICE_ID_SUCCESS));
			response.setResponseCode(RestConstants.OK);
			response.setStatus(Status.Success.getValue());
		} catch (MDNoRecordsFoundException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("PayMentController.getPaymentCountByInvoiceId() started");
		return response;
	}

	/********************* Fetch Revenue List **************************/
	@RequestMapping(value = RestURIConstants.GET_REVENUE_LIST, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getRevenueList(
			@RequestHeader(value = "Authentication") String authentication) {

		logger.info("PayMentController.getRevenueList() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<RevenueVo> revenueList = paymentServiceImpl.getRevenueList();
			if (revenueList != null && revenueList.size() > 0) {
				response.setData(revenueList);
				response.setMessage(MDProperties
						.read(MDMessageConstant.REVENUE_LIST_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("PayMentController.getRevenueList() end");
		return response;
	}
	
	/***************************** GetRevenueList Using pagination *****************************/
	
	@RequestMapping(value = RestURIConstants.GET_REVENUE_LIST_USING_PAGINATION, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity getRevenueListUsingPagination(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody String criteria, 
			@RequestParam int start,int noOfRecords,int revenueDateFlag) {

		logger.info("PayMentController.getRevenueListUsingPagination() started");
		ResponseEntity response = new ResponseEntity();
		RevenueListVo revenueListVo = new RevenueListVo();
		try {
			List<RevenueVo> revenueList = paymentServiceImpl
					.getRevenueListUsingPagination(revenueDateFlag, criteria,
							start, noOfRecords);
			List<RevenueVo> revenueAmountList = paymentServiceImpl.getRevenueList();
			int count = revenueAmountList.size();
			double totalAmount = revenueAmountList.get(0).getTotalAmount();
			revenueListVo.setAmount(totalAmount);
			revenueListVo.setRevenueVoList(revenueList);

			if (revenueList != null && revenueList.size() > 0) {
				response.setCount(count);
				response.setData(revenueListVo);
				response.setMessage(MDProperties
						.read(MDMessageConstant.REVENUE_LIST_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("PayMentController.getRevenueListUsingPagination() end");
		return response;
	}
	
	@RequestMapping(value = RestURIConstants.GET_PAYMENT_LIST_USING_PAGINATION, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity getPaymentListUsingPagination(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody String criteria, @RequestParam int start,
			int noOfRecords) {

		logger.info("PayMentController.getPaymentListUsingPagination() started");
		ResponseEntity responseEntity = new ResponseEntity();
		try {
			List<RawPaymentVo> invoicePaymentList = paymentServiceImpl
					.getPaymentListUsingPagination(criteria, start, noOfRecords);
			int count = paymentServiceImpl.getCount(criteria);
			if (invoicePaymentList != null && count > 0) {
				responseEntity.setCount(count);
				responseEntity.setData(invoicePaymentList);
				responseEntity.setMessage(MDProperties
						.read(MDMessageConstant.PAYMNETS_PAGINATION_GET_SUCCESS));
				responseEntity.setResponseCode(RestConstants.OK);
				responseEntity.setStatus(Status.Success.getValue());
			} else {
				responseEntity.setData("");
				responseEntity.setMessage(MDProperties
						.read(MDMessageConstant.PO_FETCHING_SATAUS));
				responseEntity.setResponseCode(Constant.BUSSINESS_EXCEPTION);
				responseEntity.setStatus(Status.Failure.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(mde.getCause().getMessage());
			responseEntity.setResponseCode(mde.getCode());
			responseEntity.setStatus(Status.Failure.getValue());
		} catch (MDNoRecordsFoundException nrfe) {
			logger.error(nrfe.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(nrfe.getMessage());
			responseEntity.setResponseCode(RestConstants.NO_RECORDS);
			responseEntity.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(e.getMessage());
			responseEntity.setResponseCode(Constant.SYSTEM_EXCEPTION);
			responseEntity.setStatus(Status.Failure.getValue());
		}
		logger.info("PayMentController.getPaymentListUsingPagination() end");
		return responseEntity;
	}
}
