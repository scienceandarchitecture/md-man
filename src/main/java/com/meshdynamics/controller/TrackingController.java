package com.meshdynamics.controller;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.ResponseEntity;
import com.meshdynamics.common.RestConstants;
import com.meshdynamics.common.RestURIConstants;
import com.meshdynamics.common.Status;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.service.TrackingServiceImpl;
import com.meshdynamics.vo.InvoiceTrackingVo;

@Controller
@RequestMapping("/tracking")
public class TrackingController {

	private static final Logger logger = Logger
			.getLogger(TrackingController.class);

	@Autowired(required = true)
	TrackingServiceImpl trackingServiceImpl;

	@RequestMapping(value = RestURIConstants.ADD_TRACKING, method = RequestMethod.POST, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity addTracking(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody InvoiceTrackingVo invoiceTrackingVo) {

		logger.info("TrackingController.addTracking() started");
		ResponseEntity response = new ResponseEntity();
		boolean flg = false;
		try {
			flg = trackingServiceImpl.addTracking(invoiceTrackingVo);
			if (flg) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.TRACKING)+" '"+
						invoiceTrackingVo.getShipId()+"' "+MDProperties.read(MDMessageConstant.TRACKING_ADD_SUCCESS));
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("TrackingController.addTracking() end");
		return response;
	}

	@RequestMapping(value = RestURIConstants.GET_TRACKING_LIST, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getTrackingList(
			@RequestHeader(value = "Authentication") String authentication) {

		logger.info("TrackingController.getTrackingList() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<InvoiceTrackingVo> trackingList = trackingServiceImpl
					.getTrackingList();
			if (null != trackingList && trackingList.size() > 0) {
				response.setData(trackingList);
				response.setMessage(MDProperties.read(MDMessageConstant.GET_TRACKING_LIST_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("TrackingController.getTrackingList() end");
		return response;
	}

	@RequestMapping(value = RestURIConstants.GET_TRACK_LIST_BY_TRACKING_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getTrackingListByTrackingId(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("trackId") int trackId) {

		logger.info("TrackingController.getTrackingListByTrackingId() started");
		ResponseEntity response = new ResponseEntity();
		try {
			InvoiceTrackingVo invoiceTrackingVo = trackingServiceImpl
					.getTrackingtById(trackId);
			if (null != invoiceTrackingVo) {
				response.setData(invoiceTrackingVo);
				response.setMessage(MDProperties.read(MDMessageConstant.GET_TRACKING_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("TrackingController.getTrackingListByTrackingId() end");
		return response;
	}

	@RequestMapping(value = RestURIConstants.GET_TRACKING_COUNT_BY_INVOICE_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getTrackingCountByInvoiceId(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("invId") int invId) {

		logger.info("TrackingController.getTrackingCountByInvoiceId() started");
		ResponseEntity response = new ResponseEntity();
		try {
			Map<String, Integer> map = trackingServiceImpl
					.getTrackingCountByInvoiceId(invId);
			response.setData(map);
			response.setMessage(MDProperties.read(MDMessageConstant.GET_TRACKING_COUNT_BY_INVOICE_ID_SUCCESS));
			response.setResponseCode(RestConstants.OK);
			response.setStatus(Status.Success.getValue());
		} catch (MDNoRecordsFoundException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(e.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("TrackingController.getTrackingCountByInvoiceId() end");
		return response;
	}

	/************************* Update InvoiceTrackList *****************************/
	@RequestMapping(value = RestURIConstants.UPDATE_INVOICE_TRACKING, method = RequestMethod.PUT, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity updateInvoiceTracking(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody InvoiceTrackingVo invoiceTrackingVo) {
		logger.info("TrackingController.updateInvoiceTracking() start");
		ResponseEntity response = new ResponseEntity();
		try {
			boolean flag = trackingServiceImpl
					.updateInvoiceTracking(invoiceTrackingVo);
			if (flag) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.UPDATE_TRACKING_SUCCESS));
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("TrackingController.updateInvoiceTracking() end");
		return response;
	}

	/************************* Delete InvoiceTrackList By TrackId *****************************/
	@RequestMapping(value = RestURIConstants.DELETE_INVOICE_TRACKING_BY_ID, method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity deleteInvoiceTrackListById(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("trackId") int trackId) {
		logger.info("TrackingController.deleteInvoiceTrackListById() start");
		ResponseEntity response = new ResponseEntity();
		try {
			boolean flag = trackingServiceImpl
					.deleteInvoiceTrackListById(trackId);
			if (flag) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.DELETE_TRACKING_LIST_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException md) {
			logger.error(md.getMessage());
			response.setData("");
			response.setMessage(md.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("TrackingController.deleteInvoiceTrackListById() end");
		return response;
	}

	/************************* Get InvoiceTrackList By InvId *****************************/
	@RequestMapping(value = RestURIConstants.GET_INVOICE_TRACK_LIST_BY_INVID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getInvoiceTrackListByInvId(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("invId") int invId) {
		logger.info("TrackingController.getInvoiceTrackListByInvId() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<InvoiceTrackingVo> listByInvId = trackingServiceImpl
					.getInvoiceTrackListByInvId(invId);
			if (null != listByInvId) {
				response.setData(listByInvId);
				response.setMessage(MDProperties.read(MDMessageConstant.GET_TRACKING_BY_INVOICE_ID_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException md) {
			logger.error(md.getMessage());
			response.setData("");
			response.setMessage(md.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("TrackingController.getInvoiceTrackListByInvId() end");
		return response;
	}
}
