/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : InvoiceController.java
 * Comments : TODO 
 * Created  : 15-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |15-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.ResponseEntity;
import com.meshdynamics.common.RestConstants;
import com.meshdynamics.common.RestURIConstants;
import com.meshdynamics.common.Status;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.common.helper.ModelVoHelper;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.service.InvoiceMasterServiceImpl;
import com.meshdynamics.vo.InvoiceMasterVo;

@Controller
@RequestMapping("/invoices")
public class InvoiceController {

	private static final Logger logger = Logger
			.getLogger(InvoiceController.class);

	@Autowired(required = true)
	InvoiceMasterServiceImpl invoiceMasterServiceImpl;

	@RequestMapping(value = RestURIConstants.ADD_INVOICE, method = RequestMethod.POST, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity addInvoice(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody InvoiceMasterVo invoiceMasterVo) {

		logger.info("InvoiceController.addInvoice() started");
		ResponseEntity response = new ResponseEntity();
		boolean flg = false;
		try {
			flg = invoiceMasterServiceImpl.addInvoice(ModelVoHelper
					.invoiceMasterVoToModel(invoiceMasterVo));
			if (flg) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.PUCHASE_ORDER_ADD_SUCCESS));
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());

		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage("" + mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());

		}
		logger.info("InvoiceController.addInvoice() end");
		return response;
	}

	@RequestMapping(value = RestURIConstants.GET_INVOICE, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getInvoices(
			@RequestHeader(value = "Authentication") String authentication) {

		logger.info("InvoiceController.getInvoices() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<InvoiceMasterVo> invoiceList = invoiceMasterServiceImpl
					.getInvoices();
			if (invoiceList != null && invoiceList.size() > 0) {
				response.setData(invoiceList);
				response.setMessage(MDProperties.read(MDMessageConstant.GET_PURCHASE_ORDERS_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("InvoiceController.getInvoices() end");
		return response;
	}

	@RequestMapping(value = RestURIConstants.GET_INVOICE_BY_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getInvoiceById(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("invId") int invid) {

		logger.info("InvoiceController.getInvoiceById() started");
		ResponseEntity response = new ResponseEntity();
		try {
			InvoiceMasterVo invoiceMasterVo = invoiceMasterServiceImpl
					.getInvoiceById(invid);
			response.setData(invoiceMasterVo);
			response.setMessage(MDProperties.read(MDMessageConstant.GET_PURCHASE_ORDER_SUCCESS));
			response.setResponseCode(RestConstants.OK);
			response.setStatus(Status.Success.getValue());
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("InvoiceController.getInvoiceById() end");
		return response;
	}

	@RequestMapping(value = RestURIConstants.DELETE_INVOICE_BY_ID, method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity deleteInvoiceById(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("invId") int invId) {

		logger.info("InvoiceController.deleteInvoiceById() started");
		ResponseEntity response = new ResponseEntity();
		try {
			boolean flg = invoiceMasterServiceImpl.deleteInvoiceById(invId);
			if (flg) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.PURCHASE_ORDER)
						+" '"+invId+"' "+MDProperties.read(MDMessageConstant.DELETE_PURCHASE_ORDER_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage("" + mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getCause().getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("InvoiceController.deleteInvoiceById() end");
		return response;
	}

	/******************************* UpdateInvoiceById ****************************/

	@RequestMapping(value = RestURIConstants.UPDATE_INVOICE_BY_ID, method = RequestMethod.PUT, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity updateInvoiceById(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody InvoiceMasterVo invoiceMasterVo) {

		logger.info("InvoiceController.updateInvoiceById() started");
		ResponseEntity responseEntity = new ResponseEntity();
		try {
			boolean flg = invoiceMasterServiceImpl
					.updateInvoice(invoiceMasterVo);
			if (flg) {
				responseEntity.setData("");
				responseEntity.setMessage(MDProperties.read(MDMessageConstant.PURCHASE_ORDER)
						+" '"+invoiceMasterVo.getInvId()+"' "+MDProperties.read(MDMessageConstant.UPDATE_PURCHSE_ORDER_SUCCESS));
				responseEntity.setResponseCode(RestConstants.OK); 
				responseEntity.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(mde.getCause().getMessage());
			responseEntity.setResponseCode(mde.getCode());
			responseEntity.setStatus(Status.Failure.getValue());

		} catch (Exception e) {
			logger.error(e.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(e.getMessage());
			responseEntity.setResponseCode(Constant.SYSTEM_EXCEPTION);
			responseEntity.setStatus(Status.Failure.getValue());

		}
		logger.info("InvoiceController.updateInvoiceById() end");
		return responseEntity;
	}
	
	@RequestMapping(value = RestURIConstants.GET_PURCHASE_ORDER_LIST, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity getPurchaseOrderList(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody String criteria) {

		logger.info("InvoiceController.getPurchaseOrderList() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<InvoiceMasterVo> invoiceList = invoiceMasterServiceImpl
					.getPurchaseOrderList(criteria);
			
			response.setData(invoiceList);
			response.setMessage(MDProperties.read(MDMessageConstant.GET_PURCHASE_ORDERLIST_CRITERIA_BASED));
			response.setResponseCode(RestConstants.OK);
			response.setStatus(Status.Success.getValue());
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getCause().getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("InvoiceController.getPurchaseOrderList() end");
		return response;
	}
	
	
	@RequestMapping(value = RestURIConstants.GET_PURCHASE_ORDER_LIST_PAGINATION, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity getPurchaseOrderListWithPagination(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody String criteria,
 			@RequestParam int start,int noOfRecords) {

		logger.info("InvoiceController.getPurchaseOrderListWithPagination() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<InvoiceMasterVo> invoiceList = invoiceMasterServiceImpl
					.getPurchaseOrderList(criteria, start, noOfRecords);
			int count = invoiceMasterServiceImpl.getCount(criteria);
			if (null != invoiceList && count > 0) {
				response.setCount(count);
				response.setData(invoiceList);
				response.setMessage(MDProperties
						.read(MDMessageConstant.GET_PURCHASE_ORDERLIST_CRITERIA_BASED));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getCause().getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("InvoiceController.getPurchaseOrderListWithPagination() end");
		return response;
	}
	
	@RequestMapping(value = RestURIConstants.GET_PURCHASE_ORDER_BY_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getPurchaseOrderListById(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("invId") int invid) {

		logger.info("InvoiceController.getPurchaseOrderListById() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<?> invoiceMasterVo = invoiceMasterServiceImpl
					.getPurchaseOrderById(invid);
			response.setData(invoiceMasterVo);
			response.setMessage(MDProperties.read(MDMessageConstant.GET_PURCHASE_ORDER_SUCCESS));
			response.setResponseCode(RestConstants.OK);
			response.setStatus(Status.Success.getValue());
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("InvoiceController.getPurchaseOrderListById() end");
		return response;
	}
}
