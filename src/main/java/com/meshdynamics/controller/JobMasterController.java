/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : JobOrderController.java
 * Comments : TODO 
 * Created  : 04-Oct-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |04-Oct-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.ResponseEntity;
import com.meshdynamics.common.RestConstants;
import com.meshdynamics.common.RestURIConstants;
import com.meshdynamics.common.Status;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.service.JobMasterServiceImpl;
import com.meshdynamics.vo.JobMasterVo;

@RestController
@RequestMapping("/jobOrder")
public class JobMasterController {

	private static final Logger LOGGER = Logger
			.getLogger(JobMasterController.class);

	@Autowired
	private JobMasterServiceImpl jobMasterServiceImpl;

	@RequestMapping(value = RestURIConstants.ADD_JOB_ORDERS, method = RequestMethod.POST, consumes = RestConstants.MEDIA_TYPE)
	public ResponseEntity addJobOrder(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody JobMasterVo jobMasterVo) {

		LOGGER.info("JobMasterController.addJobOrder() started ");
		ResponseEntity response = new ResponseEntity();
		boolean flg = false;
		try {
			flg = jobMasterServiceImpl.addJobOrders(jobMasterVo);
			if (flg) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.JOB_ADD_SUCCESS));
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			LOGGER.error(mde.getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		LOGGER.info("JobMasterController.addJobOrder() end ");
		return response;
	}

	@RequestMapping(value = RestURIConstants.GET_JOB_ORDERS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getJobOrderList(
			@RequestHeader(value = "Authentication") String authentication) {

		LOGGER.info("JobMasterController.getJobOrderList() started");
		ResponseEntity responseEntity = new ResponseEntity();
		try {
			List<JobMasterVo> JobMasterVoList = jobMasterServiceImpl
					.getJobOrders();
			if (JobMasterVoList != null && JobMasterVoList.size() > 0) {
				responseEntity.setData(JobMasterVoList);
				responseEntity.setMessage(MDProperties.read(MDMessageConstant.JOB_ORDERS_GET_SUCCESS));
				responseEntity.setResponseCode(RestConstants.OK);
				responseEntity.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			LOGGER.error(mde.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(mde.getCause().getMessage());
			responseEntity.setResponseCode(mde.getCode());
			responseEntity.setStatus(Status.Failure.getValue());
		} catch (MDNoRecordsFoundException mde) {
			LOGGER.error(mde.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(mde.getMessage());
			responseEntity.setResponseCode(RestConstants.NO_RECORDS);
			responseEntity.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getCause().getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(e.getMessage());
			responseEntity.setResponseCode(Constant.SYSTEM_EXCEPTION);
			responseEntity.setStatus(Status.Failure.getValue());
		}
		LOGGER.info("JobMasterController.getJobOrderList() end");
		return responseEntity;
	}

	/*********************** getJobOrderById *************************/

	@RequestMapping(value = RestURIConstants.GET_JOB_ORDER_BY_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getJobOrderById(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("jobId") int jobId) {

		LOGGER.info("JobMasterController.getJobOrderById() started");
		ResponseEntity response = new ResponseEntity();
		try {
			JobMasterVo jobMasterVo = jobMasterServiceImpl
					.getJobOrderById(jobId);
			if (jobMasterVo != null) {
				response.setData(jobMasterVo);
				response.setMessage(MDProperties.read(MDMessageConstant.JOB_ORDER_GET_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException nre) {
			LOGGER.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			LOGGER.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		LOGGER.info("JobMasterController.getJobOrderById() end");
		return response;
	}

	/*************************************** UpdateJobOrder ***************************/
	@RequestMapping(value = RestURIConstants.UPDATE_JOB_ORDER, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity updateJobOrder(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody JobMasterVo jobMasterVo) {

		LOGGER.info("JobMasterController.updateJobOrder() started");
		ResponseEntity response = new ResponseEntity();
		boolean flg = false;
		try {
			flg = jobMasterServiceImpl.updateJobOrder(jobMasterVo);
			if (flg) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.JOB_ORDER_NAME)+" '"+
				jobMasterVo.getJobId()+"' "+MDProperties.read(MDMessageConstant.JOB_ORDER_UPDATE_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			LOGGER.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (MDNoRecordsFoundException nre) {
			LOGGER.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		LOGGER.info("JobMasterController.updateJobOrder() end");
		return response;
	}

	/********************************* DeleteJobOrderByJobId ******************************/

	@RequestMapping(value = RestURIConstants.DELETE_JOB_ORDER_BY_JOB_ID, method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity DeleteJobOrderById(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("jobId") int jobId) {

		LOGGER.info("JobMasterController.deleteJobOrderById() started");
		ResponseEntity response = new ResponseEntity();
		try {
			boolean flag = jobMasterServiceImpl.deleteJobOrder(jobId);
			if (flag) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.JOB_ORDER_NAME)+" '"+
						jobId+"' "+MDProperties.read(MDMessageConstant.JOB_ORDER_DELETE_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException nre) {
			LOGGER.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mbe) {
			LOGGER.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			LOGGER.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			LOGGER.error(e.getCause().getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		LOGGER.info("JobMasterController.deleteJobOrderById() end");
		return response;
	}

	@RequestMapping(value = RestURIConstants.GET_JOB_ORDER_LIST_BY_CRITERIA, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity getJobOrderListByCriteria(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody String criteria) {

		LOGGER.info("JobMasterController.getJobOrderList() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<JobMasterVo> jobOrderList = jobMasterServiceImpl
					.getJobOrderList(criteria);

			response.setData(jobOrderList);
			response.setMessage(MDProperties.read(MDMessageConstant.JOB_ORDERS_GET_SUCCESS));
			response.setResponseCode(RestConstants.OK);
			response.setStatus(Status.Success.getValue());
		} catch (MDBusinessException mbe) {
			LOGGER.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDNoRecordsFoundException nre) {
			LOGGER.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			LOGGER.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getCause().getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		LOGGER.info("JobMasterController.getJobOrderList() end");
		return response;
	}
	
	@RequestMapping(value = RestURIConstants.GET_JOB_ORDER_LIST_PAGINATION, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity getJobOrderListWithPagination(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody String criteria,
 			@RequestParam int start,int noOfRecords) {

		LOGGER.info("JobMasterController.getJobOrderListWithPagination() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<JobMasterVo> jobList = jobMasterServiceImpl
					.getJobOrderList(criteria, start, noOfRecords);
			int count = jobMasterServiceImpl.getCount(criteria);
			if (null != jobList && count > 0) {
				response.setCount(count);
				response.setData(jobList);
				response.setMessage(MDProperties
						.read(MDMessageConstant.GET_JOB_ORDERLIST_CRITERIA_BASED));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			LOGGER.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			LOGGER.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (MDNoRecordsFoundException nre) {
			LOGGER.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			LOGGER.error(e.getCause().getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		LOGGER.info("JobMasterController.getJobOrderListWithPagination() end");
		return response;
	}
	
	@RequestMapping(value = RestURIConstants.GET_JOB_ORDER_LIST_BY_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getJobOrderListById(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("jobId") int jobId) {

		LOGGER.info("JobMasterController.getJobOrderListById() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<?> jobMasterVoList = jobMasterServiceImpl
					.getJobOrderListById(jobId);
			response.setData(jobMasterVoList);
			response.setMessage(MDProperties.read(MDMessageConstant.JOB_ORDERS_GET_SUCCESS));
			response.setResponseCode(RestConstants.OK);
			response.setStatus(Status.Success.getValue());
		} catch (MDNoRecordsFoundException nre) {
			LOGGER.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		LOGGER.info("JobMasterController.getJobOrderListById() end");
		return response;
	}
}
