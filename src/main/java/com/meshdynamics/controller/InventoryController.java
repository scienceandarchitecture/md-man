package com.meshdynamics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.ResponseEntity;
import com.meshdynamics.common.RestConstants;
import com.meshdynamics.common.RestURIConstants;
import com.meshdynamics.common.Status;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.service.InventoryServiceImpl;
import com.meshdynamics.vo.RawInventoryVo;

@Controller
@RequestMapping("/inventories")
public class InventoryController {

	private static final Logger logger = Logger
			.getLogger(InventoryController.class);
	@Autowired
	InventoryServiceImpl inventoryServiceImpl;

	@RequestMapping(value = RestURIConstants.ADD_RAW_INVENTORY, method = RequestMethod.POST, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity addRawInventories(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody RawInventoryVo rawInventoryVo) {

		logger.info("InventoryController.addRawInventory() started");
		ResponseEntity response = new ResponseEntity();
		boolean flg = false;
		try {
			if (rawInventoryVo.getRsku().trim().length() < 1)
				throw new MDBusinessException(MDProperties.read(MDMessageConstant.BUSINESS_EXCEPTION));
			flg = inventoryServiceImpl.addRawInventory(rawInventoryVo);
			if (flg) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.INVENTORY_ADD_SUCCESS));
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.DATA_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("InventoryController.addRawInventory() end");
		return response;
	}

	@RequestMapping(value = RestURIConstants.UPDATE_RAW_INVENTORY, method = RequestMethod.PUT, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity updateRawInventories(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody RawInventoryVo rawInventoryVo) {

		logger.info("InventoryController.updateRawInventory() started");
		ResponseEntity response = new ResponseEntity();
		boolean flg = false;
		try {
			flg = inventoryServiceImpl.updateRawInventory(rawInventoryVo);
			if (flg) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.INVENTORY_UPDATE_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			} else {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.INVENTORY_UPDATE_FAILURE));
				response.setResponseCode(RestConstants.NOT_MODIFIED);
				response.setStatus(Status.Failure.getValue());
			}
		} catch (Exception e) {
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(RestConstants.NOT_MODIFIED);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("InventoryController.updateRawInventory() end");
		return response;
	}

	@RequestMapping(value = RestURIConstants.GET_RAW_INVENTORY_LIST, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getInventoryList(
			@RequestHeader(value = "Authentication") String authentication) {

		logger.info("InventoryController.getRawInventoryList() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<RawInventoryVo> rawInventoryList = inventoryServiceImpl
					.getRawInventoryList();
			if (rawInventoryList != null && rawInventoryList.size() > 0) {
				response.setData(rawInventoryList);
				response.setMessage(MDProperties.read(MDMessageConstant.GET_RAW_INVENTORIES_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("InventoryController.getRawInventoryList() end");
		return response;
	}

	/************************* Delete Product Record ***********************/

	@RequestMapping(value = RestURIConstants.DELETE_RAW_INVENTORY_BY_ID, method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity DeleteRawInventoryById(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("rsku") String rsku) {
		logger.info("InventoryController.deleteRawInventoryById() started");
		ResponseEntity response = new ResponseEntity();
		try {
			inventoryServiceImpl.deleteRawInventory(rsku);
			response.setData("");
			response.setMessage(MDProperties
					.read(MDMessageConstant.INVENTORY_DELETE_SUCCESS));
			response.setResponseCode(RestConstants.OK);
			response.setStatus(Status.Success.getValue());
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mdbe) {
			logger.error(mdbe.getMessage());
			response.setData("");
			response.setMessage("" + mdbe.getMessage());
			response.setResponseCode(mdbe.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("InventoryController.deleteRawInventoryById() end");
		return response;
	}
	
	@RequestMapping(value = RestURIConstants.GET_INVENTORY_LIST_USING_PAGINATION, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity getRawInventoryListUsingPagination(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody String criteria,
			@RequestParam int start, int noOfRecords) {

		logger.info("InventoryController.getRawInventoryListUsingPagination() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<RawInventoryVo> inventoryList = inventoryServiceImpl
					.getPaginationInventoryList(criteria,start, noOfRecords);
			int count = inventoryServiceImpl.getCount(criteria);
			response.setCount(count);
			response.setData(inventoryList);
			response.setMessage(MDProperties
					.read(MDMessageConstant.GET_INVENTORY_LIST_PAGINATION));
			response.setResponseCode(RestConstants.OK);
			response.setStatus(Status.Success.getValue());
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("InventoryController.getRawInventoryListUsingPagination() end");
		return response;
	}
	
	@RequestMapping(value = RestURIConstants.GET_RAW_INVENTORY_BY_SKU, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getRawInventoryBySKU(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("rsku") String rsku) {
		logger.info("InventoryController.getRawInventoryBySKU() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<?> rawInventoryVo = inventoryServiceImpl
					.getRawInventoryBySKU(rsku);
			response.setData(rawInventoryVo);
			response.setMessage(MDProperties
					.read(MDMessageConstant.GET_RAW_INVENTORIES_SUCCESS));
			response.setResponseCode(RestConstants.OK);
			response.setStatus(Status.Success.getValue());
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mdbe) {
			logger.error(mdbe.getMessage());
			response.setData("");
			response.setMessage("" + mdbe.getMessage());
			response.setResponseCode(mdbe.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("InventoryController.getRawInventoryBySKU() end");
		return response;
	}
}