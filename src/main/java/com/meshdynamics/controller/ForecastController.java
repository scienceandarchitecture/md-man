/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ForecastController.java
 * Comments : TODO 
 * Created  : 30-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |30-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.ResponseEntity;
import com.meshdynamics.common.RestConstants;
import com.meshdynamics.common.RestURIConstants;
import com.meshdynamics.common.Status;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.service.ForecastMasterServiceImpl;
import com.meshdynamics.vo.ForecastMasterVo;

@Controller
@RequestMapping("/forecast")
public class ForecastController {
	private static final Logger logger = Logger
			.getLogger(ForecastController.class);

	@Autowired
	ForecastMasterServiceImpl forecastMasterServiceImpl;

	@RequestMapping(value = RestURIConstants.ADD_FORECAST, method = RequestMethod.POST, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity addForecast(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody ForecastMasterVo forecastMasterVo) {

		logger.info("ForecastController.addForecast() started");
		boolean flag = false;
		ResponseEntity responseEntity = new ResponseEntity();
		try {
			flag = forecastMasterServiceImpl
					.addForecastMaster(forecastMasterVo);
			if (flag) {
				responseEntity.setData("");
				responseEntity.setMessage("Forecast Added Successfully ");
				responseEntity.setResponseCode(RestConstants.CREATED);
				responseEntity.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			responseEntity.setData("");
			responseEntity.setMessage("" + mde.getCause());
			responseEntity.setResponseCode(mde.getCode());
			responseEntity.setStatus(Status.Failure.getValue());

		} catch (Exception e) {
			logger.error(e.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(e.getMessage());
			responseEntity.setResponseCode(Constant.SYSTEM_EXCEPTION);
			responseEntity.setStatus(Status.Failure.getValue());
		}
		logger.info("ForecastController.addForecast() end");
		return responseEntity;
	}

	@RequestMapping(value = RestURIConstants.GET_FORECASTS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getForecastList(
			@RequestHeader(value = "Authentication") String authentication) {

		logger.info("ForecastController.getForecasts() started");
		ResponseEntity responseEntity = new ResponseEntity();
		try {
			List<ForecastMasterVo> forecastMasterList = forecastMasterServiceImpl
					.getForecastList();
			if (forecastMasterList != null && forecastMasterList.size() > 0) {
				responseEntity.setData(forecastMasterList);
				responseEntity
						.setMessage("ForecastMaster fetched successfully");
				responseEntity.setResponseCode(RestConstants.OK);
				responseEntity.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException nrfe) {
			logger.error(nrfe.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(nrfe.getMessage());
			responseEntity.setResponseCode(RestConstants.NO_RECORDS);
			responseEntity.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseEntity.setData("");
			responseEntity.setMessage(e.getMessage());
			responseEntity.setResponseCode(Constant.SYSTEM_EXCEPTION);
			responseEntity.setStatus(Status.Failure.getValue());
		}
		logger.info("ForecastController.getForecasts() end");
		return responseEntity;
	}

	@RequestMapping(value = RestURIConstants.GET_FORECAST_BY_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getForecastById(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("fcastId") int fcastId) {

		logger.info("ForecastController.getForecastById() started");
		ResponseEntity response = new ResponseEntity();
		try {
			ForecastMasterVo forecastMasterVo = forecastMasterServiceImpl
					.getForecastById(fcastId);
			if (forecastMasterVo != null) {
				response.setData(forecastMasterVo);
				response.setMessage("ForecastItem fetched successfully");
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("ForecastController.getForecastById() end");
		return response;
	}

	/********************************************* UpdateForecastById **********************************/
	@RequestMapping(value = RestURIConstants.UPDATE_FORECAST, method = RequestMethod.PUT, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity updateForecast(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody ForecastMasterVo forecastMasterVo) {

		logger.info("ForecastMasterController.updateForecast() started");
		ResponseEntity response = new ResponseEntity();
		boolean flg = false;
		try {
			flg = forecastMasterServiceImpl.updateForecast(forecastMasterVo);
			if (flg) {
				response.setData("");
				response.setMessage("ForecastMaster updated successfully");
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			} else {
				response.setData("");
				response.setMessage("ForecastMaster not updated");
				response.setResponseCode(RestConstants.NOT_MODIFIED);
				response.setStatus(Status.Failure.getValue());
			}
		} catch (Exception e) {
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("ForecastMasterController.updateForecast() end");
		return response;
	}

	/********************************************* Delete ForecastById *****************************/

	@RequestMapping(value = RestURIConstants.DELETE_FORECAST_BY_FORECAST_ID, method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity DeleteForecastByForecastId(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("fcastId") int fcastId) {

		logger.info("ForecastController.deleteForecasttByForecastId() started");
		ResponseEntity response = new ResponseEntity();
		try {
			boolean flag = forecastMasterServiceImpl
					.deleteForecastByForecastID(fcastId);
			if (flag) {
				response.setData("");
				response.setMessage("Forecast deleted successfully");
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage("" + mde.getCause());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("ForecastController.deleteForecasttByForecastId() end");
		return response;
	}
}
