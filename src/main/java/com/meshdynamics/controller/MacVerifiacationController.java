package com.meshdynamics.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.ResponseEntity;
import com.meshdynamics.common.Status;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.service.MacVerificationService;
import com.meshdynamics.vo.RegisterMacVO;

@RestController
@RequestMapping("/macVerification")
public class MacVerifiacationController {
	
	
	private static final Logger logger = Logger.getLogger(MacVerifiacationController.class);

	@Autowired
	MacVerificationService macVerificationSevice;
	
	
	@RequestMapping(value="/register",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity macVerification(@RequestBody RegisterMacVO registerMacVO){
		logger.info("MacAddress verification  ");
		boolean flag=false;
		ResponseEntity entity=new ResponseEntity();
	try {		
		if(registerMacVO.getBoardMacAddress() != null)
		flag=macVerificationSevice.macVerification(registerMacVO);
		else{
			entity.setData(" ");
			entity.setMessage("Device MacAddress shoud  required");
			entity.setStatus(Status.Failure.getValue());
		}
		if(flag){
			logger.info("macAddress is registered");
			entity.setData("");
			entity.setMessage("Device Registered");
			entity.setResponseCode(Constant.macRegister);
			entity.setStatus(Status.Success.getValue());
		}else{
			logger.info("macAddress not registered");
			entity.setData(" ");
			entity.setMessage("Device Not Registered");
			entity.setResponseCode(Constant.not_macRegister);
			entity.setStatus(Status.Failure.getValue());
		}
	
	} catch (MDDBException mde) {
		logger.error(mde.getCause().getMessage());
		entity.setData("");
		entity.setMessage("" + mde.getCause());
		entity.setResponseCode(mde.getCode());
		entity.setStatus(Status.Failure.getValue());

	}catch (Exception e) {
		logger.info("");
		entity.setData("");
		entity.setMessage(e.getMessage());
		entity.setResponseCode(Constant.SYSTEM_EXCEPTION);
		entity.setStatus(Status.Failure.getValue());
	}
	return entity;
	}

	@RequestMapping(value="/verifyMac",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity macVerificationInMDMan(
			@RequestBody RegisterMacVO registerMacVO) {
		logger.info("MacAddress verification  ");
		boolean flag = false;
		ResponseEntity entity = new ResponseEntity();
		try {
			if (registerMacVO.getBoardMacAddress() != null)
				flag = macVerificationSevice
						.macVerificationInMDMan(registerMacVO);
			else {
				entity.setData(" ");
				entity.setMessage("Device MacAddress should required");
				entity.setStatus(Status.Failure.getValue());
			}
			if (flag) {
				logger.info("macAddress is registered");
				entity.setData("");
				entity.setMessage("Device Registered");
				entity.setResponseCode(Constant.macRegister);
				entity.setStatus(Status.Success.getValue());
			} else {
				logger.info("macAddress not registered");
				entity.setData(" ");
				entity.setMessage("Device Not Registered");
				entity.setResponseCode(Constant.not_macRegister);
				entity.setStatus(Status.Failure.getValue());
			}

		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			entity.setData("");
			entity.setMessage("" + mde.getCause());
			entity.setResponseCode(mde.getCode());
			entity.setStatus(Status.Failure.getValue());

		} catch (Exception e) {
			logger.info("");
			entity.setData("");
			entity.setMessage(e.getMessage());
			entity.setResponseCode(Constant.SYSTEM_EXCEPTION);
			entity.setStatus(Status.Failure.getValue());
		}
		return entity;
	}
}
