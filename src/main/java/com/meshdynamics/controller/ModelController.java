/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ModelController.java
 * Comments : TODO 
 * Created  : Dec 2, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Dec 2, 2016  | Created                             | Pinki  			|
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.ResponseEntity;
import com.meshdynamics.common.RestConstants;
import com.meshdynamics.common.RestURIConstants;
import com.meshdynamics.common.Status;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.service.ModelServiceImpl;
import com.meshdynamics.vo.ModelVo;

@Controller
@RequestMapping("/models")
public class ModelController {

	private static final Logger logger = Logger
			.getLogger(ModelController.class);

	@Autowired(required = true)
	ModelServiceImpl modelServiceImpl;

	@RequestMapping(value = RestURIConstants.GET_MODEL_LIST, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getModels(
			@RequestHeader(value = "Authentication") String authentication) {

		logger.info("ModelController.getModels() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<ModelVo> modelList = modelServiceImpl.getModelList();
			if (modelList != null && modelList.size() > 0) {
				response.setData(modelList);
				response.setMessage("Model list fetched successfully");
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("ModelController.getModels() end");
		return response;
	}

}
