/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : UserRoleController.java
 * Comments : TODO 
 * Created  : 23-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |23-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.ResponseEntity;
import com.meshdynamics.common.RestConstants;
import com.meshdynamics.common.RestURIConstants;
import com.meshdynamics.common.Status;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.service.UserRoleServiceImpl;
import com.meshdynamics.vo.UserRoleVo;

@Controller
@RequestMapping("/userRole")
public class UserRoleController {
	private static final Logger logger = Logger
			.getLogger(UserRoleController.class);

	@Autowired
	UserRoleServiceImpl userRoleServiceImpl;

	@RequestMapping(value = RestURIConstants.ADD_USERS, method = RequestMethod.POST, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity addUserRole(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody UserRoleVo userRoleVo) {

		logger.info("UserRoleController.addUserRole() started");
		ResponseEntity response = new ResponseEntity();
		boolean flg = false;
		try {
			flg = userRoleServiceImpl.addUserRole(userRoleVo);
			if (flg) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.USER_ADD_SUCCESS));
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());

		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("UserRoleController.addUserRole() ended");
		return response;
	}

	@RequestMapping(value = RestURIConstants.GET_USERS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getUsers(
			@RequestHeader(value = "Authentication") String authentication) {

		logger.info("UserRoleController.getUsers() started");
		ResponseEntity response = new ResponseEntity();
		List<UserRoleVo> userList = null;
		try {
			userList = userRoleServiceImpl.getUserRoleList();
			if (null != userList && userList.size() > 0) {
				response.setData(userList);
				response.setMessage(MDProperties.read(MDMessageConstant.GET_USERS_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());

		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());

		}
		logger.info("UserRoleController.getUsers() end");
		return response;
	}

	/*************************************** Update UserRole ************************/

	@RequestMapping(value = RestURIConstants.UPDATE_USER, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity updateUserRole(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody UserRoleVo userRoleVo) {

		logger.info("UserRoleController.updateUserRole() started");
		ResponseEntity response = new ResponseEntity();
		boolean flg = false;
		try {
			flg = userRoleServiceImpl.updateUserRole(userRoleVo);
			if (flg) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.USER_PREFIX)+" '"+
						userRoleVo.getUserName()+"' "+MDProperties.read(MDMessageConstant.UPDATE_USER_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("UserRoleController.updateUserRole() end");
		return response;
	}

	/************************ deleteUsersById *********************/

	@RequestMapping(value = RestURIConstants.DELETE_USER_BY_ID, method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity deleteUserById(
			@RequestHeader(value = "Authentication") String apikey,
			@RequestParam("userName") String userName) {

		logger.info("UserRoleController.deleteUserById() started");
		ResponseEntity response = new ResponseEntity();
		try {
			boolean flag = userRoleServiceImpl.deleteUserById(userName,apikey);
			if (flag) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.USER_PREFIX)+" '"+
						userName+"' "+MDProperties.read(MDMessageConstant.DELETE_USER_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("UserRoleController.deleteUserById() end");
		return response;
	}

	@RequestMapping(value = RestURIConstants.GET_USER_BY_ID, method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity getUserById(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("userName") String userName) {

		logger.info("UserRoleController.getUserById() started");
		ResponseEntity response = new ResponseEntity();
		try {
			UserRoleVo userRoleVo = userRoleServiceImpl.getUserById(userName);
			response.setData(userRoleVo);
			response.setMessage(MDProperties.read(MDMessageConstant.GET_USER_SUCCESS));
			response.setResponseCode(RestConstants.OK);
			response.setStatus(Status.Success.getValue());
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("UserRoleController.getUserById() end");
		return response;
	}
	
	/***************************************** change UserPassword *****************************/
	@RequestMapping(value = RestURIConstants.CHANGE_PASSWORD, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity changePassword(@RequestHeader("Authentication") String apikey,
			@RequestBody String jsonString) throws ParseException {
		
		logger.info("UserRoleController.changePassword() started");
		ResponseEntity response = new ResponseEntity();
		JSONObject jsonObject = (JSONObject) new JSONParser().parse(jsonString);
		try {
			String currentPassword = (String) jsonObject.get("currentPassword");
			String newPassword = (String) jsonObject.get("newPassword");
			boolean flag = userRoleServiceImpl.changePassword(apikey,currentPassword, newPassword);
			if (flag) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.CHANGE_USER_PASSWORD));
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("UserRoleController.changePassword() end");
		return response;
	}
	
	@RequestMapping(value = RestURIConstants.GET_USERS_BASED_ON_PAGINATION, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity getUsersBasedOnPagination(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody String criteria,
			@RequestParam int start, int noOfRecords) throws Exception {

		logger.info("UserRoleController.getUsersBasedOnPagination() started");
		ResponseEntity response = new ResponseEntity();
		List<UserRoleVo> userList = null;
		try {
			userList = userRoleServiceImpl.getUserRoleListBasedOnPagination(criteria,
					start, noOfRecords);
			int count = userRoleServiceImpl.getCount(criteria);
			if (null != userList && userList.size() > 0) {
				response.setCount(count);
				response.setData(userList);
				response.setMessage(MDProperties
						.read(MDMessageConstant.GET_USERS_PAGINATION_LIST_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("UserRoleController.getUsersBasedOnPagination() end");
		return response;
	}
}
