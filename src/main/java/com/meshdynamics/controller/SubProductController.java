/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : SubProductController.java
 * Comments : TODO 
 * Created  : Dec 22, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Dec 22, 2016  | Created                             | Pinki          |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.ResponseEntity;
import com.meshdynamics.common.RestConstants;
import com.meshdynamics.common.RestURIConstants;
import com.meshdynamics.common.Status;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.service.SubProductServiceImpl;
import com.meshdynamics.vo.SubProductVo;

@Controller
@RequestMapping("/subProduct")
public class SubProductController {

	private static final Logger logger = Logger
			.getLogger(SubProductController.class);

	@Autowired(required = true)
	SubProductServiceImpl subProductServiceImpl;

	@RequestMapping(value = RestURIConstants.ADD_SUB_PRODUCT, method = RequestMethod.POST, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity addSubProduct(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody final SubProductVo subProductVo) {

		logger.info("SubProductController.addSubProduct() started");
		ResponseEntity response = new ResponseEntity();
		boolean flg = false;
		try {
			flg = subProductServiceImpl.addSubProduct(subProductVo);
			if (flg) {
				response.setData("");
				response.setMessage(MDMessageConstant.SUBPRODUCT_ADD_SUCCESS);
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getMessage());
			response.setData("");
			response.setMessage("" + mde.getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("SubProductController.addSubProduct() end");
		return response;
	}

	@RequestMapping(value = RestURIConstants.GET_SUB_PRODUCT, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getSubProducts(
			@RequestHeader(value = "Authentication") String authentication) {

		logger.info("SubProductController.getSubProducts() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<SubProductVo> subProductList = subProductServiceImpl
					.getSubProducts();
			if (null != subProductList && subProductList.size() > 0) {
				response.setData(subProductList);
				response.setMessage(MDMessageConstant.GET_SUBPRODUCTS_SUCCESS);
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("SubProductController.getSubProducts() end");
		return response;
	}
}
