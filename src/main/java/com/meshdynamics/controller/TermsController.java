/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : TermsController.java
 * Comments : TODO 
 * Created  : 20-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |20-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.ResponseEntity;
import com.meshdynamics.common.RestConstants;
import com.meshdynamics.common.RestURIConstants;
import com.meshdynamics.common.Status;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.service.TermsServiceImpl;
import com.meshdynamics.vo.TermsVo;

@RestController
@RequestMapping("/terms")
public class TermsController {
	private static final Logger logger = Logger
			.getLogger(TermsController.class);

	@Autowired
	TermsServiceImpl termsServiceImpl;

	@RequestMapping(value = RestURIConstants.ADD_TERMS, method = RequestMethod.POST, consumes = RestConstants.MEDIA_TYPE)
	public ResponseEntity addTerms(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody TermsVo termsVo) {
		
		logger.info("TermsController.addTerms() started ");
		ResponseEntity response = new ResponseEntity();
		boolean flg = false;
		try {
			flg = termsServiceImpl.addTerms(termsVo);
			if (flg) {
				response.setData("");
				response.setMessage(MDMessageConstant.TERMS_ADD_SUCCESS);
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("TermsController.addTerms() end");
		return response;
	}

	@RequestMapping(value = RestURIConstants.GET_TERMS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getTerms(
			@RequestHeader(value = "Authentication") String authentication) {
		
		logger.info("TermsController.getTerms() started");
		ResponseEntity responseEntity = new ResponseEntity();
		try {
			List<TermsVo> termsList = termsServiceImpl.getTermsList();
			if (null != termsList && termsList.size() > 0) {
				responseEntity.setData(termsList);
				responseEntity.setMessage(MDMessageConstant.GET_TERMS_SUCCESS);
				responseEntity.setResponseCode(RestConstants.OK);
				responseEntity.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			responseEntity.setData("");
			responseEntity.setMessage(mbe.getMessage());
			responseEntity.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			responseEntity.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			responseEntity.setData("");
			responseEntity.setMessage(e.getMessage());
			responseEntity.setResponseCode(Constant.SYSTEM_EXCEPTION);
			responseEntity.setStatus(Status.Failure.getValue());
		}
		logger.info("TermsController.getTerms() end");
		return responseEntity;
	}

	@RequestMapping(value = RestURIConstants.DELETE_TERM_BY_ID, method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity deleteTermByID(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("termId") int termId) {
		
		logger.info("termController.deleteTermById() started");
		ResponseEntity response = new ResponseEntity();
		boolean flag = false;
		try {
			flag = termsServiceImpl.deleteTerm(termId);
			if (flag == true) {
				response.setData("");
				response.setMessage(MDMessageConstant.DELETE_TERM_SUCCESS);
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			} else {
				response.setData("");
				response.setMessage("terms not deleted");
				response.setResponseCode(Constant.DATA_EXCEPTION);
				response.setStatus(Status.Failure.getValue());
			}
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("TermController.deleteTermById() end");
		return response;
	}

	@RequestMapping(value = RestURIConstants.UPDATE_TERMS, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity updateTerm(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody TermsVo termsVo) {

		logger.info("TermsController.updateTerm() started");
		ResponseEntity response = new ResponseEntity();
		boolean flg = false;
		try {
			flg = termsServiceImpl.updateTerm(termsVo);
			if (flg) {
				response.setData("");
				response.setMessage(MDMessageConstant.UPDATE_TERM_SUCCESS);
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("TermsController.updateTerm() end");
		return response;
	}
}
