package com.meshdynamics.controller;

/**
 * @author Sanjay
 *
 */
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.DBConstant;
import com.meshdynamics.common.LoginCache;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.ResponseEntity;
import com.meshdynamics.common.RestConstants;
import com.meshdynamics.common.RestURIConstants;
import com.meshdynamics.common.Status;
import com.meshdynamics.common.helper.Helper;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.service.LoginServiceImpl;
import com.meshdynamics.vo.UserRoleVo;

@Controller
@RequestMapping("/login")
public class LoginController {
	static final Logger logger = Logger.getLogger(LoginController.class);

	@Autowired
	LoginServiceImpl loginServiceImpl;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = RestURIConstants.LOGIN, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity login(@RequestBody String jsonString)
			throws ParseException {
		logger.info("LoginController.login() started");
		JSONObject jsonObject = (JSONObject) new JSONParser().parse(jsonString);
		JSONObject jsonResponseObject = new JSONObject();
		ResponseEntity response = new ResponseEntity();
		Map<String, String> map = LoginCache.getInstance();
		try {
			String userName = (String) jsonObject.get(DBConstant.USER_NAME);
			String password = (String) jsonObject.get(DBConstant.PASSWORD);
			String apiKey = Helper.generateRandomString();
			UserRoleVo role = loginServiceImpl.findUserByUserNameAndPassword(
					userName, password);
			if (null != role) {
				map.put(apiKey,
						userName + ","
								+ Helper.getStringDateTimeFromDate(new Date()));
				jsonResponseObject.put(MDMessageConstant.AUTHKEY, apiKey);
				jsonResponseObject.put(MDMessageConstant.USER, role);
				response.setData(jsonResponseObject);
				response.setMessage(MDMessageConstant.LOGIN_SUCCESS);
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			} else {
				response.setData(null);
				response.setMessage(MDMessageConstant.LOGIN_FAILURE);
				response.setResponseCode(RestConstants.POST);
				response.setStatus(Status.Failure.getValue());
			}
		} catch (MDBusinessException mbe) {
			response.setData("");
			response.setMessage(MDMessageConstant.PASSWORD_FAILURE);
			response.setResponseCode(RestConstants.UNAUTHORISED);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			response.setData("");
			response.setMessage(MDMessageConstant.USERNAME_FAILURE);
			response.setResponseCode(RestConstants.UNAUTHORISED);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			response.setData("");
			response.setMessage(MDMessageConstant.UNABLE_LOGIN);
			response.setResponseCode(RestConstants.POST);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("LoginController.login() end");
		return response;
	}
	
	@RequestMapping(value = RestURIConstants.LOGOUT, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity logout(
			@RequestHeader("Authentication") String apikey) {
		logger.info("LoginController.logout() started");
		boolean flag = false;
		ResponseEntity response = new ResponseEntity();
		try {
			Map<String, String> map = LoginCache.getInstance();
			if (null != map && map.size() > 0) {
				if (map.containsKey(apikey)) {
					String apikyValue = map.get(apikey);
					String[] arr = apikyValue.split(",");
					map.remove(apikey);
					logger.info(arr[0] + MDMessageConstant.LOGOUT_FROM_MDMAN);
					flag = true;
				}
			}
		} catch (Exception e) {
			response.setData("");
			response.setMessage(MDMessageConstant.LOGOUT_FAILURE);
			response.setResponseCode(RestConstants.POST);
			response.setStatus(Status.Failure.getValue());
		}
		if (flag) {
			response.setData("");
			response.setMessage(MDMessageConstant.LOGIN_SUCCESS);
			response.setResponseCode(RestConstants.OK);
			response.setStatus(Status.Success.getValue());
		} else {
			response.setData("");
			response.setMessage(MDMessageConstant.AUTHORIZATION_FAILURE);
			response.setResponseCode(RestConstants.UNAUTHORISED);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("LoginController.logout() end");
		return response;
	}

	@RequestMapping(value = RestURIConstants.CHANGE_PASSWORD, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity changePassword(
			@RequestHeader("Authentication") String apikey,
			@RequestBody String jsonString) throws ParseException {
		logger.info("LoginController.changePassword() started");
		ResponseEntity response = new ResponseEntity();
		JSONObject jsonObject = (JSONObject) new JSONParser().parse(jsonString);
		try {
			String currentPassword = (String) jsonObject.get(MDMessageConstant.CURRENT_PASSWORD);
			String newPassword = (String) jsonObject.get(DBConstant.PASSWORD);
			boolean flag = loginServiceImpl.changePassword(apikey, currentPassword,
					newPassword);
			if (flag) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.CHANGE_PASSWORD));
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());

		} catch (Exception e) {
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("LoginController.changePassword() end");
		return response;
	}
	
	
	@RequestMapping(value = RestURIConstants.FORGOT_PASSWORD, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity forgotPassword(
			@RequestParam String userName) {

		logger.info("LoginController.forgotPassword() started");
		ResponseEntity response = new ResponseEntity();
		try {
			boolean flag = loginServiceImpl.forgotPassword(userName);
			if (flag) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.FORGOT_PASSWORD));
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mbe) {
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(RestConstants.UNAUTHORISED);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			response.setData("");
			response.setMessage(mde.getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("LoginController.forgotPassword() end");
		return response;
	}
}
