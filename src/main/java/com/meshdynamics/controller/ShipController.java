/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ShipController.java
 * Comments : TODO 
 * Created  : Oct 5, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |    Comment                       |     Author    |
 * ---------------------------------------------------------------------------
 * |  0  |Oct 5, 2016   |    Created                    	  |      Pinki	  |  
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.ResponseEntity;
import com.meshdynamics.common.RestConstants;
import com.meshdynamics.common.RestURIConstants;
import com.meshdynamics.common.Status;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.service.ShipMasterServiceImpl;
import com.meshdynamics.vo.ShipMasterVo;

@Controller
@RequestMapping("/ships")
public class ShipController {

	public static final Logger logger = Logger.getLogger(ShipController.class);

	@Autowired(required = true)
	ShipMasterServiceImpl shipMasterServiceImpl;

	@RequestMapping(value = RestURIConstants.ADD_SHIPPING_ITEM, method = RequestMethod.POST, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity addShipItem(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody ShipMasterVo shipMasterVo) {

		ResponseEntity response = new ResponseEntity();
		boolean flag = false;
		try {
			logger.info("ShipController.addShipItem() started");
			flag = shipMasterServiceImpl.addShipMaster(shipMasterVo);
			if (flag) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.SHIP_ADD_SUCCESS));
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage("" + mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("ShipController.addShipItem() end");
		return response;
	}

	/********************************** Fetch ShippingItem Record ***************/

	@RequestMapping(value = RestURIConstants.GET_SHIPPING_ITEM, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getShippingList(
			@RequestHeader(value = "Authentication") String authentication) {

		ResponseEntity response = new ResponseEntity();
		try {
			logger.info("ShipController.getShippingList started");
			List<ShipMasterVo> shipMasterList = shipMasterServiceImpl
					.getShippingList();
			if (null != shipMasterList && shipMasterList.size() > 0) {
				response.setData(shipMasterList);
				response.setMessage(MDProperties.read(MDMessageConstant.GET_SHIP_ITEM_LIST_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException md) {
			logger.error(md.getMessage());
			response.setData("");
			response.setMessage(md.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("shipController.getShippingList ends");
		return response;
	}

	/****************************** GetShipItemById **************************/

	@RequestMapping(value = RestURIConstants.GET_SHIP_ITEM_BY_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getShipItemById(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("shipId") int shipId) {

		ResponseEntity response = new ResponseEntity();
		try {
			logger.info("ShipController.getShipItemById() started");
			ShipMasterVo shipMasterVo = shipMasterServiceImpl
					.getShipItemById(shipId);
			if(null != shipMasterVo){
				response.setData(shipMasterVo);
				response.setMessage(MDProperties.read(MDMessageConstant.GET_SHIP_ITEM_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException md) {
			logger.error(md.getMessage());
			response.setData("");
			response.setMessage(md.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("ShipController.getShipItemById() ended");
		return response;
	}

	/********************************** Delete ShipByID ***********************************/

	@RequestMapping(value = RestURIConstants.DELETE_SHIPMASTER_BY_ID, method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity deleteShipMasterById(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("shipId") int shipId) {

		ResponseEntity response = new ResponseEntity();
		try {
			logger.info("ShipController.deleteShipMasterByID() started");
			boolean flg = shipMasterServiceImpl.deleteShipMasterById(shipId);
			if(flg){
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.SHIP_NAME)+ " '"+
						shipId+"' " +MDProperties.read(MDMessageConstant.DELETE_SHIP_ITEM_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage("" + mde.getCause());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (MDNoRecordsFoundException md) {
			logger.error(md.getMessage());
			response.setData("");
			response.setMessage(md.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("ShipController.deleteShipMasterById() end");
		return response;
	}

	/******************************** UpdateShipItem **********************/

	@RequestMapping(value = RestURIConstants.UPDATE_SHIP_ITEMS, method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity updateShipItems(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody ShipMasterVo shipMasterVo) {

		ResponseEntity response = new ResponseEntity();
		boolean flg = false;
		try {
			logger.info("ShipController.updateShipItems() started");
			flg = shipMasterServiceImpl.updateShipItemDetails(shipMasterVo);
			if (flg) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.SHIP_NAME)+ " '"+
						shipMasterVo.getShipId()+"' " +MDProperties.read(MDMessageConstant.UPDATE_SHIP_ITEM_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			} else {
				response.setData("");
				response.setMessage("Ship Item not updated");
				response.setResponseCode(RestConstants.NOT_MODIFIED);
				response.setStatus(Status.Failure.getValue());
			}
		} catch (MDNoRecordsFoundException md) {
			logger.error(md.getMessage());
			response.setData("");
			response.setMessage(md.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("ShipController.updateShipItems() end");
		return response;
	}

	/********************************** Fetch ShippingItem Record by Invoice Id.  ***************/

	@RequestMapping(value = RestURIConstants.GET_SHIPPING_ITEM_BY_INVOICE_ID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getShippingListByInvoiceId(
			@RequestHeader(value = "Authentication") String authentication, 
			@RequestParam("invId") int invId){

		ResponseEntity response = new ResponseEntity();
		try {
			logger.info("ShipController.getShippingItemByInvoiceId started");
			List<ShipMasterVo> shipMasterList = shipMasterServiceImpl
					.getShippingListByInvoiceId(invId);
			if (null != shipMasterList && shipMasterList.size() > 0) {
				response.setData(shipMasterList);
				response.setMessage(MDProperties.read(MDMessageConstant.GET_SHIP_ITEM_BY_INVOICE_ID));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException md) {
			logger.error(md.getMessage());
			response.setData("");
			response.setMessage(md.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("ShipController.getShippingItemByInvoiceId ended");
		return response;
	}
	
	@RequestMapping(value = RestURIConstants.GET_SHIP_LIST_PAGINATION, method = RequestMethod.POST)
	public @ResponseBody ResponseEntity getShipListWithPagination(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody String criteria,
 @RequestParam int start,
			int noOfRecords) {

		logger.info("ShipController.getShipListWithPagination() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<ShipMasterVo> jobList = shipMasterServiceImpl
					.getShipPaginationList(criteria, start, noOfRecords);
			int count = shipMasterServiceImpl.getCount(criteria);
			if (null != jobList && count > 0) {
				response.setCount(count);
				response.setData(jobList);
				response.setMessage(MDProperties
						.read(MDMessageConstant.SHIP_LIST_CRITERIA_BASED));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (MDNoRecordsFoundException nre) {
			logger.error(nre.getMessage());
			response.setData("");
			response.setMessage(nre.getMessage());
			response.setResponseCode(RestConstants.NO_RECORDS);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getCause().getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("ShipController.getShipListWithPagination() end");
		return response;
	}
}
