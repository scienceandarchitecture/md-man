package com.meshdynamics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.ResourceAccessException;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.ResponseEntity;
import com.meshdynamics.common.RestConstants;
import com.meshdynamics.common.RestURIConstants;
import com.meshdynamics.common.Status;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.service.JobModelFlashDetailsServiceImpl;
import com.meshdynamics.vo.JobModelFlashDetailsVo;

@Controller
@RequestMapping("/jobModelFlash")
public class JobModelFlashDetailsController {

	private static final Logger logger = Logger
			.getLogger(JobModelFlashDetailsController.class);

	@Autowired
	JobModelFlashDetailsServiceImpl jobModelFlashDetailsServiceImpl;

	@RequestMapping(value = RestURIConstants.ADD_JOBMODELDETAILS, method = RequestMethod.POST, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity addJobModelFlashDetails(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody JobModelFlashDetailsVo flashDetailsVo) {
		
		logger.info("JobModelFlashDetailsController.addJobModelFlashDetails() started");
		ResponseEntity response = new ResponseEntity();
		try {
			boolean flag = jobModelFlashDetailsServiceImpl
					.addJobModelFlashDetails(flashDetailsVo);
			if (flag) {
				response.setData("");
				response.setMessage(MDProperties.read(
						MDMessageConstant.JOB_MODEL_FLASH_DETAILS_ADD_SUCCESS));
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		}catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (MDNoRecordsFoundException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(MDProperties.read
					(MDMessageConstant.NO_RECORD_EXCEPTION)
					+mde.getCause().getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch(ResourceAccessException re){
			logger.error(re.getCause().getMessage());
			response.setMessage(MDProperties.read
					(MDMessageConstant.CONNECTION_TIMED_OUT_EXCEPTION));
			response.setResponseCode(Constant.REQUEST_TIMEOUT_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("JobModelFlashDetailsController.addJobModelFlashDetails() end");
		return response;
	}

	@RequestMapping(value = RestURIConstants.UPDATE_JOBMODELDETAILS, method = RequestMethod.PUT, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity updateJobModelFlashDetails(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody JobModelFlashDetailsVo flashDetailsVo) {
		
		logger.info("JobModelFlashDetailsController.updateJobModelFlashDetails() started");
		ResponseEntity response = new ResponseEntity();
		try {
			boolean flag = jobModelFlashDetailsServiceImpl
					.updateJobModelFlashDetails(flashDetailsVo);
			if (flag) {
				response.setData("");
				response.setMessage(MDProperties.read
						(MDMessageConstant.JOB_MODEL_FLASH_DETAILS_UPDATE_SUCCESS));
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("JobModelFlashDetailsController.updateJobModelFlashDetails() end");
		return response;
	}

	@RequestMapping(value = RestURIConstants.GET_JOBMODELDETAILS_LIST, method = RequestMethod.GET, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity getJobModelFlashDetailsList(
			@RequestHeader(value = "Authentication") String authentication) {
		logger.info("JobModelFlashDetailsController.getJobModelFlashDetailsList() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<JobModelFlashDetailsVo> flashDetailsList = jobModelFlashDetailsServiceImpl
					.getjobModelFlashDetailsList();
			if (flashDetailsList != null && flashDetailsList.size() > 0) {
				response.setData(flashDetailsList);
				response.setMessage(MDProperties.read
						(MDMessageConstant.GET_JOB_MODEL_FLASH_DETAILS_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("JobModelFlashDetailsController.getJobModelFlashDetailsList() end");
		return response;
	}

	@RequestMapping(value = RestURIConstants.GET_JOBMODELDETAILS_BYID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getJobModelFlashDetailsById(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("jobmodelflashdetailsId") int jobmodelflashdetailsId) {
		logger.info("JobModelFlashDetailsController.getJobModelFlashDetailsById() started");
		ResponseEntity response = new ResponseEntity();
		try {
			JobModelFlashDetailsVo flashDetailsVo = jobModelFlashDetailsServiceImpl
					.getjobModelFlashDetailsById(jobmodelflashdetailsId);
			if (flashDetailsVo != null) {
				response.setData(flashDetailsVo);
				response.setMessage(MDProperties.read
						(MDMessageConstant.GET_JOB_MODEL_FLASH_DETAIL_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException mdn) {
			logger.error(mdn.getMessage());
			response.setData("");
			response.setMessage(MDProperties.read(MDMessageConstant.NO_RECORD_EXCEPTION));
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("JobModelFlashDetailsController.getJobModelFlashDetailsById() end");
		return response;
	}

	@RequestMapping(value = RestURIConstants.GET_JOBMODELDETAILS_BYJOBID_AND_MODELID, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity getJobModelFlashDetailsByJobIdAndModelId(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("jobId") int jobId, @RequestParam("jobItemId") int jobItemId) {
		logger.info("JobModelFlashDetailsController.getJobModelFlashDetailsByJobIdAndModelId() started");
		ResponseEntity response = new ResponseEntity();
		try {
			List<JobModelFlashDetailsVo> detailsByJobIdAndModelId = jobModelFlashDetailsServiceImpl
					.getjobModelFlashDetailsByJobIdAndModelId(jobId, jobItemId);
			if (detailsByJobIdAndModelId != null
					&& detailsByJobIdAndModelId.size() > 0) {
				response.setData(detailsByJobIdAndModelId);
				response.setMessage(MDProperties.read
						(MDMessageConstant.GET_JOB_FLASHDETAILS_BY_INVOICEID_MODELID));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDNoRecordsFoundException mdn) {
			logger.error(mdn.getMessage());
			response.setData("");
			response.setMessage(MDProperties.read(MDMessageConstant.NO_RECORD_EXCEPTION));
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("JobModelFlashDetailsController.getJobModelFlashDetailsByJobIdAndModelId() end");
		return response;
	}
	

	// Regenerate the Build
	@RequestMapping(value = RestURIConstants.REGENERATE_BUILD, method = RequestMethod.PUT, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity regenerateJobModelFlashDetails(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody JobModelFlashDetailsVo flashDetailsVo) {
		logger.info("JobModelFlashDetailsController.regenerateJobModelFlashDetails() started");
		ResponseEntity response = new ResponseEntity();
		try {
			boolean flag = jobModelFlashDetailsServiceImpl
					.regenerateBuild(flashDetailsVo);
			if (flag) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.REGENERATE_BUILD));
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("JobModelFlashDetailsController.regenerateJobModelFlashDetails() end");
		return response;
	}

	// multiple jobModelFlashDetails rest API
	@RequestMapping(value = RestURIConstants.ADD_JOBMODELDETAILS_LIST, method = RequestMethod.POST, consumes = RestConstants.MEDIA_TYPE)
	public @ResponseBody ResponseEntity addJobModelFlashDetailLists(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestBody String jobModelFlashList) {
		logger.info("JobModelFlashDetailsController.addJobModelFlashDetailLists() started");
		ResponseEntity response = new ResponseEntity();
		try {
			boolean flag = jobModelFlashDetailsServiceImpl
					.addJobModelFlashDetailList(jobModelFlashList);
			if (flag) {
				response.setData("");
				response.setMessage(MDProperties.read
						(MDMessageConstant.ADD_MULTIPLE_JOB_FLASH_SUCCESS));
				response.setResponseCode(RestConstants.CREATED);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("JobModelFlashDetailsController.addJobModelFlashDetailLists() end");
		return response;
	}
	
	@RequestMapping(value = RestURIConstants.VERIFY_MAC_ADDRESS, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity verifyMacAddress(
			@RequestHeader(value = "Authentication") String authentication,
			@RequestParam("boardMacAddress") String boardMacAddress) {
		logger.info("JobModelFlashDetailsController.verifyMacAddress() started");
		ResponseEntity response = new ResponseEntity();
		try {
			boolean flag = jobModelFlashDetailsServiceImpl
					.verifyMacAddress(boardMacAddress);
			if (flag) {
				response.setData("");
				response.setMessage(MDProperties.read(MDMessageConstant.VERIFY_MAC_ADDRESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (MDBusinessException mbe) {
			logger.error(mbe.getMessage());
			response.setData("");
			response.setMessage(mbe.getMessage());
			response.setResponseCode(Constant.BUSSINESS_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		} catch (MDDBException mde) {
			logger.error(mde.getCause().getMessage());
			response.setData("");
			response.setMessage(mde.getCause().getMessage());
			response.setResponseCode(mde.getCode());
			response.setStatus(Status.Failure.getValue());
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("JobModelFlashDetailsController.verifyMacAddress() end");
		return response;
	}
	
	//Rest API to get the download link of Build-server-image
	@RequestMapping(value = RestURIConstants.FETCH_BUILD_SERVER_IMAGE_DOWNLOAD_PATH, method = RequestMethod.GET)
	public @ResponseBody ResponseEntity fetchBuildServerImageDownloadPath(
			@RequestHeader(value = "Authentication") String authentication) {
		logger.info("JobModelFlashDetailsController.fetchBuildServerImageDownloadPath() started");
		ResponseEntity response = new ResponseEntity();
		try {
			String downloadPath = jobModelFlashDetailsServiceImpl
					.fetchImageDownloadPath();
			if (null != downloadPath) {
				response.setData(downloadPath);
				response.setMessage(MDProperties
						.read(MDMessageConstant.IMAGE_DOWNLOAD_PATH_SUCCESS));
				response.setResponseCode(RestConstants.OK);
				response.setStatus(Status.Success.getValue());
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setData("");
			response.setMessage(e.getMessage());
			response.setResponseCode(Constant.SYSTEM_EXCEPTION);
			response.setStatus(Status.Failure.getValue());
		}
		logger.info("JobModelFlashDetailsController.fetchBuildServerImageDownloadPath() end");
		return response;
	}
}
