/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : SubProduct.java
 * Comments : TODO 
 * Created  : Dec 16, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Dec 16, 2016  | Created                             |Pinki  			|
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "subProduct")
public class SubProduct implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "subproductName")
	private String subProductName;

	@Column(name = "subproductDescription")
	private String subProductDescription;

	/**
	 * @return the subProductName
	 */
	public String getSubProductName() {
		return subProductName;
	}

	/**
	 * @param subProductName
	 *            the subProductName to set
	 */
	public void setSubProductName(String subProductName) {
		this.subProductName = subProductName;
	}

	/**
	 * @return the subProductDescription
	 */
	public String getSubProductDescription() {
		return subProductDescription;
	}

	/**
	 * @param subProductDescription
	 *            the subProductDescription to set
	 */
	public void setSubProductDescription(String subProductDescription) {
		this.subProductDescription = subProductDescription;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((subProductDescription == null) ? 0 : subProductDescription
						.hashCode());
		result = prime * result
				+ ((subProductName == null) ? 0 : subProductName.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubProduct other = (SubProduct) obj;
		if (subProductDescription == null) {
			if (other.subProductDescription != null)
				return false;
		} else if (!subProductDescription.equals(other.subProductDescription))
			return false;
		if (subProductName == null) {
			if (other.subProductName != null)
				return false;
		} else if (!subProductName.equals(other.subProductName))
			return false;
		return true;
	}
}
