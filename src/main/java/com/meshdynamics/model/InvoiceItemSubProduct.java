/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : InvoiceItemSubProduct.java
 * Comments : TODO 
 * Created  : Dec 16, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Dec 16, 2016  | Created                             | Pinki  			|
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.model;

import java.io.Serializable;
import java.util.Comparator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "invoiceItemSubProduct")
public class InvoiceItemSubProduct implements Serializable,
		Comparator<InvoiceItemSubProduct> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "invoiceitemsubproductId")
	private int invoiceItemSubProductId;
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "invoiceItemId")
	private InvoiceItem invoiceItem;
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "subproductName")
	private SubProduct subProduct;

	@Column
	private int quantity;

	/**
	 * @return the invoiceItemSubProductId
	 */
	public int getInvoiceItemSubProductId() {
		return invoiceItemSubProductId;
	}

	/**
	 * @param invoiceItemSubProductId
	 *            the invoiceItemSubProductId to set
	 */
	public void setInvoiceItemSubProductId(int invoiceItemSubProductId) {
		this.invoiceItemSubProductId = invoiceItemSubProductId;
	}

	/**
	 * @return the invoiceItem
	 */
	public InvoiceItem getInvoiceItem() {
		return invoiceItem;
	}

	/**
	 * @param invoiceItem
	 *            the invoiceItem to set
	 */
	public void setInvoiceItem(InvoiceItem invoiceItem) {
		this.invoiceItem = invoiceItem;
	}

	/**
	 * @return the subProduct
	 */
	public SubProduct getSubProduct() {
		return subProduct;
	}

	/**
	 * @param subProduct
	 *            the subProduct to set
	 */
	public void setSubProduct(SubProduct subProduct) {
		this.subProduct = subProduct;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public int compare(InvoiceItemSubProduct o1, InvoiceItemSubProduct o2) {
		return (o1.getSubProduct().getSubProductName()).compareTo(o2
				.getSubProduct().getSubProductName());
	}
}
