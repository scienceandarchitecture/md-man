/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ForecastItem.java
 * Comments : TODO 
 * Created  : 13-Sep-2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |13-Sep-2016  | Created                             | Pinki  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/

package com.meshdynamics.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "forecastItem")
public class ForecastItem implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int forecastItemId;

	@Column
	private int quantity;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "fcastId")
	private ForecastMaster forecastMaster;

	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "psku", referencedColumnName = "psku")
	private Product product;

	/**
	 * @return the forecastItemId
	 */
	public int getForecastItemId() {
		return forecastItemId;
	}

	/**
	 * @param forecastItemId
	 *            the forecastItemId to set
	 */
	public void setForecastItemId(int forecastItemId) {
		this.forecastItemId = forecastItemId;
	}

	/**
	 * @return the forecastMaster
	 */
	public ForecastMaster getForecastMaster() {
		return forecastMaster;
	}

	/**
	 * @param forecastMaster
	 *            the forecastMaster to set
	 */
	public void setForecastMaster(ForecastMaster forecastMaster) {
		this.forecastMaster = forecastMaster;
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * @param product
	 *            the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
