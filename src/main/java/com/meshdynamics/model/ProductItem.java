/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ProductItem.java
 * Comments : TODO 
 * Created  : Sep 27, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |    Comment                       |     Author    |
 * ---------------------------------------------------------------------------
 * |  0  |Sep 27, 2016   |    Created                    	  |      Pinki	  |  
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.model;

import java.io.Serializable;
import java.util.Comparator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "productItem")
public class ProductItem implements Serializable,Comparator<ProductItem> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int productItemId;

	@Column
	private int quantity;

	
	 @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	 @JoinColumn(name = "psku", referencedColumnName = "psku")
	 private Product product;
	    
	/*@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "psku")
	private Product product;*/

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "rsku")  
	private RawInventory rawInventory;

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public RawInventory getRawInventory() {
		return rawInventory;
	}

	public void setRawInventory(RawInventory rawInventory) {
		this.rawInventory = rawInventory;
	}

	public int getProductItemId() {
		return productItemId;
	}

	public void setProductItemId(int productItemId) {
		this.productItemId = productItemId;
	}

	public int compare(InvoiceItem o1, InvoiceItem o2) {
		if(o1.getPsku().equals(o2.getPsku())){
			return o1.getInvoiceItemId()-o2.getInvoiceItemId();
		}else{
			return o1.getPsku().compareTo(o2.getPsku());
		}
	}
	
	@Override
	public int compare(ProductItem productItem1, ProductItem productItem2) {
		if(productItem1.getRawInventory().getRsku().equals(productItem2.getRawInventory().getRsku())){
			return productItem1.getProductItemId()-productItem2.getProductItemId();
		} else {
			return productItem1.getRawInventory().getRsku().compareTo(productItem2.getRawInventory().getRsku());
		}
	}
}
