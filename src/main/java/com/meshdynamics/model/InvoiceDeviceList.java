/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : InvoiceDeviceList.java
 * Comments : TODO 
 * Created  : 16-Sep-2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |16-Sep-2016  | Created                             | Pinki  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "invoiceDeviceList")
public class InvoiceDeviceList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column
	private String invoiceNumber;

	@Column
	private String macAddress;

	@Column
	private int shipId;
	
	/**
	 * 
	 * @return the invoice number
	 */
	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	/**
	 * @param invoiceNumber
	 *        the invoiceNumber to set
	 */
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	/**
	 * 
	 * @return the macAddress
	 */
	public String getMacAddress() {
		return macAddress;
	}

	/**
	 * 
	 * @param macAddress
	 * 			the macAddress to set
	 */
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	/**
	 * 
	 * @return the shipId
	 */
	public int getShipId() {
		return shipId;
	}

	/** 
	 * @param shipId
	 * 			the shipId to set
	 */
	public void setShipId(int shipId) {
		this.shipId = shipId;
	}

	/**
	 *  hashCode method
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((invoiceNumber == null) ? 0 : invoiceNumber.hashCode());
		result = prime * result
				+ ((macAddress == null) ? 0 : macAddress.hashCode());
		result = prime * result + shipId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvoiceDeviceList other = (InvoiceDeviceList) obj;
		if (invoiceNumber == null) {
			if (other.invoiceNumber != null)
				return false;
		} else if (!invoiceNumber.equals(other.invoiceNumber))
			return false;
		if (macAddress == null) {
			if (other.macAddress != null)
				return false;
		} else if (!macAddress.equals(other.macAddress))
			return false;
		if (shipId != other.shipId)
			return false;
		return true;
	}
}
