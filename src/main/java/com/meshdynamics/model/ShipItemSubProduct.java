/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ShipItemSubProduct.java
 * Comments : TODO 
 * Created  : Dec 21, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Dec 21, 2016  | Created                             |Pinki           |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.model;

import java.io.Serializable;
import java.util.Comparator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "shipItemSubProduct")
public class ShipItemSubProduct implements Serializable,Comparator<ShipItemSubProduct> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="shipitemsubproductId")
	private int shipItemSubProductId;

	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "shipItemId")
	private ShipItem shipItem;
	
	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "invoiceitemsubproductId")
	private InvoiceItemSubProduct invoiceItemSubProduct;

	/**
	 * @return the shipItemSubProductId
	 */
	public int getShipItemSubProductId() {
		return shipItemSubProductId;
	}

	/**
	 * @param shipItemSubProductId the shipItemSubProductId to set
	 */
	public void setShipItemSubProductId(int shipItemSubProductId) {
		this.shipItemSubProductId = shipItemSubProductId;
	}

	/**
	 * @return the shipItem
	 */
	public ShipItem getShipItem() {
		return shipItem;
	}

	/**
	 * @param shipItem the shipItem to set
	 */
	public void setShipItem(ShipItem shipItem) {
		this.shipItem = shipItem;
	}

	/**
	 * @return the invoiceItemSubProduct
	 */
	public InvoiceItemSubProduct getInvoiceItemSubProduct() {
		return invoiceItemSubProduct;
	}

	/**
	 * @param invoiceItemSubProduct the invoiceItemSubProduct to set
	 */
	public void setInvoiceItemSubProduct(InvoiceItemSubProduct invoiceItemSubProduct) {
		this.invoiceItemSubProduct = invoiceItemSubProduct;
	}

	@Override
	public int compare(ShipItemSubProduct item1, ShipItemSubProduct item2) {
		return item1.getInvoiceItemSubProduct().getInvoiceItemSubProductId()
				- item2.getInvoiceItemSubProduct().getInvoiceItemSubProductId();
	}
}
