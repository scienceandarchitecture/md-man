package com.meshdynamics.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "jobMaster")
public class JobMaster implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int jobId;

	@Transient
	@Column 
	private String custName;

	@Column
	private int invId;
	
	@Column
	private String state;

	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date created;

	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date acknowledged;

	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date finished;

	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date needBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date ackFinishBy;

	@Column
	private String createdBy;

	@Column
	private String ackBy;

	@Transient
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "UserName", referencedColumnName = "UserName")
	private UserRole userRole;

	// @OneToMany(cascade = CascadeType.ALL, mappedBy = "jobMaster", fetch =
	// FetchType.EAGER, targetEntity = JobItem.class)
	@Transient
	@Column
	private Set<JobItem> jobItems;
	
	@Transient
	private int generatedImage;
	
	@Transient
	private int imageToBeGenerated;
	

	public int getInvId() {
		return invId;
	}

	public void setInvId(int invId) {
		this.invId = invId;
	}
	
	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	/**
	 * @return the jobId
	 */
	public int getJobId() {
		return jobId;
	}

	/**
	 * @param jobId
	 *            the jobId to set
	 */
	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param created
	 *            the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	/**
	 * @return the acknowledged
	 */
	public Date getAcknowledged() {
		return acknowledged;
	}

	/**
	 * @param acknowledged
	 *            the acknowledged to set
	 */
	public void setAcknowledged(Date acknowledged) {
		this.acknowledged = acknowledged;
	}

	/**
	 * @return the finished
	 */
	public Date getFinished() {
		return finished;
	}

	/**
	 * @param finished
	 *            the finished to set
	 */
	public void setFinished(Date finished) {
		this.finished = finished;
	}

	/**
	 * @return the needBy
	 */
	public Date getNeedBy() {
		return needBy;
	}

	/**
	 * @param needBy
	 *            the needBy to set
	 */
	public void setNeedBy(Date needBy) {
		this.needBy = needBy;
	}

	/**
	 * @return the ackFinishBy
	 */
	public Date getAckFinishBy() {
		return ackFinishBy;
	}

	/**
	 * @param ackFinishBy
	 *            the ackFinishBy to set
	 */
	public void setAckFinishBy(Date ackFinishBy) {
		this.ackFinishBy = ackFinishBy;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the ackBy
	 */
	public String getAckBy() {
		return ackBy;
	}

	/**
	 * @param ackBy
	 *            the ackBy to set
	 */
	public void setAckBy(String ackBy) {
		this.ackBy = ackBy;
	}

	/**
	 * @return the userRole
	 */
	public UserRole getUserRole() {
		return userRole;
	}

	/**
	 * @param userRole
	 *            the userRole to set
	 */
	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	/**
	 * @return the jobItems
	 */
	public Set<JobItem> getJobItems() {
		return jobItems;
	}

	/**
	 * @param jobItems
	 *            the jobItems to set
	 */
	public void setJobItems(Set<JobItem> jobItems) {
		this.jobItems = jobItems;
	}

	public int getGeneratedImage() {
		return generatedImage;
	}

	public void setGeneratedImage(int generatedImage) {
		this.generatedImage = generatedImage;
	}

	public int getImageToBeGenerated() {
		return imageToBeGenerated;
	}

	public void setImageToBeGenerated(int imageToBeGenerated) {
		this.imageToBeGenerated = imageToBeGenerated;
	}
}
