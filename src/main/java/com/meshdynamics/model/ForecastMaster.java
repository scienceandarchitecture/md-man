/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ForecastMaster.java
 * Comments : TODO 
 * Created  : 13-Sep-2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |13-Sep-2016  | Created                             | Pinki  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/

package com.meshdynamics.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "forecastMaster")
public class ForecastMaster implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int fcastId;

	@Column
	private String salesMan;

	@Column
	private String custName;

	@Column
	private Date estDate;

	@Column
	private String notes;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "forecastMaster", fetch = FetchType.EAGER, targetEntity = ForecastItem.class)
	private Set<ForecastItem> forecastItems;

	/**
	 * @return the forecastItems
	 */
	public Set<ForecastItem> getForecastItems() {
		return forecastItems;
	}

	/**
	 * @param forecastItems
	 *            the forecastItems to set
	 */
	public void setForecastItems(Set<ForecastItem> forecastItems) {
		this.forecastItems = forecastItems;
	}

	/***
	 * 
	 * @return the fcastId
	 */
	public int getFcastId() {
		return fcastId;
	}

	/**
	 * 
	 * @param fcastId
	 *            the fcastId to set
	 */
	public void setFcastId(int fcastId) {
		this.fcastId = fcastId;
	}

	/***
	 * 
	 * @return the salesMan
	 */
	public String getSalesMan() {
		return salesMan;
	}

	/**
	 * @param salesMan
	 *            the salesMan to set
	 */
	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}

	/***
	 * 
	 * @return the custName
	 */
	public String getCustName() {
		return custName;
	}

	/**
	 * @param custname
	 *            the custName to set
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}

	/***
	 * 
	 * @return the estDate
	 */
	public Date getEstDate() {
		return estDate;
	}

	/**
	 * @param estdate
	 *            the estDate to set
	 */
	public void setEstDate(Date estDate) {
		this.estDate = estDate;
	}

	/***
	 * 
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
}
