/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : Inventory.java
 * Comments : TODO 
 * Created  : 13-Sep-2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |13-Sep-2016  | Created                             | Pinki  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/

package com.meshdynamics.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "inventory")
public class Inventory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column
	private String macAddress;

	@Column
	private String modelId;

	@Column
	private String serial;

	@Column
	private String firmwareVersion;

	@Column
	private int batchNumber;
	
	/***
	 * 
	 * @return the macAddress
	 */
	public String getMacAddress() {
		return macAddress;
	}

	/**
	 * 
	 * @param macAddress
	 *        the macAddress to set
	 */
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	/***
	 * 
	 * @return the modelId
	 */
	public String getModelId() {
		return modelId;
	}

	/**
	 * 
	 * @param modelId
	 *        the modelId to set
	 */
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	/***
	 * 
	 * @return the serial
	 */
	public String getSerial() {
		return serial;
	}

	/**
	 * 
	 * @param serial
	 *        the serial to set
	 */
	public void setSerial(String serial) {
		this.serial = serial;
	}

	/***
	 * 
	 * @return the firmwareVersion
	 */
	public String getFirmwareVersion() {
		return firmwareVersion;
	}

	/**
	 * 
	 * @param firmwareVersion
	 *        the firmwareVersion to set
	 */
	public void setFirmwareVersion(String firmwareVersion) {
		this.firmwareVersion = firmwareVersion;
	}

	/***
	 * 
	 * @return the batchNumber
	 */
	public int getBatchNumber() {
		return batchNumber;
	}

	/**
	 * 
	 * @param batchNumber
	 *        the BatchNumber to set
	 */
	public void setBatchNumber(int batchNumber) {
		this.batchNumber = batchNumber;
	}
	
	/**
	 * 
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + batchNumber;
		result = prime * result
				+ ((firmwareVersion == null) ? 0 : firmwareVersion.hashCode());
		result = prime * result
				+ ((macAddress == null) ? 0 : macAddress.hashCode());
		result = prime * result + ((modelId == null) ? 0 : modelId.hashCode());
		result = prime * result + ((serial == null) ? 0 : serial.hashCode());
		return result;
	}

	/***
	 * 
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Inventory other = (Inventory) obj;
		if (batchNumber != other.batchNumber)
			return false;
		if (firmwareVersion == null) {
			if (other.firmwareVersion != null)
				return false;
		} else if (!firmwareVersion.equals(other.firmwareVersion))
			return false;
		if (macAddress == null) {
			if (other.macAddress != null)
				return false;
		} else if (!macAddress.equals(other.macAddress))
			return false;
		if (modelId == null) {
			if (other.modelId != null)
				return false;
		} else if (!modelId.equals(other.modelId))
			return false;
		if (serial == null) {
			if (other.serial != null)
				return false;
		} else if (!serial.equals(other.serial))
			return false;
		return true;
	}

}
