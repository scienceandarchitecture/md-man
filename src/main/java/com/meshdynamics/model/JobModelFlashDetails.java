package com.meshdynamics.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "jobModelFlashDetails")
public class JobModelFlashDetails {

	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int jobmodelflashdetailsId;

	@Column
	private String macAddress;

	@Column
	private String status;

	@Column
	private String version;

	@Column
	private String serialNo;
	
	@Column(columnDefinition="BIT")
	private Integer imageType;

	@Column
	private String boardMacAddress;

	@Column
	private String downloadLink;
	
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "jobItemId", referencedColumnName = "jobItemId")
	private JobItem jobItem;
	
	@Column
	private String imageName;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date lastUpdatedTime;

	
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "jobId", referencedColumnName = "jobId")
	private JobMaster jobMaster;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "psku", referencedColumnName = "psku")
	private Product product;

	public int getJobmodelflashdetailsId() {
		return jobmodelflashdetailsId;
	}

	public void setJobmodelflashdetailsId(int jobmodelflashdetailsId) {
		this.jobmodelflashdetailsId = jobmodelflashdetailsId;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	
	public Integer getImageType() {
		return imageType;
	}

	public void setImageType(Integer imageType) {
		this.imageType = imageType;
	}

	public String getBoardMacAddress() {
		return boardMacAddress;
	}

	public void setBoardMacAddress(String boardMacAddress) {
		this.boardMacAddress = boardMacAddress;
	}

	public String getDownloadLink() {
		return downloadLink;
	}

	public void setDownloadLink(String downloadLink) {
		this.downloadLink = downloadLink;
	}

	public JobMaster getJobMaster() {
		return jobMaster;
	}

	public void setJobMaster(JobMaster jobMaster) {
		this.jobMaster = jobMaster;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public JobItem getJobItem() {
		return jobItem;
	}

	public void setJobItem(JobItem jobItem) {
		this.jobItem = jobItem;
	}
	public Date getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(Date lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
}
