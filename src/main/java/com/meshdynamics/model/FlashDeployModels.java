package com.meshdynamics.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "flashDeployModels")
public class FlashDeployModels {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int flashdeploymodelsId;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "psku", referencedColumnName = "psku")
	private Product product;
	
	@Column
	private int addressesPerUnit;
	
	@Column
	private String configFileName;
	
	@Column
	private String phyModes;
	
	@Column
	private String description;

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	public int getAddressesPerUnit() {
		return addressesPerUnit;
	}
	
	public void setAddressesPerUnit(int addressesPerUnit) {
		this.addressesPerUnit = addressesPerUnit;
	}

	public String getConfigFileName() {
		return configFileName;
	}

	public void setConfigFileName(String configFileName) {
		this.configFileName = configFileName;
	}

	public String getPhyModes() {
		return phyModes;
	}

	public void setPhyModes(String phyModes) {
		this.phyModes = phyModes;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getFlashdeploymodelsId() {
		return flashdeploymodelsId;
	}

	public void setFlashdeploymodelsId(int flashdeploymodelsId) {
		this.flashdeploymodelsId = flashdeploymodelsId;
	}
}
