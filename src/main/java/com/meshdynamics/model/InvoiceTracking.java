package com.meshdynamics.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "invoiceTrackingList")
public class InvoiceTracking implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int trackId;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "invId", referencedColumnName = "invId")
	private InvoiceMaster InvoiceMaster;

	@Column
	private String carrier;

	@Column
	private String trackingNumber;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "shipId", referencedColumnName = "shipId")
	private ShipMaster shipMaster;

	public int getTrackId() {
		return trackId;
	}

	public void setTrackId(int trackId) {
		this.trackId = trackId;
	}

	public InvoiceMaster getInvoiceMaster() {
		return InvoiceMaster;
	}

	public void setInvoiceMaster(InvoiceMaster invoiceMaster) {
		InvoiceMaster = invoiceMaster;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public ShipMaster getShipMaster() {
		return shipMaster;
	}

	public void setShipMaster(ShipMaster shipMaster) {
		this.shipMaster = shipMaster;
	}
}