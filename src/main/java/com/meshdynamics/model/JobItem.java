package com.meshdynamics.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "jobItem")
public class JobItem implements Serializable,Comparator<JobItem> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int jobItemId;
  
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "invoiceItemId")  
	private InvoiceItem invoiceItem;       

	@Column
	private int quantity;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "jobId")
	private JobMaster jobMaster;
	
	@Transient
	@Column
	private String psku;

	// @OneToMany(cascade = CascadeType.ALL, mappedBy = "jobItem", fetch =
	// FetchType.EAGER, targetEntity = JobItemSubProduct.class)
	@Transient
	@Column
	private Set<JobItemSubProduct> jobItemSubProducts;

	/**
	 * @return the psku
	 */
	public String getPsku() {
		return psku;
	}

	/**
	 * @param psku the psku to set
	 */
	public void setPsku(String psku) {
		this.psku = psku;
	}

	/**
	 * @return the invoiceItem
	 */
	public InvoiceItem getInvoiceItem() {
		return invoiceItem;
	}

	/**
	 * @param invoiceItem the invoiceItem to set
	 */
	public void setInvoiceItem(InvoiceItem invoiceItem) {
		this.invoiceItem = invoiceItem;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the jobMaster
	 */
	public JobMaster getJobMaster() {
		return jobMaster;
	}

	/**
	 * @param jobMaster
	 *            the jobMaster to set
	 */
	public void setJobMaster(JobMaster jobMaster) {
		this.jobMaster = jobMaster;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the jobItemId
	 */
	public int getJobItemId() {
		return jobItemId;
	}

	/**
	 * @param jobItemId
	 *            the jobItemId to set
	 */
	public void setJobItemId(int jobItemId) {
		this.jobItemId = jobItemId;
	}

	/**
	 * @return the jobItemSubProducts
	 */
	public Set<JobItemSubProduct> getJobItemSubProducts() {
		return jobItemSubProducts;
	}

	/**
	 * @param jobItemSubProducts
	 *            the jobItemSubProducts to set
	 */
	public void setJobItemSubProducts(Set<JobItemSubProduct> jobItemSubProducts) {
		this.jobItemSubProducts = jobItemSubProducts;
	}

	@Override
	public int compare(JobItem jobItem1, JobItem jobItem2) {
		if (jobItem1.getPsku().equals(jobItem2.getPsku())) {
			return jobItem1.getJobItemId()-jobItem2.getJobItemId();
		} else {
			return jobItem1.getPsku().compareTo(jobItem2.getPsku());
		}
	}
}
