/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : InvoiceItem.java
 * Comments : TODO 
 * Created  : 16-Sep-2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |16-Sep-2016  | Created                             | Pinki  		  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "invoiceItem")
public class InvoiceItem implements Serializable,Comparator<InvoiceItem>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvoiceItem() {

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int invoiceItemId;

	@Column
	private String psku;

	@Column
	private int quantity;

	@Column
	private double price;

	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name = "invid")
	private InvoiceMaster invoiceMaster;

	// @OneToMany(cascade = CascadeType.ALL, mappedBy = "invoiceItem", fetch =
	// FetchType.EAGER, targetEntity = InvoiceItemSubProduct.class)
	@Transient
	@Column
	private Set<InvoiceItemSubProduct> invoiceItemSubProducts;

	@Column
	private String extraDesc;

	/**
	 * 
	 * @return the psku
	 */
	public String getPsku() {
		return psku;
	}

	/**
	 * @param psku
	 *            the psku to set
	 */
	public void setPsku(String psku) {
		this.psku = psku;
	}

	/**
	 * 
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * 
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * 
	 * @param price
	 *            the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * 
	 * @return the extraDesc
	 */
	public String getExtraDesc() {
		return extraDesc;
	}

	/**
	 * @param extraDesc
	 *            the extraDesc to set
	 */
	public void setExtraDesc(String extraDesc) {
		this.extraDesc = extraDesc;
	}

	/**
	 * @return the invoiceItemId
	 */
	public int getInvoiceItemId() {
		return invoiceItemId;
	}

	/**
	 * @param invoiceItemId
	 *            the invoiceItemId to set
	 */
	public void setInvoiceItemId(int invoiceItemId) {
		this.invoiceItemId = invoiceItemId;
	}

	/**
	 * @return the invoiceItemSubProducts
	 */
	public Set<InvoiceItemSubProduct> getInvoiceItemSubProducts() {
		return invoiceItemSubProducts;
	}

	/**
	 * @param invoiceItemSubProducts
	 *            the invoiceItemSubProducts to set
	 */
	public void setInvoiceItemSubProducts(
			Set<InvoiceItemSubProduct> invoiceItemSubProducts) {
		this.invoiceItemSubProducts = invoiceItemSubProducts;
	}

	/**
	 * @return the invoiceMaster
	 */
	public InvoiceMaster getInvoiceMaster() {
		return invoiceMaster;
	}

	@Override
	public String toString() {
		return "InvoiceItem [invoiceItemId=" + invoiceItemId + ", psku=" + psku
				+ ", invoiceMaster=" + invoiceMaster.getInvId() + "]";
	}

	/**
	 * @param invoiceMaster
	 *            the invoiceMaster to set
	 */
	public void setInvoiceMaster(InvoiceMaster invoiceMaster) {
		this.invoiceMaster = invoiceMaster;
	}
	
	public int compare(InvoiceItem o1, InvoiceItem o2) {
		if(o1.getPsku().equals(o2.getPsku())){
			return o1.getInvoiceItemId()-o2.getInvoiceItemId();
		}else{
			return o1.getPsku().compareTo(o2.getPsku());
		}
	}
}