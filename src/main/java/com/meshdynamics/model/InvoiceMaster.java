/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : InvoiceMaster.java
 * Comments : TODO 
 * Created  : 15-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |15-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "invoiceMaster")
public class InvoiceMaster implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int invId;

	@Transient
	@Column
	private int invoicePaymentCount;
	
	@Transient
	@Column
	private int invoiceTrackingCount;
	
	@Column
	private byte status;

	@Column(precision = 2)
	private double amount;

	@Column
	private String custName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date invDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date dueDate;

	@Column
	private String billTo;

	@Column
	private String shipTo;

	@Column
	private String salesRep;

	@Column
	private String terms;

	@Column
	private String createdBy;

	@Column
	private double discount;

	@Column
	private double shipping;

	@Column
	private String poNumber;

	@Column
	private String comments;

	@Column
	private String closedBy;

	@Transient
	@Column
	private int termId;
	
	@Transient
	@Column
	private double balance;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "custId", referencedColumnName = "custId")
	private Customer customer;

	// @OneToMany(cascade = CascadeType.ALL, mappedBy = "invoiceMaster", fetch =
	// FetchType.EAGER, targetEntity = InvoiceItem.class)
	@Transient
	@Column
	private Set<InvoiceItem> invoiceItems;

	
	/**
	 * @return the termId
	 */
	public int getTermId() {
		return termId;
	}

	/**
	 * @param termId
	 *            the termId to set
	 */
	public void setTermId(int termId) {
		this.termId = termId;
	}

	/**
	 * @return the invoicePaymentCount
	 */
	public int getInvoicePaymentCount() {
		return invoicePaymentCount;
	}

	/**
	 * @param invoicePaymentCount
	 *            the invoicePaymentCount to set
	 */
	public void setInvoicePaymentCount(int invoicePaymentCount) {
		this.invoicePaymentCount = invoicePaymentCount;
	}

	/**
	 * @return the invoiceTrackingCount
	 */
	public int getInvoiceTrackingCount() {
		return invoiceTrackingCount;
	}

	/**
	 * @param invoiceTrackingCount
	 *            the invoiceTrackingCount to set
	 */
	public void setInvoiceTrackingCount(int invoiceTrackingCount) {
		this.invoiceTrackingCount = invoiceTrackingCount;
	}
	
	/**
	 * @return the invId
	 */
	public int getInvId() {
		return invId;
	}

	/**
	 * @param invId
	 *            the invId to set
	 */
	public void setInvId(int invId) {
		this.invId = invId;
	}

	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/**
	 * @return the custName
	 */
	public String getCustName() {
		return custName;
	}

	/**
	 * @param custName
	 *            the custName to set
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}

	/**
	 * @return the invDate
	 */
	public Date getInvDate() {
		return invDate;
	}

	/**
	 * @param invDate
	 *            the invDate to set
	 */
	public void setInvDate(Date invDate) {
		this.invDate = invDate;
	}

	/**
	 * @return the dueDate
	 */
	public Date getDueDate() {
		return dueDate;
	}

	/**
	 * @param dueDate
	 *            the dueDate to set
	 */
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	/**
	 * @return the billTo
	 */
	public String getBillTo() {
		return billTo;
	}

	/**
	 * @param billTo
	 *            the billTo to set
	 */
	public void setBillTo(String billTo) {
		this.billTo = billTo;
	}

	/**
	 * @return the shipTo
	 */
	public String getShipTo() {
		return shipTo;
	}

	/**
	 * @param shipTo
	 *            the shipTo to set
	 */
	public void setShipTo(String shipTo) {
		this.shipTo = shipTo;
	}

	/**
	 * @return the salesRep
	 */
	public String getSalesRep() {
		return salesRep;
	}

	/**
	 * @param salesRep
	 *            the salesRep to set
	 */
	public void setSalesRep(String salesRep) {
		this.salesRep = salesRep;
	}

	/**
	 * @return the terms
	 */
	public String getTerms() {
		return terms;
	}

	/**
	 * @param terms
	 *            the terms to set
	 */
	public void setTerms(String terms) {
		this.terms = terms;
	}

	/**
	 * @return the balance
	 */
	public double getBalance() {
		return balance;
	}

	/**
	 * @param balance
	 *            the balance to set
	 */
	public void setBalance(double balance) {
		this.balance = balance;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the discount
	 */
	public double getDiscount() {
		return discount;
	}

	/**
	 * @param discount
	 *            the discount to set
	 */
	public void setDiscount(double discount) {
		this.discount = discount;
	}

	/**
	 * @return the shipping
	 */
	public double getShipping() {
		return shipping;
	}

	/**
	 * @param shipping
	 *            the shipping to set
	 */
	public void setShipping(double shipping) {
		this.shipping = shipping;
	}

	/**
	 * @return the poNumber
	 */
	public String getPoNumber() {
		return poNumber;
	}

	/**
	 * @param poNumber
	 *            the poNumber to set
	 */
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *            the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the closedBy
	 */
	public String getClosedBy() {
		return closedBy;
	}

	/**
	 * @param closedBy
	 *            the closedBy to set
	 */
	public void setClosedBy(String closedBy) {
		this.closedBy = closedBy;
	}

	/**
	 * @return the invoiceItems
	 */
	public Set<InvoiceItem> getInvoiceItems() {
		return invoiceItems;
	}

	/**
	 * @param invoiceItems
	 *            the invoiceItems to set
	 */
	public void setInvoiceItems(Set<InvoiceItem> invoiceItems) {
		this.invoiceItems = invoiceItems;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer
	 *            the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}
}
