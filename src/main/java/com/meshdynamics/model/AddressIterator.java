package com.meshdynamics.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="addressIterator")
public class AddressIterator {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int macId;
	
	@Column
	private String macAddress;

	public int getMacId() {
		return macId;
	}

	public void setMacId(int macId) {
		this.macId = macId;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	
}
