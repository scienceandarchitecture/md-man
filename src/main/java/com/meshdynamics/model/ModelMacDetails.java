package com.meshdynamics.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "modelMacDetails")
public class ModelMacDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int modelmacdetailsId;

	@Column
	private String macAddress;
	
	@Column
	private byte macType;
	
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "jobmodelflashdetailsId", referencedColumnName = "jobmodelflashdetailsId")
	private JobModelFlashDetails jobmodelflashdetails;

	public byte getMacType() {
		return macType;
	}

	public void setMacType(byte macType) {
		this.macType = macType;
	}

	public int getModelmacdetailsId() {
		return modelmacdetailsId;
	}

	public void setModelmacdetailsId(int modelmacdetailsId) {
		this.modelmacdetailsId = modelmacdetailsId;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public JobModelFlashDetails getJobmodelflashdetails() {
		return jobmodelflashdetails;
	}

	public void setJobmodelflashdetails(JobModelFlashDetails jobmodelflashdetails) {
		this.jobmodelflashdetails = jobmodelflashdetails;
	}

}
