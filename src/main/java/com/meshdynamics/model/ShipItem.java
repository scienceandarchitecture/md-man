package com.meshdynamics.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "shipItem")
public class ShipItem implements Serializable,Comparator<ShipItem> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int shipItemId;
	
	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "shipId")
	private ShipMaster shipMaster; 
	
	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "psku", referencedColumnName = "psku")
	private Product product;
	
	@Transient
	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "description")
	private Product description;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "shipItem", fetch = FetchType.EAGER, targetEntity = ShipItemSubProduct.class)
	private Set<ShipItemSubProduct> shipItemSubProducts;
		
	@Column
	private int quantity;

	public int getShipItemId() {
		return shipItemId;
	}

	public void setShipItemId(int shipItemId) {
		this.shipItemId = shipItemId;
	}

	public ShipMaster getShipMaster() {
		return shipMaster;
	}

	public void setShipMaster(ShipMaster shipMaster) {
		this.shipMaster = shipMaster;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public Product getDescription() {
		return description;
	}

	public void setDescription(Product description) {
		this.description = description;
	}

	/**
	 * @return the shipItemSubProducts
	 */
	public Set<ShipItemSubProduct> getShipItemSubProducts() {
		return shipItemSubProducts;
	}

	/**
	 * @param shipItemSubProducts the shipItemSubProducts to set
	 */
	public void setShipItemSubProducts(Set<ShipItemSubProduct> shipItemSubProducts) {
		this.shipItemSubProducts = shipItemSubProducts;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		result = prime * result + quantity;
		result = prime * result + shipItemId;
		result = prime
				* result
				+ ((shipItemSubProducts == null) ? 0 : shipItemSubProducts
						.hashCode());
		result = prime * result
				+ ((shipMaster == null) ? 0 : shipMaster.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShipItem other = (ShipItem) obj;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		if (quantity != other.quantity)
			return false;
		if (shipItemId != other.shipItemId)
			return false;
		if (shipItemSubProducts == null) {
			if (other.shipItemSubProducts != null)
				return false;
		} else if (!shipItemSubProducts.equals(other.shipItemSubProducts))
			return false;
		if (shipMaster == null) {
			if (other.shipMaster != null)
				return false;
		} else if (!shipMaster.equals(other.shipMaster))
			return false;
		return true;
	}

	@Override
	public int compare(ShipItem shipItem1, ShipItem shipItem2) {
		if (shipItem1.getProduct().getPsku().equals(shipItem2.getProduct().getPsku())) {
			return shipItem1.getShipItemId() - shipItem2.getShipItemId();
		} else {
			return shipItem1.getProduct().getPsku()
					.compareTo(shipItem2.getProduct().getPsku());
		}
	}
}