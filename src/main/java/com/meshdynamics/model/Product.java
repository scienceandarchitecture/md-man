/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : Product.java
 * Comments : TODO 
 * Created  : 02-Aug-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |02-Aug-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class Product implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int productId;
	
	@Column(unique = true)
	private String psku;

	@Column
	private String description;

	@Column
	private int maxAppCount;

	@Column
	private int maxVlanCount;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "product", fetch = FetchType.EAGER, targetEntity = ProductItem.class)
	private Set<ProductItem> productItems;

	public String getPsku() {
		return psku;
	}

	public void setPsku(String psku) {
		this.psku = psku;
	}

	public Set<ProductItem> getProductItems() {
		return productItems;
	}

	public void setProductItems(Set<ProductItem> productItems) {
		this.productItems = productItems;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getMaxAppCount() {
		return maxAppCount;
	}

	public void setMaxAppCount(int maxAppCount) {
		this.maxAppCount = maxAppCount;
	}

	public int getMaxVlanCount() {
		return maxVlanCount;
	}

	public void setMaxVlanCount(int maxVlanCount) {
		this.maxVlanCount = maxVlanCount;
	}
	
	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + maxAppCount;
		result = prime * result + maxVlanCount;
		result = prime * result + productId;
		result = prime * result
				+ ((productItems == null) ? 0 : productItems.hashCode());
		result = prime * result + ((psku == null) ? 0 : psku.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (maxAppCount != other.maxAppCount)
			return false;
		if (maxVlanCount != other.maxVlanCount)
			return false;
		if (productId != other.productId)
			return false;
		if (productItems == null) {
			if (other.productItems != null)
				return false;
		} else if (!productItems.equals(other.productItems))
			return false;
		if (psku == null) {
			if (other.psku != null)
				return false;
		} else if (!psku.equals(other.psku))
			return false;
		return true;
	}
}
