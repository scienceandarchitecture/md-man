package com.meshdynamics.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "shipMaster")
public class ShipMaster implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int shipId;

	@Column
	private int custId;
	
	@Column
	private String state;

	@Column
	private Date created;

	@Column
	private Date acknowledged;

	@Column
	private Date finished;

	@Column
	private Date shipBy;

	@Column
	private String custName;

	@Column
	private String address;

	@Column
	private String phone;

	@Column
	private String instructions;

	@Column
	private Date commitBy;

	@Column
	private String createdBy;

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "invId")
	private InvoiceMaster invoiceMaster;

	@Column
	private String ackBy;

	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "jobId")
	private JobMaster jobMaster;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "shipMaster", fetch = FetchType.EAGER, targetEntity = ShipItem.class)
	private Set<ShipItem> shipItems;

	public Set<ShipItem> getShipItems() {
		return shipItems;
	}

	public void setShipItems(Set<ShipItem> shipItems) {
		this.shipItems = shipItems;
	}

	public int getShipId() {
		return shipId;
	}

	public void setShipId(int shipId) {
		this.shipId = shipId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getAcknowledged() {
		return acknowledged;
	}

	public void setAcknowledged(Date acknowledged) {
		this.acknowledged = acknowledged;
	}

	public Date getFinished() {
		return finished;
	}

	public void setFinished(Date finished) {
		this.finished = finished;
	}

	public Date getShipBy() {
		return shipBy;
	}

	public void setShipBy(Date shipBy) {
		this.shipBy = shipBy;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public Date getCommitBy() {
		return commitBy;
	}

	public void setCommitBy(Date commitBy) {
		this.commitBy = commitBy;
	}

	public InvoiceMaster getInvoiceMaster() {
		return invoiceMaster;
	}

	public void setInvoiceMaster(InvoiceMaster invoiceMaster) {
		this.invoiceMaster = invoiceMaster;
	}

	/*public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}*/

	public String getAckBy() {
		return ackBy;
	}

	public void setAckBy(String ackBy) {
		this.ackBy = ackBy;
	}

	public JobMaster getJobMaster() {
		return jobMaster;
	}

	public void setJobMaster(JobMaster jobMaster) {
		this.jobMaster = jobMaster;
	}
	
	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ackBy == null) ? 0 : ackBy.hashCode());
		result = prime * result
				+ ((acknowledged == null) ? 0 : acknowledged.hashCode());
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result
				+ ((commitBy == null) ? 0 : commitBy.hashCode());
		result = prime * result + ((created == null) ? 0 : created.hashCode());
		result = prime * result
				+ ((custName == null) ? 0 : custName.hashCode());
		result = prime * result
				+ ((finished == null) ? 0 : finished.hashCode());
		result = prime * result
				+ ((instructions == null) ? 0 : instructions.hashCode());
		result = prime * result
				+ ((invoiceMaster == null) ? 0 : invoiceMaster.hashCode());
		result = prime * result
				+ ((jobMaster == null) ? 0 : jobMaster.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((shipBy == null) ? 0 : shipBy.hashCode());
		result = prime * result + shipId;
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		/*result = prime * result
				+ ((userRole == null) ? 0 : userRole.hashCode());*/
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShipMaster other = (ShipMaster) obj;
		if (ackBy == null) {
			if (other.ackBy != null)
				return false;
		} else if (!ackBy.equals(other.ackBy))
			return false;
		if (acknowledged == null) {
			if (other.acknowledged != null)
				return false;
		} else if (!acknowledged.equals(other.acknowledged))
			return false;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (commitBy == null) {
			if (other.commitBy != null)
				return false;
		} else if (!commitBy.equals(other.commitBy))
			return false;
		if (created == null) {
			if (other.created != null)
				return false;
		} else if (!created.equals(other.created))
			return false;
		if (custName == null) {
			if (other.custName != null)
				return false;
		} else if (!custName.equals(other.custName))
			return false;
		if (finished == null) {
			if (other.finished != null)
				return false;
		} else if (!finished.equals(other.finished))
			return false;
		if (instructions == null) {
			if (other.instructions != null)
				return false;
		} else if (!instructions.equals(other.instructions))
			return false;
		if (invoiceMaster == null) {
			if (other.invoiceMaster != null)
				return false;
		} else if (!invoiceMaster.equals(other.invoiceMaster))
			return false;
		if (jobMaster == null) {
			if (other.jobMaster != null)
				return false;
		} else if (!jobMaster.equals(other.jobMaster))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (shipBy == null) {
			if (other.shipBy != null)
				return false;
		} else if (!shipBy.equals(other.shipBy))
			return false;
		if (shipId != other.shipId)
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		/*if (userRole == null) {
			if (other.userRole != null)
				return false;
		} else if (!userRole.equals(other.userRole))
			return false;*/
		return true;
	}
}
