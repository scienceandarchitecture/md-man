/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : JobItemSubProduct.java
 * Comments : TODO 
 * Created  : Dec 21, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Dec 21, 2016  | Created                             | Pinki          |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "jobItemSubProduct")
public class JobItemSubProduct implements Serializable,Comparator<JobItemSubProduct> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "jobitemsubproductId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int jobItemSubProductId;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "jobItemId", referencedColumnName = "jobItemId")
	private JobItem jobItem;

	@Column
	private int invoiceitemsubproductId;

	@Transient
	private List<InvoiceItemSubProduct> invoiceItemSubProductList;

	/**
	 * @return the jobItemSubProductId
	 */
	public int getJobItemSubProductId() {
		return jobItemSubProductId;
	}

	/**
	 * @param jobItemSubProductId
	 *            the jobItemSubProductId to set
	 */
	public void setJobItemSubProductId(int jobItemSubProductId) {
		this.jobItemSubProductId = jobItemSubProductId;
	}

	/**
	 * @return the jobItem
	 */
	public JobItem getJobItem() {
		return jobItem;
	}

	/**
	 * @param jobItem
	 *            the jobItem to set
	 */
	public void setJobItem(JobItem jobItem) {
		this.jobItem = jobItem;
	}

	/**
	 * @return the invoiceitemsubproductId
	 */
	public int getInvoiceitemsubproductId() {
		return invoiceitemsubproductId;
	}

	/**
	 * @param invoiceitemsubproductId
	 *            the invoiceitemsubproductId to set
	 */
	public void setInvoiceitemsubproductId(int invoiceitemsubproductId) {
		this.invoiceitemsubproductId = invoiceitemsubproductId;
	}

	public List<InvoiceItemSubProduct> getInvoiceItemSubProductList() {
		return invoiceItemSubProductList;
	}

	public void setInvoiceItemSubProductList(
			List<InvoiceItemSubProduct> invoiceItemSubProductList) {
		this.invoiceItemSubProductList = invoiceItemSubProductList;
	}

	@Override
	public int compare(JobItemSubProduct subProduct1, JobItemSubProduct subProduct2) {
		return subProduct1.getInvoiceitemsubproductId()-subProduct2.getInvoiceitemsubproductId();
	}
}
