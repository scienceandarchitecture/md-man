/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : InvoicePayment.java
 * Comments : TODO 
 * Created  : 16-Sep-2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |16-Sep-2016  | Created                             | Pinki  		  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "invoicePayment")
public class InvoicePayment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int payId;

	@Column
	private double amount;

	@Column
	private String notes;

	@Column
	private Date payDate;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "invId", referencedColumnName = "invId")
	private InvoiceMaster invoiceMaster;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "rpId", referencedColumnName = "rpId")
	private RawPayment rawPayment;

	/**
	 * 
	 * @return
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * 
	 * @param amount
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	public int getPayId() {
		return payId;
	}

	public void setPayId(int payId) {
		this.payId = payId;
	}

	public InvoiceMaster getInvoiceMaster() {
		return invoiceMaster;
	}

	public void setInvoiceMaster(InvoiceMaster invoiceMaster) {
		this.invoiceMaster = invoiceMaster;
	}

	public RawPayment getRawPayment() {
		return rawPayment;
	}

	public void setRawPayment(RawPayment rawPayment) {
		this.rawPayment = rawPayment;
	}
}