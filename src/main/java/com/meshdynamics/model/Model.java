package com.meshdynamics.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "model")
public class Model implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column
	private String modelId;
	
	@Column
	private String description;
	
	@Column(columnDefinition="Decimal(10,2),default '0.00'")
	private double domesticPrice;
	
	@Column(columnDefinition="Decimal(10,2),default '0.00'")
	private double intlPrice;
	
	@Column
	private String manfDescription;

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getDomesticPrice() {
		return domesticPrice;
	}

	public void setDomesticPrice(double domesticPrice) {
		this.domesticPrice = domesticPrice;
	}

	public double getIntlPrice() {
		return intlPrice;
	}

	public void setIntlPrice(double intlPrice) {
		this.intlPrice = intlPrice;
	}

	public String getManfDescription() {
		return manfDescription;
	}

	public void setManfDescription(String manfDescription) {
		this.manfDescription = manfDescription;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		long temp;
		temp = Double.doubleToLongBits(domesticPrice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(intlPrice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result
				+ ((manfDescription == null) ? 0 : manfDescription.hashCode());
		result = prime * result + ((modelId == null) ? 0 : modelId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Model other = (Model) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (Double.doubleToLongBits(domesticPrice) != Double
				.doubleToLongBits(other.domesticPrice))
			return false;
		if (Double.doubleToLongBits(intlPrice) != Double
				.doubleToLongBits(other.intlPrice))
			return false;
		if (manfDescription == null) {
			if (other.manfDescription != null)
				return false;
		} else if (!manfDescription.equals(other.manfDescription))
			return false;
		if (modelId == null) {
			if (other.modelId != null)
				return false;
		} else if (!modelId.equals(other.modelId))
			return false;
		return true;
	}
}