/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : JobMasterDao.java
 * Comments : TODO 
 * Created  : 04-Oct-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |04-Oct-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.List;
import java.util.Map;

import com.meshdynamics.model.JobMaster;

public interface JobMasterDao {

	boolean addJobOrders(JobMaster jobMaster) throws Exception;
	
	List<JobMaster> getJobOrders() throws Exception;
	
	public JobMaster getJobOrderById(int jobId) throws Exception;
	
	boolean updateJobOrder(JobMaster jobMaster) throws Exception;
	
	boolean deleteJobOrderByJobId(int jobId) throws Exception;
	
	List<JobMaster> getJobOrderList(Map<?, ?> criteria) throws Exception;
	
	public List<JobMaster> getJobOrderList(Map<?, ?> criteria, int start,
			int noOfRecords) throws Exception;

	int getCount(Map<?, ?> newMapCriteria) throws Exception;

	List<Integer> getJobOrderListById(int jobId) throws Exception;
}
