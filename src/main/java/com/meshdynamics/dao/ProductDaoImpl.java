/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ProductDaoImpl.java
 * Comments : TODO 
 * Created  : Sep 28, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |    Comment                       |     Author    |
 * ---------------------------------------------------------------------------
 * |  0  |Sep 28, 2016 |    Created                    	  |      Pinki	  |  
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.DBConstant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.helper.Helper;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.common.helper.MDSqlConstant;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.model.FlashDeployModels;
import com.meshdynamics.model.Product;
import com.meshdynamics.model.ProductItem;

public class ProductDaoImpl implements ProductDao {

	private static final Logger logger = Logger.getLogger(ProductDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public boolean addProduct(Product product) throws Exception {

		Product pr = null;
		Query query = null;
		boolean flg = false;
		Session session = null;
		Transaction tx = null;
		List<?> productList = null;
		Query flashDeployQuery = null;
		List<?> flashDeployModelsList = null;
		FlashDeployModels flashDeployModels = null;
		
		logger.info("ProductDaoImpl.addProduct() started");
		
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			// this part is for validation
			query = session.createQuery(MDSqlConstant.PRODUCT_PSKU_QUERY);
			productList = query.setParameter(DBConstant.PSKU,
					product.getPsku().trim()).list();
			if (null != productList && productList.size() > 0)
				throw new MDBusinessException(
						MDProperties
								.read(MDMessageConstant.PSKU_EXIST_EXCEPTION));

			pr = new Product();
			pr.setPsku(product.getPsku());
			pr.setDescription(product.getDescription());
			pr.setMaxAppCount(product.getMaxAppCount());
			pr.setMaxVlanCount(product.getMaxVlanCount());
			session.save(pr);
			for (ProductItem productItem : product.getProductItems()) {
				session.flush();
				session.save(productItem);
				// session.merge(productItem);
			}

			flashDeployQuery = session
					.createQuery(MDSqlConstant.FLASH_DEPLOY_CONFIG_QUERY);
			flashDeployModelsList = flashDeployQuery.setParameter(
					DBConstant.PSKU,product.getPsku().trim()).list();

			if (null != flashDeployModelsList
					&& flashDeployModelsList.size() > 0) {
				logger.info(MDProperties
						.read(MDMessageConstant.CONFIG_FILE_EXIST_EXCEPTION));
			} else {
				flashDeployModels = new FlashDeployModels();
				flashDeployModels.setProduct(product);
				if(product.getPsku().startsWith(Constant.MD1_SERIES)){
					flashDeployModels.setAddressesPerUnit(Integer.parseInt(MDProperties.read(Constant.MD1_SERIES_ADDRESS_PER_UNIT)));
				} else if(product.getPsku().startsWith(Constant.MD4_SERIES)){
					flashDeployModels.setAddressesPerUnit(Integer.parseInt(MDProperties.read(Constant.MD4_SERIES_ADDRESS_PER_UNIT)));
				} else if(product.getPsku().startsWith(Constant.MD6_SERIES)){
					flashDeployModels.setAddressesPerUnit(Integer.parseInt(MDProperties.read(Constant.MD6_SERIES_ADDRESS_PER_UNIT)));
				} else {
					flashDeployModels.setAddressesPerUnit(Integer.parseInt(Constant.ADDRESS_PER_UNIT));
				}
				flashDeployModels.setDescription(product.getDescription());
				flashDeployModels.setConfigFileName(product.getPsku()
						+ DBConstant.CONFIG_SUFFIX);
				flashDeployModels.setPhyModes(DBConstant.PHYSICAL_MODE);
				session.save(flashDeployModels);
			}

			tx.commit();
			flg = true;
		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he),
					he.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.info("ProductDaoImpl.addProduct() end");
		return flg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.dao.ProductDao#getProducts()
	 */

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> getProducts() throws Exception {

		Session session = null;
		List<Product> productList = null;
		logger.info("ProductDaoImpl.getProducts() started");
		try {
			session = sessionFactory.openSession();
			productList = session.createQuery(MDSqlConstant.PRODUCTS_LIST)
					.list();

			if (null == productList || productList.size() < 1)
				throw new MDBusinessException(
						MDProperties
								.read(MDMessageConstant.NO_PRODUCT_EXCEPTION));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(null != session){
			session.close();
			}
		}
		logger.info("ProductDaoImpl.getProducts() ends");
		return productList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.dao.ProductDao#deleteProductById(java.lang.String)
	 */
	@Override
	@Transactional
	public boolean deleteProductById(String psku) throws Exception {

		Object obj = null;
		Query query = null;
		Product product = null;
		Query innerQuery = null;
		List<?> list = null;
		Session session = null;
		Transaction tx = null;
		logger.info("ProductDaoImpl.deleteProductById() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			query = session.createQuery(MDSqlConstant.PRODUCT_PSKU_QUERY);
			query.setParameter(DBConstant.PSKU, psku);

			obj = query.uniqueResult();
			if (null != obj) {
				innerQuery = session
						.createSQLQuery(MDSqlConstant.DELETE_PRODUCT_INNER_QUERY
								+ psku + "'");

				list = innerQuery.list();
				if (null != list && list.size() > 0) {
					throw new MDBusinessException(
							MDProperties
									.read(MDMessageConstant.DELETE_PRODUCT_EXCEPTION));
				}

				product = (Product) obj;
				for (ProductItem productItem : product.getProductItems()) {
					session.delete(productItem);
				}
				session.delete(product);
				tx.commit();
				return true;
			}
			throw new MDNoRecordsFoundException(
					MDProperties.read(MDMessageConstant.NO_RECORD_EXCEPTION));

		} catch (HibernateException he) {
			if (null != tx){
				tx.rollback();
			}
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("ProductDaoImpl.deleteProductById() end");
		}
	}

	/*
	 * ProductDaoImpl.getProductById
	 * 
	 * @param psku
	 * 
	 * @return Product comments : TODO
	 */
	@Override
	public Product getProductById(String psku) throws Exception {

		Query query = null;
		Session session = null;
		List<?> list = null;
		logger.info("productController.getProductById() started");
		try {
			session = sessionFactory.openSession();
			query = session.createQuery(MDSqlConstant.PRODUCT_PSKU_QUERY);
			query.setParameter(DBConstant.PSKU, psku);

			list = query.list();

			if (null != list && list.size() > 0)
				return (Product) list.get(0);

			throw new MDNoRecordsFoundException(
					MDProperties.read(MDMessageConstant.NO_RECORD_EXCEPTION));

		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause().getMessage(),
					he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("productController.getProductById() ended");
		}
	}

	/*
	 * ProductDaoImpl.updateProduct
	 * 
	 * @param product
	 * 
	 * @return boolean comments : TODO
	 */
	@Override
	public boolean updateProduct(Product product) throws Exception {

		int count = 0;
		boolean flg = false;
		Query query = null;
		Query invoiceQuery = null;
		List<?> poPskuList = null;
		Transaction tx = null;
		Session session = null;
		logger.trace("ProductDaoImpl.updateProduct() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			
			invoiceQuery = session.createQuery(MDSqlConstant.INVOICE_ITEM_PSKU_QUERY);
			invoiceQuery.setParameter(DBConstant.PSKU, product.getPsku());
			poPskuList = invoiceQuery.list();
			
			if(null != poPskuList && poPskuList.size() > 0)
				throw new MDBusinessException(MDProperties.read(MDMessageConstant.UPDATE_PRODUCT_EXCEPTION));
			
			query = session.createQuery(MDSqlConstant.DELETE_PRODUCT_QUERY);
			query.setParameter(DBConstant.PSKU, product.getPsku());
			count = query.executeUpdate();
			
			if (count > 0) {
				session.flush();
				session.update(product);
				tx.commit();
				flg = true;
			}
		} catch (Exception e) {
			if (null != tx){
				tx.rollback();
			}
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.trace("ProductDaoImpl.updateProduct() end");
		return flg;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Product> getProductListUsingPagination(Map<?, ?> citeria, int start,
			int noOfRecords) throws Exception {

		int i = 1;
		String key = null;
		Query query = null;
		Object value = null;
		Session session = null;
		StringBuilder strQuery = null;
		List<Product> productList = null;
		logger.info("ProductDaoImpl.getProductListUsingPagination() started");
		try {
			session = sessionFactory.openSession();
			strQuery = new StringBuilder(MDSqlConstant.PRODUCTS_LIST);

			for (Entry<?, ?> entry : citeria.entrySet()) {
				if (i > 1) {
					strQuery = strQuery.append(" AND ");
				} else if (i == 1) {
					strQuery = strQuery.append(" where ");
				}
				key = (String) entry.getKey();
				value = entry.getValue();
				
				strQuery = strQuery.append(key + " LIKE " + "'%" + value + "%" + "'");
				i++;
			}
			query = session.createQuery(strQuery.toString());
			query.setFirstResult(start - 1);
			query.setMaxResults(noOfRecords);
			productList = query.list();
			
			if (null == productList || productList.size() < 1)
				throw new MDNoRecordsFoundException(
						MDProperties
								.read(MDMessageConstant.NO_PRODUCT_EXCEPTION));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("ProductDaoImpl.getProductListUsingPagination() end");
		return productList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getProductListByProductId(String psku) throws Exception {

		Query productQuery = null;
		Session session = null;
		List<String> productDbList = null;
		
		logger.info("ProductDaoImpl.getProductListByProductId() start");
		try{
			session = sessionFactory.openSession(); 
			productQuery = session.createQuery(MDSqlConstant.PRODUCT_BY_PRODUCT_ID_QUERY+"%"+psku+"%'");
			
			productDbList = productQuery.list();
			
			if (null == productDbList || productDbList.size() < 1)
				throw new MDNoRecordsFoundException(
						MDProperties
								.read(MDMessageConstant.NO_RECORD_EXCEPTION));

		}catch (HibernateException he) {
			logger.error(he.getMessage());
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
			logger.info("ProductDaoImpl.getProductListByProductId() end");
		}
		return productDbList;
	}

	@SuppressWarnings("unchecked")
	public int getCount(Map<?, ?> citeria) throws Exception {
		
		int i = 1,count = 0;
		String key = null;
		Query query = null;
		Object value = null;
		Session session = null;
		StringBuilder strQuery = null;
		List<Product> productList = null;
		logger.info("ProductDaoImpl.getCount() started");
		try {
			session = sessionFactory.openSession();
			strQuery = new StringBuilder(MDSqlConstant.PRODUCTS_LIST);

			for (Entry<?, ?> entry : citeria.entrySet()) {
				if (i > 1) {
					strQuery = strQuery.append(" AND ");
				} else if (i == 1) {
					strQuery = strQuery.append(" where ");
				}
				key = (String) entry.getKey();
				value = entry.getValue();
				
				strQuery = strQuery.append(key + " LIKE " + "'%" + value + "%" + "'");
				i++;
			}
			query = session.createQuery(strQuery.toString());
			productList = query.list();
			count = productList.size(); 
			
			if (null == productList || productList.size() < 1)
				throw new MDNoRecordsFoundException(
						MDProperties
								.read(MDMessageConstant.NO_PRODUCT_EXCEPTION));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("ProductDaoImpl.getCount() end");
		return count;
	}
}
