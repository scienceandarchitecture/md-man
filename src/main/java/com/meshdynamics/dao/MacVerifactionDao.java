package com.meshdynamics.dao;

import java.util.List;

public interface MacVerifactionDao {
	public List<Object[]> macVerification(String macAddress) throws Exception;

	public List<Object[]> macVerificationInMDMan(String boardMacAddress)
			throws Exception;

}
