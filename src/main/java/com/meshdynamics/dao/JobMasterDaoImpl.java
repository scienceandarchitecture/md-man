/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : JobMasterDaoImpl.java
 * Comments : TODO 
 * Created  : 04-Oct-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |04-Oct-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.common.DBConstant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.helper.Helper;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.common.helper.MDSqlConstant;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.model.InvoiceItem;
import com.meshdynamics.model.InvoiceItemSubProduct;
import com.meshdynamics.model.InvoiceMaster;
import com.meshdynamics.model.JobItem;
import com.meshdynamics.model.JobItemSubProduct;
import com.meshdynamics.model.JobMaster;
import com.meshdynamics.model.JobModelFlashDetails;

public class JobMasterDaoImpl implements JobMasterDao {

	private static final Logger LOGGER = Logger
			.getLogger(JobMasterDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	/**
	 *
	 * @param jobMaster
	 *            JobMaster
	 * @return boolean
	 * @throws Exception
	 * @see com.meshdynamics.dao.JobMasterDao#addJobOrders(JobMaster)
	 */
	@Override
	public boolean addJobOrders(JobMaster jobMaster) throws Exception {

		/**
		 * Field flg to return boolean value. session to create the session and
		 * get a physical connection with database object tx used to perform
		 * operation with session and DataBase operations
		 * 
		 */
		boolean flg = false;
		Session session = null;
		Transaction tx = null;

		LOGGER.info("JobMasterDaoImpl.addJobOrders() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			flg = verifyJobDetails(jobMaster);
			if(true == flg)
			{
			session.save(jobMaster);
			for (JobItem jobItem : jobMaster.getJobItems()) {
				jobItem.setJobMaster(jobMaster);
				session.clear();
				session.save(jobItem);

				if (null != jobItem) {
					for (JobItemSubProduct jobItemSubProduct : jobItem
							.getJobItemSubProducts()) {
						jobItemSubProduct.setJobItem(jobItem);
						session.clear();
						session.merge(jobItemSubProduct);
					}
				}
			}
			tx.commit();
			flg = true;
		}} catch (HibernateException he) {
			LOGGER.error(he.getMessage());
			if (null != tx)
				tx.rollback();
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		LOGGER.info("JobMasterDaoImpl.addJobOrders() end");
		return flg;
	}

	@SuppressWarnings("unchecked")
	private boolean verifyJobDetails(JobMaster jobMaster) throws Exception {
		
		Session session = null;
		Transaction tx = null;
		boolean flag = true;
		Query jobItemQuery = null;
		Query invoiceQuery = null;
		int invItemQuantity = 0;
		int uiJobItemQuantity = 0;
		int totalJobItemQuantity = 0;
		Set<JobItem> jobItemUIList = null;
		List<JobItem> jobItemlist = null;
		List<Integer> jobItemList = null;
		List<InvoiceItem> invoiceItemList = null;
		LOGGER.info("JobMasterDaoImpl.verifyJobDetails() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();

			invoiceQuery = session
					.createQuery(MDSqlConstant.INVOICE_ITEMS_QUERY);
			invoiceQuery.setParameter(DBConstant.INVOICE_ID,
					jobMaster.getInvId());

			invoiceItemList = invoiceQuery.list();
			for (InvoiceItem invoiceItem : invoiceItemList) {

				invItemQuantity = invoiceItem.getQuantity();

				jobItemUIList = jobMaster.getJobItems();
				jobItemlist = new ArrayList<JobItem>(jobItemUIList);

				for (JobItem jobItem : jobItemlist) {

					if ((jobItem.getInvoiceItem().getInvoiceItemId()) == (invoiceItem
							.getInvoiceItemId())) {
						int dbJobItemQuantity = 0;
						jobItemQuery = session
								.createQuery(MDSqlConstant.JOB_ITEM_QUANTITY_QUERY);

						jobItemQuery.setParameter(DBConstant.INVOICE_ITEM_ID,
								jobItem.getInvoiceItem().getInvoiceItemId());

						jobItemList = jobItemQuery.list();
						for (int i : jobItemList) {
							dbJobItemQuantity += i;
						}
						uiJobItemQuantity = jobItem.getQuantity();
						totalJobItemQuantity = dbJobItemQuantity
								+ uiJobItemQuantity;

						if (totalJobItemQuantity <= invItemQuantity) {
							flag = true;
						} else {
							throw new MDBusinessException(
									MDProperties
											.read(MDMessageConstant.ALREADY_CREATED_EXCEPTION));
						}
					} else
						continue;
				}
			}
		} catch (HibernateException he) {
			if (null != tx)
				tx.rollback();
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			if (null != tx)
				tx.rollback();
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		LOGGER.info("InvoiceMasterDaoImpl.verifyJobDetails() end");
		return flag;
	}

	/**
	 * @param
	 * @return List<JobMaster>
	 * @throws Exception
	 * @see com.meshdynamics.dao.JobMasterDao#getJobOrders()
	 */

	@SuppressWarnings({ "unchecked", "boxing", "rawtypes" })
	@Override
	public List<JobMaster> getJobOrders() throws Exception {

		/**
		 * Field session to create the session Field invoiceItemSubProduct is
		 * used to get the particular subproductItem from
		 * invoiceItemSubProducts. invoiceItemSubProducts is used to get list of
		 * all invoiceItemSubProduct. invoiceItemSubProductList is used to
		 * create a arrayList and add the invoiceItemSubProduct into it.
		 * jobItemListFromDB returns a list of jobItem which are stored in
		 * database. jobItemSubProductList return the list of jobItemSubProduct.
		 * jobList returns the arrayList of all Jobs. jobMasterList returns a
		 * list from jobMaster. query,invoiceItemSubProductQuery and subQuery
		 * objects are used to hold the values for respective queries.
		 */
		Session session = null;
		final InvoiceItemSubProduct invoiceItemSubProduct = null;
		final List<InvoiceItemSubProduct> invoiceItemSubProducts = null;
		final List<InvoiceItemSubProduct> invoiceItemSubProductList = null;
		List<JobItem> jobItemListFromDB = null;
		List<JobItemSubProduct> jobItemSubProductList = null;
		final List<JobMaster> jobList = new ArrayList<>();
		List<JobMaster> jobMasterList = null;
		final Query query;
		final Query subQuery;
		Query custIdQuery = null;
		Query jobModelFlashQuery = null;
		Query invoiceItemSubProductQuery = null;
		Query jobItemQuery = null,pskuQuery = null;
		String custName = null,psku = null;

		LOGGER.info("JobMasterDaoImpl.getJobOrders() started");
		try {
			session = sessionFactory.openSession();
			query = session.createQuery(MDSqlConstant.JOB_LIST_QUERY);
			jobMasterList = query.list();
			subQuery = session.createQuery(MDSqlConstant.JOB_SUB_QUERY);

			if (null != jobMasterList && jobMasterList.size() > 0) {
				for (JobMaster jobMaster : jobMasterList) {

					int totalFLashingQuantity = 0;
					int imageToBeGenerated = 0;
					int generatedImageCount = 0;
					
					custIdQuery = session
							.createQuery(MDSqlConstant.INVOICE_MASTER_QUERY);
					custIdQuery.setParameter(DBConstant.INVOICE_ID,
							jobMaster.getInvId());

					List<InvoiceMaster> custIdList = custIdQuery.list();
					if (null != custIdList && custIdList.size() > 0)
						custName = custIdList.get(0).getCustName();

					jobItemQuery = session
							.createQuery(MDSqlConstant.JOB_ITEM_QUERY);
					jobItemQuery.setParameter(DBConstant.JOB_ID,
							jobMaster.getJobId());
					jobItemListFromDB = jobItemQuery.list();

					for(JobItem jobItem : jobItemListFromDB){
						Query jobItemQuantity = session.createQuery(MDSqlConstant.JOB_ITEMS_QUANTITY_QUERY);
						jobItemQuantity.setParameter(DBConstant.JOB_ITEM_ID, jobItem.getJobItemId());
						
						List<?> jobItemONEList = jobItemQuantity.list();
						if(jobItemONEList != null && jobItemONEList.size() > 0){
							totalFLashingQuantity = totalFLashingQuantity + Integer.valueOf(jobItemONEList.get(0).toString());
						}
					}
					
					jobModelFlashQuery = session.createQuery(MDSqlConstant.JOB_MODEL_FLASH_QUERY);
					jobModelFlashQuery.setParameter(DBConstant.JOB_ID,jobMaster.getJobId());

					List<JobModelFlashDetails> jobModelFlashList = jobModelFlashQuery
							.list();

					if (null != jobModelFlashList && jobModelFlashList.size() > 0) {

						for (JobModelFlashDetails jobModelFlashDetails : jobModelFlashList) {
							if (null != jobModelFlashDetails.getImageName()) {
								++generatedImageCount;
							}
						}
					}
					imageToBeGenerated = totalFLashingQuantity - generatedImageCount;
					
					for (JobItem jobItem : jobItemListFromDB) {
						psku = jobItem.getInvoiceItem().getPsku();
						pskuQuery = session
								.createQuery(MDSqlConstant.ITEM_PSKU_QUERY);
						pskuQuery.setParameter(DBConstant.PSKU, psku);

						invoiceItemSubProductQuery = session
								.createQuery(MDSqlConstant.JOB_ITEM_SUB_PRODUCT_QUERY);
						invoiceItemSubProductQuery.setParameter(
								DBConstant.JOB_ITEM_ID, jobItem.getJobItemId());

						jobItemSubProductList = invoiceItemSubProductQuery
								.list();

						getjobItemSubProduct(jobItemSubProductList,
								invoiceItemSubProducts, invoiceItemSubProduct,
								invoiceItemSubProductList, subQuery);

						jobItem.setPsku(psku);
						jobItem.setJobItemSubProducts(new HashSet(
								jobItemSubProductList));
					}
					jobMaster.setCustName(custName);
					jobMaster.setGeneratedImage(generatedImageCount);
					jobMaster.setImageToBeGenerated(imageToBeGenerated);
					jobMaster.setJobItems(new HashSet(jobItemListFromDB));
					jobList.add(jobMaster);
				}
				return jobList;
			}
			throw new MDNoRecordsFoundException(
					MDProperties.read(MDMessageConstant.NO_RECORD_EXCEPTION));

		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			LOGGER.info("JobMasterDaoImpl.getJobOrders() end");
		}
	}

	/**
	 * @param int jobId
	 * @return JobMaster
	 * @throws Exception
	 * @see com.meshdynamics.dao.JobMasterDao#getJobOrderById(int)
	 */

	@SuppressWarnings({ "unchecked", "boxing", "rawtypes" })
	@Override
	public JobMaster getJobOrderById(int jobId) throws Exception {

		/**
		 * session to create the session and get a physical connection with database Field.
		 * invoiceItemSubProduct is used to get the particular subproductItem from invoiceItemSubProducts.
		 * invoiceItemSubProducts is used to get list of all invoiceItemSubProduct.
		 * invoiceItemSubProductList is used to create a arrayList and add the invoiceItemSubProduct into it.
		 * jobItemListFromDB returns a list of jobItem which are stored in database.
		 * jobItemSubProductList return the list of jobItemSubProduct.
		 * jobItemSubProductQuery retrieves the persistence object.
		 * jobMaster returns the element from JobMasterList.
		 * list return the jobMaster's list query,jobItemQuery and subQuery,
		 * query objects are used to hold the values for respective queries.
		 */
		Session session = null;
		final InvoiceItemSubProduct invoiceItemSubProduct = null;
		JobMaster jobMaster = null;
		final List<JobMaster> list;
		final List<InvoiceItemSubProduct> invoiceItemSubProducts = null;
		final List<InvoiceItemSubProduct> invoiceItemSubProductList = null;
		List<JobItem> jobItemListFromDB = null;
		List<InvoiceMaster> custIdList = null;
		List<JobItemSubProduct> jobItemSubProductList = null;
		Query jobItemSubProductQuery = null,custIdQuery = null;
		final Query jobItemQuery;
		final Query subQuery;
		final Query query;
		int totalFLashingQuantity = 0;
		Query pskuQuery = null;
		int imageToBeGenerated = 0;
		int generatedImageCount = 0;
		Query jobModelFlashQuery = null;
		String custName = null,psku = null;

		LOGGER.info("JobMasterdaoImpl.getJobOrderById() started");
		try {
			session = sessionFactory.openSession();
			query = session.createQuery(MDSqlConstant.JOB_MASTER_ID_QUERY);
			query.setParameter(DBConstant.JOB_ID, jobId);
			list = query.list();

			if (null != list && list.size() > 0) {
				subQuery = session.createQuery(MDSqlConstant.JOB_SUB_QUERY);

				custIdQuery = session
						.createQuery(MDSqlConstant.INVOICE_MASTER_QUERY);
				custIdQuery.setParameter(DBConstant.INVOICE_ID, list.get(0)
						.getInvId());

				custIdList = custIdQuery.list();
				if (null != custIdList && custIdList.size() > 0)
					custName = custIdList.get(0).getCustName();

				jobMaster = (JobMaster) list.get(0);
				jobItemQuery = session
						.createQuery(MDSqlConstant.JOB_ITEM_QUERY);
				jobItemQuery.setParameter(DBConstant.JOB_ID, jobId);
				jobItemListFromDB = jobItemQuery.list();

				Query jobItemsQuery = session
						.createQuery(MDSqlConstant.JOB_ITEM_QUERY);
				jobItemsQuery.setParameter(DBConstant.JOB_ID,
						jobMaster.getJobId());

				List<JobItem> jobItemList = jobItemsQuery.list();

				for (JobItem jobItem : jobItemList) {
					Query jobItemQuantity = session
							.createQuery(MDSqlConstant.JOB_ITEMS_QUANTITY_QUERY);
					jobItemQuantity.setParameter(DBConstant.JOB_ITEM_ID,
							jobItem.getJobItemId());

					List<?> jobItemONEList = jobItemQuantity.list();
					if (jobItemONEList != null && jobItemONEList.size() > 0) {
						totalFLashingQuantity = totalFLashingQuantity
								+ Integer.valueOf(jobItemONEList.get(0)
										.toString());
					}
				}

				jobModelFlashQuery = session
						.createQuery(MDSqlConstant.JOB_MODEL_FLASH_QUERY);
				jobModelFlashQuery.setParameter(DBConstant.JOB_ID,
						jobMaster.getJobId());

				List<JobModelFlashDetails> jobModelFlashList = jobModelFlashQuery
						.list();

				if (null != jobModelFlashList && jobModelFlashList.size() > 0) {

					for (JobModelFlashDetails jobModelFlashDetails : jobModelFlashList) {
						if (null != jobModelFlashDetails.getImageName()) {
							++generatedImageCount;
						}
					}
				}
				imageToBeGenerated = totalFLashingQuantity
						- generatedImageCount;

				for (JobItem jobItem : jobItemListFromDB) {

					psku = jobItem.getInvoiceItem().getPsku();
					pskuQuery = session
							.createQuery(MDSqlConstant.ITEM_PSKU_QUERY);
					pskuQuery.setParameter(DBConstant.PSKU, psku);

					jobItemSubProductQuery = session
							.createQuery(MDSqlConstant.JOB_ITEM_SUB_PRODUCT_QUERY);
					jobItemSubProductQuery.setParameter(DBConstant.JOB_ITEM_ID,
							jobItem.getJobItemId());
					jobItemSubProductList = jobItemSubProductQuery.list();

					getjobItemSubProduct(jobItemSubProductList,
							invoiceItemSubProducts, invoiceItemSubProduct,
							invoiceItemSubProductList, subQuery);

					jobItem.setPsku(psku);
					jobItem.setJobItemSubProducts(new HashSet(
							jobItemSubProductList));
				}
				jobMaster.setCustName(custName);
				jobMaster.setGeneratedImage(generatedImageCount);
				jobMaster.setImageToBeGenerated(imageToBeGenerated);
				jobMaster.setJobItems(new HashSet(jobItemListFromDB));
				return jobMaster;
			}
			throw new MDNoRecordsFoundException(
					MDProperties.read(MDMessageConstant.NO_RECORD_EXCEPTION));

		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
			LOGGER.info("jobMasterdaoImpl.getJobOrderById() end");
		}
	}

	/**
	 * @param jobItemSubProductList
	 *            ,invoiceItemSubProducts,invoiceItemSubProduct,
	 *            invoiceItemSubProductList,subQuery
	 * @return doesn't return a value
	 * @throws Exception
	 * @see com.meshdynamics.dao.JobMasterDao#getjobItemSubProduct(
	 *      List<JobItemSubProduct>,
	 *      List<InvoiceItemSubProduct>,InvoiceItemSubProduct,
	 *      List<InvoiceItemSubProduct,Query)
	 */
	private void getjobItemSubProduct(
			List<JobItemSubProduct> jobItemSubProductList,
			List<InvoiceItemSubProduct> invoiceItemSubProducts,
			InvoiceItemSubProduct invoiceItemSubProduct,
			List<InvoiceItemSubProduct> invoiceItemSubProductList,
			Query subQuery) {

		LOGGER.info("JobMasterdaoImpl.getJobOrderById().getjobItemSubProduct() started");
		for (JobItemSubProduct jobItemSubProduct : jobItemSubProductList) {
			subQuery.setParameter(DBConstant.JOB_ITEM_SUB_PRODUCT_ID,
					jobItemSubProduct.getJobItemSubProductId());
			invoiceItemSubProducts = subQuery.list();

			if (null != invoiceItemSubProducts
					&& invoiceItemSubProducts.size() > 0) {
				invoiceItemSubProduct = invoiceItemSubProducts.get(0);
				invoiceItemSubProductList = new ArrayList<>();
				invoiceItemSubProductList.add(invoiceItemSubProduct);
			}
			jobItemSubProduct
					.setInvoiceItemSubProductList(invoiceItemSubProductList);
		}
		LOGGER.info("JobMasterdaoImpl.getJobOrderById().getjobItemSubProduct() end");
	}

	/**
	 * @param jobMaster
	 *            JobMaster
	 * @return boolean
	 * @throws Exception
	 * @see com.meshdynamics.dao.JobMasterDao#updateJobOrder(JobMaster)
	 */
	@SuppressWarnings({ "boxing", "unchecked" })
	@Override
	public boolean updateJobOrder(JobMaster jobMaster) throws Exception {

		/**
		 * Field flag to return boolean value. session to create the session and
		 * get a physical connection with database object tx used to perform
		 * operation with session and DataBase operations query object is used
		 * to hold the values for respective query. dbJobItemList returns the
		 * persisted jobItem's list uiItemList returns the list of jobItem which
		 * are coming from UI
		 * 
		 */
		boolean flag = false;
		Session session = null;
		Transaction tx = null;
		final Query query;
		int status = 3;
		int jobQuantity = 0;
		int jobFlashQuantity = 0;
		Query jobItemQuery = null;
		Query jobFlashQuery = null;
		List<?> shipOrderList = null;
		Query jobItemQuantityQuery = null;
		final List<JobItem> dbJobItemList;
		List<JobItem> jobItemIdList = null;
		final List<Integer> uiItemList = new ArrayList<Integer>();
		final Set<Integer> dbItemList = new HashSet<Integer>();

		LOGGER.info("JobMasterDaoImpl.updateJobOrder() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();

			jobItemQuery = session
					.createQuery(MDSqlConstant.JOB_ITEM_BASED_JOB_ID_QUERY);
			jobItemQuery.setParameter(DBConstant.JOB_ID, jobMaster.getJobId());

			jobItemIdList = jobItemQuery.list();

			// to calculate total jobOrders 
			for (JobItem jobItem : jobItemIdList) {
				jobItemQuantityQuery = session
						.createQuery(MDSqlConstant.JOB_ITEMS_QUANTITY_QUERY);
				jobItemQuantityQuery.setParameter(DBConstant.JOB_ITEM_ID,
						jobItem.getJobItemId());

				int quantity = (Integer.parseInt(jobItemQuantityQuery.list()
						.get(0).toString()));
				jobQuantity = jobQuantity + quantity;
			}

			// to calculate how much flashing has done for particular jobOrder!
			jobFlashQuery = session
					.createQuery(MDSqlConstant.JOB_MODEL_FLASH_COUNT_QUERY);
			jobFlashQuery.setParameter(DBConstant.JOB_ID, jobMaster.getJobId());
			jobFlashQuery.setParameter(DBConstant.STATUS,
					String.valueOf(status));

			jobFlashQuantity = Integer.parseInt(jobFlashQuery.list().get(0)
					.toString());

			if (jobMaster.getState().equalsIgnoreCase(DBConstant.FINISHED)
					&& (jobQuantity != jobFlashQuantity)) {
				throw new MDBusinessException(
						MDProperties
								.read(MDMessageConstant.JOB_CLOSE_EXCEPTION));
			} else if (jobQuantity == jobFlashQuantity
					&& !(jobMaster.getState().equals(DBConstant.FINISHED))) {
				throw new MDBusinessException(
						MDProperties
								.read(MDMessageConstant.JOB_UPDATE_IF_BUILD_DONE_EXCEPTION));
			}				
			
			// validation for JobOrder,is flashing has been done for the same or not
			if(jobQuantity != jobFlashQuantity){
			Query jobFlashCheckQuery = session.createQuery(MDSqlConstant.JOB_MODEL_FLASH_QUERY);
			jobFlashCheckQuery.setParameter(DBConstant.JOB_ID, jobMaster.getJobId());

			List<?> jobFlashCheckList = jobFlashCheckQuery.list();
			
			if (null != jobFlashCheckList && jobFlashCheckList.size() >= 1) {
				Query jobAcknowledgeQuery = session.createQuery(MDSqlConstant.JOB_MASTER_STATE_QUERY);
				jobAcknowledgeQuery.setParameter(DBConstant.JOB_ID,jobMaster.getJobId());
				
				List<?> jobStateList = jobAcknowledgeQuery.list();
				if(jobStateList.get(0).toString().equals(DBConstant.ACKNNOWLEDGED)) {
				
					throw new MDBusinessException(MDProperties.read(MDMessageConstant.JOB_FLASHING_INITIATED_EXCEPTION));
				}
			  }
			}
			
			query = session.createQuery(MDSqlConstant.JOB_ITEM_QUERY);
			query.setParameter(DBConstant.JOB_ID, jobMaster.getJobId());

			dbJobItemList = query.list();

			session.clear();
			session.update(jobMaster);
			if (null != dbJobItemList && dbJobItemList.size() > 0) {
				for (JobItem uiJobItem : jobMaster.getJobItems()) {
					uiItemList.add(uiJobItem.getJobItemId());
					for (JobItem dbJobItem : dbJobItemList) {

						jobItemUpdate(dbJobItem, uiJobItem, dbItemList,
								session, jobMaster);
					}
				}

				Query checkShipForJobQuery = session
						.createQuery(MDSqlConstant.SHIP_JOB_QUERY);
				shipOrderList = checkShipForJobQuery.setParameter(
						DBConstant.JOB_ID, jobMaster.getJobId()).list();
				if (null != shipOrderList && shipOrderList.size() > 0)
					throw new MDBusinessException(
							MDProperties
									.read(MDMessageConstant.SHIPPING_DONE_EXCEPTION));

				deleteAndUpdateJobItem(jobMaster, dbJobItemList, session,
						dbItemList, uiItemList);
			} else {
				for (JobItem jobItem : jobMaster.getJobItems()) {
					jobItem.setJobMaster(jobMaster);
					session.save(jobItem);
					for (JobItemSubProduct jobItemSubProduct : jobItem
							.getJobItemSubProducts()) {
						jobItemSubProduct.setJobItem(jobItem);
						session.clear();
						session.save(jobItemSubProduct);
					}
				}
			}
			tx.commit();
			flag = true;
		} catch (HibernateException he) {
			if (null != tx)
				tx.rollback();
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			if (null != tx)
				tx.rollback();
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		LOGGER.info("JobMasterDaoImpl.updateJobOrder() end");
		return flag;

	}

	/**
	 * @param jobMaster
	 *            ,dbJobItemList,session,dbItemList,uiItemList
	 * @return doesn't return anything
	 * @see 
	 *      com.meshdynamics.dao.JobMasterDao#deleteAndUpdateJobItem(JobMaster,List
	 *      <JobItem>,Set<Integer>,List<Integer>)
	 */
	private void deleteAndUpdateJobItem(JobMaster jobMaster,
			List<JobItem> dbJobItemList, Session session,
			Set<Integer> dbItemList, List<Integer> uiItemList) {

		/**
		 * Field deleteSubItemsQuery retrieves the persistence object.
		 */

		Query deleteSubItemsQuery = null;

		LOGGER.info("JobMasterDaoImpl.updateJobOrder().deleteAndUpdateJobItem() started");
		if (jobMaster.getJobItems().size() < dbJobItemList.size()) {
			dbItemList.removeAll(uiItemList);
			for (int jobItemId : dbItemList) {

				for (JobItem jobItem : dbJobItemList) {
					if (jobItemId == jobItem.getJobItemId()) {
						jobItem.setJobMaster(null);
						deleteSubItemsQuery = session
								.createQuery(MDSqlConstant.DELETE_JOB_QUERY);
						deleteSubItemsQuery.setParameter(DBConstant.JOB_ITEM_ID,
								jobItem.getJobItemId());
						deleteSubItemsQuery.executeUpdate();
						session.clear();
						session.delete(jobItem);
						break;
					}
				}
			}
		}
		LOGGER.info("JobMasterDaoImpl.updateJobOrder().deleteAndUpdateJobItem() end");
	}

	/**
	 * @param jobMaster
	 *            ,dbJobItem,uiJobItem,dbItemList,session
	 * @return boolean
	 * @throws Exception
	 * @see 
	 *      com.meshdynamics.dao.JobMasterDao#jobItemUpdate(JobItem,Set<Integer>,
	 *      Session,JobMaster)
	 */
	private void jobItemUpdate(JobItem dbJobItem, JobItem uiJobItem,
			Set<Integer> dbItemList, Session session, JobMaster jobMaster) {

		/**
		 * Field deleteJobItemSubProduct retrieves the persistence object.
		 */
		final Query deleteJobItemSubProduct;

		LOGGER.info("jobMasterDaoImpl.updateJobOrder.jobItemUpdate() started");
		dbItemList.add(dbJobItem.getJobItemId());
		if (uiJobItem.getJobItemId() == dbJobItem.getJobItemId()) {

			session.clear();
			session.update(uiJobItem);
			session.flush();

			deleteJobItemSubProduct = session.createQuery(MDSqlConstant.DELETE_JOB_QUERY);
			deleteJobItemSubProduct.setParameter(DBConstant.JOB_ITEM_ID,
					uiJobItem.getJobItemId());
			deleteJobItemSubProduct.executeUpdate();

			for (JobItemSubProduct jobItemSubProduct : uiJobItem
					.getJobItemSubProducts()) {
				session.clear();
				jobItemSubProduct.setJobItem(uiJobItem);
				session.save(jobItemSubProduct);
			}
		} else if (0 == uiJobItem.getJobItemId()) {
			uiJobItem.setJobMaster(jobMaster);
			session.save(uiJobItem);

			for (JobItemSubProduct jobItemSubProduct : uiJobItem
					.getJobItemSubProducts()) {

				jobItemSubProduct.setJobItem(uiJobItem);
				session.clear();
				session.save(jobItemSubProduct);
			}
		}
		LOGGER.info("jobMasterDaoImpl.updateJobOrder.jobItemUpdate() end");
	}

	/**
	 * @param int jobId
	 * @return boolean
	 * @throws Exception
	 * @see com.meshdynamics.dao.JobMasterDao#deleteJobOrderByJobId(int)
	 */

	@Override
	@SuppressWarnings({ "boxing", "unchecked" })
	public boolean deleteJobOrderByJobId(int jobId) throws Exception {

		/**
		 * Field list returns the records from shipMaster which is similar to @param
		 * jobId. session to create the session and get a physical connection
		 * with database object tx used to perform operation with session and
		 * DataBase operations
		 * query,deleteJobItemQuery,innerQuery,deleteSubProductQuery are used to
		 * hold the values for respective queries. jobItems returns the
		 * jobItem's list obj object holds the unique results from jobMaster
		 * corresponding to jobId
		 * 
		 */
		JobMaster jobMaster = null;
		List<?> list = null;
		List<?> flashList = null;
		List<JobItem> jobItems = null;
		Object obj = null;
		Query deleteJobItemQuery = null;
		Query innerQuery = null;
		Query query = null;
		Query deleteSubProductQuery = null;
		Query flashQuery = null;
		Session session = null;
		Transaction tx = null;

		LOGGER.info("JobMasterDaoImpl.deleteJobOrderById() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			query = session.createQuery(MDSqlConstant.JOB_MASTER_ID_QUERY);
			query.setParameter(DBConstant.JOB_ID, jobId);

			obj = query.uniqueResult();
			if (null != obj) {

				innerQuery = session
						.createSQLQuery(MDSqlConstant.INNER_DELETE_QUERY);
				innerQuery.setParameter(DBConstant.JOB_ID, jobId);

				list = innerQuery.list();
				if (null != list && list.size() > 0) {
					throw new MDBusinessException(MDProperties.
							read(MDMessageConstant.JOB_DELETE_EXCEPTION));
				}
				jobMaster = (JobMaster) obj;

				flashQuery = session.createQuery(MDSqlConstant.JOB_MODEL_FLASH_QUERY);
				flashQuery.setParameter(DBConstant.JOB_ID, jobId);
				flashList = flashQuery.list();
				if(null != flashList && flashList.size() > 0)
					throw new MDBusinessException(MDProperties.
							read(MDMessageConstant.JOB_USED_FOR_FLASHING_EXCEPTION));
				
				deleteJobItemQuery = session
						.createQuery(MDSqlConstant.JOB_ITEM_QUERY);
				deleteJobItemQuery.setParameter(DBConstant.JOB_ID, jobMaster.getJobId());
				jobItems = deleteJobItemQuery.list();

				deleteSubProductQuery = session
						.createQuery(MDSqlConstant.DELETE_JOB_QUERY);
				for (JobItem jobItem : jobItems) {
					deleteSubProductQuery.setParameter(DBConstant.JOB_ITEM_ID,
							jobItem.getJobItemId());
					deleteSubProductQuery.executeUpdate();
					jobItem.setJobMaster(null);
					session.delete(jobItem);
				}
				session.delete(jobMaster);
				tx.commit();
				return true;
			}
			throw new MDNoRecordsFoundException(MDProperties.
					read(MDMessageConstant.NO_RECORD_EXCEPTION));
		} catch (HibernateException he) {
			if (null != tx)
				tx.rollback();
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			LOGGER.info("JobMasterDaoImpl.deleteJobOrderById() end");
		}
	}

	/*
	 * JobMasterDaoImpl.getJobOrderList
	 * 
	 * @param key
	 * 
	 * @param value
	 * 
	 * @return List<JobMaster> comments : TODO
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<JobMaster> getJobOrderList(Map<?, ?> criteria) throws Exception {

		Session session = null;
		final InvoiceItemSubProduct invoiceItemSubProduct = null;
		final List<InvoiceItemSubProduct> invoiceItemSubProducts = null;
		final List<InvoiceItemSubProduct> invoiceItemSubProductList = null;
		List<JobItem> jobItemListFromDB = null;
		List<JobItemSubProduct> jobItemSubProductList = null;
		final List<JobMaster> jobList = new ArrayList<>();
		List<JobMaster> jobMasterList = null;
		List<InvoiceMaster> custIdList = null;
		final Query criteriaQuery;
		Query subQuery;
		Query pskuQuery = null;
		Query jobItemQuery = null;
		Query custIdQuery = null;
		Query jobModelFlashQuery = null;
		Query invoiceItemSubProductQuery = null;
		String psku = null, custName = null;
		StringBuilder strQuery = null;

		LOGGER.info("JobMasterDaoImpl.getJobOrderList() started");
		try {
			session = sessionFactory.openSession();

			strQuery = new StringBuilder(MDSqlConstant.JOB_GENERIC_QUERY);

			strQuery = mapCriteraList(strQuery, criteria);

			criteriaQuery = session.createQuery(strQuery.toString());
			jobMasterList = criteriaQuery.list();

			if (null != jobMasterList && jobMasterList.size() > 0) {
				for (JobMaster jobMaster : jobMasterList) {

					int totalFLashingQuantity = 0;
					int imageToBeGenerated = 0;
					int generatedImageCount = 0;

					subQuery = session.createQuery(MDSqlConstant.JOB_SUB_QUERY);

					custIdQuery = session
							.createQuery(MDSqlConstant.INVOICE_MASTER_QUERY);
					custIdQuery.setParameter(DBConstant.INVOICE_ID,
							jobMaster.getInvId());

					custIdList = custIdQuery.list();
					if (null != custIdList && custIdList.size() > 0)
						custName = custIdList.get(0).getCustName();

					jobItemQuery = session
							.createQuery(MDSqlConstant.JOB_ITEM_QUERY);
					jobItemQuery.setParameter(DBConstant.JOB_ID,
							jobMaster.getJobId());
					jobItemListFromDB = jobItemQuery.list();

					for (JobItem jobItem : jobItemListFromDB) {
						Query jobItemQuantity = session
								.createQuery(MDSqlConstant.JOB_ITEMS_QUANTITY_QUERY);
						jobItemQuantity.setParameter(DBConstant.JOB_ITEM_ID,
								jobItem.getJobItemId());

						List<?> jobItemONEList = jobItemQuantity.list();
						if (jobItemONEList != null && jobItemONEList.size() > 0) {
							totalFLashingQuantity = totalFLashingQuantity
									+ Integer.valueOf(jobItemONEList.get(0)
											.toString());
						}
					}

					jobModelFlashQuery = session
							.createQuery(MDSqlConstant.JOB_MODEL_FLASH_QUERY);
					jobModelFlashQuery.setParameter(DBConstant.JOB_ID,
							jobMaster.getJobId());

					List<JobModelFlashDetails> jobModelFlashList = jobModelFlashQuery
							.list();

					if (null != jobModelFlashList
							&& jobModelFlashList.size() > 0) {

						for (JobModelFlashDetails jobModelFlashDetails : jobModelFlashList) {
							if (null != jobModelFlashDetails.getImageName()) {
								++generatedImageCount;
							}
						}
					}
					imageToBeGenerated = totalFLashingQuantity
							- generatedImageCount;

					for (JobItem jobItem : jobItemListFromDB) {
						psku = jobItem.getInvoiceItem().getPsku();
						pskuQuery = session
								.createQuery(MDSqlConstant.ITEM_PSKU_QUERY);
						pskuQuery.setParameter(DBConstant.PSKU, psku);

						invoiceItemSubProductQuery = session
								.createQuery(MDSqlConstant.JOB_ITEM_SUB_PRODUCT_QUERY);
						invoiceItemSubProductQuery.setParameter(
								DBConstant.JOB_ITEM_ID, jobItem.getJobItemId());

						jobItemSubProductList = invoiceItemSubProductQuery
								.list();

						getjobItemSubProduct(jobItemSubProductList,
								invoiceItemSubProducts, invoiceItemSubProduct,
								invoiceItemSubProductList, subQuery);

						jobItem.setPsku(psku);
						jobItem.setJobItemSubProducts(new HashSet(
								jobItemSubProductList));
					}
					jobMaster.setCustName(custName);
					jobMaster.setGeneratedImage(generatedImageCount);
					jobMaster.setImageToBeGenerated(imageToBeGenerated);
					jobMaster.setJobItems(new HashSet(jobItemListFromDB));
					jobList.add(jobMaster);
				}
				return jobList;
			}
			throw new MDNoRecordsFoundException(
					MDProperties
							.read(MDMessageConstant.CRITERIA_BUSINESS_EXCEPTION));
		} catch (HibernateException he) {
			LOGGER.error(he.getMessage());
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
			LOGGER.info("JobMasterDaoImpl.getJobOrderList() end");
		}
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<JobMaster> getJobOrderList(Map<?, ?> criteria, int start,
			int noOfRecords) throws Exception{

		Session session = null;
		final InvoiceItemSubProduct invoiceItemSubProduct = null;
		final List<InvoiceItemSubProduct> invoiceItemSubProducts = null;
		final List<InvoiceItemSubProduct> invoiceItemSubProductList = null;
		List<JobItem> jobItemListFromDB = null;
		List<JobItemSubProduct> jobItemSubProductList = null;
		final List<JobMaster> jobList = new ArrayList<>();
		List<JobMaster> jobMasterList = null;
		final Query criteriaQuery;
		Query subQuery;
		Query pskuQuery = null;
		Query jobItemQuery = null;
		Query invoiceItemSubProductQuery = null;
		String psku = null;
		StringBuilder strQuery = null;

		LOGGER.info("JobMasterDaoImpl.getJobOrderList() started");
		try {
			session = sessionFactory.openSession();

			strQuery = new StringBuilder(MDSqlConstant.JOB_GENERIC_QUERY);
			
			strQuery = mapCriteraList(strQuery,criteria);

			criteriaQuery = session.createQuery(strQuery.toString());
			criteriaQuery.setFirstResult(start-1);
			criteriaQuery.setMaxResults(noOfRecords);
			jobMasterList = criteriaQuery.list();

			if (null != jobMasterList && jobMasterList.size() > 0) {
				for (JobMaster jobMaster : jobMasterList) {

					subQuery = session.createQuery(MDSqlConstant.JOB_SUB_QUERY);

					jobItemQuery = session
							.createQuery(MDSqlConstant.JOB_ITEM_QUERY);
					jobItemQuery.setParameter(DBConstant.JOB_ID,
							jobMaster.getJobId());
					jobItemListFromDB = jobItemQuery.list();

					for (JobItem jobItem : jobItemListFromDB) {
						psku = jobItem.getInvoiceItem().getPsku();
						pskuQuery = session
								.createQuery(MDSqlConstant.ITEM_PSKU_QUERY);
						pskuQuery.setParameter(DBConstant.PSKU, psku);

						invoiceItemSubProductQuery = session
								.createQuery(MDSqlConstant.JOB_ITEM_SUB_PRODUCT_QUERY);
						invoiceItemSubProductQuery.setParameter(
								DBConstant.JOB_ITEM_ID, jobItem.getJobItemId());

						jobItemSubProductList = invoiceItemSubProductQuery
								.list();

						getjobItemSubProduct(jobItemSubProductList,
								invoiceItemSubProducts, invoiceItemSubProduct,
								invoiceItemSubProductList, subQuery);

						jobItem.setPsku(psku);
						jobItem.setJobItemSubProducts(new HashSet(
								jobItemSubProductList));
					}
					jobMaster.setJobItems(new HashSet(jobItemListFromDB));
					jobList.add(jobMaster);
				}
				return jobList;
			}
			throw new MDNoRecordsFoundException(
					MDProperties
							.read(MDMessageConstant.CRITERIA_BUSINESS_EXCEPTION));
		} catch (HibernateException he) {
			LOGGER.error(he.getMessage());
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			LOGGER.info("JobMasterDaoImpl.getJobOrderList() end");
		}
	}

	private StringBuilder mapCriteraList(StringBuilder strQuery,
			Map<?, ?> criteria) throws Exception {

		int i = 1;
		String key = null;
		Object value = null;
		LOGGER.info("jobMasterDaoImpl.mapCriteraList() started");
		try {
			for (Entry<?, ?> entry : criteria.entrySet()) {
				if (i > 1) {
					strQuery = strQuery.append(MDMessageConstant.AND_CLAUSE);
				} else if (i == 1) {
					strQuery = strQuery.append(MDMessageConstant.WHERE_CLAUSE);
				}
				key = (String) entry.getKey();
				value = entry.getValue();
				strQuery = strQuery.append(key + "=" + "'" + value + "'");
				i++;
			}
		} catch (HibernateException he) {
			LOGGER.error(he.getMessage());
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw e;
		}
		LOGGER.info("jobMasterDaoImpl.mapCriteraList() end");
		return strQuery;
	}

	@Override
	@SuppressWarnings("unchecked")
	public int getCount(Map<?, ?> newMapCriteria) throws Exception {

		int count = 0;
		Session session = null;
		Query criteriaQuery = null;
		List<JobMaster> jobMasterList = null;
		StringBuilder strQuery = null;
		try {
			LOGGER.info("JobMasterDaoImpl.getCount() started");
			session = sessionFactory.openSession();
			strQuery = new StringBuilder(MDSqlConstant.JOB_GENERIC_QUERY);
			
			strQuery = mapCriteraList(strQuery,newMapCriteria);

			criteriaQuery = session.createQuery(strQuery.toString());
			jobMasterList = criteriaQuery.list();
			count = jobMasterList.size();

		} catch (HibernateException he) {
			LOGGER.error(he.getMessage());
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		LOGGER.info("JobMasterDaoImpl.getCount() end");
		return count;
	}
	
	@Override
	@SuppressWarnings({ "unchecked" })
	public List<Integer> getJobOrderListById(int jobId) throws Exception {

		Query jobQuery = null;
		Session session = null;
		List<Integer> JobOrderDbList = null;

		LOGGER.info("JobMasterDaoImpl.getJobOrderListById() started");
		try {
			session = sessionFactory.openSession();
			jobQuery = session.createQuery(MDSqlConstant.JOB_ORDER_ID_QUERY
					+ jobId + "%'");

			JobOrderDbList = jobQuery.list();

			if (null == JobOrderDbList || JobOrderDbList.size() < 1)
				throw new MDBusinessException(
						MDProperties
								.read(MDMessageConstant.NO_INVOICE_BUSINESS_EXCEPTION));

		} catch (HibernateException he) {
			LOGGER.error(he.getMessage());
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
			LOGGER.info("JobMasterDaoImpl.getJobOrderListById() end");
		}
		return JobOrderDbList;

	}
}
