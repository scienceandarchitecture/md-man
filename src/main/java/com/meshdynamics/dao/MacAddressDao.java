package com.meshdynamics.dao;

import java.util.List;
import java.util.Map;

import com.meshdynamics.vo.MacAddressSummaryVo;
import com.meshdynamics.vo.MacDetailsMetaResultVo;
import com.meshdynamics.vo.MacRangeVo;

public interface MacAddressDao {
	public List<MacAddressSummaryVo> getMacAddressSummaryList() throws Exception;

	public MacDetailsMetaResultVo fetchMacRangeList(MacRangeVo macRangeVo, int start,
			int noOfRecords) throws Exception;

	List<MacAddressSummaryVo> getMacAddressListUsingPagination(
			Map<?, ?> newMap, int start, int noOfRecords) throws Exception;

	List<MacAddressSummaryVo> getMacAddressListBasedSearchCritera(
			Map<?, ?> newMap, int start, int noOfRecords) throws Exception;

	MacDetailsMetaResultVo viewMacDetailsBasedOnCriteria(Map<?, ?> criteria)
			throws Exception;

}
