/*****************************************************************************
 * MeshDynamics 

 * -------------- 
 * File     : InvoiceMasterDaoImpl.java
 * Comments : TODO 
 * Created  : 15-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |15-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.DBConstant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.helper.Helper;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.common.helper.MDSqlConstant;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.model.Customer;
import com.meshdynamics.model.InvoiceItem;
import com.meshdynamics.model.InvoiceItemSubProduct;
import com.meshdynamics.model.InvoiceMaster;
import com.meshdynamics.model.Terms;

public class InvoiceMasterDaoImpl implements InvoiceMasterDao {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.meshdynamics.dao.InvoiceMasterDao#addInvoice(com.meshdynamics.model
	 * .InvoiceMaster)
	 */
	private static final Logger logger = Logger
			.getLogger(InvoiceMasterDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings({ "boxing", "unchecked" })
	@Transactional
	@Override
	public boolean addInvoice(InvoiceMaster invoiceMaster) throws Exception {
		
		Session session = null;
		Transaction tx = null;
		boolean flg = false;
		Terms term = null;
		Customer customer = null;
		Query customerQuery = null,termQuery = null;
		List<?> customerList = null;
		List<String> textlist = null;
		logger.info("InvoiceMasterDaoImpl.addInvoice() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();

			customerQuery = session.createQuery(MDSqlConstant.CUSTOMER_QUERY);
			customerQuery.setParameter(DBConstant.CUSTOMER_ID, 
							invoiceMaster.getCustomer().getCustId());

			customerList = customerQuery.list();
			if (null != customerList && customerList.size() > 0) {
				if (0 == invoiceMaster.getTermId()) {
					termQuery = session
							.createQuery(MDSqlConstant.TERM_QUERY);

					termQuery.setParameter(DBConstant.TEXT, invoiceMaster.getTerms()
							.trim());

					textlist = termQuery.list();
					if (null != textlist && textlist.size() > 0)
						throw new MDBusinessException(MDProperties.read(
								MDMessageConstant.TERMS_BUSINESS_EXCEPTION));

					term = new Terms();
					term.setText(invoiceMaster.getTerms());
					session.save(term);
				}
				customer = (Customer) customerList.get(0);
				invoiceMaster.setCustomer(customer);
				invoiceMaster.setCustName(customer.getName());

				session.save(invoiceMaster);
				for (InvoiceItem invoiceItem : invoiceMaster.getInvoiceItems()) {
					invoiceItem.setInvoiceMaster(invoiceMaster);
					session.save(invoiceItem);

					if (null != invoiceItem) {
						for (InvoiceItemSubProduct invoiceItemSubProduct : invoiceItem
								.getInvoiceItemSubProducts()) {
							invoiceItemSubProduct.setInvoiceItem(invoiceItem);
							session.merge(invoiceItemSubProduct);
						}
					}
				}
				tx.commit();
				flg = true;
			} else
				throw new MDBusinessException(MDProperties.read(
						MDMessageConstant.CUSTOMER_BUSINESS_EXCEPTION));
			
		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he),
					he.getMessage(), he.getCause());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(null != session){
			session.close();
			}
		}
		logger.info("InvoiceMasterDaoImpl.addInvoice() started");
		return flg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.dao.InvoiceMasterDao#getInvoices(int)
	 */
	@SuppressWarnings({ "unchecked", "boxing", "rawtypes" })
	@Override
	public List<InvoiceMaster> getInvoices() throws Exception {

		int invoiceId = 0,invoicePaymentCount = 0,
				invoiceTrackingCount = 0;
		double balance = 0,masterAmount = 0;
		Session session = null;
		Query trackingQuery = null,invoiceItemQuery = null,
			  invoiceItemSubProductQuery = null,invoiceQuery= null;
		
		List<InvoiceMaster> invoiceDbList = null;
		List<InvoiceItem> invoiceItemListFromDB = null;
		List<InvoiceMaster> invoiceList = new ArrayList<>();
		List<InvoiceItemSubProduct> invoiceItemSubProductList = null;
		
		logger.info("InvoiceMasterDaoImpl.getInvoices() started");

		try {
			session = sessionFactory.openSession();
			invoiceQuery = session.createQuery(MDSqlConstant.INVOICE_LIST_QUERY);
			invoiceDbList = invoiceQuery.list();
			if (null == invoiceDbList || invoiceDbList.size() < 1)
				throw new MDBusinessException(MDProperties.read
						(MDMessageConstant.NO_INVOICE_BUSINESS_EXCEPTION));

			for (InvoiceMaster invoiceMaster : invoiceDbList) {

				masterAmount = invoiceMaster.getAmount();
				invoiceId = invoiceMaster.getInvId();
				balance = getbalance(invoiceId, masterAmount);
				invoiceMaster.setBalance(balance);

				invoicePaymentCount = ((Long) (session.createQuery(
						MDSqlConstant.INVOICE_PAYMENT_COUNT_QUERY)
						.setParameter(DBConstant.INVOICE_ID, invoiceMaster.getInvId())
						.uniqueResult())).intValue();

				invoiceMaster.setInvoicePaymentCount(invoicePaymentCount);

				trackingQuery = session
						.createQuery(MDSqlConstant.INVOICE_TRACKING_QUERY);
				trackingQuery.setParameter(DBConstant.INVOICE_ID, invoiceMaster.getInvId());
				
				invoiceTrackingCount = ((Long) (session.createQuery(
						MDSqlConstant.INVOICE_TRACKING_COUNT_QUERY)
						.setParameter(DBConstant.INVOICE_ID, invoiceMaster.getInvId())
						.uniqueResult())).intValue();

				invoiceMaster.setInvoiceTrackingCount(invoiceTrackingCount);

				invoiceItemQuery = session
						.createQuery(MDSqlConstant.INVOICE_ITEMS_QUERY);
				invoiceItemQuery
						.setParameter(DBConstant.INVOICE_ID, invoiceMaster.getInvId());
				invoiceItemListFromDB = invoiceItemQuery
						.list();

				for (InvoiceItem invoiceItem : invoiceItemListFromDB) {
					invoiceItemSubProductQuery = session
							.createQuery(MDSqlConstant.INVOICE_ITEM_SUB_PRODUCT_QUERY);
					invoiceItemSubProductQuery.setParameter(DBConstant.INVOICE_ITEM_ID,
							invoiceItem.getInvoiceItemId());

					invoiceItemSubProductList = invoiceItemSubProductQuery.list();
					invoiceItem.setInvoiceItemSubProducts(new HashSet(
							invoiceItemSubProductList));
				}
				invoiceMaster
						.setInvoiceItems(new HashSet(invoiceItemListFromDB));
				invoiceList.add(invoiceMaster);
			}

		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he),
					he.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(null != session){
			session.close();
			}
		}
		logger.info("InvoiceMasterDaoImpl.getInvoices() ends");
		return invoiceList;
	}

	@SuppressWarnings("unchecked")
	private double getbalance(int invoiceId, double masterAmount) {

		Session session = null;
		Query rawQuery = null;
		List<Double> rawPaymentLists = null;
		double paymentNeed = 0,paymentAmount = 0;
		logger.info("InvoiceMasterDaoImpl.getBalance() started");
		try {
			session = sessionFactory.openSession();
			rawQuery = session
					.createSQLQuery(MDSqlConstant.INVOICE_PAYMENT_AMOUNT_QUERY);
			rawQuery.setParameter(DBConstant.INVOICE_ID, invoiceId);
			rawPaymentLists = rawQuery.list();

			for (double paidAmount : rawPaymentLists) {
				paymentAmount += paidAmount;
			}

			paymentNeed = masterAmount - paymentAmount;

		} catch (HibernateException he) {
			he.printStackTrace();
			throw he;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.info("InvoiceMasterDaoImpl.getBalance() end");
		return paymentNeed;
	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.dao.InvoiceMasterDao#getInvoiceById(int)
	 */
	@SuppressWarnings({ "boxing", "unchecked", "rawtypes" })
	@Override
	public InvoiceMaster getInvoiceById(int invId) throws Exception {
		
		Query query = null;
		List<?> list = null;
		double balance = 0;
		Session session = null;
		double masterAmount = 0;
		Query invoiceItemQuery = null;
		InvoiceMaster invoiceMaster = null;
		Query invoiceItemSubProductQuery = null;
		List<InvoiceItem> invoiceItemListFromDB = null;
		List<InvoiceItemSubProduct> invoiceItemSubProductList = null;

		logger.info("InvoiceMasterDaoImpl.getInvoiceById() started");
		
		try {
			session = sessionFactory.openSession();
			query = session
					.createQuery(MDSqlConstant.INVOICE_MASTER_QUERY);

			query.setParameter(DBConstant.INVOICE_ID, invId);
			list = query.list();
			
			if (null != list && list.size() > 0) {
				invoiceMaster = (InvoiceMaster) list.get(0);
				invoiceItemQuery = session
						.createQuery(MDSqlConstant.INVOICE_ITEM_QUERY);
				invoiceItemQuery.setParameter(DBConstant.INVOICE_ID, invId);
				invoiceItemListFromDB = invoiceItemQuery.list();

				masterAmount = invoiceMaster.getAmount();
				balance = getbalance(invId, masterAmount);
				invoiceMaster.setBalance(balance);

				for (InvoiceItem invoiceItem : invoiceItemListFromDB) {
					invoiceItemSubProductQuery = session
							.createQuery(MDSqlConstant.INVOICE_ITEM_SUB_PRODUCT_QUERY);
					invoiceItemSubProductQuery.setParameter(DBConstant.INVOICE_ITEM_ID,
							invoiceItem.getInvoiceItemId());
					invoiceItemSubProductList = invoiceItemSubProductQuery
							.list();

					invoiceItem.setInvoiceItemSubProducts(new HashSet(
							invoiceItemSubProductList));
				}
				invoiceMaster
						.setInvoiceItems(new HashSet(invoiceItemListFromDB));
				return invoiceMaster;
			}
			throw new MDNoRecordsFoundException(MDProperties.read
					(MDMessageConstant.NO_RECORD_EXCEPTION));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(null != session){
				session.close();
			}
			logger.info("InvoiceMasterDaoImpl.getInvoiceById() end");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.dao.InvoiceMasterDao#deleteInvoiceById(int)
	 */
	@SuppressWarnings({ "boxing", "unchecked" })
	@Override
	public boolean deleteInvoiceById(int invoiceId) throws Exception {

		Session session = null;
		Transaction tx = null;
		Object obj = null;
		List<?> list = null;
		List<InvoiceItem> invoiceItemList = null;
		InvoiceMaster invoiceMaster = null;
		Query query = null,innerQuery = null,invoiceItemQuery = null,
				deleteQuery = null;
		logger.info("InvoiceMasterDaoImpl.deleteInvoiceById() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			query = session
					.createQuery(MDSqlConstant.INVOICE_MASTER_QUERY);
			query.setParameter(DBConstant.INVOICE_ID, invoiceId);
			obj = query.uniqueResult();
			if (null != obj) {

				innerQuery = session.createSQLQuery(MDSqlConstant.INNER_QUERY + invoiceId);

				list = innerQuery.list();
				if (null != list && list.size() > 0) {
					throw new MDBusinessException(MDProperties.read
							(MDMessageConstant.PO_DELETE_BUSINESS_EXCEPTION));
				}

				invoiceMaster = (InvoiceMaster) obj;
				
				invoiceItemQuery = session
						.createQuery(MDSqlConstant.INVOICE_ITEM_QUERY);
				invoiceItemQuery.setParameter(DBConstant.INVOICE_ID, invoiceId);
				invoiceItemList = invoiceItemQuery.list();
				deleteQuery = session
						.createQuery(MDSqlConstant.DELETE_INVOICE_QUERY);
				for (InvoiceItem invoiceItem : invoiceItemList) {
					try {
						deleteQuery.setParameter(DBConstant.INVOICE_ITEM_ID, invoiceItem);
						deleteQuery.executeUpdate();
					} catch (Exception e) {
						e.printStackTrace();
						throw new MDBusinessException(MDProperties.read(
								MDMessageConstant.PO_DELETE_JOB_BUSINESS_EXCEPTION));
					}
					session.delete(invoiceItem);
				}
				session.delete(invoiceMaster);
				tx.commit();
				return true;
			}
			throw new MDNoRecordsFoundException(MDProperties.
					read(MDMessageConstant.NO_RECORDS_EXCEPTION)
					+ invoiceId);

		} catch (HibernateException he) {
			he.printStackTrace();
			if (null != tx)
				tx.rollback();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("InvoiceMasterDaoImpl.deleteInvoiceById() end");
		}
	}

	/*
	 * InvoiceMasterDaoImpl.updateInvoiceByID
	 * 
	 * @param invoiceMaster
	 * 
	 * @return boolean comments : TODO
	 */

	@SuppressWarnings({ "boxing" })
	@Transactional
	@Override
	public boolean updateInvoiceByID(InvoiceMaster invoiceMaster)
			throws Exception {

		Query poQuery = null;
		Session session = null;
		Transaction tx = null;
		boolean flag = false;
		List<?> poList = null;
		byte dbStatus = 0, uIStatus = 0;
		InvoiceMaster poDetails = null;
		logger.info("InvoiceMasterDaoImpl.updateInvoiceById() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();

			poQuery = session.createQuery(MDSqlConstant.INVOICE_MASTER_QUERY);
			poQuery.setParameter(DBConstant.INVOICE_ID,
					invoiceMaster.getInvId());

			poList = poQuery.list();
			if (poList != null && poList.size() > 0)
				poDetails = (InvoiceMaster) poList.get(0);

			uIStatus = invoiceMaster.getStatus();
			dbStatus = poDetails.getStatus();

			if ((uIStatus != dbStatus)) {
				flag = updatePurchaseOrder(invoiceMaster);
				return flag;
			} else {
				flag = updateInvoiceItem(session, tx, invoiceMaster);
			} 
			
		} catch (HibernateException he) {
			if (null != tx)
				tx.rollback();
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			if (null != tx)
				tx.rollback();
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.info("InvoiceMasterDaoImpl.updateInvoiceById() end");
		return flag;
	}

	@SuppressWarnings("unchecked")
	private boolean updateInvoiceItem(Session session, Transaction tx,
			InvoiceMaster invoiceMaster) throws Exception {

		boolean flag = false;
		List<?> jobList = null;
		Query invoiceItemQuery = null, jobQuery = null, deleteInvoiceItemSubProduct = null,
				deleteSubItemsQuery = null;
		List<Integer> uiItemList = new ArrayList<>();
		Set<Integer> dbItemList = new HashSet<>();
		List<InvoiceItem> dbInvoiceItemList = null;
		logger.info("InvoiceMasterDaoImpl.updateInvoiceByID.updateInvoiceItem() started");
		try {
			invoiceItemQuery = session
					.createQuery(MDSqlConstant.INVOICE_ITEM_QUERY);
			invoiceItemQuery.setParameter(DBConstant.INVOICE_ID,
					invoiceMaster.getInvId());
			dbInvoiceItemList = invoiceItemQuery.list();

			session.clear();
			invoiceMaster.setAmount(invoiceMaster.getAmount());
			session.update(invoiceMaster);
			session.flush();

			if (null != dbInvoiceItemList && dbInvoiceItemList.size() > 0) {
				for (InvoiceItem uiInvoiceItem : invoiceMaster
						.getInvoiceItems()) {
					uiItemList.add(uiInvoiceItem.getInvoiceItemId());
					for (InvoiceItem dbInvoiceItem : dbInvoiceItemList) {

						dbItemList.add(dbInvoiceItem.getInvoiceItemId());
						if (uiInvoiceItem.getInvoiceItemId() == dbInvoiceItem
								.getInvoiceItemId()) {	

							session.clear();
							session.update(uiInvoiceItem);
							session.flush();

							jobQuery = session
									.createQuery(MDSqlConstant.JOB_MASTER_QUERY);
							jobQuery.setParameter(DBConstant.INVOICE_ID,
									invoiceMaster.getInvId());

							jobList = jobQuery.list();
							if (null != jobList && jobList.size() > 0)
								throw new MDBusinessException(MDProperties.read(
										MDMessageConstant.JOB_CREATED_BUSINESS_EXCEPTION));
							
							deleteInvoiceItemSubProduct = session
									.createQuery(MDSqlConstant.DELETE_INVOICE_QUERY);
							deleteInvoiceItemSubProduct.setParameter(
									DBConstant.INVOICE_ITEM_ID,
									uiInvoiceItem.getInvoiceItemId());
							deleteInvoiceItemSubProduct.executeUpdate();

							for (InvoiceItemSubProduct invoiceItemSubProduct : uiInvoiceItem
									.getInvoiceItemSubProducts()) {
								session.clear();
								invoiceItemSubProduct
										.setInvoiceItem(uiInvoiceItem);
								session.save(invoiceItemSubProduct);
							}
						} else if (0 == uiInvoiceItem.getInvoiceItemId()) {
							uiInvoiceItem.setInvoiceMaster(invoiceMaster);
							session.save(uiInvoiceItem);

							for (InvoiceItemSubProduct invoiceItemSubProduct : uiInvoiceItem
									.getInvoiceItemSubProducts()) {

								invoiceItemSubProduct
										.setInvoiceItem(uiInvoiceItem);
								session.clear();
								session.save(invoiceItemSubProduct);
							}
						}
					}
				}
				if (invoiceMaster.getInvoiceItems().size() <= dbInvoiceItemList
						.size()) {
					dbItemList.removeAll(uiItemList); 
					for (int invoiceItemId : dbItemList) {
						for (InvoiceItem invoiceItem : dbInvoiceItemList) {
							if (invoiceItemId == invoiceItem.getInvoiceItemId()) {
								invoiceItem.setInvoiceMaster(null);
								deleteSubItemsQuery = session
										.createQuery(MDSqlConstant.DELETE_INVOICE_QUERY);
								deleteSubItemsQuery.setParameter(
										DBConstant.INVOICE_ITEM_ID,
										invoiceItem.getInvoiceItemId());
								deleteSubItemsQuery.executeUpdate();
								session.clear();
								session.delete(invoiceItem);
								session.flush();
								break;
							}
						}
					}
				}
			} else {
				for (InvoiceItem invoiceItem : invoiceMaster.getInvoiceItems()) {
					invoiceItem.setInvoiceMaster(invoiceMaster);
					session.save(invoiceItem);
					for (InvoiceItemSubProduct invoiceItemSubProduct : invoiceItem
							.getInvoiceItemSubProducts()) {
						invoiceItemSubProduct.setInvoiceItem(invoiceItem);
						session.clear();
						session.save(invoiceItemSubProduct);
					}
				}
			}
			tx.commit();
			flag = true;
		} catch (HibernateException he) {
			if (null != tx)
				tx.rollback();
			he.printStackTrace();
			throw he;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		logger.info("InvoiceMasterDaoImpl.updateInvoiceByID.updateInvoiceItem() end");
		return flag;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	private boolean updatePurchaseOrder(InvoiceMaster invoiceMaster)
			throws Exception {

		double invoicePaymentAmount = 0;
		String state = null, shipState = null;
		List<Double> invoicePaymentLists = null;
		Session session = null;
		Transaction tx = null;
		boolean flag = false;
		List<?> jobIdlist = null;
		List<?> jobIdAndStatelist = null;
		Query invQuery = null, jobIdQuery = null, jobStatequery = null, shipIdQuery = null, shipStateQuery = null;
		List<?> shipIdList = null;
		List<?> shipIdAndStateList = null;
		logger.info("invoiceMasterDaoImpl.updatePurchaseOrder() started");
		try {

			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			invQuery = session.createQuery(MDSqlConstant.PAYMENT_AMOUNT_QUERY);
			invQuery.setParameter(DBConstant.INVOICE_ID,
					invoiceMaster.getInvId());

			invoicePaymentLists = invQuery.list();

			for (double i : invoicePaymentLists) {
				invoicePaymentAmount += i;
			}

			if (invoicePaymentAmount == invoiceMaster.getAmount()) {
				jobIdQuery = session.createQuery(MDSqlConstant.JOB_ID_QUERY);
				jobIdQuery.setParameter(DBConstant.INVOICE_ID,
						invoiceMaster.getInvId());

				jobStatequery = session
						.createQuery(MDSqlConstant.JOB_STATE_QUERY);
				jobStatequery.setParameter(DBConstant.INVOICE_ID,
						invoiceMaster.getInvId());
				jobStatequery.setParameter(DBConstant.STATE,
						DBConstant.FINISHED);

				jobIdlist = jobIdQuery.list();
				jobIdAndStatelist = jobStatequery.list();

				if (null != jobIdlist && null != jobIdAndStatelist
						&& jobIdlist.size() > 0 && jobIdAndStatelist.size() > 0) {
					if (jobIdlist.size() == jobIdAndStatelist.size()) {
						shipIdQuery = session
								.createQuery(MDSqlConstant.SHIP_ID_QUERY);
						shipIdQuery.setParameter(DBConstant.INVOICE_ID,
								invoiceMaster.getInvId());

						shipStateQuery = session
								.createQuery(MDSqlConstant.SHIP_STATE_QUERY);
						shipStateQuery.setParameter(DBConstant.INVOICE_ID,
								invoiceMaster.getInvId());
						shipStateQuery.setParameter(DBConstant.STATE,
								DBConstant.FINISHED);

						shipIdList = shipIdQuery.list();
						shipIdAndStateList = shipStateQuery.list();
						if (null != shipIdList && null != shipIdAndStateList
								&& 0 < shipIdList.size()
								&& 0 < shipIdAndStateList.size()) {
							session.clear();
							invoiceMaster.setInvoiceItems(null);
							session.update(invoiceMaster);
							tx.commit();
							flag = true;
						} else {
							throw new MDBusinessException(
									MDProperties
											.read(MDMessageConstant.COUNT_JOB_SHIP_BUSINESS_EXCEPTION));
						}
					} else {
						throw new MDBusinessException(
								MDProperties
										.read(MDMessageConstant.PO_SHIPPING_BUSINESS_EXCEPTION));
					}
				} else {
					throw new MDBusinessException(
							MDProperties
									.read(MDMessageConstant.PO_JOB_BUSINESS_EXCEPTION));
				}
			} else {
				throw new MDBusinessException(
						MDProperties
								.read(MDMessageConstant.PO_PAYMENT_BUSINESS_EXCEPTION));
			}
		} catch (HibernateException he) {
			if (null != tx)
				tx.rollback();
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			if (null != tx)
				tx.rollback();
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("InvoiceMasterDaoImpl.updatePurchaseOrder() end");
		return flag;
	}

	/*
	 * InvoiceMasterDaoImpl.getPurchaseOrderList
	 * 
	 * @param key
	 * 
	 * @param value
	 * 
	 * @return String comments : TODO
	 */
	@Override
	@SuppressWarnings({ "boxing", "unchecked", "rawtypes" })
	public List<InvoiceMaster> getPurchaseOrderList(Map<?, ?> Crieria)
			throws Exception {

		int invoiceId = 0;
		double balance = 0;
		double masterAmount = 0;
		Session session = null;
		String key = null;
		Object value = null;
		int invoicePaymentCount = 0, invoiceTrackingCount = 0, i = 1;
		Query trackingQuery = null, invoiceItemQuery = null, criteriaQuery = null, invoiceItemSubProductQuery = null;
		StringBuilder strQuery = null;
		List<InvoiceMaster> invoiceDbList = null;
		List<InvoiceItem> invoiceItemListFromDB = null;
		List<InvoiceMaster> invoiceList = new ArrayList<>();
		List<InvoiceItemSubProduct> invoiceItemSubProductList = null;

		logger.info("InvoiceMasterDaoImpl.getPurchaseOrderList() started");
		try {
			session = sessionFactory.openSession();
			strQuery = new StringBuilder(MDSqlConstant.INVOICE_GENERIC_QUERY);

			for (Entry<?, ?> entry : Crieria.entrySet()) {
				if (i > 1) {
					strQuery = strQuery.append(" AND ");
				} else if (i == 1) {
					strQuery = strQuery.append(" where ");
				}
				key = (String) entry.getKey();
				value = entry.getValue();
				if (value.equals(Constant.UI_INVOICE_OPEN)) {
					value = (byte) Constant.DB_INVOICE_OPEN;
				} else if (value.equals(Constant.UI_INVOICE_CLOSE)) {
					value = (byte) Constant.DB_INVOICE_CLOSED;
				}
				strQuery = strQuery.append(key + "=" + "'" + value + "'");
				i++;
			}
			criteriaQuery = session.createQuery(strQuery.toString());
			invoiceDbList = criteriaQuery.list();
			

			if (null != invoiceDbList && invoiceDbList.size() > 0) {
				for (InvoiceMaster invoiceMaster : invoiceDbList) {

					masterAmount = invoiceMaster.getAmount();
					invoiceId = invoiceMaster.getInvId();
					balance = getbalance(invoiceId, masterAmount);
					invoiceMaster.setBalance(balance);

					invoicePaymentCount = ((Long) (session.createQuery(
							MDSqlConstant.INVOICE_PAYMENT_COUNT_QUERY)
							.setParameter(DBConstant.INVOICE_ID,
									((InvoiceMaster) invoiceMaster).getInvId())
							.uniqueResult())).intValue();

					((InvoiceMaster) invoiceMaster)
							.setInvoicePaymentCount(invoicePaymentCount);

					trackingQuery = session
							.createQuery(MDSqlConstant.INVOICE_TRACKING_QUERY);
					trackingQuery.setParameter(DBConstant.INVOICE_ID,
							((InvoiceMaster) invoiceMaster).getInvId());

					invoiceTrackingCount = ((Long) (session.createQuery(
							MDSqlConstant.INVOICE_TRACKING_COUNT_QUERY)
							.setParameter(DBConstant.INVOICE_ID,
									((InvoiceMaster) invoiceMaster).getInvId())
							.uniqueResult())).intValue();

					((InvoiceMaster) invoiceMaster)
							.setInvoiceTrackingCount(invoiceTrackingCount);

					invoiceItemQuery = session
							.createQuery(MDSqlConstant.INVOICE_ITEM_QUERY);
					invoiceItemQuery.setParameter(DBConstant.INVOICE_ID,
							((InvoiceMaster) invoiceMaster).getInvId());
					invoiceItemListFromDB = invoiceItemQuery.list();

					for (InvoiceItem invoiceItem : invoiceItemListFromDB) {
						invoiceItemSubProductQuery = session
								.createQuery(MDSqlConstant.INVOICE_ITEM_SUB_PRODUCT_QUERY);
						invoiceItemSubProductQuery.setParameter(
								DBConstant.INVOICE_ITEM_ID,
								invoiceItem.getInvoiceItemId());

						invoiceItemSubProductList = invoiceItemSubProductQuery
								.list();
						invoiceItem.setInvoiceItemSubProducts(new HashSet(
								invoiceItemSubProductList));
					}
					((InvoiceMaster) invoiceMaster)
							.setInvoiceItems(new HashSet(invoiceItemListFromDB));
					invoiceList.add((InvoiceMaster) invoiceMaster);
				}
				return invoiceList;
			}
			throw new MDNoRecordsFoundException(
					MDProperties
							.read(MDMessageConstant.CRITERIA_BUSINESS_EXCEPTION));
		} catch (HibernateException he) {
			logger.error(he.getMessage());
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
			logger.info("invoiceMasterDaoImpl.getPurchaseOrderList() end");
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<InvoiceMaster> getPurchaseOrderList(Map<?, ?> newMap,
			int start, int noOfRecords) throws Exception {

		int invoiceId = 0;
		double balance = 0, masterAmount = 0;
		Session session = null;
		int invoicePaymentCount = 0, invoiceTrackingCount = 0;
		Query trackingQuery = null, invoiceItemQuery = null, criteriaQuery = null, invoiceItemSubProductQuery = null;
		StringBuilder strQuery = null;
		List<InvoiceMaster> invoiceDbList = null;
		List<InvoiceItem> invoiceItemListFromDB = null;
		List<InvoiceMaster> invoiceList = new ArrayList<>();
		List<InvoiceItemSubProduct> invoiceItemSubProductList = null;

		logger.info("InvoiceMasterDaoImpl.getPurchaseOrderList() started");
		try {
			session = sessionFactory.openSession();
			strQuery = new StringBuilder(MDSqlConstant.INVOICE_GENERIC_QUERY);
			
			strQuery = mapCriteraList(strQuery,newMap);
			
			criteriaQuery = session.createQuery(strQuery.toString());
			criteriaQuery.setFirstResult(start - 1);
			criteriaQuery.setMaxResults(noOfRecords);
			invoiceDbList = criteriaQuery.list();
			
			if (null != invoiceDbList && invoiceDbList.size() > 0) {
				for (InvoiceMaster invoiceMaster : invoiceDbList) {

					masterAmount = invoiceMaster.getAmount();
					invoiceId = invoiceMaster.getInvId();
					balance = getbalance(invoiceId, masterAmount);
					invoiceMaster.setBalance(balance);

					invoicePaymentCount = ((Long) (session.createQuery(
							MDSqlConstant.INVOICE_PAYMENT_COUNT_QUERY)
							.setParameter(DBConstant.INVOICE_ID,
									((InvoiceMaster) invoiceMaster).getInvId())
							.uniqueResult())).intValue();

					((InvoiceMaster) invoiceMaster)
							.setInvoicePaymentCount(invoicePaymentCount);

					trackingQuery = session
							.createQuery(MDSqlConstant.INVOICE_TRACKING_QUERY);
					trackingQuery.setParameter(DBConstant.INVOICE_ID,
							((InvoiceMaster) invoiceMaster).getInvId());

					invoiceTrackingCount = ((Long) (session.createQuery(
							MDSqlConstant.INVOICE_TRACKING_COUNT_QUERY)
							.setParameter(DBConstant.INVOICE_ID,
									((InvoiceMaster) invoiceMaster).getInvId())
							.uniqueResult())).intValue();

					((InvoiceMaster) invoiceMaster)
							.setInvoiceTrackingCount(invoiceTrackingCount);

					invoiceItemQuery = session
							.createQuery(MDSqlConstant.INVOICE_ITEM_QUERY);
					invoiceItemQuery.setParameter(DBConstant.INVOICE_ID,
							((InvoiceMaster) invoiceMaster).getInvId());
					invoiceItemListFromDB = invoiceItemQuery.list();

					for (InvoiceItem invoiceItem : invoiceItemListFromDB) {
						invoiceItemSubProductQuery = session
								.createQuery(MDSqlConstant.INVOICE_ITEM_SUB_PRODUCT_QUERY);
						invoiceItemSubProductQuery.setParameter(
								DBConstant.INVOICE_ITEM_ID,
								invoiceItem.getInvoiceItemId());

						invoiceItemSubProductList = invoiceItemSubProductQuery
								.list();
						invoiceItem.setInvoiceItemSubProducts(new HashSet(
								invoiceItemSubProductList));
					}
					((InvoiceMaster) invoiceMaster)
							.setInvoiceItems(new HashSet(invoiceItemListFromDB));
					invoiceList.add((InvoiceMaster) invoiceMaster);
				}
				return invoiceList;
			}
			throw new MDNoRecordsFoundException(
					MDProperties
							.read(MDMessageConstant.CRITERIA_BUSINESS_EXCEPTION));
		} catch (HibernateException he) {
			logger.error(he.getMessage());
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
			logger.info("invoiceMasterDaoImpl.getPurchaseOrderList() end");
		}
	}

	@Override
	public int getCount(Map<?, ?> newMapCriteria) throws Exception {

		Session session = null;
		StringBuilder strQuery = null;
		int count = 0;
		Query criteriaQuery = null;
		List<?> invoiceDbList = null;

		logger.info("InvoiceMasterDaoImpl.getCount() started");
		try {
			session = sessionFactory.openSession();
			strQuery = new StringBuilder("SELECT count(*) FROM InvoiceMaster ");
			
			strQuery = mapCriteraList(strQuery,newMapCriteria);
			
			criteriaQuery = session.createQuery(strQuery.toString());
			invoiceDbList = criteriaQuery.list();
			if (null != invoiceDbList && invoiceDbList.size() > 0)
				count = Integer.valueOf(invoiceDbList.get(0).toString());

		} catch (HibernateException he) {
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
			logger.info("InvoiceMasterDaoImpl.getCount() end");
		}
		return count;
	}
	

	private StringBuilder mapCriteraList(StringBuilder strQuery,
			Map<?, ?> newMapCriteria) throws Exception {

		int i = 1;
		Date date = null;
		String key = null;
		Object value = null;
		int year = 0 , month = 0;
		String currentDate = null;
		Object poDateValue = null;
		String poDateFlagKey = null;
		
		logger.info("InvoiceMasterDaoImpl.mapCriteraList() started");
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			date = new Date();
			currentDate = dateFormat.format(date);
			
			boolean invdateflag = false;
			
			for (Entry<?, ?> entry : newMapCriteria.entrySet()) {
				if (i > 1) {
					strQuery = strQuery.append(" AND ");
				} else if (i == 1) {
					strQuery = strQuery.append(" where ");
				}

				key = (String) entry.getKey();
				value = entry.getValue();
				if (value.equals(Constant.UI_INVOICE_OPEN)) {
					value = (byte) Constant.DB_INVOICE_OPEN;
				} else if (value.equals(Constant.UI_INVOICE_CLOSE)) {
					value = (byte) Constant.DB_INVOICE_CLOSED;
				} else if (key.equals("invDate")) {

					try {
						poDateValue = newMapCriteria.get("poDateFlag");
						invdateflag = true;
					} catch (Exception ee) {
						logger.error("No parameters set for poDateFlag!");
					}
					if (invdateflag) {
						poDateFlagKey = "poDateFlag";
					}
					value = entry.getValue();

					String yearDateValue = value.toString();
					String ymd[] = yearDateValue.split("-");

					year = Integer.parseInt(ymd[0]);
					month = Integer.parseInt(ymd[1]);

					if (poDateValue.equals(1)) { // to get the current issue
													// date month results

						strQuery = strQuery.append(key
								+ MDMessageConstant.BETWEEN_CLAUSE + "'" + year
								+ "-" + month + "-01 00:00:00" + "'"
								+ MDMessageConstant.AND_CLAUSE + "'" + year
								+ "-" + month + "-31 00:00:00" + "'");
						break;
					}
					if (poDateValue.equals(2)) { // to get the current issue
													// date year results

						strQuery = strQuery.append(key
								+ MDMessageConstant.BETWEEN_CLAUSE + "'" + year
								+ "-" + month + "-01 00:00:00" + "'"
								+ MDMessageConstant.AND_CLAUSE + "'"
								+ currentDate + "'");
						break;
					}
				} else if (key.equals("dueDate")) {

					try {
						poDateValue = newMapCriteria.get("poDateFlag");
						invdateflag = true;
					} catch (Exception ee) {
						logger.error("No parameters set for poDateFlag!");
					}
					if (invdateflag) {
						poDateFlagKey = "poDateFlag";
						value = entry.getValue();

						String yearDateValue = value.toString();
						String ymd[] = yearDateValue.split("-");

						year = Integer.parseInt(ymd[0]);
						month = Integer.parseInt(ymd[1]);
						if (poDateValue != null) {
							if (poDateValue.equals(1)) { // to get the current
															// due date month
															// results

								strQuery = strQuery.append(key
										+ MDMessageConstant.BETWEEN_CLAUSE
										+ "'" + year + "-" + month
										+ "-01 00:00:00" + "'"
										+ MDMessageConstant.AND_CLAUSE + "'"
										+ year + "-" + month + "-31 00:00:00"
										+ "'");
								break;
							}
							if (poDateValue.equals(2)) { // to get the current
															// due date year
															// results

								strQuery = strQuery.append(key
										+ MDMessageConstant.BETWEEN_CLAUSE
										+ "'" + year + "-" + month
										+ "-01 00:00:00" + "'"
										+ MDMessageConstant.AND_CLAUSE + "'"
										+ currentDate + "'");
								break;
							}
						}
					}
				}
				strQuery = strQuery.append(key + " LIKE " + "'" + value + "%"
						+ "'");
				i++;
			}
		} catch (HibernateException he) {
			logger.error(he.getMessage());
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		logger.info("InvoiceMasterDaoImpl.mapCriteraList() end");
		return strQuery;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getPurchaseOrderById(int invId) throws Exception {
		
		Query invoiceQuery = null;
		Session session = null;
		List<Object[]> invoiceDbList = null;
		
		logger.info("invoiceMasterDaoImpl.getPurchaseOrderById() end");
		try{
			session = sessionFactory.openSession(); 
			invoiceQuery = session.createQuery(MDSqlConstant.PURCHASE_ORDER_ID_QUERY+invId+"%'");
			
			invoiceDbList = invoiceQuery.list();
			
			if (null == invoiceDbList || invoiceDbList.size() < 1)
				throw new MDBusinessException(MDProperties.read
						(MDMessageConstant.NO_INVOICE_BUSINESS_EXCEPTION));

		}catch (HibernateException he) {
			logger.error(he.getMessage());
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
			logger.info("invoiceMasterDaoImpl.getPurchaseOrderById() end");
		}
		return invoiceDbList;
	}
}
