/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : PayMentDaoImpl.java
 * Comments : TODO 
 * Created  : 14-Oct-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |14-Oct-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.DBConstant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.helper.Helper;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.common.helper.MDSqlConstant;
import com.meshdynamics.common.helper.ModelVoHelper;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.model.Customer;
import com.meshdynamics.model.InvoiceItem;
import com.meshdynamics.model.InvoiceItemSubProduct;
import com.meshdynamics.model.InvoicePayment;
import com.meshdynamics.model.RawPayment;
import com.meshdynamics.vo.InvoiceItemVo;
import com.meshdynamics.vo.RevenueVo;

public class PayMentDaoImpl implements PayMentDao {

	private static final Logger logger = Logger.getLogger(PayMentDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.meshdynamics.dao.PayMentDao#addInvoicePayment(com.meshdynamics.model
	 * .InvoicePayment)
	 */
	@SuppressWarnings({ "boxing", "unchecked" })
	@Override
	public boolean addInvoicePayment(InvoicePayment invoicePayment)
			throws Exception {
		
		Session session = null;
		Transaction tx = null;
		boolean flag = false;
		int paymentAmount = 0;
		double newRawPayment = 0, rawPayment = 0, paymentNeed = 0, masterAmount = 0;
		RawPayment rawPay = null;
		Customer customer = null;
		Query query = null, rawquery = null, invoicePaymentQuery = null, 
				newRawpyment = null, newRawpayment = null;
		RawPayment rawpayment = null;
		List<Double> list = null;
		List<RawPayment> rawPaymentList = null;
		List<RawPayment> rawlist = null;
		List<Double> invoiceMasterAmount = null;
		List<Double> invoicePaymentLists = null;

		logger.info("payMentDaoImpl.addInvoicePayment() started");

		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();

			query = session
					.createSQLQuery(MDSqlConstant.INVOICE_AMOUNT_QUERY);
			query.setParameter(DBConstant.INVOICE_ID, invoicePayment.getInvoiceMaster()
					.getInvId());

			invoiceMasterAmount = query.list();
			if (null != invoiceMasterAmount && invoiceMasterAmount.size() > 0) {
				masterAmount = invoiceMasterAmount.get(0);

				// get RawPayment Amount
				rawquery = session
						.createQuery(MDSqlConstant.RAW_PAYMENT_AMOUNT_QUERY);
				rawquery.setParameter(DBConstant.RAW_PAYMENT_ID, 
						invoicePayment.getRawPayment().getRpId());

				list = rawquery.list();
				if (null != list && list.size() > 0)
					rawPayment = list.get(0);

				// new changes according to new requirements
				invoicePaymentQuery = session
						.createSQLQuery(MDSqlConstant.INVOICE_PAYMENT_AMOUNT_QUERY);
				invoicePaymentQuery.setParameter(DBConstant.INVOICE_ID, invoicePayment
						.getInvoiceMaster().getInvId());
				invoicePaymentLists = invoicePaymentQuery.list();

				for (double i : invoicePaymentLists) {
					paymentAmount += i;
				}

				paymentNeed = masterAmount - paymentAmount;

				if (rawPayment > paymentNeed) {

					newRawPayment = rawPayment - paymentNeed;

					invoicePayment.setAmount(paymentNeed);

					newRawpyment = session
							.createQuery(MDSqlConstant.RAW_PAYMENT_LIST);
					newRawpyment.setParameter(DBConstant.RAW_PAYMENT_ID, invoicePayment
							.getRawPayment().getRpId());

					rawPaymentList = newRawpyment.list();

					if (null != rawPaymentList && rawPaymentList.size() > 0)
						rawPay = rawPaymentList.get(0);

					customer = new Customer();
					customer.setCustId(rawPay.getCustomer().getCustId());
					rawPay.setAmount(paymentNeed);
					rawPay.setClosed(new Date());
					rawPay.setClosedBy(rawPay.getClosedBy());
					rawPay.setState(Constant.UI_RAW_CLOSED);
					rawPay.setInvId(invoicePayment.getInvoiceMaster().getInvId());
					rawPay.setCustomer(customer);
					rawpayment = new RawPayment();
					rawpayment.setAmount(newRawPayment);
					rawpayment.setClosedBy(rawPay.getClosedBy());
					rawpayment.setCreated(new Date());
					rawpayment.setCreatedBy(rawPay.getCreatedBy());
					rawpayment.setInvId(rawPay.getInvId());   
					rawpayment.setNotes(rawPay.getNotes());
					rawpayment.setState(Constant.UI_RAW_OPEN);
					rawpayment.setCustomer(customer);
					session.save(rawpayment);

				} else {
					newRawpayment = session
							.createQuery(MDSqlConstant.RAW_PAYMENT_LIST);
					newRawpayment.setParameter(DBConstant.RAW_PAYMENT_ID, invoicePayment
							.getRawPayment().getRpId());

					rawlist = newRawpayment.list();

					if (null != rawlist && rawlist.size() > 0)
						rawPay = rawlist.get(0);
					rawPay.setInvId(invoicePayment.getInvoiceMaster().getInvId());
					
					rawPay.setAmount(rawPayment);
					rawPay.setClosed(new Date());
					rawPay.setState(Constant.UI_RAW_CLOSED);
				}

				if (masterAmount == paymentAmount)
					throw new MDBusinessException(
							MDProperties.read(MDMessageConstant.COMPLETE_PAYMENT_EXCEPTION));

				session.save(invoicePayment);
				tx.commit();
				flag = true;
			}
		} catch (HibernateException he) {
			he.printStackTrace();
			if (null != tx){
				tx.rollback();
			}
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause().getMessage(),
					he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("payMentDaoImpl.addInvoicePayment() end");
		}
		return flag;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.dao.PayMentDao#addRawPayment()
	 */
	@Override
	public boolean addRawPayment(RawPayment rawPayment) throws Exception {

		Session session = null;
		Transaction tx = null;
		boolean flag = false;
		logger.info("PayMentDaoImpl.addRawPayment() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.save(rawPayment);
			tx.commit();
			flag = true;
		} catch (HibernateException he) {
			if (null != tx){
				tx.rollback();
			}
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.info("PayMentDaoImpl.addRawPayment() started");
		return flag;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.dao.PayMentDao#getRawPaymentById(int)
	 */
	@SuppressWarnings({ "boxing" })
	@Override
	public RawPayment getRawPaymentById(int rpId) throws Exception {

		Query query = null;
		Session session = null;
		List<?> rawList = null;
		logger.info("PaymentDaoImpl.getRawPaymentById() started");
		try {
			session = sessionFactory.openSession();
			query = session
					.createQuery(MDSqlConstant.RAW_PAYMENT_LIST);
			query.setParameter(DBConstant.RAW_PAYMENT_ID, rpId);

			rawList = query.list();
			if (null != rawList && rawList.size() > 0)
				return (RawPayment) rawList.get(0);

			throw new MDNoRecordsFoundException(MDProperties.
					read(MDMessageConstant.NO_RECORD_EXCEPTION));

		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("PaymentDaoImpl.getRawPaymentById() end");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.dao.PayMentDao#getInvoicePaymentById(int)
	 */
	@SuppressWarnings({ "unchecked", "boxing" })
	@Override
	public List<InvoicePayment> getInvoicePaymentByInvoiceId(int invId)
			throws Exception {

		Query query = null;
		Session session = null;
		List<InvoicePayment> invoicePayment = null;
		logger.info("PaymentDaoImpl.getInvoicePaymentByInvoiceID() started");
		try {
			session = sessionFactory.openSession();
			query = session
					.createQuery(MDSqlConstant.INVOICE_PAYMENT_LIST);
			query.setParameter(DBConstant.INVOICE_ID, invId);
			invoicePayment = query.list();

			if (null == invoicePayment || invoicePayment.size() < 1)
				throw new MDNoRecordsFoundException(MDProperties.
						read(MDMessageConstant.NO_RECORD_EXCEPTION));
			
		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.info("PaymentDaoImpl.getInvoicePaymentByInvoiceID() started");
		return invoicePayment;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.dao.PayMentDao#deleteRawPaymentById(int)
	 */

	@SuppressWarnings({ "boxing" })
	@Override
	public boolean deleteRawPaymentById(int rpId) throws Exception {

		Query query = null;
		Query innerQuery = null;
		Object obj = null;
		List<?> list = null;
		RawPayment rawPayment = null;
		Session session = null;
		Transaction tx = null;
		logger.info("PaymentDaoImpl.deleteRawPaymentById() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			query = session.createQuery(MDSqlConstant.RAW_PAYMENT_LIST);
			query.setParameter(DBConstant.RAW_PAYMENT_ID, rpId);

			obj = query.uniqueResult();

			if (null != obj) {
				innerQuery = session
						.createQuery(MDSqlConstant.INVOICE_PAYMENT_RAW_LIST);
				innerQuery.setParameter(DBConstant.RAW_PAYMENT_ID, rpId);

				list = innerQuery.list();
				if (null != list && list.size() > 0) {
					throw new MDBusinessException(MDProperties.read
									(MDMessageConstant.RAW_PAYMENT_DELETE_EXCEPTION));
				}
				rawPayment = (RawPayment) obj;
				session.delete(rawPayment);
				tx.commit();
				return true;
			}
			throw new MDNoRecordsFoundException(
					MDProperties.read(MDMessageConstant.NO_RECORD_EXCEPTION));

		} catch (HibernateException he) {
			if (null != tx){
				tx.rollback();
			}
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("PaymentDaoImpl.deleteRawPaymentById() end");
		}
	}

	/*
	 * PayMentDaoImpl.getRawPayment
	 * 
	 * @return List<RawPayment> comments : TODO
	 */
	@Override
	@SuppressWarnings({ "unchecked" })
	public List<RawPayment> getRawPayment() throws Exception {

		Query query = null;
		Session session = null;
		List<RawPayment> rawPayment = null;
		logger.info("PaymentDaoImpl.getRawpayment() started");
		try {
			session = sessionFactory.openSession();
			query = session.createQuery(MDSqlConstant.RAW_PAYMENTS_LIST);
			rawPayment = query.list();

			if (null == rawPayment || rawPayment.size() < 1)
				throw new MDNoRecordsFoundException(
						MDProperties.read(MDMessageConstant.NO_RECORD_EXCEPTION));
			
		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.info("PaymentDaoImpl.getRawpayment() end");
		return rawPayment;
	}

	/*
	 * PayMentDaoImpl.updateRawPayment
	 * 
	 * @param rawPaymentVo
	 * 
	 * @return boolean comments : TODO
	 */
	@Override
	public boolean updateRawPayment(RawPayment rawPayment) throws Exception {

		Session session = null;
		Transaction tx = null;
		boolean flag = false;
		logger.info("PaymentDaoImpl.updateRawPayment() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.update(rawPayment);
			tx.commit();
			flag = true;

		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			if (null != tx){
				tx.rollback();
			}
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.info("PaymentDaoImpl.updateRawPayment() end");
		return flag;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.dao.PayMentDao#getOpenRawPaymentList()
	 */

	@SuppressWarnings("unchecked")
	@Override
	public List<RawPayment> getOpenRawPaymentList(int custId) throws Exception {

		Query query = null;
		Session session = null;
		List<RawPayment> list = null;
		logger.info("PaymentDaoImpl.getRawpayment() started");
		try {
			session = sessionFactory.openSession();
			query = session.createQuery(MDSqlConstant.RAW_PAYMENT_STATE_QUERY);
			query.setParameter(DBConstant.STATE, DBConstant.OPEN);
			query.setParameter(DBConstant.CUSTOMER_ID, custId);
			list = query.list();

			if (null == list || list.size() < 1)
				throw new MDNoRecordsFoundException(
						MDProperties
								.read(MDMessageConstant.NO_RECORD_EXCEPTION));

			return list;
		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("PaymentDaoImpl.getRawPayment() end");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InvoicePayment> getInvoicePayment() throws Exception {

		Query query = null;
		Session session = null;
		List<InvoicePayment> invoicePayment = null;
		logger.info("PaymentDaoImpl.getInvoicePayment() started");
		try {
			session = sessionFactory.openSession();
			query = session.createQuery(MDSqlConstant.INVOICE_PAYMENTS_LIST);
			invoicePayment = query.list();

			if (null == invoicePayment || invoicePayment.size() < 1)
				throw new MDNoRecordsFoundException(
						MDProperties
								.read(MDMessageConstant.NO_RECORD_EXCEPTION));

		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.info("PaymentDaoImpl.getInvoicePayment() end");
		return invoicePayment;
	}

	public boolean updateInvoicePayment(InvoicePayment invoicePayment)
			throws Exception {

		Session session = null;
		Transaction tx = null;
		boolean flag = false;
		logger.info("PaymentDaoImpl.updateInvoicePayment() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.update(invoicePayment);
			tx.commit();
			flag = true;

		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			if (null != tx){
				tx.rollback();
			}
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.info("PaymentDaoImpl.updateInvoicePayment() end");
		return flag;
	}

	@Override
	@SuppressWarnings("boxing")
	public boolean deleteInvoicePaymentByInvoiceId(int invId) throws Exception {

		Query query = null;
		Session session = null;
		Transaction tx = null;
		int count = 0;
		logger.info("PaymentDaoImpl.deleteInvoicePaymentByInvoiceId() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			query = session
					.createQuery(MDSqlConstant.DELETE_INVOICE_PAYMENT_QUERY);
			query.setParameter(DBConstant.INVOICE_ID, invId);
			count = query.executeUpdate();
			tx.commit();
			if (count > 0)
				return true;
			throw new MDNoRecordsFoundException(
					MDProperties.read(MDMessageConstant.NO_RECORD_EXCEPTION));

		} catch (HibernateException he) {
			if (null != tx){
				tx.rollback();
			}
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("PaymentDaoImpl.deleteInvoicePaymentByInvoiceId() end");
		}
	}

	@Override
	@SuppressWarnings("boxing")
	public int getPaymentCountByInvoiceId(int invId) throws Exception {

		Query query = null;
		List<?> list = null;
		Session session = null;
		logger.info("PaymentDaoImpl.getPaymentCountByInvoiceId() started");
		try {
			session = sessionFactory.openSession();
			query = session.createQuery(MDSqlConstant.INVOICE_PAYMENT_LIST);
			query.setParameter(DBConstant.INVOICE_ID, invId);
			list = query.list();
			if (null == list || list.size() < 1) {
				throw new MDNoRecordsFoundException(
						MDProperties.read(MDMessageConstant.NO_RECORD_EXCEPTION));
			}
			return list.size();
		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("PaymentDaoImpl.getPaymentCountByInvoiceId() end");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RevenueVo> getRevenueList() throws Exception {

		double totalAmount = 0;
		String sqlquery = null;
		SQLQuery query = null;
		Session session = null;
		Query revenueQuery = null;
		Query invoiceItemQuery = null;
		String recognizedOn = null;
		RevenueVo revenueVo =  null;
		List<Object[]> list = null;
		InvoiceItemVo invoiceItemVo = null;
		Query invoiceItemSubProductQuery = null;
		List<RevenueVo> revenueList = null;
		List<Double> revenueAmountList = null;
		List<InvoiceItem> invoiceItemListFromDB = null;
		List<InvoiceItemSubProduct> invoiceItemSubProductList = null;
		
		logger.info("PaymentDaoImpl.getRevenueList() started");
		
		try {
			session = sessionFactory.openSession();
			revenueList = new ArrayList<RevenueVo>();
			sqlquery = MDSqlConstant.REVENUE_LIST_QUERY;
			query = session.createSQLQuery(sqlquery);
			list = query.list();
			
			if (null == list || list.size() < 1){
				throw new MDNoRecordsFoundException(MDProperties.read(MDMessageConstant.NO_RECORD_EXCEPTION));
			}else{
				for (Object[] revenue : list) {
					List <InvoiceItemVo> invoiceItems= new <InvoiceItemVo> ArrayList();
					revenueVo =  new RevenueVo();
					revenueVo.setPurchaseOrder(Integer.parseInt(revenue[0].toString()));
					revenueVo.setCustomerName(revenue[1].toString());
					revenueVo.setSalesRep(revenue[2].toString());
					revenueVo.setAmount(Double.parseDouble(revenue[3].toString()));
					revenueVo.setNotes(revenue[4].toString());
					recognizedOn = revenue[5].toString();
					recognizedOn = recognizedOn.substring(0,recognizedOn.length() - 2);
					revenueVo.setRecognizedOn(recognizedOn);
					revenueVo.setInvoiceItems(invoiceItems);
					invoiceItemQuery = session
							.createQuery(MDSqlConstant.INVOICE_ITEM_QUERY);
					invoiceItemQuery
							.setParameter(DBConstant.INVOICE_ID, revenueVo.getPurchaseOrder());
					invoiceItemListFromDB = invoiceItemQuery
							.list();
					for (InvoiceItem invoiceItem : invoiceItemListFromDB) {
					
						invoiceItemSubProductQuery = session
								.createQuery(MDSqlConstant.INVOICE_ITEM_SUUB_PRODUCT_QUERY);
						invoiceItemSubProductQuery.setParameter(DBConstant.INVOICE_ITEM_ID,
								invoiceItem.getInvoiceItemId());

						invoiceItemSubProductList = invoiceItemSubProductQuery
								.list();
						invoiceItem.setInvoiceItemSubProducts(new HashSet(
								invoiceItemSubProductList));
						
						invoiceItemVo = ModelVoHelper.invoiceItemModelToVo(invoiceItem);
						
						revenueVo.getInvoiceItems().add(invoiceItemVo);
					}
					
					revenueQuery = session
							.createSQLQuery(MDSqlConstant.REVENUE_TOTAL_AMOUNT_QUERY);
					revenueAmountList = revenueQuery.list();

					for (double revenueAmount : revenueAmountList) {
						totalAmount += revenueAmount;
					}
					revenueVo.setTotalAmount(totalAmount);
					revenueList.add(revenueVo);
				}
			}
			return revenueList;
		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session)
				session.close();
			logger.info("PaymentDaoImpl.getRevenueList() end");
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<RawPayment> getPaymentListUsingPagination(Map<?, ?> criteria,
			int start, int noOfRecords) throws Exception {

		int i = 1;
		Date date = null;
		String key = null;
		Object value = null;
		Session session = null;
		String currentDate = null;
		Query criteriaQuery = null;
		StringBuilder strQuery = null;
		List<RawPayment> rawPayment = null;

		logger.info("PaymentDaoImpl.getPaymentListUsingPagination() started");
		try {
			session = sessionFactory.openSession();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			date = new Date();
			currentDate = dateFormat.format(date);
			strQuery = new StringBuilder(MDSqlConstant.RAW_PAYMENTS_LIST);
			for (Entry<?, ?> entry : criteria.entrySet()) {
				if (i > 1) {
					strQuery = strQuery.append(MDMessageConstant.AND_CLAUSE);
				} else if (i == 1) {
					strQuery = strQuery.append(MDMessageConstant.WHERE_CLAUSE);
				}
				key = (String) entry.getKey();
				value = entry.getValue();
				if (key.equals(MDMessageConstant.KEY_CREATED)) {
					strQuery = strQuery.append(key
							+ MDMessageConstant.BETWEEN_CLAUSE + "'" + value
							+ "'" + MDMessageConstant.AND_CLAUSE + "'"
							+ currentDate + "'");
				} else {
					strQuery = strQuery.append(key + "=" + "'" + value + "'");
				}
				i++;
			}
			criteriaQuery = session.createQuery(strQuery.toString());
			criteriaQuery.setFirstResult(start - 1);
			criteriaQuery.setMaxResults(noOfRecords);
			rawPayment = criteriaQuery.list();
			if (null == rawPayment || rawPayment.size() < 1)
				throw new MDNoRecordsFoundException(
						MDProperties
								.read(MDMessageConstant.NO_RECORD_EXCEPTION));

		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("PaymentDaoImpl.getPaymentListUsingPagination() end");
		return rawPayment;

	}

	@SuppressWarnings("unchecked")
	@Override
	public int getCount(Map<?, ?> criteria) throws Exception {

		Date date = null;
		String key = null;
		Object value = null;
		int i = 1, count = 0;
		Session session = null;
		String currentDate = null;
		Query criteriaQuery = null;
		StringBuilder strQuery = null;
		List<RawPayment> rawPaymentList = null;
		logger.info("paymentDaoImpl.getCount() started");
		try {
			session = sessionFactory.openSession();
			strQuery = new StringBuilder(MDSqlConstant.RAW_PAYMENTS_LIST);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			date = new Date();
			currentDate = dateFormat.format(date);
			for (Entry<?, ?> entry : criteria.entrySet()) {
				if (i > 1) {
					strQuery = strQuery.append(MDMessageConstant.AND_CLAUSE);
				} else if (i == 1) {
					strQuery = strQuery.append(MDMessageConstant.WHERE_CLAUSE);
				}
				key = (String) entry.getKey();
				value = entry.getValue();
				date = new Date();
				if (key.equals(MDMessageConstant.KEY_CREATED)) {
					strQuery = strQuery.append(key
							+ MDMessageConstant.BETWEEN_CLAUSE + "'" + value
							+ "'" + MDMessageConstant.AND_CLAUSE + "'"
							+ currentDate + "'");
				} else {
					strQuery = strQuery.append(key + " LIKE " + "'" + value + "%" + "'");
				}
				i++;
			}
			criteriaQuery = session.createQuery(strQuery.toString());
			rawPaymentList = criteriaQuery.list();
			count = rawPaymentList.size();

		} catch (HibernateException he) {
			logger.error(he.getMessage());
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("paymentDaoImpl.getCount() end");
		return count;
	}

	@Override
	@SuppressWarnings({ "unchecked", "unused" })
	public List<RevenueVo> getRevenueListUsingPagination(int revenueDateFlag, Map<?, ?> criteria,
			int start, int noOfRecords) throws Exception {

		int i = 1;
		int quarter = 0;
		Date date = null;
		String key = null;
		Object value = null;
		SQLQuery query = null;
		Session session = null;
		String sqlquery = null;
		int year = 0, month = 0;
		String currentDate = null;
		Query criteriaQuery = null;
		String recognizedOn = null;
		RevenueVo revenueVo = null;
		List<Object[]> list = null;
		Query quarterQuery = null;
		StringBuilder strQuery = null;
		Query invoiceItemQuery = null;
		List<RevenueVo> revenueList = null;
		InvoiceItemVo invoiceItemVo = null;
		List<BigInteger> quarterList = null;
		Query invoiceItemSubProductQuery = null;
		List<InvoiceItem> invoiceItemListFromDB = null;
		List<InvoiceItemSubProduct> invoiceItemSubProductList = null;

		logger.info("PaymentDaoImpl.getRevenueListUsingPagination() started");

		try {
			session = sessionFactory.openSession();
			revenueList = new ArrayList<RevenueVo>();

			strQuery = new StringBuilder(MDSqlConstant.REVENUE_LIST_QUERY);

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			date = new Date();
			currentDate = dateFormat.format(date);
			String currentYMD[] = currentDate.split("-");
			int currentYear = Integer.parseInt(currentYMD[0]);
			int currentMonth = Integer.parseInt(currentYMD[1]);

			for (Entry<?, ?> entry : criteria.entrySet()) {
				if (i > 1) {
					strQuery = strQuery.append(MDMessageConstant.AND_CLAUSE);
				}
				key = (String) entry.getKey();
				value = entry.getValue();
				if (revenueDateFlag != 0) {
					if (key == "payDate") {
						if (i == 1) {
							strQuery = strQuery
									.append(MDMessageConstant.AND_CLAUSE);
						}
						String yearDateValue = value.toString();
						String ymd[] = yearDateValue.split("-");

						year = Integer.parseInt(ymd[0]);
						month = Integer.parseInt(ymd[1]);

						if (revenueDateFlag == 1) {
							strQuery = strQuery.append(key
									+ MDMessageConstant.BETWEEN_CLAUSE + "'"
									+ value + "'"
									+ MDMessageConstant.AND_CLAUSE + "'"
									+ currentDate + "'");
						} else if (revenueDateFlag == 2) {
							quarterQuery = session
									.createSQLQuery("SELECT QUARTER('"
											+ yearDateValue + "')");
							quarterList = quarterQuery.list();

							if (quarterList != null && quarterList.size() > 0) {
								quarter = Integer.valueOf(quarterList.get(0)
										.intValue());
							}
							if (quarter == 1) {
								strQuery = strQuery.append(key
										+ MDMessageConstant.BETWEEN_CLAUSE
										+ "'" + year + "-01-01" + "'"
										+ MDMessageConstant.AND_CLAUSE + "'"
										+ year + "-03-31" + "'");
							} else if (quarter == 2) {
								strQuery = strQuery.append(key
										+ MDMessageConstant.BETWEEN_CLAUSE
										+ "'" + year + "-04-01 00:00:00" + "'"
										+ MDMessageConstant.AND_CLAUSE + "'"
										+ year + "-06-30 00:00:00" + "'");
							} else if (quarter == 3) {
								strQuery = strQuery.append(key
										+ MDMessageConstant.BETWEEN_CLAUSE
										+ "'" + year + "-07-01 00:00:00" + "'"
										+ MDMessageConstant.AND_CLAUSE + "'"
										+ year + "-09-30 00:00:00" + "'");
							} else if (quarter == 4) {
								strQuery = strQuery.append(key
										+ MDMessageConstant.BETWEEN_CLAUSE
										+ "'" + year + "-10-01 00:00:00" + "'"
										+ MDMessageConstant.AND_CLAUSE + "'"
										+ year + "-12-31 00:00:00" + "'");
							}
						} else if (revenueDateFlag == 3) {
							strQuery = strQuery.append(key
									+ MDMessageConstant.BETWEEN_CLAUSE + "'"
									+ value + "'"
									+ MDMessageConstant.AND_CLAUSE + "'"
									+ currentDate + "'");
						} else if (revenueDateFlag == 4) {
							strQuery = strQuery.append(key
									+ MDMessageConstant.BETWEEN_CLAUSE + "'"
									+ year + "-" + month + "-" + "01 00:00:00"
									+ "'" + MDMessageConstant.AND_CLAUSE + "'"
									+ year + "-" + month + "-" + "31 00:00:00"
									+ "'");
						}
					}
				} else {
					if(i == 1){
						strQuery = strQuery
								.append(MDMessageConstant.AND_CLAUSE);
					}
					strQuery = strQuery.append(key + " LIKE " + "'" + value
							+ "%" + "'");
				}
				i++;
			}
			criteriaQuery = session.createSQLQuery(strQuery.toString());
			criteriaQuery.setFirstResult(start - 1);
			criteriaQuery.setMaxResults(noOfRecords);
			list = criteriaQuery.list();

			if (null == list || list.size() < 1) {
				throw new MDNoRecordsFoundException(
						MDProperties
								.read(MDMessageConstant.NO_RECORD_EXCEPTION));
			} 
			
			else {
				for (Object[] revenue : list) {
					List<InvoiceItemVo> invoiceItems = new<InvoiceItemVo> ArrayList();
					revenueVo = new RevenueVo();
					revenueVo.setPurchaseOrder(Integer.parseInt(revenue[0]
							.toString()));
					revenueVo.setCustomerName(revenue[1].toString());
					revenueVo.setSalesRep(revenue[2].toString());
					revenueVo.setAmount(Double.parseDouble(revenue[3]
							.toString()));
					revenueVo.setNotes(revenue[4].toString());
					recognizedOn = revenue[5].toString();
					recognizedOn = recognizedOn.substring(0,
							recognizedOn.length() - 2);
					revenueVo.setRecognizedOn(recognizedOn);
					revenueVo.setInvoiceItems(invoiceItems);
					invoiceItemQuery = session
							.createQuery(MDSqlConstant.INVOICE_ITEM_QUERY);
					invoiceItemQuery.setParameter(DBConstant.INVOICE_ID,
							revenueVo.getPurchaseOrder());
					invoiceItemListFromDB = invoiceItemQuery.list();
					for (InvoiceItem invoiceItem : invoiceItemListFromDB) {

						invoiceItemSubProductQuery = session
								.createQuery(MDSqlConstant.INVOICE_ITEM_SUUB_PRODUCT_QUERY);
						invoiceItemSubProductQuery.setParameter(
								DBConstant.INVOICE_ITEM_ID,
								invoiceItem.getInvoiceItemId());

						invoiceItemSubProductList = invoiceItemSubProductQuery
								.list();
						invoiceItem.setInvoiceItemSubProducts(new HashSet(
								invoiceItemSubProductList));

						invoiceItemVo = ModelVoHelper
								.invoiceItemModelToVo(invoiceItem);

						revenueVo.getInvoiceItems().add(invoiceItemVo);
					}
					revenueList.add(revenueVo);
				}
			}
			return revenueList;
		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session)
				session.close();
			logger.info("PaymentDaoImpl.getRevenueListUsingPagination() end");
		}
	}
}
