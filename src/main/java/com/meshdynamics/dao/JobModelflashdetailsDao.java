package com.meshdynamics.dao;

import java.util.List;

import com.meshdynamics.model.JobModelFlashDetails;

public interface JobModelflashdetailsDao {

	public boolean addJobModelFlashDetails(JobModelFlashDetails flashDetails) throws Exception;

	public boolean updateJobModelFlashDetails(JobModelFlashDetails flashDetails) throws Exception;

	public JobModelFlashDetails getjobModelFlashDetailsById(int jobmodelflashdetailsId) throws Exception;

	public List<JobModelFlashDetails> getjobModelFlashDetailsByJobIdAndModelId(int jobId, int jobItemId) throws Exception;
	
	public List<JobModelFlashDetails> getjobModelFlashDetailsList() throws Exception;
	
	public boolean regenerateBuild(JobModelFlashDetails jobModelFlashDetails)throws Exception;
	
	public boolean updateJobModelFlashDetailsByImageName(String imageName ) throws Exception;
	
	public boolean verifyMacAddress(String boardMacAddress) throws Exception;

	public String fetchImageDownloadPath() throws Exception;
}
