package com.meshdynamics.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.common.DBConstant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.helper.Helper;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.common.helper.MDSqlConstant;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.model.InvoiceTracking;

public class TrackingDaoImpl implements TrackingDao {
	private static final Logger logger = Logger
			.getLogger(TrackingDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public boolean addTrackingList(InvoiceTracking invoiceTracking)
			throws Exception {

		Query query = null;
		Session session = null;
		Transaction tx = null;
		boolean flag = false;
		List<?> trackingList = null;
		logger.info("TrackingDaoImpl.addTrackingList() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			query = session.createQuery(MDSqlConstant.TRACKING_QUERY);
			query.setParameter(DBConstant.TRACK_NUMBER,
					invoiceTracking.getTrackingNumber());
			trackingList = query.list();
			if (null != trackingList && trackingList.size() > 0) 
				throw new MDBusinessException(
						MDProperties
								.read(MDMessageConstant.TRACKING_ADD_FAILURE));

			session.save(invoiceTracking);
			tx.commit();
			flag = true;
		} catch (HibernateException he) {
			he.printStackTrace();
			if (null != tx){
				tx.rollback();
			}
			throw new MDDBException(Helper.getExceptionCode(he), he.getMessage(),
					he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.info("TrackingDaoImpl.addTrackingList() end");
		return flag;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<InvoiceTracking> getTrackingLists() throws Exception {

		Session session = null;
		List<InvoiceTracking> trackingList = null;
		logger.info("TrackingDaoImpl.getTrackingLists() started");
		try {
			session = sessionFactory.openSession();
			trackingList = session.createQuery(
					MDSqlConstant.INVOICE_TRACKING_LIST).list();

			if (null == trackingList || trackingList.size() < 1)
				throw new MDBusinessException(
						MDProperties.read(MDMessageConstant.NO_TRACKING_LIST));
			return trackingList;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(null != session){
			session.close();
			}
			logger.info("TrackingDaoImpl.getTrackingLists() ends");
		}
	}

	@SuppressWarnings("boxing")
	public InvoiceTracking getTrackListByTrackId(int trackId) throws Exception {

		Query query = null;
		List<?> list = null;
		Session session = null;
		logger.info("TrackingDaoImpl.getTrackListByTrackId() started");
		try {
			session = sessionFactory.openSession();
			query = session.createQuery(MDSqlConstant.TRACKING_ID_QUERY);
			query.setParameter(DBConstant.TRACK_ID, trackId);

			list = query.list();

			if (null != list && list.size() > 0)
				return (InvoiceTracking) list.get(0);
			throw new MDNoRecordsFoundException(
					MDProperties.read(MDMessageConstant.NO_RECORD_EXCEPTION));

		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause().getMessage(),
					he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("TrackingDaoImpl.getTrackListByTrackId() end");
		}
	}

	@SuppressWarnings("boxing")
	@Override
	public int getTrackCountByInvoiceId(int invId) throws Exception {

		Query query = null;
		List<?> list = null;
		Session session = null;
		logger.info("TrackingDaoImpl.getTrackCountByInvoiceId() started");
		try {
			session = sessionFactory.openSession();
			query = session
					.createQuery(MDSqlConstant.TRACKING_INVOICE_ID_QUERY);
			query.setParameter(DBConstant.INVOICE_ID, invId);
			list = query.list();
			if (list.size() < 1)
				throw new MDNoRecordsFoundException(
						MDProperties
								.read(MDMessageConstant.NO_RECORD_EXCEPTION));

			return list.size();
		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause().getMessage(),
					he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("TrackingDaoImpl.getTrackCountByInvoiceId() end");
		}
	}

	@Override
	public boolean updateInvoiceTrackList(InvoiceTracking invoiceTracking)
			throws Exception {

		boolean flag = false;
		Session session = null;
		Transaction tx = null;
		logger.info("InvoiceTrackingListDaoImpl.updateInvoiceTrackList() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.update(invoiceTracking);
			tx.commit();
			flag = true;
		} catch (HibernateException he) {
				tx.rollback();
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getMessage(),
					he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.info("InvoiceTrackingListDaoImpl.updateInvoiceTrackList() end");
		return flag;
	}

	@Override
	public boolean deleteInvoiceTrackListById(int trackId) throws Exception {

		Session session = null;
		Transaction tx = null;
		InvoiceTracking invoiceTrackListById = null;
		logger.info("InvoiceTrackingListDaoImpl.deleteInvoiceTrackListById() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			invoiceTrackListById = getTrackListByTrackId(trackId);
			if (null != invoiceTrackListById) {
				session.delete(invoiceTrackListById);
				tx.commit();
				return true;
			}
			throw new MDNoRecordsFoundException(
					MDProperties.read(MDMessageConstant.NO_INVOICE_TRACKING));

		} catch (HibernateException he) {
			if (null != tx) {
				tx.rollback();
			}
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getMessage(),
					he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
			logger.info("InvoiceTrackingListDaoImpl.deleteInvoiceTrackListById() end");
		}
	}

	@Override
	@SuppressWarnings({ "unchecked", "boxing" })
	public List<InvoiceTracking> getInvoiceTrackListByInvId(int invId)
			throws Exception {

		Query query = null;
		Session session = null;
		List<InvoiceTracking> invoiceList = null;
		logger.info("InvoiceTrackingListDaoImpl.getInvoiceTrackListByInvId() started");
		try {
			session = sessionFactory.openSession();
			query = session
					.createQuery(MDSqlConstant.TRACKING_INVOICE_ID_QUERY);
			query.setParameter(DBConstant.INVOICE_ID, invId);
			invoiceList = query.list();
			if (null == invoiceList || invoiceList.size() < 1)
				throw new MDNoRecordsFoundException(
						MDProperties
								.read(MDMessageConstant.NO_TRACKING_LIST_AVAILABLE));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("InvoiceTrackingListDaoImpl.getInvoiceTrackListByInvId() end");
		return invoiceList;
	}
}
