package com.meshdynamics.dao;

import java.util.List;

import com.meshdynamics.model.InvoiceTracking;

public interface TrackingDao {

	boolean addTrackingList(InvoiceTracking invoiceTracking) throws Exception;

	List<InvoiceTracking> getTrackingLists() throws Exception;
	
	int getTrackCountByInvoiceId(int invId) throws Exception;
	
	boolean updateInvoiceTrackList(InvoiceTracking invoiceTracking) throws Exception;
	
	boolean deleteInvoiceTrackListById(int trackId) throws Exception;
	
	List<InvoiceTracking> getInvoiceTrackListByInvId(int invId) throws Exception;

}
