package com.meshdynamics.dao;

import com.meshdynamics.model.UserRole;

/**
 * @author Sanjay
 *
 */
public interface LoginDao {

	public UserRole findUserByUserNameAndPassword(String userName, String password) throws Exception;

	boolean changePassword(String apikey, String currentPassword, String newPassword) throws Exception;
	
	public boolean forgotPassword(String userName) throws Exception;
}
