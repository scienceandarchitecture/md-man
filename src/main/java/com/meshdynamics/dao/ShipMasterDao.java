/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ShipMasterDao.java
 * Comments : TODO 
 * Created  : Oct 5, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |    Comment                       |     Author    |
 * ---------------------------------------------------------------------------
 * |  0  |Oct 5, 2016   |    Created                    	  |      Pinki	  |  
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.List;
import java.util.Map;

import com.meshdynamics.model.ShipMaster;

public interface ShipMasterDao {

	boolean addShipItem(ShipMaster shipMaster) throws Exception;

	List<ShipMaster> getShipList() throws Exception;
	
	public ShipMaster getShipItemById(int shipId) throws Exception;
	
	boolean deleteShipByID(int shipId) throws Exception;
	
	boolean updateShipItemDetails(ShipMaster shipMaster) throws Exception;
	
	List<ShipMaster> getShipListByInvoiceId(int invid) throws Exception;
	
	public List<ShipMaster> getShipPaginationList(Map<?, ?> newMapCriteria, int start,
			int noOfRecords) throws Exception;
	
	public int getCount(Map<?, ?> newMapCriteria) throws Exception;

}
