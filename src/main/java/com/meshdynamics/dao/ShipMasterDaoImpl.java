/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ShipMasterDaoImpl.java
 * Comments : TODO 
 * Created  : Oct 5, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |    Comment                       |     Author    |
 * ---------------------------------------------------------------------------
 * |  0  |Oct 5, 2016   |    Created                    	  |      Pinki	  |  
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.meshdynamics.common.DBConstant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.helper.Helper;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.common.helper.MDSqlConstant;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.model.Customer;
import com.meshdynamics.model.ShipItem;
import com.meshdynamics.model.ShipItemSubProduct;
import com.meshdynamics.model.ShipMaster;
import com.meshdynamics.model.UserRole;

public class ShipMasterDaoImpl implements ShipMasterDao {

	private static final Logger logger = Logger
			.getLogger(ShipMasterDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings({ "unused", "unchecked" })
	@Transactional
	@Override
	public boolean addShipItem(ShipMaster shipMaster) throws Exception {
		
		int custId = 0;
		Session session = null;
		Transaction tx = null;
		List<?> jobInShipList = null;
		List<?> userList = null;
		String shipAddress = null;
		UserRole userRole = null;
		List<?> addressList = null;
		List<Integer> custList = null;
		Query query = null,custQuery = null,addressQuery = null,fetchJobQuery = null;
		logger.info("ShipMasterDaoImpl.addShipItem() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();

			fetchJobQuery = session.createQuery(MDSqlConstant.SHIP_JOB_QUERY);
			jobInShipList = fetchJobQuery.setParameter(DBConstant.JOB_ID,
					shipMaster.getJobMaster().getJobId()).list();
			
			if (null != jobInShipList && jobInShipList.size() > 0)
				throw new MDBusinessException(MDProperties.read
						(MDMessageConstant.SHIPPING_ALREADY_DONE_EXCEPTION));

			query = session.createQuery(MDSqlConstant.USER_ROLE_USERNAME_QUERY);
			query.setParameter(DBConstant.USER_NAME, shipMaster.getCreatedBy());
			userList = query.list();

			if (null != userList && userList.size() > 0) {

				userRole = (UserRole) userList.get(0);
				custQuery = session
						.createSQLQuery(MDSqlConstant.INVOICE_CUST_ID_QUERY);
				custQuery.setParameter(DBConstant.INVOICE_ID, shipMaster
						.getInvoiceMaster().getInvId());
				custList = custQuery.list();
				if (null != custList && custList.size() > 0)
					custId = custList.get(0);

				addressQuery = session
						.createQuery(MDSqlConstant.SHIP_CUSTOMER_QUERY);
				addressQuery.setParameter(DBConstant.CUSTOMER_ID, custId);
				addressList = addressQuery.list();

				if (null != addressList && addressList.size() > 0)
					shipAddress = (String) addressList.get(0);

				shipMaster.setAddress(shipAddress);
				shipMaster.setCustId(custId);

				session.save(shipMaster);
				for (ShipItem shipItem : shipMaster.getShipItems()) {
					shipItem.setShipMaster(shipMaster);
					session.merge(shipItem);

					if (null != shipItem) {
						for (ShipItemSubProduct shipItemSubProduct : shipItem
								.getShipItemSubProducts()) {
							shipItemSubProduct.setShipItem(shipItem);
							session.merge(shipItemSubProduct);
						}
					}
				}
				tx.commit();
				return true;
			}
			throw new MDBusinessException(
					MDProperties.read(MDMessageConstant.UNKNOWN_USER));
		} catch (HibernateException he) {
			he.printStackTrace();
			if (null != tx){
				tx.rollback();
			}
			throw new MDDBException(Helper.getExceptionCode(he),
					he.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("ShipMasterDaoImpl.addShipItem() ends");
		}
	}

	/*
	 * ShipMasterDaoImpl.getShipItemList
	 * 
	 * @return List<ShipMaster> comments : TODO
	 */

	@SuppressWarnings("unchecked")
	@Override
	public List<ShipMaster> getShipList() throws Exception {

		Session session = null;
		List<ShipMaster> shipList = null;
		List<Customer> customerList = null;
		List<ShipMaster> sendShipList=new ArrayList<ShipMaster>();
		Query customerQuery = null,query = null;
		
		logger.info("ShipMasterDaoImpl.getShipList() started");
		
		try {
			session = sessionFactory.openSession();
			query = session.createQuery(MDSqlConstant.SHIP_LIST_QUERY);
			shipList = query.list();
			if (null == shipList || shipList.size() < 1)
				throw new MDNoRecordsFoundException(
						MDProperties.read(MDMessageConstant.NO_SHIPPING));

			for (ShipMaster shipMaster : shipList) {
				customerQuery = session
						.createQuery(MDSqlConstant.CUSTOMER_QUERY);

				customerQuery.setParameter(DBConstant.CUSTOMER_ID,
						shipMaster.getCustId());
				customerList = customerQuery.list();
				if (null != customerList && 0 < customerList.size()) {
					shipMaster.setAddress(customerList.get(0).getShipAddress());
				}
				sendShipList.add(shipMaster);
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.info("ShipMasterDaoImpl.getShipList() ends");
		return sendShipList;
	}

	/*
	 * ShipMasterDaoImpl.getShipItemById
	 * 
	 * @param shipId
	 * 
	 * @return ShipMaster comments : TODO
	 */
	@SuppressWarnings({ "boxing", "unchecked" })
	@Override
	public ShipMaster getShipItemById(int shipId) throws Exception {

		Query query = null;
		Query customerQuery =  null;
		List<?> shipList = null;
		List<Customer> customerList = null;
		ShipMaster shipMaster = null;
		Session session = null;
		logger.info("ShipMasterDaoImpl.getShipItemById() started");
		try {
			session = sessionFactory.openSession();
			query = session.createQuery(MDSqlConstant.SHIP_QUERY);
			query.setParameter(DBConstant.SHIP_ID, shipId);

			shipList = query.list();
			if (null != shipList && shipList.size() > 0) {
				shipMaster = (ShipMaster) shipList.get(0);
			} else {
				throw new MDNoRecordsFoundException(
						MDProperties.read(MDMessageConstant.NO_SHIP_ID));
			}

			customerQuery = session.createQuery(MDSqlConstant.CUSTOMER_QUERY);
			customerQuery.setParameter(DBConstant.CUSTOMER_ID,
					shipMaster.getCustId());
			customerList = customerQuery.list();
			if (null != customerList && 0 < customerList.size()) {
				shipMaster.setAddress(customerList.get(0).getAddress());
			}
			return shipMaster;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(null != session){
			session.close();
			}
			logger.info("ShipMasterDaoImpl.getShipItemById() ended");
		}
	}

	/*
	 * ShipMasterDaoImpl.deleteShipByID
	 * 
	 * @param shipId
	 * 
	 * @return boolean comments : TODO
	 */
	@SuppressWarnings("boxing")
	@Override
	public boolean deleteShipByID(int shipId) throws Exception {

		Object obj = null;
		Query query = null;
		Query trackQuery = null;
		Session session = null;
		Transaction tx = null;
		List<?> trackingList = null;
		logger.info("ShipMasterDaoImpl.deleteShipById() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();

			query = session.createQuery(MDSqlConstant.SHIP_QUERY);
			query.setParameter(DBConstant.SHIP_ID, shipId);

			trackQuery = session
					.createQuery(MDSqlConstant.INVOICE_TRACKING_SHIP_QUERY);
			trackingList = trackQuery.setParameter(DBConstant.SHIP_ID, shipId)
					.list();

			if (null != trackingList && trackingList.size() > 0)
				throw new MDBusinessException(MDProperties.read
						(MDMessageConstant.DELETE_SHIP_EXCEPTION));

			obj = query.uniqueResult();
			if (null != obj) {
				ShipMaster shipMaster = (ShipMaster) obj;
				for (ShipItem shipItem : shipMaster.getShipItems()) {
					session.delete(shipItem);
				}
				session.delete(shipMaster);
				tx.commit();
				return true;
			}
			throw new MDNoRecordsFoundException(
					MDProperties.read(MDMessageConstant.NO_RECORD_SHIP));
		} catch (HibernateException he) {
			if (null != tx){
				tx.rollback();
			}
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(null != session){
			session.close();
			}
			logger.info("ShipMasterDaoImpl.deleteShipById() end");
		}
	}

	/*
	 * ShipMasterDaoImpl.updateShipItemDetails
	 * 
	 * @param shipMaster
	 * 
	 * @return boolean comments : TODO
	 */
	@SuppressWarnings("boxing")
	@Override
	public boolean updateShipItemDetails(ShipMaster shipMaster)
			throws Exception {

		Transaction tx = null;
		Session session = null;
		logger.info("ShipMasterDaoImpl.updateShipItemDetails() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.update(shipMaster);
			for (ShipItem shipItem : shipMaster.getShipItems()) {
				shipItem.setShipMaster(shipMaster);
			}
			tx.commit();
			return true;

		} catch (Exception e) {
			if (null != tx){
				tx.rollback();
			}
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("ShipMasterDaoImpl.updateShipItemDetails() end");
		}
	}

	/*
	 * ShipMasterDaoImpl.getShipListByInvoiceId
	 * 
	 * @return List<ShipMaster>
	 */

	@SuppressWarnings("unchecked")
	@Override
	public List<ShipMaster> getShipListByInvoiceId(int invid) throws Exception {

		Query query = null;
		Session session = null;
		List<ShipMaster> shipList = null;
		logger.info("ShipMasterDaoImpl.getShipListByInvoiceId() started");
		try {
			session = sessionFactory.openSession();
			query = session.createQuery(MDSqlConstant.SHIP_INVOICE_ID_QUERY);
			query.setParameter(DBConstant.INVOICE_ID, invid);
			shipList = query.list();
			if (null == shipList || shipList.size() < 1) {
				throw new MDNoRecordsFoundException(
						MDProperties.read(MDMessageConstant.NO_SHIPPING));
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.info("ShipMasterDaoImpl.getShipListByInvoiceId() end");
		return shipList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ShipMaster> getShipPaginationList(Map<?, ?> newMapCriteria, int start,
			int noOfRecords) throws Exception{

		StringBuilder strQuery = null;
		Session session = null;
		List<ShipMaster> shipList = null;
		List<Customer> customerList = null;
		List<ShipMaster> sendShipList=new ArrayList<ShipMaster>();
		Query customerQuery = null,query = null;
		
		logger.info("ShipMasterDaoImpl.getShipPaginationList() started");
		
		try {
			session = sessionFactory.openSession();
			strQuery = new StringBuilder(MDSqlConstant.SHIP_GENERIC_QUERY);

			strQuery = mapCriteraList(strQuery,newMapCriteria);
			
			query = session.createQuery(strQuery.toString());
			query.setFirstResult(start-1);
			query.setMaxResults(noOfRecords);
			shipList = query.list();
			if (null == shipList || shipList.size() < 1)
				throw new MDNoRecordsFoundException(
						MDProperties.read(MDMessageConstant.CRITERIA_BUSINESS_EXCEPTION));

			for (ShipMaster shipMaster : shipList) {
				customerQuery = session
						.createQuery(MDSqlConstant.CUSTOMER_QUERY);

				customerQuery.setParameter(DBConstant.CUSTOMER_ID,
						shipMaster.getCustId());
				customerList = customerQuery.list();
				if (null != customerList && 0 < customerList.size()) {
					shipMaster.setAddress(customerList.get(0).getShipAddress());
				}
				sendShipList.add(shipMaster);
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.info("ShipMasterDaoImpl.getShipPaginationList() ends");
		return sendShipList;
	}

	private StringBuilder mapCriteraList(StringBuilder strQuery,
			Map<?, ?> newMapCriteria) throws Exception {

		int i = 1;
		String key = null;
		Object value = null;
		logger.info("ShipMasterDaoImpl.mapCriteraList() started");
		try {
			for (Entry<?, ?> entry : newMapCriteria.entrySet()) {
				if (i > 1) {
					strQuery = strQuery.append(MDMessageConstant.AND_CLAUSE);
				} else if (i == 1) {
					strQuery = strQuery.append(MDMessageConstant.WHERE_CLAUSE);
				}
				key = (String) entry.getKey();
				value = entry.getValue();
				strQuery = strQuery.append(key + "=" + "'" + value + "'");
				i++;
			}
		} catch (HibernateException he) {
			logger.error(he.getMessage());
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		logger.info("ShipMasterDaoImpl.mapCriteraList() end");
		return strQuery;
	}

	@Override
	public int getCount(Map<?, ?> newMapCriteria) throws Exception {

		int count = 0;
		Query criteriaQuery = null;
		List<?> shipList = null;
		Session session = null;
		StringBuilder strQuery = null;
		logger.info("ShipMasterDaoImpl.getCount() started");
		try {
			session = sessionFactory.openSession();
			strQuery = new StringBuilder(MDSqlConstant.SHIP_GENERIC_QUERY);

			if(newMapCriteria.size() == 0) { 
					criteriaQuery = session.createQuery("SELECT COUNT(*) FROM ShipMaster");
					shipList = criteriaQuery.list();
					count = Integer.parseInt(shipList.get(0).toString());
				} else {
					strQuery = mapCriteraList(strQuery,newMapCriteria);
					criteriaQuery = session.createQuery(strQuery.toString());
					shipList = criteriaQuery.list();
					count = shipList.size();
				}

		} catch (HibernateException he) {
			logger.error(he.getMessage());
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("ShipMasterDaoImpl.getCount() end");
		return count;
	}
}
