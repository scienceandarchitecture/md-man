package com.meshdynamics.dao;

import java.util.List;
import java.util.Map;

import com.meshdynamics.model.RawInventory;

public interface InventoryDao {

	boolean addRawInventory(RawInventory rawInventory) throws Exception;

	boolean updateRawInventory(RawInventory rawInventory) throws Exception;

	List<RawInventory> getRawInventoryList() throws Exception;

	boolean deleteRawInventoryById(String rsku) throws Exception;
	
	List<String> getRawInventoryBySKU(String rsku) throws Exception;

	List<RawInventory> getInventoryPaginationList(Map<?, ?> newMap, int start,
			int noOfRecords) throws Exception;

}
