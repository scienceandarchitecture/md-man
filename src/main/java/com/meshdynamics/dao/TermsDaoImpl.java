/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : TermsDaoImpl.java
 * Comments : TODO 
 * Created  : 20-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |20-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.common.DBConstant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.helper.Helper;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.common.helper.MDSqlConstant;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.model.Terms;

public class TermsDaoImpl implements TermsDao {

	@Autowired
	SessionFactory sessionFactory;

	private static final Logger logger = Logger.getLogger(TermsDaoImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.dao.TermsDao#addTerms(com.meshdynamics.model.Terms)
	 */
	@Override
	public boolean addTerms(Terms terms) throws Exception {
		
		boolean flg = false;
		Session session = null;
		Transaction tx = null;
		logger.info("TermsDaoImpl.addTerms() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.save(terms);
			tx.commit();
			flg = true;
		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he),
					he.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(null != session){
			session.close();
			}
		}
		logger.info("TermsDaoImpl.addTerms() end");
		return flg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.meshdynamics.dao.TermsDao#updateTerms(com.meshdynamics.model.Terms)
	 */
	@Override
	public boolean updateTerms(Terms terms) throws Exception {

		boolean flg = false;
		Session session = null;
		Transaction tx = null;
		logger.info("TermsDaoImpl.updateTerms() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.update(terms);
			tx.commit();
			flg = true;
		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he),
					he.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(null != session){
			session.close();
			}
		}
		logger.info("TermsDaoImpl.updateTerms() end");
		return flg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.dao.TermsDao#getTermsList()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Terms> getTermsList() throws Exception {

		List<Terms> termsList = null;
		Session session = null;
		logger.info("TermsDaoImpl.getTermsList() started");
		try {
			session = sessionFactory.openSession();
			termsList = session.createQuery(MDSqlConstant.TERMS_LIST).list();
			if (null == termsList || termsList.size() < 1)
				throw new MDBusinessException(
						MDProperties.read(MDMessageConstant.NO_TERMS_EXIST));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if( null != session ){
			session.close();
			}
		}
		logger.info("TermsDaoImpl.getTermsList() end");
		return termsList;
	}

	/*
	 * TermsDaoImpl.deleteTermsById
	 * 
	 * @param termId
	 * 
	 * @return boolean comments : TODO
	 */
	@Override
	@SuppressWarnings({ "boxing" })
	public boolean deleteTermsById(int termId) throws Exception {

		Query query = null;
		Session session = null;
		Transaction tx = null;
		List<?> list = null;
		logger.info("TermsDaoImpl.deleteTermById() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();

			query = session.createQuery(MDSqlConstant.TERM_ID_QUERY);
			query.setParameter(DBConstant.TERM_ID,termId);
			
			list = query.list();
			if (null != list && list.size() > 0) {
				session.delete(list.get(0));
				tx.commit();
				return true;
			}
			throw new MDNoRecordsFoundException(MDProperties.
					read(MDMessageConstant.NO_RECORD_EXCEPTION));
		} catch (HibernateException he) {
			he.printStackTrace();
			if (null != tx){
				tx.rollback();
			}
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("TermsDaoImpl.deleteTermById() emd");
		}
	}

	/*
	 * TermsDaoImpl.updateTerm
	 * 
	 * @param terms
	 * 
	 * @return boolean comments : TODO
	 */
	public boolean updateTerm(Terms terms) throws Exception {

		Session session = null;
		Transaction tx = null;
		logger.info("termsDaoImpl.updateTerm() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.update(terms);
			tx.commit();
			return true;
		} catch (HibernateException he) {
			he.printStackTrace();
			if (null != tx)
				tx.rollback();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			if (null != tx){
				tx.rollback();
			}
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("termsDaoImpl.updateTerm() end");
		}
	}
}
