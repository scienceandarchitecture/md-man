/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : UserRoleDaoImpl.java
 * Comments : TODO 
 * Created  : 23-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |23-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.DBConstant;
import com.meshdynamics.common.LoginCache;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.helper.Helper;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.common.helper.MDSqlConstant;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.model.UserRole;

public class UserRoleDaoImpl implements UserRoleDao {

	private static final Logger logger = Logger
			.getLogger(UserRoleDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.meshdynamics.dao.UserRoleDao#addUserRole(com.meshdynamics.model.UserRole
	 * )
	 */
	@Override
	public boolean addUserRole(UserRole userRole) throws Exception {

		boolean flg = false;
		Session session = null;
		Transaction tx = null;
		logger.info("UserRoleDaoImpl.addUserRole() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.save(userRole);
			tx.commit();
			flg = true;
		} catch (HibernateException he) {
			he.printStackTrace();
			if (Constant.CONSTRAINT_VIOLATION_EXCEPTION == (Helper
					.getExceptionCode(he))) {
				throw new MDBusinessException(
						MDProperties.read(MDMessageConstant.USER_NAME)
								+ "'"
								+ userRole.getUserName()
								+ "'"
								+ MDProperties
										.read(MDMessageConstant.ALREADY_REGISTERED));
			}
			throw new MDDBException(Helper.getExceptionCode(he),
					he.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("UserRoleDaoImpl.addUserRole() end");
		return flg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.dao.UserRoleDao#getUserRoleList()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<UserRole> getUserRoleList() throws Exception {

		Session session = null;
		List<UserRole> userList = null;
		logger.info("UserRoleDaoImpl.getUserRoleList() started");
		try {
			session = sessionFactory.openSession();

			userList = session.createQuery(MDSqlConstant.USER_ROLE_LIST).list();

			if (null == userList || userList.size() < 1)
				throw new MDBusinessException(
						MDProperties.read(MDMessageConstant.USER_NOT_EXIST));

		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he),
					he.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("UserRoleDaoImpl.getUserRoleList() end");
		return userList;
	}

	/*
	 * UserRoleDaoImpl.updateUserRole
	 * 
	 * @param userRole
	 * 
	 * @return boolean comments : TODO
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean updateUserRole(UserRole userRole) throws Exception {

		Query query = null;
		Session session = null;
		Transaction tx = null;
		List<String> userList = null;
		String password = null;
		logger.info("userRoleDaoImpl.updateUserRole() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();

			query = session.createQuery(MDSqlConstant.USER_ROLE_PASSWORD_QUERY);
			query.setParameter(DBConstant.USER_NAME, userRole.getUserName());
			userList = query.list();
			
			if (null != userList && userList.size() > 0)
				password = userList.get(0);

			userRole.setPassword(password);
			session.update(userRole);
			tx.commit();
			return true;

		} catch (HibernateException he) {
			he.printStackTrace();
			if (null != tx) {
				tx.rollback();
			}
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			if (null != tx) {
				tx.rollback();
			}
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
			logger.info("userRoleDaoImpl.updateUserRole() end");
		}
	}

	/*
	 * UserRoleDaoImpl.deleteUserById
	 * 
	 * @param userNameuserRole
	 * 
	 * @return boolean comments : TODO
	 */
	@Override
	public boolean deleteUserById(String userName) throws Exception {

		Query query = null;
		Query innerQuery = null;
		Session session = null;
		Transaction tx = null;
		UserRole user = null;
		List<?> list = null;
		logger.info("userRoleDaoImpl.deleteUserById() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			query = session.createQuery(MDSqlConstant.USER_ROLE_USERNAME_QUERY);
			query.setParameter(DBConstant.USER_NAME, userName);

			user = (UserRole) query.uniqueResult();

			if (null != user) {
				innerQuery = session
						.createSQLQuery(MDSqlConstant.DELETE_USER_ROLE_QUERY);
				innerQuery.setParameter(DBConstant.CREATED_BY, userName);

				list = innerQuery.list();
				if (null != list && list.size() > 0) {
					throw new MDBusinessException(
							MDProperties
									.read(MDMessageConstant.USER_NAME_DELETE_EXCEPTION));
				}
				session.delete(user);
				tx.commit();
				return true;
			}
			throw new MDNoRecordsFoundException(
					MDProperties.read(MDMessageConstant.NO_RECORD_SHIP));

		} catch (HibernateException he) {
			if (null != tx) {
				tx.rollback();
			}
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
			logger.info("userRoleDaoImpl.deleteUserById() end");
		}
	}

	@Override
	public UserRole getUserById(String userName) throws Exception {
	
		Query query = null;
		Object obj = null;
		Session session = null;
		Transaction tx = null;
		logger.info("UserRoledaoImpl.getUserById() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			query = session.createQuery(MDSqlConstant.USER_ROLE_USERNAME_QUERY);
			query.setParameter(DBConstant.USER_NAME, userName);
			obj = query.uniqueResult();
			if (null != obj) {
				return (UserRole) obj;
			}
			throw new MDNoRecordsFoundException(
					MDProperties.read(MDMessageConstant.NO_USER_NAME)
							+ userName
							+ MDProperties.read(MDMessageConstant.NOT_EXIST));

		} catch (HibernateException he) {
			he.printStackTrace();
			if (null != tx){
				tx.rollback();
			}
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("userRoleDaoImpl.getUserById() end");
		}
	}

	/*
	 *  UserRoleDaoImpl.changePassword
	 *  @param apikey
	 *  @param currentPassword
	 *  @param newPassword
	 *  @return boolean
	 *  comments : TODO
	 */
	@Override
	public boolean changePassword(String apikey, String currentPassword,
			String newPassword) throws Exception {
		
		boolean flag = false;
		Session session = null;
		Transaction tx = null;
		String value = null;
		String[] arr = null;
		UserRole userRole = null;
		Map<String, String> map = LoginCache.getInstance();
		logger.info("UserRoleDaoImpl.changePassword() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			value = map.get(apikey);
			arr = value.split(",");
			userRole = getUserById(arr[0]);
			if (null != userRole) {
				if (Helper.encrypt(currentPassword).equals(
						userRole.getPassword())) {
					userRole.setPassword(Helper.encrypt(newPassword));
					session.update(userRole);
					tx.commit();
					flag = true;
				} else {
					throw new MDBusinessException(
							MDProperties
									.read(MDMessageConstant.INCORRECT_PASSWORD));
				}
			}
		} catch (HibernateException e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.info("UserRoleDaoImpl.changePassword() end");
		return flag;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserRole> getUserRoleListBasedOnPagination(Map<?, ?> criteria, int start,
			int noOfRecords) throws Exception {

		int i = 1;
		String key = null;
		Object value = null;
		Session session = null;
		Query criteriaQuery = null;
		StringBuilder strQuery = null;
		List<UserRole> userList = null;
		
		logger.info("UserRoleDaoImpl.getUserRoleListBasedOnPagination() started");
		try {
			logger.info("UserRoleDaoImpl.getCount() started");
			session = sessionFactory.openSession();
			strQuery = new StringBuilder(MDSqlConstant.USER_ROLE_LIST);

			for (Entry<?, ?> entry : criteria.entrySet()) {
				if (i > 1) {
					strQuery = strQuery.append(MDMessageConstant.AND_CLAUSE);
				} else if (i == 1) {
					strQuery = strQuery.append(MDMessageConstant.WHERE_CLAUSE);
				}
				key = (String) entry.getKey();
				value = entry.getValue();
				strQuery = strQuery.append(key + " LIKE " + "'" + value + "%" + "'");
				i++;
			}
			criteriaQuery = session.createQuery(strQuery.toString());
			criteriaQuery.setFirstResult(start - 1);
			criteriaQuery.setMaxResults(noOfRecords);
			userList = criteriaQuery.list();
			
			if (null == userList || userList.size() < 1)
				throw new MDBusinessException(
						MDProperties.read(MDMessageConstant.USER_NOT_EXIST));

		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he),
					he.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("UserRoleDaoImpl.getUserRoleListBasedOnPagination() end");
		return userList;
	}

	@SuppressWarnings("unchecked")
	public int getCount(Map<?, ?> newMapCriteria) throws Exception{

		String key = null;
		Object value = null;
		int i = 1, count = 0;
		Session session = null;
		Query criteriaQuery = null;
		List<UserRole> userList = null;
		StringBuilder strQuery = null;
		try {
			logger.info("UserRoleDaoImpl.getCount() started");
			session = sessionFactory.openSession();
			strQuery = new StringBuilder(MDSqlConstant.USER_ROLE_LIST);

			for (Entry<?, ?> entry : newMapCriteria.entrySet()) {
				if (i > 1) {
					strQuery = strQuery
							.append(MDMessageConstant.AND_CLAUSE);
				} else if (i == 1) {
					strQuery = strQuery.append(MDMessageConstant.WHERE_CLAUSE);
				}
				key = (String) entry.getKey();
				value = entry.getValue();
				strQuery = strQuery.append(key + " LIKE " + "'" + value + "%" + "'");
				i++;
			}
			criteriaQuery = session.createQuery(strQuery.toString());
			userList = criteriaQuery.list();
			count = userList.size();

		} catch (HibernateException he) {
			logger.error(he.getMessage());
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("UserRoleDaoImpl.getCount() end");
		return count;
	}
}