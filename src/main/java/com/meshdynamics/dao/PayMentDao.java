/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : PayMentDao.java
 * Comments : TODO 
 * Created  : 03-Oct-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |03-Oct-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.List;
import java.util.Map;

import com.meshdynamics.model.InvoicePayment;
import com.meshdynamics.model.RawPayment;
import com.meshdynamics.vo.RevenueVo;

public interface PayMentDao {

	boolean addInvoicePayment(InvoicePayment invoicePayment) throws Exception;

	boolean addRawPayment(RawPayment rawPayment) throws Exception;

	RawPayment getRawPaymentById(int rpId) throws Exception;

	boolean deleteRawPaymentById(int rpId) throws Exception;

	List<RawPayment> getRawPayment() throws Exception;

	boolean updateRawPayment(RawPayment rawPayment) throws Exception;

	// List<RawPayment> getOpenRawPaymentList() throws Exception;

	List<InvoicePayment> getInvoicePayment() throws Exception;

	List<InvoicePayment> getInvoicePaymentByInvoiceId(int invId)
			throws Exception;

	boolean deleteInvoicePaymentByInvoiceId(int invId) throws Exception;

	int getPaymentCountByInvoiceId(int invId) throws Exception;
	
	public List<RevenueVo> getRevenueList() throws Exception;

	List<RawPayment> getOpenRawPaymentList(int custId) throws Exception;
	
	public int getCount(Map<?, ?> criteria) throws Exception;
	
	List<RawPayment> getPaymentListUsingPagination(Map<?, ?> criteria,
				int start, int noOfRecords) throws Exception;

	List<RevenueVo> getRevenueListUsingPagination(int revenueDateFlag, Map<?, ?> criteria,
			int start, int noOfRecords) throws Exception;

}
