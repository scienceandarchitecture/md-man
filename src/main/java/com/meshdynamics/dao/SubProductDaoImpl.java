/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : SubProductDaoImpl.java
 * Comments : TODO 
 * Created  : Dec 22, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Dec 22, 2016  | Created                             | Pinki        |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.helper.Helper;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.common.helper.MDSqlConstant;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.model.SubProduct;

public class SubProductDaoImpl implements SubProductDao {

	private static final Logger logger = Logger.getLogger(SubProductDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public boolean addSubProduct(SubProduct subProduct) throws Exception {

		boolean flg = false;
		Session session = null;
		Transaction tx = null;
		logger.info("SubProductDaoImpl.addSubProduct() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.save(subProduct);
			tx.commit();
			flg = true;
		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he),
					he.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.info("SubProductDaoImpl.addSubProduct() end");
		return flg;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<SubProduct> getSubProducts() throws Exception {

		Session session = null;
		List<SubProduct> subProductList = null;
		logger.info("SubProductDaoImpl.getSubProducts() started");
		try {
			session = sessionFactory.openSession();
			subProductList = session
					.createQuery(MDSqlConstant.SUB_PRODUCT_LIST).list();

			if (null == subProductList || subProductList.size() < 1)
				throw new MDBusinessException(
						MDProperties.read(MDMessageConstant.NO_SUB_PRODUCTS));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if( null != session ){
			session.close();
			}
		}
		logger.info("SubProductDaoImpl.getSubProducts() ends");
		return subProductList;
	}
}
