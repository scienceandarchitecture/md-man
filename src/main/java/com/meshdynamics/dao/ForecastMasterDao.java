/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ForecastMasterDao.java
 * Comments : TODO 
 * Created  : 30-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |30-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.List;

import com.meshdynamics.model.ForecastMaster;

public interface ForecastMasterDao {

	boolean addForecastMaster(ForecastMaster forecastMaster) throws Exception;

	List<ForecastMaster> getForecastList() throws Exception;

	ForecastMaster getForecastById(int fcastId) throws Exception;

	boolean deleteForecastByForecastId(int fcastId) throws Exception;

	boolean updateForecast(ForecastMaster forecastMaster) throws Exception;

}
