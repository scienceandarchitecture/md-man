/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : CustomerDao.java
 * Comments : TODO 
 * Created  : Sep 23, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |    Comment                       |     Author    |
 * ---------------------------------------------------------------------------
 * |  0  |Sep 23, 2016 |    Created                    	  |      Pinki	  |  
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.List;

import com.meshdynamics.model.Customer;

public interface CustomerDao {

	boolean addCustomer(Customer customer) throws Exception;

	List<Customer> getCustomerList() throws Exception;

	public boolean deleteCustomerById(int custId) throws Exception;
	
	public Customer getCustomerById(int custId) throws Exception ;

	List<Customer> getCustomerByName(String name) throws Exception;

}
