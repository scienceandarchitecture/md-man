/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : CustomerDaoImpl.java
 * Comments : TODO 
 * Created  : Sep 23, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |    Comment                       |     Author    |
 * ---------------------------------------------------------------------------
 * |  0  |Sep 23, 2016 |    Created                    	  |     Pinki	   |  
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.common.DBConstant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.helper.Helper;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.common.helper.MDSqlConstant;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.model.Customer;

public class CustomerDaoImpl implements CustomerDao {

	final Logger logger = Logger.getLogger(CustomerDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public boolean addCustomer(Customer customer) throws Exception {

		Session session = null;
		Transaction tx = null;
		Query query = null;
		boolean flg = false;
		List<?> customersList = null;
		logger.info("CustomerDaoImpl.addCustomer() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			query = session.createQuery(MDSqlConstant.CUSTOMER_NAME_QUERY);
			customersList = query.setParameter(DBConstant.CUSTOMER_NAME,
					customer.getName().trim()).list();
			if (null != customersList && customersList.size() > 0)
				throw new MDBusinessException(MDProperties.
						read(MDMessageConstant.CUSTOMER_EXIST_EXCEPTION));
			session.save(customer);
			tx.commit();
			flg = true;
		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he),
					he.getMessage(), he.getCause());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(null != session){
			session.close();
			}
		}
		logger.info("CustomerDaoImpl.addCustomer() end");
		return flg;
	}

	/*
	 * CustomerDaoImpl.getCustomerList
	 * 
	 * @return List<Customer> comments : TODO
	 */

	@SuppressWarnings("unchecked")
	@Override
	public List<Customer> getCustomerList() throws Exception {

		Session session = null;
		List<Customer> customersList = null;
		logger.info("CustomerDaoImpl.getCustomerList() started");
		try { 
			session = sessionFactory.openSession();
			customersList = session.createQuery(MDSqlConstant.CUSTOMER_LIST_QUERY).list();
			Collections.sort(customersList,new Customer());
		
			if (null == customersList || customersList.size() < 1)
				throw new MDBusinessException(MDProperties.read
						(MDMessageConstant.NO_CUSTOMER_EXCEPTION));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(null != session){
				session.close();	
			}
		}
		logger.info("CustomerDaoImpl.getCustomerList() end");
		return customersList;
	}

	/*
	 * CustomerDaoImpl.updateCustomerDetails
	 * 
	 * @param customer
	 * 
	 * @return boolean comments : TODO
	 */

	public boolean updateCustomerDetails(Customer customer) throws Exception {

		Transaction tx = null;
		boolean flg = false;
		Session session = null;
		logger.info("CustomerDaoImpl.updateCustomerDetails() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.update(customer);
			tx.commit();
			flg = true;
		} catch (Exception e) {
			if (null != tx) {
				tx.rollback();
			}
			e.printStackTrace();
			throw e;
		} finally {
			if(null != session){
			session.close();
			}
		}
		logger.info("CustomerDaoImpl.updateCustomerDetails() end");
		return flg;
	}

	/*
	 * CustomerDaoImpl.deleteCustomer
	 * 
	 * @param customer
	 * 
	 * @return boolean comments : TODO
	 */
	@SuppressWarnings({ "boxing" })
	@Override
	public boolean deleteCustomerById(int custId) throws Exception {

		Session session = null;
		Transaction tx = null;
		Query query = null;
		Query innerQuery = null;
		List<?> list = null;
		Object obj = null;
		Customer customer = null;
		logger.info("CustomerDaoImpl.deleteCustomerById() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			query = session
					.createQuery(MDSqlConstant.CUSTOMER_QUERY);
			query.setParameter(DBConstant.CUSTOMER_ID, custId);

			obj = query.uniqueResult();
			if (null != obj) {

				innerQuery = session
						.createSQLQuery(MDSqlConstant.CUSTOMER_INNER_QUERY);
				innerQuery.setParameter(DBConstant.CUSTOMER_ID, custId);

				list = innerQuery.list();
				if (null != list && list.size() > 0) {
					throw new MDBusinessException(MDProperties.
							read(MDMessageConstant.CUSTOMER_DELETE_EXCEPTION)); 
				}
				customer = (Customer) obj;
				session.delete(customer);
				tx.commit();
				return true;
			}
			throw new MDNoRecordsFoundException(MDProperties.
					read(MDMessageConstant.NO_CUSTOMER_RECORD_EXCEPTION));

		} catch (HibernateException he) {
			he.printStackTrace();
			if (null != tx){
				tx.rollback();
			}
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(null != session){
			session.close();
			}
			logger.info("CustomerDaoImpl.deleteCustomerById() end");			

		}
	}

	/*
	 * CustomerDaoImpl.getCustomerById
	 * 
	 * @param customer Id
	 * 
	 * @return boolean comments : TODO
	 */
	@SuppressWarnings({ "boxing" })
	@Override
	public Customer getCustomerById(int custId) throws Exception {

		Session session = null;
		Transaction tx = null;
		Query query = null;
		Object obj = null;
		logger.info("CustomerDaoImpl.getCustomerById() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			query = session
					.createQuery(MDSqlConstant.CUSTOMER_QUERY);
			query.setParameter(DBConstant.CUSTOMER_ID, custId);
			obj = query.uniqueResult();
			if (null != obj) {
				return (Customer) obj;
			}
			throw new MDNoRecordsFoundException(MDProperties.
					read(MDMessageConstant.NO_CUSTOMER_RECORD_EXCEPTION));

		} catch (HibernateException he) {
			he.printStackTrace();
			if (null != tx){
				tx.rollback();
			}
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(null != session){
			session.close();
			}
			logger.info("CustomerDaoImpl.getCustomerById() end");
		}
	}

	@Override
	@SuppressWarnings({ "unchecked", "null" })
	public List<Customer> getCustomerByName(String name) throws Exception {

		Query nameQuery = null;
		Session session = null;
		List<Customer> customerList = null;
		logger.info("CustomerDaoImpl.getCustomerByName() started");
		try {
			session = sessionFactory.openSession();
			nameQuery = session.createQuery(MDSqlConstant.CUSTOMER_NAME_LIKE_QUERY+name+"%'");

			customerList = nameQuery.list();
			if (null == customerList && customerList.size() <= 0)
				throw new MDNoRecordsFoundException(MDProperties.read(MDMessageConstant.NO_CUSTOMER_EXCEPTION));

		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he),
					he.getMessage(), he.getCause());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("CustomerDaoImpl.getCustomerByName() end");
		return customerList;
	}
}
