package com.meshdynamics.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.common.helper.Helper;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.model.JobModelFlashDetails;

public class MacVerificationDaoImpl implements MacVerifactionDao {

	private static final Logger logger = Logger.getLogger(MacVerificationDaoImpl.class);
	
	@Autowired
	SessionFactory sessionFactory;
	@Override
	public List<Object[]> macVerification(String macAddress) throws Exception {
		Session session=null;
		List<Object[]> macList=null;
		try {
			session =sessionFactory.openSession();
			Query query=session.createQuery("from JobModelFlashDetails as hr where hr.boardMacAddress=:boardMacAddress");
			query.setParameter("boardMacAddress", macAddress);
			List<JobModelFlashDetails> list=query.list();	
			if(list != null && list.size()>0){
				 JobModelFlashDetails jobModelFlashDetails=list.get(0);
				 Query macDetails=session.createQuery("select macAddress,macType from ModelMacDetails where jobmodelflashdetailsId=:jobmodelflashdetailsId");
				 macDetails.setParameter("jobmodelflashdetailsId", jobModelFlashDetails.getJobmodelflashdetailsId());
				 macList=macDetails.list();
				}
		}  catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he),
					he.getMessage(), he.getCause());
		}catch (Exception e) {
			logger.error("Some internal problem At DB ");
			}finally{
				if(session != null){
					session.close();
				}
			}
		return macList;
	}
	
	@Override
	public List<Object[]> macVerificationInMDMan(String boardMacAddress)
			throws Exception {

		Session session = null;
		List<Object[]> macList = null;
		try {
			session = sessionFactory.openSession();
			Query query = session
					.createQuery("from JobModelFlashDetails as hr where hr.boardMacAddress=:boardMacAddress");
			query.setParameter("boardMacAddress", boardMacAddress);
			List<JobModelFlashDetails> list = query.list();
			if (list != null && list.size() > 0) {
				JobModelFlashDetails jobModelFlashDetails = list.get(0);
				Query macDetails = session
						.createQuery("select macAddress,macType from ModelMacDetails where jobmodelflashdetailsId=:jobmodelflashdetailsId");
				macDetails.setParameter("jobmodelflashdetailsId",
						jobModelFlashDetails.getJobmodelflashdetailsId());
				macList = macDetails.list();
			}
		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he),
					he.getMessage(), he.getCause());
		} catch (Exception e) {
			logger.error("Some internal problem At DB ");
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return macList;
	}
}
