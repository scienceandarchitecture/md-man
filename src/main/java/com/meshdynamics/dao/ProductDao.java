/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ProductDao.java
 * Comments : TODO 
 * Created  : Sep 28, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |    Comment                       |     Author    |
 * ---------------------------------------------------------------------------
 * |  0  |Sep 28, 2016   |    Created                    	  |      Pinki	  |  
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.List;
import java.util.Map;

import com.meshdynamics.model.Product;

public interface ProductDao {

	boolean addProduct(Product product) throws Exception;

	List<Product> getProducts() throws Exception;

	boolean deleteProductById(String psku) throws Exception;

	public Product getProductById(String psku) throws Exception;

	boolean updateProduct(Product product) throws Exception;

	List<String> getProductListByProductId(String psku) throws Exception;

	List<Product> getProductListUsingPagination(Map<?, ?> citeria, int start,
			int noOfRecords) throws Exception;

}
