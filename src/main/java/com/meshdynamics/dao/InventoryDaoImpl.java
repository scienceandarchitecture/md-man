package com.meshdynamics.dao;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.common.DBConstant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.helper.Helper;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.common.helper.MDSqlConstant;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.model.RawInventory;

public class InventoryDaoImpl implements InventoryDao {

	final Logger logger = Logger.getLogger(InventoryDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public boolean addRawInventory(RawInventory rawInventory) throws Exception {

		Session session = null;
		Transaction tx = null;
		boolean flg = false;
		Query query = null;
		List<?> inventoryList = null;
		logger.info("InventoryDaoImpl.addRawInventory() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			query = session.createQuery(MDSqlConstant.INVENTORY_QUERY);
			
			inventoryList = query.setParameter(DBConstant.RSKU,
					rawInventory.getRsku().trim()).list();
			
			if (null != inventoryList && inventoryList.size() > 0)
				throw new MDBusinessException(MDProperties.
						read(MDMessageConstant.DUPLICATE_ENTRY_EXCEPTION)
						+ rawInventory.getRsku());

			session.save(rawInventory);
			tx.commit();
			flg = true;
		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he),
					he.getMessage(), he.getCause());

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(null != session){
			session.close();
			}
		}
		logger.info("InventoryDaoImpl.addRawInventory() end");
		return flg;
	}

	@Override
	public boolean updateRawInventory(RawInventory rawInventory)
			throws Exception {

		Transaction tx = null;
		boolean flg = false;
		Session session = null;
		logger.info("InventoryDaoImpl.updateRawInventory() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.update(rawInventory);
			tx.commit();
			flg = true;
		} catch (Exception e) {
			if (null != tx) {
				tx.rollback();
			}
			e.printStackTrace();
			throw e;
		} finally {
			if(null != session){
			session.close();
			}
		}
		logger.info("InventoryDaoImpl.updateRawInventory() end");
		return flg;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RawInventory> getRawInventoryList() throws Exception {

		Session session = null;
		List<RawInventory> rawInventoriesList = null;
		logger.info("InventoryDaoImpl.getRawInventoryList() started");
		try {
			session = sessionFactory.openSession();
			rawInventoriesList = session.createQuery(MDSqlConstant.INVENTORY_LIST_QUERY)
					.list();

			if (null == rawInventoriesList || rawInventoriesList.size() < 1) {
				throw new MDBusinessException(MDProperties.read
						(MDMessageConstant.DUPLICATE_ENTRY_EXCEPTION));
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if(null != session){
			session.close();
			}
		}
		logger.info("InventoryDaoImpl.getRawInventoryList() end");
		return rawInventoriesList;
	}

	/************************** DeleteRawInventoryById ***********************/
	@Override
	public boolean deleteRawInventoryById(String rsku) throws Exception {

		List<?> list = null;
		Object obj = null;
		Query query = null,innerQuery = null;
		Session session = null;
		Transaction tx = null;
		RawInventory rawInventory = null;
		logger.info("InventoryDaoImpl.deleteRawInventoryById() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			query = session
					.createQuery(MDSqlConstant.INVENTORY_QUERY);
			query.setParameter(DBConstant.RSKU, rsku);
			obj = query.uniqueResult();
			if (null != obj) {

				innerQuery = session
						.createSQLQuery(MDSqlConstant.INNER_INVENTORY_QUERY);
				list = innerQuery.setParameter(DBConstant.RSKU, rsku).list();

				if (null != list && list.size() > 0) {
					throw new MDBusinessException(
							MDProperties.read(MDMessageConstant.INVENTORY_DELETE_EXCEPTION));
				}

				rawInventory = (RawInventory) obj;
				session.delete(rawInventory);
				tx.commit();
				return true;
			}
			throw new MDNoRecordsFoundException(MDProperties.
					read(MDMessageConstant.NO_RECORDS_EXCEPTION )+rsku);

		} catch (HibernateException he) {
			he.printStackTrace();
			if (null != tx)
				tx.rollback();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("InventoryDaoImpl.deleteRawInventoryById() end");
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<RawInventory> getInventoryPaginationList(Map<?, ?> newMapCriteria, int start,
			int noOfRecords) throws Exception {

		int i = 1;
		String key = null;
		Object value = null;
		Query query = null;
		StringBuilder strQuery = null;
		Session session = null;
		List<RawInventory> rawInventoryList = null;
		logger.info("InventoryDaoImpl.getInventoryPaginationList() started");
		try {
			session = sessionFactory.openSession();
			
			strQuery = new StringBuilder(MDSqlConstant.INVENTORY_LIST_QUERY);

			for (Entry<?, ?> entry : newMapCriteria.entrySet()) {
				if (i > 1) {
					strQuery = strQuery.append(" AND ");
				} else if (i == 1) {
					strQuery = strQuery.append(" where ");
				}
				key = (String) entry.getKey();
				value = entry.getValue();
				
				strQuery = strQuery.append(key + " LIKE " + "'%" + value + "%" + "'");
				i++;
			}
			query = session.createQuery(strQuery.toString());
			query.setFirstResult(start - 1);
			query.setMaxResults(noOfRecords);
			rawInventoryList = query.list();
			
			if (null == rawInventoryList || rawInventoryList.size() < 1)
				throw new MDNoRecordsFoundException(
						MDProperties
								.read(MDMessageConstant.NO_RECORD_EXCEPTION));

		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("InventoryDaoImpl.getInventoryPaginationList() end");
		return rawInventoryList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getRawInventoryBySKU(String rsku)
			throws Exception {
		
		Query rawQuery = null;
		Session session = null;
		List<String> rawInventoryDbList = null;
		
		logger.info("InventoryMasterDaoImpl.getRawInventoryBySKU() start");
		try{
			session = sessionFactory.openSession(); 
			rawQuery = session.createQuery(MDSqlConstant.INVENTORY_BY_SKU_QUERY+"%"+rsku+"%'");
			
			rawInventoryDbList = rawQuery.list();
			
			if (null == rawInventoryDbList || rawInventoryDbList.size() < 1)
				throw new MDNoRecordsFoundException(
						MDProperties
								.read(MDMessageConstant.NO_RECORD_EXCEPTION));

		}catch (HibernateException he) {
			logger.error(he.getMessage());
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
			logger.info("InventoryMasterDaoImpl.getRawInventoryBySKU() end");
		}
		return rawInventoryDbList;
	
	}

	@SuppressWarnings("unchecked")
	public int getCount(Map<?, ?> mapCriteria) throws Exception {

		int i = 1,count = 0;
		String key = null;
		Object value = null;
		Query query = null;
		StringBuilder strQuery = null;
		Session session = null;
		List<RawInventory> rawInventoryList = null;
		logger.info("InventoryDaoImpl.getCount() started");
		try {
			session = sessionFactory.openSession();
			
			strQuery = new StringBuilder(MDSqlConstant.INVENTORY_LIST_QUERY);

			for (Entry<?, ?> entry : mapCriteria.entrySet()) {
				if (i > 1) {
					strQuery = strQuery.append(" AND ");
				} else if (i == 1) {
					strQuery = strQuery.append(" where ");
				}
				key = (String) entry.getKey();
				value = entry.getValue();
				
				strQuery = strQuery.append(key + " LIKE " + "'%" + value + "%" + "'");
				i++;
			}
			query = session.createQuery(strQuery.toString());
			rawInventoryList = query.list();
			count = rawInventoryList.size();
			
			if (null == rawInventoryList || rawInventoryList.size() < 1)
				throw new MDNoRecordsFoundException(
						MDProperties
								.read(MDMessageConstant.NO_RECORD_EXCEPTION));

		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("InventoryDaoImpl.getCount() end");
		return count;
	}
}
