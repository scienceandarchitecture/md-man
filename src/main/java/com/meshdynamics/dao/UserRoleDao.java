/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : UserRoleDao.java
 * Comments : TODO 
 * Created  : 23-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |23-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.List;
import java.util.Map;

import com.meshdynamics.model.UserRole;

public interface UserRoleDao {
	boolean addUserRole(UserRole userRole) throws Exception;

	List<UserRole> getUserRoleList() throws Exception;
	
	boolean updateUserRole(UserRole userRole) throws Exception;
	
	boolean deleteUserById(String userName) throws Exception;
	
	UserRole getUserById(String userName) throws Exception;
	
	boolean changePassword(String apikey, String currentPassword,
			String newPassword) throws Exception;

	List<UserRole> getUserRoleListBasedOnPagination(Map<?, ?> newMap,
			int start, int noOfRecords) throws Exception;
}
