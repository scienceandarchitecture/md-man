/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ModelDaoImpl.java
 * Comments : TODO 
 * Created  : Dec 2, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Dec 2, 2016  | Created                             | Pinki		 |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.model.Model;

public class ModelDaoImpl implements ModelDao {

	final Logger logger = Logger.getLogger(ModelDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	/*
	 * ModelDaoImpl.getModels
	 * 
	 * @return List<Model> comments : TODO
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Model> getModels() throws Exception {

		logger.info("ModelDaoImpl.getModels() started");
		Session session = sessionFactory.openSession();
		List<Model> modelList = null;
		try {
			Query query = session.createQuery("from Model");
			modelList = query.list();

			if (modelList == null || modelList.size() < 1)
				throw new MDBusinessException("No model ID exist in Database");

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			session.close();
		}
		logger.info("ModelDaoImpl.getModels() end");
		return modelList;
	}
}
