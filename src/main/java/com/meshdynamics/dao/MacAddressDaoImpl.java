package com.meshdynamics.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.common.DBConstant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.helper.Helper;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.common.helper.MDSqlConstant;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.model.InvoiceItem;
import com.meshdynamics.model.InvoiceItemSubProduct;
import com.meshdynamics.model.JobModelFlashDetails;
import com.meshdynamics.vo.MacAddressInvIdVo;
import com.meshdynamics.vo.MacAddressInvoiceItemIdVo;
import com.meshdynamics.vo.MacAddressSummaryVo;
import com.meshdynamics.vo.MacDetailsInvoiceItemVo;
import com.meshdynamics.vo.MacDetailsMetaResultVo;
import com.meshdynamics.vo.MacRangeVo;
import com.meshdynamics.vo.MacSubProductCountVo;

public class MacAddressDaoImpl implements MacAddressDao {

	final Logger logger = Logger.getLogger(CustomerDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<MacAddressSummaryVo> getMacAddressSummaryList()
			throws Exception {

		int custId = 0; // index 0
		int invId = 0; // index 2
		int invoiceItemId = 0; // index 3
		//int invoiceitemsubproductId = 0; // index 6
		int custid1 = 0;
		int invId1 = 0;
		int invoiceItemId1 = 0;
		Session session = null;
		String sqlquery = null;
		SQLQuery query = null;
		List<Object[]> rows = null;
		MacAddressSummaryVo macAddressSummaryVo = null;
		MacAddressInvIdVo macAddressInvIdVo = null;
		MacAddressInvoiceItemIdVo macAddressInvoiceItemIdVo = null;
		int tolalMacPerCutomer = 0,invMacAddressCount = 0;
		List<MacAddressSummaryVo> macAddressSummaryList = null;
		logger.info("MacAddressDaoImpl.getMacAddressSummaryList() started");
		try {

		session = sessionFactory.openSession();
		sqlquery = MDSqlConstant.MAC_ADDRESS_QUERY;
		
			query = session.createSQLQuery(sqlquery);
			
			rows = query.list();

			if (null == rows || rows.size() < 1) {
				throw new MDBusinessException(
						MDProperties.read(MDMessageConstant.MAC_ADDRESS_NOT_EXIST));
			} else {
				macAddressSummaryList = new ArrayList<MacAddressSummaryVo>();
			}
			
			for (Object[] row : rows) {

				custid1 = Integer.parseInt(row[0].toString()); // index 0
																	// //560071
				invId1 = Integer.parseInt(row[2].toString());
				; // index 2 //97
				invoiceItemId1 = Integer.parseInt(row[3].toString());
				; // index 3 //253
					// int invoiceitemsubproductId1 =
					// Integer.parseInt(row[6].toString());; //index 6 //1001
				if (custId != custid1) {
					tolalMacPerCutomer = 0;
					macAddressSummaryVo = new MacAddressSummaryVo();
					custId = custid1;
					macAddressSummaryVo.setCustId(custid1);
					if(null != row[1]){
						macAddressSummaryVo.setCustomerName(row[1].toString());
					}
					macAddressSummaryList.add(macAddressSummaryVo);
				}

				if (null == macAddressSummaryVo.getMacAddressInvIds()) {
					List<MacAddressInvIdVo> macAddressInvIds = new ArrayList<MacAddressInvIdVo>();
					macAddressSummaryVo.setMacAddressInvIds(macAddressInvIds);
				}

				if (invId != invId1) {
					invMacAddressCount = 0;

					macAddressInvIdVo = new MacAddressInvIdVo();
					macAddressInvIdVo.setInvId(invId1);
					invId = invId1;
					macAddressSummaryVo.getMacAddressInvIds().add(
							macAddressInvIdVo);

				}
				if (null == macAddressInvIdVo.getMacAddressInvoiceItemIs()) {
					List<MacAddressInvoiceItemIdVo> macAddressInvoiceItemIs = new ArrayList<MacAddressInvoiceItemIdVo>();
					macAddressInvIdVo
							.setMacAddressInvoiceItemIs(macAddressInvoiceItemIs);
				}
				if (invoiceItemId != invoiceItemId1) {
					macAddressInvoiceItemIdVo = new MacAddressInvoiceItemIdVo();
					macAddressInvoiceItemIdVo.setInvoiceItemId(invoiceItemId1);
					macAddressInvoiceItemIdVo.setPsku(row[4].toString());
					macAddressInvoiceItemIdVo.setItemQuantity(Integer
							.parseInt(row[5].toString()));
					macAddressInvoiceItemIdVo.setAddressCount(Integer
							.parseInt(row[6].toString()));
					macAddressInvIdVo.getMacAddressInvoiceItemIs().add(
							macAddressInvoiceItemIdVo);
					invoiceItemId = invoiceItemId1;
					tolalMacPerCutomer = tolalMacPerCutomer
							+ ((Integer.parseInt(row[5].toString())) * (Integer
									.parseInt(row[6].toString())));
					macAddressSummaryVo.setTotalMacCount(tolalMacPerCutomer);

					invMacAddressCount = invMacAddressCount
							+ ((Integer.parseInt(row[5].toString())) * (Integer
									.parseInt(row[6].toString())));
					macAddressInvIdVo.setInvMacAddressCount(invMacAddressCount);
				}
				if (null == macAddressInvoiceItemIdVo.getMacSubProducts()) {

					Query invoiceItemSubProductQuery = session
							.createQuery(MDSqlConstant.INVOICE_ITEM_SUB_PRODUCT_QUERY);
					invoiceItemSubProductQuery.setParameter(DBConstant.INVOICE_ITEM_ID,
							invoiceItemId1);

					List<InvoiceItemSubProduct> invoiceItemSubProductList = invoiceItemSubProductQuery
							.list();
					if (null != invoiceItemSubProductList
							&& invoiceItemSubProductList.size() > 0) {
						List<MacSubProductCountVo> macSubProductCount = new ArrayList<MacSubProductCountVo>();
						macAddressInvoiceItemIdVo
								.setMacSubProducts(macSubProductCount);
						for (InvoiceItemSubProduct invoiceItemSubProduct : invoiceItemSubProductList) {

							MacSubProductCountVo macSubProductCountVo = new MacSubProductCountVo();

							macSubProductCountVo
									.setInvoiceItemSubProductQuantity(invoiceItemSubProduct
											.getQuantity());
							macSubProductCountVo
									.setSubProductName(invoiceItemSubProduct
											.getSubProduct()
											.getSubProductName());
							tolalMacPerCutomer = tolalMacPerCutomer
									+ macAddressInvoiceItemIdVo
											.getItemQuantity()
									* macSubProductCountVo
											.getInvoiceItemSubProductQuantity();
							invMacAddressCount = invMacAddressCount
									+ macAddressInvoiceItemIdVo
											.getItemQuantity()
									* macSubProductCountVo
											.getInvoiceItemSubProductQuantity();

							macAddressInvoiceItemIdVo.getMacSubProducts().add(
									macSubProductCountVo);
							macAddressSummaryVo
									.setTotalMacCount(tolalMacPerCutomer);
							macAddressInvIdVo
									.setInvMacAddressCount(invMacAddressCount);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if( null != session){
			session.close();
			}
		}
		logger.info("MacAddressDaoImpl.getMacAddressSummaryList() end");
		return macAddressSummaryList;

	}

	/*
	 * MacAddressDaoImpl.fetchMacRangeList
	 * 
	 * @return List<MacAddressSummaryVo> comments : TODO
	 */
	@SuppressWarnings({ "boxing", "unchecked" })
	@Override
	public MacDetailsMetaResultVo fetchMacRangeList(MacRangeVo macRangeVo, int start, int noOfRecords)
			throws Exception {

		int jobId = 0;
		int count = 0;
		int md4count = 0;
		int mdCount = 0;
		String boardMac = null;
		Session session = null;
		int totalMacQuantity = 0;
		List<?> jobOrderList = null;
		List<Object[]> macList = null;
		Query macQuery = null, jobIdQuery = null, boardMacQuery = null;
		List<String> macAddressList = null;
		List<?> boardMacList = null;
		int subItemTotalQunatity = 0;
		int jobmodelflashdetailsId = 0;
		Query jobIdFlashQuery = null;
		Query totalInvoiceItemQuery = null;
		int consumedMac = 0, availableMac = 0, flashedMacCount = 0;
		List<MacDetailsInvoiceItemVo> macDetailsInvoiceItemVoList = null;
		MacDetailsMetaResultVo macDetailsMetaResultVo = null;

		logger.info("MacAddressDaoImpl.fetchMacRangeList() started");
		try {
			session = sessionFactory.openSession();
			jobIdQuery = session.createQuery(MDSqlConstant.FETCH_JOB_ID);
			jobIdQuery.setParameter(DBConstant.INVOICE_ID,
					macRangeVo.getInvId());

			jobOrderList = jobIdQuery.list();
			if (null == jobOrderList || jobOrderList.size() < 1) {
				logger.warn(MDProperties
						.read(MDMessageConstant.NO_JOB_CREATED_EXCEPTION));
			} else {
				jobId = Integer.valueOf(jobOrderList.get(0).toString());
				boardMacQuery = session
						.createQuery(MDSqlConstant.FETCH_BOARD_MAC_ADDRESS);
				boardMacQuery.setParameter(DBConstant.JOB_ID, jobId);
				boardMacQuery.setFirstResult(start - 1);
				boardMacQuery.setMaxResults(noOfRecords);
				boardMacList = boardMacQuery.list();
			}
			
			jobIdFlashQuery = session.createQuery(MDSqlConstant.JOB_MODEL_FLASH_QUERY);
			jobIdFlashQuery.setParameter(DBConstant.JOB_ID, jobId);
			
			List<JobModelFlashDetails> jobModelFlashList = jobIdFlashQuery.list();
			for (JobModelFlashDetails jobModelFlashDetails : jobModelFlashList) {
				Query modelFlashQuery = session
						.createQuery(MDSqlConstant.COUNT_MAC_USING_FLASH_ID_QUERY);
				modelFlashQuery.setParameter(DBConstant.FLASH_ID,
						jobModelFlashDetails.getJobmodelflashdetailsId());

				consumedMac += Integer.parseInt(modelFlashQuery.list().get(0)
						.toString());

			}
			totalInvoiceItemQuery = session
					.createQuery(MDSqlConstant.INVOICE_ITEM_QUERY);
			totalInvoiceItemQuery.setParameter(DBConstant.INVOICE_ID, macRangeVo.getInvId());

			List<InvoiceItem> totalInvoiceItemList = totalInvoiceItemQuery
					.list();
			
			for (InvoiceItem invoiceItem : totalInvoiceItemList) {
				int invoiceQuantity = invoiceItem.getQuantity();
				int subItemQunatity = 0;
				Query invoiceItemQuery = session
						.createQuery(MDSqlConstant.INVOICE_ITEM_SUB_PRODUCT_QUERY);
				invoiceItemQuery.setParameter(DBConstant.INVOICE_ITEM_ID,
						invoiceItem.getInvoiceItemId());

				List<InvoiceItemSubProduct> invoiceItemSubProductList = invoiceItemQuery
						.list();

				for (InvoiceItemSubProduct invoiceItemSubProduct : invoiceItemSubProductList) {
					Query quantityQuery = session
							.createQuery(MDSqlConstant.INVOICE_SUB_PRODUCT_QUERY);
					quantityQuery.setParameter(
							DBConstant.INVOICE_ITEM_SUB_PRODUCT_ID,
							invoiceItemSubProduct.getInvoiceItemSubProductId());

					List<InvoiceItemSubProduct> subItemQuantityList = quantityQuery
							.list();
					if (null != subItemQuantityList
							&& subItemQuantityList.size() > 0)
						subItemQunatity = subItemQunatity + subItemQuantityList.get(0)
								.getQuantity();
				}
				
				if(invoiceItem.getPsku().startsWith("MD4")){
					subItemTotalQunatity = subItemQunatity + 7;
				} else if(invoiceItem.getPsku().startsWith("MD6")){
					subItemTotalQunatity = subItemQunatity + 9;
				} else if(invoiceItem.getPsku().startsWith("MD1")){
					subItemTotalQunatity = subItemQunatity + 4;
				}
				
				totalMacQuantity += (subItemTotalQunatity * invoiceQuantity);
			}
			
			if (availableMac == 0) {
				availableMac = (totalMacQuantity) - consumedMac;
			} else {
				availableMac = availableMac - consumedMac;
			}

			macDetailsMetaResultVo = new MacDetailsMetaResultVo();
			macDetailsInvoiceItemVoList = new ArrayList<MacDetailsInvoiceItemVo>();
			macDetailsMetaResultVo.setCustomerName(macRangeVo.getCustName());
			macDetailsMetaResultVo.setInvId(macRangeVo.getInvId());
			if (macRangeVo.getTotalMac() == 0) {
				macDetailsMetaResultVo.setTotalMac(totalMacQuantity);
			} else {
				macDetailsMetaResultVo.setTotalMac(macRangeVo.getTotalMac());
			}

			macDetailsMetaResultVo.setAvailableMac(macRangeVo.getTotalMac());

			if (null != boardMacList && boardMacList.size() > 0) {

				for (int i = 0; i < boardMacList.size(); i++) {
					boardMac = (String) boardMacList.get(i);
					macQuery = session
							.createSQLQuery(MDSqlConstant.FETCH_MAC_QUERY);
					macQuery.setParameter(DBConstant.INVOICE_ID,
							macRangeVo.getInvId());
					macQuery.setParameter(DBConstant.BOARD_MAC, boardMac);
					macList = macQuery.list();

					if (null == macList || macList.size() < 1) {
						throw new MDBusinessException(
								MDProperties
										.read(MDMessageConstant.MAC_ADDRESS_NOT_EXIST));
					}

					MacDetailsInvoiceItemVo macDetailsInvoiceItemVo = null;

					Integer addJobModelFlashDetails = new Integer(0);

					// 0= invid,1= psku, 2=App, 3=Vlan,
					for (Object[] mac : macList) {
						if (null != mac) {
							macAddressList = new ArrayList<String>();

							if (jobmodelflashdetailsId != Integer
									.parseInt(mac[7].toString())) {

								macDetailsInvoiceItemVo = new MacDetailsInvoiceItemVo();
								macDetailsInvoiceItemVo.setPsku(mac[1]
										.toString());
								macDetailsInvoiceItemVo
										.setInvoiceItemId(Integer
												.parseInt(mac[4].toString()));
								macDetailsInvoiceItemVo.setBoardMac(mac[5]
										.toString());

								if (null != mac[2]) {
									macDetailsInvoiceItemVo.setApp(mac[2]
											.toString());
								} else {
									macDetailsInvoiceItemVo.setApp("");
								}
								if (null != mac[3]) {

									String str = mac[3].toString();
									int n = Integer.parseInt(str.substring(7)
											.trim().toString());
									if (jobModelFlashList.get(start - 1)
											.getJobItem().getInvoiceItem()
											.getPsku().startsWith("MD4")) {
										macDetailsInvoiceItemVo
												.setVlan((str.substring(0, 6)
														+ ")  " + "  ("
														+ (++md4count) + "/" + n)
														.toString());
										start++;
									} else if (jobModelFlashList.get(start - 1)
											.getJobItem().getInvoiceItem()
											.getPsku().startsWith("MD6")) {
										macDetailsInvoiceItemVo.setVlan((str
												.substring(0, 6)
												+ ")  "
												+ "  (" + (++count) + "/" + n)
												.toString());
										start++;
									} else if (jobModelFlashList.get(start - 1)
											.getJobItem().getInvoiceItem()
											.getPsku().startsWith("MD1")) {
										macDetailsInvoiceItemVo
												.setVlan((str.substring(0, 6)
														+ ")  " + "  ("
														+ (++mdCount) + "/" + n)
														.toString());
										start++;
									} else {
										macDetailsInvoiceItemVo.setVlan("");
									}

								}

								jobmodelflashdetailsId = Integer
										.parseInt(mac[7].toString());
								macDetailsInvoiceItemVo
										.setMacList(macAddressList);

								macDetailsInvoiceItemVo.setVersion(mac[8]
										.toString());
							}
						}
						if (null != mac[9].toString()) {
							macDetailsInvoiceItemVo.getMacList().add(
									mac[9].toString());
							macDetailsInvoiceItemVo
									.setMacCount((++addJobModelFlashDetails)
											.intValue());
						}
					}
					flashedMacCount = flashedMacCount + consumedMac;
					macDetailsInvoiceItemVoList.add(macDetailsInvoiceItemVo);
					if (null != macList && null != macDetailsMetaResultVo
							&& null != macDetailsInvoiceItemVo) {
						macDetailsMetaResultVo
								.setMacDetailsInvoiceItemList(macDetailsInvoiceItemVoList);
						macDetailsMetaResultVo
								.setFlashedMacCount(flashedMacCount);
						macDetailsMetaResultVo.setAvailableMac(availableMac);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("MacAddressDaoImpl.fetchMacRangeList() end");
		return macDetailsMetaResultVo;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<MacAddressSummaryVo> getMacAddressListUsingPagination(Map<?, ?> newMap, int start,
			int noOfRecords) throws Exception {

		int i = 1;
		String key = null;
		Object value = null;
		int custId = 0; // index 0
		int invId = 0; // index 2
		int invoiceItemId = 0; // index 3
		int custid1 = 0;
		int invId1 = 0;
		int invoiceItemId1 = 0;
		Session session = null;
		StringBuilder sqlquery = null;
		SQLQuery query = null;
		List<Object[]> rows = null;
		MacAddressSummaryVo macAddressSummaryVo = null;
		MacAddressInvIdVo macAddressInvIdVo = null;
		MacAddressInvoiceItemIdVo macAddressInvoiceItemIdVo = null;
		int tolalMacPerCutomer = 0, invMacAddressCount = 0;
		List<MacAddressSummaryVo> macAddressSummaryList = null;
		logger.info("MacAddressDaoImpl.getMacAddressListUsingPagination() started");
		try {

			session = sessionFactory.openSession();
			sqlquery = new StringBuilder(MDSqlConstant.MAC_ADDRESS_QUERY);

			for (Entry<?, ?> entry : newMap.entrySet()) {
				if (i > 0) {
					sqlquery = sqlquery.append(" AND ");
				}
				key = (String) entry.getKey();
				value = entry.getValue();
				if (key.equals(DBConstant.INVOICE_ID)){
					sqlquery = sqlquery.append(DBConstant.PO_PREFIX + key + " LIKE " + "'" + value + "%" + "'");
				} else if(key.equals(DBConstant.CUSTOMER_NAME)){
					sqlquery = sqlquery.append(DBConstant.CUSTOMER_PREFIX + key + " LIKE " + "'" + value + "%" + "'");
				} else {
					sqlquery = sqlquery.append(key + " LIKE " + "'" + value + "%" + "'");
				}
				i++;
			}
			sqlquery.append(MDSqlConstant.MAC_ADDRESS_QUERY_ORDER_BY);
			
			query = session.createSQLQuery(sqlquery.toString());
			query.setFirstResult(start - 1);
			query.setMaxResults(noOfRecords);
			rows = query.list();

			if (null == rows || rows.size() < 1) {
				throw new MDBusinessException(
						MDProperties
								.read(MDMessageConstant.MAC_ADDRESS_NOT_EXIST));
			} else {
				macAddressSummaryList = new ArrayList<MacAddressSummaryVo>();
			}

			for (Object[] row : rows) {

				custid1 = Integer.parseInt(row[0].toString()); // index 0

				invId1 = Integer.parseInt(row[2].toString()); // index 2

				invoiceItemId1 = Integer.parseInt(row[3].toString()); // index 3

				if (custId != custid1) {
					tolalMacPerCutomer = 0;
					macAddressSummaryVo = new MacAddressSummaryVo();
					custId = custid1;
					macAddressSummaryVo.setCustId(custid1);
					if (null != row[1]) {
						macAddressSummaryVo.setCustomerName(row[1].toString());
					}
					macAddressSummaryList.add(macAddressSummaryVo);
				}

				if (null == macAddressSummaryVo.getMacAddressInvIds()) {
					List<MacAddressInvIdVo> macAddressInvIds = new ArrayList<MacAddressInvIdVo>();
					macAddressSummaryVo.setMacAddressInvIds(macAddressInvIds);
				}

				if (invId != invId1) {
					invMacAddressCount = 0;

					macAddressInvIdVo = new MacAddressInvIdVo();
					macAddressInvIdVo.setInvId(invId1);
					invId = invId1;
					macAddressSummaryVo.getMacAddressInvIds().add(
							macAddressInvIdVo);

				}
				if (null == macAddressInvIdVo.getMacAddressInvoiceItemIs()) {
					List<MacAddressInvoiceItemIdVo> macAddressInvoiceItemIs = new ArrayList<MacAddressInvoiceItemIdVo>();
					macAddressInvIdVo
							.setMacAddressInvoiceItemIs(macAddressInvoiceItemIs);
				}
				if (invoiceItemId != invoiceItemId1) {
					macAddressInvoiceItemIdVo = new MacAddressInvoiceItemIdVo();
					macAddressInvoiceItemIdVo.setInvoiceItemId(invoiceItemId1);
					macAddressInvoiceItemIdVo.setPsku(row[4].toString());
					macAddressInvoiceItemIdVo.setItemQuantity(Integer
							.parseInt(row[5].toString()));
					macAddressInvoiceItemIdVo.setAddressCount(Integer
							.parseInt(row[6].toString()));
					macAddressInvIdVo.getMacAddressInvoiceItemIs().add(
							macAddressInvoiceItemIdVo);
					invoiceItemId = invoiceItemId1;
					tolalMacPerCutomer = tolalMacPerCutomer
							+ ((Integer.parseInt(row[5].toString())) * (Integer
									.parseInt(row[6].toString())));
					macAddressSummaryVo.setTotalMacCount(tolalMacPerCutomer);

					invMacAddressCount = invMacAddressCount
							+ ((Integer.parseInt(row[5].toString())) * (Integer
									.parseInt(row[6].toString())));
					macAddressInvIdVo.setInvMacAddressCount(invMacAddressCount);
				}
				if (null == macAddressInvoiceItemIdVo.getMacSubProducts()) {

					Query invoiceItemSubProductQuery = session
							.createQuery(MDSqlConstant.INVOICE_ITEM_SUB_PRODUCT_QUERY);
					invoiceItemSubProductQuery.setParameter(
							DBConstant.INVOICE_ITEM_ID, invoiceItemId1);

					List<InvoiceItemSubProduct> invoiceItemSubProductList = invoiceItemSubProductQuery
							.list();
					if (null != invoiceItemSubProductList
							&& invoiceItemSubProductList.size() > 0) {
						List<MacSubProductCountVo> macSubProductCount = new ArrayList<MacSubProductCountVo>();
						macAddressInvoiceItemIdVo
								.setMacSubProducts(macSubProductCount);
						for (InvoiceItemSubProduct invoiceItemSubProduct : invoiceItemSubProductList) {

							MacSubProductCountVo macSubProductCountVo = new MacSubProductCountVo();

							macSubProductCountVo
									.setInvoiceItemSubProductQuantity(invoiceItemSubProduct
											.getQuantity());
							macSubProductCountVo
									.setSubProductName(invoiceItemSubProduct
											.getSubProduct()
											.getSubProductName());
							tolalMacPerCutomer = tolalMacPerCutomer
									+ macAddressInvoiceItemIdVo
											.getItemQuantity()
									* macSubProductCountVo
											.getInvoiceItemSubProductQuantity();
							invMacAddressCount = invMacAddressCount
									+ macAddressInvoiceItemIdVo
											.getItemQuantity()
									* macSubProductCountVo
											.getInvoiceItemSubProductQuantity();

							macAddressInvoiceItemIdVo.getMacSubProducts().add(
									macSubProductCountVo);
							macAddressSummaryVo
									.setTotalMacCount(tolalMacPerCutomer);
							macAddressInvIdVo
									.setInvMacAddressCount(invMacAddressCount);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("MacAddressDaoImpl.getMacAddressListUsingPagination() end");
		return macAddressSummaryList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MacAddressSummaryVo> getMacAddressListBasedSearchCritera(
			Map<?, ?> newMap, int start, int noOfRecords) throws Exception {

		int i = 1;
		int custId = 0;
		int invId = 0;
		int invoiceItemId = 0;
		int custid1 = 0;
		int invId1 = 0;
		int invoiceItemId1 = 0;
		String key = null;
		Object value = null;
		Session session = null;
		StringBuilder sqlquery = null;
		Query criteriaQuery = null;
		List<Object[]> rows = null;
		MacAddressSummaryVo macAddressSummaryVo = null;
		MacAddressInvIdVo macAddressInvIdVo = null;
		MacAddressInvoiceItemIdVo macAddressInvoiceItemIdVo = null;
		int tolalMacPerCutomer = 0, invMacAddressCount = 0;
		List<MacAddressSummaryVo> macAddressSummaryList = null;
		logger.info("MacAddressDaoImpl.getMacAddressListBasedSearchCritera() started");
		try {

			session = sessionFactory.openSession();

			sqlquery = new StringBuilder(MDSqlConstant.MAC_ADDRESS_QUERY);

			for (Entry<?, ?> entry : newMap.entrySet()) {
				if (i > 0) {
					sqlquery = sqlquery.append(" AND ");
				}
				key = (String) entry.getKey();
				value = entry.getValue();

				sqlquery = sqlquery.append(key + " LIKE " + "'" + value + "%"
						+ "'");
				i++;
			}
			criteriaQuery = session.createSQLQuery(sqlquery.toString());
			criteriaQuery.setFirstResult(start - 1);
			criteriaQuery.setMaxResults(noOfRecords);
			rows = criteriaQuery.list();

			if (null == rows || rows.size() < 1) {
				throw new MDBusinessException(
						MDProperties
								.read(MDMessageConstant.MAC_ADDRESS_NOT_EXIST));
			} else {
				macAddressSummaryList = new ArrayList<MacAddressSummaryVo>();
			}

			for (Object[] row : rows) {

				custid1 = Integer.parseInt(row[0].toString());

				invId1 = Integer.parseInt(row[2].toString());

				invoiceItemId1 = Integer.parseInt(row[3].toString());

				if (custId != custid1) {
					tolalMacPerCutomer = 0;
					macAddressSummaryVo = new MacAddressSummaryVo();
					custId = custid1;
					macAddressSummaryVo.setCustId(custid1);
					if (null != row[1]) {
						macAddressSummaryVo.setCustomerName(row[1].toString());
					}
					macAddressSummaryList.add(macAddressSummaryVo);
				}

				if (null == macAddressSummaryVo.getMacAddressInvIds()) {
					List<MacAddressInvIdVo> macAddressInvIds = new ArrayList<MacAddressInvIdVo>();
					macAddressSummaryVo.setMacAddressInvIds(macAddressInvIds);
				}

				if (invId != invId1) {
					invMacAddressCount = 0;

					macAddressInvIdVo = new MacAddressInvIdVo();
					macAddressInvIdVo.setInvId(invId1);
					invId = invId1;
					macAddressSummaryVo.getMacAddressInvIds().add(
							macAddressInvIdVo);

				}
				if (null == macAddressInvIdVo.getMacAddressInvoiceItemIs()) {
					List<MacAddressInvoiceItemIdVo> macAddressInvoiceItemIs = new ArrayList<MacAddressInvoiceItemIdVo>();
					macAddressInvIdVo
							.setMacAddressInvoiceItemIs(macAddressInvoiceItemIs);
				}
				if (invoiceItemId != invoiceItemId1) {
					macAddressInvoiceItemIdVo = new MacAddressInvoiceItemIdVo();
					macAddressInvoiceItemIdVo.setInvoiceItemId(invoiceItemId1);
					macAddressInvoiceItemIdVo.setPsku(row[4].toString());
					macAddressInvoiceItemIdVo.setItemQuantity(Integer
							.parseInt(row[5].toString()));
					macAddressInvoiceItemIdVo.setAddressCount(Integer
							.parseInt(row[6].toString()));
					macAddressInvIdVo.getMacAddressInvoiceItemIs().add(
							macAddressInvoiceItemIdVo);
					invoiceItemId = invoiceItemId1;
					tolalMacPerCutomer = tolalMacPerCutomer
							+ ((Integer.parseInt(row[5].toString())) * (Integer
									.parseInt(row[6].toString())));
					macAddressSummaryVo.setTotalMacCount(tolalMacPerCutomer);

					invMacAddressCount = invMacAddressCount
							+ ((Integer.parseInt(row[5].toString())) * (Integer
									.parseInt(row[6].toString())));
					macAddressInvIdVo.setInvMacAddressCount(invMacAddressCount);
				}
				if (null == macAddressInvoiceItemIdVo.getMacSubProducts()) {

					Query invoiceItemSubProductQuery = session
							.createQuery(MDSqlConstant.INVOICE_ITEM_SUB_PRODUCT_QUERY);
					invoiceItemSubProductQuery.setParameter(
							DBConstant.INVOICE_ITEM_ID, invoiceItemId1);

					List<InvoiceItemSubProduct> invoiceItemSubProductList = invoiceItemSubProductQuery
							.list();
					if (null != invoiceItemSubProductList
							&& invoiceItemSubProductList.size() > 0) {
						List<MacSubProductCountVo> macSubProductCount = new ArrayList<MacSubProductCountVo>();
						macAddressInvoiceItemIdVo
								.setMacSubProducts(macSubProductCount);
						for (InvoiceItemSubProduct invoiceItemSubProduct : invoiceItemSubProductList) {

							MacSubProductCountVo macSubProductCountVo = new MacSubProductCountVo();

							macSubProductCountVo
									.setInvoiceItemSubProductQuantity(invoiceItemSubProduct
											.getQuantity());
							macSubProductCountVo
									.setSubProductName(invoiceItemSubProduct
											.getSubProduct()
											.getSubProductName());
							tolalMacPerCutomer = tolalMacPerCutomer
									+ macAddressInvoiceItemIdVo
											.getItemQuantity()
									* macSubProductCountVo
											.getInvoiceItemSubProductQuantity();
							invMacAddressCount = invMacAddressCount
									+ macAddressInvoiceItemIdVo
											.getItemQuantity()
									* macSubProductCountVo
											.getInvoiceItemSubProductQuantity();

							macAddressInvoiceItemIdVo.getMacSubProducts().add(
									macSubProductCountVo);
							macAddressSummaryVo
									.setTotalMacCount(tolalMacPerCutomer);
							macAddressInvIdVo
									.setInvMacAddressCount(invMacAddressCount);
						}
					}
				}
			}
		} catch (HibernateException he) {
			logger.error(he.getMessage());
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("MacAddressDaoImpl.getMacAddressListBasedSearchCritera() end");
		return macAddressSummaryList;
	}

	public int getCount(Map<?, ?> newMapCriteria) throws Exception {
		
		int i = 1;
		int count = 0;
		String key = null;
		Object value = null;
		Session session = null;
		StringBuilder sqlquery = null;
		List<?> rows = null;
		Query criteriaQuery = null;

		logger.info("MacAddressDaoImpl.getCount() started");
		try {
			session = sessionFactory.openSession();

			sqlquery = new StringBuilder(MDSqlConstant.COUNT_MAC_QUERY);
			
			for (Entry<?, ?> entry : newMapCriteria.entrySet()) {
				if (i > 0) {
					sqlquery = sqlquery.append(" AND ");
				}
				key = (String) entry.getKey();
				value = entry.getValue();
				if (key.equals(DBConstant.INVOICE_ID)){
					sqlquery = sqlquery.append(DBConstant.PO_PREFIX + key + " LIKE " + "'" + value + "%" + "'");
				} else if(key.equals(DBConstant.CUSTOMER_NAME)){
					sqlquery = sqlquery.append(DBConstant.CUSTOMER_PREFIX + key + " LIKE " + "'" + value + "%" + "'");
				} else {
					sqlquery = sqlquery.append(key + " LIKE " + "'" + value + "%" + "'");
				}
				i++;
			}
			criteriaQuery = session.createSQLQuery(sqlquery.toString());
			rows = criteriaQuery.list();
			
			if(null != rows){
				count = (int) ((BigInteger)rows.get(0)).intValue();
			}
			
		} catch (HibernateException he) {
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
			logger.info("MacAddressDaoImpl.getCount() end");
		}
		return count;
	}

	@SuppressWarnings("unchecked")
	@Override
	public MacDetailsMetaResultVo viewMacDetailsBasedOnCriteria(
			Map<?, ?> criteria) throws Exception {

		StringBuilder sqlquery = null;
		String key = null;
		String custName = null;
		Object value = null;
		int invId = 0,jobId = 0;
		Query invIdQuery = null;
		Session session = null;
		int subItemTotalQunatity = 0;
		List<?> invoiceOrderList = null;
		List<Object[]> macList = null;
		Query macQuery = null;
		Query boardMacQuery = null;
		List<?> boardMacList = null;
		List<String> macAddressList = null;
		int jobmodelflashdetailsId = 0;
		int totalInvoiceItemQuantity = 0;
		Query totalInvoiceItemQuery = null;
		int consumedMac = 0, availableMac = 0, flashedMacCount = 0;
		List<MacDetailsInvoiceItemVo> macDetailsInvoiceItemVoList = null;
		MacDetailsMetaResultVo macDetailsMetaResultVo = null;

		logger.info("MacAddressDaoImpl.viewMacDetailsBasedOnCriteria() started");
		try {
				session = sessionFactory.openSession();

				sqlquery = new StringBuilder(
						MDSqlConstant.FETCH_MAC_ON_CRITERIA_QUERY);

				for (Entry<?, ?> entry : criteria.entrySet()) {
					key = (String) entry.getKey();
					value = entry.getValue();
					if (key == DBConstant.MAC_ADDRESS) {

						Query flashIdQuery = session
								.createSQLQuery(MDSqlConstant.FLASH_ID_BASED_MAC_ADDRESS_QUERY);  
						flashIdQuery.setParameter(DBConstant.MAC_ADDRESS, value);

						List<Integer> flashIdQueryList = flashIdQuery.list();

						if (null != flashIdQueryList) {
							int flashId = Integer.valueOf(flashIdQueryList.get(0)
									.toString());
							invIdQuery = session
									.createSQLQuery(MDSqlConstant.INVOICE_ID_BASED_FLASHID_QUERY);
							invIdQuery.setParameter(
									DBConstant.JOB_MODEL_FLASH_DETAIL_ID, flashId);

							invoiceOrderList = invIdQuery.list();
							Query  jobIdQuery = session.createSQLQuery(MDSqlConstant.FETCH_JOBID_FROM_FLASH_QUERY);
							jobIdQuery.setParameter(DBConstant.FLASH_ID, flashId);
							
							List<?> jobIdList = jobIdQuery.list();
							if(null != jobIdList && jobIdList.size() > 0){
								jobId = Integer.parseInt(jobIdList.get(0).toString());
							}
							
							Query flashIdsQuery = session.createSQLQuery(MDSqlConstant.FLASH_ID_BASED_JOBID_QUERY);
							flashIdsQuery.setParameter(DBConstant.JOB_ID, jobId);
							
							List<Integer> jobFlashIdList = flashIdsQuery.list();
							if( null == jobFlashIdList && jobFlashIdList.size() <= 0){
								throw new MDBusinessException(MDProperties.read(MDMessageConstant.NO_JOB_MODEL_LIST));
							}
							
							int jobFLash = 0;
							for(int j = 0; j < jobFlashIdList.size() ; j++){
								jobFLash = jobFlashIdList.get(j);
								
								consumedMac = consumedMac + ((Long)session.createQuery(MDSqlConstant.COUNT_MAC_USING_FLASH_ID_QUERY)
										.setParameter(DBConstant.FLASH_ID,jobFLash).uniqueResult()).intValue();
							}
						
						if (null == invoiceOrderList || invoiceOrderList.size() < 1) {
							logger.warn(MDProperties
									.read(MDMessageConstant.NO_JOB_CREATED_EXCEPTION));
						} else {
							invId = Integer.valueOf(invoiceOrderList.get(0)
									.toString());
						}

					} 
					}else if (key == DBConstant.BOARD_MAC_ADDRESS) {
						invIdQuery = session
								.createSQLQuery(MDSqlConstant.INVOICE_ID_BASED_BOARDMACADDRESS_QUERY);
						invIdQuery
								.setParameter(DBConstant.BOARD_MAC_ADDRESS, value);

						invoiceOrderList = invIdQuery.list();
						if (null == invoiceOrderList || invoiceOrderList.size() < 1) {
							logger.warn(MDProperties
									.read(MDMessageConstant.NO_JOB_CREATED_EXCEPTION));
						} else {
							invId = Integer.valueOf(invoiceOrderList.get(0)
									.toString());
						}
						
						Query flashIdQuery = session
								.createSQLQuery(MDSqlConstant.FLASH_ID_BASED_BOARD_MAC_ADDRESS_QUERY);  
						flashIdQuery.setParameter(DBConstant.BOARD_MAC_ADDRESS, value);

						List<Integer> flashIdQueryList = flashIdQuery.list();

						if (null != flashIdQueryList) {
							int flashId = Integer.valueOf(flashIdQueryList.get(0)
									.toString());
							
							invIdQuery = session
									.createSQLQuery(MDSqlConstant.INVOICE_ID_BASED_FLASHID_QUERY);
							invIdQuery.setParameter(
									DBConstant.JOB_MODEL_FLASH_DETAIL_ID, flashId);

							invoiceOrderList = invIdQuery.list();
							Query  jobIdQuery = session.createSQLQuery(MDSqlConstant.FETCH_JOBID_FROM_FLASH_QUERY);
							jobIdQuery.setParameter(DBConstant.FLASH_ID, flashId);
							
							List<?> jobIdList = jobIdQuery.list();
							if(null != jobIdList && jobIdList.size() > 0){
								jobId = Integer.parseInt(jobIdList.get(0).toString());
							}
							
							Query flashIdsQuery = session.createSQLQuery(MDSqlConstant.FLASH_ID_BASED_JOBID_QUERY);
							flashIdsQuery.setParameter(DBConstant.JOB_ID, jobId);
							
							List<Integer> jobFlashIdList = flashIdsQuery.list();
							if( null == jobFlashIdList && jobFlashIdList.size() <= 0){
								throw new MDBusinessException(MDProperties.read(MDMessageConstant.NO_JOB_MODEL_LIST));
							}
							
							int jobFLash = 0;
							for(int j = 0; j < jobFlashIdList.size() ; j++){
								jobFLash = jobFlashIdList.get(j);
								
								consumedMac = consumedMac + ((Long)session.createQuery(MDSqlConstant.COUNT_MAC_USING_FLASH_ID_QUERY)
										.setParameter(DBConstant.FLASH_ID,jobFLash).uniqueResult()).intValue();
							}
						
						if (null == invoiceOrderList || invoiceOrderList.size() < 1) {
							logger.warn(MDProperties
									.read(MDMessageConstant.NO_JOB_CREATED_EXCEPTION));
						} else {
							invId = Integer.valueOf(invoiceOrderList.get(0)
									.toString());
						}
					} 
				}
				}

				totalInvoiceItemQuery = session
						.createQuery(MDSqlConstant.INVOICE_ITEM_QUERY);
				totalInvoiceItemQuery.setParameter(DBConstant.INVOICE_ID, invId);

				List<InvoiceItem> totalInvoiceItemList = totalInvoiceItemQuery
						.list();

				for (InvoiceItem invoiceItem : totalInvoiceItemList) {
					int invoiceQuantity = invoiceItem.getQuantity();
					int subItemQunatity = 0;
					Query invoiceItemQuery = session
							.createQuery(MDSqlConstant.INVOICE_ITEM_SUB_PRODUCT_QUERY);
					invoiceItemQuery.setParameter(DBConstant.INVOICE_ITEM_ID,
							invoiceItem.getInvoiceItemId());

					List<InvoiceItemSubProduct> invoiceItemSubProductList = invoiceItemQuery
							.list();

					for (InvoiceItemSubProduct invoiceItemSubProduct : invoiceItemSubProductList) {
						Query quantityQuery = session
								.createQuery(MDSqlConstant.INVOICE_SUB_PRODUCT_QUERY);
						quantityQuery.setParameter(
								DBConstant.INVOICE_ITEM_SUB_PRODUCT_ID,
								invoiceItemSubProduct.getInvoiceItemSubProductId());

						List<InvoiceItemSubProduct> subItemQuantityList = quantityQuery
								.list();
						if (null != subItemQuantityList
								&& subItemQuantityList.size() > 0)
							subItemQunatity = subItemQunatity + subItemQuantityList.get(0)
									.getQuantity();
					}
					
					if(invoiceItem.getPsku().startsWith("MD4")){
						subItemTotalQunatity = subItemQunatity + 7;
					} else if(invoiceItem.getPsku().startsWith("MD6")){
						subItemTotalQunatity = subItemQunatity + 9;
					} else if(invoiceItem.getPsku().startsWith("MD1")){
						subItemTotalQunatity = subItemQunatity + 4;
					}
					
					totalInvoiceItemQuantity += (subItemTotalQunatity * invoiceQuantity);
				}

				macDetailsMetaResultVo = new MacDetailsMetaResultVo();
				macDetailsInvoiceItemVoList = new ArrayList<MacDetailsInvoiceItemVo>();
				Query custQuery = session
						.createQuery(MDSqlConstant.PURCHASE_ORDER_CUST_NAME_QUERY);
				custQuery.setParameter(DBConstant.INVOICE_ID, invId);
				List<?> custList = custQuery.list();
				if (null != custList && custList.size() > 0) {
					custName = (String) custList.get(0);
				}
				macDetailsMetaResultVo.setCustomerName(custName);
				macDetailsMetaResultVo.setInvId(invId);

				if (key == DBConstant.BOARD_MAC_ADDRESS) {

					sqlquery = sqlquery.append(MDMessageConstant.AND_CLAUSE);
					sqlquery = sqlquery.append(DBConstant.JOBFLASH_PREFIX + key
							+ " = " + "'" + value + "'");

					macQuery = session.createSQLQuery(sqlquery.toString());

					macQuery.setParameter(DBConstant.INVOICE_ID, invId);
					macList = macQuery.list();
				} else if (key == DBConstant.MAC_ADDRESS) {
					sqlquery = sqlquery.append(MDMessageConstant.AND_CLAUSE);
					sqlquery = sqlquery.append(DBConstant.MODELMAC_PREFIX + key
							+ " = " + "'" + value + "'");

					macQuery = session.createSQLQuery(sqlquery.toString());
					macQuery.setParameter(DBConstant.INVOICE_ID, invId);
					macList = macQuery.list();
				}

				if (null == macList || macList.size() < 1) {
					throw new MDBusinessException(
							MDProperties
									.read(MDMessageConstant.MAC_ADDRESS_NOT_EXIST));
				}
				
				if (key.equalsIgnoreCase(DBConstant.MAC_ADDRESS)) {
					boardMacQuery = session
							.createSQLQuery(MDSqlConstant.BOARD_MAC_BASED_MAC_QUERY);
					boardMacQuery.setParameter(DBConstant.MAC_ADDRESS, value);
					List<?> boardMacListAsPerMacAddress = boardMacQuery.list();
					if (null != boardMacListAsPerMacAddress
							&& boardMacListAsPerMacAddress.size() > 0) {
						value = boardMacListAsPerMacAddress.get(0);
					}
				}
				boardMacQuery = session
						.createSQLQuery(MDSqlConstant.BOARD_MAC_COUNT);
				boardMacQuery.setParameter(DBConstant.INVOICE_ID, invId); 
				boardMacQuery.setParameter(DBConstant.BOARD_MAC_ADDRESS, value);
				boardMacList = boardMacQuery.list(); 
				
				
				if (null != boardMacList && boardMacList.size() > 0) {

				MacDetailsInvoiceItemVo macDetailsInvoiceItemVo = null;

				if (availableMac == 0) {
					availableMac = (totalInvoiceItemQuantity) - consumedMac;
				} else {
					availableMac = availableMac - consumedMac;
				}

				macDetailsMetaResultVo.setAvailableMac(availableMac);
				macDetailsMetaResultVo.setTotalMac(totalInvoiceItemQuantity);

				Integer addJobModelFlashDetails = new Integer(0);

				// 0= invid,1= psku, 2=App, 3=Vlan,
				for (Object[] mac : macList) {
					if (null != mac) {
						macAddressList = new ArrayList<String>();

						if (jobmodelflashdetailsId != Integer.parseInt(mac[7]
								.toString())) {

							macDetailsInvoiceItemVo = new MacDetailsInvoiceItemVo();
							macDetailsInvoiceItemVo.setPsku(mac[1].toString());
							macDetailsInvoiceItemVo.setInvoiceItemId(Integer
									.parseInt(mac[4].toString()));
							macDetailsInvoiceItemVo.setBoardMac(mac[5].toString());

							if (null != mac[2]) {
								macDetailsInvoiceItemVo.setApp(mac[2].toString());
							} else {
								macDetailsInvoiceItemVo.setApp("");
							}
							if (null != mac[3]) {
								macDetailsInvoiceItemVo.setVlan(mac[3].toString());
							} else {
								macDetailsInvoiceItemVo.setVlan("");
							}

							jobmodelflashdetailsId = Integer.parseInt(mac[7]
									.toString());
							macDetailsInvoiceItemVo.setMacList(macAddressList);

							macDetailsInvoiceItemVo.setVersion(mac[8].toString());
						}
					}
					if (null != mac[9].toString()) {
						macDetailsInvoiceItemVo.getMacList().add(mac[9].toString());
						macDetailsInvoiceItemVo
								.setMacCount((++addJobModelFlashDetails).intValue());
					}
				}
				flashedMacCount = flashedMacCount + consumedMac;
				macDetailsInvoiceItemVoList.add(macDetailsInvoiceItemVo);
				if (null != macList && null != macDetailsMetaResultVo
						&& null != macDetailsInvoiceItemVo) {
					macDetailsMetaResultVo
							.setMacDetailsInvoiceItemList(macDetailsInvoiceItemVoList);
					macDetailsMetaResultVo.setFlashedMacCount(flashedMacCount);
					}
				}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("MacAddressDaoImpl.viewMacDetailsBasedOnCriteria() end");
		return macDetailsMetaResultVo;
	}

	public int getCount(MacRangeVo macRangeVo) throws Exception {

		int count = 0,jobId = 0;
		Query boardMacQuery = null;
		Session session = null;
		Query jobIdQuery = null;
		List<?> boardMacList = null;
		List<?> jobOrderList = null;
		logger.info("MacAddressDaoImpl.getCount() started");
		try{
			session = sessionFactory.openSession();
			jobIdQuery = session.createQuery(MDSqlConstant.FETCH_JOB_ID);
			jobIdQuery.setParameter(DBConstant.INVOICE_ID,
					macRangeVo.getInvId());

			jobOrderList = jobIdQuery.list();
			if (null == jobOrderList || jobOrderList.size() < 1) {
				logger.warn(MDProperties
						.read(MDMessageConstant.NO_JOB_CREATED_EXCEPTION));
			} else {
				jobId = Integer.valueOf(jobOrderList.get(0).toString());

				if (macRangeVo.getSearchFlag().equalsIgnoreCase("M")) {

					Query macQuery = session
							.createSQLQuery(MDSqlConstant.BOARD_MAC_BASED_MAC_QUERY);
					macQuery.setParameter(DBConstant.MAC_ADDRESS,
							macRangeVo.getMacAddress());

					count = macQuery.list().size();

				} else if (macRangeVo.getSearchFlag().equalsIgnoreCase("B")) {

					Query boardMacAddressQuery = session
							.createQuery(MDSqlConstant.JOB_MODEL_FLASH_MAC_ADDRESS_QUERY);
					boardMacAddressQuery.setParameter(
							DBConstant.BOARD_MAC_ADDRESS,
							macRangeVo.getBoardMacAddress());

					count = boardMacAddressQuery.list().size();

				} else {
					boardMacQuery = session
							.createQuery(MDSqlConstant.FETCH_BOARD_MAC_ADDRESS);
					boardMacQuery.setParameter(DBConstant.JOB_ID, jobId);
					boardMacList = boardMacQuery.list();
					count = boardMacList.size();
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("MacAddressDaoImpl.getCount() end");
		return count;
	}
}