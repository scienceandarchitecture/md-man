/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ModelDao.java
 * Comments : TODO 
 * Created  : Dec 2, 2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Dec 2, 2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.List;

import com.meshdynamics.model.Model;

public interface ModelDao {
	
	public List<Model> getModels() throws Exception;

}
