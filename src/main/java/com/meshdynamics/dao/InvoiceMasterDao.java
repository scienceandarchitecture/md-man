/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : InvoiceMasterDao.java
 * Comments : TODO 
 * Created  : 15-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |15-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.List;
import java.util.Map;

import com.meshdynamics.model.InvoiceMaster;

public interface InvoiceMasterDao {
	boolean addInvoice(InvoiceMaster invoiceMaster) throws Exception;

	List<InvoiceMaster> getInvoices() throws Exception;

	InvoiceMaster getInvoiceById(int invId) throws Exception;

	boolean deleteInvoiceById(int invoiceId) throws Exception;

	public int getCount(Map<?, ?> newMapCriteria) throws Exception;
	
	boolean updateInvoiceByID(InvoiceMaster invoiceMaster) throws Exception;
	
	List<InvoiceMaster> getPurchaseOrderList(Map<?, ?> Crieria) throws Exception;
	
	public List<InvoiceMaster> getPurchaseOrderList(Map<?, ?> newMap,int start, int noOfRecords) throws Exception;
	
	List<Object[]> getPurchaseOrderById(int invid) throws Exception;

}
