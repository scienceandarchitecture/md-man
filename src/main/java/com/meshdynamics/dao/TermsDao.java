/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : TermsDao.java
 * Comments : TODO 
 * Created  : 20-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |20-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.List;

import com.meshdynamics.model.Terms;

public interface TermsDao {

	boolean addTerms(Terms terms) throws Exception;

	boolean updateTerms(Terms terms) throws Exception;

	List<Terms> getTermsList() throws Exception;
	
	boolean deleteTermsById(int termId) throws Exception;
}
