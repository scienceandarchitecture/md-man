package com.meshdynamics.dao;

/**
 * @author Sanjay
 *
 */
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.common.Constant;
import com.meshdynamics.common.DBConstant;
import com.meshdynamics.common.LoginCache;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.helper.Helper;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.common.helper.MDSqlConstant;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.model.UserRole;

public class LoginDaoImpl implements LoginDao {

	final Logger logger = Logger.getLogger(LoginDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	UserRoleDao dao;

	@Override
	public UserRole findUserByUserNameAndPassword(String userName,
			String password) throws Exception {
		
		UserRole userRole = null;
		logger.info("LoginDaoImpl.findUserByUserNameAndPassword() started");
		try {
			userRole = dao.getUserById(userName);
			if (null != userRole && (userRole.getUserName()).equals(userName)) {
				if (Helper.encrypt(password).equals(userRole.getPassword())) {
					userRole.setPassword("");
				} else {
					throw new MDBusinessException(
							MDProperties
									.read(MDMessageConstant.PASSWORD_NOT_MATCH));
				}
			} else {
				throw new MDDBException(
						MDProperties.read(MDMessageConstant.USER_NOT_EXIST));
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		logger.info("LoginDaoImpl.findUserByUserNameAndPassword() end");
		return userRole;
	}

	@Override
	public boolean changePassword(String apikey, String currentPassword,
			String newPassword) throws Exception {
		
		boolean flag = false;
		org.hibernate.Session session = null;
		Transaction tx = null;
		String value = null;
		String[] arr = null;
		UserRole userRole = null;
		Map<String, String> map = LoginCache.getInstance();
		logger.info("LoginDaoImpl.changePassword() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			value = map.get(apikey);
			arr = value.split(",");
			userRole = dao.getUserById(arr[0]);
			if (null != userRole) {
				if (Helper.encrypt(currentPassword).equals(
						userRole.getPassword())) {
					userRole.setPassword(Helper.encrypt(newPassword));
					session.update(userRole);
					tx.commit();
					flag = true;
				} else {
					throw new MDBusinessException(
							MDProperties
									.read(MDMessageConstant.INCORRECT_PASSWORD));
				}
			}
		} catch (HibernateException e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("LoginDaoImpl.changePassword() end");
		return flag;
	}

	/*
	 *  LoginDaoImpl.forgotPassword
	 *  @param email
	 *  @return boolean
	 *  comments : TODO
	 */
	@SuppressWarnings({ "boxing", "unchecked" })
	@Override
	public boolean forgotPassword(String userName) throws Exception {

		String email = null;
		boolean flag = false;
		Transaction tx = null;
		UserRole userRole = null;
		Query userQuery = null;
		Boolean flg = null;
		String randomPassword = null;
		String encodedString = null;
		List<UserRole> userRoleList = null;
		org.hibernate.Session dbSession = null;
		
		logger.info("LoginDaoImpl.forgotPassword() started");

		try {

			dbSession = sessionFactory.openSession();
			tx = dbSession.beginTransaction();
			
			Query emailQuery = dbSession.createQuery(MDSqlConstant.FETCH_EMAIL);
			emailQuery.setParameter(DBConstant.USER_NAME, userName);
			
			List<?> emailList = emailQuery.list();
			if(null != emailList && emailList.size() > 0)
				email = (String) emailList.get(0);

			userQuery = dbSession.createQuery(MDSqlConstant.FETCH_USER_QUERY);
			userQuery.setParameter(DBConstant.USER_NAME, userName);

			userRoleList = userQuery.list();

			if (null != userRoleList && userRoleList.size() > 0) {
				userRole = userRoleList.get(0);
			} else {
				throw new MDNoRecordsFoundException(
						MDProperties
								.read(MDMessageConstant.NO_USER_CONFIGURATION));
			}

			// code to generate random string
			randomPassword = Helper.generateRandomString().substring(0, 8);
			byte[] encoded = Base64.getEncoder().encode(
					randomPassword.getBytes());

			encodedString = Base64.getEncoder().encodeToString(encoded);

			flg = sendMail(email, encodedString);

			if (flg) {

				// update Details in DB
				userRole.setUserName(userRole.getUserName());
				userRole.setEmail(userRole.getEmail());
				userRole.setPassword(Helper.encrypt(encodedString));
				userRole.setRoles(userRole.getRoles());
				userRole.setFullName(userRole.getFullName());
				userRole.setViews(userRole.getViews());
				dbSession.saveOrUpdate(userRole);
				tx.commit();
				flag = true;
			}
		} catch (HibernateException he) {
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he),
					he.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != dbSession) {
				dbSession.close();
			}
		}
		logger.info("LoginDaoImpl.forgotPassword() end");
		return flag;
	}

	/*
	 *  LoginDaoImpl.sendMail void
	 *  comments : TODO
	 */
	private boolean sendMail(String email, String randomPassword)
			throws Exception {

		Session session = null;
		boolean flag = false;
		final String username = MDProperties.read(Constant.ADMIN_EMAIL);
		final String password = MDProperties.read(Constant.ADMIN_PASSWORD);

		logger.info("LoginDaoImpl.sendMail() started");

		if ((null != username && username.length() < 1)
				&& (null != password && password.length() < 1)) {
			throw new MDBusinessException(
					MDProperties.read(MDMessageConstant.NO_EMAIL_CONFIGURATION));
		}

		Properties props = new Properties();
		props.put(Constant.SMTP_AUTHENTICATION, MDMessageConstant.TRUE);
		props.put(Constant.SMTP_ENABLE, MDMessageConstant.TRUE);
		props.put(Constant.SMTP_HOST, MDProperties.read(Constant.DOMAIN_NAME));
		props.put(Constant.SMTP_PORT, Constant.SMTP_PORT_NO);
		try {
			session = Session.getInstance(props,
					new javax.mail.Authenticator() {
						@Override
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(username,
									password);
						}
					});

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));// From email Id

			message.setRecipients(Message.RecipientType.TO,// To email Id
					InternetAddress.parse(email));

			// send to mail
			message.setText(MDMessageConstant.LOGIN_MESSAGE
					+ System.lineSeparator()
					+ MDMessageConstant.PASSWORD_GENERATED_MESSAGE
					+ System.lineSeparator() + System.lineSeparator()
					+ randomPassword + System.lineSeparator()
					+ System.lineSeparator() + System.lineSeparator()
					+ MDMessageConstant.SUCCESSFUL_LOGIN_MESSAGE);

			Transport.send(message);
			flag = true;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		logger.info("LoginDaoImpl.sendMail() end");
		return flag;
	}
}
