/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : SubProductDao.java
 * Comments : TODO 
 * Created  : Dec 22, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Dec 22, 2016  | Created                             | Pinki         |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.List;

import com.meshdynamics.model.SubProduct;

public interface SubProductDao {

	/*
	 *  SubProductDao.addSubProduct
	 *  @param subProduct
	 *  @return
	 *  @throws Exception boolean
	 *  comments : TODO
	 */
	boolean addSubProduct(SubProduct subProduct) throws Exception;

	/*
	 *  SubProductDao.getSubProducts
	 *  @return
	 *  @throws Exception List<SubProduct>
	 *  comments : TODO
	 */
	List<SubProduct> getSubProducts() throws Exception;

}
