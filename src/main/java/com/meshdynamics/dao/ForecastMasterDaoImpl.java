/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ForecastMasterDaoImpl.java
 * Comments : TODO 
 * Created  : 30-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |30-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.common.DBConstant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.helper.Helper;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.common.helper.MDSqlConstant;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.model.ForecastItem;
import com.meshdynamics.model.ForecastMaster;

public class ForecastMasterDaoImpl implements ForecastMasterDao {

	private static final Logger logger = Logger
			.getLogger(ForecastMasterDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public boolean addForecastMaster(ForecastMaster forecastMaster)
			throws Exception {

		boolean flg = false;
		Session session = null;
		Transaction tx = null;
		logger.info("ForecastMasterDaoImpl.addForecastMaster() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();

			session.save(forecastMaster);
			for (ForecastItem forecastItem : forecastMaster.getForecastItems()) {
				forecastItem.setForecastMaster(forecastMaster);
				session.save(forecastItem);
			}
			tx.commit();
			flg = true;
		} catch (HibernateException he) {
			if (null != tx)
				tx.rollback();
			he.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(he),
					he.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.info("ForecastMasterDaoImpl.addForecastMaster() end");
		return flg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.dao.ForecastMasterDao#getForecastList()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ForecastMaster> getForecastList() throws Exception {

		Session session = null;
		List<ForecastMaster> forecastMasterList = null;
		logger.info("ForecastMasterDaoImpl.getForecastList() started");
		try {
			session = sessionFactory.openSession();

			forecastMasterList = session.createQuery(MDSqlConstant.FORECAST_LIST_QUERY)
					.list();

			if (null != forecastMasterList && forecastMasterList.size() > 0) {
				return forecastMasterList;
			}

			throw new MDNoRecordsFoundException(MDProperties.
					read(MDMessageConstant.NO_RECORD_EXCEPTION));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("ForecastMasterDaoImpl.getForecastList() end");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.dao.ForecastMasterDao#getForecastById(int)
	 */
	@SuppressWarnings("boxing")
	@Override
	public ForecastMaster getForecastById(int fcastId) throws Exception {

		Query query = null;
		List<?> list = null;
		Session session = null;
		logger.info("ForecastDaoImpl.getForecastById() started");
		try {
			session = sessionFactory.openSession();
			query = session
					.createQuery(MDSqlConstant.FORECAST_ID_QUERY);
			query.setParameter(DBConstant.FORECAST_ID, fcastId);

			list = query.list();
			if (null != list && list.size() > 0)
				return (ForecastMaster) list.get(0);

			throw new MDNoRecordsFoundException(MDProperties.read
					(MDMessageConstant.NO_RECORD_EXCEPTION));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("ForecastDaoImpl.getForecastById() end");
		}
	}

	@SuppressWarnings("boxing")
	@Override
	public boolean deleteForecastByForecastId(int fcastId) throws Exception {

		Query query = null;
		List<?> list = null;
		Session session = null;
		Transaction tx = null;
		logger.info("ForecastDaoImpl.deleteForecastByForecastId() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();

			query = session
					.createQuery(MDSqlConstant.FORECAST_ID_QUERY);
			query.setParameter(DBConstant.FORECAST_ID, fcastId);

			list = query.list();
			if (null != list && list.size() > 0) {
				session.delete(list.get(0));
				tx.commit();
				return true;
			}
			throw new MDNoRecordsFoundException(MDProperties.
					read(MDMessageConstant.NO_RECORD_EXCEPTION));
		} catch (HibernateException he) {
			he.printStackTrace();
			if (null != tx)
				tx.rollback();
			throw new MDDBException(Helper.getExceptionCode(he), he.getCause()
					.getMessage(), he.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
			logger.info("ForecastDaoImpl.deleteForecastByForecastId() end");
		}
	}

	@SuppressWarnings("boxing")
	@Override
	public boolean updateForecast(ForecastMaster forecastMaster)
			throws Exception {

		Query query = null;
		Transaction tx = null;
		boolean flg = false;
		Session session = null;
		logger.info("ForecastMasterDaoImpl.updateForecast() started");
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();

			query = session
					.createQuery(MDSqlConstant.DELETE_FORECAST_QUERY);
			query.setParameter(DBConstant.FORECAST_ID, forecastMaster.getFcastId());
			query.executeUpdate();
			session.update(forecastMaster);
			tx.commit();
			flg = true;
		} catch (Exception e) {
			if (null != tx) {
				tx.rollback();
			}
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.info("ForecastMasterDaoImpl.updateForecast() end");
		return flg;
	}
}