package com.meshdynamics.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.security.cert.CertificateException;
import javax.security.cert.X509Certificate;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.meshdynamics.common.Constant;
import com.meshdynamics.common.DBConstant;
import com.meshdynamics.common.MDMessageConstant;
import com.meshdynamics.common.WSClient;
import com.meshdynamics.common.helper.Helper;
import com.meshdynamics.common.helper.MDProperties;
import com.meshdynamics.common.helper.MDSqlConstant;
import com.meshdynamics.common.helper.MacAddress;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.exceptions.MDDBException;
import com.meshdynamics.exceptions.MDNoRecordsFoundException;
import com.meshdynamics.model.AddressIterator;
import com.meshdynamics.model.FlashDeployModels;
import com.meshdynamics.model.InvoiceItemSubProduct;
import com.meshdynamics.model.JobModelFlashDetails;
import com.meshdynamics.model.ModelMacDetails;
import com.meshdynamics.vo.RequestVo;

public class JobModelflashdetailsDaoImpl implements JobModelflashdetailsDao {

	private static final Logger logger = Logger
			.getLogger(JobModelflashdetailsDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings({ "boxing", "unchecked", "rawtypes" })
	@Override
	public boolean addJobModelFlashDetails(JobModelFlashDetails flashDetails)
			throws Exception {
		
		Session session = null;
		FlashDeployModels flashDeployModels = null;
		List<Integer> invoiceItemList = null;
		List<?> subProduList = null;
		Query invoiceQuery = null;
		Query query = null;
		Query subProductQuery = null;
		Query macQuery = null;
		Query jobAcknowledgeQuery = null;
		ModelMacDetails macDetails = null;
		List<ModelMacDetails> vlanModelMacDetails = null;
		List<ModelMacDetails> appModelMacDetails = null;
		List<String> rModelmacdetails = null;
		List<?> macList = null;
		List<ModelMacDetails> modelMacDetails = null;
		String strMacAddress = null;
		int units = 0, invoiceItemId = 0, i = 0, j = 0, quantity = 0, z = 0, r = 0;
		boolean flag = false;
		Transaction tx = null;
		InvoiceItemSubProduct invSubProduct = null;
		byte app = 2, vlan = 3;
		
		logger.info("JobmodelflashdetailsDaoImpl.addJobModelFlashDetails() started");
		try {
			appModelMacDetails = new ArrayList();
			vlanModelMacDetails = new ArrayList();
			rModelmacdetails = new ArrayList();

			session = sessionFactory.openSession();
			tx = session.beginTransaction();

			jobAcknowledgeQuery = session
					.createQuery(MDSqlConstant.JOB_STATE_FLASH_QUERY);
			jobAcknowledgeQuery.setParameter(DBConstant.JOB_ID, flashDetails
					.getJobMaster().getJobId());

			List<String> jobStateList = jobAcknowledgeQuery.list();

			if (jobStateList.get(0).equals(DBConstant.ACKNNOWLEDGED)) {

				query = session
						.createQuery(MDSqlConstant.JOB_MODEL_FLASH_PSKU_QUERY);
				query.setParameter(DBConstant.PSKU, flashDetails.getProduct()
						.getPsku());
				flashDeployModels = (FlashDeployModels) query.uniqueResult();
				if (null != flashDeployModels) {
					units = flashDeployModels.getAddressesPerUnit();

					// validation of BoardMacAddress
					macQuery = session
							.createQuery(MDSqlConstant.JOB_MODEL_FLASH_MAC_ADDRESS_QUERY);
					macQuery.setParameter(DBConstant.BOARD_MAC_ADDRESS,
							flashDetails.getBoardMacAddress());
					macList = macQuery.list();

					if (null != macList && macList.size() > 0)
						throw new MDBusinessException(
								MDProperties
										.read(MDMessageConstant.GIVEN_MAC_EXCEPTION)
										+ "'"
										+ flashDetails.getBoardMacAddress()
										+ "'"
										+ MDProperties
												.read(MDMessageConstant.ALREADY_USE_EXCEPTION));

					modelMacDetails = generateMacAddress(session, units,
							flashDetails);

					for (r = 0; r < modelMacDetails.size(); r++) {
						String s = modelMacDetails.get(r).getMacAddress();
						rModelmacdetails.add(s);
					}

					invoiceQuery = session
							.createSQLQuery(MDSqlConstant.INVOICEITEM_FROM_JOBITEM_QUERY);
					invoiceQuery.setParameter(DBConstant.JOB_ITEM_ID,
							flashDetails.getJobItem().getJobItemId());

					invoiceItemList = invoiceQuery.list();

					for (j = 0; j < invoiceItemList.size(); j++) {

						invoiceItemId = invoiceItemList.get(j);

						subProductQuery = session
								.createQuery(MDSqlConstant.INVOICE_ITEM_SUB_PRODUCT_QUERY);
						subProductQuery = subProductQuery.setParameter(
								DBConstant.INVOICE_ITEM_ID, invoiceItemId);

						subProduList = subProductQuery.list();

						for (i = 0; i < subProduList.size(); i++) {

							invSubProduct = (InvoiceItemSubProduct) subProduList
									.get(i);
							quantity = invSubProduct.getQuantity();

							for (z = 0; z < quantity; z++) {

								strMacAddress = getNextMacAddress();

								if (i == 0) {
									// flashDetails.setMacAddress(strMacAddress);
									flashDetails.setMacAddress(modelMacDetails
											.get(0).getMacAddress());
									flashDetails
											.setStatus(Constant.STATUS_REGENERATE_BUILD_VALUE);
									flashDetails
											.setImageType(Integer
													.valueOf(Constant.IMAGE_TYPE_VALUE));
									session.save(flashDetails);

								}
								if (flashDetails.getBoardMacAddress().length() == 17) {
									macDetails = new ModelMacDetails();
									macDetails
											.setJobmodelflashdetails(flashDetails);
									macDetails.setMacAddress(strMacAddress);

									if (invSubProduct
											.getSubProduct()
											.getSubProductName()
											.equalsIgnoreCase(
													Constant.APP_FLASH)) {
										macDetails.setMacType(app);
									} else {
										macDetails.setMacType(vlan);
									}

									session.save(macDetails);
									if (invSubProduct
											.getSubProduct()
											.getSubProductName()
											.equalsIgnoreCase(
													Constant.APP_FLASH)) {
										appModelMacDetails.add(macDetails);
									} else {
										vlanModelMacDetails.add(macDetails);
									}
								}
							}
						}
					}

					createRequestVo(flashDeployModels, flashDetails,
							rModelmacdetails, vlanModelMacDetails,
							appModelMacDetails, session);
					tx.commit();

					flag = true;

				} else {
					throw new MDNoRecordsFoundException(
							MDProperties
									.read(MDMessageConstant.NO_RECORD_EXCEPTION));
				}
			} else {
				throw new MDBusinessException(MDProperties.read(MDMessageConstant.JOB_STATE_FLASH_STATUS));
			}
		}
		catch (HibernateException hbe) {
			throw new MDDBException(Helper.getExceptionCode(hbe), hbe.getMessage(),
					hbe.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("JobmodelflashdetailsDaoImpl.addJobModelFlashDetails() end");
		return flag;
	}

	/*
	 * JobModelflashdetailsDaoImpl.generateMacAddress
	 * 
	 * @param session
	 * 
	 * @param units void comments : TODO
	 */
	private List<ModelMacDetails> generateMacAddress(Session session,
			int units, JobModelFlashDetails flashDetails) throws Exception {

		
		String strMacAddress = null;
		byte radio = 1;
		ModelMacDetails macDetails = null;
		List<ModelMacDetails> modelMacDetailsList = new ArrayList<ModelMacDetails>();
		
		logger.info("JobmodelflashdetailsDaoImpl.generateMacAddress() started");

		for (int i = 0; i < units; i++) {
			strMacAddress = getNextMacAddress();
			
				if (i == 0) {
					flashDetails.setMacAddress(strMacAddress);
					flashDetails.setImageType(Integer.valueOf(Constant.IMAGE_TYPE_VALUE));
					session.save(flashDetails);
				}
				if (flashDetails.getBoardMacAddress().length() == 17) {
					macDetails = new ModelMacDetails();
					macDetails.setJobmodelflashdetails(flashDetails);
					macDetails.setMacAddress(strMacAddress);
					macDetails.setMacType(radio);
					session.save(macDetails);
					modelMacDetailsList.add(macDetails);
				} else {
					throw new MDBusinessException(
							MDProperties.read(MDMessageConstant.LIMIT_MAC_ADDRESS_EXCEPTION));
				}
		}
		logger.info("JobmodelflashdetailsDaoImpl.generateMacAddress() end");
		return modelMacDetailsList;
	}

	@Override
	public boolean updateJobModelFlashDetails(JobModelFlashDetails flashDetails)
			throws Exception {
		
		Session session = null;
		boolean flag = false;
		logger.info("JobmodelflashdetailsDaoImpl.updateJobModelFlashDetails() started");
		try {
			session = sessionFactory.openSession();
			session.beginTransaction();
			session.update(flashDetails);
			session.flush();
			session.getTransaction().commit();
			flag = true;
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(e), e.getMessage(),
					e.getCause());
		} catch (Exception e1) {
			e1.printStackTrace();
			throw e1;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("JobmodelflashdetailsDaoImpl.updateJobModelFlashDetails() end");
		return flag;
	}

	@Override
	public JobModelFlashDetails getjobModelFlashDetailsById(
			int jobmodelflashdetailsId) throws Exception {
		
		Session session = null;
		JobModelFlashDetails flashDetails = null;
		logger.info("JobmodelflashdetailsDaoImpl.getjobModelFlashDetailsById() started");
		try {
			session = sessionFactory.openSession();
			flashDetails = (JobModelFlashDetails) session.get(
					JobModelFlashDetails.class,
					Integer.valueOf(jobmodelflashdetailsId));
			if (null != flashDetails)
				return flashDetails;
			throw new MDNoRecordsFoundException(MDProperties.
					read(MDMessageConstant.NO_RECORD_EXCEPTION));
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(e), e.getMessage(),
					e.getCause());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			if (null != session) {
				session.close();
			}
			logger.info("JobmodelflashdetailsDaoImpl.getjobModelFlashDetailsById() end");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobModelFlashDetails> getjobModelFlashDetailsByJobIdAndModelId(
			int jobId, int jobItemId) throws Exception {
		
		Query query = null;
		Session session = null;
		List<JobModelFlashDetails> details = null;
		logger.info("JobmodelflashdetailsDaoImpl.getjobModelFlashDetailsByJobIdAndModelId() started");
		try {
			session = sessionFactory.openSession();
			query = session
					.createQuery(MDSqlConstant.JOBID_JOBITEM_ID_FLASH_QUERY);
			query.setParameter(DBConstant.JOB_ID, Integer.valueOf(jobId));
			query.setParameter(DBConstant.JOB_ITEM_ID, Integer.valueOf(jobItemId));
			details = query.list();
			if (null != details && !details.isEmpty())
				updateStatusBasedOnTime(details);
			else	
			throw new MDBusinessException(
					MDMessageConstant.NO_JOB_MODEL_LIST);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session){
				session.close();
			}
		}
		logger.info("JobmodelflashdetailsDaoImpl.getjobModelFlashDetailsByJobIdAndModelId() end");
		return details;
	}
	private void updateStatusBasedOnTime(List<JobModelFlashDetails> flashDetailsList) throws Exception {
		
		for(JobModelFlashDetails  flashDetails : flashDetailsList){
		    	final String status = flashDetails.getStatus();
		    	Date lastUpdatedTime = flashDetails.getLastUpdatedTime();
	    	    Date currentDateTime = new Date();
		    	
		    	switch(status){
		    	
		    	 case Constant.STATUS_INPROGRESS_JOBDETAILS_VALUE:
		    		final long inProgIntrvl = Long.parseLong(MDProperties.read(Constant.FLASHING_STATUS_INPROGRESS));
		    		final boolean isInprogTimeExcds = Helper.getTimeDiffFlagInMinutes(lastUpdatedTime,currentDateTime,inProgIntrvl);
		    		if(isInprogTimeExcds)
		    			flashDetails.setStatus(Constant.STATUS_REGENERATE_BUILD_VALUE);//update status to 1(regenerate build)
		    		    flashDetails.setLastUpdatedTime(currentDateTime);//update with current date-time
		    			this.updateJobModelFlashDetails(flashDetails);
		    		
		    		break;
		    		
		    	 case Constant.STATUS_COMPLETE_VALUE:
		    		final long cmpltIntrvl = Long.parseLong(MDProperties.read(Constant.FLASHING_STATUS_COMPLETE));
		    		final boolean isCmpltTimeExcds = Helper.getTimeDiffFlagInMinutes(lastUpdatedTime,currentDateTime,cmpltIntrvl);
		    		if(isCmpltTimeExcds)
		    			flashDetails.setStatus(Constant.STATUS_REGENERATE_BUILD_VALUE);//update status to 1(regenerate build)
	    		        flashDetails.setLastUpdatedTime(currentDateTime);//update with current date-time
	    		        this.updateJobModelFlashDetails(flashDetails);

		    		break;
		    		
		    	 default:
		    	    logger.warn(MDMessageConstant.STATUS_INVALID+status);
		    	    break;
		    	
		    	}
		    }
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobModelFlashDetails> getjobModelFlashDetailsList()
			throws Exception {
	
		Session session = null;
		List<JobModelFlashDetails> details = null;
		logger.info("JobmodelflashdetailsDaoImpl.getjobModelFlashDetailsList() started");
		try {
			session = sessionFactory.openSession();
			details = session.createQuery(MDSqlConstant.JOB_MODEL_FLASH_LIST_QUERY).list();
			if (null != details && details.size() > 0)
				return details;
			throw new MDBusinessException(
					MDProperties.read(MDMessageConstant.NO_JOB_MODEL_LIST));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
			logger.info("JobmodelflashdetailsDaoImpl.getjobModelFlashDetailsList() end");
		}
	}

	@SuppressWarnings({ "unused", "boxing" })
	private static void createRequestVo(FlashDeployModels deployModels,
			JobModelFlashDetails jobModelFlashDetails,
			List<String> rModelmacdetails,
			List<ModelMacDetails> vlanMacDetailsList,
			List<ModelMacDetails> appMacDetailsList, Session session)
			throws Exception {

		byte app = 1, vlan = 2;
		byte[] mac = null;
		String imageURL = null, macAddressDigest = null;
		String uniqueId = null, signedKey = null;
		String result = null;
		ResponseEntity<JSONObject> responseResult = null;
		String downloadLink = null;
		javax.websocket.Session webSession = null;
		String clientMacAddress=null;
		RequestVo requestVo = null;
		ArrayList<String> rMac = null;
		ArrayList<String> vMac = null;
		ArrayList<String> appMac = null;
		Gson gson = null;
		String jsonInString = null;
		RestTemplate restTemplate = null;
		String generateBuildURL = null;
		
		logger.info("JobmodelflashdetailsDaoImpl.createRequestVo() start");
		
		try {
			clientMacAddress = WSClient.keyMap.get(Constant.CLIENT_MAC)
					.toString();
			requestVo = new RequestVo();
			if (deployModels.getConfigFileName().startsWith(
					DBConstant.CONFIG_FILE_NAME)) {
				requestVo.setTarget(DBConstant.TARGET_CNS);
			} else {
				requestVo.setTarget(DBConstant.TARGET_IMX);
			}

			requestVo.setClientMacAddress(clientMacAddress);
			requestVo.setBoardMacAddress(jobModelFlashDetails
					.getBoardMacAddress());
			requestVo.setModelConf(deployModels.getConfigFileName());
			requestVo.setVersion(jobModelFlashDetails.getVersion());
			requestVo
					.setImageType(jobModelFlashDetails.getImageType() != null ? jobModelFlashDetails
							.getImageType().toString()
							: Constant.IMAGE_TYPE_VALUE);

			rMac = new ArrayList<>();

			for (String rModelMacDetails : rModelmacdetails) {
				rMac.add(rModelMacDetails);
			}

			requestVo.setrMacAddress(rMac);

			vMac = new ArrayList<>();
			for (ModelMacDetails vlanModelMacDetails : vlanMacDetailsList) {
				vMac.add(vlanModelMacDetails.getMacAddress());
			}
			requestVo.setVlanMacAddress(vMac);

			appMac = new ArrayList<>();
			for (ModelMacDetails appModelMacDetails : appMacDetailsList) {
				appMac.add(appModelMacDetails.getMacAddress());
			}
			requestVo.setAppMacAddress(appMac);

			new WSClient().Connect();
			// converting JAVA object to JSON String
			gson = new Gson();
			jsonInString = gson.toJson(requestVo);

			// calling MD-Build rest API from RestTemplate
			restTemplate = getRestTemplate();

			// prepare header
			final HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);   
			
			// here we will check for webSocket connection
			if(null == WSClient.keyMap.get(Constant.CLIENT_KEY)){
				throw new MDBusinessException(MDProperties.read(MDMessageConstant.WEBSOCKET_CONNECTION_NOT_UP));
			}
			
			headers.set(DBConstant.AUTHENTICATION,
					WSClient.keyMap.get(Constant.CLIENT_KEY).toString());

			// call the generate Build Rest API
			HttpEntity<String> entity = new HttpEntity<String>(jsonInString,
					headers);

			generateBuildURL = MDProperties.read(Constant.GENERATE_BUILD_URL);

			responseResult = restTemplate.exchange(generateBuildURL,
					HttpMethod.POST, entity, JSONObject.class);
			
			JSONObject body = responseResult.getBody();
			imageURL = (String) body.get(DBConstant.RESPONSE_BODY);
			macAddressDigest = (String) body.get(DBConstant.MAC_ADDRESS_DIGIT);
			uniqueId = (String) body.get(DBConstant.UNIQUE_ID);
			signedKey = (String) body.get(DBConstant.SIGNED_KEY);
			result = (String) body.get(DBConstant.RESULT);

			if ((Constant.STATUS_INPROGRESS).equalsIgnoreCase(result)) {
				jobModelFlashDetails
						.setStatus(Constant.STATUS_INPROGRESS_JOBDETAILS_VALUE);
				jobModelFlashDetails.setImageName(imageURL);
			} else if ((Constant.STATUS_SUCCESS).equalsIgnoreCase(result)) {
				jobModelFlashDetails.setStatus(Constant.STATUS_COMPLETE_VALUE);
				jobModelFlashDetails.setImageName(imageURL);
			} else if ((Constant.STATUS_FAILURE)
					.equalsIgnoreCase(Constant.STATUS_FAILURE)) {
				jobModelFlashDetails.setStatus(Constant.STATUS_FAILURE_VALUE);
				jobModelFlashDetails
						.setStatus(Constant.STATUS_REGENERATE_BUILD_VALUE);
			}

			downloadLink = signedKey.concat("," + uniqueId)
					.concat("," + imageURL)
					.concat("," + clientMacAddress.toString());
			jobModelFlashDetails.setDownloadLink(downloadLink);
			jobModelFlashDetails.setLastUpdatedTime(new Date());// update time
			session.saveOrUpdate(jobModelFlashDetails);

		} catch(ResourceAccessException re){
			re.printStackTrace();
			throw re;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		logger.info("JobmodelflashdetailsDaoImpl.createRequestVo() end");
	}

	/*
	 * JobModelflashdetailsDaoImpl.regenerateBuild
	 * 
	 * @param jobModelFlashDetailsVotoModel
	 * 
	 * @return boolean comments : TODO
	 */
	@Override
	@SuppressWarnings({ "boxing", "unchecked", "rawtypes" })
	public boolean regenerateBuild(JobModelFlashDetails jobModelFlashDetails)
			throws Exception {

		Session session = null;
		Transaction tx = null;
		Query flashDeployQuery = null,jobModelFlashquery = null;
		Query appQuery = null,vlanQuery = null,radioQuery = null;
		JobModelFlashDetails jobFlashModelList = null;
		FlashDeployModels flashDeployModelList = null;
		int r = 0;
		byte radio = 1, app = 2, vlan = 3;
		boolean flag = false;
		List<String> rModelmacdetails = null;
		List<ModelMacDetails> vlanModelMacDetails = null;
		List<ModelMacDetails> appModelMacDetails = null;
		List<ModelMacDetails> rModelmacdetailList = null;

		logger.info("jobModelFlashDetailsDaoImpl.regenerateBuild() started");
		
		try {

			session = sessionFactory.openSession();
			tx = session.beginTransaction();

			rModelmacdetails = new ArrayList();
			appModelMacDetails = new ArrayList();
			vlanModelMacDetails = new ArrayList();

			jobModelFlashquery = session
					.createQuery(MDSqlConstant.PSKU_MAC_FLASH_QUERY);
			jobModelFlashquery.setParameter(DBConstant.PSKU,
					jobModelFlashDetails.getProduct().getPsku());
			jobModelFlashquery.setParameter(DBConstant.BOARD_MAC_ADDRESS,
					jobModelFlashDetails.getBoardMacAddress());
			jobModelFlashquery.setParameter(
					DBConstant.JOB_MODEL_FLASH_DETAIL_ID,
					jobModelFlashDetails.getJobmodelflashdetailsId());

			jobFlashModelList = (JobModelFlashDetails) jobModelFlashquery
					.uniqueResult();

			appQuery = session.createQuery(MDSqlConstant.MAC_TYPE_QUERY);
			appQuery.setParameter(DBConstant.MAC_TYPE, app);
			appQuery.setParameter(DBConstant.JOB_MODEL_FLASH_DETAIL_ID,
					jobModelFlashDetails.getJobmodelflashdetailsId());
			appModelMacDetails = appQuery.list();

			vlanQuery = session.createQuery(MDSqlConstant.MAC_TYPE_QUERY);
			vlanQuery.setParameter(DBConstant.MAC_TYPE, vlan);
			vlanQuery.setParameter(DBConstant.JOB_MODEL_FLASH_DETAIL_ID,
					jobModelFlashDetails.getJobmodelflashdetailsId());
			vlanModelMacDetails = vlanQuery.list();

			radioQuery = session.createQuery(MDSqlConstant.MAC_TYPE_QUERY);
			radioQuery.setParameter(DBConstant.MAC_TYPE, radio);
			radioQuery.setParameter(DBConstant.JOB_MODEL_FLASH_DETAIL_ID,
					jobModelFlashDetails.getJobmodelflashdetailsId());
			rModelmacdetailList = radioQuery.list();

			for (r = 0; r < rModelmacdetailList.size(); r++) {
				String s = rModelmacdetailList.get(r).getMacAddress();
				rModelmacdetails.add(s);
			}

			flashDeployQuery = session
					.createQuery(MDSqlConstant.JOB_MODEL_FLASH_PSKU_QUERY);
			flashDeployQuery.setParameter(DBConstant.PSKU, jobModelFlashDetails
					.getProduct().getPsku());
			flashDeployModelList = (FlashDeployModels) flashDeployQuery
					.uniqueResult();

			createRequestVo(flashDeployModelList, jobFlashModelList,
					rModelmacdetails, vlanModelMacDetails, appModelMacDetails,
					session);

			tx.commit();
			flag = true;

		} catch (HibernateException e) {
			e.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(e), e.getMessage(),
					e.getCause());
		} catch (Exception e1) {
			e1.printStackTrace();
			throw e1;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("jobModelFlashDetailsDaoImpl.regenerateBuild() started");
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public boolean updateJobModelFlashDetailsByImageName(String imageName ) throws Exception{
		
		Query query = null;
		Session session = null;
		JobModelFlashDetails flashDetails = null;
		List<JobModelFlashDetails> details = null;
		boolean flag=false;
		logger.info("JobmodelflashdetailsDaoImpl.updateJobModelFlashDetailsByImageName() started");
		try {
			session = sessionFactory.openSession();
			query = session.createQuery(MDSqlConstant.IMAGE_NAME_FLASH_QUERY);
			query.setParameter(DBConstant.IMAGE_NAME, imageName);
			details = query.list();
			if (null != details && details.size() > 0) {
				flashDetails = details.get(0);
				flashDetails.setStatus(Constant.STATUS_COMPLETE_VALUE);
				session.beginTransaction();
				session.update(flashDetails);
				session.flush();
				session.getTransaction().commit();
				flag = true;
			} else
				throw new MDBusinessException(
						MDProperties
								.read(MDMessageConstant.NO_JOB_FOR_IMAGE_EXCEPTION)
								+ imageName);

		} catch (HibernateException e) {
			e.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(e), e.getMessage(),
					e.getCause());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("JobmodelflashdetailsDaoImpl.updateJobModelFlashDetailsByImageName() end");
		return flag;
	}
	

	public boolean verifyMacAddress(String boardMacAddress) throws Exception {

		Query macQuery = null;
		Session session = null;
		boolean flag = false;
		List<?> macList = null;
		logger.info("JobmodelflashdetailsDaoImpl.verifyMacAddress() started");
		try {
			session = sessionFactory.openSession();
			macQuery = session
					.createQuery(MDSqlConstant.JOB_MODEL_FLASH_MAC_ADDRESS_QUERY);
			macQuery.setParameter(DBConstant.BOARD_MAC_ADDRESS, boardMacAddress);
			macList = macQuery.list();

			if (null != macList && macList.size() > 0) {
				throw new MDBusinessException(
						MDProperties
								.read(MDMessageConstant.GIVEN_MAC_EXCEPTION)
								+ "' "
								+ boardMacAddress
								+ " '"
								+ MDProperties
										.read(MDMessageConstant.ALREADY_USE_EXCEPTION));
			} else {
				flag = true;
			}
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new MDDBException(Helper.getExceptionCode(e), e.getMessage(),
					e.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		logger.info("JobmodelflashdetailsDaoImpl.verifyMacAddress() end");
		return flag;
	}
	
	private String getNextMacAddress(){
		
	   Session session1 = null;
	   Transaction tx = null;
	   Query innerQuery = null;
	   AddressIterator iterator = null;
	   MacAddress macAddress = null;
	   String strMacAddress = null,inMacAddress = null;
	   logger.info("JobModelFlashDetailsDaoImpl.getNextAddress() started");
		try {
			session1 = sessionFactory.openSession();
			tx = session1.beginTransaction();
			innerQuery = session1
					.createQuery(MDSqlConstant.ADDRESS_ITERATOR_LIST);
			iterator = (AddressIterator) innerQuery.uniqueResult();
			if (null != iterator) {
				synchronized (this) {
					strMacAddress = iterator.getMacAddress();
					macAddress = new MacAddress(); // getting next sequence of
													// macAddress
					macAddress.setBytes(strMacAddress);
					inMacAddress = MacAddress.getMacAddressbyLongValue(Long
							.parseLong(macAddress.toRMSString(), 16) + 1); 
					iterator.setMacAddress(inMacAddress);
					session1.update(iterator);
					tx.commit();
				}
			}
		} catch (Exception e) {
			logger.error(e);
			throw e;
		} finally {
			if (null != session1) {
				session1.close();
			}
		}
	   logger.info("JobModelFlashDetailsDaoImpl.getNextAddress() end");
	   return strMacAddress;
	}

	@Override
	public String fetchImageDownloadPath() throws Exception {

		Session session = null;
		String downloadPath = null;
		logger.info("JobModelFlashDetailsDaoImpl.fetchImageDownloadPath() started");
		try {
			session = sessionFactory.openSession();
			downloadPath = MDProperties.read(Constant.DOWNLOAD_BUILD_URL);
			return downloadPath;
			
		} catch (Exception e) {
			logger.error(e);
			throw e;
		} finally {
			if (null != session) {
				session.close();
			}
			logger.info("JobModelFlashDetailsDaoImpl.fetchImageDownloadPath() end");
		}
	}
	
	private static RestTemplate getRestTemplate() throws Exception {
	    return new RestTemplate(clientHttpRequestFactory());
	}
	private static ClientHttpRequestFactory clientHttpRequestFactory() throws Exception {
		
			return new HttpComponentsClientHttpRequestFactory(getHttpsClient());    
	}

	private static HttpClient getHttpsClient() throws Exception {
        SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustSelfSignedStrategy() {
           public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException
           {
             return true;
           }
         }).build();
        CloseableHttpClient httpClient =HttpClients.custom().setSSLContext(sslContext).setSSLHostnameVerifier(new NoopHostnameVerifier())
                .build(); 
       
       return httpClient;
   }
}
