package com.meshdynamics.service;

import java.util.List;

import com.meshdynamics.vo.MacAddressSummaryVo;
import com.meshdynamics.vo.MacDetailsMetaResultVo;
import com.meshdynamics.vo.MacRangeVo;

public interface MacAddressService {
	
	public List<MacAddressSummaryVo> getMacAddressSummaryList() throws Exception;
	
	public MacDetailsMetaResultVo fetchMacRangeDetails(MacRangeVo macRangeVo,int start, int noOfRecords) throws Exception;
	
	List<MacAddressSummaryVo> getMacAddressListUsingPagination(String criteria,
			int start, int noOfRecords) throws Exception;

	List<MacAddressSummaryVo> getMacAddressListBasedSearchCritera(
			String criteria, int start, int noOfRecords) throws Exception;

}
