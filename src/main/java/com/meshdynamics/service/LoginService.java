package com.meshdynamics.service;

import com.meshdynamics.vo.UserRoleVo;

/**
 * @author Sanjay
 *
 */
public interface LoginService {

	public UserRoleVo findUserByUserNameAndPassword(String userName, String password) throws Exception;
	
	boolean changePassword(String apikey, String currentPassword, String newPassword) throws Exception;
	
	public boolean forgotPassword(String userName) throws Exception;
}
