package com.meshdynamics.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.common.helper.ModelVoHelper;
import com.meshdynamics.dao.TrackingDaoImpl;
import com.meshdynamics.model.InvoiceTracking;
import com.meshdynamics.vo.InvoiceTrackingVo;

public class TrackingServiceImpl implements TrackingService {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.meshdynamics.service.InvoiceMasterService#addInvoice(com.meshdynamics
	 * .model.InvoiceMaster)
	 */
	private static final Logger logger = Logger
			.getLogger(TrackingServiceImpl.class);
	@Autowired
	TrackingDaoImpl trackingDaoImpl;

	/*
	 * public boolean addTracking(InvoiceTracking invoiceTrackingVoToModel)
	 * throws Exception {
	 */
	@Override
	public boolean addTracking(InvoiceTrackingVo invoiceTrackingVo)
			throws Exception {
		logger.info("TrackingServiceImpl.addTracking() started");
		boolean flag = false;
		InvoiceTracking invoiceTracking = ModelVoHelper
				.invoiceTrackingVoToModel(invoiceTrackingVo);
		flag = trackingDaoImpl.addTrackingList(invoiceTracking);
		logger.info("TrackingServiceImpl.addTracking() end");
		return flag;
	}

	@Override
	public List<InvoiceTrackingVo> getTrackingList() throws Exception {

		logger.info("TrackingServiceImpl.getTrackingList() started");
		List<InvoiceTrackingVo> invoiceTrackingVoList = new ArrayList<>();
		List<InvoiceTracking> invoiceTrackingList = trackingDaoImpl
				.getTrackingLists();
		for (InvoiceTracking invoiceTracking : invoiceTrackingList) {
			invoiceTrackingVoList.add(ModelVoHelper
					.invoiceTrackingModelToVo(invoiceTracking));
		}
		logger.info("TrackingServiceImpl.getTrackingList() end");
		return invoiceTrackingVoList;
	}

	@Override
	public InvoiceTrackingVo getTrackingtById(int trackId)
			throws Exception {

		logger.info("TrackingServiceImpl.getTrackListByTrackId() started");
		InvoiceTracking invoiceTracking = trackingDaoImpl
				.getTrackListByTrackId(trackId);
		InvoiceTrackingVo invoiceTrackingVo = ModelVoHelper
				.invoiceTrackingModelToVo(invoiceTracking);
		logger.info("TrackingServiceImpl.getTrackListByTrackId() end");
		return invoiceTrackingVo;
	}

	@Override
	public Map<String, Integer> getTrackingCountByInvoiceId(int invId)
			throws Exception {

		logger.info("TrackingServiceImpl.getTrackingCountByInvoiceId() started");
		Map<String, Integer> map = new HashMap<String, Integer>();
		int trackCount = trackingDaoImpl.getTrackCountByInvoiceId(invId);
		map.put("invoiceTrackingCount", trackCount);
		logger.info("TrackingServiceImpl.getTrackingCountByInvoiceId() end");
		return map;
	}

	@Override
	public boolean updateInvoiceTracking(InvoiceTrackingVo invoiceTrackingVo) throws Exception {
		
		logger.info("TrackingServiceImpl.updateInvoiceTrackList() started");
		InvoiceTracking invoiceTracking = ModelVoHelper.invoiceTrackingVoToModel(invoiceTrackingVo);
		boolean flag = trackingDaoImpl.updateInvoiceTrackList(invoiceTracking);
		logger.info("TrackingServiceImpl.updateInvoiceTrackList() end");
		return flag;
	}

	@Override
	public boolean deleteInvoiceTrackListById(int trackId) throws Exception {
		
		logger.info("InvoiceTrackingListServiceImpl.deleteInvoiceTrackListById() started");
		boolean flag = trackingDaoImpl.deleteInvoiceTrackListById(trackId);
		logger.info("InvoiceTrackingListServiceImpl.deleteInvoiceTrackListById() end");
		return flag;
	}

	@Override
	public List<InvoiceTrackingVo> getInvoiceTrackListByInvId(int invId) throws Exception {
		
		logger.info("InvoiceTrackingListServiceImpl.getInvoiceTrackListByInvId() started");
		List<InvoiceTrackingVo> InvoiceList = new ArrayList<>();
		List<InvoiceTracking> invoiceTrackList = trackingDaoImpl.getInvoiceTrackListByInvId(invId);
		for (InvoiceTracking invoiceTracking : invoiceTrackList) {
			InvoiceList.add(ModelVoHelper.invoiceTrackingModelToVo(invoiceTracking));
		}
		logger.info("InvoiceTrackingListServiceImpl.getInvoiceTrackListByInvId() end");
		return InvoiceList;
	}
}
