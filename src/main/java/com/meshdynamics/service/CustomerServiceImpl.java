/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : CustomerServiceImpl.java
 * Comments : TODO 
 * Created  : Sep 23, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |    Comment                       |     Author    |
 * ---------------------------------------------------------------------------
 * |  0  |Sep 23, 2016   |    Created                    	  |      Pinki	  |  
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.common.helper.ModelVoHelper;
import com.meshdynamics.dao.CustomerDaoImpl;
import com.meshdynamics.model.Customer;
import com.meshdynamics.vo.CustomerVo;

public class CustomerServiceImpl implements CustomerService {

	private static final Logger logger = Logger
			.getLogger(CustomerServiceImpl.class);

	@Autowired
	CustomerDaoImpl customerDaoImpl;

	@Override
	public boolean addCustomers(CustomerVo customerVo) throws Exception {
		logger.info("CustomerServiceImpl.addCustomers() started");
		Customer customer = ModelVoHelper.customerVoToModel(customerVo);
		boolean flg = customerDaoImpl.addCustomer(customer);
		logger.info("CustomerServiceImpl.addCustomers() end");
		return flg;
	}

	@Override
	public List<CustomerVo> getCustomerList() throws Exception {
		logger.info("customerServiceImpl.getCustomerList() started");
		List<Customer> customerList = customerDaoImpl.getCustomerList();
		List<CustomerVo> customerVoList = new ArrayList<>();
		for (Customer customer : customerList) {
			CustomerVo customerVo = ModelVoHelper.customerModelToVo(customer);
			customerVoList.add(customerVo);
		}
		logger.info("customerServiceImpl.getCustomerList() end");
		return customerVoList;
	}

	/*
	 * CustomerServiceImpl.updateCustomerDetails
	 * 
	 * @param customerVo
	 * 
	 * @return boolean comments : TODO
	 */

	public boolean updateCustomerDetails(CustomerVo customerVo)
			throws Exception {

		logger.info("customerServiceImpl.updateCustomerDetails() started");
		Customer customer = ModelVoHelper.customerVoToModel(customerVo);
		boolean flag = customerDaoImpl.updateCustomerDetails(customer);
		logger.info("customerServiceImpl.updateCustomerDetails() end");
		return flag;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.service.CustomerService#deleteCustomer(int)
	 */
	@Override
	public boolean deleteCustomer(int custId) throws Exception {

		logger.info("customerServiceImpl.deleteCustomer() started");

		boolean flag = customerDaoImpl.deleteCustomerById(custId);

		logger.info("customerServiceImpl.deleteCustomer() end");
		return flag;
	}
	
	@Override
	public CustomerVo getCustomerById(int custId) throws Exception {
		logger.info("customerServiceImpl.getCustomerById() started");
		Customer customer = customerDaoImpl.getCustomerById(custId);
		CustomerVo customerVo = ModelVoHelper.customerModelToVo(customer);
		
		logger.info("customerServiceImpl.getCustomerById() end");
		return customerVo;
	}

	@Override
	public List<CustomerVo> getCustomerByName(String name) throws Exception {
		logger.info("customerServiceImpl.getCustomerByName() started");
		List<Customer> customerList = customerDaoImpl.getCustomerByName(name);
		List<CustomerVo> customerVoList = new ArrayList<>();
		for (Customer customer : customerList) {
			CustomerVo customerVo = ModelVoHelper.customerModelToVo(customer);
			customerVoList.add(customerVo);
		}
		logger.info("customerServiceImpl.getCustomerByName() end");
		return customerVoList;
	}
}
