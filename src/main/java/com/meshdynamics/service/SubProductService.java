/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : SubProductService.java
 * Comments : TODO 
 * Created  : Dec 22, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Dec 22, 2016  | Created                             | Pinki         |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.List;

import com.meshdynamics.vo.SubProductVo;

public interface SubProductService {

	boolean addSubProduct(SubProductVo subProductVo) throws Exception;
	List<SubProductVo> getSubProducts() throws Exception;
}
