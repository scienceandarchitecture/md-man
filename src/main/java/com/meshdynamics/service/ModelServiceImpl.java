/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ModelServiceImpl.java
 * Comments : TODO 
 * Created  : Dec 2, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Dec 2, 2016  | Created                             | Pinki		  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.common.helper.ModelVoHelper;
import com.meshdynamics.dao.ModelDaoImpl;
import com.meshdynamics.model.Model;
import com.meshdynamics.vo.ModelVo;

public class ModelServiceImpl implements ModelService {

	private static final Logger logger = Logger
			.getLogger(ModelServiceImpl.class);

	@Autowired
	ModelDaoImpl modelDaoImpl;

	/*
	 * ModelServiceImpl.getModels
	 * 
	 * @return List<ModelVo> comments : TODO
	 */
	@Override
	public List<ModelVo> getModelList() throws Exception {
		logger.info("ModelServiceImpl.getModelList() started");
		List<Model> modelList = modelDaoImpl.getModels();
		List<ModelVo> modelVoList = new ArrayList<>();
		for (Model model : modelList) {
			ModelVo modelVo = ModelVoHelper.modelModelToVo(model);
			modelVoList.add(modelVo);
		}
		logger.info("ModelServiceImpl.getModelList() end");
		return modelVoList;
	}
}
