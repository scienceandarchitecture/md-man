/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ForecastMasterService.java
 * Comments : TODO 
 * Created  : 30-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |30-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.List;

import com.meshdynamics.vo.ForecastMasterVo;

public interface ForecastMasterService {
	boolean addForecastMaster(ForecastMasterVo forecastMasterVo)
			throws Exception;

	List<ForecastMasterVo> getForecastList() throws Exception;

	ForecastMasterVo getForecastById(int fcastId) throws Exception;	

	boolean deleteForecastByForecastID(int fcastId) throws Exception;
	
	boolean updateForecast(ForecastMasterVo forecastMasterVo) throws Exception;
}
