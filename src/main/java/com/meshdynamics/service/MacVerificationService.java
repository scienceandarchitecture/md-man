package com.meshdynamics.service;

import com.meshdynamics.vo.RegisterMacVO;

public interface MacVerificationService {

	public boolean macVerification(RegisterMacVO registerMacVO) throws Exception;

	public boolean macVerificationInMDMan(RegisterMacVO registerMacVO) throws Exception;
}
