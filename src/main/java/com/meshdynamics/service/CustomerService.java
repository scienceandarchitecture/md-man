/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : CustomerService.java
 * Comments : TODO 
 * Created  : Sep 23, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |    Comment                       |     Author    |
 * ---------------------------------------------------------------------------
 * |  0  |Sep 23, 2016   |    Created                    	  |      Pinki	  |  
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.List;

import com.meshdynamics.vo.CustomerVo;


public interface CustomerService {
	
		boolean addCustomers(CustomerVo customerVo) throws Exception;

		List<CustomerVo> getCustomerList() throws Exception;
		
		boolean deleteCustomer(int custId)throws Exception;
		
		public CustomerVo getCustomerById(int custId)throws Exception;

		List<CustomerVo> getCustomerByName(String name) throws Exception;

}
