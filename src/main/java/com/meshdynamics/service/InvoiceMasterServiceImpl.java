/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : InvoiceMasterServiceImpl.java
 * Comments : TODO 
 * Created  : 15-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------;--
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |15-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meshdynamics.common.helper.ModelVoHelper;
import com.meshdynamics.dao.InvoiceMasterDaoImpl;
import com.meshdynamics.model.InvoiceMaster;
import com.meshdynamics.vo.InvoiceMasterVo;
import com.meshdynamics.vo.InvoiceVoCast;

public class InvoiceMasterServiceImpl implements InvoiceMasterService {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.meshdynamics.service.InvoiceMasterService#addInvoice(com.meshdynamics
	 * .model.InvoiceMaster)
	 */
	private static final Logger logger = Logger
			.getLogger(InvoiceMasterServiceImpl.class);
	@Autowired
	InvoiceMasterDaoImpl invoiceMasterDaoImpl;

	@Override
	public boolean addInvoice(InvoiceMaster invoiceMaster) throws Exception {
		logger.info("InvoiceMasterServiceImpl.addInvoice() started");
		boolean flg = false;
		flg = invoiceMasterDaoImpl.addInvoice(invoiceMaster);
		logger.info("InvoiceMasterServiceImpl.addInvoice() end");
		return flg;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.service.InvoiceMasterService#getInvoices(int)
	 */
	public List<InvoiceMasterVo> getInvoices() throws Exception {
		logger.info("InvoiceMasterServiceImpl.getInvoices() started");
		List<InvoiceMasterVo> invoiceMasterVoList = new ArrayList<>();
		List<InvoiceMaster> invoiceMasterList = invoiceMasterDaoImpl
				.getInvoices();

		for (InvoiceMaster invoiceMaster : invoiceMasterList) {
			invoiceMasterVoList.add(ModelVoHelper
					.invoiceMasterModelToVo(invoiceMaster));
		}
		logger.info("InvoiceMasterServiceImpl.getInvoices() end");
		return invoiceMasterVoList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.service.InvoiceMasterService#getInvoiceById(int)
	 */
	@Override
	public InvoiceMasterVo getInvoiceById(int invoiceId) throws Exception {
		logger.info("InvoiceMasterServiceImpl.getInvoiceById() started");
		InvoiceMaster invoiceMaster = invoiceMasterDaoImpl
				.getInvoiceById(invoiceId);

		InvoiceMasterVo invoiceMasterVo = ModelVoHelper
				.invoiceMasterModelToVo(invoiceMaster);

		logger.info("InvoiceMasterServiceImpl.getInvoiceById() end");
		return invoiceMasterVo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.service.InvoiceMasterService#deleteInvoiceById(int)
	 */
	@Override
	public boolean deleteInvoiceById(int invoiceId) throws Exception {
		logger.info("InvoiceMasterServiceImpl.deleteInvoiceById() started");
		boolean flg = invoiceMasterDaoImpl.deleteInvoiceById(invoiceId);
		logger.info("InvoiceMasterServiceImpl.deleteInvoiceById() end");
		return flg;
	}

	/*
	 * InvoiceMasterServiceImpl.updateInvoiceById
	 * 
	 * @param invId
	 * 
	 * @return boolean comments : TODO
	 */
	@Override
	public boolean updateInvoice(InvoiceMasterVo invoiceMasterVo)
			throws Exception {

		logger.info("InvoiceMasterServiceImpl.updateInvoice() started");
		
		InvoiceMaster invoiceMaster = ModelVoHelper
				.invoiceMasterVoToModel(invoiceMasterVo);
		boolean flag = invoiceMasterDaoImpl.updateInvoiceByID(invoiceMaster);
		
		logger.info("InvoiceMasterServiceImpl.updateInvoice() end");
		return flag;
	}

	/*
	 *  InvoiceMasterServiceImpl.getPurchaseOrderList
	 *  @param criteria
	 *  @return List<InvoiceMasterVo>
	 *  comments : TODO
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public List<InvoiceMasterVo> getPurchaseOrderList(String criteria) throws Exception {

		logger.info("InvoiceMasterServiceImpl.getPurchaseOrderList() started");

		Object ob = new ObjectMapper().readValue(criteria, Object.class);

		Map<?, ?> newMap = (Map) ob;

		List<InvoiceMasterVo> invoiceMasterVoList = new ArrayList<>();
		List<InvoiceMaster> invoiceMasterList = invoiceMasterDaoImpl
				.getPurchaseOrderList(newMap);
		for (InvoiceMaster invoiceMaster1 : invoiceMasterList) {
			invoiceMasterVoList.add(ModelVoHelper
					.invoiceMasterModelToVo(invoiceMaster1));
		}

		logger.info("InvoiceMasterServiceImpl.getPurchaseOrderList() ended");
		return invoiceMasterVoList;
	}

	@Override
	public List<InvoiceMasterVo> getPurchaseOrderList(String criteria,int start, int noOfRecords) throws Exception{
		
		logger.info("InvoiceMasterServiceImpl.getPurchaseOrderList() started");

		Object ob = new ObjectMapper().readValue(criteria, Object.class);

		Map<?, ?> newMap = (Map) ob;

		List<InvoiceMasterVo> invoiceMasterVoList = new ArrayList<>();
		List<InvoiceMaster> invoiceMasterList = invoiceMasterDaoImpl
				.getPurchaseOrderList(newMap,start,noOfRecords);
		for (InvoiceMaster invoiceMaster1 : invoiceMasterList) {
			invoiceMasterVoList.add(ModelVoHelper
					.invoiceMasterModelToVo(invoiceMaster1));
		}

		logger.info("InvoiceMasterServiceImpl.getPurchaseOrderList() ended");
		return invoiceMasterVoList;
	}

	@Override
	public int getCount(String criteria) throws Exception {
		logger.info("InvoiceMasterServiceImpl.getCount() started");

		Object ob = new ObjectMapper().readValue(criteria, Object.class);
		Map<?, ?> newMapCriteria = (Map) ob;
		int count = invoiceMasterDaoImpl.getCount(newMapCriteria);
		
		logger.info("InvoiceMasterServiceImpl.getCount() ended");
		return count;
	}
	

	@Override
	public List<?> getPurchaseOrderById(int invid) throws Exception {

		logger.info("InvoiceMasterServiceImpl.getPurchaseOrderById() started");
		List<InvoiceVoCast> invoiceMasterVoList = new ArrayList<>();
		List<Object[]> invoiceMasterList = invoiceMasterDaoImpl
				.getPurchaseOrderById(invid);
		for (Object[] object : invoiceMasterList) {
			InvoiceVoCast InvoiceVoCast = castToClassObject(object);
			invoiceMasterVoList.add(InvoiceVoCast);
		}

		logger.info("InvoiceMasterServiceImpl.getPurchaseOrderById() end");
		return invoiceMasterVoList;
	}

	private InvoiceVoCast castToClassObject(Object[] object) {
		
		InvoiceVoCast invoiceVoCast = new InvoiceVoCast();
		int invId = (int) object[0];
		String poNumber = (String) object[1];
		int custId = (int) object[2];
		invoiceVoCast.setInvId(invId);
		invoiceVoCast.setPoNumber(poNumber);
		invoiceVoCast.setCustId(custId);
		return invoiceVoCast;
	}
}
