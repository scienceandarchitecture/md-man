package com.meshdynamics.service;

import java.util.List;

import com.meshdynamics.vo.JobModelFlashDetailsVo;

public interface JobModelFlashDetailsService {

	public boolean addJobModelFlashDetails(JobModelFlashDetailsVo flashDetailsVo) throws Exception;

	public boolean updateJobModelFlashDetails(JobModelFlashDetailsVo flashDetailsVo) throws Exception;

	public JobModelFlashDetailsVo getjobModelFlashDetailsById(int jobmodelflashdetailsId) throws Exception;

	public List<JobModelFlashDetailsVo> getjobModelFlashDetailsByJobIdAndModelId(int jobId, int jobItemId) throws Exception;
	
	public List<JobModelFlashDetailsVo> getjobModelFlashDetailsList() throws Exception;
	
	public boolean addJobModelFlashDetailList(String jobModelFlashList) throws Exception;
	
	public boolean regenerateBuild(JobModelFlashDetailsVo flashDetailsVo) throws Exception;
	
	public boolean updateJobModelFlashDetailsByImageName(String imageName ) throws Exception;
	
	boolean verifyMacAddress(String boardMacAddress) throws Exception;
	
	public String fetchImageDownloadPath() throws Exception;

}
