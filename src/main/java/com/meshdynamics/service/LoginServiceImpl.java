package com.meshdynamics.service;

/**
 * @author Sanjay
 *
 */
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.common.helper.ModelVoHelper;
import com.meshdynamics.dao.LoginDaoImpl;
import com.meshdynamics.model.UserRole;
import com.meshdynamics.vo.UserRoleVo;

public class LoginServiceImpl implements LoginService {
	final Logger logger = Logger.getLogger(LoginServiceImpl.class);
	
	@Autowired
	LoginDaoImpl loginDaoImpl;
	
	@Override
	public UserRoleVo findUserByUserNameAndPassword(String userName, String password) throws Exception {
		logger.info("LoginServiceImpl.findUserByUserNameAndPassword() started");
		UserRole userRole = loginDaoImpl.findUserByUserNameAndPassword(userName, password);
		UserRoleVo roleVo = ModelVoHelper.userRoleToUserRoleVo(userRole);
		logger.info("LoginServiceImpl.findUserByUserNameAndPassword() end");
		return roleVo;
	}

	@Override
	public boolean changePassword(String apikey, String currentPassword, String newPassword) throws Exception {
		logger.info("LoginServiceImpl.checkCurrentPassword() started");
		boolean flag = loginDaoImpl.changePassword(apikey,currentPassword,newPassword);
		logger.info("LoginServiceImpl.checkCurrentPassword() end");
		return flag;
	}

	@Override
	public boolean forgotPassword(String userName) throws Exception {
		logger.info("LoginserviceImpl.forgotPassword() started");
		boolean flag = loginDaoImpl.forgotPassword(userName);
		logger.info("LoginserviceImpl.forgotPassword() end");
		return flag;
	}
}
