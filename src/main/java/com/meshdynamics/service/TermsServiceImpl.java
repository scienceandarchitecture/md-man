/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : TermsServiceImpl.java
 * Comments : TODO 
 * Created  : 20-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |20-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.common.helper.ModelVoHelper;
import com.meshdynamics.dao.TermsDaoImpl;
import com.meshdynamics.model.Terms;
import com.meshdynamics.vo.TermsVo;

public class TermsServiceImpl implements TermsService {

	private static final Logger logger = Logger
			.getLogger(TermsServiceImpl.class);
	@Autowired
	TermsDaoImpl termsDaoImpl;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.meshdynamics.service.TermsService#addTerms(com.meshdynamics.vo.TermsVo
	 * )
	 */
	@Override
	public boolean addTerms(TermsVo termsVo) throws Exception {
		logger.info("TermsServiceImpl.addTerms() started ");
		Terms terms = ModelVoHelper.termsVoToModel(termsVo);
		boolean flg = termsDaoImpl.addTerms(terms);
		logger.info("TermsServiceImpl.addTerms() end");
		return flg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.service.TermsService#getTermsList()
	 */
	@Override
	public List<TermsVo> getTermsList() throws Exception {
		logger.info("TermsServiceImpl.getTermsList() started");
		List<TermsVo> termsVoList = new ArrayList<>();
		List<Terms> termsList = termsDaoImpl.getTermsList();
		for (Terms terms : termsList) {
			termsVoList.add(ModelVoHelper.termsModelToVo(terms));
		}
		logger.info("TermsServiceImpl.getTermsList() end");
		return termsVoList;
	}

	/*
	 * TermsServiceImpl.deleteTerm
	 * 
	 * @param termId void comments : TODO
	 */
	@Override
	public boolean deleteTerm(int termId) throws Exception {

		logger.info("TermsServiceImpl.deleteTerm() started");
		boolean flag = termsDaoImpl.deleteTermsById(termId);
		logger.info("TermsServiceImpl.deleteTerm() end");
		return flag;
	}

	/*
	 * TermsServiceImpl.updateTerm
	 * 
	 * @param termsVo
	 * 
	 * @return boolean comments : TODO
	 */

	public boolean updateTerm(TermsVo termsVo) throws Exception {

		logger.info("TermsServiceImpl.updateTerm() started");
		Terms terms = ModelVoHelper.termsVoToModel(termsVo);
		boolean flag = termsDaoImpl.updateTerm(terms);
		logger.info("TermsServiceImpl.updateTerm() end");
		return flag;
	}
}
