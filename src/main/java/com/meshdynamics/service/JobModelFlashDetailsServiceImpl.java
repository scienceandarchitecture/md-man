package com.meshdynamics.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meshdynamics.common.ResponseEntity;
import com.meshdynamics.common.helper.ModelVoHelper;
import com.meshdynamics.dao.JobModelflashdetailsDaoImpl;
import com.meshdynamics.model.JobModelFlashDetails;
import com.meshdynamics.vo.JobModelFlashDetailsVo;

public class JobModelFlashDetailsServiceImpl implements JobModelFlashDetailsService {

	private static final Logger logger = Logger.getLogger(JobModelFlashDetailsServiceImpl.class);

	@Autowired
	JobModelflashdetailsDaoImpl jobModelFlashDetailsDaoImpl;

	@Override
	public boolean addJobModelFlashDetails(JobModelFlashDetailsVo flashDetailsVo) throws Exception {
		logger.info("JobModelFlashDetailsServiceImpl.addJobModelFlashDetails() started");
		boolean flag = jobModelFlashDetailsDaoImpl
				.addJobModelFlashDetails(ModelVoHelper.jobModelFlashDetailsVotoModel(flashDetailsVo));
		logger.info("JobModelFlashDetailsServiceImpl.addJobModelFlashDetails() end");
		return flag;
	}

	@Override
	public boolean updateJobModelFlashDetails(JobModelFlashDetailsVo flashDetailsVo) throws Exception {
		logger.info("JobModelFlashDetailsServiceImpl.updateJobModelFlashDetails() started");
		boolean flag = jobModelFlashDetailsDaoImpl
				.updateJobModelFlashDetails(ModelVoHelper.jobModelFlashDetailsVotoModel(flashDetailsVo));
		logger.info("JobModelFlashDetailsServiceImpl.updateJobModelFlashDetails() end");
		return flag;
	}

	@Override
	public JobModelFlashDetailsVo getjobModelFlashDetailsById(int jobmodelflashdetailsId) throws Exception {
		logger.info("JobModelFlashDetailsServiceImpl.getjobModelFlashDetailsById() started");
		JobModelFlashDetailsVo detailsVo = ModelVoHelper.jobModelFlashDetailsToVO(
				jobModelFlashDetailsDaoImpl.getjobModelFlashDetailsById(jobmodelflashdetailsId));
		logger.info("JobModelFlashDetailsServiceImpl.getjobModelFlashDetailsById() end");
		return detailsVo;
	}

	@Override
	public List<JobModelFlashDetailsVo> getjobModelFlashDetailsByJobIdAndModelId(int jobId, int jobItemId)
			throws Exception {
		logger.info("JobModelFlashDetailsServiceImpl.getjobModelFlashDetailsByJobIdAndModelId() started");
		List<JobModelFlashDetailsVo> detailsVos = jobModelFlashDetailsDaoImpl
				.getjobModelFlashDetailsByJobIdAndModelId(jobId, jobItemId).stream()
				.map(ModelVoHelper::jobModelFlashDetailsToVO).collect(Collectors.toList());
		logger.info("JobModelFlashDetailsServiceImpl.getjobModelFlashDetailsByJobIdAndModelId() end");
		return detailsVos;
	}

	@Override
	public List<JobModelFlashDetailsVo> getjobModelFlashDetailsList() throws Exception {
		logger.info("JobModelFlashDetailsServiceImpl.getjobModelFlashDetailsByJobIdAndModelId() started");
		List<JobModelFlashDetailsVo> detailsVos = jobModelFlashDetailsDaoImpl.getjobModelFlashDetailsList().stream()
				.map(ModelVoHelper::jobModelFlashDetailsToVO).collect(Collectors.toList());
		logger.info("JobModelFlashDetailsServiceImpl.getjobModelFlashDetailsByJobIdAndModelId() end");
		return detailsVos;
	}
	
	/*
	 *  JobModelFlashDetailsServiceImpl.regenerateBuild
	 *  @param flashDetailsVo
	 *  @return boolean
	 *  comments : TODO
	 */
	@Override
	public boolean regenerateBuild(JobModelFlashDetailsVo flashDetailsVo) throws Exception {
		logger.info("JobModelFlashDetailsServiceImpl.regenerateBuild() started");
		boolean flag = jobModelFlashDetailsDaoImpl.regenerateBuild(ModelVoHelper.jobModelFlashDetailsVotoModel(flashDetailsVo));
		logger.info("JobModelFlashDetailsServiceImpl.regenerateBuild() end");
		return flag;
	}
	
	/*
	 *  JobModelFlashDetailsServiceImpl.addJobModelFlashDetailList
	 *  @param jobModelFlashList
	 *  @return boolean
	 *  comments : TODO
	 */
	@Override
	public boolean addJobModelFlashDetailList(String jobModelFlashList)
			throws Exception {

		logger.info("JobModelFlashDetailsServiceImpl.getjobModelFlashDetailsByJobIdAndModelId() started");

		String jsonInString = null;
		int jobId = 0,jobModelFlashId = 0;
		JSONParser parser = new JSONParser();
		JSONArray jsonarray = new JSONArray();
		ResponseEntity response = new ResponseEntity();

		JSONObject jsonObject = (JSONObject) parser.parse(jobModelFlashList);

		jsonarray = (JSONArray) jsonObject.get("abc");

		for (Object object : jsonarray) {
			// JSONObject obj = (JSONObject) object;

			ObjectMapper mapper = new ObjectMapper();
			
			jsonInString = mapper.writeValueAsString(object);
			
			JobModelFlashDetailsVo jobModelflashdetailsVo = mapper.readValue(
					jsonInString, JobModelFlashDetailsVo.class);

			JobModelFlashDetails jobModelflashDetails = ModelVoHelper
					.jobModelFlashDetailsVotoModel(jobModelflashdetailsVo);
			jobModelFlashId = jobModelflashDetails.getJobmodelflashdetailsId();
			jobId = jobModelflashDetails.getJobMaster().getJobId();

			boolean flag = jobModelFlashDetailsDaoImpl
					.addJobModelFlashDetails(jobModelflashDetails);
			if(flag == true){
				response.setMessage("jobFlashing successful for jobId = "+jobId+" & jobFlashId = "+jobModelFlashId);
			}
			else
			{
				response.setMessage("jobFlashing Failed for jobId = "+jobId+" & jobFlashId = "+jobModelFlashId);
			}
		}
		logger.info("JobModelFlashDetailsServiceImpl.getjobModelFlashDetailsByJobIdAndModelId() end");
		return true;
	}

	@Override
	public boolean updateJobModelFlashDetailsByImageName(String imageName ) throws Exception {
		logger.info("JobModelFlashDetailsServiceImpl.updateJobModelFlashDetails() started");
		boolean flag = jobModelFlashDetailsDaoImpl.updateJobModelFlashDetailsByImageName(imageName);
		logger.info("JobModelFlashDetailsServiceImpl.updateJobModelFlashDetails() end");
		return flag;
	}

	@Override
	public boolean verifyMacAddress(String boardMacAddress) throws Exception{
		logger.info("JobModelFlashDetailsServiceImpl.verifyMacAddress() started");
		boolean flag = jobModelFlashDetailsDaoImpl.verifyMacAddress(boardMacAddress);
		logger.info("JobModelFlashDetailsServiceImpl.verifyMacAddress() started");
		return flag;
	}

	@Override
	public String fetchImageDownloadPath() throws Exception {
		logger.info("JobModelFlashDetailsServiceImpl.fetchImageDownloadPath() started");
		String downloadPath = jobModelFlashDetailsDaoImpl.fetchImageDownloadPath();
		logger.info("JobModelFlashDetailsServiceImpl.fetchImageDownloadPath() end");
		return downloadPath;
	}
}
