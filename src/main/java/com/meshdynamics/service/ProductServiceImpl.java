/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ProductServiceImpl.java
 * Comments : TODO 
 * Created  : Sep 28, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |    Comment                       |     Author    |
 * ---------------------------------------------------------------------------
 * |  0  |Sep 28, 2016 |    Created                    	  |      Pinki	  |  
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meshdynamics.common.helper.ModelVoHelper;
import com.meshdynamics.dao.ProductDaoImpl;
import com.meshdynamics.model.Product;
import com.meshdynamics.vo.ProductVo;
import com.meshdynamics.vo.ProductVoCast;

public class ProductServiceImpl implements ProductService {

	private static final Logger logger = Logger
			.getLogger(InvoiceMasterServiceImpl.class);

	@Autowired
	ProductDaoImpl productDaoImpl;

	@Override
	public boolean addProduct(ProductVo productVo) throws Exception {

		logger.info("ProductServiceImpl.addProduct() started");
		boolean flg = false;
		Product product = ModelVoHelper.productVoToModel(productVo);
		flg = productDaoImpl.addProduct(product);
		logger.info("ProductServiceImpl.addProduct() end");
		return flg;
	}

	@Override
	public List<ProductVo> getProducts() throws Exception {
		logger.info("ProductServiceImpl.getProducts started");
		List<ProductVo> productVoList = new ArrayList<>();
		List<Product> productList = productDaoImpl.getProducts();
		for (Product product : productList) {
			productVoList.add(ModelVoHelper.productModelToVo(product));
		}
		logger.info("ProductServiceImpl.getProducts() end");
		return productVoList;
	}

	@Override
	public boolean deleteProduct(String psku) throws Exception {

		logger.info("ProductServiceImpl.deleteProduct() started");

		boolean flag = productDaoImpl.deleteProductById(psku);

		logger.info("ProductServiceImpl.deleteCustomer() end");
		return flag;
	}

	/*
	 * ProductServiceImpl.getProductById
	 * 
	 * @param psku
	 * 
	 * @return ProductVo comments : TODO
	 */
	@Override
	public ProductVo getProductById(String psku) throws Exception {

		logger.info("ProductServiceImpl.getProductById() started");

		Product product = productDaoImpl.getProductById(psku);

		ProductVo productVo = ModelVoHelper.productModelToVo(product);

		logger.info("ProductServiceImpl.getProductById() end");
		return productVo;
	}

	/*
	 * ProductServiceImpl.updateProduct
	 * 
	 * @param productVo
	 * 
	 * @return boolean comments : TODO
	 */
	@Override
	public boolean updateProduct(ProductVo productVo) throws Exception {

		logger.info("ProductServiceImpl.updateProduct() started");
		Product product = ModelVoHelper.productVoToModel(productVo);
		boolean flag = productDaoImpl.updateProduct(product);
		logger.info("ProductServiceImpl.updateProduct() end");
		return flag;
	}

	@Override
	public List<ProductVo> getProductListUsingPagination(String criteria, int start,
			int noOfRecords) throws Exception {

		logger.info("ProductServiceImpl.getProductListUsingPagination() started");
		
		Object ob = new ObjectMapper().readValue(criteria, Object.class);

		Map<?, ?> citeria = (Map) ob;
		List<ProductVo> productVoList = new ArrayList<>();
		List<Product> productList = productDaoImpl
				.getProductListUsingPagination(citeria,start, noOfRecords);
		for (Product product : productList) {
			productVoList.add(ModelVoHelper.productModelToVo(product));
		}
		logger.info("ProductServiceImpl.getProductListUsingPagination() end");
		return productVoList;
	}

	@Override
	public List<?> getProductListByProductId(String psku) throws Exception {
		
		logger.info("ProductServiceImpl.getProductListByProductId() started");
		
		List<ProductVoCast> productVoList = new ArrayList<>();
		List<String> productList = productDaoImpl
				.getProductListByProductId(psku);
		for (String psk : productList) {
			ProductVoCast productVoCast = castToClassObject(psk);
			productVoList.add(productVoCast);
		}

		logger.info("ProductServiceImpl.getProductListByProductId() end");
		return productVoList;
	}
	
	private ProductVoCast castToClassObject(String psku) {
		
		logger.info("ProductServiceImpl.castToClassObject() started");
	
		ProductVoCast productVoCast = new ProductVoCast();
		productVoCast.setPsku(psku);
		
		logger.info("ProductServiceImpl.castToClassObject() end");
		return productVoCast;
	}

	public int getCount(String criteria) throws Exception {
		
		logger.info("ProductServiceImpl.getCount() started");
		
		Object ob = new ObjectMapper().readValue(criteria, Object.class);
		Map<?, ?> newMapCriteria = (Map) ob;
		int count = productDaoImpl.getCount(newMapCriteria);
		
		logger.info("ProductServiceImpl.getCount() end");
		return count;
	}
}
