package com.meshdynamics.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meshdynamics.common.helper.ModelVoHelper;
import com.meshdynamics.dao.InventoryDaoImpl;
import com.meshdynamics.model.RawInventory;
import com.meshdynamics.vo.RawInventoryVo;
import com.meshdynamics.vo.RawInventoryVoCast;

public class InventoryServiceImpl implements InventoryService {

	private static final Logger logger = Logger
			.getLogger(InventoryServiceImpl.class);

	@Autowired
	InventoryDaoImpl inventoryDaoImpl;

	@Override
	public boolean addRawInventory(RawInventoryVo rawInventoryVo)
			throws Exception {

		logger.info("InventoryServiceImpl.addRawInventory() started");
		RawInventory rawInventory = ModelVoHelper
				.rawInventoryVoToModel(rawInventoryVo);
		boolean flg = inventoryDaoImpl.addRawInventory(rawInventory);
		logger.info("InventoryServiceImpl.addRawInventory() end");
		return flg;
	}

	@Override
	public boolean updateRawInventory(RawInventoryVo rawInventoryVo)
			throws Exception {

		logger.info("InventoryServiceImpl.updateRawInventory() started");
		RawInventory rawInventory = ModelVoHelper
				.rawInventoryVoToModel(rawInventoryVo);
		boolean flag = inventoryDaoImpl.updateRawInventory(rawInventory);
		logger.info("inventoryserviceImpl.updateRawInventory() ended");
		return flag;
	}

	@Override
	public List<RawInventoryVo> getRawInventoryList() throws Exception {
		logger.info("InventoryServiceImpl.getRawInventoryList() started");
		List<RawInventory> rawInventoryList = inventoryDaoImpl
				.getRawInventoryList();
		List<RawInventoryVo> rawInventoryVoList = new ArrayList<>();
		for (RawInventory rawInventory : rawInventoryList) {
			RawInventoryVo rawInventoryVo = ModelVoHelper
					.rawInventoryModelToVo(rawInventory);
			rawInventoryVoList.add(rawInventoryVo);
		}
		logger.info("InventoryServiceImpl.getRawInventoryList() end");
		return rawInventoryVoList;
	}

	@Override
	public boolean deleteRawInventory(String rsku) throws Exception {

		logger.info("InventoryServiceImpl.deleteRawInventory() started");
		boolean flag = inventoryDaoImpl.deleteRawInventoryById(rsku);
		logger.info("InventoryServiceImpl.deleteRawInventory() end");
		return flag;
	}

	@Override
	public List<RawInventoryVo> getPaginationInventoryList(String criteria, int start,
			int noOfRecords) throws Exception {

		logger.info("InventoryServiceImpl.getPaginationInventoryList() started");
		
		Object ob = new ObjectMapper().readValue(criteria, Object.class);

		Map<?, ?> newMap = (Map) ob;

		List<RawInventoryVo> rawInventoryVoList = new ArrayList<>();
		List<RawInventory> rawInventoryList = inventoryDaoImpl
				.getInventoryPaginationList(newMap,start, noOfRecords);
		for (RawInventory rawInventory : rawInventoryList) {
			rawInventoryVoList.add(ModelVoHelper
					.rawInventoryModelToVo(rawInventory));
		}
		logger.info("InventoryServiceImpl.getPaginationInventoryList() end");
		return rawInventoryVoList;
	}

	@Override
	public List<?> getRawInventoryBySKU(String rsku)
			throws Exception {
		
		logger.info("InventoryServiceImpl.getRawInventoryBySKU() started");
		List<RawInventoryVoCast> rawInventoryVoList = new ArrayList<>();
		List<String> rawInventoryList = inventoryDaoImpl
				.getRawInventoryBySKU(rsku);
		for (String string : rawInventoryList) {
			RawInventoryVoCast rawInventoryVoCast = castToClassObject(string);
			rawInventoryVoList.add(rawInventoryVoCast);
		}

		logger.info("InventoryServiceImpl.getRawInventoryBySKU() end");
		return rawInventoryVoList;
	}

	private RawInventoryVoCast castToClassObject(String string) {
		
		logger.info("InventoryServiceImpl.castToClassObject() started");
	
		RawInventoryVoCast rawInventoryVoCast = new RawInventoryVoCast();
		rawInventoryVoCast.setRsku(string);
		
		logger.info("InventoryServiceImpl.castToClassObject() end");
		return rawInventoryVoCast;
	}

	public int getCount(String criteria) throws Exception {

		logger.info("InventoryServiceImpl.getCount() started");
		
		Object ob = new ObjectMapper().readValue(criteria, Object.class);
		Map<?, ?> mapCriteria = (Map) ob;
		int count = inventoryDaoImpl.getCount(mapCriteria);
		
		logger.info("InventoryServiceImpl.getCount() end");
		return count;
	}
}
