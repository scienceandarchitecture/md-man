/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ProductService.java
 * Comments : TODO 
 * Created  : Sep 28, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |    Comment                       |     Author    |
 * ---------------------------------------------------------------------------
 * |  0  |Sep 28, 2016   |    Created                    	  |      Pinki	  |  
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.List;

import com.meshdynamics.model.Product;
import com.meshdynamics.vo.ProductVo;

public interface ProductService {

	boolean addProduct(ProductVo productVo) throws Exception;

	List<ProductVo> getProducts() throws Exception;

	boolean deleteProduct(String psku) throws Exception;

	public ProductVo getProductById(String psku) throws Exception;
	
	boolean updateProduct(ProductVo productVo) throws Exception;

	List<?> getProductListByProductId(String psku) throws Exception;

	List<ProductVo> getProductListUsingPagination(String criteria, int start,
			int noOfRecords) throws Exception;

}
