/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : UserRoleServiceImpl.java
 * Comments : TODO 
 * Created  : 23-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |23-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meshdynamics.common.LoginCache;
import com.meshdynamics.common.helper.ModelVoHelper;
import com.meshdynamics.dao.UserRoleDaoImpl;
import com.meshdynamics.exceptions.MDBusinessException;
import com.meshdynamics.model.UserRole;
import com.meshdynamics.vo.UserRoleVo;

public class UserRoleServiceImpl implements UserRoleService {

	private static final Logger logger = Logger
			.getLogger(UserRoleServiceImpl.class);

	@Autowired
	UserRoleDaoImpl userRoleDaoImpl;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.meshdynamics.service.UserRoleService#addUserRole(com.meshdynamics
	 * .vo.UserRoleVo)
	 */
	@Override
	public boolean addUserRole(UserRoleVo userRoleVo) throws Exception {
		logger.info("UserRoleServiceImpl.addUserRole() started");
		boolean flg = false;
		UserRole userRole = ModelVoHelper.userRoleVoToUserRole(userRoleVo);
		flg = userRoleDaoImpl.addUserRole(userRole);
		logger.info("UserRoleServiceImpl.addUserRole() end");
		return flg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.service.UserRoleService#getUserRoleList()
	 */
	@Override
	public List<UserRoleVo> getUserRoleList() throws Exception {
		logger.info("UserRoleServiceImpl.getUserRole() started");
		List<UserRole> userRoleList = userRoleDaoImpl.getUserRoleList();
		List<UserRoleVo> userVoList = new ArrayList<>();
		for (UserRole userRole : userRoleList) {
			userVoList.add(ModelVoHelper.userRoleToUserRoleVo(userRole));
		}
		logger.info("UserRoleServiceImpl.getUserRole() end");
		return userVoList;
	}

	/*
	 * UserRoleServiceImpl.updateUserRole
	 * 
	 * @param userRoleVo
	 * 
	 * @return boolean comments : TODO
	 */
	@Override
	public boolean updateUserRole(UserRoleVo userRoleVo) throws Exception {
		logger.info("UserRoleServiceImpl.updateUserRole() started");
		UserRole userRole = ModelVoHelper.userRoleVoToUserRole(userRoleVo);
		boolean flag = userRoleDaoImpl.updateUserRole(userRole);
		logger.info("UserRoleServiceImpl.updateUserRole() end");
		return flag;
	}

	/*
	 * UserRoleServiceImpl.deleteUser
	 * 
	 * @param userName void comments : TODO
	 */
	@Override
	public boolean deleteUserById(String userName, String apikey)
			throws Exception {

		logger.info("UserRoleServiceImpl.deleteUserById() started");
		Map<String, String> map = LoginCache.getInstance();
		if (map != null && map.size() > 0) {
			if (map.containsKey(apikey)) {
				String apikyValue = map.get(apikey);
				String[] arr = apikyValue.split(",");
				String currentLoggedInUser = arr[0];
				if (currentLoggedInUser.equals(userName)) {
					throw new MDBusinessException(
							"This user is currently logged in,You can not delete");
				}
			}
		}
		boolean flag = userRoleDaoImpl.deleteUserById(userName);
		logger.info("UserRoleServiceImpl.deleteUserById() end");
		return flag;
	}

	/*
	 * UserRoleServiceImpl.getUserById
	 * 
	 * @param userName
	 * 
	 * @return UserRole comments : TODO
	 */
	public UserRoleVo getUserById(String userName) throws Exception {

		logger.info("UserRoleServiceImpl.getUserById() started");
		UserRole userRole = userRoleDaoImpl.getUserById(userName);
		UserRoleVo userRoleVo = ModelVoHelper.userRoleToUserRoleVo(userRole);
		logger.info("UserRoleServiceImpl.getUserById() end");
		return userRoleVo;
	}

	/*
	 * UserRoleServiceImpl.changePassword
	 * 
	 * @param apikey
	 * 
	 * @param currentPassword
	 * 
	 * @param newPassword
	 * 
	 * @return boolean comments : TODO
	 */
	@Override
	public boolean changePassword(String apiKey, String currentPassword,
			String newPassword) throws Exception {

		logger.info("UserRoleServiceImpl.checkCurrentPassword() started");
		boolean flag = userRoleDaoImpl.changePassword(apiKey, currentPassword,
				newPassword);
		logger.info("UserRoleServiceImpl.checkCurrentPassword() end");
		return flag;
	}

	@Override
	public List<UserRoleVo> getUserRoleListBasedOnPagination(String criteria,int start,
			int noOfRecords) throws Exception {

		logger.info("UserRoleServiceImpl.getUserRoleListBasedOnPagination() started");
		
		Object ob = new ObjectMapper().readValue(criteria, Object.class);

		Map<?, ?> newMap = (Map) ob;

		List<UserRole> userRoleList = userRoleDaoImpl
				.getUserRoleListBasedOnPagination(newMap, start, noOfRecords);
		List<UserRoleVo> userVoList = new ArrayList<>();
		for (UserRole userRole : userRoleList) {
			userVoList.add(ModelVoHelper.userRoleToUserRoleVo(userRole));
		}
		logger.info("UserRoleServiceImpl.getUserRoleListBasedOnPagination() end");
		return userVoList;
	}

	@SuppressWarnings("rawtypes")
	public int getCount(String criteria) throws Exception {

		logger.info("UserRoleServiceImpl.getCount() started");
		
		Object ob = new ObjectMapper().readValue(criteria, Object.class);
		Map<?, ?> newMapCriteria = (Map) ob;
		int count = userRoleDaoImpl.getCount(newMapCriteria);
		
		logger.info("UserRoleServiceImpl.getCount() end");
		return count;
	}
}
