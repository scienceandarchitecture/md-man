/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ModelService.java
 * Comments : TODO 
 * Created  : Dec 2, 2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Dec 2, 2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.List;

import com.meshdynamics.vo.ModelVo;

public interface ModelService {

	public List<ModelVo> getModelList() throws Exception;
}
