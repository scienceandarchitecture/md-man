/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : TermsService.java
 * Comments : TODO 
 * Created  : 20-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |20-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.List;

import com.meshdynamics.vo.TermsVo;

public interface TermsService {
	public boolean addTerms(TermsVo termsVo) throws Exception;

	boolean updateTerm(TermsVo termsVo) throws Exception;

	List<TermsVo> getTermsList() throws Exception;
	
	boolean deleteTerm(int termId) throws Exception;
}
