package com.meshdynamics.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meshdynamics.dao.MacAddressDaoImpl;
import com.meshdynamics.vo.MacAddressSummaryVo;
import com.meshdynamics.vo.MacDetailsMetaResultVo;
import com.meshdynamics.vo.MacRangeVo;

public class MacAddressServiceImpl implements MacAddressService {
	
	private static final Logger logger = Logger
			.getLogger(MacAddressServiceImpl.class);
	@Autowired
	MacAddressDaoImpl macAddressDaoImpl;
	
	@Override
	public List<MacAddressSummaryVo> getMacAddressSummaryList()
			throws Exception {
		logger.info("MacAddressServiceImpl.getMacAddressSummaryList() started");
		List<MacAddressSummaryVo> macAddressSummaryList = macAddressDaoImpl.getMacAddressSummaryList();
		logger.info("MacAddressServiceImpl.getMacAddressSummaryList() end");
		return macAddressSummaryList;
	}

	/*
	 *  MacAddressServiceImpl.fetchMacRangeDetails
	 *  @param macRangeVo
	 *  @return boolean
	 *  comments : TODO
	 */
	@Override
	public MacDetailsMetaResultVo fetchMacRangeDetails(MacRangeVo macRangeVo, int start, int noOfRecords) throws Exception{
		
		logger.info("MacAddressServiceImpl.fetchMacRangeDetails started"); 
		HashMap<String, String> hmap = new HashMap<String, String>();
		MacDetailsMetaResultVo macAddressSummaryList = null;
		if(macRangeVo.getSearchFlag().equalsIgnoreCase("M"))
		{
			hmap.put("macAddress", macRangeVo.getMacAddress());
			macAddressSummaryList = macAddressDaoImpl.viewMacDetailsBasedOnCriteria(hmap); 
		} else if(macRangeVo.getSearchFlag().equalsIgnoreCase("B")){
			hmap.put("boardMacAddress", macRangeVo.getBoardMacAddress());
			macAddressSummaryList = macAddressDaoImpl.viewMacDetailsBasedOnCriteria(hmap);
		} else {
			macAddressSummaryList = macAddressDaoImpl.fetchMacRangeList(macRangeVo,start,noOfRecords);
		}
		logger.info("MacAddressServiceImpl.fetchMacRangeDetails end");
		return macAddressSummaryList;
	}

	@Override
	public List<MacAddressSummaryVo> getMacAddressListUsingPagination(String criteria, int start,
			int noOfRecords) throws Exception {

		logger.info("MacAddressServiceImpl.getMacAddressListUsingPagination() started");
		
		Object ob = new ObjectMapper().readValue(criteria, Object.class);

		Map<?, ?> newMap = (Map) ob;
		
		List<MacAddressSummaryVo> macAddressSummaryList = macAddressDaoImpl
				.getMacAddressListUsingPagination(newMap, start, noOfRecords);
		logger.info("MacAddressServiceImpl.getMacAddressListUsingPagination() end");
		return macAddressSummaryList;
	}

	@Override
	public List<MacAddressSummaryVo> getMacAddressListBasedSearchCritera(String criteria, int start, int noOfRecords) throws Exception {
		
		logger.info("MacAddressServiceImpl.getMacAddressListByNameAndPoNumber() started");

		Object ob = new ObjectMapper().readValue(criteria, Object.class);

		Map<?, ?> newMap = (Map) ob;

		List<MacAddressSummaryVo> macAddressSummaryList = macAddressDaoImpl
				.getMacAddressListBasedSearchCritera(newMap,start,noOfRecords);

		logger.info("MacAddressServiceImpl.getMacAddressListByNameAndPoNumber() end");
		return macAddressSummaryList;
	}

	public int getCount(String criteria) throws Exception{

		logger.info("MacAddressServiceImpl.getCount() started");

		Object ob = new ObjectMapper().readValue(criteria, Object.class);
		Map<?, ?> newMapCriteria = (Map) ob;
		int count = macAddressDaoImpl.getCount(newMapCriteria);
		
		logger.info("MacAddressServiceImpl.getCount() ended");
		return count;
	}

	public int getCount(MacRangeVo macRangeVo) throws Exception{
		
		logger.info("MacAddressServiceImpl.getCount() started");
		
		int count = macAddressDaoImpl.getCount(macRangeVo);
		
		logger.info("MacAddressServiceImpl.getCount() end");
		return count;
	}
}
