/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : JobMasterService.java
 * Comments : TODO 
 * Created  : 04-Oct-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |04-Oct-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.List;

import com.meshdynamics.vo.JobMasterVo;
import com.meshdynamics.vo.JobOrderVoCast;

public interface JobMasterService {

	boolean addJobOrders(JobMasterVo jobMasterVo) throws Exception;

	List<JobMasterVo> getJobOrders() throws Exception;
	
	public JobMasterVo getJobOrderById(int jobId) throws Exception;
	
	boolean updateJobOrder(JobMasterVo jobMasterVo) throws Exception;
	
	boolean deleteJobOrder(int jobId) throws Exception;
	
	List<JobMasterVo> getJobOrderList(String criteria) throws Exception;

	List<JobMasterVo> getJobOrderList(String criteria, int start,
			int noOfRecords) throws Exception;

	public int getCount(String criteria) throws Exception;

	List<JobOrderVoCast> getJobOrderListById(int jobId) throws Exception;
	
}
