package com.meshdynamics.service;

import java.util.List;
import java.util.Map;

import com.meshdynamics.vo.InvoiceTrackingVo;

public interface TrackingService {

	boolean addTracking(InvoiceTrackingVo invoiceTrackingVo) throws Exception;

	List<InvoiceTrackingVo> getTrackingList() throws Exception;

	public InvoiceTrackingVo getTrackingtById(int trackId)
			throws Exception;
	
	Map<String,Integer> getTrackingCountByInvoiceId(int invId) throws Exception;
	
	boolean updateInvoiceTracking(InvoiceTrackingVo invoiceTrackingVo) throws Exception;
	
	boolean deleteInvoiceTrackListById(int trackId) throws Exception;
	
	List<InvoiceTrackingVo> getInvoiceTrackListByInvId(int invId) throws Exception;

}
