/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : InvoiceMasterService.java
 * Comments : TODO 
 * Created  : 15-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |15-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.List;

import com.meshdynamics.model.InvoiceMaster;
import com.meshdynamics.vo.InvoiceMasterVo;

public interface InvoiceMasterService {
	boolean addInvoice(InvoiceMaster invoiceMaster) throws Exception;

	List<InvoiceMasterVo> getInvoices() throws Exception;

	InvoiceMasterVo getInvoiceById(int invoiceId) throws Exception;

	boolean deleteInvoiceById(int invoiceId) throws Exception;
	
	boolean updateInvoice(InvoiceMasterVo invoiceMasterVo) throws Exception;
	
	List<InvoiceMasterVo> getPurchaseOrderList(String criteria) throws Exception;
	
	public List<InvoiceMasterVo> getPurchaseOrderList(String criteria,int start, int noOfRecords) throws Exception;
	
	public int getCount(String criteria) throws Exception;
	
	List<?> getPurchaseOrderById(int invid) throws Exception;

}
