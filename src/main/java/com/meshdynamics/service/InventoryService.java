package com.meshdynamics.service;

import java.util.List;

import com.meshdynamics.vo.RawInventoryVo;

public interface InventoryService {
	boolean addRawInventory(RawInventoryVo rawInventoryVo) throws Exception;

	boolean updateRawInventory(RawInventoryVo rawInventoryVo) throws Exception;

	boolean deleteRawInventory(String rsku) throws Exception;

	List<RawInventoryVo> getRawInventoryList() throws Exception;
	
	List<?> getRawInventoryBySKU(String rsku) throws Exception;

	List<RawInventoryVo> getPaginationInventoryList(String criteria, int start,
			int noOfRecords) throws Exception;

}
