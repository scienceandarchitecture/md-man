/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ShipServiceImpl.java
 * Comments : TODO 
 * Created  : Oct 5, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |    Comment                       |     Author    |
 * ---------------------------------------------------------------------------
 * |  0  |Oct 5, 2016   |    Created                       |      Pinki	  |  
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meshdynamics.common.helper.ModelVoHelper;
import com.meshdynamics.dao.ShipMasterDaoImpl;
import com.meshdynamics.model.ShipMaster;
import com.meshdynamics.vo.ShipMasterVo;

public class ShipMasterServiceImpl implements ShipMasterService {

	private static final Logger logger = Logger
			.getLogger(ShipMasterServiceImpl.class);
	@Autowired
	ShipMasterDaoImpl shipMasterDaoImpl;

	@Override
	public boolean addShipMaster(ShipMasterVo shipMasterVo) throws Exception {

		logger.info("ShipMasterServiceImpl.addShipMaster() started");
		boolean flg = false;
		ShipMaster shipMaster = ModelVoHelper.shipMasterVoToModel(shipMasterVo);
		flg = shipMasterDaoImpl.addShipItem(shipMaster);
		logger.info("ShipMasterServiceImpl.addShipMaster() end");
		return flg;
	}

	/*
	 * ShipMasterServiceImpl.getShippingItemList
	 * 
	 * @return List<ShipMasterVo> comments : TODO
	 */
	@Override
	public List<ShipMasterVo> getShippingList() throws Exception {
		
		logger.info("ShipMasterServiceImpl.getShippingList() started");
		List<ShipMasterVo> shipMasterVoList = new ArrayList<>();
		List<ShipMaster> shipMasterList = shipMasterDaoImpl.getShipList();
		for (ShipMaster shipMaster : shipMasterList) {
			shipMasterVoList.add(ModelVoHelper.shipMasterModelToVo(shipMaster));
		}
		logger.info("ShipMasterServiceImpl.getShippingList() ended");
		return shipMasterVoList;
	}

	/*
	 * ShipMasterServiceImpl.getShipItemById
	 * 
	 * @param shipId
	 * 
	 * @return ShipMasterVo comments : TODO
	 */
	@Override
	public ShipMasterVo getShipItemById(int shipId) throws Exception {
		logger.info("ShipMasterServiceImpl.getShipItemById() started");
		ShipMaster shipMaster = shipMasterDaoImpl.getShipItemById(shipId);

		ShipMasterVo shipMasterVo = ModelVoHelper
				.shipMasterModelToVo(shipMaster);

		logger.info("ShipMasterServiceImpl.getInvoiceById() end");
		return shipMasterVo;
	}

	/*
	 * ShipMasterServiceImpl.deleteShipMasterById
	 * 
	 * @param shipId
	 * 
	 * @param shipMasterVo void comments : TODO
	 */
	@Override
	public boolean deleteShipMasterById(int shipId) throws Exception {
		logger.info("ShipMasterServiceImpl.deleteShipMasterById() started");
		boolean flag = shipMasterDaoImpl.deleteShipByID(shipId);
		logger.info("ShipMasterServiceImpl.deleteShipMasterById() ends");
		return flag;

	}

	/*
	 * ShipMasterServiceImpl.updateShipItemDetails
	 * 
	 * @param shipMasterVo
	 * 
	 * @return boolean comments : TODO
	 */
	@Override
	public boolean updateShipItemDetails(ShipMasterVo shipMasterVo)
			throws Exception {

		logger.info("ShipMasterServiceImpl.updateShipItemDetails() started");
		ShipMaster shipMaster = ModelVoHelper.shipMasterVoToModel(shipMasterVo);
		boolean flag = shipMasterDaoImpl.updateShipItemDetails(shipMaster);
		logger.info("ShipMasterServiceImpl.updateShipItemDetails() end");
		return flag;
	}
	
	/*
	 * ShipMasterServiceImpl.getShippingListByInvoiceId
	 * 
	 * @return List<ShipMasterVo> comments 
	 */
	@Override
	public List<ShipMasterVo> getShippingListByInvoiceId(int invid) throws Exception {
		
		logger.info("ShipMasterServiceImpl.getShippingListByInvoiceId() started");
		List<ShipMasterVo> shipMasterVoList = new ArrayList<>();
		List<ShipMaster> shipMasterList = shipMasterDaoImpl.getShipListByInvoiceId(invid);
		for (ShipMaster shipMaster : shipMasterList) {
			shipMasterVoList.add(ModelVoHelper.shipMasterModelToVo(shipMaster));
		}
		logger.info("ShipMasterServiceImpl.getShippingListByInvoiceId() end");
		return shipMasterVoList;
	}

	@Override
	public List<ShipMasterVo> getShipPaginationList(String criteria, int start,
			int noOfRecords) throws Exception {
		logger.info("ShipMasterServiceImpl.getShipPaginationList() started");

		Object ob = new ObjectMapper().readValue(criteria, Object.class);

		Map<?, ?> newMapCriteria = (Map) ob;

		List<ShipMasterVo> shipMasterVoList = new ArrayList<ShipMasterVo>();
		List<ShipMaster> shipMasterList = shipMasterDaoImpl
				.getShipPaginationList(newMapCriteria, start, noOfRecords);
		for (ShipMaster shipMaster : shipMasterList) {
			shipMasterVoList.add(ModelVoHelper.shipMasterModelToVo(shipMaster));
		}
		logger.info("ShipMasterServiceImpl.getShipPaginationList() started");
		return shipMasterVoList;
	}

	@Override
	public int getCount(String criteria) throws Exception {
		logger.info("ShipMasterServiceImpl.getCount() started");
		
		Object ob = new ObjectMapper().readValue(criteria, Object.class);
		Map<?, ?> newMapCriteria = (Map) ob;
		int count = shipMasterDaoImpl.getCount(newMapCriteria);
		
		logger.info("ShipMasterServiceImpl.getCount() end");
		return count;
	}
}
