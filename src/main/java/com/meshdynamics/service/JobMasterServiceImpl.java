/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : JobMasterServiceImpl.java
 * Comments : TODO 
 * Created  : 04-Oct-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |04-Oct-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meshdynamics.common.helper.ModelVoHelper;
import com.meshdynamics.dao.JobMasterDaoImpl;
import com.meshdynamics.model.JobMaster;
import com.meshdynamics.vo.JobMasterVo;
import com.meshdynamics.vo.JobOrderVoCast;

public class JobMasterServiceImpl implements JobMasterService {

	private static final Logger LOGGER = Logger
			.getLogger(JobMasterServiceImpl.class);

	@Autowired
	private JobMasterDaoImpl jobMasterDaoImpl;

	/**
	 *
	 * @param jobMasterVo
	 *            JobMasterVo
	 * @return boolean
	 * @throws Exception
	 * @see com.meshdynamics.service.JobMasterService#addJobOrders(JobMasterVo)
	 */
	@Override
	public boolean addJobOrders(JobMasterVo jobMasterVo) throws Exception {
		LOGGER.info("JobMasterServiceImpl.addJobOrders() started");
		JobMaster jobMaster = ModelVoHelper.jobMasterVoToModel(jobMasterVo);
		boolean flg = jobMasterDaoImpl.addJobOrders(jobMaster);
		LOGGER.info("JobMasterServiceImpl.addJobOrders() ends");
		return flg;
	}

	/**
	 *
	 * @param
	 * @return List<JobMasterVo>
	 * @throws Exception
	 * @see com.meshdynamics.service.JobMasterService#getJobOrders()
	 */
	@Override
	public List<JobMasterVo> getJobOrders() throws Exception {

		LOGGER.info("JobMasterServiceImpl.getJobOrders() started");
		List<JobMasterVo> jobMasterVosList = new ArrayList<>();
		List<JobMaster> jobMasterList = jobMasterDaoImpl.getJobOrders();
		for (JobMaster jobMaster : jobMasterList) {
			jobMasterVosList.add(ModelVoHelper.jobMasterModelToVo(jobMaster));
		}
		LOGGER.info("JobMasterServiceImpl.getJobOrders() end");
		return jobMasterVosList;
	}

	/**
	 *
	 * @param jobId
	 * @return JobMasterVo
	 * @throws Exception
	 * @see com.meshdynamics.service.JobMasterService#getJobOrderById(int jobId)
	 */
	@Override
	public JobMasterVo getJobOrderById(int jobId) throws Exception {

		LOGGER.info("JobMasterServiceImpl.getJobOrderById() started");
		JobMaster jobMaster = jobMasterDaoImpl.getJobOrderById(jobId);
		JobMasterVo jobMasterVo = ModelVoHelper.jobMasterModelToVo(jobMaster);
		LOGGER.info("JobMasterServiceImpl.getJobOrderById() end");
		return jobMasterVo;
	}

	/**
	 *
	 * @param jobMasterVo
	 * @return boolean
	 * @throws Exception
	 * @see com.meshdynamics.service.JobMasterService#updateJobOrder(JobMasterVo)
	 */
	@Override
	public boolean updateJobOrder(JobMasterVo jobMasterVo) throws Exception {

		LOGGER.info("JobMasterServiceImpl.updateJobOrder() started");
		JobMaster jobMaster = ModelVoHelper.jobMasterVoToModel(jobMasterVo);
		boolean flag = jobMasterDaoImpl.updateJobOrder(jobMaster);
		LOGGER.info("JobMasterServiceImpl.updateJobOrder() end");
		return flag;
	}

	/**
	 *
	 * @param jobMasterVo
	 * @return boolean
	 * @throws Exception
	 * @see com.meshdynamics.service.JobMasterService#deleteJobOrder(int jobId)
	 */
	@Override
	@Transactional
	public boolean deleteJobOrder(int jobId) throws Exception {

		LOGGER.info("JobMasterServiceImpl.deleteJobOrder() started");

		boolean flag = jobMasterDaoImpl.deleteJobOrderByJobId(jobId);

		LOGGER.info("JobMasterServiceImpl.deleteJobOrder() end");
		return flag;
	}

	/*
	 * JobMasterServiceImpl.getJobOrderList
	 * 
	 * @param criteria
	 * 
	 * @return List<JobMasterVo> comments : TODO
	 */
	@SuppressWarnings({ "rawtypes" })
	public List<JobMasterVo> getJobOrderList(String criteria) throws Exception {

		LOGGER.info("JobMasterServiceImpl.getJobOrderList() started");

		Object ob = new ObjectMapper().readValue(criteria, Object.class);

		Map<?, ?> newMap = (Map) ob;

		List<JobMasterVo> jobMasterVoList = new ArrayList<>();
		List<JobMaster> jobMasterList = jobMasterDaoImpl
				.getJobOrderList(newMap);
		for (JobMaster jobMasters : jobMasterList) {
			jobMasterVoList.add(ModelVoHelper.jobMasterModelToVo(jobMasters));
		}
		LOGGER.info("JobMasterServiceImpl.getJobOrderList() ended");
		return jobMasterVoList;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<JobMasterVo> getJobOrderList(String criteria, int start,int noOfRecords) throws Exception{
		LOGGER.info("JobMasterServiceImpl.getJobOrderList() started");

		Object ob = new ObjectMapper().readValue(criteria, Object.class);

		Map<?, ?> newMap = (Map) ob;

		List<JobMasterVo> jobMasterVoList = new ArrayList<>();
		List<JobMaster> jobMasterList = jobMasterDaoImpl
				.getJobOrderList(newMap,start,noOfRecords);
		for (JobMaster jobMaster : jobMasterList) {
			jobMasterVoList.add(ModelVoHelper
					.jobMasterModelToVo(jobMaster));
		}

		LOGGER.info("JobMasterServiceImpl.getJobOrderList() end");
		return jobMasterVoList;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public int getCount(String criteria) throws Exception {
		LOGGER.info("JobMasterServiceImpl.getCount() started");

		Object ob = new ObjectMapper().readValue(criteria, Object.class);
		Map<?, ?> newMapCriteria = (Map) ob;
		int count = jobMasterDaoImpl.getCount(newMapCriteria);
		
		LOGGER.info("JobMasterServiceImpl.getCount() end");
		return count;
	}

	@Override
	public List<JobOrderVoCast> getJobOrderListById(int jobId) throws Exception {
		LOGGER.info("JobMasterServiceImpl.getJobOrderListById() started");
		
		List<JobOrderVoCast> jobOrderVoList = new ArrayList<>();
		List<Integer> jobOrderList = jobMasterDaoImpl
				.getJobOrderListById(jobId);
		for (Integer integer : jobOrderList) {
			JobOrderVoCast jobOrderVoCast = castToClassObject(integer);
			jobOrderVoList.add(jobOrderVoCast);
		}

		LOGGER.info("JobMasterServiceImpl.getJobOrderListById() end");
		return jobOrderVoList;
	}

	private JobOrderVoCast castToClassObject(Integer integer) {
		
		LOGGER.info("JobMasterServiceImpl.castToClassObject() started");
		JobOrderVoCast jobOrderVoCast = new JobOrderVoCast();
		jobOrderVoCast.setJobId(integer);
		LOGGER.info("JobMasterServiceImpl.castToClassObject() end");
		return jobOrderVoCast;
	}
		
}
