/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ForecastMasterServiceImpl.java
 * Comments : TODO 
 * Created  : 30-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |30-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.common.helper.ModelVoHelper;
import com.meshdynamics.dao.ForecastMasterDaoImpl;
import com.meshdynamics.model.ForecastMaster;
import com.meshdynamics.vo.ForecastMasterVo;

public class ForecastMasterServiceImpl implements ForecastMasterService {

	private static final Logger logger = Logger
			.getLogger(ForecastMasterServiceImpl.class);

	@Autowired
	ForecastMasterDaoImpl forecastMasterDaoImpl;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.meshdynamics.service.ForecastMasterService#addForecastMaster(com.
	 * meshdynamics.model.ForecastMaster)
	 */
	@Override
	public boolean addForecastMaster(ForecastMasterVo forecastMasterVo)
			throws Exception {
		boolean flg = false;
		logger.info("ForecastMasterServiceImpl.addForecastMaster() started");
		ForecastMaster forecastMaster = ModelVoHelper
				.forecastMasterVoToModel(forecastMasterVo);
		flg = forecastMasterDaoImpl.addForecastMaster(forecastMaster);
		logger.info("ForecastMasterServiceImpl.addForecastMaster() end");
		return flg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.service.ForecastMasterService#getForecastList()
	 */
	@Override
	public List<ForecastMasterVo> getForecastList() throws Exception {
		logger.info("ForecastMasterServiceImpl.getForecastList() started");

		List<ForecastMasterVo> forecastMasterVo = new ArrayList<>();
		List<ForecastMaster> forecastMasterList = forecastMasterDaoImpl
				.getForecastList();
		for (ForecastMaster forecastMaster : forecastMasterList) {
			forecastMasterVo.add(ModelVoHelper
					.forecastMasterModeltovo(forecastMaster));
		}
		logger.info("ForecastMasterServiceImpl.getForecastList() end");
		return forecastMasterVo;
	}

	@Override
	public ForecastMasterVo getForecastById(int fcastId) throws Exception {

		logger.info("ForecastMasterServiceImpl.getForecastById() started");
		ForecastMaster forecastMaster = forecastMasterDaoImpl
				.getForecastById(fcastId);

		ForecastMasterVo forecastMasterVo = ModelVoHelper
				.forecastMasterModeltovo(forecastMaster);
		logger.info("ForecastMasterServiceImpl.getForecastById() end");
		return forecastMasterVo;
	}

	@Override
	public boolean deleteForecastByForecastID(int fcastId) throws Exception {

		logger.info("ForecastServiceimpl.deleteForecastById() started");
		boolean flag = forecastMasterDaoImpl
				.deleteForecastByForecastId(fcastId);
		logger.info("ForecastServiceimpl.deleteForecastById() end");
		return flag;
	}

	@Override
	public boolean updateForecast(ForecastMasterVo forecastMasterVo)
			throws Exception {

		logger.info("ForecastServiceimpl.updateForecast() started");
		ForecastMaster forecastMaster = ModelVoHelper
				.forecastMasterVoToModel(forecastMasterVo);
		boolean flag = forecastMasterDaoImpl.updateForecast(forecastMaster);
		logger.info("ForecastServiceimpl.updateForecast() end");
		return flag;
	}
}
