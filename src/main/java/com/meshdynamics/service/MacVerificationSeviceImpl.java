package com.meshdynamics.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.dao.MacVerifactionDao;
import com.meshdynamics.vo.RegisterMacVO;

public class MacVerificationSeviceImpl implements MacVerificationService{

	private static final Logger logger = Logger.getLogger(MacVerificationSeviceImpl.class);
	/*private static final byte rMacType           = 0;
	private static final byte vMacType           = 1; // same macType as flashing one
	private static final byte appMacType         = 2;*/
	
	// changes for the macAddressType
	private static final byte rMacType           = 1;
	private static final byte appMacType         = 2;
	private static final byte vMacType           = 3; 
    
	@Autowired
    MacVerifactionDao macVerificationDao;
	
	@Override
	public boolean macVerification(RegisterMacVO registerMacVO) throws Exception{
	logger.info("macVerification w.r.t registered macs");
	boolean flag=false;
	List<Object[]> list=macVerificationDao.macVerification(registerMacVO.getBoardMacAddress());
	List<String> rMacAddress=registerMacVO.getrMacAddress();
	List<String> vMacAddress=registerMacVO.getVlanMacAddress();
	List<String> appMacAddress=registerMacVO.getAppMacAddress();
	int rcount  =rMacAddress.size();
	int vcount  =vMacAddress.size();
	int appcount=appMacAddress.size();
	if(list != null && list.size()>0){
		if(rMacAddress != null && rMacAddress.size()>0){
			int registerdrCount   = verifyMac(getMacAddress_By_MacType(list,rMacType),rMacAddress);
			if(rcount ==registerdrCount)
				flag=true;
			}
		
		if(vMacAddress != null && vMacAddress.size()>0){
			int registerdvcount   = verifyMac(getMacAddress_By_MacType(list,vMacType),vMacAddress);
			if(vcount ==registerdvcount)
				flag=true;
			}
		if(appMacAddress != null && appMacAddress.size()>0){
			int registerdappcount   = verifyMac(getMacAddress_By_MacType(list,appMacType),appMacAddress);
			if(appcount ==registerdappcount)
				flag=true;
			}

		}
	return flag;
	}
	private int verifyMac(List<String> originalMacList,List<String> macList){
		int count=0;
		for(int i=0;i<macList.size();i++){
			if(originalMacList.contains(macList.get(i)))
				count++;
		}
		return count;
	}
	
	private List<String> getMacAddress_By_MacType(List<Object[]> array,int macType){
		List<String> list=new ArrayList<String>();
		for(int i=0;i<array.size();i++){
			Object[] objectArr=array.get(i);
			if(macType == (byte)objectArr[1]){
				list.add((String) objectArr[0]);
			}			 
		 }
		return list;
		
	}
		
	@Override
	public boolean macVerificationInMDMan(RegisterMacVO registerMacVO)
			throws Exception {

		logger.info("macVerification w.r.t registered macs");
		boolean flag = false;
		List<Object[]> list = macVerificationDao
				.macVerificationInMDMan(registerMacVO.getBoardMacAddress());
		if (null != list) {
			flag = true;
		}
		List<String> rMacAddress = registerMacVO.getrMacAddress();
		List<String> vMacAddress = registerMacVO.getVlanMacAddress();
		List<String> appMacAddress = registerMacVO.getAppMacAddress();
		int rcount = rMacAddress.size();
		int vcount = vMacAddress.size();
		int appcount = appMacAddress.size();
		if (list != null && list.size() > 0) {
			if (rMacAddress != null && rMacAddress.size() > 0) {
				int registerdrCount = verifyMac(
						getMacAddress_By_MacType(list, rMacType), rMacAddress);
				if (rcount == registerdrCount) {
					flag = true;
				} else {
					flag = false;
					return flag;
				}
			}

			if (vMacAddress != null && vMacAddress.size() > 0) {
				int registerdvcount = verifyMac(
						getMacAddress_By_MacType(list, vMacType), vMacAddress);
				if (vcount == registerdvcount) {
					flag = true;
				} else {
					flag = false;
					return flag;
				}
			}

			if (appMacAddress != null && appMacAddress.size() > 0) {
				int registerdappcount = verifyMac(
						getMacAddress_By_MacType(list, appMacType),
						appMacAddress);
				if (appcount == registerdappcount) {
					flag = true;
				} else {
					flag = false;
					return flag;
				}
			}
		}
		return flag;
	}
}
