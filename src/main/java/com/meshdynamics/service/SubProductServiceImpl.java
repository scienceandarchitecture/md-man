/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : SubProductServiceImpl.java
 * Comments : TODO 
 * Created  : Dec 22, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |Dec 22, 2016  | Created                             | Pinki          |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.meshdynamics.common.helper.ModelVoHelper;
import com.meshdynamics.dao.SubProductDaoImpl;
import com.meshdynamics.model.SubProduct;
import com.meshdynamics.vo.SubProductVo;

public class SubProductServiceImpl implements SubProductService  {

	private static final Logger logger = Logger
			.getLogger(SubProductServiceImpl.class);

	@Autowired
	SubProductDaoImpl subProductDaoImpl;
	
	/*
	 *  SubProductServiceImpl.addSubProduct
	 *  @param subProductVoToModel
	 *  @return boolean
	 *  comments : TODO
	 */
	@Override
	public boolean addSubProduct(SubProductVo subProductVo) throws Exception {
		logger.info("SubProductServiceImpl.addSubProduct() started");
		SubProduct subProduct = ModelVoHelper.subProductVoToModel(subProductVo);
		boolean flg = subProductDaoImpl.addSubProduct(subProduct);
		logger.info("SubProductServiceImpl.addSubProduct() end");
		return flg;
	}
	
	/*
	 *  SubProductServiceImpl.getSubProducts
	 *  @return List<SubProductVo>
	 *  comments : TODO
	 */
	@Override
	public List<SubProductVo> getSubProducts() throws Exception {
		
		logger.info("SubProductServiceImpl.getSubProduct() started");
		List<SubProductVo> subProductVoList = new ArrayList<>();
		List<SubProduct> subProductList = subProductDaoImpl.getSubProducts();
		for (SubProduct subProduct : subProductList) {
			subProductVoList.add(ModelVoHelper.subProductModelToVo(subProduct));
		}
		logger.info("SubProductServiceImpl.getSubProduct() end");
		return subProductVoList;
	}
}
