/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ShipService.java
 * Comments : TODO 
 * Created  : Oct 5, 2016
 *
 * 
 * Author   : Pinki
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |    Comment                       |     Author    |
 * ---------------------------------------------------------------------------
 * |  0  |Oct 5, 2016   |    Created                    	  |      Pinki	  |  
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.List;

import com.meshdynamics.vo.ShipMasterVo;

public interface ShipMasterService {

	boolean addShipMaster(ShipMasterVo shipMasterVo) throws Exception;

	public List<ShipMasterVo> getShippingList() throws Exception;

	public ShipMasterVo getShipItemById(int shipId) throws Exception;

	boolean deleteShipMasterById(int shipId) throws Exception;

	boolean updateShipItemDetails(ShipMasterVo shipMasterVo) throws Exception;
	
	public List<ShipMasterVo> getShippingListByInvoiceId(int invid) throws Exception;
	
	public List<ShipMasterVo> getShipPaginationList(String criteria, int start,
			int noOfRecords) throws Exception;
	
	public int getCount(String criteria) throws Exception;

}
