/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : UserRoleService.java
 * Comments : TODO 
 * Created  : 23-Sep-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |23-Sep-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.List;

import com.meshdynamics.vo.UserRoleVo;

public interface UserRoleService {
	boolean addUserRole(UserRoleVo userRoleVo) throws Exception;

	List<UserRoleVo> getUserRoleList() throws Exception;
	
	boolean updateUserRole(UserRoleVo userRoleVo) throws Exception;
	
	boolean deleteUserById(String userName, String apikey) throws Exception;
	
	boolean changePassword(String apikey, String currentPassword,
			String newPassword) throws Exception;

	List<UserRoleVo> getUserRoleListBasedOnPagination(String critera,
			int start, int noOfRecords) throws Exception;
}
