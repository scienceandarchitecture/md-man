/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : PaymentService.java
 * Comments : TODO 
 * Created  : 14-Oct-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |14-Oct-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.List;
import java.util.Map;

import com.meshdynamics.vo.InvoicePaymentVo;
import com.meshdynamics.vo.RawPaymentVo;
import com.meshdynamics.vo.RevenueVo;

public interface PaymentService {

	boolean addInvoicePayment(InvoicePaymentVo invoicePaymentVo)
			throws Exception;

	boolean addRawPayment(RawPaymentVo rawPaymentVo) throws Exception;

	List<InvoicePaymentVo> getInvoicePaymentList() throws Exception;

	RawPaymentVo getRawPaymentById(int rpId) throws Exception;

	boolean deleteRawPaymentById(int rpId) throws Exception;

	public List<RawPaymentVo> getRawPayment() throws Exception;

	boolean updateRawPayment(RawPaymentVo rawPaymentVo) throws Exception;

	boolean updateInvoicePayment(InvoicePaymentVo invoicePaymentVo)
			throws Exception;

	public List<InvoicePaymentVo> getInvoicePaymentByInvoiceId(int invId)
			throws Exception;

	boolean deleteInvoicePaymentByInvoiceId(int invId) throws Exception;

	Map<String, Integer> getPaymentCountByInvoiceId(int invId) throws Exception;
	
	public List<RevenueVo> getRevenueList() throws Exception;

	List<RawPaymentVo> getOpenRawPayment(int custId) throws Exception;

	List<RawPaymentVo> getPaymentListUsingPagination(String criteria,
			int start, int noOfRecords) throws Exception;

	int getCount(String criteria) throws Exception;

	public List<RevenueVo> getRevenueListUsingPagination(int revenueDateFlag, String criteria,
			int start, int noOfRecords) throws Exception;
	

}
