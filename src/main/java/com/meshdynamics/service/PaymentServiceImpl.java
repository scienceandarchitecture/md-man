/*****************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : PaymentServiceImpl.java
 * Comments : TODO 
 * Created  : 14-Oct-2016
 *
 * 
 * Author   : Manik Chandra
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------
 * | No  |Date         |  Comment                            | Author         |
 * ----------------------------------------------------------------------------
 * |  0  |14-Oct-2016  | Created                             | Manik Chandra  |
 * ----------------------------------------------------------------------------
 ******************************************************************************/
package com.meshdynamics.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meshdynamics.common.helper.ModelVoHelper;
import com.meshdynamics.dao.PayMentDaoImpl;
import com.meshdynamics.model.InvoicePayment;
import com.meshdynamics.model.RawPayment;
import com.meshdynamics.vo.InvoicePaymentVo;
import com.meshdynamics.vo.RawPaymentVo;
import com.meshdynamics.vo.RevenueVo;

public class PaymentServiceImpl implements PaymentService {

	private static final Logger logger = Logger
			.getLogger(PaymentServiceImpl.class);

	@Autowired
	PayMentDaoImpl payMentDaoImpl;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.meshdynamics.service.PaymentService#addInvoicePayment(com.meshdynamics
	 * .vo.InvoicePaymentVo)
	 */
	@Override
	public boolean addInvoicePayment(InvoicePaymentVo invoicePaymentVo)
			throws Exception {

		logger.info("PaymentServiceImpl.addInvoicePayment() started");
		InvoicePayment invoicePayment = ModelVoHelper
				.invoicePaymentVoToModel(invoicePaymentVo);
		boolean flag = payMentDaoImpl.addInvoicePayment(invoicePayment);
		logger.info("PaymentServiceImpl.addInvoicePayment() end");
		return flag;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.meshdynamics.service.PaymentService#addRawPayment(com.meshdynamics
	 * .vo.RawPaymentVo)
	 */
	@Override
	public boolean addRawPayment(RawPaymentVo rawPaymentVo) throws Exception {
		boolean flg = false;
		logger.info("PaymentServiceImpl.addRawPayment() started");
		RawPayment rawPayment = ModelVoHelper.rawPaymentVoToModel(rawPaymentVo);
		flg = payMentDaoImpl.addRawPayment(rawPayment);
		logger.info("PaymentServiceImpl.addRawPayment() end");
		return flg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.service.PaymentService#getInvoicePaymentList()
	 */
	@Override
	public List<InvoicePaymentVo> getInvoicePaymentList() throws Exception {

		logger.info("PaymentServiceImpl.getInvoicePaymentList() started");

		List<InvoicePaymentVo> invoicePaymentVoList = new ArrayList<>();
		List<InvoicePayment> invoicePaymentList = payMentDaoImpl
				.getInvoicePayment();
		for (InvoicePayment invoicePayment : invoicePaymentList) {
			invoicePaymentVoList.add(ModelVoHelper
					.invoicePaymentModelToVo(invoicePayment));
		}
		logger.info("PaymentServiceImpl.getInvoicePaymentList() end");
		return invoicePaymentVoList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.service.PaymentService#deleteRawPaymentById(int)
	 */
	@Override
	public boolean deleteRawPaymentById(int rpId) throws Exception {

		logger.info("PaymentServiceImpl.deleteRawPaymentById() started");

		boolean flag = payMentDaoImpl.deleteRawPaymentById(rpId);

		logger.info("PaymentServiceImpl.deleteRawPaymentById() end");
		return flag;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.meshdynamics.service.PaymentService#deleteInvoicePaymentById(int)
	 */
	@Override
	public boolean deleteInvoicePaymentByInvoiceId(int invId) throws Exception {

		logger.info("PaymentServiceImpl.deleteInvoicePaymentByInvoiceId() started");

		boolean flag = payMentDaoImpl.deleteInvoicePaymentByInvoiceId(invId);

		logger.info("PaymentServiceImpl.deleteInvoicePaymentByInvoiceId() end");
		return flag;
	}

	/*
	 * PaymentServiceImpl.getRawPayment
	 * 
	 * @return List<RawPaymentVo> comments : TODO
	 */
	@Override
	public List<RawPaymentVo> getRawPayment() throws Exception {

		logger.info("PaymentServiceImpl.getRawPayment() started");

		List<RawPaymentVo> rawPaymentVoList = new ArrayList<>();
		List<RawPayment> rawPaymentList = payMentDaoImpl.getRawPayment();
		for (RawPayment rawPayment : rawPaymentList) {
			rawPaymentVoList.add(ModelVoHelper.rawPaymentModelToVo(rawPayment));
		}
		logger.info("PaymentServiceImpl.getrawPayment() end");
		return rawPaymentVoList;
	}

	/*
	 * PaymentServiceImpl.geRawPaymentById
	 * 
	 * @param rpId
	 * 
	 * @return RawPaymentVo comments : TODO
	 */
	@Override
	public RawPaymentVo getRawPaymentById(int rpId) throws Exception {

		logger.info("PaymentserviceImpl.getRawPaymentById() started");

		RawPayment rawPayment = payMentDaoImpl.getRawPaymentById(rpId);

		RawPaymentVo rawPaymentVo = ModelVoHelper
				.rawPaymentModelToVo(rawPayment);

		logger.info("PaymentserviceImpl.getRawPaymentById() end");

		return rawPaymentVo;
	}

	/*
	 * PaymentServiceImpl.updateRawPayment
	 * 
	 * @param rawPaymentVo
	 * 
	 * @return boolean comments : TODO
	 */

	@Override
	public boolean updateRawPayment(RawPaymentVo rawPaymentVo) throws Exception {

		logger.info("PaymentServiceImpl.updateRawpayment() started");
		RawPayment rawPayment = ModelVoHelper.rawPaymentVoToModel(rawPaymentVo);
		boolean flag = payMentDaoImpl.updateRawPayment(rawPayment);
		logger.info("PaymentServiceImpl.updateRawpayment() started");
		return flag;
	}

	/*
	 * PaymentServiceImpl.getOpenRawPayment
	 * 
	 * @return List<RawPaymentVo> comments : TODO
	 */
	@Override
	public List<RawPaymentVo> getOpenRawPayment(int custId) throws Exception {

		logger.info("PaymentServiceImpl.getOpenRawPayment() started");

		List<RawPaymentVo> rawPaymentVoList = new ArrayList<>();
		List<RawPayment> rawPaymentList = payMentDaoImpl.getOpenRawPaymentList(custId);
		for (RawPayment rawPayment : rawPaymentList) {
			rawPaymentVoList.add(ModelVoHelper.rawPaymentModelToVo(rawPayment));
		}
		logger.info("PaymentServiceImpl.getOpenRawPayment() end");
		return rawPaymentVoList;
	}

	@Override
	public List<InvoicePaymentVo> getInvoicePaymentByInvoiceId(int invId)
			throws Exception {

		logger.info("PaymentServiceImpl.getInvoicePaymentByInvoiceId() started");
		List<InvoicePaymentVo> invoicePaymentVoList = new ArrayList<>();
		List<InvoicePayment> invoicePayments = payMentDaoImpl
				.getInvoicePaymentByInvoiceId(invId);
		for (InvoicePayment invoicePayment : invoicePayments) {
			invoicePaymentVoList.add(ModelVoHelper
					.invoicePaymentModelToVo(invoicePayment));
		}
		logger.info("PaymentServiceImpl.getInvoicePaymentByInvoiceId() end");
		return invoicePaymentVoList;
	}

	@Override
	public boolean updateInvoicePayment(InvoicePaymentVo invoicePaymentVo)
			throws Exception {

		logger.info("PaymentServiceImpl.updateInvoicePayment() started");
		InvoicePayment invoicePayment = ModelVoHelper
				.invoicePaymentVoToModel(invoicePaymentVo);
		boolean flag = payMentDaoImpl.updateInvoicePayment(invoicePayment);
		logger.info("PaymentServiceImpl.updateInvoicePayment() end");
		return flag;
	}

	@SuppressWarnings({ "boxing" })
	@Override
	public Map<String, Integer> getPaymentCountByInvoiceId(int invId)
			throws Exception {

		logger.info("PaymentserviceImpl.getPaymentCountByInvoiceId() started");
		Map<String, Integer> map = new HashMap<String, Integer>();
		int paymentCount = payMentDaoImpl.getPaymentCountByInvoiceId(invId);
		map.put("invoicePaymentCount", paymentCount);
		logger.info("PaymentserviceImpl.getPaymentCountByInvoiceId() started");
		return map;
	}
	
	@Override
	public List<RevenueVo> getRevenueList() throws Exception{
		logger.info("PaymentServiceImpl.getRevenueList() started");
		List<RevenueVo> revenueList = payMentDaoImpl
				.getRevenueList();
		logger.info("PaymentServiceImpl.getRevenueList() end");
		return revenueList;
	}
	
	@Override
	public List<RawPaymentVo> getPaymentListUsingPagination(String criteria,
			int start, int noOfRecords) throws Exception {
		
		logger.info("PaymentServiceImpl.getPaymentListUsingPagination() started");
		Object ob = new ObjectMapper().readValue(criteria, Object.class);

		Map<?, ?> newMap = (Map) ob;

		List<RawPaymentVo> paymentVoList = new ArrayList<>();
		List<RawPayment> paymentList = payMentDaoImpl
				.getPaymentListUsingPagination(newMap, start, noOfRecords);
		for (RawPayment rawPayment : paymentList) {
			paymentVoList.add(ModelVoHelper.rawPaymentModelToVo(rawPayment));
		}

		logger.info("PaymentServiceImpl.getPaymentListUsingPagination() end");
		return paymentVoList;
	}

	@Override
	public int getCount(String criteria) throws Exception {

		logger.info("PaymentServiceImpl.getCount() started");

		Object ob = new ObjectMapper().readValue(criteria, Object.class);
		Map<?, ?> newMapCriteria = (Map) ob;
		int count = payMentDaoImpl.getCount(newMapCriteria);

		logger.info("PaymentServiceImpl.getCount() end");
		return count;
	}

	@Override
	public List<RevenueVo> getRevenueListUsingPagination(int revenueDateFlag, String criteria, int start, int noOfRecords) throws Exception{
		
		logger.info("PaymentServiceImpl.getRevenueListUsingPagination() started");
		Object ob = new ObjectMapper().readValue(criteria, Object.class);

		Map<?, ?> newMap = (Map) ob;
		
		List<RevenueVo> revenueList = payMentDaoImpl
				.getRevenueListUsingPagination(revenueDateFlag,newMap,start,noOfRecords);
		logger.info("PaymentServiceImpl.getInvoicePaymentByInvoiceId() end");
		return revenueList;
	}
}
