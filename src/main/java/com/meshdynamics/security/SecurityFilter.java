package com.meshdynamics.security;

/**
 * @author Sanjay
 *
 */
import java.io.IOException;
import java.util.Date;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;

import com.meshdynamics.common.LoginCache;
import com.meshdynamics.common.MDManufacturerProperties;
import com.meshdynamics.common.helper.Helper;

public class SecurityFilter extends OncePerRequestFilter {

	private static final String url = "/MDManufacturer/login/logout";
	private static final String url1 = "/MDManufacturer/customers";
	private static final String url2 = "/MDManufacturer/forecast";
	private static final String url3 = "/MDManufacturer/inventories";
	private static final String url4 = "/MDManufacturer/invoices";
	private static final String url5 = "/MDManufacturer/jobOrder";
	private static final String url6 = "/MDManufacturer/payment";
	private static final String url7 = "/MDManufacturer/products";
	private static final String url8 = "/MDManufacturer/ships";
	private static final String url9 = "/MDManufacturer/terms";
	private static final String url10 = "/MDManufacturer/tracking";
	private static final String url11 = "/MDManufacturer/userRole";
	private static final String url12 = "/MDManufacturer/login/changePassword";
	private static final String url13 = "/MDManufacturer/customers/getCustomerList";
//	private static final String pattern = "^((http[s]?|ftp):\\/)?\\/?([^:\\/\\s]+)((\\/\\w+)\\/)([\\w\\+\\.]+[^#\\/\\s]+)$";
//	private static final String pattern_1 = "^((http[s]?|ftp):\\/)?\\/?([^:\\/\\s]+)(\\/)$";
	
	public SecurityFilter() {
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		String errorMsg = null;
		
		String requestUrl = request.getRequestURI();
		/*if ((!(requestUrl.matches(pattern)) || (!(requestUrl.matches(pattern_1)))))
				{
			errorMsg = MDManufacturerProperties.getValue("errorMsg.incorrectUrl");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, errorMsg);
		} else 
		*/
			if (requestUrl.contains(url) || requestUrl.contains(url1) || requestUrl.contains(url2)
				|| requestUrl.contains(url3) || requestUrl.contains(url4) || requestUrl.contains(url5)
				|| requestUrl.contains(url6) || requestUrl.contains(url7) || requestUrl.contains(url8)
				|| requestUrl.contains(url9) || requestUrl.contains(url10) || requestUrl.contains(url11)
				|| requestUrl.contains(url12)) {

			Map<String, String> map = LoginCache.getInstance();
			boolean flag = true;
			String parmValue = request.getHeader("Authentication");
			if (map.containsKey(parmValue)) {
				flag = false;
				String apikyValue = map.get(parmValue);
				String[] arr = apikyValue.split(",");
				String sdate = arr[1];
				Date logindate = Helper.getDateTimeFromString(sdate);
				Date date = new Date();
				long timediff = date.getTime() - logindate.getTime();
				long diffsec = timediff / 1000 % 60;
				long diffmin = timediff / (60 * 1000) % 60;
				if ((diffmin * 60 + diffsec) > 900) {
					map.remove(parmValue);
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Time out ");
				} else {
					// for to calculate in active time of
					// user
					String value = arr[0] + "," + Helper.getStringDateTimeFromDate(new Date());
					map.put(parmValue, value);
					// forward the request to related rest api
					chain.doFilter(request, response);
				}
			} else {
				errorMsg = MDManufacturerProperties.getValue("errorMsg.security");
			}
			if (requestUrl.contains(url13)) {
				chain.doFilter(request, response);
			}else if (flag) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, errorMsg);
			}
		} else {
			chain.doFilter(request, response);
		}
	}
}
