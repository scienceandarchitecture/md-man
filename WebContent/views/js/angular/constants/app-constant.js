(function() {
	'use strict';
	app.constant('appConstants', {
	  "apiUrl":"",
	  "sucesscode": 201,
	  "messages":{
		  "addMessage":"Record added successfully",
		  "updateMessage":"Record updated successfully"
	  },
	  "jobstatus":{
		  "created":"created",
		  "acknowledged":"acknowledged",
		  "finished":"finished"  
	  },
	  "shipstatus":{
		  "created":"created",
		  "acknowledged":"acknowledged",
		  "finished":"finished"  
	  },
	  "restApiUrl":{
		  "getPurchaseOrderList": "invoices/getPurchaseOrderList",
		  "getRawInventoryList":"inventories/getRawInventories",
		  "getRawInventoryPaginationList":"inventories/getInventoryListUsingPagination",
		  "getProductList" : "products/getProductList",
		  "getProductPaginationList":"products/getProductListUsingPagination",
		  "getSubProductList" : "subProduct/getSubProduct",
		  "jobOrderList" : "jobOrder/getJobOrders",
		  "getJobOrderListById" : "jobOrder/getJobOrderListById",
		  "getjobOrderPaginationList":"jobOrder/getJobOrderListBasedOnPagination",
		  "addRawInventory":"inventories/addRawInventories",
		  "updateRawInventory":"inventories/updateRawInventories",
		  "deleteRawInventory":"inventories/deleteRawInventoryById?rsku=",
		  "getshipOrderPaginationList":"ships/getShipListBasedOnPagination",
		  "addUser" : "userRole/addUsers",
		  "updateUser":"userRole/updateUser",
		  "deleteUser":"userRole/deleteUserById?userName=",
		  "getUserPaginationList":"userRole/getUserBasedOnPagination",
		  "addProduct":"products/addProducts",
		  "updateProduct":"products/updateProducts",
		  "deleteProduct":"products/deleteProductById?psku=",
		  "changePassword":"userRole/changePassword",
		  "forgotPassword":"login/forgotPassword",
		  "rebuildImage":"jobModelFlash/regenerateBuild",
		  "macAddressRange":"macaddress/viewMacRange",
		  "getmacAddressPaginationList":"macaddress/getMacAddressListUsingPagination",
		  "deletePurchaseOrder"  :"invoices/deleteInvoiceById",
		  "getRevenuePaginationList" : "payment/getRevenueListUsingPagination",
		  "getPurchaseOrderById" : "invoices/getPurchaseOrderById",
		  "getSinglePurchaseOrderById" :"invoices/getInvoiceById",
		  "loadDynamicBuildURL" : "jobModelFlash/fetchBuildServerImageDownloadPath"
	  },
	  "companyAddress" :"3355 Benton St. \n Santa Clara CA 95051, USA \n Phone (408) 373-7700 Fax (408) 493-4424",
	  "flashingStatus" : {
		  "1":"Open",
		  "2":"In Progress",
		  "3":"Ready DN",
		  "4":"Download Complete",
		  "5":"Download Failed"
	  },
	  "revenueDateFlag" : {
		  "current_month" : 1,
		  "current_quarter" : 2,
		  "current_year" : 3,
		  "any_month" : 4
	},
	  "internal_server_error_codes" : {
		  "no_records":202
	  },
	  "httpStatusCode": {
		  "UNAUTHORIZED": 401
	  },
	  "responseCode" : {
		  OK:200,
		  CREATED : 201,
		  NO_RECORDS : 202,
		  PARTIAL_CONTENT : 206,
		  UNAUTHORISED : 401,
		  UNSUPPORTED_MEDI : 415,
		  LOGIN_TIMEOUT : 440,
		  BAD_REQUEST : 400,
		  INVALID_TOKEN : 498,
		  INTERNAL_SERVER_ERR : 500,
		  NOT_IMPLEMENTED : 501,
		  NOT_MODIFIED : 304
	  }
	});
})();