(function() {
	'use strict';
	app.factory('customerService',['apiDelegateService','apiHandlerService','appConstants','errorHandlerService',function(apiDelegateService,apiHandlerService,appConstants,errorHandlerService){
		return {
			getCustomerLists : function(){
				var options = {
						url:appConstants.apiUrl+'customers/getCustomerList',
						method:'GET'
					}
					return apiDelegateService.apiHandler(options)
						.then(function(sucessResponse) {
							return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
				});
			},
			getCustomerListsByName : function(name) {
				var options = {
						url:appConstants.apiUrl+'customers/getCustomerByName?name='+name ,
						method:'GET'
					}
					return apiDelegateService.apiHandler(options)
						.then(function(sucessResponse) {
							return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
				});
			},
			getCustomerById : function(customerId) {
				var options = {
						url:appConstants.apiUrl+'customers/getCustomerById?custId='+customerId ,
						method:'GET'
					}
					return apiDelegateService.apiHandler(options)
						.then(function(sucessResponse) {
							return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
				});
			}
		}
	}]);
})();