(function() {
	'use strict';
	app.factory('customerDailogService',['dailogService','apiHandlerService','$rootScope','customerService',function(dailogService,apiHandlerService,$rootScope,customerService) {
		var currentCustomer = null;
		var activeAfterCall;
		var dialogObject;
		var addUpdate = false;
		var dialogScope = null;
		var dailogObject;
		
		function callback(model) {
			dialogScope = model;
			model.customerFlag = false;
			/*Reset data*/
			model.resetCustomerData = function() {
				model.customer = {
					name:"",
					address:"",
					shipAddress:"",
					instructions:""
				};
			}
			
			model.resetCustomerData();
	
			if(currentCustomer !== null) {
					model.customerFlag = true;
					model.customerId = currentCustomer.custId;
					model.customer = angular.copy(currentCustomer);
			}
			
			model.addUpdateCustomer = function() {
				if( model.customerFlag ) {
					apiHandlerService.updateData( model.customer ,'customers/editCustomerRecord', 'PUT',activeAfterCall);
				} else {
					apiHandlerService.addData( model.customer ,'customers/addCustomers', 'POST',activeAfterCall);
				}
			}
		
			model.cancel = function() {
				dailogService.closeDailog();
			};
		}
		
		var customerDailogService =  {
			openCustomerDialog : function($event,refreshList,customer) {
				activeAfterCall = refreshList;
				currentCustomer = customer || null;
				addUpdate = false;
				dailogObject = {
						targetEvent : $event,
						templateUrl : 'views/partials/dialogs/addCustomer.html',
						skipHideValue:true
					};
				dailogService.showDailog(dailogObject,callback);
			}
		};
		return customerDailogService;
	}]);
})();