(function() {
	'use strict';
	app.factory('purchaseOrderDailogService',['dailogService','purchaseOrderService','apiHandlerService','$rootScope','termService','userService','productService','customerService','customerDailogService','productDailogService','shipOrderService','paymentService','trackingService','MessageService',function(dailogService,purchaseOrderService,apiHandlerService,$rootScope,termService,userService,productService,customerService,customerDailogService,productDailogService,shipOrderService,paymentService,trackingService,MessageService) {
		var currentPurchaseOrder = null;
		var activeAfterCall;
		var dialogObject;
		var addUpdate = false;
		var dialogScope = null;
		var customers = null;
		var initallyLoadedData = {
		};
		var currentPurchaseOrderId = null;
		
	
		 
		function callbackAfterSippeedOrderByInvoiceDailog(model) {
			model.invoiceId = currentPurchaseOrderId;
			model.shippedOrdersList = [];
			shipOrderService.getShippedOrderLists(currentPurchaseOrderId).then(function(shippedOrderList){
				model.shippedOrdersList = shippedOrderList;
			});
			
			model.cancel = function() {
				dailogService.closeDailog();
			};
		}
		
		function callbackAfterPaymentsFromInvoiveDialog(model) {
			model.purchaseOrderId = currentPurchaseOrderId;
			model.invoicePaymentLists = [];
			paymentService.getInvoicePaymentLists(model.purchaseOrderId).then(function(invoicePaymentList){
				model.invoicePaymentLists = invoicePaymentList;
			});
			
			model.cancel = function() {
				dailogService.closeDailog();
			};
		}
		
		function callbackAfterTrackingItemsByInvoiveDialog(model) {
			model.purchaseOrderId = currentPurchaseOrderId;
			model.trackingItemsList = [];
			trackingService.getTrakingLists(model.purchaseOrderId).then(function(trackingList){
				model.trackingItemsList = trackingList;
			});
			
			model.cancel = function() {
				dailogService.closeDailog();
			};
		}
		function callback(model) {
			dialogScope = model;
			model.showTerm = false;
			model.showSearch = false;
			model.invoiceFlag = false;
			model.invoiceitems = [];
			model.addedProduct = false;
			model.updateButton = false;
			model.saveButton = true;
			model.todayDate = new Date();
			model.customerFlag = false;
			
			model.termsList = initallyLoadedData.termsList;
			model.customerList = [];
			model.usersList = initallyLoadedData.usersList;
			model.productList =  initallyLoadedData.productList;
			model.subItems = initallyLoadedData.subItems;
			model.newItem;
			
			model.subDropDownList = [{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9},{value:10},{value:11},{value:12},{value:13},{value:14},{value:15},{value:16},{value:17},{value:18},{value:19},{value:20},{value:21},{value:22},{value:23},{value:24},{value:25},{value:26},{value:27},{value:28},{value:29},{value:30},{value:31},{value:32},{value:33},{value:34},{value:35},{value:36},{value:37},{value:38},{value:39},{value:40},{value:41},{value:42},{value:43},{value:44},{value:45},{value:46},{value:47},{value:48},{value:49},{value:50},{value:51},{value:52},{value:53},{value:54},{value:55},{value:56},{value:57},{value:58},{value:59},{value:60},{value:61},{value:62},{value:63},{value:64},{value:65},{value:66},{value:67},{value:68},{value:69},{value:70}];
			model.subItems;
			
			model.customerAutocompleteData = {
					 selectedItem:"",
					 customers : []
			 };
			
			/*Reset data*/
			model.resetInvoiceData = function() {
				model.newInvoice = {
					itemTotal:0,
					amount:0,
					discount:0,
					amountbeforeDiscount:0,
					shipping:0.00,
					status:0,
					invJSDate: new Date(),
					dueJSDate: new Date(),
					invDate:"",
					dueDate:""
				};
				model.invoiceitems = [];
				model.customerId = "";
				model.newInvoice.terms = "";
				model.todayDate = new Date();
				model.invoiceFlag = false;
				model.showTerm = false;
			}
			
			function loadCustomers() {
		         var customersForAutoComplete = model.customerList.map( function (customer) {
		        	return {
		               value : customer,
		               display : customer.name
		            };
		         });
		         model.customerAutocompleteData.customers = customersForAutoComplete;
		         return customersForAutoComplete;
		     }
				
			//filter function for search query
		     function createFilterFor(query) {
		        var lowercaseQuery = angular.lowercase(query);
		        return function filterFn(customer) {
		            return (customer.value.name.toLowerCase().indexOf(lowercaseQuery) === 0);
		         };
		     }	
		    
			model.querySearch = function(query) {
				return customerService.getCustomerListsByName(query).then(function(customerList) {
					model.customerList = customerList;
					return loadCustomers();
					
				});
		    }
			 
			model.calculateTotalAndAmount = function() {
				model.newInvoice.itemTotal = 0;
				model.newInvoice.amountbeforeDiscount = 0;
				angular.forEach( model.invoiceitems , function(item) {
					model.newInvoice.itemTotal += +item.quantity;
					model.newInvoice.amountbeforeDiscount += +item.quantity * +item.price;
				});
				model.newInvoice.amount = +model.newInvoice.amountbeforeDiscount + +model.newInvoice.shipping - +model.newInvoice.discount;
			}
			 /*Get Customer List*/	 
			if(addUpdate) {
				model.resetInvoiceData();
				if( currentPurchaseOrder !== null ) {
					model.invoiceFlag = true;
					model.newInvoice = _.clone(currentPurchaseOrder);
					model.invoiceitems =  _.clone(model.newInvoice.invoiceitems);
					model.calculateTotalAndAmount();
					model.newInvoice.invJSDate = new Date.createFromMysql(currentPurchaseOrder.invDate);
					model.newInvoice.dueJSDate = new Date.createFromMysql(currentPurchaseOrder.dueDate);
					
					model.todayDate = new Date.createFromMysql(model.newInvoice.invDate);
					//Set Term from term list
					for ( var i=0; i < model.termsList.length; i++ ) {
						if(currentPurchaseOrder.terms === model.termsList[i].text) {
							model.newInvoice.termText  = model.termsList[i];
						}
					}
					
					/*Get the customer From API and  set it on purchaseorder object*/
					model.customerAutocompleteData.customers = [{
			               value : customers,
			               display : customers.name
			            }];
					model.newInvoice.customer = model.customerAutocompleteData.customers[0];
				}
			}
			
				
		    
			/*Add/update invoice*/	
			model.addUpdateInvoice = function() {
				model.newInvoice.invoiceitems= model.invoiceitems;
				model.newInvoice.custName = model.newInvoice.customer.value.name;
				model.newInvoice.custId = model.newInvoice.customer.value.custId;
				model.newInvoice.invDate = model.newInvoice.invJSDate.toMysqlFormatForInvoice();
				if(model.newInvoice.dueJSDate !== null) {
					model.newInvoice.dueDate = model.newInvoice.dueJSDate.toMysqlFormatForInvoice();
				} else {
					model.newInvoice.dueDate = "";
				}
				model.calculateTotalAndAmount();
				if(model.showTerm) {
					model.newInvoice.termId = 0;
				} else {
					model.newInvoice.termId = model.newInvoice.termText.termId;
					model.newInvoice.terms = model.newInvoice.termText.text;
				}
				
				if( model.invoiceFlag ) {
					apiHandlerService.updateData(model.newInvoice,'invoices/updateInvoiceById', 'PUT', 'refreshInvoiceList');
				} else {
					apiHandlerService.addData(model.newInvoice,'invoices/addInvoices', 'POST','refreshInvoiceList');
				}
			}
					 
			/*Toggle Text and Dropdown box*/
			model.toggle = function(){
				return model.showTerm = !model.showTerm;
			}
			
			if(currentPurchaseOrder != null) {
				//Title to shown when delete purchase order dailog is called.
				model.title="Are you sure want to delete purchase order number "+currentPurchaseOrder.invId;
			}
			
			/*Delete selected purchase Order*/
			model.deleteEntry = function() {
				purchaseOrderService.deletePurchaseOrder(currentPurchaseOrder.invId,activeAfterCall);   
			}
			
			model.selectedCustomerDetails = function(selectedCustomer) {
				if(selectedCustomer != null) {
					model.newInvoice.billTo = selectedCustomer.address;
					model.newInvoice.shipTo = selectedCustomer.shipAddress;
					model.newInvoice.comments = selectedCustomer.instructions;
				}
			}
			
			model.updateCustomerDataAfterAPICall = function() {
				if( model.newInvoice.customer !== null ) {
					customerService.getCustomerById(model.newInvoice.customer.value.custId).then(function(currentCustomer) {
						var counter = 0;
						for(var i = 0; i < model.customerAutocompleteData.customers.length;i++){
							if(model.customerAutocompleteData.customers[i].value.custId == currentCustomer.custId) {
								model.selectedCustomerDetails(currentCustomer);
								model.customerAutocompleteData.customers[i].value.address = currentCustomer.address;
								model.customerAutocompleteData.customers[i].value.shipAddress = currentCustomer.shipAddress;
								model.customerAutocompleteData.customers[i].value.instructions = currentCustomer.instructions;
							}
							counter++;
						}
					});
				}
			}
			
			model.cancel = function() {
				dailogService.closeDailog();
			};
			
			
			
			/*Customer Related Code*/
			model.showCustomerDialog = function($event,isNew) {
				if( !isNew ) {
					if(model.newInvoice.customer != null) {
						customerDailogService.openCustomerDialog($event,'refreshCustomerList',model.newInvoice.customer.value);
					} else {
						MessageService.showCustomAlert("Please select valid customer before edit.",true);
					}
				} else {
					customerDailogService.openCustomerDialog($event,'refreshCustomerList',null);
				}
			}
			
			model.showProductDialog = function($event) {
				productDailogService.openProductDailog($event,'refreshProducts');
			}
		}
		
		var purchaseDailogOrderService =  {
			openDeleteProductDialog : function($event,refreshList,purchaseOrder) {
				activeAfterCall = refreshList;
				currentPurchaseOrder = purchaseOrder || null;
				addUpdate = false;
				dialogObject = {
						targetEvent:$event,
						templateUrl:'views/partials/dialogs/alertDialog.html'
				};
				dailogService.showDailog(dialogObject,callback);
			},
			showInvoiceDialog : function($event,refreshList,purchaseOrder) {
				dialogObject = {
						targetEvent : $event,
						templateUrl : 'views/partials/dialogs/addInvoice.html',
						skipHideValue:true
					};
				addUpdate = true;
				if( purchaseOrder !== undefined ) {
					currentPurchaseOrder  = purchaseOrder;
					customerService.getCustomerById(currentPurchaseOrder.custId).then(function(customerById) {
						customers = customerById;
						dailogService.showDailog(dialogObject,callback);
						return;
					});
					return;
				} else {
					currentPurchaseOrder = null;
				}
				dailogService.showDailog(dialogObject,callback);
			},
			showShippedOrderByInvoiveDialog : function($event,purchaseOrderId) {
				currentPurchaseOrderId = purchaseOrderId;
				dialogObject = {
						targetEvent : $event,
						templateUrl :  'views/partials/dialogs/shipOrderFromInvoive.html'
				};
				dailogService.showDailog(dialogObject,callbackAfterSippeedOrderByInvoiceDailog);
			},
			showPaymentsFromInvoiveDialog : function($event,purchaseOrderId) {
				currentPurchaseOrderId = purchaseOrderId;
				dialogObject = {
						targetEvent:$event,
						templateUrl:'views/partials/dialogs/paymentsFromInvoive.html'
				};
				dailogService.showDailog(dialogObject,callbackAfterPaymentsFromInvoiveDialog);
			},
			showTrackingItemsByInvoiveDialog : function($event,purchaseOrderId) {
				currentPurchaseOrderId = purchaseOrderId;
				dialogObject = {
						targetEvent:$event,
						templateUrl:'views/partials/dialogs/trackingFromInvoive.html'
				};
				dailogService.showDailog(dialogObject,callbackAfterTrackingItemsByInvoiveDialog);
			},
			getUpdateFlag : function() {
				if(dialogScope == null || dialogScope == undefined) {
					return true;
				}
				return dialogScope.invoiceFlag;
			},
			setCustomerDetails : function() {
				dialogScope.updateCustomerDataAfterAPICall();
			},
			setTermList : function() {
				termService.getTermLists().then(function(termList) {
					initallyLoadedData.termsList = termList;
				});
			},
			setUserList : function() {
				/*Get users list*/	  
				userService.getUserLists().then(function(userList) {
					initallyLoadedData.usersList = userList;
				});
			},
			setProductList : function() {
				productService.getProductsList().then(function(productList){
					initallyLoadedData.productList = productList;
				});
			},
			setSubProductList : function() {
				productService.getSubProductLists().then(function(subProducts){
					 initallyLoadedData.subItems = subProducts;
				 });
			}
		};
		return purchaseDailogOrderService;
	}]);
})();