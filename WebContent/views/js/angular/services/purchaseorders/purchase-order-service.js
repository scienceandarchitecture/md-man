(function() {
	'use strict';
	app.factory('purchaseOrderService',['apiHandlerService','apiDelegateService','appConstants','errorHandlerService','wordWrapService','$filter',function(apiHandlerService,apiDelegateService,appConstants,errorHandlerService,wordWrapService,$filter){
		
		function convertToPrintableItem(currentInvoice) {
			 let invoiceItems = [
	           		[
	           		    {text:'QTY',style: 'billtoShipToHeader'},
	                     {text:'ITEM',style: 'billtoShipToHeader'},
	                     {text:'DESCRIPTION',style: 'billtoShipToHeader'},
	                     {text:'PRICE',style: 'billtoShipToHeader'},
	                      {text:'AMOUNT',style: 'billtoShipToHeader'},
	           		    ]
	           	    ];
			 let total = 0;
			 currentInvoice.invoiceitems.forEach(function(item) {
				 total += parseFloat(item.price) * parseFloat(item.quantity);
				 invoiceItems.push([{ 'text': item.quantity,style:"itemContent",border: [true, false, true, false]},{'text':wordWrapService.gerWordWrapString( item.psku,13,"\n",true),style:"itemContent",border: [true, false, true, false]},{'text': wordWrapService.gerWordWrapString(item.extraDesc,44,"\n",true),style:"itemContent",border: [true, false, true, false]},{'text' : item.price,style:"priceandamount",border: [true, false, true, false]},{'text' : parseFloat(item.price) * parseFloat(item.quantity),style:"priceandamount",border: [true, false, true, false]}]);
			 });
			 invoiceItems.push( [
   		        {text:'',border: [true, false, false, true]},
                   {text : 'Subtotal',style:'qtyItemTableContent',border: [true, false, true, true]},
                   {text : '',border: [false, false, true, true]},
                   {text : '',border: [false, false, false, true]},
                   {text : total,border: [true, false, true, true],style:"priceandamount"},
    		    ]);
			 //Discount
			 invoiceItems.push( [
	   		        {text:'',border: [true, false, false, true]},
	                   {text:'Discount',style:'itemContent',border: [true, false, true, true]},
	                   {text:'',border: [false, false, true, true]},
	                   {text:'',border: [false, false, false, true]},
	                   {text :currentInvoice.discount ,border: [true, false, true, true],style:"priceandamount"},
	    		    ]);
			 //Shipping
			 invoiceItems.push( [
	   		        {text:'',border: [true, false, false, true]},
	                   {text:'Shipping',style:'itemContent',border: [true, false, true, true]},
	                   {text:'',border: [false, false, true, true]},
	                   {text:'',border: [false, false, false, true]},
	                   {text : currentInvoice.shipping ,border: [true, false, true, true],style:"priceandamount"},
	    		    ]);
			 invoiceItems.push([
 		      {text:'',style:'qtyItemTableContent',border: [true, true, false, true]},
            {text:'',style:'qtyItemTableContent',border: [false, true, false, true]},
            {text:'',style:'qtyItemTableContent',border: [false, true, false, true]},
            {text:'TOTAL',border: [true, true, false, true]},
            {text : currentInvoice.amount ,border: [false,true, true, true],style:{alignment:"right"}}
 		    ]);
			 return invoiceItems;
		 }
		var purchaseOrderService =  {
			getList : function() {
				 var options = {
						  url : appConstants.apiUrl + 'invoices/getInvoices',
						  method : 'GET'
				  };
				  return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
					  return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
					});
			},
			getOpenPurchaseOrderList : function() {
				 var options = {
						  url : appConstants.apiUrl + appConstants.restApiUrl.getPurchaseOrderList,
						  method : 'POST',
						  data :{ status : 'open'}
				  };
				  return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
					  return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
					});
			}
			,
			getListWithPagination : function(pagination,currentPage,criteria) {
				var count = currentPage * pagination.perPageItemCount - pagination.perPageItemCount + 1;
				var url = appConstants.apiUrl + 'invoices/getPurchaseOrderListWithPagination?start='+count+"&noOfRecords="+ pagination.perPageItemCount;
				var options = {
						  url : url,
						  method : 'POST',
						  data : criteria
					};
				  return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
					  return sucessResponse.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
					});
			},
			getPurchaseOrderListForCSV : function(numberOfRecords,criteria) {
				 var options = {
						  url : appConstants.apiUrl + 'invoices/getPurchaseOrderListWithPagination?start='+ 1 +"&noOfRecords="+ numberOfRecords,
						  method : 'POST',
						  data : criteria
				  };
				 
				 return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
					  return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
					});
			},
			getPurchaseOrderById : function(purchaseOrderId) {
				 var options = {
						  url : appConstants.apiUrl + appConstants.restApiUrl.getPurchaseOrderById + "?invId=" + purchaseOrderId,
						  method : 'GET'
				  };
				  return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
					  return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
					});
			},
			getSinglePurchaseOrderById : function(purchaseOrderId) {
				 var options = {
						  url : appConstants.apiUrl + appConstants.restApiUrl.getSinglePurchaseOrderById + "?invId=" + purchaseOrderId,
						  method : 'GET'
				  };
				  return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
					  return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
					});
			},
			deletePurchaseOrder : function(purchaseOrderId,refreshList) {
				apiHandlerService.deleteData(appConstants.restApiUrl.deletePurchaseOrder+"?invId="+purchaseOrderId,'DELETE',refreshList);
			},
			downloadInvoicePdf: function(currentInvoice) {
				 convertToPrintableItem(currentInvoice);

				 var docDefinition =  {
				 			 	content: [
				 			 			{
				 			 			    style:'logoTable',
				 			 			    table: {
				 			 			    	widths: [375,115],
				 			 				body: [
				 			 					[
				 			 					    { image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHwAAAAnCAMAAAD6idy4AAAC/VBMVEU/N9U/QvZANc5AN9RBRPdCOdJDO9dDRvdEO9REPtxER/dFO9JFSPdGSfdHPM9HPdBHSvdHS/lIQNhISO9IS/dJPs9JTPdKPspKPsxKTfdKTflLQM5LTvdMQ9VMR99MT/dNQMpNQc1NQ9BNTOxNUPRNUPdORtlOUfdPUvZPUvdPUvlQT+5QU/dRQ8pRVPdSVfdSVfhTVvdUV/dVTdpWWfdZScVaXfhbXvddWeNfYfhiZfhjVMZkZfZnavlpavNqV7xqbfhtcPhucfhvcvhxc/hydPl0dvl0ef11dvR4bNJ5fPl9ZrV+fvN/gvmChPmEhvqFif2JjPqMca2PkvqTlfqWmPqZh8WbnfqfofqjpfumiKWmqPurrfuvsPuwsvu0tfy2r+W4ufy8vfy+wPzBwPjEnY/ExfzGpqTHyPzLzf3O0P3QvcjQ0v3Sp4fU1f3X2P3b2/7d3f7hsoLi4v7mwp3m3+jn6P7qt3fq6v70w3n09P/1wHL1xoD17ef3yIH316n43LX5wWr5wWz5wm35w2/5w3H5w3L5xHH5xHP5xXX5xXb5xnX5xnb5xnf5x3j5x3r5yHv5yH35yX75yYD5yn/5yoH5y4P5y4T5zIT5zYf5zov6x3b6yX/6yn76y4P6zIP6zIX6zYf6zYj6zon6zov6z4360I/60ZH60ZL60pP605X605b6+f/6+//7xW/7xnH7xnL7x3T7yHb71Jf71Jn71Zr71Zz71p3715/716D72KL72qX726n73Kv73a/8zYP81Jb83Kv83az83rH837L84LX84bj847z85L/85cH858X86cr868/8/P/9zX/947n96Mf96Mj96s3969H97NH97dP97tf979n98Nv98t/+x3D+yXH+2Zv+8d3+8uH+8+L+9OX+9ef+9ej+9un+9+v+9+z++O7/yGv/ynT/zHL/zXj/0XD/1Gf/14v/3JL/46n/8dj/9OL/9dX/9uf/+vL/+/b//Mv//Pf//Pn//ej//fv//vv//v3///7///96GOGmAAAIx0lEQVR4AcST0WoaTxTG/wH/LCy7sFDZZtKFIQ3D5FjR1oo2YqCauoUFtVZF0xhlI1gVySpSEe8KCbQXeYa+gD5CH6AXXjdX6Tv0JvSiQ89sIXchUIV84DDnzAc/Ps/Z/8Q96l/h3mAw6E3uBz5LUQCtcT/wYdhiNNheM3w5nwsUnjeNxeLmdYHyLx3CgfE+utcCbzn5fL5YtjOZwtB1Mhnbj+WW7Ez2wGks8T6pF7JZWSC/pgHwqIPu/GgNcGfDQOkmMY2EpROiRsdCHCmqiTKU8lLM7Q2DBE1TD1SFyBuAwsrcKK0On+1ZAGBxQJkMD8b64sSieOH409riUOfAWRiAwnjuuymX5sxiZbgHSDTzOQuAOGUDsPQmMYLpSgfYU2qzGAXOG7jljJ+OpNvKvsIX016uDHcNmavjmADqcUNFeHTaUgB5nRwB0Oo9zGmlp9GApljTE3SDcVRAt76Gv72iAdDkGINxclIMYv6Xfo8lO3FMqR41FSwSjVqlUq6Lqr9vvQyRLyvDlzaGMB3vGQMe6exTP1HBAIBInOOptJoBALBY27dLN00OExR40F0ZPksiT6v5n2/ClVm1hsghAhihlFrBXp+gAygrdpd/3Wa+Bxw47a4MH2NkMNqHKgBNHRIAjsu+R0OhUGxPKj0VLWIB9nW1Kd3+JPzVGK4MPw0z4OGBHLZl13QAFpt+eLKzs/OgNT5FjdDTjBL/20ovPXSD4eLk0TcRt+r31eXl5fcfd8E7FschjlIYzihXEW6lrt9vPtrefNwtROJxVpl53qiRZoC48LSLbhYd2ET6ZrfDr798vrg4+3oXvIkhSK4fYQD6cQnzm69/vft/e3vzeTdBGA20+iwcfVqqBmXWmavJ6Xhxmb8obtfVp7Pz84/f7oLXVIBgoWVy4LzrbMGu/ubnW4Q/fOGGsUeHbY0xpSanTPZFXdmFrVwHQiH4I/bw37+RzfsD4ULIBzt27ty57cY/iMjvP0AKxv6NsPyslwgovEN4wZndRk5Wli3sHchyaQtPITU1IY+fUZxAygfE4Qx77ykkqyYcECGoLacmm/52+Y7LQCNObd6+ffvOPSuXLdvx4OKWZcs2XwKKbdmxY/vOR//+PTm6bdmyZSsPPgUq/Hps53IgZ8uJ3/8YEDmNN9QLnNkLjMUVFFmj3kQxKyoqassqKEgwpXyLYlVQUFKWAHJYU1tMJRQVOIMDhJUUJHSLbre3tAIt2tzY2tre2tLe0dE6paO1o6Ol/8m/zU3t7S2dD/49W9zU2tHe0d686Om/37saWzra2ztam/f9hVieB6qu2MMtecXEWMJTWPn4+bhTXxdZM4vKqshKcCsG1b7P1WWVkFOWluYW9a7M5efm52MItuPg42e1rtzU29u++M/36cC01QvEQNDRBSS6um78Bol1Tvr+bGkLVK5597/zLT29PV1g/kOI5RkOTs5Orslejs5OtpkJ9iBQcu5FUZCpmqaarnfuqw+Lb+cGmWorK+p6p258nw6St471BFFhd9qB5kz8fa8XaGD79hWdQLtX72ntBXLvPQCLrf63p6m3t2fKvt6e3s5FjyeCHHFiUWdvV88NBlhzBQjOrgWBfw3VlUBwfmrnixfl2TnpRbNfzj0xoefF7ZK87NyiTS8nnKgDSlfXfwUUY31PaVxR+B/vi+xu9t71smy3YE2TsaBAoplJI0qUH9AmIAo41VLApR1/WJT2qZ22OjgLe6bnW3TygJnpSybnYe/cc++e7zvfOWdc+QLLIYCkG7R5UYvdLcGz1dzX2GePOuzjSRsnGM/cbQsGT7UVniNH0zU5nP+GC374Lpfb/L5lq+izX/+hP3/JWYk8v/76iP76/Y9ncqW2mcvlXh//m3uz+ebHv5/KMLv3OuPFL5MQulNgNUSGqlDb7HhSMYejKjfFQv6IfYvP9+uVSnlvMg9+G4d028cCco5ow4wq94WIKtGjdUicqiBJ69RzE8lksriI7Mp8DXqfxSwWt7sq2VdiH9QfNlkHJQ6b+d3d/E9HEZRbHnzk6/UsCqK1A6TydELbHMF+rqKW/dv4q5DWTOjR1L9qV5f4FoT2caTXugJHLRcRDqaQWy5N6ogUu6DQekICXez2po+BNxHPOC7zYm4EtMNQoCO+oRMrpFU0IXTgN5aUwZEgw2CAI7PTQLGTDQGfN4qhIm+pCLLLPs2sZcmQr958DBzVU9E+Kx01CgTw0LQKtcIaNvNmKHQnIlkPHEnnthW+5BX4snhZ0+G7GWC6tDq9QkWykzEMuTtCIc7q3Tz4FJWSK55rMcT7D+CyTxBDxfrpsKNnVV7/ViIy1ZFy4iKFXMtMH/3WBciTNqXZaRR7rus6ef/87OIgFcazL+fBb1wEKHTRotrRPTgAgiAj0G+nDjeV6F4ja9kAuP6OSjpS9mwLxS4ZoTRQw4pd3iGgXt+LSBmpDZ2o7aTfmZDmeh586OBuY3/WooSwMK1GN18+0EKz9zG28Rb7IMP2Ez7aaZkQvxcO+z6FjZbwZwH3MH5G1xNSGjv7oLrmz4Ofgr1ZeiGZtnP1AK7sCwIqB+U3QbuBZamiAHcWpMWD3jI+DMXv3Nd6lo3cSlhR+fVdx1Q8kW8FtKB58L5EE3FLosEnM3DwnNC9GGHzuuNK6Mc13sykrW6DwtY5ht06mcbBIU9jnEU5qrVQpkNDcXAUTO89An6uTEsJXbBFijT1IRcHLvAYLAhh2F6Kz/Q1akakZZmG4E2KBtIUQquv8Hah+rNuCkMNz4XJuxIFhYiwLClNtXNN10lNWpI3cvf2sVFruUpmKlm21TZVl+1Qdb1NtLeWzWaKA/wxXmuQjz8U64VMNpsuUy/NzpedVxk+6h+v8r0Nvw/fapPodg/t7WT6iH5ZWuJNbOPkI/+f33jeLfkTNvKXNQme2rZPNGWfP+UnLwG+U07OphT6cMQWTPzwiQsT+nCRfP6QvJo8hB943nXwP34cGMVtJ+Yk1lvjz/CbjH/JdjUGz08L/hnsPxE2cF+QsfTSAAAAAElFTkSuQmCC',border: [false, false, false, false],style: 'logo'},
				 			 					    { text : "INVOICE" , style: 'invoiceHeading',border: [false, false, false, false]}]
				 			 				    ]
				 			 			    } 
				 			 			},
				 			 			{
				 			 			    style:'addressTable',
				 			 			    table: {
				 			 			    	widths: [457,60],
				 			 			
				 			 				body: [
				 			 					[
				 			 					    {text:'3355 Benton St. \n Santa Clara CA 95051, USA \n Phone (408) 373-7700 Fax (408) 493-4424',style: 'address',border: [false, false, false, false]},
				                                     {
				                                         border: [false, false, false, false],
				                                         table: {
				                                          body: [
				                                             [{ text : 'NO.' , style: 'numberContent'},{ text : currentInvoice.invId , style: 'numberContent'}]
				                                         ]
				                                     },
				 							}
				 			 					    ]
				 			 				    ]
				 			 			    }
				 			 			},
				                         {
				                              style:'billtoShipToTable',
				                              table: {
				                              	widths: [230,87,230],
				                          
				                          	body: [
				                          		[
				                          		    {text:'BILL TO',style: 'billtoShipToHeader'},
				                          		    {text:'',style: 'billtoShipToHeader', border: [false, false, false, false]},
				                                    {text:'SHIP TO',style: 'billtoShipToHeader'},
				                                    
				                          		    ],
				                          		    [
				                          		    {text:"ATTN NO:"+ wordWrapService.gerWordWrapString( currentInvoice.custName ,32,"\n",true) + ",\n" + wordWrapService.gerWordWrapString(currentInvoice.billTo ,33,"\n",true),style: 'billToShipAddress'},
				                                     {text:'', border: [false, false, false, false]},
				                                     {text: wordWrapService.gerWordWrapString(currentInvoice.shipTo,33,"\n",true) ,style: 'billToShipAddress'},
				                                    ]
				                          	    ]
				                              }
				                          },
				                          {
				                              style:'datePoTable',
				                              table: {
				                              	widths: [60,114,285,80],
				                          
				                          	body: [
				                          		[
				                          		    {text:'DATE',style: 'billtoShipToHeader'},
				                                    {text:'Sales PO. Number',style: 'billtoShipToHeader'},
				                                    {text:'TERMS',style: 'billtoShipToHeader'},
				                                    {text:'REP',style: 'billtoShipToHeader'},
				                          		    ],
				                          		    [
				                          		    {text:$filter('dateFilter')(currentInvoice.invDate),style:"numberContent"},
				                                     {text:wordWrapService.gerWordWrapString(currentInvoice.poNumber,18,"\n",true),style:"numberContent"},
				                                     {text:wordWrapService.gerWordWrapString(currentInvoice.terms,37,"\n",true),style:"numberContent"},
				                                     {text: wordWrapService.gerWordWrapString(currentInvoice.salesRep,13,"\n",true),style:"numberContent"},
				                                 
				                          		    ]
				                          	    ]
				                              }
				                          },
				                          {
				                              style:'qtyItemTable',
				                              table: {
				                              	widths: [25,90,314,50,50],
				                          
				                          	body: convertToPrintableItem(currentInvoice)
				                              }
				                          },
				                            {
				                              style:'commentsTable',
				                              table: {
				                              	widths: [500],
				                          	body: [
				                          		[ { text:'Comments:\n'+ wordWrapService.gerWordWrapString(currentInvoice.comments,75,"\n",true),border: [false,false, false, false],style:"comment"}
				                                 ]
				                          	    ]
				                              }
				                          },
				 			 		   {
				 			 		       style:"qtyItemTable",
				                 			text: [
				                 			{ text: 'Make all checks payable to ', style: 'numberContent'},
				                 				{ text: 'Meshdynamics\n', bold: true, style: 'numberContent' },
				                 				{ text: 'For questions concerning invoice, contact Francis daCosta at +1.408.373.7700 or fdacosta@meshdynamics.com' ,style: 'numberContent'},
				                 			]
				 		                },
				 		                  {
				                              
				                              	style:"wairtransferTable",
				                              table: {
				                              	widths: [278,278],
				                          	body: [
				                          		[ { text:'Wire transfer instructions: \n\n Meshdynamics\n 3355 Benton St. \nSANTA CLARA, CA 95051',style: 'numberContent'},
				                          		  {
				                                         table: {
				                                             	widths: [80,120],
				                                          body: [
				                                             [{ text : 'Bank Name:' , style: 'numberContent',border: [false, false, false, false]},{ text : 'Wells Fargo' , style: 'numberContent',border: [false, false, false, false]}],
				                                             [{ text : 'Bank Address:' , style: 'numberContent',border: [false, false, false, false]},{ text : '2792 Homestead Rd, Santa Clara, CA 95051' , style: 'numberContent',border: [false, false, false, false]}],
				                                             [{ text : 'ABA:' , style: 'numberContent',border: [false, false, false, false]},{ text : '8048831500:' , style: 'numberContent',border: [false, false, false, false]}],
				                                             [{ text : 'Acct No:' , style: 'numberContent',border: [false, false, false, false]},{ text : '8881212', style: 'numberContent',border: [false, false, false, false]}],
				                                              [{ text : 'Acct Name:' , style: 'numberContent',border: [false, false, false, false]},{ text : 'Meshdynamics', style: 'numberContent',border: [false, false, false, false]}]
				                                         ]
				                                     }
				                          		  }
				                          		]
				                          	    ]
				                              }
				                          },
				                          	{ text: 'THANK YOU FOR YOUR BUSINESS!',style:"tankyouHeading"}
				 			 					   
				 			 		],
				 			 		styles: {
				 			 		    logoTable:{
				 			 		         margin: [-40, -20, 0, 10]
				 			 		    },
				 			 		    logo :{
				 			 		        
				 			 		    },
				 			 		    header: {
				 			 		         fontSize: 15,
				 			 		           bold: true,
				 			 		    },
				 			 		    invoiceHeading: { 
				 			 		        fontSize: 25,
				 			 		           bold: true,
				 			 			     margin: [100,-5, 0, 10]
				 			 		    },
				 			 		  
				 			 		     smallBoxContent: {
				 			 		        fontSize: 14,
				 			 		           bold: true,
				 			 			     margin: [0, 10, 0, 0]
				 			 		    },
				 			 		    billtoShipToTable: {
				 			 		        fontSize : 14,
				 			 		         margin: [-30, 20, 0, 10]
				 			 		    },
				 			 		  numberContent: {
				 			 		        fontSize: 9,
				 			 		         margin:[0, 0, 0, 0]
				 			 		    },
				 			 		    address: {
				 			 		        fontSize: 10,
				 			 		         margin:[-45, 0, 0, 10]
				 			 		    },
				 			 		    billToShipAddress : {
				 			 		        fontSize: 10,
				 			 		         margin:[0, 0, 0, 0]
				 			 		    },
				 			 		    addressTable: {
				 			 		        margin: [10, -20, 0, 0]
				 			 		    },
				 			 		    billtoShipToHeader:{
				 			 		        fontSize: 13,
				 			 		           bold: true,
				 			 			     margin: [0, 0, 0, 0]
				 			 		    },
				 			 		    datePoTable :{
				 			 		        margin: [-30,5, 0, 0],
				 			 		    },
				 			 		    wairtransferTable:{
				 			 		        margin: [-30,10, 0, 0],
				 			 		    }
				 			 		    ,
				 			 		    commentsTable :{
				 			 		         margin: [-36,10, 0, 20],
				 			 		    },
				 			 		    qtyItemTable:{
				 			 		        margin: [-30,20, 0, 0]
				 			 		    },
				 			 		    comment :{
				 			 		        italics: true,
				 			 		       
				 			 		    },
				 			 		    priceandamount: {
				 			 		        alignment: 'right',
				 			 		        fontSize: 8
				 			 		    },
				 			 		    itemContent:{
				 			 		        fontSize: 8
				 			 		    },
				 			 		    tankyouHeading:{
				 			 		        fontSize: 7,
				 			 		        margin:[10,5],
				 			 		        bold:true,
				 			 		        alignment: 'center'
				 			 		    },
				 			 		    qtyItemContent :{
				 			 		        alignment: 'justify',
				 			 		        fontSize: 1,
				 			 		        margin: [3,10,10,10]
				 			 		    },
				 			 		    qtyItemTableContent :{
				 			 		         fontSize: 11
				 			 		    }
				 			 		    
				 			 		}
				 			 };
				 var d = new Date();
				 var timestamp = d.getTime();
				pdfMake.createPdf(docDefinition).download("PackingSlip_" + currentInvoice.invId + "_" + timestamp + ".pdf" );
			 },
			 setSerachTypeCriteria : function(selectedShowFilter) {
				 var compareDate ;
				 switch (selectedShowFilter) {
					case "all":
						return null;
					case "open":
						return {key:"status",value:"open"};
					case "issued_this_month":
						return {key:"invDate",value : moment().startOf('month').format('YYYY-MM-DD'),filterType:1};
					case "due_this_month":
						return {key:"dueDate",value : moment().startOf('month').format('YYYY-MM-DD'),filterType:1};
					case "issued_this_year":
						return {key:"invDate",value : moment().startOf('year').format('YYYY-MM-DD'),filterType:2};
					case "due_this_year":
						return {key:"dueDate",value : moment().startOf('year').format('YYYY-MM-DD'),filterType:2};
				}
			 }
		};
		return purchaseOrderService;
	}]);
})();