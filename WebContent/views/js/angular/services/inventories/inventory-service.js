(function() {
	'use strict';
	app.factory('inventoryService',['apiDelegateService','apiHandlerService','appConstants','errorHandlerService',function(apiDelegateService,apiHandlerService,appConstants,errorHandlerService){
		var inventoryService = {
			/*Get raw inventories list*/
			getRawInventoryLists : function() {
				 var options = {
						  url:appConstants.apiUrl + appConstants.restApiUrl.getRawInventoryList,
						  method:'GET'
				  }
				  return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
						return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
				});
			},
			getRawInventoryPaginationList : function(pagination,currentPage,criteria) {
				var count = currentPage * pagination.perPageItemCount - pagination.perPageItemCount + 1;
				 var options = {
						  url : appConstants.apiUrl + appConstants.restApiUrl.getRawInventoryPaginationList+'?start='+count+"&noOfRecords="+ pagination.perPageItemCount,
						  method : 'POST',
						  data : criteria
				  };
				  return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
					  return sucessResponse.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
					});
			},
			/*Add new raw inventories*/
			addRawInventories : function(dataObj,eventName){
				apiHandlerService.addData(dataObj,appConstants.restApiUrl.addRawInventory,'POST',eventName);
			},
			
			/*Update raw inventories*/
			updateRawInventories : function(dataObj,eventName){
				apiHandlerService.updateData(dataObj,appConstants.restApiUrl.updateRawInventory,'PUT',eventName);
			},
			/*Delete raw inventory*/
			deleteRawInventories : function(dataObj,eventName){
				apiHandlerService.deleteData(appConstants.restApiUrl.deleteRawInventory+dataObj.rsku,'DELETE',eventName);
			}
		};
		return inventoryService;
	}]);
})();	