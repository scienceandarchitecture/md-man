(function() {
	'use strict';
	app.factory('inventoryDailogService',['dailogService','inventoryService','apiHandlerService','$rootScope','appConstants',function(dailogService,inventoryService,apiHandlerService,$rootScope,appConstants) {
		var activeAfterCall;
		var currentInventory = null;
		var deleteInventory = null;
		var dialogObject;
		var dailogScope = null;
		function callback(model){
			dailogScope = model;
			model.inventoryFlag = false;
			model.newInventory = {
					rsku:"",
					description:"",
					comment:""
				};
			
			/*Reset Inventory data*/	
			model.resetInventoryData = function(){
				model.newInventory = {
						rsku:"",
						description:"",
						comment:""
					};
				model.inventoryFlag = false;
			}
			
			if(currentInventory != undefined){
				model.inventoryFlag = true;
				model.newInventory = _.clone(currentInventory);
			}
			
			if(deleteInventory != undefined){
				model.title="Are you sure want to delete inventory " + deleteInventory.rsku;
				model.deleteEntry = function(){
					inventoryService.deleteRawInventories(deleteInventory,activeAfterCall);  
				  }
			}
				
		/*Cancel current open Dialog*/
			model.cancel = function() {
				dailogService.closeDailog();
				model.resetInventoryData();
			};
			
			$rootScope.$on('closeDailog',function() {
				 model.cancel();
			 });
			
		/*Add/update Raw inventory*/	
			model.addUpdateInventory = function() {
				if(currentInventory != undefined) {
					inventoryService.updateRawInventories(model.newInventory,activeAfterCall);
		    	} else {
		    		inventoryService.addRawInventories(model.newInventory,activeAfterCall);
		    	}
			}
		}
		return  {
			/*Open add/update inventory dialog*/
			openInventoryDailog : function($event,refreshList,inventoryDetails) {
				activeAfterCall = refreshList;
				currentInventory = inventoryDetails || null;
				dialogObject = {
						targetEvent : $event,
						templateUrl : 'views/partials/dialogs/addInventory.html',
						skipHideValue:true
				};
				dailogService.showDailog(dialogObject,callback);
			},
			
			/*Open delete inventory dialog*/
			openDeleteInventoryDialog : function($event,refreshList,inventoryDetails){
				activeAfterCall = refreshList;
				deleteInventory = inventoryDetails || null;
				dialogObject = {
						targetEvent : $event,
						templateUrl : 'views/partials/dialogs/alertDialog.html',
						skipHideValue:true
				};
				dailogService.showDailog(dialogObject,callback);
			},
			getUpdateFlag : function() {
				return dailogScope.inventoryFlag;
			}
		};
	}]);
})();