(function() {
	'use strict';
	app.service('apiHandlerService',['$rootScope','appConstants','apiDelegateService','MessageService','$mdDialog','errorHandlerService',function($rootScope,appConstants,apiDelegateService,MessageService,$mdDialog,errorHandlerService){
		var _self = this;
		_self.addData = addData;
		_self.updateData = updateData;
		_self.deleteData = deleteData;
		_self.getData = getData;
		
		function getData(restUrl,methodName,eventName,callback){
			var newDataDetails = {
					url:appConstants.apiUrl+restUrl,
					method:methodName
			}
			apiDelegateService.apiHandler(newDataDetails)
			 .then(function(sucessResponse) {
				 	$rootScope.$broadcast(eventName,sucessResponse);
				 	callback(false,sucessResponse);
			    }).catch(function(errorResponse) {
			    	callback(true,errorResponse);
			        errorHandlerService.handleError(errorResponse);
			    });
		}
		
		function addData(dataObj,restUrl,methodName,eventName) {
			var newDataDetails = {
					url:appConstants.apiUrl+restUrl,
					method:methodName,
					data:dataObj	
			}

			$rootScope.pauseEvent = true;
			apiDelegateService.apiHandler(newDataDetails)
			 .then(function( sucessResponse ) {
				 	$rootScope.$broadcast(eventName,sucessResponse);
				 	MessageService.showAlert(sucessResponse.data);
				}).catch(function(errorResponse) {
					errorHandlerService.handleError(errorResponse);
			    }).finally(function() {
			    	$rootScope.pauseEvent = false;
			    });
		}
		
		function updateData( dataObj,restUrl,methodName,eventName) {
			var updateDataDetails = {
					url : appConstants.apiUrl+restUrl,
					method : methodName,
					data : dataObj
			}
			$rootScope.pauseEvent = true;
			apiDelegateService.apiHandler(updateDataDetails)
			 .then(function(sucessResponse) {
				 	$rootScope.$broadcast(eventName,sucessResponse);
				 	MessageService.showAlert(sucessResponse.data);
			 }).catch(function(errorResponse) {
				 	errorHandlerService.handleError(errorResponse);
			    }).finally(function() {
			    	$rootScope.pauseEvent = false;
			    });
		}
		
		function deleteData(restUrl,methodName,eventName){
			var deleteDataDetails = {
					url:appConstants.apiUrl+restUrl,
					method:methodName
			}
			return apiDelegateService.apiHandler(deleteDataDetails)
			 .then(function(sucessResponse) {
				 	$rootScope.$broadcast(eventName,sucessResponse);
				 	MessageService.showAlert(sucessResponse.data);
			 	}).catch(function(errorResponse) {
			 		errorHandlerService.handleError(errorResponse);
			    });
		}
	}]);
})();	