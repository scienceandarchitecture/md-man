(function() {
	'use strict';
	app.factory('errorHandlerService',['authService','appConstants',function(authService,appConstants) {
		return {
			handleError: function(error) {
				if(error.status == appConstants.httpStatusCode.UNAUTHORIZED) {
					authService.logout();
				}
			}
		};
	}]);
})();	