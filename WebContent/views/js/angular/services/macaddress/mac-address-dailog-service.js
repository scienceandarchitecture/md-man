(function() {
	'use strict';
		app.factory('macAddressDailogService',['dailogService','macAddressService','apiHandlerService','$rootScope',function(dailogService,macAddressService,apiHandlerService,$rootScope) {
			let currentMacRange;
			let currentTitle;
			var dialogObject;
			function callback(model) {
				model.title = currentTitle; 
				model.macRange = {}; 
				model.pagination = { perPageItemCount:10, goToLastPage : false };
				/*Cancel currently open dialog*/
				model.cancel = function() {
					dailogService.closeDailog();
				};
				
				model.filters = [
					    {key: 'boardMacAddress', value: 'Board Mac Address'},
					    {key: 'macAddress', value: 'Mac Address'},
				];
				 
				model.selectedFilter = "";
				 
				model.searchText = "";
				model.macAddressCriteria = {};

				/*macAddressService.getMacRange(currentMacRange).then(function(data) {
					model.macRange = data;
				});*/
				
				 model.macAddressCriteria = currentMacRange;
				 model.macAddressCriteria.searchFlag = "";
				 
				 model.getMacRangeList = function(currentPage,setPagination) {
						currentPage = currentPage || 1;
						macAddressService.getMacRangePaginationList(model.pagination,currentPage,model.macAddressCriteria).then(function(response) {
							 model.macRange = response.data;
							 setPagination(response.count,currentPage);
						});
				  }
				 
				 model.setCriteria = function() {
					 if(model.selectedFilter === undefined || model.selectedFilter === "") {
						 model.searchText = "";
						 /*Empty old Object*/
						 model.macAddressCriteria = {};
						 /*Merge data*/
						 model.macAddressCriteria = angular.extend(model.macAddressCriteria,currentMacRange);
					 } else{
						 model.macAddressCriteria = {};
						 /*Empty old Object*/
						 model.macAddressCriteria[model.selectedFilter] = model.searchText;
						 /*Merge data*/
						 model.macAddressCriteria = angular.extend(model.macAddressCriteria,currentMacRange);
						 if(model.selectedFilter == "boardMacAddress") {
							 model.macAddressCriteria.searchFlag = "B";
							 model.macAddressCriteria.macAddress = "";
						 }else {
							 model.macAddressCriteria.searchFlag = "M";
							 model.macAddressCriteria.boardMacAddress = "";
						 }
					 }
					 
					 /*After this send user to first page.*/
					 $rootScope.$broadcast('goToSpecificPageForDailog',1);
				}
				
				$rootScope.$on('closeDailog',function() {
					 model.cancel();
				 });
			}
			
			return  {
				/*Open add invoice payment dialog*/
				openMacRangeDailog : function($event,macRange,title) {
						currentTitle = title;
						
						currentMacRange = macRange;
						dialogObject = {
								targetEvent : $event,
								templateUrl : 'views/partials/dialogs/macRange.html'
						};
						
						dailogService.showDailog(dialogObject,callback);
				},
				
				
			}
		}
	]);
})();