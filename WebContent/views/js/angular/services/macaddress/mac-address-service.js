(function() {	
	'use strict';
	app.factory('macAddressService',['apiDelegateService','apiHandlerService','appConstants','errorHandlerService',function(apiDelegateService,apiHandlerService,appConstants,errorHandlerService){
		var macAddressService =  {
			
			getMacAddressList : function() {
				var options = {
					  url:appConstants.apiUrl+'macaddress/getMacAddressSummary',
					  method:'GET',
				};
				return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
						 return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
				});
			},
			getMacAddressPaginationList : function(pagination,currentPage,criteria) {
					var count = currentPage * pagination.perPageItemCount - pagination.perPageItemCount + 1;
					 var options = {
							  url : appConstants.apiUrl + appConstants.restApiUrl.getmacAddressPaginationList +'?start='+count+"&noOfRecords="+ pagination.perPageItemCount,
							  method : 'POST',
							  data :criteria
					  };
					  return apiDelegateService.apiHandler(options)
					  .then(function(sucessResponse) {
						  return sucessResponse.data;
						}).catch(function(errorResponse) {
							errorHandlerService.handleError(errorResponse);
						});
			},
			getMacRangePaginationList : function(pagination,currentPage,criteria) {
				var count = currentPage * pagination.perPageItemCount - pagination.perPageItemCount + 1;
				var options = {
						  url:appConstants.apiUrl + appConstants.restApiUrl.macAddressRange +'?start='+count+"&noOfRecords="+ pagination.perPageItemCount,
						  method:'POST',
						  data: criteria
					};
				return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
						 return sucessResponse.data;
					}).catch(function(errorResponse) {
						 errorHandlerService.handleError(errorResponse);
				});
			}
		};
		return macAddressService;
	}]);
})();