(function() {
	'use strict';
	app.factory('changePasswordService',['apiDelegateService','apiHandlerService','appConstants','errorHandlerService',function(apiDelegateService,apiHandlerService,appConstants,errorHandlerService){
		return {
			updatePassword : function(dataObj){
				var options = {
						url:appConstants.apiUrl+appConstants.restApiUrl.changePassword,
						method:'PUT',
						data:dataObj
					};
					return apiDelegateService.apiHandler(options)
						.then(function(sucessResponse) {
							return sucessResponse;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
					});
			}
		}
	}]);
})();