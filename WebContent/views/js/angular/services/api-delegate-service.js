(function() {
	'use strict';	
	app.service('apiDelegateService',function($rootScope,$q,$http,$cookies) {
			 var _self = this;
			 _self.apiHandler = apiHandler;
			 
			function apiHandler(option) {
			    var deferred = $q.defer();
			    var currentUser = angular.fromJson($cookies.get("currentUser"));
			    if(currentUser === undefined) {
			    	authService.logout();
			    }
			    option.headers = option.headers || {'Authentication':currentUser.authKey,'Content-Type':'application/json'};
			      $http({
			    	  url : option.url,
			    	  method : option.method,
			    	  data : option.data,
			    	  headers : option.headers
			      }).then(function(res) {
			        	return deferred.resolve(res);
			        },function(err) {
			        	return deferred.reject(err);
			        });
			      return deferred.promise;
			}
	});
})();	