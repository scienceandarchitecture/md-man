(function() {
	'use strict';
	app.factory('productService',['apiDelegateService','apiHandlerService','appConstants','errorHandlerService',function(apiDelegateService,apiHandlerService,appConstants,errorHandlerService){
		var productService =  {
			getProductsList : function(){
				var options = {
						  url:appConstants.apiUrl + appConstants.restApiUrl.getProductList,
						  method:'GET'
				  }
				  return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
						return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
					});
			},
			getProductPaginationList : function(pagination,currentPage,criteria) {
				var count = currentPage * pagination.perPageItemCount - pagination.perPageItemCount + 1;
				 var options = {
						  url : appConstants.apiUrl + appConstants.restApiUrl.getProductPaginationList + '?start='+count+"&noOfRecords="+ pagination.perPageItemCount,
						  method : 'POST',
						  data : criteria
				  };
				  return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
					  return sucessResponse.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
					});
			},
			getSubProductLists : function(){
				var options = {
						  url : appConstants.apiUrl + appConstants.restApiUrl.getSubProductList,
						  method :  'GET'
				  };
				  return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
					  	return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
					});
			},
			/*Add new products*/
			addProducts : function(dataObj,eventName){
				apiHandlerService.addData(dataObj,appConstants.restApiUrl.addProduct,'POST',eventName);
			},
			
			/*Update products*/
			updateProducts : function(dataObj,eventName){
				apiHandlerService.updateData(dataObj,appConstants.restApiUrl.updateProduct,'PUT',eventName);
			},
		
			/*Delete products*/
			deleteProducts : function(dataObj,eventName){
				apiHandlerService.deleteData(appConstants.restApiUrl.deleteProduct+dataObj.psku,'DELETE',eventName);
			}
		};
		return productService;
	}]);
})();