(function() {
	'use strict';
	app.factory('productDailogService',['dailogService','productService','apiHandlerService','$rootScope','inventoryService',function(dailogService,productService,apiHandlerService,$rootScope,inventoryService) {
		var currentProduct = null;	
		var activeAfterCall;
		var deleteProduct = null;
		var dialogObject;
		let dialogScope = null;
		function callback(model) {
				dialogScope = model;
				model.productFlag = false;
				model.addButton = true;
				model.updateButton = false;
				model.productItems = [];
				model.newProduct = {
						psku:"",
						description:"",
						productItems:[]
				};
				
				model.resetProductData = function(){
					model.newProduct = {
							psku:"",
							description:"",
							productItems:[]
					};
					model.productFlag = false;
					model.addButton = true;
					model.updateButton = false;
					model.productItems = [];
				}
				
				
				if(currentProduct != undefined){
					model.productFlag = true;
					model.newProduct = _.clone(currentProduct);
					model.productItems =  _.clone(currentProduct.productItems);
				}
				
				if(deleteProduct != undefined) {
					model.title="Are you sure want to delete product " +  deleteProduct.psku;
					model.deleteEntry = function() {
						productService.deleteProducts(deleteProduct,activeAfterCall);  
					}
				}
				
				model.cancel = function() {
					dailogService.closeDailog();
					model.resetProductData();
				};
				
				$rootScope.$on('closeDailog',function() {
					 model.cancel();
				});
				
				inventoryService.getRawInventoryLists().then(function(rawInventory) {
					model.inventoryItem = rawInventory;
				});
				
				model.addUpdateProduct = function(){
					model.newProduct.productItems = model.productItems;
					if(currentProduct === undefined){
						productService.addProducts(model.newProduct,activeAfterCall);
					}else{
						productService.updateProducts(model.newProduct,activeAfterCall);
					}
				}
				
				model.addItem = function(){
					  if(model.quantity != undefined && model.quantity != "" && model.quantity != null){
					  model.addedItems = true;
					  console.log(model.productItems);
					  if(model.productItems.length === 0){
						  model.productItems.push(
					  				{
					  					'rsku':model.selectedItem,
					  					'quantity':model.quantity
					  				}
					  			);
					  
					  }else{
						  if (isItemNameExits(model.productItems,model.selectedItem)){
							  _.find(model.productItems, function (item) {
								  if(item.rsku === model.selectedItem){
									   item.quantity = +item.quantity + +model.quantity;
									  }
							  });
						  }else{ 
						  model.productItems.push({'rsku':model.selectedItem,'quantity':model.quantity});
					  	} 
					  }
					}
					  model.quantity = '';
					  model.selectedItem = model.inventoryItem[0].rsku;
				  }
	
				  
				  function isItemNameExits(items, inventoryName) {
					  return _.find(items, function (items, index) {
					      return items.rsku == inventoryName ? true : false;
					  });
				  }
			
				  model.editItem = function(index){
					  model.addButton = false;
					  model.updateButton = true;
					  localStorage.setItem("productItemIndex",JSON.stringify(index));
				      model.quantity = model.productItems[index].quantity;
				      model.selectedItem = model.productItems[index].rsku;
				  }
				  
		
				  model.updateItem=function(){
				    	var index=localStorage.getItem('productItemIndex');
				    	model.productItems[index].quantity=model.quantity;
				    	model.productItems[index].rsku=model.selectedItem;
				    	model.addButton=true;
				   	  	model.updateButton=false;
				   	  	model.quantity = '';
				   	  	model.selectedItem = model.inventoryItem[0].rsku;
				   	  	localStorage.removeItem('productItemIndex');
				    }
				  
		
				  model.deleteItem = function (index) {
				    	 model.productItems.splice(index, 1);
				    	 console.log("Deleted Item...."+ model.productItems[index])
				    }
				  
				  
			}
			return  {
				openProductDailog : function($event,refreshList,productDetails) {
					activeAfterCall = refreshList;
					if(productDetails != undefined) {
						currentProduct = productDetails;
					} else {
						currentProduct = undefined;
					}
				
						dialogObject = {
								targetEvent : $event,
								templateUrl : 'views/partials/dialogs/addProduct.html',
								skipHideValue:true
						};
						dailogService.showDailog(dialogObject,callback);
				},
				
				/*Open delete inventory dialog*/
				openDeleteProductDialog : function($event,refreshList,productDetails){
					activeAfterCall = refreshList;
					deleteProduct = productDetails || null;
					dialogObject = {
							targetEvent : $event,
							templateUrl : 'views/partials/dialogs/alertDialog.html',
							skipHideValue:true
					};
					dailogService.showDailog(dialogObject,callback);
				},
				getUpdateFlag : function() {
					//as there is no update scenario we are not maintaining any flag.
					return dialogScope.productFlag;
				}
			};
	}]);
})();