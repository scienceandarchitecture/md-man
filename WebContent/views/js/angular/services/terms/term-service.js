(function() {
	'use strict';
	app.factory('termService',['apiDelegateService','apiHandlerService','appConstants','errorHandlerService',function(apiDelegateService,apiHandlerService,appConstants,errorHandlerService){
		return {
			getTermLists : function(){
				var options = {
						url:appConstants.apiUrl+'terms/getTerms',
						method:'GET'
					}
					return apiDelegateService.apiHandler(options)
						.then(function(sucessResponse) {
							return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
					});
			}
		}
	}]);
})();	