(function() {
	'use strict';
	app.factory('revenueService',['apiDelegateService','apiHandlerService','appConstants','errorHandlerService',function(apiDelegateService,apiHandlerService,appConstants,errorHandlerService){
		var revenueService =  {
				getRevenuePaginationList : function(pagination,currentPage,criteria) {
					var thisCriteria = angular.copy(criteria);
					var count = currentPage * pagination.perPageItemCount - pagination.perPageItemCount + 1;
					var revenueDateFlag = thisCriteria.revenueDateFlag;
					delete thisCriteria.revenueDateFlag;
					 var options = {
							  url : appConstants.apiUrl + appConstants.restApiUrl.getRevenuePaginationList +'?start='+count+"&noOfRecords="+ pagination.perPageItemCount + "&revenueDateFlag="+ revenueDateFlag,
							  method : 'POST',
							  data :thisCriteria
					  };
					  return apiDelegateService.apiHandler(options)
					  .then(function(sucessResponse) {
						  return sucessResponse.data;
						}).catch(function(errorResponse) {
							errorHandlerService.handleError(errorResponse);
						});
				},
				setSerachTypeCriteria : function(selectedShowFilter,selectedMonth) {
					 var currentDate = new Date();
					 switch (selectedShowFilter) {
						case "current_month":
							currentDate.setDate(1);
							return { acknowledged : currentDate.toMysqlFormatForInvoice().split(" ")[0], revenueDateFlag : appConstants.revenueDateFlag.current_month};
						case "month":
							currentDate.setDate(1);
							if( selectedMonth != -1 ) {
								currentDate.setMonth(selectedMonth);
							}
							return { acknowledged : currentDate.toMysqlFormatForInvoice().split(" ")[0], revenueDateFlag : appConstants.revenueDateFlag.any_month};
						case "quarter":
							return { acknowledged : moment().startOf('quarter').format('YYYY-MM-DD'), revenueDateFlag : appConstants.revenueDateFlag.current_quarter};
						case "year":
							return { acknowledged : moment().startOf('year').format('YYYY-MM-DD'), revenueDateFlag : appConstants.revenueDateFlag.current_year};
					}
					 return null;
				 }
		};
		return revenueService;
	}]);
})();