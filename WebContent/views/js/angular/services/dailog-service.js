(function() {
	'use strict';
	app.factory('dailogService',['$mdDialog','$mdMedia',function($mdDialog,$mdMedia) {
		return {
			//TODO: Some scope dependency are present in this method. More detial see in main controller showDailog.Remove those dependencies.
			showDailog : function (dailogObject,callback) {
			    var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
			    let dailog;
		        dailog = $mdDialog.show({
		        	controller: function () { callback(this); },
		        	controllerAs: 'ctrl',
		            parent: angular.element(document.body),
		            clickOutsideToClose: false,
		            fullscreen: useFullScreen,
		            targetEvent : dailogObject.targetEvent,
		            templateUrl : dailogObject.templateUrl,
		            skipHide:dailogObject.skipHideValue
		        }).then(function (answer) {
		        	//handle dialog closing
		        }, function () {
		               
		        });
			},
			closeDailog : function() {
				$mdDialog.cancel();
			}
		}
	}]);
})();