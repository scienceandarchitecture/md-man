(function() {
	'use strict';
	app.service('MessageService', function($mdToast){
	
		  /**
		   * @ngdoc method
		   * @name showAlert
		   * @methodOf Services.messageService
		   * @param {string}
		   *            message Message text
		   */
		    this.showAlert = function(reponse){
		    	let messageObject = getMessage(reponse);
		    	if( !messageObject.error) {
		    		$mdToast.show($mdToast.simple()
			                .content(messageObject.message)
			                .highlightAction(false)
			                .position('right top')
			                .hideDelay(6000)
			        );
		    	} else {
		    		$mdToast.show($mdToast.simple()
			                .content(messageObject.message)
			                .highlightAction(false)
			                .position('right top')
			                .theme("error-toast")
			                .hideDelay(6000)
			        );
		    	}
		    }
		    
		   
		    
		    this.showError =  function(response) {
		    	$mdToast.show($mdToast.simple()
		                .content(getMessage(reponse))
		                .highlightAction(false)
		                .position('right top')
		                .theme("error-toast")
		                .hideDelay(6000)
		        )
		    }
		    
		    this.showCustomAlert = function(response,error) {
		    	if(error) {
		    		 $mdToast.show($mdToast.simple()
				                .content(response)
				                .highlightAction(false)
				                .position('right top')
				                 .theme("error-toast")
				                .hideDelay(6000)
				        );
		    		return;
		    	}
		    	 $mdToast.show($mdToast.simple()
			                .content(response)
			                .highlightAction(false)
			                .position('right top')
			                .hideDelay(6000)
			        )
		    };
		    
		    var getMessage = function(reponse) {
		    	switch(reponse.responseCode) {
		    		case 201:
		    			return {message : reponse.message,error:false};
		    		case 200:
		    			return {message : reponse.message,error:false};
		    		default:
		    			return {message : reponse.message,error:true};
		    			//TODO: Create lookup table for error message.
		    			//return "Internal Server Error. Please try again.";
		    	} 
		    }
	
		});
})();	