(function() {
	'use strict';	
	app.factory( 'authService', ["$rootScope","$http","$cookies","appConstants","MessageService","apiDelegateService","$q","$location","dailogService", function($rootScope,$http,$cookies,appConstants,MessageService,apiDelegateService,$q,$location,dailogService) {
	  var loggedInUser 
	  var isLoggedIn = true;
	  return {
	    logout : function() { 
	    	//TODO Call API for logout.
	    	$cookies.put("currentUser");
	    	dailogService.closeDailog();
	    	$location.path("/login");
	    },
	    login : function(userDetails) {
	    	return $http.post(appConstants.apiUrl + "login/login",userDetails,{headers:{'Content-Type':'application/json'}}).then(function(response,status) {
	    		if( response.data.responseCode == 200 ) {
	    			response.data.data.user.authKey = response.data.data.authkey; 
	    			$cookies.put("currentUser",angular.toJson(response.data.data.user));
	    			return true;
	    		}
	    		MessageService.showAlert(response.data);
	    		return false;
	    	});
	    },
	    isLoggedIn: function() { return isLoggedIn; },
	    authenticate : function() { isLoggedIn = true },
	    currentUser: function() { return angular.fromJson($cookies.get("currentUser")); },
	    forgotPassword : function(data) {
	    	var deferred = $q.defer();
	 	     $http({
	 	    	  url : appConstants.apiUrl + appConstants.restApiUrl.forgotPassword +"?" + "userName=" + data.userName,
	 	    	  method : 'PUT',
	 	    	  data : data,
	 	    	  headers :  {'Authentication':'abc','Content-Type':'application/json'}
	 	     }).then(function(res) {
	 	        	return deferred.resolve(res);
	 	        },function(err) {
	 	        	return deferred.reject(err);
	 	        });
	 	      return deferred.promise;
	    }
	  };
	}]);
})();