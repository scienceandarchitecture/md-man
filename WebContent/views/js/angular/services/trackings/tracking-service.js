(function() {
	'use strict';
	app.factory('trackingService',['apiDelegateService','apiHandlerService','appConstants','errorHandlerService',function(apiDelegateService,apiHandlerService,appConstants,errorHandlerService){
		return {
			getTrakingLists : function(invoiceId){
				var options = {
						  url : appConstants.apiUrl+'tracking/getInvoiceTrackListByInvId?invId='+invoiceId,
						  method : 'GET'
				  }
				  return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
						return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
				});
			}
		}
	}]);
})();	