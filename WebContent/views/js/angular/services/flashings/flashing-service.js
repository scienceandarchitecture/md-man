(function() {
	'use strict';
	app.factory('flashingService',['apiDelegateService','apiHandlerService','appConstants','$window','MessageService','$rootScope','$http','errorHandlerService','jobOrderService',function(apiDelegateService,apiHandlerService,appConstants,$window,MessageService,$rootScope,$http,errorHandlerService,jobOrderService){
		function setDownloadStatus(jobmodelflashdetailsId,visibility,readyToDownload,status) {
			 return {
					 visibility: visibility,	
					 readyToDownload : readyToDownload,
					 status : status
					
			 };
		}
		var flashingServiceObj =  {
			getJobModelFlashDetailsByJobIdAndModelId : function(jobId,jobItemId) {
				var options = {
						  url : appConstants.apiUrl+'jobModelFlash/getJobModelFlashDetailsByJobIdAndModelId?jobId=' + jobId + "&jobItemId=" + jobItemId,
						  method : 'GET'
				};
				return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
						 return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
				});
			},
			addJobModelFlashDetails : function(buildConfugarations) {
				apiHandlerService.addData(buildConfugarations,'jobModelFlash/addJobModelFlashDetails', 'POST', 'getBuildConfugations' );
			},
			addJobModelFlashDetailsList : function(buildConfugarations) {
				apiHandlerService.addData(buildConfugarations,'jobModelFlash/addJobModelFlashDetailsList', 'POST', 'getBuildConfugations' ); 
			},
			updateJobModelFlashDetails : function(buildConfugarations) {
				apiHandlerService.updateData(buildConfugarations,'jobModelFlash/updateJobModelFlashDetails', 'PUT', 'getBuildConfugations' );
			},
			updateJobModelFlashDetailsAfterDownload : function(buildConfugarations) {
				var options = {
						url : appConstants.apiUrl + 'jobModelFlash/updateJobModelFlashDetails',
						method : 'PUT',
						data : buildConfugarations
					};
				apiDelegateService.apiHandler(options)
				.then(function(sucessResponse) {
					$rootScope.$broadcast('getBuildConfugations');
					return sucessResponse.data.data;
				}).catch(function(errorResponse) {
					 errorHandlerService.handleError(errorResponse);
				});
			},
			getDynamicBuildURL: function() {
				var options = {
						url : appConstants.apiUrl + appConstants.restApiUrl.loadDynamicBuildURL,
						method:'GET'
					};
				return apiDelegateService.apiHandler(options)
				.then(function(sucessResponse) {
					return sucessResponse.data.data;
				}).catch(function(errorResponse) {
					 errorHandlerService.handleError(errorResponse);
				});
			},
			downloadImage : function(configuration,donwloadURL) {
				var headerAndPostParams = configuration.downloadLink.split(",");
				return $http({
				        url: donwloadURL,
						method : 'POST',
				        responseType: "blob",
				        headers : {'Authentication' : headerAndPostParams[0],
							  'Content-Type' : 'application/json',
							  'Accept': 'application/octet-stream'
						  },
						  data : {
							  uniqueId: headerAndPostParams[1],
							  imageName: headerAndPostParams[2],
							  clientMacAddress: headerAndPostParams[3]
						  }
				  }).then(function (sucessResponse) {
					  if( sucessResponse.data != "" && sucessResponse.data.responseCode === undefined  ) {
							var blob = new Blob([sucessResponse.data], { type: "application/octet-stream" });
					        saveAs(blob, headerAndPostParams[2]);
							MessageService.showCustomAlert("Image Downloaded Successfully.",false);
							return true;
						}
						MessageService.showCustomAlert("Image is removed from server. Please rebuild image.",true);
						return false;
				  }, function (response) {
					 if(response !== undefined && response.status == -1 && response.statusText == "") {
						 $window.open(donwloadURL, '_blank');
					 }
					 return false;
				  });
			},
			rebuildImage : function(configuration) {
				var options = {
						  url : appConstants.apiUrl + appConstants.restApiUrl.rebuildImage,
						  method : 'PUT',
						  data : configuration
					};
				return apiDelegateService.apiHandler(options).then(function(sucessResponse) {
					if(sucessResponse.data.responseCode == 201 || sucessResponse.data.responseCode == 200) {
						MessageService.showCustomAlert("Build regenerated Successfully.",false);
						return true;
					} else {
						MessageService.showCustomAlert("Failed to rebuild image.Contact Adminstrator.",true);
					}
				}).catch(function(errorResponse) {
					 errorHandlerService.handleError(errorResponse);
				});
			},
			readHeaderExcel : function(workbook) {
				var firstSheetName = workbook.SheetNames[0];
				var flashingJobDetails = {};
				var desired_cell;
				var keys = [{actualKey:"jobId",excelKey:'A2'},{actualKey:"purchaseOrderId",excelKey:'B2'},{actualKey:"psku",excelKey:'C2'},{actualKey:"VLAN",excelKey:'D2'},{actualKey:"APP",excelKey:'E2'},{actualKey:"quantity",excelKey:'F2'},{actualKey:"fwversion",excelKey:'G2'}];
				/* Get worksheet */
				var worksheet = workbook.Sheets[firstSheetName];
	
				/* Find desired cells */
				for(var i = 0; i < keys.length; i++ ) {
					desired_cell = worksheet[keys[i]['excelKey']];
					flashingJobDetails[keys[i]['actualKey']] = (desired_cell ? desired_cell.v : undefined);
				}
				/* Get the value */
				return flashingJobDetails;
			},
			readFalshingListFromExcel: function(workbook,quantity) {
				var firstSheetName = workbook.SheetNames[1];
			
				var desired_cell;
				
				/* Get worksheet */
				var worksheet = workbook.Sheets[firstSheetName];
				
				var flashingJson = XLS.utils.sheet_to_json(worksheet, {header: 1});
				console.log(flashingJson);
				
				/*Delete empty arrays*/
				flashingJson = flashingJson.filter(function (element) {
					return element.length !== 0;
				});
				
				var flashingJobList = [];
				
				/* If rows provided are more than required.*/
				if(flashingJson.length != (quantity + 1) ) {
					return false;
				}
				for(var i = 0; i < quantity; i++ ) {
					/* Put on some validation 
					 * Throw error if item given is less than quantity
					 **/
					if(flashingJson[i+1].length > 2) {
						return false;
					}
					if( flashingJson[i+1][0] !== undefined  && flashingJson[i+1][1] !== undefined) {
						flashingJobList.push({"serialNo": flashingJson[i+1][0] ,"boardMacAddress" :  flashingJson[i+1][1]});
					} else {
						return false;
					}
				}
				
				return flashingJobList;
			},
			isJobOrderValid : function(jobId) {
				return jobOrderService.getJobOrderById(jobId).then(function(jobOrder) {
					return jobOrder;
				});
			},
			isJobItemValid : function(jobItems,flashingJobDetails) {
				var flag = false;
				for(var i = 0; i < jobItems.length; i++ ) {
					if( jobItems[i].psku == flashingJobDetails.psku && jobItems[i].quantity == flashingJobDetails.quantity ) {
						//If given produuct have no VLAN/APP
						if( jobItems[i].jobItemSubProducts.length == 0 && flashingJobDetails.VLAN == 0 && flashingJobDetails.APP == 0 ) {
							flag = true;
						}
					
						for( var j = 0; j < jobItems[i].jobItemSubProducts.length; j++ ) {
							if( flashingJobDetails[[jobItems[i].jobItemSubProducts[j].invoiceItemSubProductList[0].subproductName]] != jobItems[i].jobItemSubProducts[j].invoiceItemSubProductList[0].quantity ) {
								flag = false;
								break;
							} else {
								flag = true;
							}
						}
						if(flag) {
							return jobItems[i];
						}
					}
				}
				return false;
			}
			,
			isFirmwareVersionValid : function(fwLookUp,fwversion) {
				if(fwLookUp.indexOf(fwversion) !== -1 ) {
					return true;
				}
				 return false;
			},
			getDownloadStatus : function(flashingItem) {
				switch(flashingItem.status) {
				/*Open*/
				case "1": 
					return setDownloadStatus(flashingItem.jobmodelflashdetailsId,true,false,flashingItem.status);
					/*IN Progress*/
				case "2":
					return setDownloadStatus(flashingItem.jobmodelflashdetailsId,false,false,flashingItem.status);
				/*Ready to Download*/
				case "3":
					return setDownloadStatus(flashingItem.jobmodelflashdetailsId,true,true,flashingItem.status);
					/*Download Complete*/
				case "4":
					return setDownloadStatus(flashingItem.jobmodelflashdetailsId,true,true,flashingItem.status);
					/*Rebuild Image*/
				case "5":
					return setDownloadStatus(flashingItem.jobmodelflashdetailsId,true,false,flashingItem.status);
				}
			}
		};
		return flashingServiceObj;
	}]);
})();