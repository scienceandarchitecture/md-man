(function() {
	'use strict';	
	/**
	 * Created by Benison pc on 9/11/2016.
	 */
	app.factory('routeNavigation',["$rootScope", "$location","$cookies","authService", function ($rootScope, $location,$cookies,authService) {
	
	    $rootScope.$location = $location;
	    $rootScope.keys = Object.keys;
	
	    var routes = []; 
	    var currentUser = authService.currentUser();
	    
	    var views = angular.fromJson(currentUser.views);
	    
	  /*  angular.forEach($route.routes, function (route, path) {
	    	if (route.name) {
	        	  if(views.indexOf(route.name) !== -1) {
	        		routes.push({
		                path: path,
		                name: route.name,
		                icon:route.icon
	        			});
	        	}	
	        }
	    });*/
	    return {
	        routes : routes,
	        currentUser : currentUser,
	        activeRoute : function (route) {
	            return route.path === $location.path();
	        }
	    };
	}]);
})();
