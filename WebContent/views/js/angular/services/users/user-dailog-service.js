(function() {
	'use strict';
	app.factory('userDailogService',['dailogService','userService','apiHandlerService','$rootScope','authService','appConstants',function(dailogService,userService,apiHandlerService,$rootScope,authService,appConstants) {
			let currentUser;
			//Event we want to hire after  add/update is done
			var postUpdateEvent;
			var paymentsDetail;
			var currentUserDetails;
			var deleteUserDetails = null;
			let modelScope = null;
			function callback(model) {
				modelScope = model;
				model.newUser = {
						userName: "",
						password: "",
						fullName: "",
						roles: "",
						views: "",
						email: ""
				};
				model.userFlag = false;
				model.roles = ["Admin","Normal User"];
				
				model.views = ["Purchase Order","Payments","Inventory","Revenue","Products","Job Orders","Ship Orders","PO Summary","Image Generation","Manage User"];
				
				model.resetUserDetails = function() {
					model.newUser = {
							userName: "",
							password: "",
							fullName: "",
							roles: "",
							views: "",
							email: ""
					};
					model.userFlag = false;
				} 
				if(currentUserDetails != undefined){
					model.userFlag = true;
					model.newUser = _.clone(currentUserDetails);
					model.newUser.views = _.clone(angular.fromJson(currentUserDetails.views));
				}
				
				
				
				if( deleteUserDetails != null ){
					model.title="Are you sure want to delete user " + deleteUserDetails.userName;
					model.deleteEntry = function(){
						userService.deleteUserRole(deleteUserDetails,postUpdateEvent).then(function() {
							deleteUserDetails = null;
						});  
					  }
					
				}

				model.addUser = function(){
					model.newUser.views = angular.toJson(model.newUser.views);
					if(currentUserDetails != undefined){
						delete model.newUser.password;
						userService.updateUserRole(model.newUser,postUpdateEvent);
					}else{
						userService.addUserRole(model.newUser,postUpdateEvent);
					}
				};
				
				model.isManageUserDisabled = function(view) {
					return view ==='Manage User' && model.newUser.roles ==='Normal User';
				}
				
				model.flushViews = function() {
					model.newUser.views = [];
				}
				
				/*Cancel currently open dialog*/
				model.cancel = function() {
					dailogService.closeDailog();
					model.resetUserDetails();
				};
				
				
				$rootScope.$on('closeDailog',function() {
					 model.cancel();
				 });
			}
			
			return  {
				/*Open add user dialog*/
				openUserDailog : function($event,updateEvent,userDetails) {
					postUpdateEvent = updateEvent;
					currentUserDetails = userDetails || null;
					var dialogObject = {
							targetEvent : $event,
							templateUrl : 'views/partials/dialogs/addUser.html'
					};
					dailogService.showDailog(dialogObject,callback);
				},
				openDeleteUserDialog : function( $event , updateEvent , userDetail ) {
					postUpdateEvent = updateEvent;
					deleteUserDetails = userDetail || null;
					var dialogObject = {
							targetEvent : $event,
							templateUrl : 'views/partials/dialogs/alertDialog.html'
					};
					dailogService.showDailog(dialogObject,callback);
				},
				getUpdateFlag : function() {
					//as there is no update scenario we are not maintaining any flag.
					return modelScope.userFlag;
				},
				resetViewsArray : function() {
					modelScope.newUser.views = _.clone(angular.fromJson(modelScope.newUser.views));
				}
			}
		}
	]);
})();	