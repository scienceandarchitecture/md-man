(function() {
	'use strict';
	app.factory('userService',['apiDelegateService','apiHandlerService','appConstants','errorHandlerService',function(apiDelegateService,apiHandlerService,appConstants,errorHandlerService){
		return {
			/*Get users list*/
			getUserLists : function(){
				var options = {
						url : appConstants.apiUrl + 'userRole/getUsers',
						method:'GET'
					};
					return apiDelegateService.apiHandler(options)
						.then(function(sucessResponse) {
							return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
					});
			},
			getUserPaginationList : function(pagination,currentPage,criteria) {
				var count = currentPage * pagination.perPageItemCount - pagination.perPageItemCount + 1;
				 var options = {
						  url : appConstants.apiUrl + appConstants.restApiUrl.getUserPaginationList +'?start='+count+"&noOfRecords="+ pagination.perPageItemCount,
						  method : 'POST',
						  data :criteria
				  };
				  return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
					  return sucessResponse.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
					});
			},
			/*Add User roles*/
			addUserRole : function(dataObj,eventName){
				apiHandlerService.addData(dataObj,appConstants.restApiUrl.addUser,'POST',eventName);
			},
			
			/*Update User roles*/
			updateUserRole : function(dataObj,eventName) {
				apiHandlerService.updateData(dataObj,appConstants.restApiUrl.updateUser,'PUT',eventName);
			},
			
			/*Delete raw inventory*/
			deleteUserRole : function(dataObj,eventName){
				return apiHandlerService.deleteData(appConstants.restApiUrl.deleteUser+dataObj.userName,'DELETE',eventName);
			}
			
		}
	}]);
})();