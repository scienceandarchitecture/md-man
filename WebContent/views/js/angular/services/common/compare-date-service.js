(function() {
	'use strict';
	app.service('compareDateService',function(){
		return {
			compareDate: function(firstDate,secondDate,type) {
			switch (type) {
				case "month":
					if(firstDate.getFullYear() == secondDate.getFullYear() && firstDate.getMonth() == secondDate.getMonth()) {
						return true;
					}	 
					return false;
				case "year":
					if(firstDate.getFullYear() == secondDate.getFullYear()) {
						return true;
					}	 
					return false;
				case "quarter":	
				/*Write Logic for quarter */
					return true;
			}
		}
		}
	});
})();