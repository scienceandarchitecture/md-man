(function() {
	'use strict';
	app.service('wordWrapService',function(){
		function identity(str) {
			  return str;
			}
		var wordWrapService =  {
			gerWordWrapString: function(str, width, brk, cut) {
				brk = brk || 'n';
				    width = width || 75;
				    cut = cut || false;
				 
				    if (!str) { return str; }
				 
				    var regex = '.{1,' +width+ '}($)' + (cut ? '|.{' +width+ '}|.+$' : '|\S+?(\s|$)');
				    /*If there is already new line than for temporary purpose just replace them with some other character*/
				    str = str.replace(/\n/g,"|");
				    
				    str = str.match( RegExp(regex, 'g') ).join( brk );
				   return str.replace(/\|/g,"\n");
			}
		}
		return wordWrapService;
		
	});
})();