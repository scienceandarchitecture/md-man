(function() {
	'use strict';
	app.service('validateResponseService',['appConstants',function(appConstants){
		return {
			isUpdateSuccessful: function(responseCode) {
				if(responseCode == appConstants.responseCode.OK || responseCode == appConstants.responseCode.CREATED) {
					return true;
				}
				return false;
			}
		}
	}]);
})();