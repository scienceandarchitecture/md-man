(function() {
	'use strict';
	app.factory('jobOrderService',['apiDelegateService','apiHandlerService','appConstants','errorHandlerService',function(apiDelegateService,apiHandlerService,appConstants,errorHandlerService){
		var jobOrderService = {
			getJobOrderList : function() {
				var options = {
					  url:appConstants.apiUrl+appConstants.restApiUrl.jobOrderList,
					  method:'GET',
				};
				return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
						 return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
				});
			},
			getJobOrderPaginationList : function(pagination,currentPage,criteria) {
				var count = currentPage * pagination.perPageItemCount - pagination.perPageItemCount + 1;
				 var options = {
						  url : appConstants.apiUrl + appConstants.restApiUrl.getjobOrderPaginationList +'?start='+count+"&noOfRecords="+ pagination.perPageItemCount,
						  method : 'POST',
						  data :criteria
				  };
				  return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
					  return sucessResponse.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
					});
			},
			getJobOrderById : function(jobId) {
				var options = {
						  url : appConstants.apiUrl+'jobOrder/getJobOrderById?jobId='+ jobId,
						  method : 'GET'
				};
				return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
						 return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						 errorHandlerService.handleError(errorResponse);
				});
			},
			saveJobOrder : function( newJob,isUpdate ) {
				if( isUpdate ) {
		    		apiHandlerService.addData( newJob , 'jobOrder/updateJobOrder', 'PUT', 'getJobOrders' );
		    	} else {
		    		apiHandlerService.addData( newJob , 'jobOrder/addJobOrders', 'POST', 'getJobOrders' );
		    	}
			},
			getJobOrderByPurchaseOrderId : function(purchaseOrderId) {
				var options = {
						  url : appConstants.apiUrl+'jobOrder/getJobOrderListByCriteria',
						  method : 'POST',
						  data : {
							  invId:purchaseOrderId
						  }
				};
				return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
						 return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						 errorHandlerService.handleError(errorResponse);
				});
			},
			getJobOrderByCriteria : function(criteria) {
				var options = {
						  url : appConstants.apiUrl+'jobOrder/getJobOrderListByCriteria',
						  method : 'POST',
						  data : criteria
				};
				return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
						 return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						 errorHandlerService.handleError(errorResponse);
				});
			},
			getJobOrderListById : function(jobOrderId) {
				var options = {
						  url : appConstants.apiUrl + appConstants.restApiUrl.getJobOrderListById + "?jobId="+ jobOrderId,
						  method : 'GET'
				};
				return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
						 return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						 errorHandlerService.handleError(errorResponse);
				});
			}
		}
		return jobOrderService;
	}]);
})();	