(function() {
	'use strict';
	app.factory('jobOrderDailogService',['dailogService','jobOrderService','apiHandlerService','$rootScope',function(dailogService,jobOrderService,apiHandlerService,$rootScope) {
		var currentPurchaseOrderId = null;	
		var activeAfterCall;
		var dialogObject;
		function callback(model) {
			model.jobOrderList = [];
			model.purchaseOrderId = currentPurchaseOrderId;
			if(currentPurchaseOrderId != null ){
				jobOrderService.getJobOrderByPurchaseOrderId(currentPurchaseOrderId).then(function(data) {
					model.jobOrderList = data;
				});
				
			}
			model.cancel = function() {
				dailogService.closeDailog();
			};
		}
		
		return  {
			openJobOrderByPurchaseOrderIdDialog : function($event,purchaseOrderId) {
				currentPurchaseOrderId = purchaseOrderId || null;
				dialogObject = {
						targetEvent:$event,
						templateUrl:'views/partials/dialogs/showJobOrder.html'
				}
				dailogService.showDailog(dialogObject,callback);
			}
		};
	}]);
})();	