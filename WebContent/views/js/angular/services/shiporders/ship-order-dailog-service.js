(function() {
	'use strict';
	app.factory('shipOrderDailogService',['dailogService','apiHandlerService','$rootScope','authService','appConstants',function(dailogService,apiHandlerService,$rootScope,authService,appConstants) {
			var currentShipOrder;
			var activateAfterRefresh;
			var dailogObject;
			let dailogScope = null;
			function callbackAfterAcknowledged(model) {
				 model.shipOrder = angular.copy(currentShipOrder);
			
				 model.cancel = function() {
						dailogService.closeDailog();
				 };
					
				 model.updateShipOrder = function() {
						apiHandlerService.addData(model.shipOrder,'ships/updateShipItems', 'PUT', 'refeshShipList' );
				 }
				 
				   model.minimumFinishBy =  new Date.createFromMysql(model.shipOrder.acknowledged);
				   
				   model.maximumFinishBy = new Date();
				   
				   model.shipOrder.finished = new Date.createFromMysql(model.shipOrder.acknowledged);
				   
				   model.shipOrder.state = appConstants.shipstatus.finished; 
				   
				   model.heading = "Finish Ship Order";
				
				   model.title = "Enter Finish date";
			}
			
			return  {
					/*Open add invoice payment dialog*/
					openFinishShipOrderDailog : function($event,shipOrder) {
						currentShipOrder = shipOrder;
						dailogObject = {
									targetEvent : $event,
							        templateUrl : 'views/partials/dialogs/alertDailogFinish.html',
					    };
						dailogService.showDailog(dailogObject,callbackAfterAcknowledged);
					}
			}
		}
	]);
})();
	