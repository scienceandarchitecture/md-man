(function() {
	'use strict';
	app.factory('shipOrderService',['apiDelegateService','apiHandlerService','appConstants','errorHandlerService',function(apiDelegateService,apiHandlerService,appConstants,errorHandlerService){
		var shipOrderService =  {
				getShippedOrderLists : function(invoiceId){
					var options = {
						url:appConstants.apiUrl+'ships/getShippingItemByInvoiceId?invId='+invoiceId,
				method:'GET'
					};
				return apiDelegateService.apiHandler(options)
				  .then(function(sucessResponse) {
						return sucessResponse.data.data;
					}).catch(function(errorResponse) {
						errorHandlerService.handleError(errorResponse);
					});
				},
				getShipOrderPaginationList : function(pagination,currentPage,criteria) {
					var count = currentPage * pagination.perPageItemCount - pagination.perPageItemCount + 1;
					 var options = {
							  url : appConstants.apiUrl + appConstants.restApiUrl.getshipOrderPaginationList +'?start='+count+"&noOfRecords="+ pagination.perPageItemCount,
							  method : 'POST',
							  data :criteria
					  };
					  return apiDelegateService.apiHandler(options)
					  .then(function(sucessResponse) {
						  	return sucessResponse.data;
						}).catch(function(errorResponse) {
							errorHandlerService.handleError(errorResponse);
						});
				}		
		};
		return shipOrderService;
	}]);
})();