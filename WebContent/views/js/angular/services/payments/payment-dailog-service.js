(function() {
	'use strict';
	app.factory('paymentDailogService',['dailogService','paymentService','apiHandlerService','$rootScope','authService','customerService',function(dailogService,paymentService,apiHandlerService,$rootScope,authService,customerService) {
			var currentInvoice;
			var activateAfterRefresh;
			var paymentsDetail;
			let dialogObject;
			let dailogScope = null;
			function callback(model) {
				dailogScope = model;
				model.todaysDate = new Date();
				model.todayDate = new Date();
				model.newInvoicePayment = {
						invId :"",
						rpId :"",
						notes :"",
						paysDate:new Date()
				};
				model.customerList = [];
				
				model.newPayment = {
						amount:"",
						notes:"",
						state:"open",
						created:new Date(),
						createdBy: authService.currentUser().userName,
						closedBy: authService.currentUser().userName
					};
	
				/*Reset Data*/
				model.resetPaymentData = function(){
					model.newPayment = {
						amount:"",
						notes:"",
						state:"open",
						created:new Date()
					};
				};
				 model.customerAutocompleteData = {
						 selectedItem:"",
						 customers : []
				 };
				/*Get open raw payment lists*/
				if(currentInvoice != undefined ) {
					paymentService.getOpenRawPaymentListByCustomerId(currentInvoice.custId).then(function(openPaymentList) {
						model.openPaymentList = openPaymentList;
					});
				}
				
				function loadCustomers() {
			         var customersForAutoComplete = model.customerList.map( function (customer) {
			        	return {
			               value : customer,
			               display : customer.name
			            };
			         });
			         model.customerAutocompleteData.customers = customersForAutoComplete;
			         return customersForAutoComplete;
			    }
				
				
			//filter function for search query
		     function createFilterFor(query) {
		        var lowercaseQuery = angular.lowercase(query);
		        return function filterFn(customer) {
		            return (customer.value.name.toLowerCase().indexOf(lowercaseQuery) === 0);
		         };
		     }	
			 
			  model.querySearch = function(query) {
				  return customerService.getCustomerListsByName(query).then(function(customerList) {
						model.customerList = customerList;
						return loadCustomers();
						
					});
			 }
				/*Add Payment*/
				model.addPayment = function(){
					model.newPayment.custId =  model.newPayment.customer.value.custId;
					apiHandlerService.addData(model.newPayment,'payment/addRawPayment','POST',activateAfterRefresh);
				};
				
				if( paymentsDetail != undefined ) {
					model.title="Are you sure want to delete payment "+paymentsDetail.rpId;
					model.deleteEntry = function() {
						apiHandlerService.deleteData('payment/deleteRawPaymentById?rpId='+paymentsDetail.rpId, 'DELETE',activateAfterRefresh);  
					 };
				};
				
				/*Add invoice payment*/
				model.addInvoicePayment = function() {
					model.newInvoicePayment.invId = currentInvoice.invId;
					model.newInvoicePayment.rpId = model.newInvoicePayment.selectedPayment.rpId;
					model.newInvoicePayment.notes = model.newInvoicePayment.selectedPayment.notes;
					model.newInvoicePayment.payDate = model.newInvoicePayment.paysDate.toMysqlFormat();
					apiHandlerService.addData(model.newInvoicePayment,'payment/addInvoicePayment', 'POST','refreshAfterAddPayment');
				};
				
				/*Cancel currently open dialog*/
				model.cancel = function() {
					dailogService.closeDailog();
				};
				
				model.setAmount = function() {
					model.newInvoicePayment.amount = model.newInvoicePayment.selectedPayment.amount;
					console.log(model.newInvoicePayment);
				};
				
				$rootScope.$on('closeDailog',function() {
					 model.cancel();
				 });
			}
			
			return  {
				/*Open add invoice payment dialog*/
				openPaymentDailog : function($event,invoice) {
						
						currentInvoice = invoice;
						dialogObject = {
								targetEvent : $event,
								templateUrl : 'views/partials/dialogs/addPaymentFromInvoive.html'
						};
						dailogService.showDailog(dialogObject,callback);
				},
				
				/*Open add payment dialog*/
				openAddPaymentDialog : function($event,refreshPaymentLists){
					activateAfterRefresh = refreshPaymentLists;
					dialogObject = {
							targetEvent : $event,
							templateUrl : 'views/partials/dialogs/addPayment.html'
					};
					dailogService.showDailog(dialogObject,callback);
				},
				
				/*Open delete payment dialog*/
				openDeletePaymentDialog : function($event, refreshPaymentLists,paymentDetails){
					activateAfterRefresh = refreshPaymentLists;
					paymentsDetail = paymentDetails;
					 dialogObject = {
								targetEvent:$event,
								templateUrl:'views/partials/dialogs/alertDialog.html'
						};
					 dailogService.showDailog(dialogObject,callback);
				},
				getUpdateFlag : function() {
					//as there is no update scenario we are not maintaining any flag.
					return false;
				}
			}
		}
	]);
})();
	