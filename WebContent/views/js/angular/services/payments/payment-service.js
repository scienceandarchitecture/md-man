(function() {
	'use strict';
	app.factory('paymentService',['apiDelegateService','apiHandlerService','appConstants','errorHandlerService',function(apiDelegateService,apiHandlerService,appConstants,errorHandlerService){
		var paymentService = {
				paymentByInvoiceId : function(invoiceId) {
					 var options = {
							  url : appConstants.apiUrl+'payment/getInvoicePaymentByInvoiceId?invId=' + invoiceId,
							  method : 'GET'
					  };
					  apiDelegateService.apiHandler(options)
					  .then(function(sucessResponse) {
							return sucessResponse.data.data;
					   }).catch(function(errorResponse) {
						   errorHandlerService.handleError(errorResponse);
					   });
				},
				getRawPaymentList : function() {
					 var options = {
							  url:appConstants.apiUrl + 'payment/getRawPaymentList',
							  method:'GET'
					  }
					  return apiDelegateService.apiHandler(options)
					  .then(function(sucessResponse) {
							return sucessResponse.data.data;
						}).catch(function(errorResponse) {
							errorHandlerService.handleError(errorResponse);
						});
				},
				getPaymentListWithPagination : function(pagination,currentPage,criteria) {
					var count = currentPage * pagination.perPageItemCount - pagination.perPageItemCount + 1;
					 var options = {
							  url : appConstants.apiUrl + 'payment/getPaymentListUsingPagination?start='+count+"&noOfRecords="+ pagination.perPageItemCount,
							  method : 'POST',
							  data:criteria
					  };
					  return apiDelegateService.apiHandler(options)
					  .then(function(sucessResponse) {
						  return sucessResponse.data;
						}).catch(function(errorResponse) {
							errorHandlerService.handleError(errorResponse);
						});
				},
				getOpenRawPaymentListByCustomerId : function(customerId) {
					 var options = {
							  url : appConstants.apiUrl + 'payment/getOpenRawPaymentList?custId=' + customerId ,
							  method:'GET'
					  }
					  return apiDelegateService.apiHandler(options)
					  .then(function(sucessResponse) {
							return sucessResponse.data.data;
						}).catch(function(errorResponse) {
							errorHandlerService.handleError(errorResponse);
						});
				},
				getInvoicePaymentLists : function(invoiceId) {
					 var options = {
							  url : appConstants.apiUrl+'payment/getInvoicePaymentByInvoiceId?invId=' + invoiceId,
							  method : 'GET'
					  };
					  return apiDelegateService.apiHandler(options)
					  .then(function(sucessResponse) {
							return sucessResponse.data.data;
						}).catch(function(errorResponse) {
							errorHandlerService.handleError(errorResponse);
					});
				},
				 setSerachTypeCriteria : function(selectedShowFilter) {
				 var compareDate ;
				 switch (selectedShowFilter) {
					case "all":
						return null;
					case "open":
						return {key:"state",value:"open"};
					case "this_month":
						return {key:"created",value : moment().startOf('month').format('YYYY-MM-DD')};
					case "this_year":
						return {key:"created",value : moment().startOf('year').format('YYYY-MM-DD')};
				}
				 }
			};
			return paymentService;
		}
	]);
})();