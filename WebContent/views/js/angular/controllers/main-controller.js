(function() {
	'use strict';
app.controller('mdCtrl', ['$scope', '$mdBottomSheet', '$mdSidenav', '$mdDialog', '$mdMedia', '$rootScope', '$location','apiDelegateService','appConstants','authService', function ($scope, $mdBottomSheet, $mdSidenav, $mdDialog, $mdMedia, $rootScope, $location,apiDelegateService,appConstants,authService) {
   
	/*$rootScope.updateDetails = false;*/
	//$rootScope.addDetails = true;

	$scope.dateFormat = 'MM-dd-yyyy';
	
	$rootScope.pauseEvent = false;
	
    $scope.myDate = new Date();
    $scope.minDate = new Date(
        $scope.myDate.getFullYear(),
        $scope.myDate.getMonth() - 2,
        $scope.myDate.getDate());
    $scope.maxDate = new Date(
        $scope.myDate.getFullYear(),
        $scope.myDate.getMonth() + 2,
        $scope.myDate.getDate());
    $scope.onlyWeekendsPredicate = function (date) {
        var day = date.getDay();
        return day === 0 || day === 6;
    };

    /*$scope.menu4 = [
        {
            link: '',
            title: 'Account Settings',
            icon: 'settings',
            path: ''
        },
        {
            link: '',
            title: 'Logout',
            icon: 'power_settings_new',
            path: ''
        }
    ];*/
    
    	
   
    $scope.arrowDown = true;
    $scope.resetSideMenu = function(){
    	 $scope.arrowDown = true;
    }
    $scope.resetSideMenu();
    $scope.toggleMenu = function () {
        $scope.arrowDown = !$scope.arrowDown;
    };

    /*Scope JobOrder*/
    
    
    $scope.shows = [
        {id: 1, name: 'Open'},
        {id: 2, name: 'Issued This Mounth'},
        {id: 3, name: 'Due This Mounth'},
        {id: 4, name: 'Issued This Year'},
        {id: 4, name: 'Due This Year'},
        {id: 4, name: 'All'}
    ];
    $scope.selectedShow = {id: 1, name: 'Open'};

    $scope.filters = [
        {id: 1, name: 'None'},
        {id: 2, name: 'By Customer'},
        {id: 3, name: 'By SalesPerson'},
        {id: 4, name: 'By Invoice Number'}
    ];
    $scope.selectedFilter = {id: 1, name: 'None'};

    //***Add Invoices
    $scope.sales = [
        {id: 1, name: 'Anat Krispin'},
        {id: 2, name: 'Issued This Mounth'},
        {id: 3, name: 'Due This Mounth'},
        {id: 4, name: 'Issued This Year'},
        {id: 4, name: 'Due This Year'},
        {id: 4, name: 'All'}
    ];
    $scope.salesRep = {id: 1, name: 'Anat Krispin'};


    $scope.sales = [
        "Anat Krispin",
        "medium (14-inch)",
        "large (16-inch)",
        "insane (42-inch)"
    ];


    

    /* Add Invoice*/
    /*$scope.showAddInvoice = function (ev) {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: invoiceController,
            templateUrl: 'views/partials/dialogs/addInvoice.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
            fullscreen: useFullScreen
        }).then(function (answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });
        $scope.$watch(function () {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function (wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });
    };*/

  //*****Open Payment from Invoice
    $scope.showaddPaymentFromInvoive = function(ev) {
  		var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
  		$mdDialog.show({
  		  controller: DialogController,
  		  templateUrl: 'views/partials/dialogs/addPaymentFromInvoive.html',
  		  parent: angular.element(document.body),
  		  targetEvent: ev,
  		  clickOutsideToClose: true,
  		  fullscreen: useFullScreen
  		})
  		.then(function(answer) {
  		  $scope.status = 'You said the information was "' + answer + '".';
  		}, function() {
  		  $scope.status = 'You cancelled the dialog.';
  		});
  		$scope.$watch(function() {
  		  return $mdMedia('xs') || $mdMedia('sm');
  		}, function(wantsFullScreen) {
  		  $scope.customFullscreen = (wantsFullScreen === true);
  		});
     };  
    
    //*****Open Payment from Invoice End
     

     //*****Shipped Items from Invoice
     $scope.showShippedItemsFromInvoive = function(ev) {
   		var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
   		$mdDialog.show({
   		  controller: DialogController,
   		  templateUrl: 'views/partials/dialogs/shippedItemsFromInvoive.html',
   		  parent: angular.element(document.body),
   		  targetEvent: ev,
   		  clickOutsideToClose: true,
   		  fullscreen: useFullScreen
   		})
   		.then(function(answer) {
   		  $scope.status = 'You said the information was "' + answer + '".';
   		}, function() {
   		  $scope.status = 'You cancelled the dialog.';
   		});
   		$scope.$watch(function() {
   		  return $mdMedia('xs') || $mdMedia('sm');
   		}, function(wantsFullScreen) {
   		  $scope.customFullscreen = (wantsFullScreen === true);
   		});
      };  
     //*****Shipped Items from Invoice End
      
    //*****Ship Orders from Invoice
      $scope.showShipOrderFromInvoive = function(ev) {
    		var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
    		$mdDialog.show({
    		  controller: DialogController,
    		  templateUrl: 'views/partials/dialogs/shipOrderFromInvoive.html',
    		  parent: angular.element(document.body),
    		  targetEvent: ev,
    		  clickOutsideToClose: true,
    		  fullscreen: useFullScreen
    		})
    		.then(function(answer) {
    		  $scope.status = 'You said the information was "' + answer + '".';
    		}, function() {
    		  $scope.status = 'You cancelled the dialog.';
    		});
    		$scope.$watch(function() {
    		  return $mdMedia('xs') || $mdMedia('sm');
    		}, function(wantsFullScreen) {
    		  $scope.customFullscreen = (wantsFullScreen === true);
    		});
       };  
      //*****Shipped Items from Invoice End
       
     //*****Payments From Invoive
       $scope.showPaymentsFromInvoive = function(ev,invoiceDetails) {
    	   localStorage.setItem("InvoiceDetailsForPayment",JSON.stringify(invoiceDetails));
     		var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
     		$mdDialog.show({
     		  controller: invoiceController,
     		  templateUrl: 'views/partials/dialogs/paymentsFromInvoive.html',
     		  parent: angular.element(document.body),
     		  targetEvent: ev,
     		  clickOutsideToClose: true,
     		  fullscreen: useFullScreen
     		})
     		.then(function(answer) {
     		  $scope.status = 'You said the information was "' + answer + '".';
     		}, function() {
     		  $scope.status = 'You cancelled the dialog.';
     		});
     		$scope.$watch(function() {
     		  return $mdMedia('xs') || $mdMedia('sm');
     		}, function(wantsFullScreen) {
     		  $scope.customFullscreen = (wantsFullScreen === true);
     		});
        };  
       //*****Payments From Invoive End
        
      //*****Tracking From Invoive
        $scope.showTrackingFromInvoive = function(ev) {
      		var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
      		$mdDialog.show({
      		  controller: DialogController,
      		  templateUrl: 'views/partials/dialogs/trackingFromInvoive.html',
      		  parent: angular.element(document.body),
      		  targetEvent: ev,
      		  clickOutsideToClose: true,
      		  fullscreen: useFullScreen
      		})
      		.then(function(answer) {
      		  $scope.status = 'You said the information was "' + answer + '".';
      		}, function() {
      		  $scope.status = 'You cancelled the dialog.';
      		});
      		$scope.$watch(function() {
      		  return $mdMedia('xs') || $mdMedia('sm');
      		}, function(wantsFullScreen) {
      		  $scope.customFullscreen = (wantsFullScreen === true);
      		});
         };  
        //*****Tracking From Invoive End
         
       //*****Acknowledge
         $scope.showAcknowledge = function(ev) {
       		var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
       		$mdDialog.show({
       		  controller: DialogController,
       		  templateUrl: 'views/partials/dialogs/shipOrdersAknowledge.html',
       		  parent: angular.element(document.body),
       		  targetEvent: ev,
       		  clickOutsideToClose: true,
       		  fullscreen: useFullScreen
       		})
       		.then(function(answer) {
       		  $scope.status = 'You said the information was "' + answer + '".';
       		}, function() {
       		  $scope.status = 'You cancelled the dialog.';
       		});
       		$scope.$watch(function() {
       		  return $mdMedia('xs') || $mdMedia('sm');
       		}, function(wantsFullScreen) {
       		  $scope.customFullscreen = (wantsFullScreen === true);
       		});
          };  
         //*****Acknowledge
         
         //*****Add Tracking 
         $scope.showAddTracking = function(ev) {
       		var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
       		$mdDialog.show({
       		  controller: DialogController,
       		  templateUrl: 'views/partials/dialogs/shipOrdersAddTracking.html',
       		  parent: angular.element(document.body),
       		  targetEvent: ev,
       		  clickOutsideToClose: true,
       		  fullscreen: useFullScreen
       		})
       		.then(function(answer) {
       		  $scope.status = 'You said the information was "' + answer + '".';
       		}, function() {
       		  $scope.status = 'You cancelled the dialog.';
       		});
       		$scope.$watch(function() {
       		  return $mdMedia('xs') || $mdMedia('sm');
       		}, function(wantsFullScreen) {
       		  $scope.customFullscreen = (wantsFullScreen === true);
       		});
          };  
         //*****Add Tracking End
         
         //*****Scan Shipment 
         $scope.showScanShipment = function(ev) {
       		var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
       		$mdDialog.show({
       		  controller: DialogController,
       		  templateUrl: 'views/partials/dialogs/shipOrdersScanShipment.html',
       		  parent: angular.element(document.body),
       		  targetEvent: ev,
       		  clickOutsideToClose: true,
       		  fullscreen: useFullScreen
       		})
       		.then(function(answer) {
       		  $scope.status = 'You said the information was "' + answer + '".';
       		}, function() {
       		  $scope.status = 'You cancelled the dialog.';
       		});
       		$scope.$watch(function() {
       		  return $mdMedia('xs') || $mdMedia('sm');
       		}, function(wantsFullScreen) {
       		  $scope.customFullscreen = (wantsFullScreen === true);
       		});
          };  
         //*****Scan Shipment End
         
         
         //*****Packaging Slip
         $scope.showPackagingSlip = function(ev) {
       		var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
       		$mdDialog.show({
       		  controller: DialogController,
       		  templateUrl: 'views/partials/dialogs/shipOrdersPackagingSlip.html',
       		  parent: angular.element(document.body),
       		  targetEvent: ev,
       		  clickOutsideToClose: true,
       		  fullscreen: useFullScreen
       		})
       		.then(function(answer) {
       		  $scope.status = 'You said the information was "' + answer + '".';
       		}, function() {
       		  $scope.status = 'You cancelled the dialog.';
       		});
       		$scope.$watch(function() {
       		  return $mdMedia('xs') || $mdMedia('sm');
       		}, function(wantsFullScreen) {
       		  $scope.customFullscreen = (wantsFullScreen === true);
       		});
          };  
         //*****Packaging Slip End
    //Payment

   

    //Revenues
    $scope.revenues = [
        {id: 1, name: 'None'},
        {id: 2, name: 'By Customer'},
        {id: 3, name: 'By SalesPerson'}
    ];
    $scope.revenueFilter = {id: 1, name: 'None'};


    $scope.revShows = [
        {id: 1, opt: 'Corrent Month'},
        {id: 2, opt: 'Month'},
        {id: 3, opt: 'Quarter'},
        {id: 4, opt: 'Year'}
    ];
    $scope.revenueShow = {id: 1, opt: 'Corrent Month'};

    /*Show Add Inventory*/
    $scope.showAddInventory = function (ev,inventory) {
    	localStorage.setItem("oldInventoryDetails",JSON.stringify(inventory));
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: inventoryController,
            templateUrl: 'views/partials/dialogs/addInventory.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
            fullscreen: useFullScreen
        })
            .then(function (answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });
        $scope.$watch(function () {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function (wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });
    };
    
/*Update Inventory*/
   /* $scope.showUpdateInventory = function (ev,oldInventory) {
    	localStorage.setItem("oldInventoryDetails",JSON.stringify(oldInventory));
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: inventoryController,
            templateUrl: 'views/partials/dialogs/updateInventory.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: useFullScreen
        })
            .then(function (answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });
        $scope.$watch(function () {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function (wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });
    };*/

    /*Show Scroll bar*/
    $scope.$on('scrollbar.show', function () {
        console.log('Scrollbar show');
    });

    /*Hide Scroll bar*/
    $scope.$on('scrollbar.hide', function () {
        console.log('Scrollbar hide');
    });

    /*Show Add Payment*/
    $scope.showAddPayment = function (ev) {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: paymentController,
            templateUrl: 'views/partials/dialogs/addPayment.html',
            //parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
            fullscreen: useFullScreen
        })
            .then(function (answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });
        $scope.$watch(function () {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function (wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });
    };

    /*Show Add Product*/
    $scope.showAddProduct = function (ev,product) {
    	localStorage.setItem("oldProductDetails",JSON.stringify(product));
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: productController,
            templateUrl: 'views/partials/dialogs/addProduct.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
            fullscreen: useFullScreen
        })
            .then(function (answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });
        $scope.$watch(function () {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function (wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });
    };

    /*Show Add Forecast*/
    $scope.showAddForecast = function (ev,forecast) {
    	localStorage.setItem("oldForecastDetails",JSON.stringify(forecast))
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: forecastController,
            templateUrl: 'views/partials/dialogs/addForecast.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
            fullscreen: useFullScreen
        })
            .then(function (answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });
        $scope.$watch(function () {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function (wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });
    };

    /*Show Add Job Order
    $scope.showAddJobOrder = function (ev) {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'views/partials/dialogs/addJobOrder.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
            fullscreen: useFullScreen
        })
            .then(function (answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });
        $scope.$watch(function () {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function (wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });
    };*/

    /*Show Add Ship Order*/
    $scope.showAddShipOrder = function (ev) {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: shipMasterlController,
            templateUrl: 'views/partials/dialogs/addShipOrder.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: useFullScreen
        })
            .then(function (answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });
        $scope.$watch(function () {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function (wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });
    };

    /*View MAC address range*/
    $scope.showRange = function (ev) {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'views/partials/dialogs/macRange.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
            fullscreen: useFullScreen
        })
            .then(function (answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });
        $scope.$watch(function () {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function (wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });
    };
    $scope.alert = '';
    $scope.showListBottomSheet = function ($event) {
        $scope.alert = '';
        $mdBottomSheet.show({
            template: '<md-bottom-sheet class="md-list md-has-header"> <md-subheader>Settings</md-subheader> <md-list> <md-item ng-repeat="item in items"><md-item-content md-ink-ripple flex class="inset"> <a flex aria-label="{{item.name}}" ng-click="listItemClick($index)"> <span class="md-inline-list-icon-label">{{ item.name }}</span> </a></md-item-content> </md-item> </md-list></md-bottom-sheet>',
            controller: 'ListBottomSheetCtrl',
            targetEvent: $event
        }).then(function (clickedItem) {
            $scope.alert = clickedItem.name + ' clicked!';
        });
    };

    $scope.showAdd = function (ev) {
        $mdDialog.show({
            controller: DialogController,
            template: '<md-dialog aria-label="Mango (Fruit)"> <md-content class="md-padding"> <form name="userForm"> <div layout layout-sm="column"> <md-input-container flex> <label>First Name</label> <input ng-model="user.firstName" placeholder="Placeholder text"> </md-input-container> <md-input-container flex> <label>Last Name</label> <input ng-model="theMax"> </md-input-container> </div> <md-input-container flex> <label>Address</label> <input ng-model="user.address"> </md-input-container> <div layout layout-sm="column"> <md-input-container flex> <label>City</label> <input ng-model="user.city"> </md-input-container> <md-input-container flex> <label>State</label> <input ng-model="user.state"> </md-input-container> <md-input-container flex> <label>Postal Code</label> <input ng-model="user.postalCode"> </md-input-container> </div> <md-input-container flex> <label>Biography</label> <textarea ng-model="user.biography" columns="1" md-maxlength="150"></textarea> </md-input-container> </form> </md-content> <div class="md-actions" layout="row"> <span flex></span> <md-button ng-click="answer(\'not useful\')"> Cancel </md-button> <md-button ng-click="answer(\'useful\')" class="md-primary"> Save </md-button> </div></md-dialog>',
            targetEvent: ev
        })
            .then(function (answer) {
                $scope.alert = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.alert = 'You cancelled the dialog.';
            });
    };
    
    
    /*Show Add Job Order*/
    $scope.showDailog = function ($dailogObject) {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        
        $mdDialog.show({
        	controller: function () { this.parent = $dailogObject.scope; },
        	controllerAs: 'ctrl',
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            fullscreen: useFullScreen,
            targetEvent : $dailogObject.targetEvent,
            templateUrl : $dailogObject.templateUrl,
        }).then(function (answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.status = 'You cancelled the dialog.';
        });
        
        $scope.$watch(function () {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function (wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });
    };
    
    
    /*Convert date from MySQL format to Date Object*/
	Date.createFromMysql = function(mysql_string) { 
	   var t, result = null;
	   
	   if( typeof mysql_string === 'string' ) {
	      t = mysql_string.split(/[- :]/);

	      //when t[3], t[4] and t[5] are missing they defaults to zero
	      result = new Date(t[0], t[1] - 1, parseInt(t[2]));     
	      //Change any any time offset issue.
	      result =  new Date( result.getTime() + Math.abs(result.getTimezoneOffset()*60000) )  ;
	   }
	   return result;
	}
	
	/**
	 * You first need to create a formatting function to pad numbers to two digits…
	 **/
	function twoDigits(d) {
	    if(0 <= d && d < 10) return "0" + d.toString();
	    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
	    return d.toString();
	}

	/**
	 * …and then create the method to output the date string as desired.
	 * Some people hate using prototypes this way, but if you are going
	 * to apply this to more than one Date object, having it as a prototype
	 * makes sense.
	 **/
	Date.prototype.toMysqlFormat = function() {
	    return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
	};
	
	Date.prototype.toMysqlFormatForInvoice = function() {
	    return this.getFullYear() + "-" + twoDigits(1 + this.getMonth()) + "-" + twoDigits(this.getDate()) + " " + twoDigits(this.getHours()) + ":" + twoDigits(this.getMinutes()) + ":" + twoDigits(this.getSeconds());
	};
	
	
	/*Helper to compare two dates by month or year*/
	$scope.compareDate = function(firstDate,secondDate,type) {
		switch (type) {
			case "month":
				if(firstDate.getFullYear() == secondDate.getFullYear() && firstDate.getMonth() == secondDate.getMonth()) {
					return true;
				}	 
				return false;
			case "year":
				if(firstDate.getFullYear() == secondDate.getFullYear()) {
					return true;
				}	 
				return false;
			case "quarter":	
				/*Write Logic for quarter */
				return true;
		}
	}
	
	$scope.refreshList = function(options,listname) {
		  apiDelegateService.apiHandler(options)
		  .then(function(sucessResponse) {
				$scope[listname] = sucessResponse.data.data;
			}).catch(function(errorResponse) {
		         console.log(errorResponse);
		});
	};
	
	$scope.getRawInventoryList = function() {
		  var options = {
				  url:appConstants.apiUrl+'inventories/getRawInventories',
				  method:'GET'
		  }
		  apiDelegateService.apiHandler(options)
		  .then(function(sucessResponse) {
			  	$scope.inventoryItem = sucessResponse.data.data;
				$scope.selectedItem = $scope.inventoryItem[0].rsku;
		  }).catch(function(errorResponse) {
			  console.log(errorResponse);
		});
	 };
		 $scope.navigateTo = function(menuitem) {
		
		 $location.path(menuitem.path);
	 }    
	 
	 $scope.logout = function() {
			authService.logout();
	 }
	 
	 
	 $scope.checkResponseCode = function( responsecode ) {
			switch( responsecode ) {
				case 201:
					 $rootScope.$broadcast('closeDailog');
					break;
				case 200:
					 $rootScope.$broadcast('closeDailog');
					break;
			}
	 }
}]);

	app.controller('ListBottomSheetCtrl', function ($scope, $mdBottomSheet) {
	    $scope.items = [
	        {name: 'Share', icon: 'share'},
	        {name: 'Upload', icon: 'upload'},
	        {name: 'Copy', icon: 'copy'},
	        {name: 'Print this page', icon: 'print'},
	    ];
	
	    $scope.listItemClick = function ($index) {
	        var clickedItem = $scope.items[$index];
	        $mdBottomSheet.hide(clickedItem);
	    };
	});

	function DialogController($scope, $mdDialog) {
	    $scope.hide = function () {
	        $mdDialog.hide();
	    };
	    $scope.cancel = function () {
	        $mdDialog.cancel();
	    };
	    $scope.answer = function (answer) {
	        $mdDialog.hide(answer);
	    };
	    
	};



	app.directive('userAvatar', function () {
	    return {
	        replace: true,
	        template: '<svg class="user-avatar" viewBox="0 0 128 128" height="64" width="64" pointer-events="none" display="block" > <path fill="#FF8A80" d="M0 0h128v128H0z"/> <path fill="#FFE0B2" d="M36.3 94.8c6.4 7.3 16.2 12.1 27.3 12.4 10.7-.3 20.3-4.7 26.7-11.6l.2.1c-17-13.3-12.9-23.4-8.5-28.6 1.3-1.2 2.8-2.5 4.4-3.9l13.1-11c1.5-1.2 2.6-3 2.9-5.1.6-4.4-2.5-8.4-6.9-9.1-1.5-.2-3 0-4.3.6-.3-1.3-.4-2.7-1.6-3.5-1.4-.9-2.8-1.7-4.2-2.5-7.1-3.9-14.9-6.6-23-7.9-5.4-.9-11-1.2-16.1.7-3.3 1.2-6.1 3.2-8.7 5.6-1.3 1.2-2.5 2.4-3.7 3.7l-1.8 1.9c-.3.3-.5.6-.8.8-.1.1-.2 0-.4.2.1.2.1.5.1.6-1-.3-2.1-.4-3.2-.2-4.4.6-7.5 4.7-6.9 9.1.3 2.1 1.3 3.8 2.8 5.1l11 9.3c1.8 1.5 3.3 3.8 4.6 5.7 1.5 2.3 2.8 4.9 3.5 7.6 1.7 6.8-.8 13.4-5.4 18.4-.5.6-1.1 1-1.4 1.7-.2.6-.4 1.3-.6 2-.4 1.5-.5 3.1-.3 4.6.4 3.1 1.8 6.1 4.1 8.2 3.3 3 8 4 12.4 4.5 5.2.6 10.5.7 15.7.2 4.5-.4 9.1-1.2 13-3.4 5.6-3.1 9.6-8.9 10.5-15.2M76.4 46c.9 0 1.6.7 1.6 1.6 0 .9-.7 1.6-1.6 1.6-.9 0-1.6-.7-1.6-1.6-.1-.9.7-1.6 1.6-1.6zm-25.7 0c.9 0 1.6.7 1.6 1.6 0 .9-.7 1.6-1.6 1.6-.9 0-1.6-.7-1.6-1.6-.1-.9.7-1.6 1.6-1.6z"/> <path fill="#E0F7FA" d="M105.3 106.1c-.9-1.3-1.3-1.9-1.3-1.9l-.2-.3c-.6-.9-1.2-1.7-1.9-2.4-3.2-3.5-7.3-5.4-11.4-5.7 0 0 .1 0 .1.1l-.2-.1c-6.4 6.9-16 11.3-26.7 11.6-11.2-.3-21.1-5.1-27.5-12.6-.1.2-.2.4-.2.5-3.1.9-6 2.7-8.4 5.4l-.2.2s-.5.6-1.5 1.7c-.9 1.1-2.2 2.6-3.7 4.5-3.1 3.9-7.2 9.5-11.7 16.6-.9 1.4-1.7 2.8-2.6 4.3h109.6c-3.4-7.1-6.5-12.8-8.9-16.9-1.5-2.2-2.6-3.8-3.3-5z"/> <circle fill="#444" cx="76.3" cy="47.5" r="2"/> <circle fill="#444" cx="50.7" cy="47.6" r="2"/> <path fill="#444" d="M48.1 27.4c4.5 5.9 15.5 12.1 42.4 8.4-2.2-6.9-6.8-12.6-12.6-16.4C95.1 20.9 92 10 92 10c-1.4 5.5-11.1 4.4-11.1 4.4H62.1c-1.7-.1-3.4 0-5.2.3-12.8 1.8-22.6 11.1-25.7 22.9 10.6-1.9 15.3-7.6 16.9-10.2z"/> </svg>'
	    };
	});

	app.config(function ($mdThemingProvider) {
	    var customBlueMap = $mdThemingProvider.extendPalette('light-blue', {
	        'contrastDefaultColor': 'light',
	        'contrastDarkColors': ['50'],
	        '50': 'ffffff'
	    });
	    $mdThemingProvider.definePalette('customBlue', customBlueMap);
	    $mdThemingProvider.theme('default')
	        .primaryPalette('customBlue', {
	            'default': '500',
	            'hue-1': '50'
	        })
	        .accentPalette('pink');
	    $mdThemingProvider.theme('input', 'default')
	        .primaryPalette('grey')
	}); 
})();	