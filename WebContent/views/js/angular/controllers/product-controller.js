(function() {
	'use strict';
	app.controller('productCtrl',['$rootScope','$scope','$mdDialog','$mdMedia','apiDelegateService','appConstants','MessageService','apiHandlerService','productService','productDailogService','validateResponseService',function($rootScope,$scope,$mdDialog,$mdMedia,apiDelegateService,appConstants,MessageService,apiHandlerService,productService,productDailogService,validateResponseService){
		
		$scope.totalRecords = null;
		
		$scope.criteria = {psku : ""};
		
		$scope.pagination = { perPageItemCount:10, goToLastPage : false};
		/*Get product list*/	
		$scope.getProductList = function(currentPage,setPagination) {
			  currentPage = currentPage || 1;
			  productService.getProductPaginationList($scope.pagination,currentPage,$scope.criteria).then(function(response) {
					//Get the total count from the server
				  	$scope.productList  =  response.data;
				  	$scope.totalRecords = response.count;
				  	setPagination(response.count,currentPage);
			  });
		};
		
		/*Refresh Product List*/	  
		  $scope.$on('refreshProductList',function(event,data) { 
			  if( productDailogService.getUpdateFlag() ) {
				  $rootScope.$broadcast("refreshPage");
			  } else {
					if(!validateResponseService.isUpdateSuccessful(data.data.responseCode)) {
						  return false;
					}
				  	if( parseInt($scope.totalRecords) % parseInt($scope.pagination.perPageItemCount) === 0 ) { 
						$rootScope.$broadcast("goToSpecificPage",Math.ceil( $scope.totalRecords / $scope.pagination.perPageItemCount ) + 1 );
					} else {
						$rootScope.$broadcast("goToLastPage");
					}
			  }
			  $scope.checkResponseCode(data.data.responseCode);
		  });
		  
		  $scope.$on('refreshAfterDelete',function(event,data) {
				 if(!validateResponseService.isUpdateSuccessful(data.data.responseCode)) {
					  return false;
				}
				if(parseInt($scope.totalRecords) % parseInt($scope.pagination.perPageItemCount) === 1) { 
					$rootScope.$broadcast("goToSpecificPage",Math.ceil( $scope.totalRecords / $scope.pagination.perPageItemCount ) - 1 );
				} else {
					$rootScope.$broadcast("refreshPage");
				}
			$scope.checkResponseCode(data.data.responseCode);
		});
		  
		  /*Show Add/Update Product Dialog */
		  $scope.showProductDialog = function($event,product) {
				productDailogService.openProductDailog($event,'refreshProductList',product);
		  };
		  
		  /*Delete Product*/
		  $scope.deleteInventoryDialog = function($event,productDetails) {
			productDailogService.openDeleteProductDialog($event,'refreshAfterDelete',productDetails);
		  }
		  
			$scope.setCriteria = function() {
				 $rootScope.$broadcast('goToSpecificPage',1);
			 }
	}]);
})();