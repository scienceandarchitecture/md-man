(function() {
	'use strict';
app.controller('jobOrderCtrl',
['$rootScope','$scope','$mdDialog','$mdMedia','appConstants','apiDelegateService','apiHandlerService','authService','MessageService','jobOrderService','purchaseOrderService','validateResponseService',function ($rootScope,$scope,$mdDialog,$mdMedia,appConstants,apiDelegateService,apiHandlerService,authService,MessageService,jobOrderService,purchaseOrderService,validateResponseService) {
	/*Get All list and populate the table*/
	$scope.jobOrder = {};
	var dailogObject;
	/*For Add Item Vars Starts*/
	
	$scope.jobItems;
	$scope.update;
	$scope.purchaseOrders = [];
	$scope.minimumNeedBy;
	
	$scope.totalRecords = null;
	

	
	$scope.pagination = { perPageItemCount:10, goToLastPage : false };
	
	/*Defualt object for init */
	$scope.init = function() {
		$scope.update = false;
		$scope.newJob = {
				state : appConstants.jobstatus.created,
				ackBy : authService.currentUser().userName,
				created : (new Date()).toMysqlFormat(),
				createdBy : authService.currentUser().userName,
				invId : "",
				jobItems : [],
				model : "",
				quantity : 0,
				needBy : "",
				needByJSDate : new Date()
		};
		$scope.minimumNeedBy = new Date();
		$scope.jobItems = [];
	}
	 
	$scope.formValid = true;
	/* $scope.validJobItemQuantity = 20; */
	 $scope.cancel = function () {
	        $mdDialog.cancel();
	        //Reset all data
	        $scope.init();
	  };
		$scope.criterias = [{key:{state:"created"},value:"Relevant"},{key:{},value:"All"},{key:{state:"finished"},value:"Finished"}];
		
		$scope.selectedCriteria = {};
	$scope.$on('getJobOrders',function(event,data) {   	
		if($scope.update) {
			$rootScope.$broadcast("refreshPage");
		} else {
			if(!validateResponseService.isUpdateSuccessful(data.data.responseCode)) {
				  return false;
			}
			if( parseInt($scope.totalRecords) % parseInt($scope.pagination.perPageItemCount) === 0 ) { 
				$rootScope.$broadcast("goToSpecificPage",Math.ceil( $scope.totalRecords / $scope.pagination.perPageItemCount ) + 1 );
			} else {
				$rootScope.$broadcast("goToLastPage");
			}
		}
		$scope.cancel();
	});
	
	$scope.$on('refreshAfterDelete',function(event,data) {
		if(!validateResponseService.isUpdateSuccessful(data.data.responseCode)) {
			  return false;
		}
		if(parseInt($scope.totalRecords) % parseInt($scope.pagination.perPageItemCount) === 1) { 
			$rootScope.$broadcast("goToSpecificPage",Math.ceil( $scope.totalRecords / $scope.pagination.perPageItemCount ) - 1 );
		} else {
			$rootScope.$broadcast("refreshPage");
		}
		$scope.checkResponseCode(data.data.responseCode);
	});
	  
	$scope.getJobOrderList = function(currentPage,setPagination) {
		currentPage = currentPage || 1;
		jobOrderService.getJobOrderPaginationList($scope.pagination,currentPage,$scope.selectedCriteria).then(function(response) {
			 $scope.jobOrderList = response.data;
			 $scope.totalRecords = response.count;
			 setPagination(response.count,currentPage);
		});
	}
	
	
	$scope.purhaseOrderAutocompleteData = {
			 selectedItem:"",
			 purchaseOrders : []
	 };
	
	function loadPurchaseorders() {
       var purhaseOrderForAutoComplete = $scope.invoiceList.map( function (purchaseOrder) {
      	return {
             value : purchaseOrder,
             display : "" + purchaseOrder.invId
          };
       });
       $scope.purhaseOrderAutocompleteData.purchaseOrders = purhaseOrderForAutoComplete;
       return purhaseOrderForAutoComplete;
    }
		
	$scope.querySearch = function(query) {
		return purchaseOrderService.getPurchaseOrderById(query).then(function(purchaseOrders) {
			if( typeof purchaseOrders == "string" || typeof purchaseOrders == "undefined" ) {
				return [];
			}
			$scope.invoiceList = purchaseOrders;
			return loadPurchaseorders();
		});
	}

	$scope.openJobOrder = function($event,currentJobOrder) {
		$scope.minimumNeedBy = null;
		$scope.jobItems = [];
		$scope.init();
		if(currentJobOrder !== undefined ) {
			purchaseOrderService.getSinglePurchaseOrderById(currentJobOrder.invId).then(function(purchaseOrder) {
				/*Get the customer From API and  set it on purchaseorder object*/
					$scope.purhaseOrderAutocompleteData.purchaseOrders = [{
						value : purchaseOrder,
			             display : "" + purchaseOrder.invId
				    }];
					$scope.update = true;
					$scope.newJob = _.clone(currentJobOrder);
					$scope.newJob.invoice = $scope.purhaseOrderAutocompleteData.purchaseOrders[0];
					$scope.newJob.needByJSDate = new Date.createFromMysql(currentJobOrder.needBy);
					$scope.minimumNeedBy = Date.createFromMysql(currentJobOrder.created);
					$scope.setInvoiceitems(currentJobOrder);
					//TODO: Simple workaround find better approach
					dailogObject = {
							targetEvent : $event,
					        templateUrl : 'views/partials/dialogs/addJobOrder.html',
					        scope: $scope
						};
						$scope.showDailog(dailogObject);
					return;
				});
			return;
		}
		dailogObject = {
			targetEvent : $event,
	        templateUrl : 'views/partials/dialogs/addJobOrder.html',
	        scope: $scope
		};
		$scope.showDailog(dailogObject);
	}
	
    $scope.addJobOrder = function() {
    	/*Copy it to local variable so that UI should not get blank.*/
    	var tempNewJob = angular.copy($scope.newJob);
    	tempNewJob.jobItems = $scope.jobItems;
    	if(tempNewJob.invoice !== undefined) {
    		tempNewJob.invId = tempNewJob.invoice.value.invId;
        	delete tempNewJob.invoice;
        }
    	if(tempNewJob.needByJSDate !== undefined) {
    		tempNewJob.needBy = tempNewJob.needByJSDate.toMysqlFormat();
    		delete tempNewJob.needByJSDate;
    	}
    	jobOrderService.saveJobOrder( tempNewJob , $scope.update);
    }
    
	  $scope.$on('closeDailog',function() {
			 $scope.cancel();
	  });
	  
	   $scope.deleteOrder = function($event,jobOrder) {
			dailogObject = {
				targetEvent : $event,
				templateUrl : 'views/partials/dialogs/alertDialogNew.html',
			    scope : $scope
			};
			$scope.title = "Are you sure want to delete Job Order " + jobOrder.jobId;
			
			$scope.deleteEntry = function() {
				apiHandlerService.deleteData('jobOrder/deleteJobOrderByJobId?jobId=' + jobOrder.jobId, 'DELETE','refreshAfterDelete');  
			}
		   $scope.showDailog(dailogObject);
	   }
	   
	   //Under construction.
	   function sanitizeObject(object,metaData) {
		   for(index in metaData) {
			   console.log(data);
		   }
	   }
	   
	   $scope.acknowledgeOrder = function($event,jobOrder) {
		   /*
		    * 1)Put the record in job order.
		    * 2)Add save method to template.
		    * 
		    * */
		   $scope.newJob = _.clone(jobOrder);
		    
		   $scope.update = true;
		   
		   $scope.newJob.created = new Date.createFromMysql($scope.newJob.created);
		   
		   if($scope.newJob.acknowledged != "") {
			   $scope.newJob.acknowledged = new Date.createFromMysql($scope.newJob.acknowledged);
		   } else {
			   $scope.newJob.acknowledged = new Date();
		   }
		   
		   $scope.newJob.ackBy = authService.currentUser().userName;
		   
		   $scope.newJob.state = appConstants.jobstatus.acknowledged; 
		   
		   $scope.jobItems = _.clone($scope.newJob.jobItems);
		   
		   console.log(angular.toJson($scope.newJob));
		   
		   dailogObject = {
					targetEvent : $event,
			        templateUrl : 'views/partials/dialogs/alertDialogNotify.html',
			        scope : $scope
			};
			
		   $scope.title = "Please add the date for Notification.";
			
		   $scope.showDailog(dailogObject);
		   
	   }
	   
	   $scope.finishOrder = function($event,jobOrder) {
		   /*
		    * 1)Put the record in job order.
		    * 2)Add save method to template.
		    * 
		    */
		   $scope.newJob = _.clone(jobOrder);
		   
		   $scope.newJob.created = new Date.createFromMysql($scope.newJob.created);
		   
		   $scope.newJob.acknowledged = new Date.createFromMysql($scope.newJob.acknowledged);
		   
		   $scope.newJob.finished = $scope.newJob.acknowledged;
		   
		   /* change status finished */
		   $scope.newJob.state = appConstants.jobstatus.finished;
		   
		   $scope.jobItems = _.clone($scope.newJob.jobItems);
		   
		   $scope.update = true;
		   
		   dailogObject = {
					targetEvent : $event,
			        templateUrl : 'views/partials/dialogs/alertDialogComplete.html',
			        scope : $scope
			};
			
			$scope.title = "Please add the date for Notification.";
			$scope.showDailog(dailogObject);
	   }
	   
	  
	   
	   $scope.setInvoiceitems = function(currentJobOrder) {
			  createJobOrderSubProduct(currentJobOrder);
			  jobOrderService.getJobOrderByPurchaseOrderId($scope.newJob.invoice.value.invId).then(function(jobOrdersForPurchaseOrder) {
				  angular.forEach( jobOrdersForPurchaseOrder , function(jobOrder) {
					  	/*Prep : First Push all element in joborder
					  	 *  1)Traverse Job Order
						  *2)Check if that purchase order id is present in job order list
						  *3)If present that check weather subproduct match
						  *4)If match than do the calculations 
						  */
						  for( var i = 0; i < $scope.jobItems.length; i++ ) {
							 for( var j = 0; j < jobOrder.jobItems.length; j++ ) {
								 if($scope.jobItems[i].invoiceItemId === jobOrder.jobItems[j].invoiceItemId) {
									 if( currentJobOrder !== undefined ) {
											 if( currentJobOrder.jobId != jobOrder.jobId ) {
												 $scope.jobItems[i].validationQuantity -= +jobOrder.jobItems[j].quantity;
											 }
									 } else {
										 $scope.jobItems[i].quantity -= +jobOrder.jobItems[j].quantity;
										 $scope.jobItems[i].validationQuantity -= +jobOrder.jobItems[j].quantity;
										 $scope.jobItems[i].isInValid = false;
									}
								 }
							 }
						  }
					   
				  });
				  
				  deleteIfEmpty();
				  $scope.jobItemErrorMessage = "No Job Item remaining in this purchase order.";
				  /*If quantity is 20 related code
				  $scope.setValidationToSecificValueForJobItems(); */
			  });
			  
	   }
	   
	   /*Make this method as public because we have to call this in directive.*/
	   $scope.setValidationToSecificValueForJobItems = function() {
		   var temp = [];
		   $scope.formValid = true;
		   for( var i = 0; i < $scope.jobItems.length; i++) {
			   if( $scope.jobItems[i].quantity > 20 ) {
				   $scope.jobItems[i].isInValid =  true;
				   $scope.formValid = false;
			   } else {
				   $scope.jobItems[i].isInValid =  false;
				   
			   }
			   temp.push($scope.jobItems[i]);
		   } 
		   $scope.jobItems = temp;
	   }
	   
	   function deleteIfEmpty() {
		   //If product has no quantity left.
		   var temp = [];
		   for( var i = 0; i < $scope.jobItems.length; i++) {
			   if( $scope.jobItems[i].quantity == 0 ) {
					continue;
			   }
			   temp.push($scope.jobItems[i]);
		   } 
		   $scope.jobItems = temp;
		  
	   }
	   
	   function createJobOrderSubProduct(currentJobOrder) {
			  if(currentJobOrder === undefined ) {
				  angular.forEach( $scope.newJob.invoice.value.invoiceitems , function(item) {
					  let jobItemSubProducts = [];
					  for( var i = 0; i < item.invoiceItemSubProducts.length; i++ ) {
						  jobItemSubProducts.push({
							  invoiceitemsubproductId : item.invoiceItemSubProducts[i].invoiceitemsubproductId,
							  invoiceItemSubProductList:[{
								  subproductName : item.invoiceItemSubProducts[i].subproductName,
								  quantity : item.invoiceItemSubProducts[i].quantity,
								  invoiceitemsubproductId : item.invoiceItemSubProducts[i].invoiceitemsubproductId,
								  invoiceItemId : item.invoiceItemSubProducts[i].invoiceItemId
							  }]
						  });
					  }
					  this.push({
						  	invoiceItemId : item.invoiceItemId,
						 	jobItemSubProducts : jobItemSubProducts,
						 	quantity : _.clone(item.quantity),
						 	psku : item.psku,
						 	validationQuantity : _.clone(item.quantity)
				  		});
				  }, $scope.jobItems );
			  } else {
				  $scope.jobItems = _.clone(currentJobOrder.jobItems);
				  angular.forEach( $scope.jobItems , function( invoiceItemParent ) {
					  angular.forEach( $scope.newJob.invoice.value.invoiceitems , function(invoiceitemchild) {
						  if(invoiceItemParent.invoiceItemId == invoiceitemchild.invoiceItemId ) {
							  invoiceItemParent.validationQuantity = invoiceitemchild.quantity;
						  }
					  });
				  });
			  }
	   }
		  
	   $scope.changeJobItems = function() {
		   /*If the function called while closing the dailog than no need to proceed further*/
		   $scope.jobItemErrorMessage = "";
		   if($scope.newJob.invoice === undefined || $scope.newJob.invoice === null) {
			   return;
		   }
 		     $scope.jobItems = [];
			 var setFlag = false;
			 purchaseOrderService.getSinglePurchaseOrderById($scope.newJob.invoice.value.invId).then(function(purchaseOrder) {
				  $scope.newJob.invoice.value.invoiceitems = purchaseOrder.invoiceitems;
				  $scope.setInvoiceitems();
			 });
			
		 };
		 
		 $scope.setCriteria = function() {
			 /*After this send user to first page.*/
			 $rootScope.$broadcast('goToSpecificPage',1);
		  }
}]);

})();
