(function() {
	'use strict';
	app.controller("changePasswordController",function($scope,$rootScope,changePasswordService,MessageService,$location,authService){
		$scope.changePassword ={
				currentPassword:"",
				newPassword:""
		};
		
		/*Change password*/
		$scope.changePasswords = function(){
			changePasswordService.updatePassword($scope.changePassword).then(function(sucessresponse){
					if(sucessresponse.data.responseCode === 201){
						MessageService.showAlert(sucessresponse.data);
						authService.logout();
						$location.path("/login");
					}else{
						MessageService.showAlert(sucessresponse.data);
					}
			  });
		};
	});
})();
