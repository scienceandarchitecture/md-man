(function() {
	'use strict';
app.controller('flashingCtrl',['$scope','$mdDialog','$mdMedia','appConstants','apiDelegateService','apiHandlerService','authService','MessageService','flashingService','jobOrderService','$interval','$rootScope',function ($scope,$mdDialog,$mdMedia,appConstants,apiDelegateService,apiHandlerService,authService,MessageService,flashingService,jobOrderService,$interval,$rootScope) {
	$scope.jobOrderList = [];
	$scope.refreshListParams;
	$scope.selectedJobOrder;
	$scope.selectedJobItem;
	
	$scope.hidePaginationTab = true;
	
	$scope.buildConfugations;
	
	$scope.fwVersionView;
	
	$scope.fwVersionModel;
	
	$scope.dynamicBuildURL = "";
	
	$scope.generalFilters = [{ key : 'boardMacAddress',value : 'Board Mac Address'},{key : 'macAddress',value:'Mac Address'}];
	
	$scope.statusFilters = [];
	
	$scope.selectedStatusFilter = "";
	
	$scope.selectedFilter = "";
	
	$scope.initContoller = function() {
		
		$scope.statusFilters = [];
		$scope.assignMacList = [];
		$scope.buildConfugations = [];
		$scope.loaderStatus = [];
		if(interval != null) {
			$interval.cancel(interval);
		}
		for(var index in appConstants.flashingStatus ) {
			$scope.statusFilters.push({ key : index, value : appConstants.flashingStatus[index]});
		}
		$scope.criteria = {};
	};
	
	$scope.criteria = {};
	
	$scope.loaderStatus = [];
	
	$scope.fwLookUp = ["3.0","4.4.4"];
	
	$scope.downloadStatusList = {};
	
	$scope.existingFlashingListCount = 0;
	
	var interval = null;
	
	$scope.initContoller();
	
	$scope.jobOrderAutocompleteData = {
			 selectedItem:"",
			 jobOrders : []
	 };
	
	$scope.jobItems = [];
	
	$scope.isReadFromExcel = false;
	
	$scope.oldFlashingList = [];
	
	$scope.pagination = { perPageItemCount : 10, goToLastPage : false, pageStartIndex : 0 };
	
	function loadJobOrders() {
        var jobOrdersForAutoComplete = $scope.jobOrderList.map( function (jobOrder) {
       	return {
              value : jobOrder,
              display : "" + jobOrder.jobId
           };
        });
        $scope.jobOrderAutocompleteData.jobOrders = jobOrdersForAutoComplete;
        return jobOrdersForAutoComplete;
     }
	
	
	$scope.subItemChanged = function() {
		$scope.hidePaginationTab = true;
		$scope.flashingList = [];
		/*Pagination view should also get deleted.*/
		$scope.initContoller();
	}
	
	$scope.fwVersionChanged = function() {
		$scope.hidePaginationTab = true;
	}
	
	/*Selected Dialog*/
	$scope.changeTextFieldVisibility = function() {
		if( $scope.selectedFilter != undefined ) {
			$scope.filterTextFieldVisibility = true;
		} else {
			$scope.filterTextFieldVisibility = false;
			$scope.filterTextField = "";
		}
	};
	
	 $scope.querySearch = function(query) {
		 /*Remove subitems first when job item is changed.*/
		 $scope.jobItems = [];
		 $scope.selectedJobItem = {};
		 $scope.flashingList = [];
		 $scope.hidePaginationTab = true;
		 return jobOrderService.getJobOrderListById(query).then(function(jobOrderList) {
			  $scope.jobOrderList = jobOrderList;
			  return loadJobOrders();
		  });
	 }
	 
	/*Get Job Order List*/
	  
	  
	$scope.getBuildConfugations = function() {
		//call web service for getting data
		//1)Get data form web service
		//2)Check length  match the quantity 
		//3)If length does not match than dummy rows till quantity is matched.
		$scope.hidePaginationTab = false;
		$scope.initContoller();
		flashingService.getJobModelFlashDetailsByJobIdAndModelId($scope.selectedJobOrder.value.jobId,$scope.selectedJobItem.jobItemId).then(function(flashingList) {
			updateBuildConfugation(flashingList);
		});
	}
	
	$scope.getFlashingList = function(currentPage,setPagination) {
		$scope.flashingList = [];
		 currentPage = currentPage || 1;
		 var count = currentPage * $scope.pagination.perPageItemCount - $scope.pagination.perPageItemCount + 1;
		 $scope.pagination.pageStartIndex = count;
		 if( $scope.buildConfugations.length > 0 ) {
			 var tempBuildConfugations = $scope.buildConfugations.filter(filterWords);
			 $scope.flashingList = tempBuildConfugations.slice(count - 1, count + $scope.pagination.perPageItemCount - 1);
			 $scope.totalRecords = tempBuildConfugations.length;
			 setPagination($scope.totalRecords,currentPage);
		 }
	} 
	
	
	$scope.setDynamicBuildURL = function() {
		flashingService.getDynamicBuildURL().then(function(response) {
			$scope.dynamicBuildURL = response;
		});
	}
	$scope.setDynamicBuildURL();
	//TODO: Implement better approach instead of defining same function more than one time.
	$scope.getBuildConfugationsForExcel = function(flashingList) {
		//call web service for getting data
		//1)Get data form web service
		//2)Check length  match the quantity 
		//3)If length does not match than dummy rows till quantity is matched.
		$scope.initContoller();
		flashingService.getJobModelFlashDetailsByJobIdAndModelId($scope.selectedJobOrder.value.jobId,$scope.selectedJobItem.jobItemId).then(function(remoteFlashingList) {
			var combinedFlashingList = [];
			var matchedFlashingItem;
			var matchedItemIndex = -1;
			for(var i = 0; i < flashingList.length; i++ ) {
				matchedFlashingItem = _.find( remoteFlashingList , function(item,index) {
					if(flashingList[i].boardMacAddress.toLowerCase() == item.boardMacAddress.toLowerCase()  && item.serialNo.toLowerCase() == flashingList[i].serialNo.toLowerCase()) {
						matchedItemIndex = index;
						return true;
					}
					matchedItemIndex = -1;
					return false;
				});
				//Remove matched from the array
				if(matchedItemIndex !== -1) {
					remoteFlashingList.splice(matchedItemIndex,1);
				}
				
				if( matchedFlashingItem !== undefined ) {
					combinedFlashingList.push(matchedFlashingItem);
					continue;
				} else {
					$scope.assignMacList[i] = false;
					/*Check if user added a valid mac address.*/
					if(/^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/.test(flashingList[i].boardMacAddress)) {
						$scope.assignMacList[i] = true;
					}
					combinedFlashingList.push({
						psku : $scope.selectedJobItem.psku,
						serialNo : flashingList[i].serialNo,
						jobId : $scope.selectedJobOrder.value.jobId,
						version : $scope.fwVersionModel,
						boardMacAddress : flashingList[i].boardMacAddress,
						macAddress : "",
						status : "0",
						downloadLink : "",
						jobItemId : $scope.selectedJobItem.jobItemId
					});
				}
			}
			
			//IF Item are still remaining in remoteFlashingList than data is invalid. Terminate the function here
			if(remoteFlashingList.length != 0) {
				MessageService.showCustomAlert("Invalid items provided.");
				return false;
			}
			
			setTimersForList();
			angular.extend($scope.buildConfugations,combinedFlashingList);
			if(isErrorExcelNeeded()) {
				generateErrorExcel();
			}
			$rootScope.$broadcast("refreshPage");
			$scope.hidePaginationTab = false;
		 });
	}
	
	function isErrorExcelNeeded() {
		let isNeeded = false;
		angular.forEach($scope.buildConfugations,function(item) {
			if(isNeeded) {
				return;
			}
			if(!item.boardMacAddress.match(/^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/)) {
				isNeeded = true;
			}
		});
		return isNeeded;
	}
	
	function generateErrorExcel() {
		angular.forEach($scope.buildConfugations,function(item) {
			if(!item.boardMacAddress.match(/^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/)) {
				item.invalidBoardMacAddress = "Invalid Mac Address.";
			}
		});
		alasql('SELECT serialNo as Board_Serial_No,boardMacAddress as Board_Mac_Address,invalidBoardMacAddress as Error_Status INTO XLSX("error.xlsx",{headers:true}) FROM ?',[$scope.buildConfugations]);
	}
	
	
	
	$scope.$on('getBuildConfugations',function($event,data) {
		$scope.oldFlashingList =  $scope.buildConfugations;
		$scope.getBuildConfugations();
	});
	
	$scope.$on('updateBuildConfugation',function($event,data) {
		updateBuildConfugation(data);
	});
	
	function isEmpty(value){
		  return (value == undefined || value === '');
	}
	
	function getExistingDataBeforeFlashing(remoteFlashingList) {
		var localRemoteFlashingList = angular.copy(remoteFlashingList);
		var nonEmptyOldFlashingList = [];
		if( $scope.oldFlashingList.length > 0 ) {
			/* Step 1) Get only relevant items*/
			$scope.oldFlashingList.forEach(function(singleFlashingItem,index) {
				if( !isEmpty(singleFlashingItem.boardMacAddress) && !isEmpty(singleFlashingItem.serialNo)) {
					nonEmptyOldFlashingList.push({index:index,item:angular.copy(singleFlashingItem)});
				}
			});
			
			/* Step 2) Find and replace items from the remote flashing list*/
			nonEmptyOldFlashingList.forEach(function(flashingItem) {
				for(var i = 0; i < localRemoteFlashingList.length;i++) {
					if(localRemoteFlashingList[i].boardMacAddress == flashingItem.item.boardMacAddress && localRemoteFlashingList[i].serialNo == flashingItem.item.serialNo ) {
						 $scope.oldFlashingList[flashingItem.index] = angular.copy(localRemoteFlashingList[i]);
						 /*Remove Item from that position.*/
						 localRemoteFlashingList.splice(i,1);
						 break;
					}
				}
			}); 
			return $scope.oldFlashingList;
		}
		return false;
	}
	
	function updateBuildConfugation(existingFlashingList) {
		var tempFlashingList;
		setTimersForList();
		tempFlashingList = getExistingDataBeforeFlashing(existingFlashingList);
		if( tempFlashingList !== false ) {
			$scope.buildConfugations = angular.copy(tempFlashingList);
			$scope.oldFlashingList = [];
			$rootScope.$broadcast("refreshPage");
			return;
		}
		
		angular.extend($scope.buildConfugations,existingFlashingList);
		
		/*Set the count for pagination purpose*/
		$scope.existingFlashingListCount = existingFlashingList.length;
		for( var i = $scope.buildConfugations.length; i < $scope.selectedJobItem.quantity; i++ ) {
			var tempBuildConfigObj = {
					psku : $scope.selectedJobItem.psku,
					serialNo : "",
					jobId : $scope.selectedJobOrder.value.jobId,
					version : $scope.fwVersionModel,
					boardMacAddress : "",
					macAddress : "",
					status : "0",
					downloadLink : "",
					jobItemId : $scope.selectedJobItem.jobItemId
			};
			//If Subproduct exist for this SubItem
			if($scope.selectedJobItem.jobItemSubProducts.length > 0) {
				tempBuildConfigObj.jobItemId = $scope.selectedJobItem.jobItemSubProducts[0].jobItemId;
				tempBuildConfigObj.invoiceitemsubproductId = $scope.selectedJobItem.jobItemSubProducts[0].invoiceitemsubproductId;
				tempBuildConfigObj.jobitemsubproductId = $scope.selectedJobItem.jobItemSubProducts[0].invoiceitemsubproductId;
			}
			$scope.buildConfugations.push(tempBuildConfigObj);
		}
		delete $scope.flashingList;
		$rootScope.$broadcast("goToSpecificPage",1);
	}
	
	$scope.$watch("fwVersionView",function(newValue) {
		$scope.fwVersionModel = $scope.fwLookUp[newValue];
	});
	
	$scope.assignMac = function($event) {
		//send data to web service
		var dataToSend = [];
		for(var i = 0; i < $scope.assignMacList.length; i++ ) {
			if( $scope.assignMacList[i] !== undefined && $scope.assignMacList[i] === true ) {
				$scope.buildConfugations[i].status = "1";
				$scope.buildConfugations[i].imageType = "1";
				dataToSend.push($scope.buildConfugations[i]);
				//flashingService.addJobModelFlashDetails($scope.buildConfugations[i]); 
			}
	    }
		//List API working properly.
		if(dataToSend.length == 1) {
			flashingService.addJobModelFlashDetails(dataToSend[0]);
			return;
		}
		flashingService.addJobModelFlashDetailsList({"abc":dataToSend});
		
		//Empty the checkbox List
	}
	
	$scope.validateMacList = function() {
		for(var i =0; i < $scope.assignMacList.length; i++ ) {
			 if($scope.assignMacList[i] !== undefined  && $scope.assignMacList[i] === true) {
				 return false;
			 }
		}
		return true;
	}
	
	function setTimersForList() {
		//clear current interval
		$interval.cancel(interval);
		 //Clear new Interval
		setDownloadButtonStatus();
		interval = $interval(setDownloadButtonStatus , 10000);
	}
	
	function setDownloadButtonStatus() {
		//Hit Rest API for the updated list
		flashingService.getJobModelFlashDetailsByJobIdAndModelId($scope.selectedJobOrder.value.jobId,$scope.selectedJobItem.jobItemId).then(function(flashingList) {
			angular.forEach(flashingList,function(flashingItem) {
				$scope.downloadStatusList[flashingItem.jobmodelflashdetailsId] = flashingService.getDownloadStatus(flashingItem);
			}); 
		 });
	}
	
	$scope.dowloadImage = function(configuration) {
		$scope.loaderStatus[configuration.jobmodelflashdetailsId] = true;
		flashingService.downloadImage(configuration,$scope.dynamicBuildURL).then(function(response) {
			$scope.loaderStatus[configuration.jobmodelflashdetailsId] = false;
			if(response) {
				//Change Status to complete
				configuration.status = 4;
				flashingService.updateJobModelFlashDetailsAfterDownload(configuration);
				return;
			}  
			//Change status to file to download
			configuration.status = 5;
			flashingService.updateJobModelFlashDetailsAfterDownload(configuration);
			return;
		}).catch(function(errorResponse) {
			$scope.loaderStatus[configuration.jobmodelflashdetailsId] = false;
		});
	}
	
	$scope.rebuildImage = function(configuration) {
		flashingService.rebuildImage(configuration).then(function() {
			$scope.$broadcast("getBuildConfugations");
		});
	}
	
	$scope.readExcel = function(workbook) {
		var flashingJobDetails = flashingService.readHeaderExcel(workbook);
		 flashingService.isJobOrderValid(flashingJobDetails.jobId).then(function(jobOrder) {
				if( jobOrder !== false ) { 
					$scope.jobOrderAutocompleteData.jobOrders = [{
			               value : jobOrder,
			               display : "" + jobOrder.jobId
			            }];
					$scope.selectedJobOrder = $scope.jobOrderAutocompleteData.jobOrders[0];
					$scope.jobItems = jobOrder.jobItems;
				} else {
					MessageService.showCustomAlert("Invalid JobId. Please correct data in excel sheet.",true);
					return;
				}
				
				if( flashingJobDetails.purchaseOrderId !== $scope.selectedJobOrder.value.invId ) {
					MessageService.showCustomAlert("Invalid Purchase Order Number. Please correct data in excel sheet.",true);
					return;
				}
			
				//2)loop through all job items and select the matching one.
				var jobItem = flashingService.isJobItemValid($scope.jobItems,flashingJobDetails);
				
				if(jobItem !== false) {
					$scope.selectedJobItem = jobItem;
				} else {
					MessageService.showCustomAlert("Invalid JobItem. Please correct data in excel sheet.",true);
					return;
				}
				
				
				//3)compare fireware version
				if(flashingService.isFirmwareVersionValid($scope.fwLookUp,flashingJobDetails.fwversion)) {
					$scope.fwVersionView = $scope.fwLookUp.indexOf(flashingJobDetails.fwversion);
				} else {
					MessageService.showCustomAlert("Invalid Firmware Version. Please correct data in excel sheet.",true);
					return;
				}
				
				var flashingItemList = flashingService.readFalshingListFromExcel(workbook,$scope.selectedJobItem.quantity);
				
				if(flashingItemList === false) {
					MessageService.showCustomAlert("Invalid count of rows for job item.",true);
					return;
				}
				
				//4)getBuildConfugations
				$scope.getBuildConfugationsForExcel(flashingItemList);
				
				$scope.isReadFromExcel = true;
		});
		
	}
	
	$scope.error = function(error) {
		console.log(error);
	};
	
	$scope.ifFlashingDisabled = function(formStatus,dirty,index) {
		if(formStatus && dirty) {
			$scope.assignMacList[index] = false;
			return true;
		}
		return false;
	}

	$scope.$on("$destroy", function() {
		$interval.cancel(interval);
	});
	
	$scope.setSubJobItems = function() {
		if(!$scope.isReadFromExcel) {
			$scope.initContoller();
			jobOrderService.getJobOrderById($scope.selectedJobOrder.value.jobId).then(function( jobOrder ){
				$scope.jobItems = jobOrder.jobItems;
			});
		}
		$scope.isReadFromExcel = false;
	}
	
	var filterWords = function(flashingItem) {
		/*1)Check if criteria is empty.
		 * If so than return true.
		 * Else do filter according to two thing 
		 * a)boardmac address b)mac address
		 * 2)If it is done than go to
		 * a)status while filter 
		 **/
		var currentFilter;
		if(angular.equals($scope.criteria, {})) {
			return true;
		}
		
		if( !$scope.criteria.hasOwnProperty('boardMacAddress') && !$scope.criteria.hasOwnProperty('macAddress') ) {
			return filterByStatus(flashingItem);
		}
		/*TODO: Try to make it dynamic.*/
		if($scope.criteria.hasOwnProperty('boardMacAddress')) {
			currentFilter = 'boardMacAddress';
		} else {
			currentFilter = 'macAddress';
		}
		return flashingItem[currentFilter].includes($scope.criteria[currentFilter]) && filterByStatus(flashingItem);
	}; 
	
	function filterByStatus(flashingItem) {
		if( $scope.criteria.status == undefined || $scope.criteria.status == "" ) {
			return true;
		}
		return $scope.criteria.status == flashingItem.status;
	}
	
	$scope.setCriteria = function() {
		 if($scope.selectedFilter === undefined || $scope.selectedFilter == "") {
			 $scope.criteria = {};
		 } else{
			 $scope.criteria = {};
			 $scope.criteria[$scope.selectedFilter] = $scope.filterTextField;
		 }
		 
		 if($scope.selectedStatusFilter !== undefined) {
			 $scope.criteria.status = $scope.selectedStatusFilter;
		 }
		 /*After this send user to first page.*/
		 $rootScope.$broadcast('goToSpecificPage',1);
	 }
	
}]);
	
	
})();