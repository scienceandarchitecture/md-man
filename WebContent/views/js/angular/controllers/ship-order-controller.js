(function() {
	'use strict';
app.controller('shipOrderCtrl',['$rootScope','$scope','$mdDialog','$mdMedia','appConstants','apiDelegateService','apiHandlerService','authService','jobOrderService','_','$filter','shipOrderService','purchaseOrderService','wordWrapService','shipOrderDailogService','validateResponseService',
	function ($rootScope,$scope,$mdDialog,$mdMedia,appConstants,apiDelegateService,apiHandlerService,authService,jobOrderService,_,$filter,shipOrderService,purchaseOrderService,wordWrapService,shipOrderDailogService,validateResponseService) {
	/*Get All list and populate the table*/
	$scope.defaultShipOrder = {
		state:"open",
		created :new Date(),
		commitBy :new Date(),
		shipBy : new Date(),
		createdBy : authService.currentUser().userName,
		ackBy : authService.currentUser().userName
	};
	
	$scope.totalRecords = null;
	
	var dailogObject;
	
	/*Ship order used for add/update*/
	$scope.shipOrder;
	
	$scope.shipOrderList;
	
	$scope.jobOrderList;
	
	$scope.update = false;
	
	$scope.refreshListParams;
	
	$scope.invoiceList;
	
	$scope.selectedCustomer;
	
	/*Product Data Strucutre Starts*/
	$scope.productItems;
	
	$scope.minimumNeedBy;
	
	$scope.tracking;
	
	$scope.itemKeys = { 
			qunititykey:"psku",
			itemkey:"quantity"
	};
	
	/*Product Data Structure Ends*/
	
	 $scope.resetData = function() {
		$scope.update = false;
		$scope.shipOrder = _.clone($scope.defaultShipOrder);
		
		$scope.selectedCustomer = {
				custId : "",
				name : "",
				address : "",
				shipAddress : "",
				instructions : ""
		};
		
		$scope.productItems = [];
		
		$scope.updateItemFlag = false;
		
		
		$scope.refreshListParams = { 
			url : appConstants.apiUrl + 'ships/getShippingItemList',
			method : 'GET'
		};
		
	
		
		$scope.minimumNeedBy = new Date();
		
		$scope.tracking = {
				 invoiceNumber : 0,
				 carrier : "",
				 trackingNumber : "",
				 shipId : 0,
				 invId : 0
		};
	 }
	 
	 $scope.maxQuantity = 0;
	 
	 $scope.selectedCriteria = {};
	 
	 $scope.pagination = { perPageItemCount:10, goToLastPage : false };
	 
	 $scope.criterias = [{key:{state:"open"},value:"Relevant"},{key:{},value:"All"},{key:{state:"finished"},value:"Finished"}];
	 
	 /*Reset All variables to Default.*/
	 $scope.resetData();
	
	/*Load Invoice List for Dropdown */
	$scope.getJobOrdersByCriteria = function( criteria ) {
		jobOrderService.getJobOrderByCriteria( criteria ).then(function(jorOrders) {
			  $scope.jobOrderList = jorOrders;
		});
	}
	
	
	/*Purhcase order related code*/
	$scope.purhaseOrderAutocompleteData = {
			 selectedItem:"",
			 purchaseOrders : []
	 };
	
	function loadPurchaseorders() {
        var purhaseOrderForAutoComplete = $scope.invoiceList.map( function (purchaseOrder) {
       	return {
              value : purchaseOrder,
              display : "" + purchaseOrder.invId
           };
        });
        $scope.purhaseOrderAutocompleteData.purchaseOrders = purhaseOrderForAutoComplete;
        return purhaseOrderForAutoComplete;
     }
		
	$scope.querySearch = function(query) {
		return purchaseOrderService.getPurchaseOrderById(query).then(function(purchaseOrders) {
			if( typeof purchaseOrders == "string" || typeof purchaseOrders == "undefined" ) {
				return [];
			}
			$scope.invoiceList = purchaseOrders;
			return loadPurchaseorders();
			
		});
	}
	
	/*Refresh List when page loads*/
	
	 $scope.$on('refeshShipList',function(event,data) {  
		if($scope.update) {
				$rootScope.$broadcast("refreshPage");
		} else {
			if(!validateResponseService.isUpdateSuccessful(data.data.responseCode)) {
				  return false;
			}
			if( parseInt($scope.totalRecords) % parseInt($scope.pagination.perPageItemCount) === 0 ) { 
				$rootScope.$broadcast("goToSpecificPage",Math.ceil( $scope.totalRecords / $scope.pagination.perPageItemCount ) + 1 );
			} else {
				$rootScope.$broadcast("goToLastPage");
			}
		}
		 $scope.checkResponseCode(data.data.responseCode);
	 });
	 
	 $scope.$on('refeshAfterTracking',function(event,data) {    	
			 $scope.checkResponseCode(data.data.responseCode);
	 });
	 
	 $scope.$on('refreshAfterDelete',function(event,data) {
			 if(!validateResponseService.isUpdateSuccessful(data.data.responseCode)) {
				  return false;
			}
			if(parseInt($scope.totalRecords) % parseInt($scope.pagination.perPageItemCount) === 1) { 
				$rootScope.$broadcast("goToSpecificPage",Math.ceil( $scope.totalRecords / $scope.pagination.perPageItemCount ) - 1 );
			} else {
				$rootScope.$broadcast("refreshPage");
			}
			$scope.checkResponseCode(data.data.responseCode);
	 });
	 
	 $scope.getShipOrderList = function(currentPage,setPagination) {
			currentPage = currentPage || 1;
			shipOrderService.getShipOrderPaginationList($scope.pagination,currentPage,$scope.selectedCriteria).then(function(response) {
				 $scope.shipOrderList = response.data;
				 $scope.totalRecords = response.count;
				 setPagination(response.count,currentPage);
			});
	}
	
	 $scope.setShipOrderProducts = function() {
		 //Convert job item data into ship item data structure.
		 $scope.shipOrder.jobId = $scope.shipOrder.jobOrder.jobId;
		 $scope.shipOrder.shipItems = [];
		 
		 for( let i = 0; i <  $scope.shipOrder.jobOrder.jobItems.length; i++ ) {
			 $scope.shipOrder.shipItems.push({
				 psku : $scope.shipOrder.jobOrder.jobItems[i].psku,
				 quantity : $scope.shipOrder.jobOrder.jobItems[i].quantity
			 });
			 if(!angular.isUndefined($scope.shipOrder.jobOrder.jobItems[i].jobItemSubProducts)) {
				 $scope.shipOrder.shipItems[i].shipItemSubProducts = [];
				 $scope.shipOrder.jobOrder.jobItems[i].jobItemSubProducts.forEach(function(item) {
					 $scope.shipOrder.shipItems[i].shipItemSubProducts.push({
						 invoiceitemsubproductId : item.invoiceitemsubproductId,
						 invoiceItemSubProductList : item.invoiceItemSubProductList
					 });
				 });
			 }
		 }
		 //TODO: Don't use this productItems extra veriable.
		 $scope.productItems = _.clone($scope.shipOrder.shipItems);
	 }
	 
	$scope.openShipOrder = function($event,currentShipOrder) {	
		/*For Update Code*/
		if(currentShipOrder !== undefined ) {
				$scope.update = true;
				$scope.shipOrder = _.clone(currentShipOrder);
				$scope.shipOrder.shipBy = new Date.createFromMysql(currentShipOrder.shipBy);
				$scope.productItems = _.clone($scope.shipOrder.shipItems);
				 $scope.minimumNeedBy = new Date.createFromMysql($scope.shipOrder.created);
				/* Set Invoice List */
				purchaseOrderService.getPurchaseOrderById(currentShipOrder.invId).then(function(purchaseOrders) {
					$scope.invoiceList = purchaseOrders;
					$scope.shipOrder.invoice = {
			              value : purchaseOrders[0],
			              display : "" + purchaseOrders[0].invId
			           }
					
					 /*Set Job order*/
					jobOrderService.getJobOrderByPurchaseOrderId( currentShipOrder.invId ).then(function(jorOrders) {
						$scope.jobOrderList = jorOrders;
						 _.find($scope.jobOrderList, function (jobOrder) {
			                  if( jobOrder.jobId === $scope.shipOrder.jobId ) {
			                	  $scope.shipOrder.jobOrder = jobOrder;
			                  }
			             });
						 
						 /*TODO: Code for dialog object copied.Find some better approach.*/
						 dailogObject = {
									targetEvent : $event,
								    templateUrl : 'views/partials/dialogs/addShipOrder.html',
								    scope: $scope
								};
						$scope.showDailog(dailogObject);
						return;
					});
					return;
				});	
				return;
		}
		dailogObject = {
			targetEvent : $event,
		    templateUrl : 'views/partials/dialogs/addShipOrder.html',
		    scope: $scope
		};
		$scope.showDailog(dailogObject);
	}
	
	 $scope.addShipOrder = function() {
		 /*Add product*/
		$scope.shipOrder.shipItems = $scope.productItems;
		if($scope.update) {
			delete $scope.shipOrder.invoice;
			apiHandlerService.addData( $scope.shipOrder,'ships/updateShipItems', 'PUT', 'refeshShipList' );
		} else {
			/*Fetch Data from Object*/
			$scope.shipOrder.invId = $scope.shipOrder.invoice.value.invId;
			apiHandlerService.addData( $scope.shipOrder,'ships/addShippingItems', 'POST', 'refeshShipList' );
		}
	 }
	
	
	$scope.getCustomer = function(purchaseOrder) {
		if(purchaseOrder != undefined ) {
			$scope.productItems = [];
			apiDelegateService.apiHandler({
				url : appConstants.apiUrl + 'customers/getCustomerById?custId='+purchaseOrder.value.custId,
				method : 'GET'
			  })
			 .then(function(successResponse) {
			  	$scope.selectedCustomer = successResponse.data.data;
				$scope.shipOrder.address = $scope.selectedCustomer.shipAddress;
				$scope.shipOrder.instructions = $scope.selectedCustomer.instructions;
				$scope.shipOrder.custName = $scope.selectedCustomer.name;
			}).catch(function(errorResponse) {
		         console.log(errorResponse);
			});
			$scope.getJobOrdersByCriteria({
				  invId:purchaseOrder.value.invId,
				  state:'finished'
			});
		} else {
			$scope.selectedCustomer = {
					custId : "",
					name : "",
					address : "",
					shipAddress : "",
					instructions : ""
			};
			$scope.shipOrder.address = "";
			$scope.shipOrder.instructions = "";
			$scope.shipOrder.custName = "";
			$scope.shipOrder.phone = "";
		}
	}
	 
	$scope.getproductItemList = function() {
		  var options = {
				  url : appConstants.apiUrl + 'products/getProductList',
				  method : 'GET'
		  }
		  apiDelegateService.apiHandler(options)
		  .then(function(sucessResponse) {
			  	$scope.products = sucessResponse.data.data;
				$scope.selectedItem = $scope.products[0].psku;
		  }).catch(function(errorResponse) {
			  console.log(errorResponse);
		});
	 };
	 
	
	 $scope.getproductItemList('products/getProductList');
	 
	 $scope.cancel = function () {
	        $mdDialog.cancel();
	        $scope.resetData();
	 }; 
	 
	 $scope.$on('closeDailog',function() {
		 $scope.cancel();
	 });
	 
	 /*Delete Ship Order */
	 $scope.deleteOrder = function($event,shipOrder) {
		   dailogObject = {
					targetEvent : $event,
			        templateUrl : 'views/partials/dialogs/alertDialogNew.html',
			        scope : $scope
			};
			$scope.title = "Are you sure want to delete Ship Order " + shipOrder.shipId;
			
			$scope.deleteEntry = function() {
				apiHandlerService.deleteData('ships/deleteShipMasterByID?shipId=' + shipOrder.shipId, 'DELETE','refreshAfterDelete');  
			}
		   $scope.showDailog(dailogObject);
	 }
	 
	 /*Ack by Order */
	 $scope.acknowledgeOrder = function($event,shipOrder) {
		   $scope.shipOrder = _.clone(shipOrder);
		   $scope.update = true;
		   
		   if($scope.shipOrder.acknowledged != "" ) {
			   $scope.shipOrder.acknowledged = new Date.createFromMysql($scope.shipOrder.acknowledged);
		   } else {
			   $scope.shipOrder.acknowledged = new Date();
		   }
		   
		   $scope.shipOrder.ackBy = authService.currentUser().userName;
		   
		   $scope.productItems = _.clone($scope.shipOrder.shipItems);
		   
		   $scope.shipOrder.created = new Date.createFromMysql($scope.shipOrder.created);
		   
		   $scope.minimumNeedBy = $scope.shipOrder.created;
		   
		   $scope.maximumNeedBy = new Date();
		   
		   $scope.shipOrder.state = appConstants.jobstatus.acknowledged; 
		   
		   dailogObject = {
					targetEvent : $event,
			        templateUrl : 'views/partials/dialogs/shipOrdersAknowledge.html',
			        scope : $scope
			};
		   
		   $scope.title = "Items need to ship by " + $filter('dateFilter')($scope.shipOrder.shipBy) + ".<br>Enter the approximate date by which the items will be shipped.";
		   $scope.showDailog(dailogObject);
	 }
	 
	 /*Add Ship Order*/
	 $scope.addTrackingDialog = function($event,shipOrder) {
		 	$scope.tracking.invoiceNumber = shipOrder.invId;
			$scope.tracking.invId = shipOrder.invId;
			$scope.tracking.shipId = shipOrder.shipId;
			
		 	dailogObject = {
					targetEvent : $event,
			        templateUrl : 'views/partials/dialogs/shipOrdersAddTracking.html',
			        scope : $scope
			};
			
		   $scope.showDailog(dailogObject);
	 }
	 
	 $scope.scanShipmentDialog = function($event,shipOrder) {
		 	$scope.shipOrder = shipOrder;
		 	dailogObject = {
					targetEvent : $event,
			        templateUrl : 'views/partials/dialogs/shipOrdersScanShipment.html',
			        scope : $scope
			};
		   $scope.showDailog(dailogObject);
	 }
	 
	 $scope.addTrackOrder = function() {
		 /*Fetch Data from Object*/
		//	$scope.update = true;
		 apiHandlerService.addData( $scope.tracking,'tracking/addTracking', 'POST', 'refeshAfterTracking' );
	 }
	 
	 $scope.packagingShipDialog = function($event,shipOrder) {
		 	$scope.shipOrder = shipOrder;
		 	dailogObject = {
					targetEvent : $event,
			        templateUrl : 'views/partials/dialogs/shipOrdersPackagingSlip.html',
			        scope : $scope
			}; 
		   $scope.showDailog(dailogObject);
	 }
	 
	 $scope.setCriteria = function() {
		 $rootScope.$broadcast('goToSpecificPage',1);
	  }
	 
	 $scope.convertToPrintableItem = function() {
		 let shipItems = [];
		 $scope.shipOrder.shipItems.forEach(function(item) {
			 shipItems.push([{ 'text': item.quantity },{'text': wordWrapService.gerWordWrapString(item.psku,15,"\n",true)},{'text' :  wordWrapService.gerWordWrapString(item.description,45,"\n",true)}]);
		 });
		 return shipItems;
	 }
	 
	 $scope.finishShipOrderDailog = function($event,shipOrder) {
		 $scope.update = true;
		 shipOrderDailogService.openFinishShipOrderDailog($event,shipOrder);
	 }
	 
	 
	 $scope.downloadTrackPdf = function() {
		 console.log($scope.shipOrder);
		 
		 var docDefinition =  {
			 	content: [
			 			{
			 			    table: {
			 			    	widths: [300,115,70],
			 				body: [
			 					[
			 					    { image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHwAAAAnCAMAAAD6idy4AAAC/VBMVEU/N9U/QvZANc5AN9RBRPdCOdJDO9dDRvdEO9REPtxER/dFO9JFSPdGSfdHPM9HPdBHSvdHS/lIQNhISO9IS/dJPs9JTPdKPspKPsxKTfdKTflLQM5LTvdMQ9VMR99MT/dNQMpNQc1NQ9BNTOxNUPRNUPdORtlOUfdPUvZPUvdPUvlQT+5QU/dRQ8pRVPdSVfdSVfhTVvdUV/dVTdpWWfdZScVaXfhbXvddWeNfYfhiZfhjVMZkZfZnavlpavNqV7xqbfhtcPhucfhvcvhxc/hydPl0dvl0ef11dvR4bNJ5fPl9ZrV+fvN/gvmChPmEhvqFif2JjPqMca2PkvqTlfqWmPqZh8WbnfqfofqjpfumiKWmqPurrfuvsPuwsvu0tfy2r+W4ufy8vfy+wPzBwPjEnY/ExfzGpqTHyPzLzf3O0P3QvcjQ0v3Sp4fU1f3X2P3b2/7d3f7hsoLi4v7mwp3m3+jn6P7qt3fq6v70w3n09P/1wHL1xoD17ef3yIH316n43LX5wWr5wWz5wm35w2/5w3H5w3L5xHH5xHP5xXX5xXb5xnX5xnb5xnf5x3j5x3r5yHv5yH35yX75yYD5yn/5yoH5y4P5y4T5zIT5zYf5zov6x3b6yX/6yn76y4P6zIP6zIX6zYf6zYj6zon6zov6z4360I/60ZH60ZL60pP605X605b6+f/6+//7xW/7xnH7xnL7x3T7yHb71Jf71Jn71Zr71Zz71p3715/716D72KL72qX726n73Kv73a/8zYP81Jb83Kv83az83rH837L84LX84bj847z85L/85cH858X86cr868/8/P/9zX/947n96Mf96Mj96s3969H97NH97dP97tf979n98Nv98t/+x3D+yXH+2Zv+8d3+8uH+8+L+9OX+9ef+9ej+9un+9+v+9+z++O7/yGv/ynT/zHL/zXj/0XD/1Gf/14v/3JL/46n/8dj/9OL/9dX/9uf/+vL/+/b//Mv//Pf//Pn//ej//fv//vv//v3///7///96GOGmAAAIx0lEQVR4AcST0WoaTxTG/wH/LCy7sFDZZtKFIQ3D5FjR1oo2YqCauoUFtVZF0xhlI1gVySpSEe8KCbQXeYa+gD5CH6AXXjdX6Tv0JvSiQ89sIXchUIV84DDnzAc/Ps/Z/8Q96l/h3mAw6E3uBz5LUQCtcT/wYdhiNNheM3w5nwsUnjeNxeLmdYHyLx3CgfE+utcCbzn5fL5YtjOZwtB1Mhnbj+WW7Ez2wGks8T6pF7JZWSC/pgHwqIPu/GgNcGfDQOkmMY2EpROiRsdCHCmqiTKU8lLM7Q2DBE1TD1SFyBuAwsrcKK0On+1ZAGBxQJkMD8b64sSieOH409riUOfAWRiAwnjuuymX5sxiZbgHSDTzOQuAOGUDsPQmMYLpSgfYU2qzGAXOG7jljJ+OpNvKvsIX016uDHcNmavjmADqcUNFeHTaUgB5nRwB0Oo9zGmlp9GApljTE3SDcVRAt76Gv72iAdDkGINxclIMYv6Xfo8lO3FMqR41FSwSjVqlUq6Lqr9vvQyRLyvDlzaGMB3vGQMe6exTP1HBAIBInOOptJoBALBY27dLN00OExR40F0ZPksiT6v5n2/ClVm1hsghAhihlFrBXp+gAygrdpd/3Wa+Bxw47a4MH2NkMNqHKgBNHRIAjsu+R0OhUGxPKj0VLWIB9nW1Kd3+JPzVGK4MPw0z4OGBHLZl13QAFpt+eLKzs/OgNT5FjdDTjBL/20ovPXSD4eLk0TcRt+r31eXl5fcfd8E7FschjlIYzihXEW6lrt9vPtrefNwtROJxVpl53qiRZoC48LSLbhYd2ET6ZrfDr798vrg4+3oXvIkhSK4fYQD6cQnzm69/vft/e3vzeTdBGA20+iwcfVqqBmXWmavJ6Xhxmb8obtfVp7Pz84/f7oLXVIBgoWVy4LzrbMGu/ubnW4Q/fOGGsUeHbY0xpSanTPZFXdmFrVwHQiH4I/bw37+RzfsD4ULIBzt27ty57cY/iMjvP0AKxv6NsPyslwgovEN4wZndRk5Wli3sHchyaQtPITU1IY+fUZxAygfE4Qx77ykkqyYcECGoLacmm/52+Y7LQCNObd6+ffvOPSuXLdvx4OKWZcs2XwKKbdmxY/vOR//+PTm6bdmyZSsPPgUq/Hps53IgZ8uJ3/8YEDmNN9QLnNkLjMUVFFmj3kQxKyoqassqKEgwpXyLYlVQUFKWAHJYU1tMJRQVOIMDhJUUJHSLbre3tAIt2tzY2tre2tLe0dE6paO1o6Ol/8m/zU3t7S2dD/49W9zU2tHe0d686Om/37saWzra2ztam/f9hVieB6qu2MMtecXEWMJTWPn4+bhTXxdZM4vKqshKcCsG1b7P1WWVkFOWluYW9a7M5efm52MItuPg42e1rtzU29u++M/36cC01QvEQNDRBSS6um78Bol1Tvr+bGkLVK5597/zLT29PV1g/kOI5RkOTs5Orslejs5OtpkJ9iBQcu5FUZCpmqaarnfuqw+Lb+cGmWorK+p6p258nw6St471BFFhd9qB5kz8fa8XaGD79hWdQLtX72ntBXLvPQCLrf63p6m3t2fKvt6e3s5FjyeCHHFiUWdvV88NBlhzBQjOrgWBfw3VlUBwfmrnixfl2TnpRbNfzj0xoefF7ZK87NyiTS8nnKgDSlfXfwUUY31PaVxR+B/vi+xu9t71smy3YE2TsaBAoplJI0qUH9AmIAo41VLApR1/WJT2qZ22OjgLe6bnW3TygJnpSybnYe/cc++e7zvfOWdc+QLLIYCkG7R5UYvdLcGz1dzX2GePOuzjSRsnGM/cbQsGT7UVniNH0zU5nP+GC374Lpfb/L5lq+izX/+hP3/JWYk8v/76iP76/Y9ncqW2mcvlXh//m3uz+ebHv5/KMLv3OuPFL5MQulNgNUSGqlDb7HhSMYejKjfFQv6IfYvP9+uVSnlvMg9+G4d028cCco5ow4wq94WIKtGjdUicqiBJ69RzE8lksriI7Mp8DXqfxSwWt7sq2VdiH9QfNlkHJQ6b+d3d/E9HEZRbHnzk6/UsCqK1A6TydELbHMF+rqKW/dv4q5DWTOjR1L9qV5f4FoT2caTXugJHLRcRDqaQWy5N6ogUu6DQekICXez2po+BNxHPOC7zYm4EtMNQoCO+oRMrpFU0IXTgN5aUwZEgw2CAI7PTQLGTDQGfN4qhIm+pCLLLPs2sZcmQr958DBzVU9E+Kx01CgTw0LQKtcIaNvNmKHQnIlkPHEnnthW+5BX4snhZ0+G7GWC6tDq9QkWykzEMuTtCIc7q3Tz4FJWSK55rMcT7D+CyTxBDxfrpsKNnVV7/ViIy1ZFy4iKFXMtMH/3WBciTNqXZaRR7rus6ef/87OIgFcazL+fBb1wEKHTRotrRPTgAgiAj0G+nDjeV6F4ja9kAuP6OSjpS9mwLxS4ZoTRQw4pd3iGgXt+LSBmpDZ2o7aTfmZDmeh586OBuY3/WooSwMK1GN18+0EKz9zG28Rb7IMP2Ez7aaZkQvxcO+z6FjZbwZwH3MH5G1xNSGjv7oLrmz4Ofgr1ZeiGZtnP1AK7sCwIqB+U3QbuBZamiAHcWpMWD3jI+DMXv3Nd6lo3cSlhR+fVdx1Q8kW8FtKB58L5EE3FLosEnM3DwnNC9GGHzuuNK6Mc13sykrW6DwtY5ht06mcbBIU9jnEU5qrVQpkNDcXAUTO89An6uTEsJXbBFijT1IRcHLvAYLAhh2F6Kz/Q1akakZZmG4E2KBtIUQquv8Hah+rNuCkMNz4XJuxIFhYiwLClNtXNN10lNWpI3cvf2sVFruUpmKlm21TZVl+1Qdb1NtLeWzWaKA/wxXmuQjz8U64VMNpsuUy/NzpedVxk+6h+v8r0Nvw/fapPodg/t7WT6iH5ZWuJNbOPkI/+f33jeLfkTNvKXNQme2rZPNGWfP+UnLwG+U07OphT6cMQWTPzwiQsT+nCRfP6QvJo8hB943nXwP34cGMVtJ+Yk1lvjz/CbjH/JdjUGz08L/hnsPxE2cF+QsfTSAAAAAElFTkSuQmCC',border: [false, false, false, false]},
			 					    { text: 'Packing Slip Number:',style: 'subHeader',border: [false, false, false, false]},
			 					    { text : $scope.shipOrder.shipId ,style: 'shipNumber',border: [false, false, false, false]}]
			 				    ]
			 			    }
			 			},
			 			{
			 			    style:'dateTable',
			 			    table: {
			 			    	widths: [27,70,130,10],
			 				body: [
			 					[
			 					    {text:'Date:',style: 'subHeader',border: [false, false, false, false]},
			 					    {text: $filter('dateFilter')($scope.shipOrder.shipBy) , style: 'smallBoxContent',border: [false, false, false, false]},
			 					    {text:'Purchase Order Number:',style: 'subHeader' ,border: [false, false, false, false]},
			 					    {text: $scope.shipOrder.invId , style: 'shipNumber',border: [false, false, false, false]}]
			 				    ]
			 			    }
			 			},
			 				{
			 			    style:'addressTable',
			 			    table: {
			 			    	widths: [250,250],
			 				body: [
			 					    [
			 					    {text:'Ship From:',style: 'addressHeader',border: [false, false, false, false]},
			 					    {text:'Ship To:',style: 'addressHeader',border: [false, false, false, false]}
			 					    ],
			 					    [
			 					    {text:'Mesh Dynamics\n3355 Benton St,\nSanta Clara,\nCA 95051',style: 'address',border: [false, false, false, false]},
			 					    {text :	wordWrapService.gerWordWrapString( $scope.shipOrder.custName,30,"\n" ,true ) + " \n" + wordWrapService.gerWordWrapString( $scope.shipOrder.address,30,"\n",true ) ,style: 'address',border: [false, false, false, false]}
			 					    ]
			 				    ]
			 			    }
			 			},
			 			{
			 			    style:'addressTable',
			 			    table: {
			 			    	widths: [30,100,350],
			 				body: [
			 					    [
			     					    {text:'Qty',style: 'itemHeader'},
			     					    {text:'Item',style: 'itemHeader'},
			     					    {text:'Description',style: 'itemHeader'}
			 					    ]
			 					   ]
			 			    }
			 			},
			 			{
			 			    table: {
			 			    	widths: [30,100,350],
			 			    	body:  $scope.convertToPrintableItem()
			 			    },
			 			    layout: {
			 				    fillColor: function (i, node) { return (i % 2 === 0) ?  '#eeeeee' : null; }
			 			    }
			 			}
			 			,
			 					   
			 		],
			 		styles: {
			 		    header: {
			 		         fontSize: 15,
			 		           bold: true,
			 		    },
			 		    subHeader: {
			 		    	bold: true,
			 			    fontSize: 11,
			 			    margin: [0, 12, 0, 10]
			 		    },
			 		    shipNumber: {
			 		        fontSize: 15,
			 		         margin: [0, 10, 0, 10]
			 		    },
			 		     smallBoxContent: {
			 		        fontSize: 14,
			 		         margin: [0, 10, 0, 0]
			 		    },
			 		    dateTable:{
			 		        margin:[0, 20, 0, 0]
			 		    },
			 		    addressHeader:{
			 		         fontSize: 14,
			 		           bold: true,
			 			     margin: [0, 2, 0, 0]
			 		    },
			 		    address: {
			 		        fontSize: 13,
			 		         margin:[0, 0, 0, 10]
			 		    },
			 		    addressTable: {
			 		        margin: [0, 30, 0, 0]
			 		    },
			 		    itemHeader:{
			 		        fontSize: 13,
			 		           bold: true,
			 			     margin: [0, 2, 0, 2]
			 		    }
			 		}
			 };
		 var d = new Date();
		 var timestamp = d.getTime(); 
		 pdfMake.createPdf(docDefinition).download("PackingSlip_" + $scope.shipOrder.shipId + "_" + timestamp + ".pdf" );
		 //pdfMake.createPdf(docDefinition).print();
		 /*After setting data on PDF reset the data.*/
	 }	 
}]);

})();

