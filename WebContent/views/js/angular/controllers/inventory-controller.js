(function() {
	'use strict';
	app.controller('inventoryCtrl',['$scope','$rootScope','$mdDialog','appConstants','apiDelegateService','apiHandlerService','MessageService','$mdMedia','_','inventoryService','inventoryDailogService','validateResponseService',function($scope,$rootScope,$mdDialog,appConstants,apiDelegateService,apiHandlerService,MessageService,$mdMedia,_,inventoryService,inventoryDailogService,validateResponseService) {
	
		$scope.pagination = { perPageItemCount:10, goToLastPage : false};
		$scope.totalRecords = null;
		
		$scope.criteria = {rsku : ""};
	/*Get Inventory List*/
	  $scope.getRawInventoryList = function(currentPage,setPagination) { 
		  currentPage = currentPage || 1;
		  inventoryService.getRawInventoryPaginationList($scope.pagination,currentPage,$scope.criteria).then(function(response) {
				//Get the total count from the server
			  $scope.inventoryList =  response.data;
			  $scope.totalRecords = response.count;
			  setPagination(response.count,currentPage);
				 //If it a add senario than send it to last page.
			});
	  };
	  
	  
	/*Refresh Inventory list*/  
	  $scope.$on('refreshInventoryList',function(event,data) {
		  if(inventoryDailogService.getUpdateFlag()) {
			  $rootScope.$broadcast("refreshPage");
		  } else {
				if(!validateResponseService.isUpdateSuccessful(data.data.responseCode)) {
					  return false;
				}
			  	if( parseInt($scope.totalRecords) % parseInt($scope.pagination.perPageItemCount) === 0 ) { 
					$rootScope.$broadcast("goToSpecificPage",Math.ceil( $scope.totalRecords / $scope.pagination.perPageItemCount ) + 1 );
				} else {
					$rootScope.$broadcast("goToLastPage");
				}
		  }
		  $scope.checkResponseCode(data.data.responseCode);
	  });
	  
	 $scope.$on('refreshAfterDelete',function(event,data) {
		 if(!validateResponseService.isUpdateSuccessful(data.data.responseCode)) {
			  return false;
		}
			if(parseInt($scope.totalRecords) % parseInt($scope.pagination.perPageItemCount) === 1) { 
				$rootScope.$broadcast("goToSpecificPage",Math.ceil( $scope.totalRecords / $scope.pagination.perPageItemCount ) - 1 );
			} else {
				$rootScope.$broadcast("refreshPage");
			}
		$scope.checkResponseCode(data.data.responseCode);
	});
	  
	/*Show Add/Update Inventory Dialog*/
	  $scope.showInventoryDialog = function($event,inventory) {
		  inventoryDailogService.openInventoryDailog($event,'refreshInventoryList',inventory);
	  };
		
	/*Show Delete Inventory dialog*/
		$scope.deleteInventoryDialog = function($event,inventoryDetails) {
			inventoryDailogService.openDeleteInventoryDialog($event,'refreshAfterDelete',inventoryDetails);
		};
		
		$scope.setCriteria = function() {
			 $rootScope.$broadcast('goToSpecificPage',1);
		 }
	}]);
})();