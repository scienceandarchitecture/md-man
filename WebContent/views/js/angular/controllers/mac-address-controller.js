(function() {
	'use strict';
app.controller('macAddressCtrl',
['$rootScope','$scope','$mdDialog','$mdMedia','appConstants','macAddressService','macAddressDailogService',function ($rootScope,$scope,$mdDialog,$mdMedia,appConstants,macAddressService,macAddressDailogService) {
	/*Get All list and populate the table*/
	$scope.macAddressList = {};

	$scope.pagination = { perPageItemCount:10, goToLastPage : false };
 	
	 $scope.cancel = function () {
	        $mdDialog.cancel();
	        
	 };

	 $scope.filters = [
		    {key: 'name', value: 'Customer Name'},
		    {key: 'invId', value: 'Purchase Order Number'},
	];
	 
	 $scope.macAddressCriteria = {};

	 $scope.selectedFilter = "";
	 
	 $scope.searchText = "";
	 
	  $scope.getMacAddressList = function(currentPage,setPagination) {
			currentPage = currentPage || 1;
			macAddressService.getMacAddressPaginationList($scope.pagination,currentPage,$scope.macAddressCriteria).then(function(response) {
				 $scope.macAddressList = response.data;
				 setPagination(response.count,currentPage);
			});
	  }
		
	  $scope.$on('getMacAddress',function(event,data) {   	
		  $rootScope.$broadcast("refreshPage");
		  $scope.cancel();
	  });
	  $scope.$broadcast('getMacAddress');
	  
	  $scope.showMacRange = function($event,macAddress,invoiceMacCount,invoiceId) {
		  let data = {
			 custName : macAddress.customerName,
			 invId : invoiceId,
			 totalMac : invoiceMacCount
		  };
		  macAddressDailogService.openMacRangeDailog($event,data,"MAC RANGE");
	  };
	  
	  
	$scope.setCriteria = function() {
		 if($scope.selectedFilter === undefined || $scope.selectedFilter === "") {
			 $scope.searchText = "";
			 $scope.macAddressCriteria = {};
		 } else{
			 $scope.macAddressCriteria = {};
			 $scope.macAddressCriteria[$scope.selectedFilter] = $scope.searchText;
		 }
		 
		 /*After this send user to first page.*/
		 $rootScope.$broadcast('goToSpecificPage',1);
	}
		 
	
}]);

})();