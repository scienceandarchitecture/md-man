(function() {
	'use strict';
/**
 * Created by Benison pc on 9/14/2016.
 */
app.controller('loginCtrl',["$scope","$location","$cookies","authService","MessageService",function($scope,$location,$cookies,authService,MessageService){
    /*Login*/
	$scope.user = {userName:$cookies.get("username"),password:""};
	$scope.rememberMeFlag = false;
	$scope.init = function() {
		console.log($cookies.get("username"));
	}
	
	$scope.init();
	
    $scope.login = function() {
    	authService.login($scope.user).then(function( data ) {
    		if(data) {
    			$scope.resetSideMenu();
    			$location.path("/purchase_order");
    			return;
    		}
    	},function(err) {
    		$scope.invalidLogin = true;
    	});
    };
    
    $scope.rememberMe = function() {
    	if($scope.rememberMeFlag) {
    		$cookies.put("username",$scope.user.userName);
    	} else {
    		$cookies.put("username");
    	}
    }
}]);
})();
