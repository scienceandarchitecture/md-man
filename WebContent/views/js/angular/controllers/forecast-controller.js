(function() {
	'use strict';
app.controller('forecastCtrl',['$scope','$rootScope','appConstants','apiDelegateService','MessageService','apiHandlerService','$mdDialog','$mdMedia','_',function($scope,$rootScope,appConstants,apiDelegateService,MessageService,apiHandlerService,$mdDialog,$mdMedia,_){

	$rootScope.forecastFlag = false;
	$scope.forecastItemVo=[];
	$scope.addButton = true;
	$scope.updateButton = false;
	$scope.newForecast = {
			estDate:new Date(),
			notes:"",
			forecastItemVo:[]
	}
	
	/*Reset Data*/
	$scope.resetForecastData = function() {
		$scope.newForecast = {
				estDate:new Date(),
				notes:"",
				forecastItemVo:[]
		}
		$scope.forecastItemVo = [];
		$rootScope.forecastFlag = false;
	}
	
	$scope.criterias = [
	    { key: "relevent", value: "Relevent"},
	    { key: "all", value: "All"}
	];
	
	$scope.selectedCriteria = "all";

	/*Get Forecast List*/	
	$scope.getForecastList = function() {
		 var options={
				  url:appConstants.apiUrl+'forecast/getForecastList',
				  method:'GET'
		  }
		  apiDelegateService.apiHandler(options)
		  .then(function(sucessResponse) {
				$scope.forecastList=sucessResponse.data.data
			}).catch(function(errorResponse) {
		         console.log(errorResponse);
		});
	};
	
	$scope.getForecastList();
	
	$scope.$on('refreshForecastList',function(event,data){
		$scope.getForecastList();
		$scope.resetForecastData();
	})
	
	/*Get Customers List*/	
	$scope.getCustomersList = function() {
		  var options = {
				  url:appConstants.apiUrl+'customers/getCustomerList',
				  method:'GET'
		  }
		  apiDelegateService.apiHandler(options)
		  .then(function(sucessResponse) {
			  	$scope.customerList = sucessResponse.data.data;
				$scope.newForecast.custName = $scope.customerList[0].name;
				console.log("selected customer"+JSON.stringify($scope.newForecast.custName));
		    }).catch(function(errorResponse) {
		         console.log(errorResponse);
		    });
	  };
	  
	  $scope.getCustomersList();

/*Get users list*/	  
	  $scope.getUsersList = function() { 
		  var options={
				  url:appConstants.apiUrl+'userRole/getUsers',
				  method:'GET'
		  }
		  apiDelegateService.apiHandler(options)
		  .then(function(sucessResponse) {
			  	$scope.usersList = sucessResponse.data.data;
				$scope.newForecast.salesMan = $scope.usersList[0].username;
				console.log("selected customer"+JSON.stringify($scope.newForecast.salesMan));
		    }).catch(function(errorResponse) {
		         console.log(errorResponse);
		    });
	  };
	  
	  $scope.getUsersList();
	 
/*Get product list*/
		$scope.getProductList = function() { 
			  var options={
					  url:appConstants.apiUrl+'products/getProductList',
					  method:'GET'
			  }
			  apiDelegateService.apiHandler(options)
			  .then(function(sucessResponse) {
				  $scope.productsList = sucessResponse.data.data;
					$scope.productName = $scope.productsList[0].psku;
					console.log("selected customer"+JSON.stringify($scope.productName));
			        console.log($scope.productList);
			    }).catch(function(errorResponse) {
			         console.log(errorResponse);
			    });
		  };
		  
		  $scope.getProductList();
		  	
/*Show add/update forecast dialog*/	
	$scope.showForecastDialog = function($event,forecast) {
		if(forecast !== undefined){
			$rootScope.forecastFlag = true;
			$scope.newForecast = _.clone(forecast);
			$scope.forecastItemVo = $scope.newForecast.forecastItemVo
		}
		dailogObject = {
				targetEvent : $event,
		        templateUrl : 'views/partials/dialogs/addForecast.html',
		        scope: $scope
			};
	 $scope.forecastDialog(dailogObject);
	}
	
/*Forecast Dialog*/	 
	 $scope.forecastDialog = function (dailogObject) {
	      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
	      $mdDialog.show({
	          controller: function(){ this.parent = dailogObject.scope; },
	          controllerAs : 'ctrl',
	          templateUrl: dailogObject.templateUrl,
	          parent: angular.element(document.body),
	          targetEvent: dailogObject.targetEvent,
	          clickOutsideToClose: false,
	          fullscreen: useFullScreen
	      })
	          .then(function (answer) {
	              $scope.status = 'You said the information was "' + answer + '".';
	          }, function () {
	              $scope.status = 'You cancelled the dialog.';
	          });
	      $scope.$watch(function () {
	          return $mdMedia('xs') || $mdMedia('sm');
	      }, function (wantsFullScreen) {
	          $scope.customFullscreen = (wantsFullScreen === true);
	      });
	  };
	  
/*Add/Update new inventory*/    
	  $scope.addUpdateForecast = function(forecastDetails){
		  forecastDetails["forecastItemVo"]= $scope.forecastItemVo
		  console.log("Updated forecast details....."+JSON.stringify(forecastDetails));
			if($rootScope.forecastFlag) {
				apiHandlerService.updateData(forecastDetails,'forecast/updateForecast','PUT','refreshForecastList');
	    	} else {
	    		apiHandlerService.addData(forecastDetails,'forecast/addForecast', 'POST','refreshForecastList' );
	    	}
	  }
	  
	  /*Add Product*/
	  $scope.addProduct = function(){
		  if($scope.quantity != undefined && $scope.quantity != "" && $scope.quantity != null){
		  $scope.addedProduct=true;
		  if($scope.forecastItemVo.length === 0){
			  $scope.forecastItemVo.push({
				  'psku':$scope.productName,
				  'quantity':$scope.quantity
			  })
		  }else{
			  if (isItemNameExits($scope.forecastItemVo,$scope.productName)){
				  _.find($scope.forecastItemVo, function (item) {
					  if(item.psku === $scope.productName){
						   item.quantity = +item.quantity + +$scope.quantity;
						  }
				  });
			  }else{ 
			  $scope.forecastItemVo.push({'psku':$scope.productName,'quantity':$scope.quantity});
		  	} 
		  }
		}
		  $scope.quantity = '';
		  $scope.productName = $scope.productsList[0].psku;
	  }

/*Find entered item name exist in array*/	  
	  function isItemNameExits(items, inventoryName) {
		  return _.find(items, function (items, index) {
		      return items.psku == inventoryName ? true : false;
		  });
	  }
	  
/*Copy selected item into respective fields*/
	  $scope.editProduct = function(index){
		  $scope.addButton=false;
		  $scope.updateButton=true;
		  localStorage.setItem("productIndex",JSON.stringify(index));
	      $scope.quantity=$scope.forecastItemVo[index].quantity;
	      $scope.productName=$scope.forecastItemVo[index].psku;
	  }
	  		  
/*Update Selected item*/	  
	  $scope.updateProduct=function(){
	    	var index=localStorage.getItem('productIndex');
	    	$scope.forecastItemVo[index].quantity=$scope.quantity;
	    	$scope.forecastItemVo[index].psku=$scope.productName;
	    	$scope.addButton=true;
	   	  	$scope.updateButton=false;
	   	  	$scope.quantity='';
	   	  	$scope.productName=$scope.productsList[0].psku;
	   	  	localStorage.removeItem('productIndex');
	    }
	  
/*Delete Added Product*/
  $scope.deleteProduct = function (index) {
    	 $scope.forecastItemVo.splice(index, 1);
    	 console.log("Deleted Item...."+ $scope.forecastItemVo[index])
    }
  
  /*Delete Product*/
	$scope.deleteForecastDialog = function($event,forecastDetails) {
		$dialogObject = {
				targetEvent:$event,
				templateUrl:'views/partials/dialogs/alertDialog.html'
		}
		$scope.title="Are you sure want to delete forecast "+forecastDetails.fcastId;
		
		/*Delete selected inventory*/
		$scope.deleteEntry = function(){
			 apiHandlerService.deleteData('forecast/deleteForecastById?fcastId='+forecastDetails.fcastId, 'DELETE','forecastList');
		  }
		$scope.showAlertDialog($dialogObject);
	}
	  
/*Alert Dialog Box*/    
    $scope.showAlertDialog = function($dialogObject) {
		var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
		$mdDialog.show({
			controller:function(){this.parent = $scope;},
			controllerAs:'ctrl',
			parent: angular.element(document.body),
			templateUrl: $dialogObject.templateUrl,
			targetEvent: $dialogObject.targetEvent,
			clickOutsideToClose: true,
			fullscreen: useFullScreen
		})  
			.then(function (answer) {
				$scope.status = 'You said the information was "' + answer + '".';
			}, function () {
				$scope.status = 'You cancelled the dialog.';
		});
			$scope.$watch(function () {
				return $mdMedia('xs') || $mdMedia('sm');
			}, function (wantsFullScreen) {
				$scope.customFullscreen = (wantsFullScreen === true);
		});
    }
    
/*Cancel dialog box*/    
	  $scope.cancel = function () {
	      $mdDialog.cancel();
	      $scope.resetForecastData();
	  };
	  
	  $scope.searchByType = function(forecast) {
		  var currentDate = new Date(); 
			var compareDate;
			switch ($scope.selectedCriteria) {
				case "all":
					return true;
				case "relevent":
					compareDate = new Date.createFromMysql(forecast.estDate);
					if(compareDate.getTime() > currentDate.getTime() ) {
						return true;
					}
					return false;
			}
	  }
}]);
})();