(function() {
	'use strict';
	app.controller('userCtrl',['$rootScope','$scope','$mdDialog','$mdMedia','appConstants','apiDelegateService','apiHandlerService','_','$filter','userService','userDailogService','validateResponseService',
		function ($rootScope,$scope,$mdDialog,$mdMedia,appConstants,apiDelegateService,apiHandlerService,_,$filter,userService,userDailogService,validateResponseService) {
		/*Get All list and populate the table*/
		$scope.userList = [];
		
		$scope.selectedFilter = null;
		
		$scope.filters = [{key:"username",value:"User name"},{key:"email",value:"Email Address"},{key:"fullname",value:"Full Name"},{key:"roles",value:"Roles"}];
		
		$scope.pagination = { perPageItemCount : 10, goToLastPage : false};
		
		$scope.initContoller = function() {
			$scope.userList = [];
		};
		
		$scope.searchText = "";
		
		$scope.selectedCriteria = {};
		
		 $scope.totalRecords = null;
		
		$scope.getUserList = function(currentPage,setPagination) {
			  currentPage = currentPage || 1;
			  userService.getUserPaginationList($scope.pagination,currentPage,$scope.selectedCriteria).then(function(response) {
				  //Get the total count from the server
				  $scope.userList =  response.data;
				  
				  $scope.totalRecords = response.count;
				  
				  setPagination(response.count,currentPage);
				  //If it a add senario than send it to last page.
			  });
		}
		
		$scope.$on('getUsers',function(event,data) {  
			if( data.data != undefined && data.data.responseCode == 99 ) {
				userDailogService.resetViewsArray();
			}
			if(userDailogService.getUpdateFlag()) {
				$rootScope.$broadcast("refreshPage");
			} else {
				if(!validateResponseService.isUpdateSuccessful(data.data.responseCode)) {
					  return false;
				}
				if( parseInt($scope.totalRecords) % parseInt($scope.pagination.perPageItemCount) === 0 ) { 
					$rootScope.$broadcast("goToSpecificPage",Math.ceil( $scope.totalRecords / $scope.pagination.perPageItemCount ) + 1 );
				} else {
					$rootScope.$broadcast("goToLastPage");
				}
			}
			$scope.checkResponseCode(data.data.responseCode);
		});
		
		$scope.$on('refreshAfterDelete',function(event,data) {
				if(!validateResponseService.isUpdateSuccessful(data.data.responseCode)) {
					  return false;
				}
				if(parseInt($scope.totalRecords) % parseInt($scope.pagination.perPageItemCount) === 1) { 
					$rootScope.$broadcast("goToSpecificPage",Math.ceil( $scope.totalRecords / $scope.pagination.perPageItemCount ) - 1 );
				} else {
					$rootScope.$broadcast("refreshPage");
				}
			$scope.checkResponseCode(data.data.responseCode);
		});
		
		$scope.addUser = function ($event,userDetails) {
			userDailogService.openUserDailog($event,'getUsers',userDetails);
		}
		
		$scope.deleteUser = function($event,userDetails) {
			userDailogService.openDeleteUserDialog($event,'refreshAfterDelete',userDetails);
		};
		
		$scope.setCriteria = function() {
			$scope.selectedCriteria = {};
			if($scope.selectedFilter != null) {
				$scope.selectedCriteria[$scope.selectedFilter] = $scope.searchText;
			}
			$rootScope.$broadcast("refreshPage");
		}
	}]);

})();
