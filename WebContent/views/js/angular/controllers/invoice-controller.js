(function() {
	'use strict';
angular.module('mdApp').controller('invoiceCtrl',['$scope','$rootScope','$location','$mdMedia','$mdDialog','$anchorScroll','$q','appConstants','apiDelegateService','apiHandlerService','paymentDailogService','productDailogService','$filter','productService','purchaseOrderService','paymentService','shipOrderService','customerService','termService','userService','trackingService','wordWrapService','purchaseOrderDailogService','jobOrderDailogService','macAddressDailogService','validateResponseService',function($scope,$rootScope,$location,$mdMedia,$mdDialog,$anchorScroll,$q,appConstants,apiDelegateService,apiHandlerService,paymentDailogService,productDailogService,$filter,productService,purchaseOrderService,paymentService,shipOrderService,customerService,termService,userService,trackingService,wordWrapService,purchaseOrderDailogService,jobOrderDailogService,macAddressDailogService,validateResponseService){
	$scope.newInvoice;
	
	$scope.customer = {
		name:"",
		address:"",
		shipAddress:"",
		instructions:""
	};
	var dialogObject;
	
	$scope.newItem;
	
	$scope.subDropDownList = [{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9},{value:10},{value:11},{value:12},{value:13},{value:14},{value:15},{value:16},{value:17},{value:18},{value:19},{value:20},{value:21},{value:22},{value:23},{value:24},{value:25},{value:26},{value:27},{value:28},{value:29},{value:30},{value:31},{value:32},{value:33},{value:34},{value:35},{value:36},{value:37},{value:38},{value:39},{value:40},{value:41},{value:42},{value:43},{value:44},{value:45},{value:46},{value:47},{value:48},{value:49},{value:50},{value:51},{value:52},{value:53},{value:54},{value:55},{value:56},{value:57},{value:58},{value:59},{value:60},{value:61},{value:62},{value:63},{value:64},{value:65},{value:66},{value:67},{value:68},{value:69},{value:70}];
	
	$scope.invoiceList = [];
	
	$scope.showTerm = false;
	$scope.customerFlag = false;
	$scope.showSearch = false;
	$scope.invoiceFlag = false;
	$scope.invoiceitems = [];
	$scope.addedProduct = false;
	$scope.updateButton = false;
	$scope.saveButton = true;
	$scope.todayDate = new Date();
			
	$scope.pagination = { perPageItemCount:10, goToLastPage:false};
	/*Reset data*/
	$scope.resetInvoiceData = function() {
		$scope.newInvoice = {
			itemTotal:0,
			amount:0,
			discount:0,
			amountbeforeDiscount:0,
			shipping:0.00,
			status:0,
			invJSDate: new Date(),
			dueJSDate: new Date(),
			invDate:"",
			dueDate:""
		};
		//$scope.invoiceitems = [];
		$scope.customerId = "";
		$scope.newInvoice.terms = "";
		$scope.todayDate = new Date();
		$scope.invoiceFlag = false;
		$scope.showTerm = false;
	}
	
	$scope.subItems;
	  	
  	$scope.invoiceItemIds = {};
  	
	$scope.criteria = {};
	/*Filters */
	 $scope.selectedFilter = undefined;
	 
	 $scope.filterTextFieldVisibility = false;
	 
	 $scope.filterTextField = "";
	
	 $scope.filters = [
	        { key: "custName", value: "By Customer" },
	        { key: "salesRep", value: "By SalesPerson" },
	        { key: "invId", value: "By Purchase Order Number" }
	 ];
	 
	 $scope.selectedShowFilter = "all";
	 
	 $scope.showFilters = [
		 	{ key: "open", value : 'Open' },
	        { key: "issued_this_month", value : 'Issued This Month' },
	        { key: "due_this_month", value : 'Due This Month' },
	        { key: "issued_this_year", value : 'Issued This Year' },
	        { key: "due_this_year", value : 'Due This Year' },
	        { key: "all", value : 'All' }
	 ];
	
	 $scope.totalRecords = null;
	 
	 
	 purchaseOrderDailogService.setTermList();
	 purchaseOrderDailogService.setUserList();
	// purchaseOrderDailogService.setProductList();
	// purchaseOrderDailogService.setSubProductList();
	 
	 /*Get Invoice List*/
	 $scope.getInvoiceList = function(currentPage,setPagination) {
		 currentPage = currentPage || 1;
		 purchaseOrderService.getListWithPagination($scope.pagination,currentPage,$scope.criteria).then(function(response) {
			//Get the total count from the server
			 $scope.invoiceList =  response.data;
			 //Total records
			 $scope.totalRecords = response.count;
			 setPagination(response.count,currentPage);
		});
	 };
		
	 $scope.$on('refreshAfterAddPayment',function( event , data ) {
				  $rootScope.$broadcast("refreshPage");
				  $scope.checkResponseCode(data.data.responseCode);
	 });
	 	/*Refresh Invoice list*/
		$scope.$on('refreshInvoiceList',function( event , data ) {
			//Now Pagination is involved.
			//If it a add senario than send it to last page.
			  if(purchaseOrderDailogService.getUpdateFlag()) {
				  $rootScope.$broadcast("refreshPage");
			  } else {
				  if(!validateResponseService.isUpdateSuccessful(data.data.responseCode)) {
					  return false;
				  }
				  if( parseInt($scope.totalRecords) % parseInt($scope.pagination.perPageItemCount) === 0 ) { 
						$rootScope.$broadcast("goToSpecificPage",Math.ceil( $scope.totalRecords / $scope.pagination.perPageItemCount ) + 1 );
					} else {
						$rootScope.$broadcast("goToLastPage");
					}
			  }
			
			purchaseOrderDailogService.setTermList();
			$scope.checkResponseCode(data.data.responseCode);
		});
		
		$scope.$on('refreshAfterDelete',function(event,data) {
				if(!validateResponseService.isUpdateSuccessful(data.data.responseCode)) {
					  return false;
				}
				if(parseInt($scope.totalRecords) % parseInt($scope.pagination.perPageItemCount) === 1) { 
					$rootScope.$broadcast("goToSpecificPage",Math.ceil( $scope.totalRecords / $scope.pagination.perPageItemCount ) - 1 );
				} else {
					$rootScope.$broadcast("refreshPage");
				}
			$scope.checkResponseCode(data.data.responseCode);
		});
		
		/*Get shipped order list by invoice id*/		
		$scope.getShippedOrdersList = function(invoiceId) {
			shipOrderService.getShippedOrderLists(invoiceId).then(function(shippedOrderList){
				$scope.shippedOrdersList = shippedOrderList;
			});
		};
		
	
		/*Show add payment from invoice dialog*/		
		$scope.showaddPaymentFromInvoive = function($event,invoice) {
			paymentDailogService.openPaymentDailog($event,invoice);
		}

		/*View Shipped item dialog*/	
		$scope.showShippedOrderByInvoiveDialog = function($event,purchaseOrderId) {
			purchaseOrderDailogService.showShippedOrderByInvoiveDialog($event,purchaseOrderId);
		}
		
		/*View Job dialog*/	
		$scope.showJobOrderByInvoiveDialog = function($event,purchaseOrderId) {
			jobOrderDailogService.openJobOrderByPurchaseOrderIdDialog($event,purchaseOrderId);
		}
		
		/* show Payment from Invoice dialog */
		$scope.showPaymentsFromInvoiveDialog = function($event,purchaseOrderId){
			purchaseOrderDailogService.showPaymentsFromInvoiveDialog($event,purchaseOrderId);
		}
		/*Show tracking no dialog*/
		$scope.showTrackingItemsByInvoiveDialog = function($event,purchaseOrderId) {
			purchaseOrderDailogService.showTrackingItemsByInvoiveDialog($event,purchaseOrderId);
		}
		
		
		/*View Shipped item dialog*/	
		$scope.showShippedItemByInvoiveDialog = function($event,invoiceId) {
			$scope.shippedInvoiceId = invoiceId;
			dialogObject = {
					targetEvent:$event,
					templateUrl:'views/partials/dialogs/shippedItemsFromInvoive.html'
			}
			$scope.showAlertDialog(dialogObject);
			$scope.getShippedItemsList(invoiceId);
		}

		/*Show add/update invoice dialog*/
		$scope.showInvoiceDialog = function($event,invoice) {
			purchaseOrderDailogService.showInvoiceDialog($event,'refreshInvoiceList',invoice);
		}
		
		/*Show product dialog*/
		$scope.showProductDialog = function($event){
			productDailogService.openProductDailog($event,'refreshProducts');
		}
				 
		/*Toggle Text and Dropdown box*/
		$scope.toggle = function(){
			return $scope.showTerm = !$scope.showTerm;
		}
					  
		/*Add new customer*/ 
		$scope.addUpdateCustomer = function(){
			if( $scope.customerFlag ) {
				apiHandlerService.updateData($scope.customer,'customers/editCustomerRecord', 'PUT','refreshCustomerList');
			} else {
				apiHandlerService.addData($scope.customer,'customers/addCustomers', 'POST','refreshCustomerList');
			}
		}

		/*Display Customer details based on selection*/	
		$scope.selectedCustomerDetails = function(selectedCustomer) {
			if(selectedCustomer != null) {
				$scope.newInvoice.billTo = selectedCustomer.address;
				$scope.newInvoice.shipTo = selectedCustomer.shipAddress;
			}
		}
				
		/*show delete dialog*/	
		$scope.deleteInoiceDialog = function($event,purchaseOrder) {
			purchaseOrderDailogService.openDeleteProductDialog($event,'refreshAfterDelete',purchaseOrder);
		}
			    
			/*Cancel Dialog*/ 	
			$scope.cancel = function (isDelete) {
				$mdDialog.cancel();
			};
			
			$scope.$on('refreshCustomerList',function(event,data) {
				purchaseOrderDailogService.setCustomerDetails();
				switch( data.data.responseCode ) {
					case 201:
						$mdDialog.cancel();
						break;
					case 200:
						$mdDialog.cancel();
					break;
				}
			});
			
			 $scope.$on('closeDailog',function() {
				 $scope.cancel();
			 });

			 /*close invoice payment*/	
			$scope.closeInvoice = function(invoice) {
				var tempInvoice; 
				tempInvoice = angular.copy(invoice);
				tempInvoice.status = "closed";
				apiHandlerService.updateData(tempInvoice,'invoices/updateInvoiceById', 'PUT', 'refreshInvoiceList');
			}
		
			/*Open invoice payment*/	
			$scope.openInvoice = function(invoice) {
				var tempInvoice; 
				tempInvoice = angular.copy(invoice);
				tempInvoice.status = "open";
				apiHandlerService.updateData(tempInvoice,'invoices/updateInvoiceById', 'PUT', 'refreshInvoiceList');
			}
			
			/*Selected Dialog*/
			$scope.changeTextFieldVisibility = function() {
				if( $scope.selectedFilter != undefined ) {
					$scope.filterTextFieldVisibility = true;
				} else {
					$scope.filterTextFieldVisibility = false;
					$scope.filterTextField = "";
				}
				
			};
		 
		 $scope.getInvoiceListForCSV = function(){
		 	var deferred = $q.defer();
			purchaseOrderService.getPurchaseOrderListForCSV($scope.totalRecords,$scope.criteria).then(function(purchaseOrderList) {
				angular.forEach(purchaseOrderList,function(invoice) {
						invoice.invDate = $filter('dateFilter')(invoice.invDate);
						invoice.dueDate = $filter('dateFilter')(invoice.dueDate);
				});
				deferred.resolve(purchaseOrderList);
			});
			return deferred.promise;
		}
		 
		 $scope.downloadInvoicePdf = function(currentInvoice) {
			 purchaseOrderService.downloadInvoicePdf(currentInvoice);
		 };
		 
		 $scope.setCriteria = function() {
			 if($scope.selectedFilter === undefined) {
				 $scope.criteria = {};
			 } else{
				 $scope.criteria = {};
				 $scope.criteria[$scope.selectedFilter] = $scope.filterTextField;
			 }
			
			 var typeObj = purchaseOrderService.setSerachTypeCriteria($scope.selectedShowFilter);
			 if( typeObj !== null) {
				 $scope.criteria[typeObj.key] = typeObj.value.split(" ")[0];
				 $scope.criteria.poDateFlag = typeObj.filterType;
			 }
			 /*After this send user to first page.*/
			 $rootScope.$broadcast('goToSpecificPage',1);
		 }
		 
		 $scope.openMacRangeDailog = function($event,purchaseOrdeId,customerName) {
			 var macRange = {
					 invId : purchaseOrdeId,
					 custName : customerName,
					 totalMac : 0
			 };
			 macAddressDailogService.openMacRangeDailog($event,macRange,"PURCHASE ORDER DETAILS");
		 }
		 
}]);

})();