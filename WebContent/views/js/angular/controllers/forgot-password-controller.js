(function() {
	'use strict';
	app.controller('forgotCtrl',['$scope','appConstants','MessageService','authService','$timeout','$location','$mdToast',function($scope,appConstants,MessageService,authService,$timeout,$location,$mdToast){
		$scope.forgotData = {
		};
		$scope.forgotPassword = function() {
			authService.forgotPassword($scope.forgotData).then(function(response) {
				if(response.data.responseCode == 201) {
					$mdToast.show($mdToast.simple()
			                .content(response.data.message)
			                .highlightAction(false)
			                .position('right top')
			                .hideDelay(7000));
					$timeout(function() {
						$location.path("/login");
					},8000);
				} else {
					MessageService.showAlert(response.data);
				}
			},function(error) {
				console.log(error);
			});
		};
	}]);
})();