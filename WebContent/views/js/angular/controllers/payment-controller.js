(function() {
	'use strict';
app.controller('paymentCtrl',['$scope','$mdDialog','$mdMedia','appConstants','apiDelegateService','apiHandlerService','MessageService','authService','paymentService','paymentDailogService','$rootScope','validateResponseService',function($scope,$mdDialog,$mdMedia,appConstants,apiDelegateService,apiHandlerService,MessageService,authService,paymentService,paymentDailogService,$rootScope,validateResponseService) {

	$scope.counts = {
		total:0,
		totalopen:0,
		totalclose:0
	};
	$scope.filters = [
	    {key: 'open', value: 'Open'},
	    {key: 'all', value: 'All'},
	    {key: 'this_month', value: 'This Month'},
	    {key: 'this_year', value: 'This Year'}
	];
	
	$scope.selectedFilter = "all";
	$scope.pagination = {perPageItemCount:10};
	$scope.paymentCriteria = {};

	$scope.totalRecords = null;
/*Get total*/
	$scope.getTotal = function() {
		$scope.counts = {
				total: 0,
				totalopen: 0,
				totalclose: 0
		};
		$scope.counts.total = $scope.paymentList.length;
		angular.forEach($scope.paymentList,function(payment) {
			if(payment.state.toLowerCase() == 'open') {
				$scope.counts.totalopen++;
			} else if(payment.state.toLowerCase() == 'closed') {
				$scope.counts.totalclose++;
			}
		});
	};
	
	
	/*Get Payment List*/
	  $scope.getPaymentList = function(currentPage,setPagination) { 
		  currentPage = currentPage || 1;
		  paymentService.getPaymentListWithPagination($scope.pagination,currentPage,$scope.paymentCriteria).then(function(response) {
				//Get the total count from the server
			  $scope.paymentList =  response.data;
			  $scope.getTotal();
			  $scope.totalRecords = response.count;
			  setPagination(response.count,currentPage);
				 //If it a add senario than send it to last page.
				
			});
	  };
  
  //$scope.getPaymentList();
  
/*Refresh paymentList*/
	  $scope.$on('refreshPaymentList',function(event,data) {
		  if(!paymentDailogService.getUpdateFlag() ) {
				if(!validateResponseService.isUpdateSuccessful(data.data.responseCode)) {
					  return false;
				}
			  	if( parseInt($scope.totalRecords) % parseInt($scope.pagination.perPageItemCount) === 0 ) { 
					$rootScope.$broadcast("goToSpecificPage",Math.ceil( $scope.totalRecords / $scope.pagination.perPageItemCount ) + 1 );
				} else {
					$rootScope.$broadcast("goToLastPage");
				}
		  }
		  $scope.checkResponseCode(data.data.responseCode);
	  });
  
		$scope.$on('refreshAfterDelete',function(event,data) {
				if(!validateResponseService.isUpdateSuccessful(data.data.responseCode)) {
					return false;
				}
				if(parseInt($scope.totalRecords) % parseInt($scope.pagination.perPageItemCount) === 1) { 
					$rootScope.$broadcast("goToSpecificPage",Math.ceil( $scope.totalRecords / $scope.pagination.perPageItemCount ) - 1 );
				} else {
					$rootScope.$broadcast("refreshPage");
				}
			$scope.checkResponseCode(data.data.responseCode);
		});
  
/*Show Payment Dialog*/  
  $scope.showPaymentDialog = function($event){
	  paymentDailogService.openAddPaymentDialog($event,'refreshPaymentList');
  };
  
/*Show delete payment dialog*/  
  $scope.deletePaymentDialog = function($event,paymentDetails){
	  paymentDailogService.openDeletePaymentDialog($event,'refreshAfterDelete',paymentDetails);
  };

  
  $scope.setCriteria = function() {
	  $scope.paymentCriteria = {};
	  
		 if($scope.selectedFilter === undefined) {
			 return;
		 }
		 var typeObj = paymentService.setSerachTypeCriteria($scope.selectedFilter);
		 if( typeObj !== null) {
			 $scope.paymentCriteria[typeObj.key] = typeObj.value.split(" ")[0];
		 }
		 /*After this send user to first page.*/
		 $rootScope.$broadcast('goToSpecificPage',1);
  }
  
  
}]);
})();