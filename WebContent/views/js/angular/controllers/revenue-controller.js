(function() {
	'use strict';
app.controller('revenueCtrl',['$rootScope','$scope','$mdDialog','appConstants','apiDelegateService','apiHandlerService','MessageService','$mdMedia','revenueService',function($rootScope,$scope,$mdDialog,appConstants,apiDelegateService,apiHandlerService,MessageService,$mdMedia,revenueService) {

	$scope.selectedFilter = undefined;
	 
	 $scope.filterTextFieldVisibility = false;
	 
	 $scope.filterTextField = "";
	 
	 $scope.totalAmount = 0.0;
		
	 $scope.filters = [
	        { key: "name", value: "By Customer" },
	        { key: "salesRep", value: "By SalesPerson" }
	        ];
	 
	 $scope.selectedShowFilter = "";
	 
	 $scope.showFilters = [
		    {key: "current_month", value: "Current Month"},
	        {key: "month", value: "Month"},
	        {key: "quarter", value: "Quarter"},
	        {key: "year", value: "Year"}
	 ];
	 $scope.criteria ={revenueDateFlag:0};
	 
	 $scope.pagination = {perPageItemCount:10,goToLastPage:false};
	 
	 $scope.selectedMonth = "";
/*Get Revenue List*/
	 
	 $scope.getRevenueList = function(currentPage,setPagination) {
		  currentPage = currentPage || 1;
		  revenueService.getRevenuePaginationList($scope.pagination,currentPage,$scope.criteria).then(function(response) {
				//Get the total count from the server
			  	$scope.revenueList  =  response.data.revenueVoList;
			  	 $scope.totalAmount = response.data.amount;
			  	setPagination(response.count,currentPage);
		  });
	};
  
/*Refresh Revenue List*/  
  $scope.$on('refreshRevenueList',function(event,data){    	
	  $scope.getRevenueList();
  });
  
	$scope.search = function(invoice) {
		switch ($scope.selectedFilter) {
			case undefined:
				return true;
			case "name":
				if(invoice.customerName.toLowerCase().indexOf($scope.filterTextField.toLowerCase()) !== -1) {
					return true;
				}
				return false;
			case "salesRep":
				if(invoice.salesRep.toLowerCase().indexOf($scope.filterTextField.toLowerCase()) !== -1) {
					return true;
				}
				return false;
		}
	}
	

	$scope.changeTextFieldVisibility = function() {
		if( $scope.selectedFilter != undefined ) {
			$scope.filterTextFieldVisibility = true;
		} else {
			$scope.filterTextFieldVisibility = false;
			$scope.filterTextField = "";
		}
	};
	
	$scope.setCriteria = function() {
		 if($scope.selectedFilter === undefined) {
			 $scope.criteria ={revenueDateFlag:0};
		 } else {
			 $scope.criteria ={revenueDateFlag:0};
			 $scope.criteria[$scope.selectedFilter] = $scope.filterTextField;
		 }
		 var response = revenueService.setSerachTypeCriteria($scope.selectedShowFilter,$scope.selectedMonth);
		 if( response !== null ) {
				 $scope.criteria["payDate"] = response.acknowledged;
				 $scope.criteria["revenueDateFlag"] = response.revenueDateFlag;
		}
		 /*After this send user to first page.*/
		 $rootScope.$broadcast('goToSpecificPage',1);
	 }
	
}]);
})();