'use strict';

/**
 * @ngdoc overview
 * @name Mesh Dynamics
 * @description
 * @author - Benison Technologies Pvt. Ltd.
 * This is the main module of the application.
 */

/***
 * init Mesh Dynamics app
 */
var app = angular.module('mdApp', ['ngRoute', 'ngMaterial', 'ngMdIcons', 'ngScrollbar','ngMessages','angularMoment','ngSanitize','ngCookies','ngRoute.middleware','ngCsv','angular-loading-bar','angular-js-xlsx']);

/***
 * App Config
 */
app.config(['$mdThemingProvider','$routeProvider','$middlewareProvider','$cookiesProvider','cfpLoadingBarProvider',function ($mdThemingProvider,$routeProvider,$middlewareProvider,$cookiesProvider,cfpLoadingBarProvider) {
	$mdThemingProvider.theme("success-toast");
	$mdThemingProvider.theme("error-toast");
	
	cfpLoadingBarProvider.includeSpinner = false;
	$middlewareProvider.map({
		'isLoggedIn': function isLoggedInAndAllowed() {
			var $cookies;
			  angular.injector(['ngCookies','ngRoute']).invoke(['$cookies', function(_$cookies_) {
			    $cookies = _$cookies_;
			  }]);
			 if($cookies.get("currentUser") == undefined || $cookies.get("currentUser") == null) {
				 this.redirectTo('/login');
			 } 
			 this.next();
        },
		'isLoggedInAndAllowed': function isLoggedInAndAllowed() {
			var $cookies;
			  angular.injector(['ngCookies','ngRoute']).invoke(['$cookies', function(_$cookies_) {
			    $cookies = _$cookies_;
			  }]);
			 if($cookies.get("currentUser") == undefined || $cookies.get("currentUser") == null) {
				 this.redirectTo('/login');
			 } else {
				 var currentUser = angular.fromJson($cookies.get("currentUser"));
				 var views = angular.fromJson(currentUser.views);
				 if(views.indexOf(this.route) === -1) {
					 this.redirectTo("/"+views[0].replace(" ","_").toLowerCase());
				 }
			 }
			 this.next();
        },
        'ifLoggedin': function ifLoggedIn() {
        	 var $cookies;
			  	angular.injector(['ngCookies','ngRoute']).invoke(['$cookies', function(_$cookies_) {
			  		$cookies = _$cookies_;
			  	}]);
			  	
        	 if($cookies.get("currentUser") !== undefined) {
   			  	var currentUser = angular.fromJson($cookies.get("currentUser"));
   			  	var views = angular.fromJson(currentUser.views);
   			  	this.redirectTo("/"+views[0].replace(" ","_").toLowerCase());
        	 }
        	 this.next();
        }
	});
    $routeProvider

        // route for the login page
        .when('/login', {
            templateUrl: 'views/partials/login.html',
            controller: 'loginCtrl',
           middleware: 'ifLoggedin'
        })
 
        .when('/purchase_order', {
            templateUrl: 'views/partials/invoices.html',
            name: 'Purchase Order',
            icon: 'receipt',
            controller: 'invoiceCtrl',
            middleware: 'isLoggedInAndAllowed'
        })

        .when('/payments', {
            templateUrl: 'views/partials/payment.html',
            name: 'Payments',
            icon: 'payment',
            controller:'paymentCtrl',
            middleware: 'isLoggedInAndAllowed'
        })

        .when('/revenue', {
            templateUrl: 'views/partials/revenue.html',
            name: 'Revenue',
            icon: 'trending_up',
            controller:'revenueCtrl',
            middleware: 'isLoggedInAndAllowed'
        })

        .when('/inventory', {
            templateUrl: 'views/partials/inventory.html',
            name: 'Inventory',
            icon: 'store',
            controller:'inventoryCtrl',
            middleware: 'isLoggedInAndAllowed'
        })

        .when('/products', {
            templateUrl: 'views/partials/products.html',
            name: 'Products',
            icon: 'local_offer',
            controller:'productCtrl',
            middleware: 'isLoggedInAndAllowed'
        })

        .when('/forecast', {
            templateUrl: 'views/partials/forecast.html',
            name: 'Forecast',
            icon: 'timelapse',
            controller:'forecastCtrl',
            middleware: 'isLoggedInAndAllowed'
        })

        .when('/job_orders', {
            templateUrl: 'views/partials/job_orders.html',
            name: 'Job Orders',
            controller:'jobOrderCtrl',
            icon: 'verified_user',
            middleware: 'isLoggedInAndAllowed'
        })

        .when('/ship_orders', {
            templateUrl: 'views/partials/ship_orders.html',
            name: 'Ship Orders',
            controller:'shipOrderCtrl',
            icon: 'local_shipping',
            middleware: 'isLoggedInAndAllowed'
        })

        .when('/po_summary', {
            templateUrl: 'views/partials/mac_address.html',
            name: 'PO Summary',
            icon: 'router',
            controller:'macAddressCtrl',
            middleware: 'isLoggedInAndAllowed'

        })
        .when('/image_generation', {
            templateUrl: 'views/partials/flashing.html',
            name: 'Image Generation',
            icon: 'publish',
            middleware: 'isLoggedInAndAllowed'
        })
		 .when('/manage_user', {
		    templateUrl : 'views/partials/userManagement.html',
		    name : 'Manage User',
		    icon : 'people',
		    controller : 'userCtrl',
		    middleware : 'isLoggedInAndAllowed'
		})
        .when('/change_password', {
            templateUrl: 'views/partials/change-psw.html',
            middleware: 'isLoggedIn',
            name:'Change Password',
            icon:'Seeting',
            controller:'changePasswordController'
         })
         .when('/forgot_password', {
            templateUrl: 'views/partials/forgot-password.html',
            name:'Forgot Password',
            controller:'forgotCtrl'
         })
        .otherwise({
        redirectTo : '/login',
        name: 'Login Controller',
        controller:'loginCtrl'
    });
}]);


app.run(function ($rootScope, $location, $route, appConstants) {
	appConstants.urlPath = window.location.origin + "/MDManufacturer/";
	$rootScope.appConstants = appConstants;
	
});
