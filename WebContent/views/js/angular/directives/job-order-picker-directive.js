(function() {
	'use strict';
	app.directive('jobOrderPicker', function (MessageService) {
		return {
			restrict : 'E',
			replace : true,
			templateUrl : "views/partials/components/job-order-picker.html",
			$scope: true,
			link : function($scope,ele,attr) {
				$scope = $scope.ctrl.parent;
				
				 $scope.newItem;
				 $scope.selectedItem = "";
				 $scope.addButton=true;
				 $scope.updateButton = false;
				 $scope.addedItems=false;
				 $scope.subItemQtyValidation;		
				
				 
				 $scope.resetItem = function() {
					$scope.newItem = {
								invoiceItemId : "",
							quantity : "",
						};
					$scope.jobItems = [];
					$scope.addedItems = false;
					$scope.jobItems;
					$scope.selectedItem = "";
					$scope.addButton = true;
					$scope.updateButton = false;
				 }
				 
				 
				 
				 /*Add one or more items*/	  
				 $scope.addItem = function() {
					 	$scope.addedItems = true;
						if($scope.jobItems.length === 0) {
						
			            } else {
							if (isItemNameExits($scope.jobItems,$scope.selectedItem)){
								 _.find($scope.jobItems, function (item) {
			                    if(item.invoiceItemId === $scope.selectedItem){
			                        item.quantity = +item.quantity + +$scope.quantity;
			                    }});
						}
					}
					$scope.resetItem();
					$scope.quantity = '';
					$scope.selectedItem = $scope.jobItems[0].invoiceItemId;
			    }
	
				 	/*Find entered item name exist in array*/
				    function isItemNameExits(items, inventoryName) {
						return _.find(items, function (items, index) {
						    return items.invoiceItemId == inventoryName ? true : false;
				        });
					}
				    
				    /*Copy selected item into respective fields*/
					  $scope.editItem = function(invoiceItem) {
						  $scope.addButton = false;
						  $scope.updateButton = true;
						  $scope.newItem = angular.copy(invoiceItem);
					}
					  
					  /*Update Selected item*/  
					  $scope.updateItem = function() {
						  	$scope.addButton = true;
					   	  	$scope.updateButton = false;
					   	  	var index = 0;
					   	  	angular.forEach( $scope.jobItems , function( invoiceItem ) {
					   	  		if(invoiceItem.invoiceItemId === $scope.newItem.invoiceItemId) {
					   	  			//Now it's updated.
					   	  			$scope.newItem.isInValid = false;
					   	  			$scope.jobItems[index] = $scope.newItem;
					   	  			
					   	  			return;
					   	  		}
					   	  		index++;
					   	  	});
					   	  	$scope.newItem = {
									invoiceItemId:"",
									quantity:"",
							};
					   	  	/*Call Parent function after update
					   	  	$scope.setValidationToSecificValueForJobItems();*/
					  }
					  
					  /*Delete Selected Item*/
					  $scope.deleteItem = function (index) {
					    	 $scope.jobItems.splice(index, 1);
					    	 if( $scope.jobItems.length == 0 ) {
					    		 $scope.jobItemErrorMessage = "Atleast keep one job item to proceed this Job Order.";
					    	 }
					    	 /*
					    	$scope.setValidationToSecificValueForJobItems();*/
					  }
					  
			}
		};
	});
})();