(function() {
	'use strict';
	/**
	 * Created by Benison pc on 9/11/2016.
	 */
	app.directive('sideNav',function(routeNavigation,authService,$mdSidenav,$route,$rootScope){
	    return {
	        restrict: 'E',
	        templateUrl: 'views/partials/components/sidenav.html',
	        controller: function ($scope,$mdSidenav) {
	        	$scope.routes = [];
	        	$rootScope.$route = $route;
	        	$scope.activeRoute = routeNavigation.activeRoute;
	            $scope.currentUser = authService.currentUser();
	            /*TODO Copied from sidenav service. Try to optimize it.*/
	            var views = angular.fromJson($scope.currentUser.views);
	            var alreadyAddedViews = [];
	         
	            angular.forEach($route.routes, function (route, path) {
	            	if (route.name) {
	                	  if( views.indexOf(route.name) !== -1 && alreadyAddedViews.indexOf(route.name) === -1 ) {
	                		$scope.routes.push({
	        	                path: path,
	        	                name: route.name,
	        	                icon:route.icon
	                			});
	                		alreadyAddedViews.push(route.name);
	                	}
	                	  
	                }
	            });
	        	$scope.menu5 = ["Change Password","Logout"];
	        	angular.forEach($scope.route, function (route, path) {
	            	if($scope.menu5.indexOf(route.name) === -1){
	            		$scope.routes.push(route);
	            	}
	            });
	            
	        	
	        	$scope.menu4 = [
	                {
	                    name: 'Change Password',
	                    icon: 'settings',
	                    path: '/change_password'
	                },
	                {
	                    name: 'Logout',
	                    icon: 'power_settings_new',
	                    path: ''
	                }
	            ];
	        	
	        	/*Toggle side navigation*/
	            $scope.toggleSidenav = function(menuId) {
	                $mdSidenav(menuId).toggle();
	            };
	        }
	    };
	});
})();	