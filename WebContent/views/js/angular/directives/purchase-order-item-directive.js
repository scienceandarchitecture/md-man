(function() {
	'use strict';
	app.directive('purchaseOrderItem',function() {
		return {
			templateUrl: 'views/partials/components/purchase-order-item.html',
			restrict: 'E',
			scope: false,
			controller:["$scope","$mdDialog","productService",function($scope,$mdDialog,productService) {
				$scope.newItem;

				$scope.subItems;
				$scope.productList= [];
				
				$scope.invoiceItemSubProducts;
				
				$scope.selectedSubItemArray;

				$scope.dropdown_models;			  	
			  	
				$scope.invoiceItemIds = {};

				$scope.updateButton = false;
				
				$scope.saveButton = true;
				
				$scope.subItemValidation = {APP:0,VLAN:0}; 
				
				$scope.resetItem = function() {
					$scope.newItem = {
									psku : "",
									quantity : "",
									price : "",
									extraDesc : "",
									invoiceItemSubProducts : []
					 };
					$scope.selectedSubItemArray = [];
					$scope.dropdown_models = {};
					$scope.invoiceItemSubProducts = [];
					$scope.invoiceItemIds = {};
					
					$scope.subItemValidation = {APP:0,VLAN:0}; 
					 //clear form validation.
				}
				//Temp Code
				$scope.resetItem();
				/*Add new items*/  
				$scope.addItem = function() {
					$scope.ctrl.addedProduct = true;
					    if($scope.ctrl.invoiceitems.length === 0) {
					    	$scope.ctrl.invoiceitems.push($scope.newItem);
					    } else {
						     var isProductAvailable = false;
							   _.find($scope.ctrl.invoiceitems, function (item) {
								 if(item.psku === $scope.newItem.psku) {
									if( isSubProductEqual( item.invoiceItemSubProducts , $scope.newItem.invoiceItemSubProducts ) ) {
										item.quantity = +item.quantity + +$scope.newItem.quantity;
										item.price = +$scope.newItem.price;
										isProductAvailable = true;
									}
								 }
							   });
							   if(!isProductAvailable) {
								   $scope.ctrl.invoiceitems.push($scope.newItem);
							   }
					    }
					    $scope.ctrl.calculateTotalAndAmount();
					    $scope.resetItem();
				}
				
				/*Get Product List*/
				$scope.getProductList = function() {
					productService.getProductsList().then(function(productList){
						$scope.productList = productList;
					});
				};
				
				$scope.getProductList();
				
				/*Get subproduct list  */
				 $scope.getSubProductList = function() {
					 productService.getSubProductLists().then(function(subProducts){
						 $scope.subItems = subProducts;
					 });
				 };
				 
				 $scope.getSubProductList();
				 
				$scope.$on('refreshProducts',function(event,data){   
					  $scope.getProductList();
					switch( data.data.responseCode ) {
						case 201:
							$mdDialog.cancel();
							break;
						case 200:
							$mdDialog.cancel();
							break;
					}
					 // $scope.checkResponseCode(data.data.responseCode);
				  });
					
				
				 
				function isSubProductEqual(obj1,obj2) {
					if(obj1.length !== obj2.length) {
						return false;
					}
					obj1 = _.sortBy( obj1, 'subitem' );
					obj2 = _.sortBy( obj2, 'subitem' );
					var isEqual = false;
					for(var i = 0; i < obj1.length; i++) {
						if(obj1[i].subproductName === obj2[i].subproductName && obj1[i].quantity === obj2[i].quantity ) {
							isEqual = true;
						} else {
							isEqual = false;
						}
					}
			   		return isEqual;
				}
				
			
				
				/*Delete added item*/     
				$scope.deleteItem = function (index) {
					$scope.ctrl.invoiceitems.splice(index, 1);
					$scope.ctrl.calculateTotalAndAmount();
					$scope.updateButton = false;
					$scope.saveButton = true;
				}
					    
				/*Copy items into text fields which want to edited*/    
				$scope.editItem = function( newItem, index ) {
					$scope.updateButton = true;
					$scope.saveButton = false;
					$scope.resetItem();
					
					angular.copy( newItem , $scope.newItem );
					$scope.changeSubItemValidation();
					angular.forEach(newItem.invoiceItemSubProducts,function(value) {
						$scope.selectedSubItemArray.push(value.subproductName);
						$scope.dropdown_models[value.subproductName] = value.quantity;
						$scope.invoiceItemIds[value.subproductName] = value.invoiceitemsubproductId;
					});
					$scope.newItem.index = index;
				}
					    
				/*Update item values*/    
				$scope.updateItem = function() {
					$scope.ctrl.newInvoice.amount += +$scope.newItem.quantity * +$scope.newItem.price;
					var index = $scope.newItem.index;
					delete $scope.newItem.index;
					$scope.ctrl.invoiceitems.splice(index,1);
					$scope.addItem();
					$scope.updateButton = false;
					$scope.saveButton = true;
					$scope.resetItem();
				}
				
				$scope.$watchCollection('selectedSubItemArray', function(newValue, oldValue) {
					 $scope.newItem.invoiceItemSubProducts = [];
						angular.forEach(newValue,function(value) {
								if( $scope.dropdown_models[value] == undefined) {
									$scope.dropdown_models[value] = 1;
								}
								//TODO: remove this if next code works.
								var invoiceItemSubProductId;
								if($scope.invoiceItemIds[value] !== undefined) {
									invoiceItemSubProductId =  $scope.invoiceItemIds[value];
								} else {
									 invoiceItemSubProductId = 0;
								}
									
								$scope.newItem.invoiceItemSubProducts.push({
			  					subproductName : value,
			  					quantity : $scope.dropdown_models[value],
			  					invoiceitemsubproductId: invoiceItemSubProductId
			  					});
				  		});
				});

				$scope.$watchCollection("dropdown_models",function( newValue ) {
					angular.forEach( newValue ,function( value, key ) {
						angular.forEach($scope.newItem.invoiceItemSubProducts,function(subItem) {
							if(subItem.subproductName == key) {
								subItem.quantity = value;
							}
						});
					});
				});
				
				$scope.changeSubItemValidation = function() {
					
					 for(var i in $scope.productList) {
						 if($scope.newItem.psku == $scope.productList[i].psku) {
							 $scope.subItemValidation.VLAN = $scope.productList[i].maxVlanCount;
							 $scope.subItemValidation.APP = $scope.productList[i].maxAppCount;
						 }
					 }	
					 $scope.selectedSubItemArray = [];
					$scope.dropdown_models = {};
				}
			}]
		};
	});
})();	