(function() {
	'use strict';
	app.directive('itemPicker', function (MessageService) {
		return {
			restrict : 'E',
			replace : true,
			templateUrl : "views/partials/components/item-picker.html",
			scope: true,
			link : function(scope,ele,attr) {
				 scope = scope.ctrl.parent;
				 scope.addItem = function() {
					 if( scope.newItem.quantity != undefined && scope.newItem.quantity != "" && scope.newItem.quantity != null ) {
						if( scope.productItems.length === 0 ) {
							 scope.productItems.push(scope.newItem);
						} else {
							if (isItemNameExits(scope.productItems,scope.newItem.psku)) {
								 _.find(scope.productItems, function (item) {
									 if(item.psku === scope.newItem.psku) {
										 if(scope.maxQuantity < (+item.quantity + +scope.newItem.quantity)){
											 MessageService.showAlert("Quantity should not exceed than"+ scope.maxQuantity);
											 return;
										 }
										 item.quantity = +item.quantity + +scope.newItem.quantity;
									 }
								 });
							} else {
			                  scope.productItems.push(scope.newItem);
							}
						}
						scope.emptyNewItem();
					}
				}
				 
				 function isItemNameExits(items, inventoryName) {
						var flag = false;
						 _.find(items, function (item, index) {
							       if(item.psku == inventoryName) {
						        	   flag = true;
						           }
					          });
						 	return flag;
				 		}
					 
					 /*Copy selected item into respective fields*/
					 
					  scope.editItem = function(index) {
						  scope.updateItemFlag = true;
						  scope.newItem = {
					    		  index : index,
					    		  quantity : scope.productItems[index].quantity,
					    	      psku : scope.productItems[index].psku
					      }
					  }
					  
					  /*Update Selected item*/  
					  scope.updateItem = function() {
						scope.productItems[scope.newItem.index].quantity = scope.newItem.quantity;
					  
						scope.productItems[scope.newItem.index].psku =  scope.newItem.psku;
						
						scope.selectedItem = scope.products[0].psku;
						
						scope.emptyNewItem();
						
					  	scope.updateItemFlag = false;
					  }
					  
					  /*Delete Selected Item*/
					  scope.deleteItem = function (index) {
					    	 scope.productItems.splice(index, 1);
					  }
					  
					 scope.emptyNewItem = function() {
						 scope.newItem = {
									psku : "",
									quantity : "" 
							 	};
					 }
			}
		};
	});
})();
