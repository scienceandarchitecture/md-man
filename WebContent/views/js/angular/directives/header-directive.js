(function() {
	'use strict';
	/**
	 * Created by Benison pc on 9/10/2016.
	 */
	app.directive('meshHeader',function(routeNavigation,$route){
	    return {
	        restrict: 'E',
	        transclude: true,
	        templateUrl: 'views/partials/components/header.html',
	        controller: function($scope,$location) {
	            var currentPath = $location.path();
	            _.find($route.routes, function (route) {
	                if(currentPath === route.originalPath){
	                    $scope.pageTitle =  route.name;
	                    console.log($scope.pageTitle);
	                }
	            })
	        }
	    }
	});
})();	