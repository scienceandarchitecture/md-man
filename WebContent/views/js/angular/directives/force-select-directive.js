angular.module('mdApp').directive('forceSelect', ['$mdUtil', forceSelect]);

function forceSelect($mdUtil) {

  function postLink(scope, element, attrs, autoComplete) {
    let isSelected = (item) => {
      return autoComplete.matches.indexOf(item) !== -1;
    };

    $mdUtil.nextTick(() => {
      let ngModel = element.find('input').controller('ngModel');

      autoComplete.registerSelectedItemWatcher((selectedItem) => {
        ngModel.$setValidity('forceSelect', isSelected(selectedItem));
      });

      ngModel.$viewChangeListeners.push(() => {
        $mdUtil.nextTick(() => {
          ngModel.$setValidity('forceSelect', isSelected(autoComplete.scope.selectedItem));
        });
      });
    });
  }

  return {
    link: postLink,
    require: 'mdAutocomplete',
    restrict: 'A'
  };
}