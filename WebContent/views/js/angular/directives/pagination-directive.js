(function() {
	'use strict';
	app.directive('pagination',function() {
		return {
			templateUrl: 'views/partials/components/pagination.html',
			restrict: 'E',
			scope: {
				numberOfItemPerPage : '@perPageItemCount',
				totalPagesToShow: '=',
				getList:'&method',
				isPaginationInDailog:'='
			},
			controller:function($scope,$rootScope) {
				$scope.pagination = { numberOfItemPerPage: parseInt($scope.numberOfItemPerPage),goToPage:1,totalPagesToShow:parseInt($scope.totalPagesToShow),currentPage:1 };
				 $scope.paginationArray = [undefined];
				 
				 function setPaginationArray(start,end) {
					 for(var i = start; i <= end; i++ ) {
						 $scope.paginationArray.push(i);
					}
				 } 
				 $scope.setPagination = function(listCount,currentPage) {
					 $scope.pagination.listCount = listCount;
					 $scope.pagination.totalPages = Math.ceil( $scope.pagination.listCount / $scope.pagination.numberOfItemPerPage );
					 if( $scope.pagination.goToLastPage ) {
						 $scope.pagination.currentPage = $scope.pagination.totalPages;
						 $scope.pagination.goToLastPage = false;
					 } else {
						 $scope.pagination.currentPage = currentPage;
					 }
					 
					 if( currentPage  >  $scope.pagination.totalPages ) {
						 $scope.pagination.currentPage = 1;
					 }
					 $scope.paginationArray = [];
					 if( $scope.pagination.totalPages < $scope.pagination.totalPagesToShow ) {
						 setPaginationArray(1,$scope.pagination.totalPages);
						 return;
					 }
					 
					 if( $scope.pagination.currentPage < $scope.pagination.totalPagesToShow ) {
						 setPaginationArray(1,$scope.pagination.totalPagesToShow);
					 } else if($scope.pagination.currentPage > $scope.pagination.totalPages - $scope.pagination.totalPagesToShow) {
						 setPaginationArray( $scope.pagination.totalPages - $scope.pagination.totalPagesToShow , $scope.pagination.totalPages );
					 } else {
						 setPaginationArray( $scope.pagination.currentPage - Math.round($scope.pagination.totalPagesToShow /2 ), $scope.pagination.currentPage + Math.round($scope.pagination.totalPagesToShow /2 ) - 1 );
						 
					 }
				 }
				 
				 $scope.$watch('numberOfItemPerPage',function( newValue ) {
					 $scope.pagination.numberOfItemPerPage = parseInt( newValue );
					 $scope.getPaginationList(1);
				 });
				
				 $scope.$on('goToLastPage',function(event) {
					 
					  $scope.getPaginationList($scope.pagination.totalPages );
				 });
				 
				 $scope.$on('goToSpecificPage',function(event,pageNumber) {
					 if(pageNumber > 0) {
						 $scope.getPaginationList( pageNumber );
						 return;
					 }
					 $scope.$broadcast('refreshPage');
				 });
				 
				 $scope.$on('goToSpecificPageForDailog',function(event,pageNumber) { 
					 if(pageNumber > 0 && $scope.isPaginationInDailog ) {
						 $scope.getPaginationList( pageNumber );
						 return;
					 }
				 })
				 
				 $scope.$on('refreshPage',function(  ) {
						 $scope.getPaginationList($scope.pagination.currentPage );
				 });
				 
				 $scope.getPaginationList = function(pageNumber) {
					 $scope.getList({pageNumber:pageNumber,setPagination:$scope.setPagination});
				 }
				 
			}
		};
	});
})();	