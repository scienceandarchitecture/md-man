(function() {
	'use strict';
	app.filter('flashingStatusConvertor',function(appConstants) {
		return function(input) {
			if(input !== undefined) {
				return appConstants.flashingStatus[input];
			}
			return "";
		}
	});
})();