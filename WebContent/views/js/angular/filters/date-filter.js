(function() {
	'use strict';
	app.filter('dateFilter',["$filter",function($filter) {
		return function(input) {
			var t, result = null;
				if( input !== '' && input !== undefined ) {
			      t = input.split(/[- :]/);
			      //when t[3], t[4] and t[5] are missing they defaults to zero
			     return $filter('date')(new Date(t[0], t[1] - 1, t[2], t[3] || 0, t[4] || 0, t[5] || 0),'MM-dd-yyyy');  
			   }
				return "";
		}
		
		function convertUTCDateToLocalDate(date) {
		    var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);
	
		    var offset = date.getTimezoneOffset() / 60;
		    var hours = date.getHours();
	
		    newDate.setHours(hours - offset);
	
		    return newDate;   
		}
	}]);
})();