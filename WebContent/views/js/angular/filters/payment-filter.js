(function() {
	'use strict';
	app.filter('paymentFilter',['compareDateService',function(compareDateService) {
		return function(payments,selectedFilter) {
			let filteredPayments = [];
			angular.forEach(payments, function(payment) {
				var currentDate = new Date(); 
				var compareDate;
				if( payment !== undefined ) {
					switch (selectedFilter) {
						case "all":
							filteredPayments.push(payment);
							break;
						case "open":
							if(payment.state == "open") {
								filteredPayments.push(payment);
							}
							break;
						case "this_month":
							compareDate = new Date.createFromMysql(payment.created);
							if( compareDateService.compareDate(currentDate,compareDate,"month") ) {
								filteredPayments.push(payment);
							}
							break;
						case "this_year":
							compareDate = new Date.createFromMysql(payment.created);
							if( compareDateService.compareDate(currentDate,compareDate,"year") ) {
								filteredPayments.push(payment);
							}
							break;
					}
				}
			});
			return filteredPayments;
		}
	}]);	
})();