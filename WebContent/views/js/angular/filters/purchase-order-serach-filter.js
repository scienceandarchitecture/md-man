(function() {
	'use strict';
	app.filter('purchaseOrderSearchFilter',function() {
		return function(purchaseOrders,selectedFilter,filteredText) {
			let filteredPurchaseOrders = [];
			angular.forEach(purchaseOrders, function(purchaseOrder) {
				switch (selectedFilter) {
					case undefined:
						filteredPurchaseOrders.push(purchaseOrder);
						break;
					case "custName":
						if(purchaseOrder.custName.toLowerCase().indexOf(filteredText.toLowerCase()) !== -1) {
							filteredPurchaseOrders.push(purchaseOrder);
						}
						break;
					case "salesRep":
						if(purchaseOrder.salesRep.toLowerCase().indexOf(filteredText.toLowerCase()) !== -1) {
							filteredPurchaseOrders.push(purchaseOrder);
						}
						break;
					case "invId":
						if(purchaseOrder.invId.toString().indexOf(filteredText) !== -1) {
							filteredPurchaseOrders.push(purchaseOrder);
						}
						break;
				}
			});
			return filteredPurchaseOrders;
		}
	});
})();