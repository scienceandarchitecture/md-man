(function() {
	'use strict';
	app.filter('purchaseOrderByTypeFilter',['compareDateService',function(compareDateService) {
		return function(purchaseOrders,selectedShowFilter,filteredText) {
			let filteredPurchaseOrders = [];
			angular.forEach(purchaseOrders, function(purchaseOrder) {
				var currentDate = new Date(); 
				var compareDate;
				if( purchaseOrder !== undefined ) {
					switch (selectedShowFilter) {
						case "all":
							filteredPurchaseOrders.push(purchaseOrder);
							break;
						case "open":
							if( purchaseOrder.status == "open" ) {
								filteredPurchaseOrders.push(purchaseOrder);
							}
							break;
						case "issued_this_month":
							compareDate = new Date.createFromMysql(purchaseOrder.invDate);
							if( compareDateService.compareDate(currentDate,compareDate,"month") ) {
								filteredPurchaseOrders.push(purchaseOrder);
							}
							break;
						case "due_this_month":
							compareDate = new Date.createFromMysql(purchaseOrder.dueDate);
							if( compareDateService.compareDate(currentDate,compareDate,"month") ) {
								filteredPurchaseOrders.push(purchaseOrder);
							}
							break;
						case "issued_this_year":
							compareDate = new Date.createFromMysql(purchaseOrder.invDate);
							if(compareDateService.compareDate(currentDate,compareDate,"year")) {
								filteredPurchaseOrders.push(purchaseOrder);
							}
							break;
						case "due_this_year":
							compareDate = new Date.createFromMysql(purchaseOrder.dueDate);
							if(compareDateService.compareDate(currentDate,compareDate,"year")) {
								filteredPurchaseOrders.push(purchaseOrder);
							}
							break;
					}
				}
			});
			return filteredPurchaseOrders;
		}
	}]);	
})();