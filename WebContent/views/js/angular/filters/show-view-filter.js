(function() {
	'use strict';
	app.filter('showView',function() {
		return function(input) {
			if(input !== undefined) {
				return JSON.parse(input).join(",");
			}
			return false;
		}
	});
})();