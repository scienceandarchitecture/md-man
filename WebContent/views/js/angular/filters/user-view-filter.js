(function() {
	'use strict';
	app.filter('userView',function() {
		return function(input,selectedFilter,filterText) {
			 return input.filter(function(user, index, array) {
				 switch (selectedFilter) {
					case null:
						return true;
					case "username":
						if(user.userName.toLowerCase().indexOf(filterText.toLowerCase()) !== -1) {
							return true;
						}
						return false;
					case "email":
						if(user.email.toLowerCase().indexOf(filterText.toLowerCase()) !== -1) {
							return true;
						}
						return false;
					case "fullname":
						if(user.fullName.toLowerCase().indexOf(filterText.toLowerCase()) !== -1) {
							return true;
						}
						return false;
					case "roles":
						if(user.roles.toLowerCase().indexOf(filterText.toLowerCase()) !== -1) {
							return true;
						}
						return false;	
					
				 }
			 });
		}
	});
})();