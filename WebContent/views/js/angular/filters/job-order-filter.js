(function() {
	'use strict';
	app.filter('jobOrderFilter',function() {
		return function(input,invoiceId) {
			if(invoiceId !== undefined) {
				 return input.filter(function(element, index, array) {
					 return element.invId === invoiceId && element.state === 'finished';
				 });
			}
			return false;
			
		}
	});
})();